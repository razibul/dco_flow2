#ifndef __LOGO_H__
#define __LOGO_H__

#include <stdio.h>

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


void PrintLogo()
{
    printf("                                                                     \n");
    printf("             `///-                                        `....`     \n");
    printf("MMMMMMMMMMM- -MMMs                                      hNMMMMMMmy.  \n");
    printf("MMMMsoooooo` -MMMs      .:::-`    `...`    ...`    ...` mds++odMMMN` \n");
    printf("MMMM.``````  -MMMs   -hNMMMMMMmo  .MMMm   /MMMm`  +MMM+       `MMMM. \n");
    printf("MMMMMMMMMMd  -MMMs  :MMMm-``+MMMd  oMMM+ `NMMMMs  mMMd       `hMMMs  \n");
    printf("MMMMo+++++/  -MMMs  yMMM/    dMMM-  mMMN`sMM/NMM-/MMM-     `oNMMd:   \n");
    printf("MMMM`        -MMMs  sMMMo    NMMM.  -MMMyMMh :MMdmMMs    :yMMMy:     \n");
    printf("MMMM`        -MMMs  `dMMMs+odMMM+    yMMMMM.  hMMMMN`  -NMMMMmdddddh \n");
    printf("hhhh`        .hhh+    :sdmNNmho.     `hhhh+   .hhhh:   :hhhhhhhhhhhh \n");
    printf("                                                                              \n");
    printf("                                                                              \n");
}



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __LOGO_H__
