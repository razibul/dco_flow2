#ifndef __STOPWATCH_H__
#define __STOPWATCH_H__

#include <time.h>
#include "libcoresystem/mgdecl.h"

//////////////////////////////////////////////////////////////////////
// class StopWatch
//////////////////////////////////////////////////////////////////////
class StopWatch
{
public:
	StopWatch()		{ Reset(); }

	void	Reset()
	{
		mTotalTime = 0.0;
	}

	void	Start()
	{
		mStart = clock();
	}

	void	Mark()
	{
		mFinish = clock();
		mDuration = (MGFloat)(mFinish - mStart) / CLOCKS_PER_SEC;
		mStart = mFinish;
		mTotalTime += mDuration;
	}

	const MGFloat&	cTotalTime() const		{ return mTotalTime; }
	const MGFloat&	cDuration() const		{ return mDuration; }

private:
	clock_t		mStart;
	clock_t		mFinish;
	MGFloat		mDuration;
	MGFloat		mTotalTime;
};




#endif // __STOPWATCH_H__
