#ifndef __MULTITAB_H__
#define __MULTITAB_H__

#include "mgdecl.h"
#include "vect.h"

//////////////////////////////////////////////////////////////////////
// class Cage
//////////////////////////////////////////////////////////////////////
class Cage 
{
public:

	set<MGSize>&	rSet()	{ return msetId;}

private:
	set<MGSize>	msetId;
};




//////////////////////////////////////////////////////////////////////
// class MultiTab
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class MultiTab
{
public:

	void	Init( const Vect<MGFloat,DIM>& vmin, const Vect<MGFloat,DIM>& vmax, const MGFloat& delta);
	void	Insert( const Vect<MGFloat,DIM>& vct, const MGSize& id);

	MGSize	Index( const Vect<MGFloat,DIM>& vct);

	set<MGSize>&	rSet( const Vect<MGFloat,DIM>& vct)		{ return mTab[Index(vct)].rSet(); }

private:
	Vect<MGSize,DIM>	mvN;
	Vect<MGFloat,DIM>	mvDX;
	Vect<MGFloat,DIM>	mVmax;
	Vect<MGFloat,DIM>	mVmin;

	vector<Cage>		mTab;
};




template <Dimension DIM>
MGSize MultiTab<DIM>::Index( const Vect<MGFloat,DIM>& vct)
{
	Vect<MGSize,DIM>	vid;
	Vect<MGFloat,DIM>	dv = vct - mVmin;

	for ( MGSize k=0; k<DIM; ++k)
	{
		vid.rX(k) = (MGSize)floor( dv.cX(k) / mvDX.cX(k) );
	}

	MGSize	size = 1;
	MGSize	id = 0;
	for ( MGSize k=0; k<DIM; ++k)
	{
		id += vid.cX(k) * size;

		size *= mvN.cX(k);
	}

	return id;
}


template <Dimension DIM>
void MultiTab<DIM>::Insert( const Vect<MGFloat,DIM>& vct, const MGSize& id)
{
	MGSize	ind = Index( vct);

	set<MGSize> &rset = mTab[ind].rSet();

	if ( rset.find( id) == rset.end() )
		rset.insert( id);
}


template <Dimension DIM>
void MultiTab<DIM>::Init( const Vect<MGFloat,DIM>& vmin, const Vect<MGFloat,DIM>& vmax, const MGFloat& delta)
{
	Vect<MGFloat,DIM>	vd = vmax - vmin;

	mVmin = vmin;
	mVmax = vmax;

	MGSize	size = 1;

	for ( MGSize k=0; k<DIM; ++k)
	{
		MGFloat dn = vd.cX(k) / delta;
		mvN.rX(k) = (MGSize)ceil(dn);

		size *= mvN.cX(k);

		mvDX.rX(k) = vd.cX(k) / (MGFloat)mvN.cX(k);

		printf( "nx%1d = %d; dx%1d = %lg\n", k, mvN.cX(k), k, mvDX.cX(k) );
	}
	printf( "size of tab = %d\n", size);

	mTab.resize( size);
}


#endif // __MULTITAB_H__
