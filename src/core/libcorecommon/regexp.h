#ifndef __REGEXP_H__
#define __REGEXP_H__


// Definitions etc. for regexp(3) routines.
// Caveat:  this is V8 regexp(3) [actually, a reimplementation thereof],
// not the System V one.


#include "libcoresystem/mgdecl.h"




// The first byte of the regexp internal "program" is actually this magic
// number; the start node begins in the second byte.
const int	MAGIC =	0234;


// max. number of substrings in a regular expression
const int	NSUBEXP = 10;


//////////////////////////////////////////////////////////////////////
/// class RegExp
//////////////////////////////////////////////////////////////////////
class RegExp
{
public:
	RegExp();
	~RegExp();

	void	InitPattern( const string& str);		// set and compile search pattern
	void	SetString( const string& str);			// set string and try to fit to pattern
	string	GetSubString( int ind);			// returns substring if IsOk = true (1-9)
	int		GetNoSubString();				// returns no of substrings
	bool	IsOk()		{ return mbBufOk && mbPatternOk;}		// true if successful


protected:

	// internal data struct
	struct regexp 
	{
		const char	*startp[NSUBEXP];
		const char	*endp[NSUBEXP];
		char		regstart;		// Internal use only.
		char		reganch;		// Internal use only.
		char		*regmust;		// Internal use only.
		int			regmlen;		// Internal use only.
		char		program[1];		// Unwarranted chumminess with compiler.
	};
	
	bool	mbPatternOk;
	bool	mbBufOk;
	string	msPattern;
	string	msBuf;

	regexp	*mpRex;


private:
	const char	*regparse;		// Input-scan pointer.
	int			regnpar;		// () count.
	char		regdummy;
	char		*regcode;		// Code-emit pointer; &regdummy = don't. 
	long		regsize;		// Code size. 
	const char	*reginput;		// String-input pointer. 
	const char	*regbol;		// Beginning of input, for ^ check. 
	const char	**regstartp;	// Pointer to startp array. 
	const char	**regendp;		// Ditto for endp. 

	void	RegComp();
	int		RegExec();
	char*	Reg( int paren, int* flagp);
	char*	RegBranch( int* flagp);
	char*	RegPiece( int* flagp);
	char*	RegAtom( int* flagp);
	char*	RegNode( char op);
	char*	RegNext( char* p);
	void	RegC( char c);
	void	RegInsert( char op, char* opnd);
	void	RegTail( char* p, char* val);
	void	RegOpTail( char* p,  char* val);
	int		RegMatch( char* prog); // 0 failure, 1 success 
	int		RegTry( regexp* prog, const char* pstr);
	int		RegRepeat( char* p);
};


#endif // __REGEXP_H__
