#ifndef __INTERPOL_H__
#define __INTERPOL_H__

#include "libcoresystem/mgdecl.h"


//////////////////////////////////////////////////////////////////////
// class InterpBase - for interpolation; uses cubic bases
//////////////////////////////////////////////////////////////////////
class InterpBase
{
public:
	InterpBase();
	virtual ~InterpBase()	{ Destroy();}

	virtual MGFloat	Fun( const MGFloat& xx) const		= 0;
	virtual void	Diff( const MGFloat& xx, MGFloat& yy, MGFloat& d1y, MGFloat& d2y) const	= 0;
	virtual void	CalcCoeff()		= 0;


	virtual void Init( const MGInt& n);
	virtual void CopyPoints( const MGInt& nn, MGFloatArr *ptabx, MGFloatArr *ptaby);
	virtual void UsePoints( const MGInt& nn, MGFloatArr *ptabx, MGFloatArr *ptaby);

	const MGFloat&	ParamTb( const MGInt& nx) const;
	const MGFloat&	FunTb( const MGInt& nx) const;

	MGFloat&		rParamTb( const MGInt& nx);
	MGFloat&		rFunTb( const MGInt& nx);
	
	MGFloatArr*		GetPtrTabX()		{ return mptabX;}
	MGFloatArr*		GetPtrTabY()		{ return mptabY;}

	const MGFloat&	LeftVal() const		{ return mXl;}
	const MGFloat&	RightVal() const	{ return mXr;}
	const MGInt&	Size() const		{ return mNop;}

	void	ExportTEC( char *fname);

protected:
	bool		mbDir;
    bool		mbDestr;
	MGInt		mNop;
	MGFloat		mXl, mXr;
	MGFloatArr	*mptabX;
	MGFloatArr	*mptabY;


	virtual void Destroy();
};
//////////////////////////////////////////////////////////////////////


inline const MGFloat& InterpBase::FunTb( const MGInt& nx) const
{
	return (*mptabY)[nx];
}

inline const MGFloat& InterpBase::ParamTb( const MGInt& nx) const
{
	return (*mptabX)[nx];
}


inline MGFloat& InterpBase::rFunTb( const MGInt& nx)
{
	return (*mptabY)[nx];
}

inline MGFloat& InterpBase::rParamTb( const MGInt& nx)
{
	return (*mptabX)[nx];
}



//////////////////////////////////////////////////////////////////////
// class Spline - for interpolation; uses cubic bases
//////////////////////////////////////////////////////////////////////
class Spline : public InterpBase
{
public:
	Spline()						{}
	Spline( const Spline& spli)		{ ASSERT(0);}
	virtual ~Spline()				{}

	virtual MGFloat	Fun( const MGFloat& xx) const;
	virtual void	Diff( const MGFloat& xx, MGFloat& yy, MGFloat& d1y, MGFloat& d2y) const;
	virtual void CalcCoeff();


	virtual void Init( const MGInt& n);	


	
protected:
	MGFloatArr	mtabA1;
	MGFloatArr	mtabA2;
	MGFloatArr	mtabA3;
	MGFloatArr	mtabA4;


	virtual void Destroy();
};
//////////////////////////////////////////////////////////////////////







inline MGFloat Spline::Fun( const MGFloat& xx) const
{
	static MGInt	i;
	static MGFloat	t;

	i = 0;
	do
	{
		i++;
		if ( i >= mNop )
			break;
	}
	while ( (( (*mptabX)[i] < xx && mbDir) || ( (*mptabX)[i] > xx && !mbDir)) );

	if ( i >= mNop)
		i -= 2;
	else
		--i;

	t = xx - (*mptabX)[i];

	return ((mtabA4[i]*t + mtabA3[i])*t + mtabA2[i])*t + mtabA1[i];
}




//////////////////////////////////////////////////////////////////////
// class PolyLine - for interpolation; uses linear interpolation
//////////////////////////////////////////////////////////////////////
class PolyLine : public InterpBase
{
public :

	PolyLine()							{}
	PolyLine( const PolyLine& pline)	{ ASSERT(0);}
	virtual ~PolyLine()					{}

	virtual MGFloat	Fun( const MGFloat& xx) const;
	virtual void	Diff( const MGFloat& xx, MGFloat& yy, MGFloat& d1y, MGFloat& d2y) const;
	virtual void CalcCoeff()	{}
	
protected :
};
//////////////////////////////////////////////////////////////////////



inline MGFloat PolyLine::Fun( const MGFloat& xx) const
{
	MGInt i = 0;

	do
	{
		i++;
		if ( i >= mNop )
			break;
	}
	while ( (( (*mptabX)[i] < xx && mbDir) || ( (*mptabX)[i] > xx && !mbDir)) );

	if ( i >= mNop)
		i -= 2;
	else
		i--;

	return ((*mptabY)[i+1] - (*mptabY)[i])/( (*mptabX)[i+1] - (*mptabX)[i])*(xx - (*mptabX)[i]) + (*mptabY)[i];
}




#endif // __INTERPOL_H__
