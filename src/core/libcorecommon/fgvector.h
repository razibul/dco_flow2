#ifndef __FGVECTOR_H__
#define __FGVECTOR_H__

#include "libcoresystem/mgdecl.h"


//////////////////////////////////////////////////////////////////////
//	class fgvector - dynamic array with gaps; based on vector container
//	indexing is like for fortran arrays - 1...N
//////////////////////////////////////////////////////////////////////
template <class T>
class fgvector
{
	typedef pair<T,bool>		pair_type;
	typedef vector<pair_type>	vector_type;

public:
	typedef size_t				size_type;
	typedef T					value_type;
	typedef value_type			*pointer;
	typedef value_type&			reference;
	typedef const value_type	*const_pointer;
	typedef const value_type&	const_reference;


	class const_iterator;


	////////////////////////////
	// CLASS iterator
	////////////////////////////
	class iterator
	{
		friend class fgvector<T>;
		friend class const_iterator;

	protected:		
		iterator( const size_type& ipos, vector_type* ptab) : miPos(ipos), mpTab(ptab)	{}

	public:
		typedef forward_iterator_tag			iterator_category;
		typedef class fgvector<T>				parent;

		typedef typename parent::pointer		pointer;
		typedef typename parent::reference		reference;
		typedef typename parent::value_type		value_type;

		
		iterator()	{}

		iterator( const iterator& itr)	{ miPos=itr.miPos; mpTab=itr.mpTab;}

		const size_type&	index() const		{ return miPos;}

		const bool&		is_valid() const	{ return (*mpTab)[miPos].second;}

		
		reference operator*() const
		{
			return (*mpTab)[miPos].first;
		}

		pointer operator->() const
		{
			return (&( (*mpTab)[miPos].first ));
		}

		iterator& operator ++()	
		{
			do	{
				++miPos;
				if ( miPos >= mpTab->size() )
					break;
			} while ( (! (*mpTab)[miPos].second) );
			return (*this);
		}

		iterator operator ++(int)	
		{
			iterator itmp(*this);
			do	{
				++miPos;
				if ( miPos >= mpTab->size() )
					break;
			} while ( (! (*mpTab)[miPos].second) );
			return itmp;
		}

		iterator& operator --()	
		{
			do	{
				--miPos;
				if ( miPos <= 0 )
					break;
			} while ( (! (*mpTab)[miPos].second) );
			return (*this);
		}

		iterator operator --(int)	
		{
			iterator itmp(*this);
			do	{
				--miPos;
				if ( miPos <= 0 )
					break;
			} while ( (! (*mpTab)[miPos].second) );
			return itmp;
		}

		bool operator == (const iterator& i2) const
		{
			ASSERT( mpTab == i2.mpTab );
			return ( miPos == i2.miPos);
		}

		bool operator != (const iterator& i2) const
		{
			ASSERT( mpTab == i2.mpTab );
			return ( miPos != i2.miPos);
		}

		bool operator < (const iterator& i2) const
		{
			ASSERT( mpTab == i2.mpTab );
			return ( miPos < i2.miPos);
		}

		bool operator > (const iterator& i2) const
		{
			ASSERT( mpTab == i2.mpTab );
			return ( miPos > i2.miPos);
		}

		bool operator <= (const iterator& i2) const
		{
			ASSERT( mpTab == i2.mpTab );
			return ( miPos <= i2.miPos);
		}

		bool operator >= (const iterator& i2) const
		{
			ASSERT( mpTab == i2.mpTab );
			return ( miPos >= i2.miPos);
		}

	protected:
		size_type		miPos;
		vector_type	*mpTab;
	};


	////////////////////////////
	// CLASS const_iterator
	////////////////////////////
	class const_iterator
	{
		friend class fgvector<T>;

	protected:		
		const_iterator( const size_type& ipos, const vector_type* ptab) : miPos(ipos), mpTab(ptab)	{}

	public:
		typedef forward_iterator_tag				iterator_category;
		typedef class fgvector<T>					parent;

		typedef typename parent::const_pointer		const_pointer;
		typedef typename parent::const_reference	const_reference;
		typedef typename parent::value_type			value_type;



		const_iterator() : miPos(0), mpTab(NULL)	{}

		const_iterator( const const_iterator& itr) : miPos(itr.miPos), mpTab(itr.mpTab)	{}
		const_iterator( const iterator& itr) : miPos(itr.miPos), mpTab(itr.mpTab)	{}

		
		
		const size_type&	index() const		{ return miPos;}

		const bool&		is_valid() const	{ return (*mpTab)[miPos].second;}


		const_reference operator*() const
		{
			return (*mpTab)[miPos].first;
		}

		const_pointer operator->() const
		{
			return (&( (*mpTab)[miPos].first ));
		}

		const_iterator& operator ++()	
		{
			do	{
				++miPos;
				if ( miPos >= mpTab->size() )
					break;
			} while ( (! (*mpTab)[miPos].second) );
			return (*this);
		}

		const_iterator operator ++(int)	
		{
			const_iterator itmp(*this);
			do	{
				++miPos;
				if ( miPos >= mpTab->size() )
					break;
			} while ( (! (*mpTab)[miPos].second) );
			return itmp;
		}

		const_iterator& operator --()	
		{
			do	{
				--miPos;
				if ( miPos <= 0 )
					break;
			} while ( (! (*mpTab)[miPos].second) );
			return (*this);
		}

		const_iterator operator --(int)	
		{
			const_iterator itmp(*this);
			do	{
				--miPos;
				if ( miPos <= 0 )
					break;
			} while ( (! (*mpTab)[miPos].second) );
			return itmp;
		}

		bool operator == (const const_iterator& i2) const
		{
			ASSERT( mpTab == i2.mpTab );
			return ( miPos == i2.miPos);
		}

		bool operator != (const const_iterator& i2) const
		{
			ASSERT( mpTab == i2.mpTab );
			return ( miPos != i2.miPos);
		}

		bool operator < (const const_iterator& i2) const
		{
			ASSERT( mpTab == i2.mpTab );
			return ( miPos < i2.miPos);
		}

		bool operator > (const const_iterator& i2) const
		{
			ASSERT( mpTab == i2.mpTab );
			return ( miPos > i2.miPos);
		}

		bool operator <= (const const_iterator& i2) const
		{
			ASSERT( mpTab == i2.mpTab );
			return ( miPos <= i2.miPos);
		}

		bool operator >= (const const_iterator& i2) const
		{
			ASSERT( mpTab == i2.mpTab );
			return ( miPos >= i2.miPos);
		}

	protected:
		size_type			miPos;
		const vector_type	*mpTab;
	};
	////////////////////////////

public:
	fgvector()	{ mtab.push_back( pair_type( value_type(), false) ); }	// inserting 0-element

	fgvector( const fgvector& t) : mtab(t.mtab), mtaber(t.mtaber)	{}



	const T&	operator[]( const size_type& i) const	{ return mtab[i].first;}
	T&			operator[]( const size_type& i)			{ return mtab[i].first;}

	const bool&	is_valid( const size_type& i) const		{ return mtab[i].second;}

	size_type		size() const						{ return mtab.size(); }
	size_type		size_valid() const					{ return mtab.size() - mtaber.size() - 1; }

	iterator	begin()									{ iterator itr( 0, &mtab); return ++itr; }
	iterator	end()									{ return iterator( mtab.size(), &mtab); }

	const_iterator	begin() const						{ const_iterator itr( 0, &mtab); return ++itr; }
	const_iterator	end() const							{ return const_iterator( mtab.size(), &mtab); }


	size_type	insert( const value_type& t)
	{
		if ( mtaber.size() > 0)
		{
			const size_type& ind = mtaber.back();

			mtab[ind].first = t;
			mtab[ind].second = true;
			mtaber.pop_back();
			return ind;
		}
		else
		{
			//printf( " darray = %d / %d\n", mtab.size(), mtab.capacity() );
			mtab.push_back( pair_type( t, true) );
			return mtab.size() - 1;
		}
	}

	void	clear()
	{
		mtab.clear();
		mtaber.clear();
		mtab.push_back( pair_type( value_type(), false) );
	}

	void	erase( const size_type& ind)
	{
		if ( mtab[ind].second )
		{
			mtab[ind].second = false;
			mtaber.push_back( ind);
		}
	}

	void	erase( const iterator& itr)
	{
		if ( mtab[itr.index()].second )
		{
			mtab[itr.index()].second = false;
			mtaber.push_back( itr.index());
		}
	}

	//void	erase( const vector<size_type>& tab)
	template <class TSIZE>
	void	erase( const vector<TSIZE>& tab)
	{
		mtaber.reserve( mtaber.size() + tab.size() );
		for ( size_type i=0; i<tab.size(); ++i)
			if ( mtab[tab[i]].second )
			{
				mtab[ static_cast<size_t>( tab[i] ) ].second = false;
				mtaber.push_back( static_cast<size_t>( tab[i] ) );
			}
	}


	void resize( const size_type& newsize)
	{
		resize( newsize, value_type() );
		//mtab.resize( newsize);
		//mtaber.clear();
	}

	void resize( const size_type& newsize, const value_type& t)
	{
		mtab.resize( newsize+1, pair_type( t, true) );
		mtaber.clear();
		mtab[0] = pair_type( value_type(), false);	// inserting 0-element
	}

	void	consolidate();


	void	dump( ostream& f);

private:
	vector_type			mtab;
	vector<size_type>	mtaber;
};
//////////////////////////////////////////////////////////////////////



template <class T>
inline void fgvector<T>::consolidate()
{
	sort( mtaber.begin(), mtaber.end() );
	mtaber.erase( unique( mtaber.begin(), mtaber.end() ), mtaber.end() );
}


template <class T>
inline void fgvector<T>::dump( ostream& f=cout)
{
	sort( mtaber.begin(), mtaber.end() );

	for ( size_type i=1; i<mtaber.size(); ++i)
		if ( mtaber[i-1] == mtaber[i] )
		{
			f << "duplicate found : (" << mtaber[i] << ") :  "  << i-1 << "  " << i << endl;
		}


	for ( size_type i=0; i<mtab.size(); ++i)
	{
		bool bfound = binary_search( mtaber.begin(), mtaber.end(), i );

		if ( mtab[i].second && bfound )
			f << setw(4) << i << " true -- exist in taber" << endl;

		if ( !mtab[i].second && !bfound )
			f << setw(4) << i << " false -- not exist in taber" << endl;
	}

	size_type nold = mtaber.size();

	mtaber.erase( unique( mtaber.begin(), mtaber.end() ), mtaber.end() );

	size_type nnew = mtaber.size();

	f << nold << " / " << nnew << endl;

	f << "number of holes = " << mtaber.size() << endl;
	
	f << "MTAB:" << endl;
	for ( size_type i=0; i<mtab.size(); ++i)
		//fprintf( f, "%4d = %d | %1d\n", i, (int)mtab[i].first, (int)mtab[i].second);
		f << " " << setw(4) << i << " = " << /*mtab[i].first <<*/ " | " << (int)mtab[i].second << endl;

	//fprintf( f, "MSET:\n");
	f << "MSET:" << endl;
	for ( vector<size_type>::const_iterator sitr=mtaber.begin(); sitr!=mtaber.end(); ++sitr)
		f << " " << setw(4) << *sitr << endl;
	f << "END:" << endl;
}

#endif // __FGVECTOR_H__

