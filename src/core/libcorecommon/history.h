#ifndef __HISTORY_H__
#define __HISTORY_H__

#include <list>

//////////////////////////////////////////////////////////////////////
// class HistoryStack
//////////////////////////////////////////////////////////////////////
template <class T>
class HistoryStack
{
public:
	HistoryStack( const size_t& size) : mMaxSize(size)	{}

	size_t		Size()		{ return mBackwardStack.size(); }

	const T&	CurItem() const;
	void		ShiftBackward();
	void		ShiftForward();

	void	Push( const T& item);

private:
	size_t			mMaxSize;
	std::list<T>	mBackwardStack;
	std::list<T>	mForwardStack;
};




template <class T>
void HistoryStack<T>::Push( const T& item)
{
	mBackwardStack.push_back( item);
	while ( mBackwardStack.size() > mMaxSize)
		mBackwardStack.erase( mBackwardStack.begin() );

	mForwardStack.clear();
}


template <class T>
const T& HistoryStack<T>::CurItem() const
{
	return mBackwardStack.back();
}



template <class T>
void HistoryStack<T>::ShiftBackward() 
{
	typename std::list<T>::iterator itr;
	if ( mBackwardStack.size() > 1 )
	{
		itr = --mBackwardStack.end();
		mForwardStack.push_back( *itr);
		mBackwardStack.erase( itr);
	}
}

template <class T>
void HistoryStack<T>::ShiftForward() 
{
	typename std::list<T>::iterator itr;
	if ( mForwardStack.size() > 0 )
	{
		itr = --mForwardStack.end();
		mBackwardStack.push_back( *itr);
		mForwardStack.erase( itr);
	}
}



#endif // __HISTORY_H__
