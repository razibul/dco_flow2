#ifndef __ARRBITSET_H__
#define __ARRBITSET_H__

#include "libcoresystem/mgdecl.h"



//////////////////////////////////////////////////////////////////////
//  class arrbitset
//
//  SHIFT defines the size of the chunk bitsets ( 2^SHIFT ). 
//  By defalt the chunk size is 1024
//////////////////////////////////////////////////////////////////////
template <size_t SHIFT=10>
class arrbitset
{
public:

	typedef size_t			size_type;
	typedef bool			value_type;

	enum { MASK = ~((~static_cast<size_type>(0)) << SHIFT ) };
	enum { CHUNKSIZE = static_cast<size_type>(1) << SHIFT }; 

	typedef	bitset<CHUNKSIZE>							chunk_type;
	typedef allocator<chunk_type>						chunkallocator_type;
	typedef vector< chunk_type, chunkallocator_type >	vector_type;


public:
	arrbitset()	: msize(0)	{}


	size_type			size() const		{ return msize;}
	size_type			chunk_size() const	{ return CHUNKSIZE;}

	void				resize( const size_type& size)
						{
							reserve( size);
							msize = size;
						}
						
	void 				resize( const size_type& newsize, const bool& b)
						{
							msize = newsize;
							const size_type n = master_index(msize) + 1;
							mtab.resize(n);

							if ( b)
								for ( size_type i=0; i<n; ++i)
									mtab[i].set();
							else
								for ( size_type i=0; i<n; ++i)
									mtab[i].reset();
						}
						

	void				reserve( const size_type& size)
						{
							const size_type n = master_index(size) + 1;
							if ( mtab.size() == n)
								return;

							if ( mtab.size() < n )
							{
								const size_type cursize = mtab.size();
								mtab.resize( n);
							}
							else
							{
								const size_type cursize = mtab.size();
								mtab.resize(n);
							}
						}

	void				clear()
						{
							mtab.clear();
							msize = 0;
						}


	bool				operator[]( const size_type& i) const	{ return mtab[master_index(i)].test( inner_index(i) );}
	void				set( const size_type& i, const bool& b)	{ mtab[master_index(i)].set( inner_index(i), b );}

	void				push_back( const bool& b)
						{
							const size_type id = msize;
							resize( msize + 1);
							this->set(id,b);
						}

	void				pop_back()
						{
							resize( msize - 1);
						}


	size_type			master_index( const size_type& i) const	{ return i >> SHIFT; }
	size_type			inner_index( const size_type& i) const	{ return i & MASK; }

	void				dump( ostream& f=cout) const
						{
							f << "msize = " << msize << endl;
							f << "mtab.size() = " << mtab.size() << endl;
							for ( size_type i=0; i<mtab.size(); ++i)
							{
								f << "   mtab[" << i << "].size() = " << mtab[i].size() << endl;
								for ( size_type k=0; k<mtab[i].size(); ++k)
									f << mtab[i].test(k) << " ";
								f << endl;
							}
						}

private:
	size_type	msize;	// number of all valid elements
	vector_type	mtab;
};



#endif // __ARRBITSET_H__
