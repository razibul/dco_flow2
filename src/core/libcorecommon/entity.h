#ifndef __ENTITY_H__
#define __ENTITY_H__

#include "libcoresystem/mgdecl.h"

//////////////////////////////////////////////////////////////////////
//	class Entity
//////////////////////////////////////////////////////////////////////
class Entity
{
public:
	Entity( const MGSize& id=0) : mId(id), mName("")	{}

	MGSize&			rId()		{ return mId;}
	const MGSize&	cId() const	{ return mId;}

	MGString&		rName()			{ return mName;}
	const MGString&	cName() const	{ return mName;}

private:
	MGSize		mId;
	MGString	mName;
};
//////////////////////////////////////////////////////////////////////


#endif // __ENTITY_H__

