#ifndef __VERSION_H__
#define __VERSION_H__



//////////////////////////////////////////////////////////////////////
/// class Version
//////////////////////////////////////////////////////////////////////
class Version
{
public:
	Version( MGString snum, MGString stxt, MGString sdat, MGString stim) 
		: mNumber(snum), mText(stxt), mDate(sdat), mTime(stim)	{}

		const char*	Number()	{ return mNumber.c_str();}
		const char*	Text()		{ return mText.c_str();}
		const char*	Date()		{ return mDate.c_str();}
		const char*	Time()		{ return mTime.c_str();}

protected:
	MGString	mNumber;
	MGString	mText;
	MGString	mDate;
	MGString	mTime;

};



//////////////////////////////////////////////////////////////////////
// macros for version tracking
//////////////////////////////////////////////////////////////////////
#define INIT_VERSION(f1,f2)	Version gVer( f1, f2, __DATE__, __TIME__)

extern Version gVer;

#define VERSION_NUMBER	gVer.Number()
#define VERSION_DATE	gVer.Date()
#define VERSION_TIME	gVer.Time()
#define VERSION_TEXT	gVer.Text()


#endif // __VERSION_H__
