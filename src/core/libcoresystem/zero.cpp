#include "zero.h"

MGFloat	Zero::length	= 1.0e-10;
MGFloat	Zero::area		= Zero::length * Zero::length;
MGFloat	Zero::volume	= Zero::length * Zero::length * Zero::length;

