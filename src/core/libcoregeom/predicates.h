#ifndef __PREDICATES_H__
#define __PREDICATES_H__
#include"dco.hpp"
//#define CPU86


//razibul:
/*
#ifdef SINGLE
  #define REAL float
#else
  #define REAL double
#endif 	// not defined SINGLE*/
//razibul: Here with dco::gt1s<double>::type tangent linear mode can be switched on or dco::ga1s<double>::type adjoint mode can be switched on
#ifdef SINGLE
  #define REAL dco::ga1s<float>::type
#else
  #define REAL dco::ga1s<double>::type
#endif 	// not defined SINGLE

// initialization
REAL exactinit();

// 2D stuff
REAL orient2d(const REAL *pa, const REAL *pb, const REAL *pc);
REAL orient2dfast(const REAL *pa, const REAL *pb, const REAL *pc);

REAL incircle(const REAL *pa, const REAL *pb, const REAL *pc, const REAL *pd);
REAL incirclefast(const REAL *pa, const REAL *pb, const REAL *pc, const REAL *pd);

// 3D stuff
REAL orient3d(const REAL *pa, const REAL *pb, const REAL *pc, const REAL *pd);
REAL orient3dfast(const REAL *pa, const REAL *pb, const REAL *pc, const REAL *pd);

REAL insphere(const REAL *pa, const REAL *pb, const REAL *pc, const REAL *pd, const REAL *pe);
REAL inspherefast(const REAL *pa, const REAL *pb, const REAL *pc, const REAL *pd, const REAL *pe);


#endif // __PREDICATES_H__
