#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace SpaceGeomTools {




const char	STRDIM_NONE[]	= "NONE";
const char	STRDIM_0D[]		= "0D";
const char	STRDIM_1D[]		= "1D";
const char	STRDIM_2D[]		= "2D";
const char	STRDIM_3D[]		= "3D";
const char	STRDIM_4D[]		= "4D";



Dimension StrToDim( const MGString& str)
{
	if ( strcmp( str.c_str(), STRDIM_1D) == 0 )
		return DIM_1D;
	else if ( strcmp( str.c_str(), STRDIM_2D) == 0 )
		return DIM_2D;
	else if ( strcmp( str.c_str(), STRDIM_3D) == 0 )
		return DIM_3D;
	else if ( strcmp( str.c_str(), STRDIM_4D) == 0 )
		return DIM_4D;
	else
		return DIM_NONE;
}

MGString DimToStr( const Dimension& dim)
{
	switch (dim)
	{
	case DIM_1D:
		return MGString( STRDIM_1D);
	case DIM_2D:
		return MGString( STRDIM_2D);
	case DIM_3D:
		return MGString( STRDIM_3D);
	case DIM_4D:
		return MGString( STRDIM_4D);
	}

	return MGString( STRDIM_NONE);
}



Dimension GlobDim::mDim = DIM_NONE;


} // end of namespace SpaceGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace Geom = SpaceGeomTools;

