#ifndef __TREELEAF_H__
#define __TREELEAF_H__

#include "libcoregeom/dimension.h"
#include "box.h"

using namespace Geom;



//////////////////////////////////////////////////////////////////////
//  class ChildTab
//////////////////////////////////////////////////////////////////////
template <class DATA, int DIM>
class ChildTab
{
public:
	int	size() const	{ return mask<<1;}

	DATA&						operator() ( const MGInt& i);
	const DATA&					operator() ( const MGInt& i) const;

	ChildTab<DATA,DIM-1>&		operator[] ( const MGInt& i)		{ return mtab[i];}
	const ChildTab<DATA,DIM-1>&	operator[] ( const MGInt& i) const	{ return mtab[i];}

private:
	static	int	mask;
	ChildTab<DATA,DIM-1>	mtab[2];
};
//////////////////////////////////////////////////////////////////////


template <class DATA, int DIM>
int	ChildTab<DATA,DIM>::mask = 1<<(DIM-1);


template <class DATA, int DIM>
inline DATA& ChildTab<DATA,DIM>::operator() ( const MGInt& i)
{
	return mtab[(mask&i)>>(DIM-1)]((~mask)&i);
}

template <class DATA, int DIM>
inline const DATA& ChildTab<DATA,DIM>::operator() ( const MGInt& i) const
{
	return mtab[(mask&i)>>(DIM-1)]((~mask)&i);
}




template <class DATA>
class ChildTab<DATA,DIM_1D>
{
public:
	int	size() const	{ return 2;}

	DATA&		operator() ( const MGInt& i)		{ return mtab[1&i];}
	const DATA&	operator() ( const MGInt& i) const	{ return mtab[1&i];}
	DATA&		operator[] ( const MGInt& i)		{ return mtab[i];}
	const DATA&	operator[] ( const MGInt& i) const	{ return mtab[i];}

private:
	DATA				mtab[2];
};








template <class DATA, Dimension DIM> class Tree;

//////////////////////////////////////////////////////////////////////
//  class Leaf
//////////////////////////////////////////////////////////////////////
template <class DATA, Dimension DIM> 
class Leaf
{
	friend class Tree<DATA,DIM>;
	
protected:
	typedef pair<Vect<DIM>,DATA>	PAIR;


	Leaf();
	Leaf( const Vect<DIM>& vc, Leaf<DATA,DIM> *ptr = NULL);
	~Leaf();


	const DATA&					Data( const MGSize& i) const	{ return mtabData[i].second;}
		  DATA&					rData( const MGSize& i)			{ return mtabData[i].second;}

	const Vect<DIM>&	Pos( const MGSize& i) const		{ return mtabData[i].first;} 
		  Vect<DIM>&	rPos( const MGSize& i)			{ return mtabData[i].first;}

	const Vect<DIM>&	Center() const	{ return mCenter;}


	Box<DIM>	FindBoundBox() const;

	void		ExportTEC( FILE *f);


	Leaf<DATA,DIM>*		DetachFromParent();

	Leaf<DATA,DIM>*		Traverse( const Vect<DIM>& pos);

	void				Insert( const Vect<DIM>& pos, const DATA& dat, Box<DIM> box);
	void				RangeSearch( const Box<DIM>& rec, vector<Leaf<DATA,DIM>*>& lst);
	MGInt				IndClosestData( const Vect<DIM>& vct);
	
	void				Erase( const Vect<DIM>& pos, const DATA& dat);

	Leaf<DATA,DIM>*		Child( const MGInt& i) const; 
	Leaf<DATA,DIM>*&	rChild( const MGInt& i);

	Vect<DIM>&	rCenter() 	{ return mCenter;}

	vector< PAIR >&		rTab()			{ return mtabData;}

	bool				IsEmpty();
	bool				HaveData()		{ return (mtabData.size() > 0);}


	static const MGSize& cSize()		{ return mSize;}
	static		 MGSize& rSize()		{ return mSize;}

private:
	static	MGSize		mSize;					// maximum allowed size of the mtabData; can be changed during runtime
		  
	vector< PAIR >		mtabData;				// used for storage of data connected to point

	Vect<DIM>	mCenter;				// center point of the leaf; bbox is calc. thanks to parent informations
	Leaf<DATA,DIM>		*mpParent;				// pointer to parent leaf; for leafs attached to tree should be always valid

	ChildTab< Leaf<DATA,DIM>*, DIM>	mtabChild;	// table of children: 2 for 1D, 4 for 2D, 8 for 3D
};
//////////////////////////////////////////////////////////////////////

template <class DATA, Dimension DIM> 
MGSize Leaf<DATA,DIM>::mSize;





template <class DATA, Dimension DIM> 
inline Leaf<DATA,DIM>::Leaf()	
{
	for ( MGInt i=0; i<DIM; ++i)
		mCenter.rX(i)=0.0;

	mpParent = NULL;

	//mtabData.reserve( cSize());

	for ( MGInt i=0; i<mtabChild.size(); ++i)
		mtabChild(i) = NULL;
}


template <class DATA, Dimension DIM> 
inline Leaf<DATA,DIM>::Leaf( const Vect<DIM>& vc, Leaf<DATA,DIM> *ptr)	
{
	mCenter = vc;
	mpParent = ptr;

	//mtabData.reserve( cSize());

	for ( MGInt i=0; i<mtabChild.size(); ++i)
		mtabChild(i) = NULL;
}

template <class DATA, Dimension DIM> 
inline Leaf<DATA,DIM>::~Leaf()	
{
	//mtabData.clear();

	for ( MGInt i=0; i<mtabChild.size(); ++i)
		if ( mtabChild(i) )
		{
			delete mtabChild(i);
			mtabChild(i) = NULL;
		}
}



//////////////
template <class DATA, Dimension DIM> 
inline Box<DIM> Leaf<DATA,DIM>::FindBoundBox() const
{
	Vect<DIM>	vdst;
	Box<DIM>			rec;
	
	if ( mpParent)
	{
		vdst = mpParent->Center() - Center();

		for ( MGInt i=0; i<DIM; ++i)
			vdst.rX(i) = ::fabs( vdst.cX(i) );

		rec.rVMax() = Center() + vdst;
		rec.rVMin() = Center() - vdst;
	}

	return rec;
}


template <class DATA, Dimension DIM> 
inline void Leaf<DATA,DIM>::Erase( const Vect<DIM>& pos, const DATA& dat)
{
	bool bFound = false;
	typename vector<PAIR>::iterator itr;

	for ( itr=mtabData.begin(); itr!=mtabData.end(); ++itr)
	{
		//if ( ((*itr).first - pos).module() < ZERO && (*itr).second == dat )
		//if ( ((*itr).first - pos).module() < 1.0e-8 && (*itr).second == dat )
		if ( (*itr).second == dat )
		{
			bFound = true;
			break;
		}
	}

	if ( bFound)
		mtabData.erase( itr);
	else
	{
		TRACE( "corrupted Leaf<DATA,DIM>::Erase(...)");
		THROW_INTERNAL( "corrupted Leaf<DATA,DIM>::Erase(...)");
	}
}



template <class DATA, Dimension DIM> 
inline MGInt Leaf<DATA,DIM>::IndClosestData( const Vect<DIM>& vct)
{	
	if ( ! HaveData() )
		return -1;

	MGSize ind = 0;
	MGFloat dst = (vct - Pos(0)).module();
	MGFloat dmod;
	for ( MGSize k=0; k<mtabData.size(); ++k)
	{
		dmod = (vct - Pos(k)).module();

		if ( dmod < dst )
		{
			ind = k;
			dst = dmod;
		}
	}

	return (MGInt)ind;
}


template <class DATA, Dimension DIM> 
inline Leaf<DATA,DIM>* Leaf<DATA,DIM>::Child( const MGInt& i) const
{
	return mtabChild(i);
}

template <class DATA, Dimension DIM> 
inline Leaf<DATA,DIM>*& Leaf<DATA,DIM>::rChild( const MGInt& i)
{
	return mtabChild(i);
}



template <class DATA, Dimension DIM> 
inline Leaf<DATA,DIM>* Leaf<DATA,DIM>::DetachFromParent()
{
	if ( mpParent != NULL)
	{
		for ( MGInt i=0; i<mtabChild.size(); ++i)
			if ( mpParent->Child( i) == this)
				mpParent->rChild( i) = NULL;
	}

	return mpParent;
}


template <class DATA, Dimension DIM> 
bool Leaf<DATA,DIM>::IsEmpty()
{
	for ( MGInt i=0; i<mtabChild.size(); ++i)
		if ( Child( i) != NULL)
			return false;

	return true;
}


template <class DATA, Dimension DIM> 
Leaf<DATA,DIM>* Leaf<DATA,DIM>::Traverse( const Vect<DIM>& pos)
{
	MGInt		k, ind=0;

	Leaf<DATA,DIM>* plf;
	
	for ( MGInt i=0; i<DIM; ++i)
	{
		ind <<= 1;

		if ( pos.cX(i) > mCenter.cX(i) )
			k = 1;
		else
			k = 0;

		ind += k;
	}

	plf = Child( ind);
	
	if ( plf)
		return plf->Traverse( pos);

	return this;
}


template <class DATA, Dimension DIM> 
void Leaf<DATA,DIM>::Insert( const Vect<DIM>& pos, const DATA& dat, Box<DIM> box)
{
	Box<DIM>		oldbox;
	MGInt			ind;

	Leaf<DATA,DIM>	*plf;


	if ( HaveData() )
	{
		for ( MGSize i=0; i<mtabData.size(); ++i)
			//if ( (pos - Pos(i)).module()  < 1.0e-17 )
			if ( pos.cX() == Pos(i).cX() && pos.cY() == Pos(i).cY() && pos.cZ() == Pos(i).cZ() )
			{
				char	sbuf[1024];
				sprintf( sbuf, "new point (%20.16lg, %20.16lg, %20.16lg) old point (%20.16lg, %20.16lg, %20.16lg)", 
					pos.cX(), pos.cY(), pos.cZ(), Pos(i).cX(), Pos(i).cY(), Pos(i).cZ() );
				printf( "%s\n", sbuf);
				TRACE( sbuf);
				THROW_INTERNAL( "Leaf<DATA>::Insert - trying to insert two points with the same coordinates");
			}
	}

	bool b = true;
	for ( MGInt i=0; i<mtabChild.size(); ++i)
		if ( Child(i) )
		{
			b = false;
			break;
		}

	if ( (mtabData.size() < cSize() ) && b)
	{
		mtabData.push_back( PAIR( pos, dat) );
		return;
	}
	
	oldbox = box;

	ind = 0;
	for ( MGInt i=0; i<DIM; ++i)
	{
		ind <<= 1;

		if ( pos.cX(i) > mCenter.cX(i) )
		{
			box.rVMin().rX(i) = mCenter.cX(i);
			ind += 1;
		}
		else
		{
			box.rVMax().rX(i) = mCenter.cX(i);
		}
	}

	plf = Child( ind);


	if ( plf == NULL)
	{
		plf = new Leaf<DATA,DIM>( box.Center(), this );
		rChild( ind) = plf;
	}

	plf->Insert( pos, dat, box );

	if ( HaveData() )
	{
		for ( MGSize k=0; k<mtabData.size(); ++k)
		{
			box = oldbox;

			ind = 0;
			for ( MGInt i=0; i<DIM; ++i)
			{
				ind <<= 1;

				if ( Pos(k).cX(i) > mCenter.cX(i) )
				{
					box.rVMin().rX(i) = mCenter.cX(i);
					ind += 1;
				}
				else
				{
					box.rVMax().rX(i) = mCenter.cX(i);
				}
			}


			plf = Child( ind);

			if ( plf == NULL)
			{
				plf = new Leaf<DATA,DIM>( box.Center(), this );
				rChild( ind) = plf;
			}

			plf->Insert( Pos(k), Data(k), box );

		}

		mtabData.clear();
	}

}





template <class DATA, Dimension DIM> 
void Leaf<DATA,DIM>::RangeSearch( const Box<DIM>& rec, vector<Leaf<DATA,DIM>*>& lst)
{
	Box<DIM>	bbox;
	
	bbox = FindBoundBox();
	
	if ( rec.IsOverlapping( bbox) )
	{
		if ( HaveData() )
			//lst.insert( lst.end(), this);
			lst.push_back( this);
		else
			for ( MGInt i=0; i<mtabChild.size(); ++i)
				if ( Child( i) )
					Child( i)->RangeSearch( rec, lst);
	}
}






template <class DATA, Dimension DIM> 
void Leaf<DATA,DIM>::ExportTEC( FILE *f)
{
	Box<DIM> 		box;
	
	box = FindBoundBox();
	box.ExportTEC( f);

	for ( MGInt i=0; i<mtabChild.size(); ++i)
		if ( Child(i) )
			Child(i)->ExportTEC( f);

}






#endif // __TREELEAF_H__
