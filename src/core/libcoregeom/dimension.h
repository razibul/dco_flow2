#ifndef __DIMENSION_H__
#define __DIMENSION_H__



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace SpaceGeomTools {



enum Dimension
{
	DIM_NONE	= 0,
	DIM_0D		= 0,
	DIM_1D		= 1,
	DIM_2D		= 2,
	DIM_3D		= 3,
	DIM_4D		= 4
};

extern const char	STRDIM_NONE[];
extern const char	STRDIM_0D[];
extern const char	STRDIM_1D[];
extern const char	STRDIM_2D[];
extern const char	STRDIM_3D[];
extern const char	STRDIM_4D[];


Dimension StrToDim( const MGString& str);
MGString DimToStr( const Dimension& dim);

//////////////////////////////////////////////////////////////////////
// class GlobDim
//////////////////////////////////////////////////////////////////////
class GlobDim
{
public:
	GlobDim()						{ mDim = DIM_NONE;}
	GlobDim( const Dimension& dim)	{ ASSERT( mDim == DIM_NONE); mDim = dim;}
	
	static const Dimension&	Dim()	{ return mDim;}
	static Dimension&		rDim()	{ return mDim;}
	
protected:
	static Dimension	mDim;
};
//////////////////////////////////////////////////////////////////////



} // end of namespace SpaceGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace Geom = SpaceGeomTools;



#endif // __DIMENSION_H__
