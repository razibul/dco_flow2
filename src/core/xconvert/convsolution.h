#ifndef __SOLUTION_H__
#define __SOLUTION_H__

#include "libcorecommon/svector.h"
#include "libcoreio/store.h"
#include "libcoreio/readsol.h"
#include "libcoreio/writesol.h"


//////////////////////////////////////////////////////////////////////
// class ConvSolution
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, MGSize SIZE>
class ConvSolution : public IO::SolFacade
{
public:
	ConvSolution() 		{}

	//const Sparse::BlockVector<ESIZE>&	operator [] ( const  MGSize& i) const	{ return mvecQ[i]; }
	//Sparse::BlockVector<ESIZE>&			operator [] ( const  MGSize& i)			{ return mvecQ[i]; }

	MGSize		Size() const	{ return mtabSol.size(); }

	void		Resize( const MGSize& n)	{ mtabSol.resize( n); }

	//void		CheckNAN() const			{ CheckIsNAN( mvecQ); }


protected:
	//void	CheckIsNAN( const Sparse::Vector<ESIZE>& vec) const;
	//void	CheckQ( const Sparse::Vector<ESIZE>& vec) const;

// ConvSolution Facade
protected:
	virtual Dimension	Dim() const						{ return DIM;}

	virtual void	IOGetName( MGString& name) const	{ name = mSolName;}
	virtual void	IOSetName( const MGString& name)	{ mSolName = name;}

	virtual void	DimToUndim( vector<MGFloat>& tab ) const;//	{ mpPhysics->DimToUndim( tab); }
	virtual void	UndimToDim( vector<MGFloat>& tab ) const;//	{ mpPhysics->UndimToDim( tab); }
 
	virtual MGSize	IOSize() const						{ return Size();}			
	virtual MGSize	IOBlockSize() const					{ return SIZE;}

	virtual void	IOResize( const MGSize& n)			{ Resize( n); }

	virtual void	IOGetBlockVector( vector<MGFloat>& tabx, const MGSize& id) const;
	virtual void	IOSetBlockVector( const vector<MGFloat>& tabx, const MGSize& id );

 
private:
	MGString				mSolName; 

	vector< SVector<SIZE> >	mtabSol;
};
//////////////////////////////////////////////////////////////////////


 
template <Dimension DIM, MGSize SIZE>
inline void ConvSolution<DIM,SIZE>::IOGetBlockVector( vector<MGFloat>& tabx, const MGSize& id) const
{
	if ( tabx.size() != SIZE)
		THROW_INTERNAL( "ConvSolution<SIZE>::IOGetBlockVector - tabx.size() != SIZE");

	for ( MGSize k=0; k<SIZE; ++k)
		tabx[k] = mtabSol[id](k);
}


template <Dimension DIM, MGSize SIZE>
inline void ConvSolution<DIM,SIZE>::IOSetBlockVector( const vector<MGFloat>& tabx, const MGSize& id )
{
	if ( tabx.size() != SIZE)
		THROW_INTERNAL( "ConvSolution<SIZE>::IOSetBlockVector - tabx.size() != SIZE");

	for ( MGSize k=0; k<SIZE; ++k)
		mtabSol[id](k) = tabx[k];
}



template <Dimension DIM, MGSize SIZE>
inline void ConvSolution<DIM,SIZE>::DimToUndim( vector<MGFloat>& tab ) const
{ 
}

template <Dimension DIM, MGSize SIZE>
inline void ConvSolution<DIM,SIZE>::UndimToDim( vector<MGFloat>& tab ) const
{ 
}




#endif // __SOLUTION_H__
