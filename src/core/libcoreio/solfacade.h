#ifndef __IO_SOLFACADE_H__
#define __IO_SOLFACADE_H__

#include "libcoresystem/mgdecl.h"
#include "libcoreio/facade.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace IOSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

//////////////////////////////////////////////////////////////////////
// class SolFacade
//////////////////////////////////////////////////////////////////////
class SolFacade : virtual public WriteFacade, virtual public ReadFacade
{
public:
	virtual MGSize	IOSize() const = 0;
	virtual MGSize	IOBlockSize() const = 0;

	virtual void	IOResize( const MGSize& n) = 0;

	virtual void	IOGetBlockVector( vector<MGFloat>& tabx, const MGSize& id) const = 0;
	virtual void	IOSetBlockVector( const vector<MGFloat>& tabx, const MGSize& id ) = 0;

	virtual void	DimToUndim( vector<MGFloat>& tab ) const = 0;
	virtual void	UndimToDim( vector<MGFloat>& tab ) const = 0;
};


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace IO = IOSpace;

#endif // __IO_SOLFACADE_H__

