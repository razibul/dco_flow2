#ifndef __IO_GRIDFACADE_H__
#define __IO_GRIDFACADE_H__

#include "libcoresystem/mgdecl.h"
#include "libcoreio/facade.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace IOSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

//////////////////////////////////////////////////////////////////////
// class GridWriteFacade
//////////////////////////////////////////////////////////////////////
class GridWriteFacade : virtual public WriteFacade
{
public:
	virtual MGSize	IOSizeNodeTab() const = 0;
	virtual MGSize	IOSizeCellTab( const MGSize& type) const = 0;
	virtual MGSize	IOSizeBFaceTab( const MGSize& type) const = 0;

	virtual void	IOGetNodePos( vector<MGFloat>& tabx, const MGSize& id) const = 0;
	virtual void	IOGetCellIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const = 0;
	virtual void	IOGetBFaceIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const = 0;
	virtual void	IOGetBFaceSurf( MGSize& idsurf, const MGSize& type, const MGSize& id) const = 0;

	virtual MGSize	IONumCellTypes() const = 0;
	virtual MGSize	IONumBFaceTypes() const = 0;
	virtual MGSize	IOCellType( const MGSize& i) const = 0;
	virtual MGSize	IOBFaceType( const MGSize& i) const = 0;
};


//////////////////////////////////////////////////////////////////////
// class GridReadFacade
//////////////////////////////////////////////////////////////////////
class GridReadFacade : virtual public ReadFacade
{
public:
	virtual void	IOTabNodeResize( const MGSize& n) = 0;
	virtual void	IOTabCellResize( const MGSize& type, const MGSize& n) = 0;
	virtual void	IOTabBFaceResize( const MGSize& type, const MGSize& n) = 0;

	virtual void	IOSetNodePos( const vector<MGFloat>& tabx, const MGSize& id ) = 0;
	virtual void	IOSetCellIds( const vector<MGSize>& tabid, const MGSize& type, const MGSize& id ) = 0;
	virtual void	IOSetBFaceIds( const vector<MGSize>& tabid, const MGSize& type, const MGSize& id ) = 0;
	virtual void	IOSetBFaceSurf( const MGSize& idsurf, const MGSize& type, const MGSize& id ) = 0;


};


//////////////////////////////////////////////////////////////////////
// class GridBndFacade
//////////////////////////////////////////////////////////////////////
class GridBndFacade : virtual public GridWriteFacade
{
public:
	virtual MGSize	IOSizeBNodeTab() const = 0;

	virtual void	IOGetBNodePos( vector<MGFloat>& tabx, const MGSize& id) const = 0;
	virtual void	IOGetBNodeVn( vector<MGFloat>& tabx, const MGSize& id) const = 0;
	virtual void	IOGetBNodePId( MGSize& idparent, const MGSize& id) const = 0;
	virtual void	IOGetBFaceBIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const = 0;

};


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace IO = IOSpace;


#endif // __IO_GRIDFACADE_H__

