
#include "writemsh2.h"
#include "libcoreio/store.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace IOSpace {


MGString WriteMSH2::mstVersionString = "MSH_VER";
MGString WriteMSH2::mstVersionNumber = "2.0";


template <Dimension DIM>
void WriteMSH2::WriteMSHascii( ostream& file)
{
	MGString	st;
	MGSize		n, id;

	file << VerStr() << " " << VerNum() << endl;
	st = ::DimToStr( DIM);
	file << st << endl;

	mGrd.IOGetName( st);
	file << st <<endl;

	// nodes
	vector<MGFloat>	tabx(DIM);

	n = mGrd.IOSizeNodeTab();
	file << n << endl;

	for ( MGSize i=0; i<n; ++i)
	{
		mGrd.IOGetNodePos( tabx, i);
	
		for ( MGInt j=0; j<DIM; ++j)
			file << setw(25) << setprecision(20) << tabx[j] << " ";

		file << endl;
	}

	// cells
	MGSize	nb, ntyp;
	vector<MGSize> tabid;

	nb = mGrd.IONumCellTypes();
	file << nb << endl;

	for ( MGSize k=0; k<nb; ++k)
	{
		ntyp = mGrd.IOCellType( k);
		n = mGrd.IOSizeCellTab( ntyp);
		tabid.resize( ntyp);

		file << ntyp << " " << n << endl;

		for ( MGSize i=0; i<n; ++i)
		{
			mGrd.IOGetCellIds( tabid, ntyp, i);

			for ( MGSize j=0; j<ntyp; ++j)
				file << tabid[j]+1 << " ";
			file << endl;
		}
	}

	// bfaces
	nb = mGrd.IONumBFaceTypes();
	file << nb << endl;

	for ( MGSize k=0; k<nb; ++k)
	{
		ntyp = mGrd.IOBFaceType( k);
		n = mGrd.IOSizeBFaceTab( ntyp);
		tabid.resize( ntyp);

		file << ntyp << " " << n << endl;

		for ( MGSize i=0; i<n; ++i)
		{
			mGrd.IOGetBFaceIds( tabid, ntyp, i);

			mGrd.IOGetBFaceSurf( id, ntyp, i );
			file << id << "  ";

			//if ( id == 3 || id == 4)
			//{
			//	for ( MGSize j=0; j<ntyp; ++j)
			//		file << tabid[ntyp-1-j]+1 << " ";
			//	file << 0 << endl;
			//}
			//else
			//{
			//	for ( MGSize j=0; j<ntyp; ++j)
			//		file << tabid[j]+1 << " ";
			//	file << 0 << endl;
			//}

			for ( MGSize j=0; j<ntyp; ++j)
				file << tabid[j]+1 << " ";
			file << 0 << endl;
		}
	}
}

template <Dimension DIM>
void WriteMSH2::WriteMSHbin( ostream& file)
{
	MGSize	n, id;
	Dimension	dim;
	MGString	str;

	n = VerStr().size();
	file.write( (char*)&n, sizeof(MGSize) );
	file.write( VerStr().c_str(), n );
	
	n = VerNum().size();
	file.write( (char*)&n, sizeof(MGSize) );
	file.write( VerNum().c_str(), n );
	
	dim = DIM;
	file.write( (char*)&dim, sizeof(Dimension) );

	mGrd.IOGetName( str);
	n = str.size();
	file.write( (char*)&n, sizeof(MGSize) );
	file.write( str.c_str(), n );
	
	// nodes
	vector<MGFloat>	tabx(DIM);

	n = mGrd.IOSizeNodeTab();
	file.write( (char*)&n, sizeof(MGSize) );
	for ( MGSize i=0; i<n; ++i)
	{
		mGrd.IOGetNodePos( tabx, i);
	
		file.write( (char*)&tabx[0], DIM*sizeof(MGFloat) );
	}


	// cells
	MGSize	nb, ntyp;
	vector<MGSize> tabid;

	nb = mGrd.IONumCellTypes();
	file.write( (char*)&nb, sizeof(MGSize) );

	for ( MGSize k=0; k<nb; ++k)
	{
		ntyp = mGrd.IOCellType( k);
		n = mGrd.IOSizeCellTab( ntyp);

		tabid.resize(ntyp);

		file.write( (char*)&ntyp, sizeof(MGSize) );
		file.write( (char*)&n, sizeof(MGSize) );

		for ( MGSize i=0; i<n; ++i)
		{
			mGrd.IOGetCellIds( tabid, ntyp, i);

			file.write( (char*)&tabid[0], ntyp*sizeof(MGSize) );
		}
	}

	// bfaces
	nb = mGrd.IONumBFaceTypes();
	file.write( (char*)&nb, sizeof(MGSize) );

	for ( MGSize k=0; k<nb; ++k)
	{
		ntyp = mGrd.IOBFaceType( k);
		n = mGrd.IOSizeBFaceTab( ntyp);

		tabid.resize(ntyp);

		file.write( (char*)&ntyp, sizeof(MGSize) );
		file.write( (char*)&n, sizeof(MGSize) );

		for ( MGSize i=0; i<n; ++i)
		{
			mGrd.IOGetBFaceIds( tabid, ntyp, i);
			mGrd.IOGetBFaceSurf( id, ntyp, i );

			file.write( (char*)&id, sizeof(MGSize) );
			file.write( (char*)&tabid[0], ntyp*sizeof(MGSize) );
		}
	}

}

void WriteMSH2::DoWrite( ostream& file, const bool& bin)
{
	if ( mGrd.Dim() == DIM_2D )
	{
		if ( bin)
			WriteMSHbin<DIM_2D>( file);
		else
			WriteMSHascii<DIM_2D>( file);
	}
	else if ( mGrd.Dim() == DIM_3D )
	{
		if ( bin)
			WriteMSHbin<DIM_3D>( file);
		else
			WriteMSHascii<DIM_3D>( file);
	}
	else
		THROW_INTERNAL( "WriteMSH2::DoWrite - bad dim");
}



void WriteMSH2::DoWrite( const MGString& fname, const bool& bin)
{
	ofstream ofile;

	if ( bin)
		ofile.open( fname.c_str(), ios::out | ios::binary);
	else
		ofile.open( fname.c_str(), ios::out );

	try
	{
		if ( ! ofile)
			THROW_FILE( "can not open the file", fname);

		DoWrite( ofile, bin);
	}
	catch ( EHandler::Except& e)
	{
		cout << "ERROR: WriteMSH2::DoWrite - file name: " << fname << endl << endl;
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);

		THROW_INTERNAL("END");
	}
}



} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
