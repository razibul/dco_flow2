
#include "writeintf.h"
#include "libcoreio/store.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace IOSpace {


MGString WriteINTF::mstVersionString = "INTF_VER";
MGString WriteINTF::mstVersionNumber = "0.1";




template <Dimension DIM> 
inline void WriteINTF::WriteINTFascii( ostream& file )
{
	MGString	st;
	MGSize	n, neq;

	file << VerStr() << " " << VerNum() << endl;	// file version
	st = ::DimToStr( DIM);
	file << st << endl;			// dimension


	mIntf.IOGetName( st);
	//st="-----";
	file << st << endl;			// description

	neq=mIntf.IOGetProcNum();
	file << neq << endl;

	n=mIntf.IOTabIntfSize();
	file << n << endl;			// size of the solution vector


	vector<MGSize>	tabid(3);

	for ( MGSize i=0; i<n; ++i)
	{
		mIntf.IOGetIds( tabid, i);

		for ( MGSize k=0; k<3; ++k)
			file << " "  << tabid[k]+1;
		file << endl;
	}

}


template <Dimension DIM> 
inline void WriteINTF::WriteINTFbin( ostream& file)
{
	THROW_INTERNAL( "WriteINTF::WriteINTFbin - not implemented");
/*	MGSize	n, neq;
	Dimension	dim;
	MGString	str;

	n = VerStr().size();
	file.write( (char*)&n, sizeof(MGSize) );
	file.write( VerStr().c_str(), n );
	
	n = VerNum().size();
	file.write( (char*)&n, sizeof(MGSize) );
	file.write( VerNum().c_str(), n );
	
	dim = DIM;
	file.write( (char*)&dim, sizeof(Dimension) );

	mIntf.IOGetName( str);
	n = str.size();
	file.write( (char*)&n, sizeof(MGSize) );
	file.write( str.c_str(), n );


	neq=mIntf.IOBlockSize();
	file.write( (char*)&neq, sizeof(MGSize) );

	n=mIntf.IOSize();
	file.write( (char*)&n, sizeof(MGSize) );

	vector<MGFloat>	tabx(neq);

	for ( MGSize i=0; i<n; ++i)
	{
		mIntf.IOGetBlockVector( tabx, i);

		mIntf.UndimToDim( tabx );

		file.write( (char*)&tabx[0], neq*sizeof(MGFloat) );
	}
*/
}

void WriteINTF::DoWrite( ostream& file, const bool& bin )
{
	if ( mIntf.Dim() == DIM_2D )
	{
		if ( bin)
			WriteINTFbin<DIM_2D>( file);
		else
			WriteINTFascii<DIM_2D>( file);
	}
	else if ( mIntf.Dim() == DIM_3D )
	{
		if ( bin)
			WriteINTFbin<DIM_3D>( file);
		else
			WriteINTFascii<DIM_3D>( file);
	}
	else
		THROW_INTERNAL( "WriteINTF::DoWrite - bad dim");

}


void WriteINTF::DoWrite( const MGString& fname, const bool& bin)
{
	ofstream ofile;

	if ( bin)
		ofile.open( fname.c_str(), ios::out | ios::binary);
	else
		ofile.open( fname.c_str(), ios::out );

	try
	{
		if ( ! ofile)
			THROW_FILE( "can not open the file", fname);

		DoWrite( ofile, bin);
	}
	catch ( EHandler::Except& e)
	{
		cout << "ERROR: WriteINTF::DoWrite - file name: " << fname << endl << endl;
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);

		THROW_INTERNAL("END");
	}
}




} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

