#ifndef __IO_WRITEMSH2_H__
#define __IO_WRITEMSH2_H__

#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"

#include "libcoreio/gridfacade.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace IOSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


//////////////////////////////////////////////////////////////////////
// class WriteMSH2
//////////////////////////////////////////////////////////////////////
class WriteMSH2
{
public:
	WriteMSH2( GridWriteFacade& grd, MGString title = "") : mGrd(grd), mTitle(title)	{}


	void	DoWrite( const MGString& fname, const bool& bin);
	void	DoWrite( ostream& file, const bool& bin);

protected:
	template <Dimension DIM> void	WriteMSHascii( ostream& file);
	template <Dimension DIM> void	WriteMSHbin( ostream& file);

	static const MGString& VerNum()	{ return mstVersionNumber;}
	static const MGString& VerStr()	{ return mstVersionString;}

private:
	static MGString	mstVersionString;
	static MGString	mstVersionNumber;

	GridWriteFacade&		mGrd;

	MGString		mTitle;
};



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace IO = IOSpace;



#endif // __IO_WRITEMSH2_H__

