#ifndef __IO_WRITESOL_H__
#define __IO_WRITESOL_H__


#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"

#include "libcoreio/solfacade.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace IOSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//





//////////////////////////////////////////////////////////////////////
// class WriteSOL
//////////////////////////////////////////////////////////////////////
//template <class VECT>
class WriteSOL
{
public:
	WriteSOL( const SolFacade& sol, MGString title = "") : mSol(sol), mTitle(title)	{}


	void	DoWrite( const MGString& fname, const bool& bin );
	void	DoWrite( ostream& file, const bool& bin );

protected:
	template <Dimension DIM> void	WriteSOLascii( ostream& file);
	template <Dimension DIM> void	WriteSOLbin( ostream& file);

	static const MGString& VerNum()	{ return mstVersionNumber;}
	static const MGString& VerStr()	{ return mstVersionString;}

private:
	static MGString	mstVersionString;
	static MGString	mstVersionNumber;

	const SolFacade&		mSol;

	MGString		mTitle;

};


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace IO = IOSpace;


#endif // __IO_WRITESOL_H__

