#ifndef __IO_READVGRID_H__
#define __IO_READVGRID_H__

#include "libcoresystem/mgdecl.h"
#include "libcorecommon/key.h"
#include "libcoregeom/dimension.h"
#include "libcoregeom/vect.h"

#include "libcoreio/gridfacade.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace IOSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


//////////////////////////////////////////////////////////////////////
// class ReadVGRID
//////////////////////////////////////////////////////////////////////
class ReadVGRID
{
public:
	ReadVGRID( GridReadFacade& grd) : mGrd(grd)	{}

	void	DoRead( const MGString& fname);
	void	WriteTEC( const MGString& name);

protected:
	void	ReadCOGSG( istream& file);
	void	ReadCOGSG_multi( istream& file);
	void	ReadBC( istream& file);
	void	CopyToGrid();

private:
	GridReadFacade&		mGrd;

	MGString			mFileTitle;

	vector< Vect<DIM_3D> >				mtabNode;
	vector< Key<4> >					mtabCell;
	vector< pair< MGSize, Key<3> > >	mtabBnd;
};




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace IO = IOSpace;


#endif // __IO_READVGRID_H__
