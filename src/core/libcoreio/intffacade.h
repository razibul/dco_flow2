#ifndef __INTFFACADE_H__
#define __INTFFACADE_H__

#include "libcoresystem/mgdecl.h"
#include "libcoreio/facade.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace IOSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

//////////////////////////////////////////////////////////////////////
// class IntfFacade
//////////////////////////////////////////////////////////////////////
class IntfFacade : virtual public WriteFacade, virtual public ReadFacade
{
public:
	virtual MGSize	IOGetProcNum() const = 0;
	virtual void	IOSetProcNum( const MGSize& n) = 0;

	virtual MGSize	IOTabIntfSize() const = 0;
	virtual void	IOTabIntfResize( const MGSize& n) = 0;

	virtual void	IOGetIds( vector<MGSize>& tabid, const MGSize& id) const = 0;
	virtual void	IOSetIds( const vector<MGSize>& tabid, const MGSize& id ) = 0;
};
//////////////////////////////////////////////////////////////////////


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace IO = IOSpace;


#endif // __INTFFACADE_H__


