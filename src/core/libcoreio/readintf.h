#ifndef __IO_READINTF_H__
#define __IO_READINTF_H__

#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"

#include "libcoreio/intffacade.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace IOSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


//////////////////////////////////////////////////////////////////////
// class ReadINTF
//////////////////////////////////////////////////////////////////////
class ReadINTF
{
public:
	ReadINTF( IntfFacade& intf, MGString title = "") : mIntf(intf), mTitle(title)	{}

	void	DoRead( const MGString& fname, const bool& bin );
	void	DoRead( istream& file, const bool& bin );

protected:
	template <Dimension DIM> void	ReadINTFascii( istream& file);
	template <Dimension DIM> void	ReadINTFbin( istream& file);

	static const MGString& VerNum()	{ return mstVersionNumber;}
	static const MGString& VerStr()	{ return mstVersionString;}


private:
	static MGString	mstVersionString;
	static MGString	mstVersionNumber;

	IntfFacade&		mIntf;

	MGString		mTitle;
};
//////////////////////////////////////////////////////////////////////



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace IO = IOSpace;


#endif // __IO_READINTF_H__

