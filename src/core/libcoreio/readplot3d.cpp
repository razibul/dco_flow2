#include "libcoreio/readplot3d.h"
#include "libcoreio/store.h"
#include "libcorecommon/regexp.h"
#include "libcorecommon/progressbar.h"
#include "libcoregeom/tree.h"

#include "libcoregeom/simplex.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace IOSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

template<>
MGSize ReadPlot3D<DIM_2D>::NodeId(const MGSize x, const MGSize y, const MGSize z, const MGSize& blockid)
{
	return 0;
}
template<>
MGSize ReadPlot3D<DIM_3D>::NodeId(const MGSize x, const MGSize y, const MGSize z, const MGSize& blockid)
{
	return z* mtabBlockN[blockid].cX()*mtabBlockN[blockid].cY() +y * mtabBlockN[blockid].cX() + x;
}


void ConvertPrism2Tetra(MGSize prism[6], MGSize tabtetra[3][4])
{
	if ((prism[0] > prism[1]) && (prism[0] > prism[2]))
	{
		tabtetra[0][0] = prism[0];
		tabtetra[0][1] = prism[4];
		tabtetra[0][2] = prism[5];
		tabtetra[0][3] = prism[3];

		if (prism[1] > prism[2])
		{
			tabtetra[1][0] = prism[0];
			tabtetra[1][1] = prism[1];
			tabtetra[1][2] = prism[2];
			tabtetra[1][3] = prism[5];

			tabtetra[2][0] = prism[0];
			tabtetra[2][1] = prism[1];
			tabtetra[2][2] = prism[5];
			tabtetra[2][3] = prism[4];
		}
		else
		{
			tabtetra[1][0] = prism[0];
			tabtetra[1][1] = prism[1];
			tabtetra[1][2] = prism[2];
			tabtetra[1][3] = prism[4];

			tabtetra[2][0] = prism[0];
			tabtetra[2][1] = prism[4];
			tabtetra[2][2] = prism[2];
			tabtetra[2][3] = prism[5];
		}
	}
	else if ((prism[2] > prism[0]) && (prism[2] > prism[1]))
	{
		tabtetra[0][0] = prism[2];
		tabtetra[0][1] = prism[3];
		tabtetra[0][2] = prism[4];
		tabtetra[0][3] = prism[5];

		if (prism[0] > prism[1])
		{
			tabtetra[1][0] = prism[0];
			tabtetra[1][1] = prism[1];
			tabtetra[1][2] = prism[2];
			tabtetra[1][3] = prism[4];

			tabtetra[2][0] = prism[0];
			tabtetra[2][1] = prism[2];
			tabtetra[2][2] = prism[3];
			tabtetra[2][3] = prism[4];
		}
		else
		{
			tabtetra[1][0] = prism[0];
			tabtetra[1][1] = prism[1];
			tabtetra[1][2] = prism[2];
			tabtetra[1][3] = prism[3];

			tabtetra[2][0] = prism[2];
			tabtetra[2][1] = prism[3];
			tabtetra[2][2] = prism[1];
			tabtetra[2][3] = prism[4];		
		}
	}
	else if ((prism[1] > prism[0]) && (prism[1] > prism[2]))
	{
		tabtetra[0][0] = prism[1];
		tabtetra[0][1] = prism[4];
		tabtetra[0][2] = prism[5];
		tabtetra[0][3] = prism[3];
		
		if (prism[0] > prism[2])
		{
			tabtetra[1][0] = prism[0];
			tabtetra[1][1] = prism[1];
			tabtetra[1][2] = prism[5];
			tabtetra[1][3] = prism[3];

			tabtetra[2][0] = prism[0];
			tabtetra[2][1] = prism[1];
			tabtetra[2][2] = prism[2];
			tabtetra[2][3] = prism[5];
		}
		else
		{
			tabtetra[1][0] = prism[0];
			tabtetra[1][1] = prism[1];
			tabtetra[1][2] = prism[2];
			tabtetra[1][3] = prism[3];

			tabtetra[2][0] = prism[2];
			tabtetra[2][1] = prism[3];
			tabtetra[2][2] = prism[1];
			tabtetra[2][3] = prism[5];
		}
	}
}

template<>
void ReadPlot3D<DIM_2D>::ExtractBoundary()
{
	cout << endl;
	cout << "ExtractBoundary" << endl;

	vector< Key<2> > tabface;
	vector< Key<2> >::iterator itr, itre;

	tabface.reserve( 2 * mtabCell.size() );


	for ( int i=0; i<mtabCell.size(); ++i)
	{
		Key<3>& cell = mtabCell[i];
		Key<2> key;

		key = Key<2>( cell.cElem(0), cell.cElem(1) );	
		key.Sort();
		tabface.push_back( key );

		key = Key<2>( cell.cElem(1), cell.cElem(2) );	
		key.Sort();
		tabface.push_back( key );

		key = Key<2>( cell.cElem(2), cell.cElem(0) );	
		key.Sort();
		tabface.push_back( key );

	}

	//cout << " before = " << tabedge.size() << endl;
	sort( tabface.begin(), tabface.end() );

	vector< Key<2> > tabbnd;
	for ( itr=tabface.begin(); itr!=tabface.end(); ++itr)
	{
		vector< Key<2> >::iterator itrnext = itr+1;

		if ( itrnext!=tabface.end() )
		{
			if ( *itr != *itrnext)
				tabbnd.push_back( *itr);
			else
				itr = itrnext;
		}
		else
			tabbnd.push_back( *itr);
	}

	cout << " tabbnd = " << tabbnd.size() << endl;

	// orient
	sort( tabbnd.begin(), tabbnd.end() );

	vector<bool> taborient( tabbnd.size(), true);
	for ( int i=0; i<mtabCell.size(); ++i)
	{
		Key<3>& cell = mtabCell[i];
		Key<2> key, keysort;
		pair< vector< Key<2> >::iterator, vector< Key<2> >::iterator > result;

		keysort = key = Key<2>( cell.cElem(0), cell.cElem(1) );
		keysort.Sort();
		if ( key != keysort )
		{
			result = equal_range( tabbnd.begin(), tabbnd.end(), keysort );
			if ( *result.first == keysort)
				taborient[ result.first - tabbnd.begin() ] = false;
		}


		keysort = key = Key<2>( cell.cElem(1), cell.cElem(2) );	
		keysort.Sort();
		if ( key != keysort )
		{
			result = equal_range( tabbnd.begin(), tabbnd.end(), keysort );
			if ( *result.first == keysort)
				taborient[ result.first - tabbnd.begin() ] = false;
		}


		keysort = key = Key<2>( cell.cElem(2), cell.cElem(0) );	
		keysort.Sort();
		if ( key != keysort )
		{
			result = equal_range( tabbnd.begin(), tabbnd.end(), keysort );
			if ( *result.first == keysort)
				taborient[ result.first - tabbnd.begin() ] = false;
		}

	}

	//itre = unique( tabbnd.begin(), tabbnd.end() );
	//cout << " tabbnd unique = " << itre - tabbnd.begin() << endl;


	// NACA 0012
	mtabBnd.resize( 2 );		// 0 - viscous; 1 - farfield

	Vect<DIM_2D> vcglob( 0.5, 0.);

	for ( int i=0; i<tabbnd.size(); ++i)
	{
		Key<2>& cell = tabbnd[i];

		if ( taborient[i] )
			cell.Reverse();

		Vect<DIM_2D> vc = static_cast<MGFloat>(0.5 )* ( mtabNode[cell.cFirst()] + mtabNode[cell.cSecond()]  );

		if ( (vc - vcglob).module() < 1.0)
			mtabBnd[0].push_back( cell);
		else
			mtabBnd[1].push_back( cell);
	}


	//// flatplate
	//mtabBnd.resize( 5 );		// 0 - viscous; 1 - farfield

	//MGFloat x0 = 1.0e-6;
	//MGFloat xLeft = -0.332;
	//MGFloat xRight = 1.995;
	//MGFloat yTop = 0.99;
	//MGFloat yBottom = 1.0e-7;

	//Vect<DIM_2D> vcglob( 0.5, 0.);

	//for ( int i=0; i<tabbnd.size(); ++i)
	//{
	//	Key<2>& cell = tabbnd[i];

	//	if ( taborient[i] )
	//		cell.Reverse();

	//	Vect<DIM_2D> v1 = mtabNode[cell.cFirst()];
	//	Vect<DIM_2D> v2 = mtabNode[cell.cSecond()];

	//	if ( v1.cX() < xLeft && v2.cX() < xLeft )
	//		mtabBnd[0].push_back( cell);
	//	else
	//	if ( v1.cY() > yTop && v2.cY() > yTop )
	//		mtabBnd[1].push_back( cell);
	//	else
	//	if ( v1.cX() > xRight && v2.cX() > xRight )
	//		mtabBnd[2].push_back( cell);
	//	else
	//	if ( v1.cY() < yBottom && v2.cY() < yBottom && v1.cX() > -x0 && v2.cX() > -x0 )
	//		mtabBnd[3].push_back( cell);
	//	else
	//	if ( v1.cY() < yBottom && v2.cY() < yBottom && v1.cX() < x0 && v2.cX() < x0 )
	//		mtabBnd[4].push_back( cell);
	//	else
	//		THROW_INTERNAL( "BC CRASH");

	//}




	//////////////////////
	string name = "out_surf.dat";
	cout << "Write surf TEC - " << name << endl;

	ofstream of( name.c_str());

	for ( int u=0; u<mtabBnd.size(); ++u)
	{
		of << "VARIABLES = \"X\", \"Y\"" << endl;
		of << "ZONE T=\"grid\", N=" << mtabNode.size() << ", E=" << mtabBnd[u].size() << ", F=FEPOINT, ET=LINESEG" << endl;

		for ( int i=0; i<mtabNode.size(); ++i)
		{
			Vect2D& v = mtabNode[i];
			of << setprecision( 16) << v.cX() << " " << v.cY() << endl;
		}

		for ( int i=0; i<mtabBnd[u].size(); ++i)
		{
			of << mtabBnd[u][i].cFirst()+1 << " " << mtabBnd[u][i].cSecond()+1  << endl;
		}
	}

}

template<>
void ReadPlot3D<DIM_3D>::ExtractBoundary()
{

}

template<>
void ReadPlot3D<DIM_2D>::ExtractBoundary(vector< vector<MGSize> >& tabsurfnode)
{

}

template<>
void ReadPlot3D<DIM_3D>::ExtractBoundary(vector< vector<MGSize> >& tabsurfnode)
{
	cout << endl;
	cout << "ExtractBoundary" << endl;

	vector< Key<3> > tabface;
	vector< Key<3> >::iterator itr, itre;

	tabface.reserve(4 * mtabCell.size());


	for (int i = 0; i<mtabCell.size(); ++i)
	{
		Key<4>& cell = mtabCell[i];
		Key<3> key;

		key = Key<3>(cell.cElem(0), cell.cElem(1), cell.cElem(2));
		key.Sort();
		tabface.push_back(key);

		key = Key<3>(cell.cElem(0), cell.cElem(1), cell.cElem(3));
		key.Sort();
		tabface.push_back(key);

		key = Key<3>(cell.cElem(0), cell.cElem(3), cell.cElem(2));
		key.Sort();
		tabface.push_back(key);

		key = Key<3>(cell.cElem(1), cell.cElem(2), cell.cElem(3));
		key.Sort();
		tabface.push_back(key);
	}

	//cout << " before = " << tabedge.size() << endl;
	sort(tabface.begin(), tabface.end());

	vector< Key<3> > tabbnd;
	for (itr = tabface.begin(); itr != tabface.end(); ++itr)
	{
		vector< Key<3> >::iterator itrnext = itr + 1;

		if (itrnext != tabface.end())
		{
			if (*itr != *itrnext)
				tabbnd.push_back(*itr);
			else
				itr = itrnext;
		}
		else
			tabbnd.push_back(*itr);
	}

	cout << " tabbnd = " << tabbnd.size() << endl;

	map<MGSize, MGSize>	mapvalidsurfid;
	map<MGSize, MGSize>::iterator itmap;
	MGSize id = 0;

	//mtabBnd.resize(tabsurfnode.size());
	for (MGSize i = 0; i < tabbnd.size(); ++i)
	{
		for (MGSize j = 0; j < tabsurfnode.size(); ++j)
		{
			bool isonsurf = true;
			for (MGSize k = 0; k < 3; ++k)
			{
				vector<MGSize>::iterator it = lower_bound(tabsurfnode[j].begin(), tabsurfnode[j].end(), tabbnd[i].cElem(k));
				if ((it == tabsurfnode[j].end()) || ((*it) != tabbnd[i].cElem(k)))
				{
					isonsurf = false;
					break;
				}
			}
			if (isonsurf)
			{
				itmap = mapvalidsurfid.find(j);

				if (itmap != mapvalidsurfid.end())
				{
					mtabBnd[itmap->second].push_back(tabbnd[i]);
				}
				else
				{
					mapvalidsurfid[j] = id++;
					//Key<3> tmp = tabbnd[i];
					mtabBnd.push_back(vector<Key<3> >());
					mtabBnd.back().push_back(Key<3>(tabbnd[i]));
				}
				
			}
		}
	}

}
template<>
void ReadPlot3D<DIM_2D>::Stitch()
{
	// find TE
	MGSize ite;
	for ( MGSize i=0; i<mtabBlockN[0].cX(); ++i)
	{
		Vect<DIM_2D> vt = mtabNode[i];
		Vect<DIM_2D> vb = mtabNode[ mtabBlockN[0].cX()-1 - i];

		if ( (vb - vt).module() > 1.0e-14 )
		{
			ite = i;
			break;
		}
	}

	cout << "ite = " << ite << endl;

	mtabNode.erase( mtabNode.begin(), mtabNode.begin() + ite );

	for ( MGSize i=0; i<mtabCell.size(); ++i)
		for ( MGSize in=0; in<3; ++in)
			if ( mtabCell[i].cElem(in) >= ite )
				mtabCell[i].rElem(in) -= ite;
			else
				mtabCell[i].rElem(in) = mtabBlockN[0].cX() - 1 - mtabCell[i].cElem(in) - ite;

	// boundary

}

template<>
void ReadPlot3D<DIM_3D>::Stitch()
{

}

template<Dimension DIM>
void ReadPlot3D<DIM>::RemoveDuplicatePoints( const MGFloat& zero)
{
	cout << endl;
	cout << "RemoveDuplicateNodes" << endl;

	multimap< Geom::Vect<DIM>, int>	nodmap;

	for ( int i=0; i<mtabNode.size(); ++i)
        nodmap.insert(typename multimap< Geom::Vect<DIM>, int>::value_type(mtabNode[i], i));

	cout << "mtabNode.size() = " << mtabNode.size() << endl;
	cout << "nodmap.size() = " << nodmap.size() << endl;

    typename multimap< Geom::Vect<DIM>, int>::iterator	itr, itrold;

	map<int,int>	idmap;
	vector< Geom::Vect<DIM> >	tabnn;

	int id = 0;
	itrold = itr = nodmap.begin();

	tabnn.push_back( itr->first);
	idmap.insert( map<int,int>::value_type( itr->second, id ) );
		
	itr->second = id;

	auto comp = [](const Geom::Vect<DIM>& c1, const Geom::Vect<DIM>& c2)->bool
	{
		if (fabs(c1.cX() - c2.cX()) < 1e-8 &&
			fabs(c1.cY() - c2.cY()) < 1e-8 &&
			fabs(c1.cZ() - c2.cZ()) < 1e-8)

			return true;
		else
			return false;
	};

	for ( ++itr; itr!=nodmap.end(); ++itr, ++itrold)
	{
		//if ( ! ( itrold->first == itr->first ) )
		if (!comp(itrold->first,itr->first))
		{
			tabnn.push_back( itr->first);
			++id;
		}

		idmap.insert( map<int,int>::value_type( itr->second, id ) );
		itr->second = id;
	}

	cout << id << " " << tabnn.size() << endl;

	for ( int i=0; i<mtabCell.size(); ++i)
	{
		Key<DIM+1>& cell = mtabCell[i];

		for (MGSize j = 0; j <= DIM; ++j)
			cell.rElem(j) = idmap[cell.cElem(j)];
		//cell.rFirst()  = idmap[cell.cFirst()];
		//cell.rSecond() = idmap[cell.cSecond()];
		//cell.rThird()  = idmap[cell.cThird()];
	}

	mtabNode.swap( tabnn);
	cout << mtabNode.size() << endl;
}

template<>
void ReadPlot3D<DIM_2D>::RemoveDuplicatePointsStr(vector< vector<MGSize> >& tabsurfnode)
{

}

template<>
void ReadPlot3D<DIM_3D>::RemoveDuplicatePointsStr(vector< vector<MGSize> >& tabsurfnode)
{
	cout << endl;
	cout << "RemoveDuplicateNodesStr" << endl;

	multimap< Geom::Vect<DIM_3D>, int>	nodmap;

	for (int i = 0; i<mtabNode.size(); ++i)
		nodmap.insert(multimap< Geom::Vect<DIM_3D>, int>::value_type(mtabNode[i], i));

	cout << "mtabNode.size() = " << mtabNode.size() << endl;
	cout << "nodmap.size() = " << nodmap.size() << endl;

	multimap< Geom::Vect<DIM_3D>, int>::iterator	itr, itrold;

	map<int, int>	idmap;
	vector< Geom::Vect<DIM_3D> >	tabnn;
	//vector< vector<MGSize> >		newtabsurfnode(tabsurfnode.size());

	int id = 0;
	itrold = itr = nodmap.begin();

	tabnn.push_back(itr->first);
	idmap.insert(map<int, int>::value_type(itr->second, id));

	itr->second = id;

	auto comp = [](const Geom::Vect<DIM_3D>& c1, const Geom::Vect<DIM_3D>& c2)->bool
	{
		if (fabs(c1.cX() - c2.cX()) < 1e-7 &&
			fabs(c1.cY() - c2.cY()) < 1e-7 &&
			fabs(c1.cZ() - c2.cZ()) < 1e-7)

			return true;
		else
			return false;
	};

	for (++itr; itr != nodmap.end(); ++itr, ++itrold)
	{
		//if ( ! ( itrold->first == itr->first ) )
		if (!comp(itrold->first, itr->first))
		{
			tabnn.push_back(itr->first);

		//	newtabsurfid.push_back(tabsurfid[itr->second]);
			++id;
		}

		idmap.insert(map<int, int>::value_type(itr->second, id));
		itr->second = id;
	}

	cout << id << " " << tabnn.size() << endl;

	for (int i = 0; i<mtabstrcell.size(); ++i)
	{
		Key<6>& cell = mtabstrcell[i];

		for (MGSize j = 0; j < 6; ++j)
			cell.rElem(j) = idmap[cell.cElem(j)];
		//cell.rFirst()  = idmap[cell.cFirst()];
		//cell.rSecond() = idmap[cell.cSecond()];
		//cell.rThird()  = idmap[cell.cThird()];
	}

	for (MGSize i = 0; i < tabsurfnode.size(); ++i)
	{
		for (MGSize j = 0; j < tabsurfnode[i].size(); ++j)
		{
			tabsurfnode[i][j] = idmap[tabsurfnode[i][j]];
		}

		//vector<MGSize>::iterator it = lower_bound(tabsurfnode[i].begin(), tabsurfnode[i].end(), itr->second);
		//if ((it != tabsurfnode[i].end()) && ((*it) == itr->second))
		//{
		//	newtabsurfnode[i].push_back(id + 1);
		//}
	}

	for (MGSize i = 0; i < tabsurfnode.size(); ++i)
	{
		sort(tabsurfnode[i].begin(), tabsurfnode[i].end());
		vector<MGSize>::iterator it = unique(tabsurfnode[i].begin(), tabsurfnode[i].end());
		tabsurfnode[i].erase(it, tabsurfnode[i].end());
	}

	//tabsurfnode.swap(newtabsurfnode);
	mtabNode.swap(tabnn);
	cout << mtabNode.size() << endl;
}

template<Dimension DIM>
void ReadPlot3D<DIM>::CopyToGrid()
{
	mGrd.IOTabNodeResize( mtabNode.size() );

	vector<MGFloat>	tabx(DIM);

	for ( MGSize i=0; i<mtabNode.size(); ++i)
	{
		for ( MGInt j=0; j<DIM; ++j)
			tabx[j] = mtabNode[i].cX(j);

		mGrd.IOSetNodePos( tabx, i );

	}

	MGSize	ntyp = DIM+1;	// only simplex grid is supported
	vector<MGSize> tabid;

	tabid.resize( ntyp);
	mGrd.IOTabCellResize( ntyp, mtabCell.size() );

	for ( MGSize i=0; i<mtabCell.size(); ++i)
	{
		for (MGSize j = 0; j <= DIM; ++j)
			tabid[j] = mtabCell[i].cElem(j);

		//tabid[0] = mtabCell[i].cFirst();;
		//tabid[1] = mtabCell[i].cThird();;
		//tabid[2] = mtabCell[i].cSecond();;
		mGrd.IOSetCellIds( tabid, ntyp, i);
	}

	MGSize nbface = 0;
	for ( MGSize ibc=0; ibc<mtabBnd.size(); ++ibc)
		nbface += mtabBnd[ibc].size();


	mGrd.IOTabBFaceResize( DIM, nbface );

	tabid.resize( DIM );
	MGSize id = 0;
	for ( MGSize ibc=0; ibc<mtabBnd.size(); ++ibc)
	{
		for ( MGSize i=0; i<mtabBnd[ibc].size(); ++i)
		{
			for (MGSize j = 0; j < DIM; ++j)
			{
				tabid[j] = mtabBnd[ibc][i].cElem(j);
			}
			//tabid[0] = mtabBnd[ibc][i].cFirst();
			//tabid[1] = mtabBnd[ibc][i].cSecond();
			mGrd.IOSetBFaceSurf( ibc+1, DIM, id );
			mGrd.IOSetBFaceIds( tabid, DIM, id );
			++id;
		}
	}

}

template<Dimension DIM>
void ReadPlot3D<DIM>::Read( istream& file)
{
	ProgressBar	bar(40);

	file >> mBlockN;
	
	mtabBlockN.resize( mBlockN );
	for (MGSize iblock = 0; iblock < mBlockN; ++iblock)
		for (MGSize i = 0; i < DIM; ++i)
			file >> mtabBlockN[iblock].rX(i);

	MGSize n=0;
	for (MGSize iblock = 0; iblock < mBlockN; ++iblock)
	{
		MGSize nn = 1;
		for (MGSize i = 0; i < DIM; ++i)
		{
			nn *= mtabBlockN[iblock].cX(i);
		}
		n += nn;
		//n += mtabBlockN[iblock].cX() * mtabBlockN[iblock].cY();
	}
		

	mtabNode.resize( n );

	MGSize offset = 0;
	for ( MGSize iblock=0; iblock<mBlockN; ++iblock)
	{
		//MGSize nnode = mtabBlockN[iblock].cX() * mtabBlockN[iblock].cY();
		MGSize nnode = 1;
		for (MGSize i = 0; i < DIM; ++i)
		{
			nnode *= mtabBlockN[iblock].cX(i);
		}

		//vector< Vect<DIM_2D> > tabnode;
		vector< Vect<DIM> > tabnode;
		tabnode.resize( nnode );

		if (DIM == DIM_2D)
		{
			for (MGSize ii = 0; ii < DIM; ++ii)
				for (MGSize i = 0; i<mtabBlockN[iblock].cY(); ++i)
					for (MGSize j = 0; j<mtabBlockN[iblock].cX(); ++j)
						file >> mtabNode[i*mtabBlockN[iblock].cX() + j + offset].rX(ii);
		}
		else if (DIM_3D)
		{
			for (MGSize ii = 0; ii < DIM; ++ii)
				for (MGSize k = 0; k < mtabBlockN[iblock].cZ(); ++k)
					for (MGSize i = 0; i < mtabBlockN[iblock].cY(); ++i)
						for (MGSize j = 0; j < mtabBlockN[iblock].cX(); ++j)
						{
							MGFloat tmp;
							file >> tmp;
							MGSize ind = k*mtabBlockN[iblock].cX()*mtabBlockN[iblock].cY() + i*mtabBlockN[iblock].cX() + j + offset;
							mtabNode[ind].rX(ii) = tmp;
							// file >> mtabNode[k*mtabBlockN[iblock].cX()*mtabBlockN[iblock].cY() + i*mtabBlockN[iblock].cX() + j + offset].rX(ii);
						}
		}

		//for ( MGSize i=0; i<mtabBlockN[iblock].cY(); ++i)
		//	for ( MGSize j=0; j<mtabBlockN[iblock].cX(); ++j)
		//		file >> mtabNode[ i*mtabBlockN[iblock].cX() + j + offset].rY();

		offset += nnode;
	}

	MGSize	ntyp = DIM+1;

	MGSize nc=0;
	//for ( MGSize iblock=0; iblock<mBlockN; ++iblock)
	//	nc += ( mtabBlockN[iblock].cX()-1) * (mtabBlockN[iblock].cY()-1);

	for (MGSize iblock = 0; iblock < mBlockN; ++iblock)
	{
		MGSize n = 1;
		for (MGSize ii = 0; ii < DIM; ++ii)
			n *= (mtabBlockN[iblock].cX(ii) - 1);
		nc += n;	
	}
	cout << "Cell n: " << nc << endl;
	if (DIM == DIM_2D)
	{
		mtabCell.resize(2 * nc);

		MGSize id = 0;
		for (MGSize iblock = 0; iblock < mBlockN; ++iblock)
		{
			// cells

			MGSize nxcell = mtabBlockN[iblock].cX() - 1;
			MGSize nycell = mtabBlockN[iblock].cY() - 1;
			MGSize ncell = nxcell * nycell;

			vector<MGSize> tabid;
			tabid.resize(ntyp);

			for (MGSize i = 0; i < nycell; ++i)
				for (MGSize j = 0; j < nxcell; ++j)
					//if ( j < nxcell/2)
					if (j >= nxcell / 2)
					{
						mtabCell[id].rElem(0) = i    *mtabBlockN[iblock].cX() + j;
						mtabCell[id].rElem(1) = (i + 1)*mtabBlockN[iblock].cX() + j;
						mtabCell[id].rElem(2) = (i + 1)*mtabBlockN[iblock].cX() + j + 1;
						++id;

						mtabCell[id].rElem(0) = i    *mtabBlockN[iblock].cX() + j;
						mtabCell[id].rElem(1) = (i + 1)*mtabBlockN[iblock].cX() + j + 1;
						mtabCell[id].rElem(2) = i    *mtabBlockN[iblock].cX() + j + 1;
						++id;
					}
					else
					{
						mtabCell[id].rElem(0) = i    *mtabBlockN[iblock].cX() + j;
						mtabCell[id].rElem(1) = (i + 1)*mtabBlockN[iblock].cX() + j;
						mtabCell[id].rElem(2) = i    *mtabBlockN[iblock].cX() + j + 1;
						++id;

						mtabCell[id].rElem(0) = (i + 1)*mtabBlockN[iblock].cX() + j;
						mtabCell[id].rElem(1) = (i + 1)*mtabBlockN[iblock].cX() + j + 1;
						mtabCell[id].rElem(2) = i    *mtabBlockN[iblock].cX() + j + 1;
						++id;
					}
		}
	}
	else if (DIM == DIM_3D)
	{
		//vector<MGSize>	tabsurfid(mtabNode.size(), 0);

		vector< vector<MGSize> > tabsurfnode(mBlockN*6);



		mtabstrcell.resize(2*nc);
		MGSize id = 0;
		offset = 0;

		MGSize posvol = 0;
		MGSize negvol = 0;

		MGSize surfid = 1;
		for (MGSize iblock = 0; iblock < mBlockN; ++iblock)
		{
			MGSize nnode = 1;

			for (MGSize i = 0; i < DIM; ++i)
			{
				nnode *= mtabBlockN[iblock].cX(i);
			}

			// cells
			MGSize nxcell = mtabBlockN[iblock].cX() - 1;
			MGSize nycell = mtabBlockN[iblock].cY() - 1;
			MGSize nzcell = mtabBlockN[iblock].cZ() - 1;

			
			//for (MGSize i = 0; i < nycell; ++i)
				for (MGSize k = 0; k <= nzcell; ++k)
					for (MGSize j = 0; j <= nxcell; ++j)
					{
						tabsurfnode[iblock * 6 + 0].push_back(NodeId(j, 0, k, iblock) + offset);
						tabsurfnode[iblock * 6 + 1].push_back(NodeId(j, nycell, k, iblock) + offset);

			//			tabsurfid[NodeId(j, 0, k, iblock) + offset] = surfid;
			//			tabsurfid[NodeId(j + 1, 0, k, iblock) + offset] = surfid;
			//			tabsurfid[NodeId(j + 1, 0, k + 1, iblock) + offset] = surfid;
			//			tabsurfid[NodeId(j, 0, k + 1, iblock) + offset] = surfid;

			//			tabsurfid[NodeId(j, nycell-1, k, iblock) + offset] = surfid+1;
			//			tabsurfid[NodeId(j + 1, nycell - 1, k, iblock) + offset] = surfid+1;
			//			tabsurfid[NodeId(j + 1, nycell - 1, k + 1, iblock) + offset] = surfid+1;
			//			tabsurfid[NodeId(j, nycell - 1, k + 1, iblock) + offset] = surfid + 1;
					}
			//surfid += 2;
			for (MGSize i = 0; i <= nycell; ++i)
				//for (MGSize k = 0; k < nzcell; ++k)
					for (MGSize j = 0; j <= nxcell; ++j)
					{
						tabsurfnode[iblock * 6 + 2].push_back(NodeId(j, i, 0, iblock) + offset);
						tabsurfnode[iblock * 6 + 3].push_back(NodeId(j, i, nzcell, iblock) + offset);
						//tabsurfid[NodeId(j, i, 0, iblock) + offset] = surfid;
						//tabsurfid[NodeId(j + 1, i, 0, iblock) + offset] = surfid;
						//tabsurfid[NodeId(j + 1, i+1, 0, iblock) + offset] = surfid;
						//tabsurfid[NodeId(j, i+1, 0, iblock) + offset] = surfid;

						//tabsurfid[NodeId(j, i, nzcell-1, iblock) + offset] = surfid + 1;
						//tabsurfid[NodeId(j + 1, i, nzcell - 1, iblock) + offset] = surfid + 1;
						//tabsurfid[NodeId(j + 1, i + 1, nzcell - 1, iblock) + offset] = surfid + 1;
						//tabsurfid[NodeId(j, i + 1, nzcell - 1, iblock) + offset] = surfid + 1;
					}
		//	surfid += 2;
			for (MGSize i = 0; i <= nycell; ++i)
				for (MGSize k = 0; k <= nzcell; ++k)
					//for (MGSize j = 0; j < nxcell; ++j)
					{
						tabsurfnode[iblock * 6 + 4].push_back(NodeId(0, i, k, iblock) + offset);
						tabsurfnode[iblock * 6 + 5].push_back(NodeId(nxcell, i, k, iblock) + offset);
						//tabsurfid[NodeId(0, i, k, iblock) + offset] = surfid;
						//tabsurfid[NodeId(0, i+1, k, iblock) + offset] = surfid;
						//tabsurfid[NodeId(0, i+1, k + 1, iblock) + offset] = surfid;
						//tabsurfid[NodeId(0, i, k + 1, iblock) + offset] = surfid;

						//tabsurfid[NodeId(nxcell-1, i, k, iblock) + offset] = surfid + 1;
						//tabsurfid[NodeId(nxcell - 1, i + 1, k, iblock) + offset] = surfid + 1;
						//tabsurfid[NodeId(nxcell - 1, i + 1, k + 1, iblock) + offset] = surfid + 1;
						//tabsurfid[NodeId(nxcell - 1, i, k + 1, iblock) + offset] = surfid + 1;
					}
		//	surfid += 2;

			for (MGSize i = 0; i < tabsurfnode.size(); ++i)
			{
				sort(tabsurfnode[i].begin(), tabsurfnode[i].end());
			}


			for (MGSize i = 0; i < nycell; ++i)
				for (MGSize k = 0; k < nzcell; ++k)
					for (MGSize j = 0; j < nxcell; ++j)
					{
						if (k >= nzcell / 2.)
						{
							mtabstrcell[id].rElem(0) = NodeId(j, i, k, iblock) + offset;
							mtabstrcell[id].rElem(2) = NodeId(j + 1, i, k, iblock) + offset;
							mtabstrcell[id].rElem(1) = NodeId(j + 1, i, k + 1, iblock) + offset;
							mtabstrcell[id].rElem(3) = NodeId(j, i + 1, k, iblock) + offset;
							mtabstrcell[id].rElem(5) = NodeId(j + 1, i + 1, k, iblock) + offset;
							mtabstrcell[id].rElem(4) = NodeId(j + 1, i + 1, k + 1, iblock) + offset;
							++id;

							mtabstrcell[id].rElem(0) = NodeId(j, i, k, iblock) + offset;
							mtabstrcell[id].rElem(2) = NodeId(j + 1, i, k + 1, iblock) + offset;
							mtabstrcell[id].rElem(1) = NodeId(j, i, k + 1, iblock) + offset;
							mtabstrcell[id].rElem(3) = NodeId(j, i + 1, k, iblock) + offset;
							mtabstrcell[id].rElem(5) = NodeId(j + 1, i + 1, k + 1, iblock) + offset;
							mtabstrcell[id].rElem(4) = NodeId(j, i + 1, k + 1, iblock) + offset;
							++id;
						}
						else
						{
							mtabstrcell[id].rElem(0) = NodeId(j, i, k, iblock) + offset;
							mtabstrcell[id].rElem(2) = NodeId(j + 1, i, k, iblock) + offset;
							mtabstrcell[id].rElem(1) = NodeId(j, i, k + 1, iblock) + offset;
							mtabstrcell[id].rElem(3) = NodeId(j, i + 1, k, iblock) + offset;
							mtabstrcell[id].rElem(5) = NodeId(j + 1, i + 1, k, iblock) + offset;
							mtabstrcell[id].rElem(4) = NodeId(j, i + 1, k + 1, iblock) + offset;
							++id;

							mtabstrcell[id].rElem(0) = NodeId(j+1, i, k, iblock) + offset;
							mtabstrcell[id].rElem(2) = NodeId(j + 1, i, k + 1, iblock) + offset;
							mtabstrcell[id].rElem(1) = NodeId(j, i, k + 1, iblock) + offset;
							mtabstrcell[id].rElem(3) = NodeId(j+1, i + 1, k, iblock) + offset;
							mtabstrcell[id].rElem(5) = NodeId(j + 1, i + 1, k + 1, iblock) + offset;
							mtabstrcell[id].rElem(4) = NodeId(j, i + 1, k + 1, iblock) + offset;
							++id;
						}
					}
			offset += nnode;
		}

		//cout << "\nN. pos area:\t" << posvol << "\nN. neg area:\t" << negvol << endl;

		mtabstrcell.resize(id);

		RemoveDuplicatePointsStr(tabsurfnode);
		
		mtabCell.resize(mtabstrcell.size() * 3);

		cout << "Tetra cell n: " << mtabCell.size() << endl;

		posvol = 0;
		negvol = 0;

		id = 0;

		vector<bool> ispos(mtabNode.size(), true);
		for (MGSize i = 0; i < mtabstrcell.size(); ++i)
		{
			MGSize prism[6];
			for (MGSize j = 0; j < 6; ++j)
			{
				prism[j] = mtabstrcell[i].rElem(j);
			}
			MGSize tabtetra[3][4];

			ConvertPrism2Tetra(prism, tabtetra);
			for (MGSize j = 0; j < 3; ++j)
			{
				for (MGSize k = 0; k < 4; ++k)
				{
					mtabCell[id].rElem(k) = tabtetra[j][k];
				}

				Simplex<DIM> smpl(
					mtabNode[tabtetra[j][0]],
					mtabNode[tabtetra[j][1]], 
					mtabNode[tabtetra[j][2]], 
					mtabNode[tabtetra[j][3]]);
				MGFloat vol = smpl.Volume();

				if (fabs(vol - 0.) < 1e-15)
				{
					++negvol;
					ispos[tabtetra[j][0]] = 0;
					ispos[tabtetra[j][1]] = 0;
					ispos[tabtetra[j][2]] = 0;
					ispos[tabtetra[j][3]] = 0;
				}
				else
					++posvol;

				++id;
			}
		}

		ExtractBoundary(tabsurfnode);

		cout << "\nN. pos vol:\t" << posvol << "\nN. neg vol:\t" << negvol << endl;

		//ofstream file(MGString("test.dat").c_str());
		//MGString type;
		//type = "FEBRICK";

		//file << "VARIABLES = ";
		//file << "\"X\", \"Y\"";
		//if (DIM == DIM_3D) file << ", \"Z\"";
		//file << ", \"VOL_FLAG\"";
		//file << endl;

		//file << "ZONE T=\"" << "test" << "\", DATAPACKING=POINT, NODES=" << mtabNode.size() << " , ELEMENTS=" << mtabCell.size()
		//	<< " , ZONETYPE=" << type << endl;

		//for (size_t i = 0; i < mtabNode.size(); i++)
		//{
		//	file << mtabNode[i].cX() << "\t" << mtabNode[i].cY() << "\t" << mtabNode[i].cZ() << "\t" << ispos[i] << "\n";
		//}

		//for (size_t i = 0; i < mtabCell.size(); i++)
		//{
		//	file << mtabCell[i].cElem(0) + 1 << "\t" << mtabCell[i].cElem(1) + 1 << "\t" << mtabCell[i].cElem(2) + 1 << "\t"
		//		<< mtabCell[i].cElem(2) + 1 << "\t" << mtabCell[i].cElem(3) + 1 << "\t" << mtabCell[i].cElem(3) + 1 << "\t"
		//		<< mtabCell[i].cElem(3) + 1 << "\t" << mtabCell[i].cElem(3) + 1 << "\n";
		//}

		//file.close();
	}

	//Stitch();
	if (DIM == DIM_2D)
	{
		RemoveDuplicatePoints();
		ExtractBoundary();

		CopyToGrid();
	}
	else if (DIM == DIM_3D)
	{
		//RemoveDuplicatePoints();
		

		CopyToGrid();
	}

}


//template <Dimension DIM> 
//void ReadPlot3D<DIM>::Read( istream& file)
//{
//	ProgressBar	bar(40);
//
//	file >> mBlockN;
//	
//	mtabBlockN.resize( mBlockN );
//	for ( MGSize iblock=0; iblock<mBlockN; ++iblock)
//		file >> mtabBlockN[iblock].rX() >> mtabBlockN[iblock].rY();
//
//	MGSize n=0;
//	for ( MGSize iblock=0; iblock<mBlockN; ++iblock)
//		n += mtabBlockN[iblock].cX() * mtabBlockN[iblock].cY();
//
//	mGrd.IOTabNodeResize( n );
//
//	MGSize offset = 0;
//	for ( MGSize iblock=0; iblock<mBlockN; ++iblock)
//	{
//		MGSize nnode = mtabBlockN[iblock].cX() * mtabBlockN[iblock].cY();
//		vector< Vect<DIM_2D> > tabnode;
//		tabnode.resize( nnode );
//
//		for ( MGSize i=0; i<mtabBlockN[iblock].cY(); ++i)
//			for ( MGSize j=0; j<mtabBlockN[iblock].cX(); ++j)
//				file >> tabnode[ i*mtabBlockN[iblock].cX() + j].rX();
//
//		for ( MGSize i=0; i<mtabBlockN[iblock].cY(); ++i)
//			for ( MGSize j=0; j<mtabBlockN[iblock].cX(); ++j)
//				file >> tabnode[ i*mtabBlockN[iblock].cX() + j].rY();
//
//		vector<MGFloat>	tabx(DIM_2D);
//		for ( MGSize i=0; i<nnode; ++i)
//		{
//			tabx[0] = tabnode[i].cX();
//			tabx[1] = tabnode[i].cY();
//			mGrd.IOSetNodePos( tabx, i + offset );
//		}
//
//		offset = nnode;
//	}
//
//	MGSize	ntyp = DIM_2D+1;
//
//	MGSize nc=0;
//	for ( MGSize iblock=0; iblock<mBlockN; ++iblock)
//		nc += ( mtabBlockN[iblock].cX()-1) * (mtabBlockN[iblock].cY()-1);
//
//	mGrd.IOTabCellResize( ntyp, 2*nc);
//
//	MGSize id = 0;
//	for ( MGSize iblock=0; iblock<mBlockN; ++iblock)
//	{
//		// cells
//
//		MGSize nxcell = mtabBlockN[iblock].cX() - 1; 
//		MGSize nycell = mtabBlockN[iblock].cY() - 1; 
//		MGSize ncell =  nxcell * nycell;
//
//		vector<MGSize> tabid;
//		tabid.resize( ntyp);
//
//		for ( MGSize i=0; i<nycell; ++i)
//			for ( MGSize j=0; j<nxcell; ++j)
//				if ( j < nxcell/2)
//				{
//					tabid[0] = i    *mtabBlockN[iblock].cX() + j;
//					tabid[1] = (i+1)*mtabBlockN[iblock].cX() + j;
//					tabid[2] = (i+1)*mtabBlockN[iblock].cX() + j+1;
//					mGrd.IOSetCellIds( tabid, ntyp, id++);
//
//					tabid[0] = i    *mtabBlockN[iblock].cX() + j;
//					tabid[1] = (i+1)*mtabBlockN[iblock].cX() + j+1;
//					tabid[2] = i    *mtabBlockN[iblock].cX() + j+1;
//					mGrd.IOSetCellIds( tabid, ntyp, id++);
//				}
//				else
//				{
//					tabid[0] = i    *mtabBlockN[iblock].cX() + j;
//					tabid[1] = (i+1)*mtabBlockN[iblock].cX() + j;
//					tabid[2] = i    *mtabBlockN[iblock].cX() + j+1;
//					mGrd.IOSetCellIds( tabid, ntyp, id++);
//
//					tabid[0] = (i+1)*mtabBlockN[iblock].cX() + j;
//					tabid[1] = (i+1)*mtabBlockN[iblock].cX() + j+1;
//					tabid[2] = i    *mtabBlockN[iblock].cX() + j+1;
//					mGrd.IOSetCellIds( tabid, ntyp, id++);
//				}
//
//	}
//
//
////	read(2,*) nbl
////     read(2,*) (idim(n),jdim(n),kdim(n),n=1,nbl)
////      do n=1,nbl
////        read(2,*) (((x(i,j,k,n),i=1,idim(n)),j=1,jdim(n)),k=1,kdim(n)),
////     +            (((y(i,j,k,n),i=1,idim(n)),j=1,jdim(n)),k=1,kdim(n)),
////     +            (((z(i,j,k,n),i=1,idim(n)),j=1,jdim(n)),k=1,kdim(n))
////      enddo
//
//
//	//MGSize nvar = mtabVar.size() - DIM;
//	//if ( mtabVar.size() < DIM)
//	//	THROW_INTERNAL("ReadTEC::ReadTECascii :: negative nvar = " << nvar );
////
////
////
////	mGrd.IOTabNodeResize( mNNode);
////	if ( mpSol) mpSol->IOResize( mNNode);
////
////	vector<MGFloat>	tabx(DIM);
////	vector<MGFloat>	tabs(nvar);
////
////	cout << endl;
////	bar.Init( mNNode );
////	bar.Start();
////	for ( MGSize i=0; i<mNNode; ++i, ++bar)
////	{
////		for ( MGInt j=0; j<DIM; ++j)
////			file >> tabx[j];
////
////		mGrd.IOSetNodePos( tabx, i );
////
////		for ( MGSize j=0; j<nvar; ++j)
////			file >> tabs[j];
////
////		if ( mpSol)
////			mpSol->IOSetBlockVector( tabs, i);
////
////	}
////	bar.Finish();
////
////	MGSize	ntyp = DIM+1;	// only simplex grid is supported
////	vector<MGSize> tabid;
////
////	tabid.resize( ntyp);
////	mGrd.IOTabCellResize( ntyp, mNCell);
////
////	bar.Init( mNCell );
////	bar.Start();
////	for ( MGSize i=0; i<mNCell; ++i, ++bar)
////	{
////		ReadCell<DIM>( file, tabid, i);
////		mGrd.IOSetCellIds( tabid, ntyp, i);
////	}
////	bar.Finish();
////
////	//cout << endl;
//}

template<Dimension DIM>
void ReadPlot3D<DIM>::DoRead( istream& file )
{
	if ( mGrd.Dim() == DIM_2D )
	{
		Read( file);
	}
	else if (mGrd.Dim() == DIM_3D)
	{
		Read(file);
	}
	else
		THROW_INTERNAL( "ReadPlot3D<DIM>::DoRead() - bad dim");
}

template<Dimension DIM>
void ReadPlot3D<DIM>::DoRead( const MGString& fname)
{
	ifstream file( fname.c_str(), ios::in);

	try
	{
		if ( ! file)
			THROW_FILE( "can not open the file", fname);

		cout << "Reading Plot3D '" << fname << "' :";
		cout.flush();

		DoRead( file);

		cout <<  "Reading Plot3D - FINISHED" << endl;
	}
	catch ( EHandler::Except& e)
	{
		cout << "ERROR: ReadPlot3D<DIM>::DoRead() - file name: " << fname << endl << endl;
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);

		THROW_INTERNAL("END");
	}
}


template class ReadPlot3D < DIM_2D > ;
template class ReadPlot3D < DIM_3D >;
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

