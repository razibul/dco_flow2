#include "writegmsh.h"
#include "libcoreio/store.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace IOSpace {  



template <> 
MGSize	WriteGMSH::GetGMSHtype<DIM_1D>( const MGSize& type)
{
	switch ( type)
	{
	case 2:
		return 1;
	}

	return 0;
}

template <> 
MGSize	WriteGMSH::GetGMSHtype<DIM_2D>( const MGSize& type)
{
	switch ( type)
	{
	case 3:
		return 2;
	case 4:
		return 3;
	}

	return 0;
}

template <> 
MGSize	WriteGMSH::GetGMSHtype<DIM_3D>( const MGSize& type)
{
	switch ( type)
	{
	case 4:
		return 4;
	case 8:
		return 5;
	case 6:
		return 6;
	case 5:
		return 7;
	}

	return 0;
}


template <Dimension DIM>
void WriteGMSH::WriteMSHascii( ostream& file)
{
	MGString	st;
	MGSize		n, id;

	file << "$MeshFormat" << endl;
	file << "2.2 0 8" << endl;
	file << "$EndMeshFormat" << endl;

	////////////////////////////
	// nodes
	vector<MGFloat>	tabx(DIM);

	file << "$Nodes" << endl;

	n = mGrd.IOSizeNodeTab();
	file << n << endl;

	for ( MGSize i=0; i<n; ++i)
	{
		mGrd.IOGetNodePos( tabx, i);
	
		file << i+1 << " ";
		for ( MGInt j=0; j<DIM; ++j)
			file << setw(25) << setprecision(20) << tabx[j] << " ";

		file << endl;
	}
	file << "$EndNodes" << endl;

	////////////////////////////
	// cells

	file << "$Elements" << endl;

	MGSize	nb, ntyp;
	vector<MGSize> tabid;

	nb = mGrd.IONumCellTypes();

	MGSize ncell = 0;
	for ( MGSize k=0; k<nb; ++k)
		ncell += mGrd.IOSizeCellTab( mGrd.IOCellType( k) );

	file << ncell << endl;

	MGSize icell = 0;
	for ( MGSize k=0; k<nb; ++k)
	{
		ntyp = mGrd.IOCellType( k);
		n = mGrd.IOSizeCellTab( ntyp);
		tabid.resize( ntyp);

		for ( MGSize i=0; i<n; ++i)
		{
			mGrd.IOGetCellIds( tabid, ntyp, i);

			MGSize gmshtype = GetGMSHtype<DIM>( ntyp);
			file << ++icell << " " << gmshtype << " ";
			file << 2 << " " << 0 << " " << DIM << " ";

			for ( MGSize j=0; j<ntyp; ++j)
				file << tabid[j]+1 << " ";
			file << endl;
		}
	}

	file << "$EndElements" << endl;

	////////////////////////////
	// bfaces

	file << "$Elements" << endl;

	nb = mGrd.IONumBFaceTypes();

	MGSize nbface = 0;
	for ( MGSize k=0; k<nb; ++k)
		nbface += mGrd.IOSizeBFaceTab( mGrd.IOBFaceType( k) );

	file << nbface << endl;

	MGSize ibface = 0;
	for ( MGSize k=0; k<nb; ++k)
	{
		ntyp = mGrd.IOBFaceType( k);
		n = mGrd.IOSizeBFaceTab( ntyp);
		tabid.resize( ntyp);

		file << ntyp << " " << n << endl;

		for ( MGSize i=0; i<n; ++i)
		{
			mGrd.IOGetBFaceIds( tabid, ntyp, i);
			mGrd.IOGetBFaceSurf( id, ntyp, i );

			MGSize gmshtype = GetGMSHtype<static_cast<Dimension>(DIM-1)>( ntyp);
			file << ++ibface << " " << gmshtype << " ";
			file << 2 << " " << id << " " << DIM-1 << " ";

			for ( MGSize j=0; j<ntyp; ++j)
				file << tabid[j]+1 << " ";
			file << 0 << endl;
		}
	}

	file << "$EndElements" << endl;
}


void WriteGMSH::DoWrite( ostream& file, const bool& bin)
{
	if ( mGrd.Dim() == DIM_2D )
	{
		if ( bin)
			WriteMSHbin<DIM_2D>( file);
		else
			WriteMSHascii<DIM_2D>( file);
	}
	else if ( mGrd.Dim() == DIM_3D )
	{
		if ( bin)
			WriteMSHbin<DIM_3D>( file);
		else
			WriteMSHascii<DIM_3D>( file);
	}
	else
		THROW_INTERNAL( "WriteGMSH::DoWrite - bad dim");
}



void WriteGMSH::DoWrite( const MGString& fname, const bool& bin)
{
	ofstream ofile;

	if ( bin)
		ofile.open( fname.c_str(), ios::out | ios::binary);
	else
		ofile.open( fname.c_str(), ios::out );

	try
	{
		if ( ! ofile)
			THROW_FILE( "can not open the file", fname);

		DoWrite( ofile, bin);
	}
	catch ( EHandler::Except& e)
	{
		cout << "ERROR: WriteGMSH::DoWrite - file name: " << fname << endl << endl;
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);

		THROW_INTERNAL("END");
	}
}



} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
 