#ifndef __IO_READTEC_H__
#define __IO_READTEC_H__

#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"

#include "libcoreio/gridfacade.h"
#include "libcoreio/solfacade.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace IOSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


//////////////////////////////////////////////////////////////////////
// class ReadTEC
//////////////////////////////////////////////////////////////////////
class ReadTEC
{
public:
	ReadTEC( GridReadFacade& grd, SolFacade* psol=NULL) : mGrd(grd), mpSol(psol)	{}

	void	DoRead( const MGString& fname);
	void	DoRead( istream& file);

protected:
	template <Dimension DIM> void	ReadTECascii( istream& file);

	void	ReadHeader( istream& file);

	void	InitVariables( MGString& sparams);
	void	InitZone( MGString& sparams);

	template <Dimension DIM> void	ReadCell( istream& file, vector<MGSize>& tabid, const MGSize& id);

private:
	GridReadFacade&		mGrd;
	SolFacade			*mpSol;

	MGString			mFileTitle;
	MGString			mZoneTitle;
	
	vector<MGString>	mtabVar;
	MGSize				mNNode;
	MGSize				mNCell;
	MGString			mEType;
};




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace IO = IOSpace;


#endif // __IO_READTEC_H__
