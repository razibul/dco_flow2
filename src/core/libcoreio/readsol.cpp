
#include "readsol.h"
#include "libcoreio/store.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace IOSpace {


MGString ReadSOL::mstVersionString = "SOL_VER";
MGString ReadSOL::mstVersionNumber = "2.1";

bool ReadSOL::IsBin( const MGString& fname)
{
	ifstream ifile;

	ifile.open( fname.c_str(), ios::in | ios::binary);

	if ( CheckVersion_bin( ifile) )
		return true;

	return false;
}



bool ReadSOL::CheckVersion_ascii( istream& file)
{
	istringstream is;
	MGString	sname, sver;

	ReadLine( file, is);
	is >> sname >> sver;

	if ( sname != VerStr() != 0 || sver != VerNum() != 0 )
		return false;

	return true;
}


bool ReadSOL::CheckVersion_bin( istream& file)
{
	MGSize		n;
	MGString	sname, sver;

	file.read( (char*)&n, sizeof(MGSize) );
	if ( n > 255)
		return false;//THROW_INTERNAL( "bad binary file" );

	sname.resize( n);
	file.read( &sname[0], n);
	
	file.read( (char*)&n, sizeof(MGSize) );
	if ( n > 255)
		return false;//THROW_INTERNAL( "bad binary file" );

	sver.resize( n);
	file.read( &sver[0], n);

	if ( sname != VerStr() != 0 || sver != VerNum() != 0)
		return false;

	return true;
}


Dimension ReadSOL::ReadDim( const MGString& fname, const bool& bin)
{
	Dimension	dim;
	ifstream	file;
	MGString	skey;

	if ( bin)
	{
		file.open( fname.c_str(), ios::in | ios::binary);
		
		if ( ! CheckVersion_bin( file) )
		{
			ostringstream os;
			os << "incompatible grid file version (current ver: '" << VerStr() << " " << VerNum() << "')";
	
			THROW_INTERNAL( os.str() );
		}
	
		file.read( (char*)&dim, sizeof(Dimension) );
	}
	else
	{
		file.open( fname.c_str(), ios::in );
		
		if ( ! CheckVersion_ascii( file) )
		{
			ostringstream os;
			os << "incompatible grid file version (current ver: '" << VerStr() << " " << VerNum() << "')";
	
			THROW_INTERNAL( os.str() );
		}
		
		istringstream is;
		
		ReadLine( file, is);	// dimension
		is >> skey;
	
		dim = ::StrToDim( skey);
	
	}

	return dim;
} 

void ReadSOL::ReadHEADERascii( istream& file )
{
	istringstream is;

	MGString	skey, sname, sver;

	ReadLine( file, is);	// solution version
	is >> sname >> sver;

	if ( sname != VerStr() != 0 || sver != VerNum() != 0)
	{
		ostringstream os;
		os << "incompatible solution file version (current ver: '" << VerStr() << " " << VerNum() << "')";

		THROW_INTERNAL( os.str() );
	}

	ReadLine( file, is);	// dimension
	is >> skey;

	mDim = ::StrToDim( skey);

	ReadLine( file, mDesc);	// description

	ReadLine( file, is);	// number of variables / equations
	is >> mNEq;

	ReadLine( file, is);	// size of the solution vector
	is >> mN;

}

void ReadSOL::ReadHEADERbin( istream& file )
{
	MGString skey, sname, sver;
	MGSize n;

	file.read( (char*)&n, sizeof(MGSize) );
	if ( n > 255)
		THROW_INTERNAL( "bad binary file" );

	sname.resize( n);
	file.read( &sname[0], n);
	
	file.read( (char*)&n, sizeof(MGSize) );
	if ( n > 255)
		THROW_INTERNAL( "bad binary file" );

	sver.resize( n);
	file.read( &sver[0], n);

	if ( sname != VerStr() != 0 || sver != VerNum() != 0)
	{
		ostringstream os;
		os << "incompatible solution file version (current ver: '" << VerStr() << " " << VerNum() << "')";

		THROW_INTERNAL( os.str() );
	}


	file.read( (char*)&mDim, sizeof(Dimension) );

	file.read( (char*)&n, sizeof(MGSize) );	
	mDesc.resize( n);
	file.read( (char*)&mDesc[0], n );	// description

	//
	file.read( (char*)&mNEq, sizeof(MGSize) );	// number of variables / equations

	file.read( (char*)&mN, sizeof(MGSize) );	// size of the solution vector
}


inline void ReadSOL::ReadSOLascii( istream& file )
{
	ReadHEADERascii( file);

	if ( mDim != mSol.Dim() )
		THROW_INTERNAL( "Incompatible dimension" );

	mSol.IOSetName( mDesc);

	if ( mNEq != mSol.IOBlockSize() )
		THROW_INTERNAL( "incompatible block vector size in ReadSOLascii" );

	if ( mN != mSol.IOSize() )
	{
		mSol.IOResize( mN);
		cout << "ReadSOLascii :: solution resized !!!" << endl;
		//THROW_INTERNAL( "incompatible vector size in ReadSOLascii" );
	}


	istringstream is;
	vector<MGFloat>	tabx(mNEq);

	for ( MGSize i=0; i<mN; ++i)
	{
		ReadLine( file, is);

		for ( MGSize k=0; k<mNEq; ++k)
			is >> tabx[k];

		mSol.DimToUndim( tabx);

		mSol.IOSetBlockVector( tabx, i);
	}
}

inline void ReadSOL::ReadSOLbin( istream& file )
{
	ReadHEADERbin( file);

	if ( mDim != mSol.Dim() )
		THROW_INTERNAL( "Incompatible dimension" );

	mSol.IOSetName( mDesc);

	if ( mNEq != mSol.IOBlockSize() )
		THROW_INTERNAL( "incompatible block vector size in ReadSOLbin" );

	if ( mN != mSol.IOSize() )
	{
		mSol.IOResize( mN);
		cout << "ReadSOLbin :: solution resized !!!" << endl;
		//THROW_INTERNAL( "incompatible vector size in ReadSOLascii" );
	}

	vector<MGFloat>	tabx(mNEq);

	for ( MGSize i=0; i<mN; ++i)
	{
		file.read( (char*)&tabx[0], mNEq*sizeof(MGFloat) );

		mSol.DimToUndim( tabx);
		mSol.IOSetBlockVector( tabx, i);
	}
}


inline void ReadSOL::DoRead( istream& file, const bool& bin )
{
	if ( bin)
		ReadSOLbin( file);
	else
		ReadSOLascii( file);
}

void ReadSOL::DoRead( const MGString& fname, const bool& bin)
{
	ifstream ifile;

	if ( bin)
		ifile.open( fname.c_str(), ios::in | ios::binary);
	else
		ifile.open( fname.c_str(), ios::in );

	try
	{
		if ( ! ifile)
			THROW_FILE( "can not open the file", fname);

		DoRead( ifile, bin);
	}
	catch ( EHandler::Except& e)
	{
		cout << "ERROR: ReadSOL::DoRead - file name: " << fname << endl << endl;
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);

		THROW_INTERNAL("END");
	}
}


inline void ReadSOL::ReadHEADER( istream& file, const bool& bin )
{
	if ( bin)
		ReadHEADERbin( file);
	else
		ReadHEADERascii( file);
}

void ReadSOL::ReadHEADER( const MGString& fname, const bool& bin)
{
	ifstream ifile;

	if ( bin)
		ifile.open( fname.c_str(), ios::in | ios::binary);
	else
		ifile.open( fname.c_str(), ios::in );

	try
	{
		if ( ! ifile)
			THROW_FILE( "can not open the file", fname);

		ReadHEADER( ifile, bin);
	}
	catch ( EHandler::Except& e)
	{
		cout << "ERROR: ReadSOL::ReadHEADER - file name: " << fname << endl << endl;
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);

		THROW_INTERNAL("END");
	}
}



/*
template <Dimension DIM> 
inline void ReadSOL::ReadSOLascii( istream& file )
{
	istringstream is;

	MGString	skey, sname, sver;
	MGSize	n, neq;
	Dimension	dim;

	ReadLine( file, is);	// solution version
	is >> sname >> sver;

	if ( sname != VerStr() != 0 || sver != VerNum() != 0)
	{
		ostringstream os;
		os << "incompatible solution file version (current ver: '" << VerStr() << " " << VerNum() << "')";

		THROW_INTERNAL( os.str() );
	}

	ReadLine( file, is);	// dimension
	is >> skey;

	dim = ::StrToDim( skey);
	if ( dim != DIM)
		THROW_INTERNAL( "Incompatible dimension" );

	ReadLine( file, skey);	// description
	mSol.IOSetName( skey);

	ReadLine( file, is);	// number of variables / equations
	is >> neq;
	if ( neq != mSol.IOBlockSize() )
		THROW_INTERNAL( "incompatible block vector size in ReadSOLascii" );

	ReadLine( file, is);	// size of the solution vector
	is >> n;

	if ( n != mSol.IOSize() )
		THROW_INTERNAL( "incompatible vector size in ReadSOLascii" );

	vector<MGFloat>	tabx(neq);

	for ( MGSize i=0; i<n; ++i)
	{
		ReadLine( file, is);

		for ( MGSize k=0; k<neq; ++k)
			is >> tabx[k];

		mSol.DimToUndim( tabx);

		mSol.IOSetBlockVector( tabx, i);
	}
}

template <Dimension DIM>
inline void ReadSOL::ReadSOLbin( istream& file )
{
	MGString	skey, sname, sver;
	MGSize	n, neq;
	Dimension	dim;


	file.read( (char*)&n, sizeof(MGSize) );
	if ( n > 255)
		THROW_INTERNAL( "bad binary file" );

	sname.resize( n);
	file.read( &sname[0], n);
	
	file.read( (char*)&n, sizeof(MGSize) );
	if ( n > 255)
		THROW_INTERNAL( "bad binary file" );

	sver.resize( n);
	file.read( &sver[0], n);

	if ( sname != VerStr() != 0 || sver != VerNum() != 0)
	{
		ostringstream os;
		os << "incompatible solution file version (current ver: '" << VerStr() << " " << VerNum() << "')";

		THROW_INTERNAL( os.str() );
	}


	file.read( (char*)&dim, sizeof(Dimension) );
	if ( dim != DIM)
		THROW_INTERNAL( "Incompatible grid dimension" );


	file.read( (char*)&n, sizeof(MGSize) );	
	skey.resize( n);
	file.read( (char*)&skey[0], n );	// description
	mSol.IOSetName( skey);

	//
	file.read( (char*)&neq, sizeof(MGSize) );	// number of variables / equations

	if ( neq != mSol.IOBlockSize() )
		THROW_INTERNAL( "incompatible block vector size in ReadSOLascii" );

	file.read( (char*)&n, sizeof(MGSize) );	// size of the solution vector

	if ( n != mSol.IOSize() )
		THROW_INTERNAL( "incompatible vector size in ReadSOLascii" );

	vector<MGFloat>	tabx(neq);

	for ( MGSize i=0; i<n; ++i)
	{
		file.read( (char*)&tabx[0], neq*sizeof(MGFloat) );

		mSol.DimToUndim( tabx);
		mSol.IOSetBlockVector( tabx, i);
	}
}

*/

} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

