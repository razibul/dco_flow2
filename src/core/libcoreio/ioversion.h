#ifndef __IO_IOVERSION_H__
#define __IO_IOVERSION_H__

#include "mgdecl.h"
#include "dimension.h"
#include "store.h"

#include "gridfacade.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace IOSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


//////////////////////////////////////////////////////////////////////
// class IOVersion
//////////////////////////////////////////////////////////////////////
class IOVersion
{
public:

protected:
	template <Dimension DIM> void	ReadMSHascii( istream& file);
	template <Dimension DIM> void	ReadMSHbin( istream& file);

	static const MGString& VerNum()	{ return mstVersionNumber;}
	static const MGString& VerStr()	{ return mstVersionString;}

private:
	static MGString	mstVersionString;
	static MGString	mstVersionNumber;
};
//////////////////////////////////////////////////////////////////////




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace IO = IOSpace;


#endif // __IO_IOVERSION_H__

