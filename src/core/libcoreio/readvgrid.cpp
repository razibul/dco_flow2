#include "libcoreio/readvgrid.h"
#include "libcoreio/store.h"
#include "libcorecommon/regexp.h"
#include "libcorecommon/progressbar.h"




//                           VGRID Files
//
//The following is description of grid files generated with VGRID. 
//For most solvers, only mapbc, bc, and cogsg files are needed.
// 
//------------------------------------------------------------
//
//  1)  project.mapbc:  Patch/flow-boundary-condition file (ASCII)
//
//                    ----------format------------
//c
//       parameter(mpatch=***)
//c
//       integer bcpch(mpatch)
//       character*1 text(80)
//c
//       open(13,file='project.mapbc',form='formatted')
//c
//       read(13,900)text
//       read(13,900)text
//       read(13,900)text
//       read(13,900)text
//       do ipatch=1,npatch
//        read(13,*)jpatch,bcpch(ipatch),idum1,idum2,idum3
//       enddo
//c
//  900 format(80a1)
//c
//                   ----------------------------
//
//  where
//
//       text = text line
//     npatch = number of surface patches defining the geometry
//     jpatch = surface patch index
//      bcpch = flow boundary condition assigned to surface patch "jpatch"
//      idum1,idum2,idum3 = dummy variables
//
//
//**********************************************************************
//
//
//  2)  project.bc: patch/surface-triangle file (ASCII)
//
//                   ----------format-------------
//c
//       parameter(mbf=***)
//c
//       integer fapch(mbf),fnode(mbf,3)
//       character*1 text(80)
//c
//       open(unit=12,file='project.bc',form='formatted')
//c
//       read(12,*)nbf,nbc,npatch,igrid
//       read(12,900)text
//       do if=1,nbf
//        read(12,*)jf,fapch(if),fnode(if,in),in=1,3)
//       enddo
//  900  format(80a1)
//c
//                   ----------------------------
//
//  where
//
//        nbf = number of boundary triangular faces
//        nbc = number of surface grid nodes along patch boundaries (curves)
//     npatch = number of surface patches
//      igrid = 1 for inviscid grids; 2 for viscous grids
//       text = text line
//         jf = triangle index
//  fapch(if) = surface patch index containing surface triangle "if"
//fnode(if,in) = node "in" of triangle "if"
//
//Note: triangle connectivities are according to the right-hand rule with
//      the outward normals pointing into the computational domain.
//
//***********************************************************************
//
//
//  3)  project.cogsg: x,y,z coordinates of the grid nodes and
//                    tetrahedral node connectivity
//
//                   ----------format------------
//c
//      parameter(mp=***, mc=***)   !maximum number of grid nodes and cells
//      real(8)  crd(mp,3),t
//      integer int(mc,4)
//c
//       open(9,file='project.cogrd',form='unformatted',iostat=ios,
//     &      err=555,status='old')
//c
//      npoic=0
//      npois=1
//      nelec=0
//      neles=1
//      read(9)inew,nc,npo,nbn,npv,nev,t,
//     &       ((int(ie,in),ie=neles,nelee),in=1,4)
//      read(9)((crd(ip,id),ip=npois,npoie),id=1,3)
//      nelec=nelee
//      npoic=npoie
//c
// 100  continue
//      neles=neles+nelec
//      read(9,end=400)nelec
//      if(nelec .eq. 0) goto 400
//      nelee=neles+nelec-1
//      read(9)((int(ie,in),ie=neles,nelee),in=1,4)
//      npois=npois+npoic
//      read(9)npoic
//      npoie=npois+npoic-1
//      read(9)((crd(ip,id),ip=npois,npoie),id=1,3)
//c
//      goto 100
// 400  continue
//      npo=npoie
//      nc=nelee
//c
//                   ----------------------------
//
//  where
//
//         inew = a dummy variable (integer)
//           nc = number of tetrahedral cells
//          npo = total number of grid nodes (including nbn)
//          nbn = number of grid nodes on the boundaries (including nbc)
//          npv = number of grid points in the viscous layers
//                (=0 for Euler grids)
//          ncv = number of cells in the viscous layers
//                (=0 for Euler grids)
//            t = a dummy variable (real - double)
//   int(ie,in) = tetradhedral cell connectivity
//                (node "in" of cell "ie")
//   crd(ip,id) = x, y, and z coordinates of node "ip"
//
//  Note 1: the first "nbn" coordinates listed in this file are those of
//         the boundary nodes.
//
//  Note 2:  tetrahedral cell connectivities are given according to the
//           right-hand rule (3 nodes of the base in the counter-clockwise
//           direction followed by the 4th node.)
//
//************************************************************************
//
//  4) project.poin1: field nodes in the B.L. linked to the surface 
//                   mesh nodes (ASCII)
//
//                     ---------format----------
//       parameter(mp=***)
//c
//       integer lpoin(mp)
//c
//       open(14,file='project.poin1',form='formatted')
//c
//       read(14,*)nbn,npv
//       do ip=1,npv
//         read(14,*)jp,lpoin(ip)
//       enddo
//                        ---------------------
//
// where
//
//      nbn = number of grid nodes on the boundaries
//      npv = number of grid nodes inside the boundary layer (including nbn)
//       jp = node index
//lpoin(ip) = surface node linked to the field node "ip"
//
// Note 1: "viscous" field nodes are generated along rays emanating from
//          the surface mesh nodes. lpoin gives the corresponding surface
//          node index for each viscous field node along the ray.
//
// Note 2: lpoin(ip) = ip, for ip=1,nbn (surface mesh nodes).
//
//************************************************************************ 


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace IOSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//



template <class T>
void endswap(T *objp)
{
  unsigned char *memp = reinterpret_cast<unsigned char*>(objp);
  std::reverse(memp, memp + sizeof(T));
}

MGSize readMGSize( istream& file, const bool& bverb=false)
{
	MGSize n;
	file.read( (char*)&n, sizeof(MGSize) );
	endswap( &n);

	if ( bverb)
		cout << dec << n << "  " << hex << n << dec << endl;

	return n;
}

void ReadVGRID::ReadCOGSG_multi( istream& file)
{

	ProgressBar	bar(40);

	MGSize llength, junk;

	llength = readMGSize( file, true);

	readMGSize( file);

	MGSize nc, npo, nbn, npv, nev;
	nc  = readMGSize( file, true);
	npo = readMGSize( file, true);
	nbn = readMGSize( file, true);
	npv = readMGSize( file, true);
	nev = readMGSize( file, true);

	readMGSize( file);
	readMGSize( file);

	mtabCell.resize( nc);
	mtabNode.resize( npo);

	MGSize nPredictedLength = (8 + 4*nc)*4; // That last 4 is sizeof(int)...
	cout << endl;
	cout << "length = " << llength << " " <<nPredictedLength << endl;

	MGSize nel_start = 0;
	MGSize nel_end   = nc;
	MGSize nel_cur   = nel_end - nel_start;

	MGSize npo_start = 0;
	MGSize npo_end   = npo;
	MGSize npo_cur   = npo_end - npo_start;


	for ( MGSize k=0; k<4; ++k)
	{
		bar.Init( nel_cur );
		bar.Start();

		for ( MGSize i=nel_start; i<nel_end; ++i, ++bar)
		{
			MGSize id;
			file.read( (char*)&id, sizeof(MGSize) );
			endswap( &id);
			mtabCell[i].rElem(k) = id;
		}

		bar.Finish();
	}

	MGSize lineCheck = readMGSize( file);;
	cout << dec << "length = " << llength << " " << lineCheck << endl;

	llength = readMGSize( file, true);


	for ( MGSize k=0; k<3; ++k)
	{
		bar.Init( mtabNode.size() );
		bar.Start();

		for ( MGSize i=npo_start; i<npo_end; ++i, ++bar)
		{
			MGFloat x;
			file.read( (char*)&x, sizeof(MGFloat) );
			endswap( &x);
			mtabNode[i].rX(k) = x;
		}

		bar.Finish();
	}

	file.read( (char*)&lineCheck, sizeof(MGSize) );
	endswap( &lineCheck);

	cout << dec << "length = " << llength << " " << lineCheck << endl;


	cout << setprecision(16);
	cout << dec << mtabNode[0].cX(0) << endl;
	cout << dec << mtabNode[1].cX(0) << endl;
	cout << dec << mtabNode[2].cX(0) << endl;
	cout << dec << mtabNode[3].cX(0) << endl;
	cout << dec << mtabNode[4].cX(0) << endl;



	cout << endl;
	cout << "mtabNode[0]" << endl;
	cout << dec << mtabNode[0].cX(0) << endl;
	cout << dec << mtabNode[0].cX(1) << endl;
	cout << dec << mtabNode[0].cX(2) << endl;
	cout << endl;
	cout << "mtabNode[mtabNode.size()-1]" << endl;
	cout << dec << mtabNode[mtabNode.size()-1].cX(0) << endl;
	cout << dec << mtabNode[mtabNode.size()-1].cX(1) << endl;
	cout << dec << mtabNode[mtabNode.size()-1].cX(2) << endl;

	cout << dec;

	/////////////
	do
	{
		nel_start = nel_end;


		readMGSize( file);
		nel_cur = readMGSize( file, true);
		nel_end = nel_start + nel_cur;

		if ( nel_cur == 0)
			break;

		mtabCell.resize( nel_end);

		cout << endl;
		readMGSize( file, true);
		llength = readMGSize( file, true);

		nPredictedLength = (8 + 4*(nel_cur))*4; // That last 4 is sizeof(int)...
		cout << endl;
		cout << dec << "length = " << llength << " " << nPredictedLength << endl;

		for ( MGSize k=0; k<4; ++k)
		{
			bar.Init( nel_cur );
			bar.Start();

			for ( MGSize i=nel_start; i<nel_end; ++i, ++bar)
			{
				MGSize id;
				file.read( (char*)&id, sizeof(MGSize) );
				endswap( &id);
				mtabCell[i].rElem(k) = id;
			}

			bar.Finish();
		}
		readMGSize( file, true);

		//-----------
		npo_start = npo_end;

		readMGSize( file, true);
		npo_cur = readMGSize( file, true);
		npo_end = npo_start + npo_cur;

		mtabNode.resize( npo_end);

		cout << endl;
		readMGSize( file, true);
		llength = readMGSize( file, true);

		nPredictedLength = (8 + 4*(nel_cur))*4; // That last 4 is sizeof(int)...
		cout << endl;
		cout << dec << "length = " << llength << " " << nPredictedLength << endl;

		for ( MGSize k=0; k<3; ++k)
		{
			bar.Init( npo_cur );
			bar.Start();

			for ( MGSize i=npo_start; i<npo_end; ++i, ++bar)
			{
				MGFloat x;
				file.read( (char*)&x, sizeof(MGFloat) );
				endswap( &x);
				mtabNode[i].rX(k) = x;
			}

			bar.Finish();
		}
		readMGSize( file, true);

		cout << endl;
		//readMGSize( file, true);
		//readMGSize( file, true);
		//readMGSize( file, true);
		//readMGSize( file, true);
		//readMGSize( file, true);
	}
	while ( true);


	bar.Init( mtabCell.size() );
	bar.Start();
	npo = mtabNode.size();
	for ( MGSize i=0; i<mtabCell.size(); ++i, ++bar)
	{
		if ( mtabCell[i].rElem(0) == mtabCell[i].rElem(1) || mtabCell[i].rElem(0) == mtabCell[i].rElem(2) ||
			 mtabCell[i].rElem(0) == mtabCell[i].rElem(3) || mtabCell[i].rElem(1) == mtabCell[i].rElem(2) ||
			 mtabCell[i].rElem(1) == mtabCell[i].rElem(3) || mtabCell[i].rElem(2) == mtabCell[i].rElem(3) )
		{
			THROW_INTERNAL( " duplicate ids in cell - " << i);
		}

		if ( mtabCell[i].rElem(0) > npo || mtabCell[i].rElem(1) > npo || mtabCell[i].rElem(2) > npo || mtabCell[i].rElem(3) > npo )
		{
			THROW_INTERNAL( " id > max_nnodes in cell - " << i);
		}

	}
	bar.Finish();

}

void ReadVGRID::ReadCOGSG( istream& file)
{

	ProgressBar	bar(40);

	MGSize llength, junk;

	file.read( (char*)&llength, sizeof(MGSize) );
	endswap( &llength);
	cout << dec << llength << "  " << hex << llength << endl;


	file.read( (char*)&junk, sizeof(MGSize) );

	MGSize nc, npo, nbn, npv, nev;
	file.read( (char*)&nc, sizeof(MGSize) );
	file.read( (char*)&npo, sizeof(MGSize) );
	file.read( (char*)&nbn, sizeof(MGSize) );
	file.read( (char*)&npv, sizeof(MGSize) );
	file.read( (char*)&nev, sizeof(MGSize) );
	
	endswap( &nc);
	endswap( &npo);
	endswap( &nbn);
	endswap( &npv);
	endswap( &nev);
	
	cout << endl;
	cout << dec << nc << "  " << hex << nc << endl;
	cout << dec << npo << "  " << hex << npo << endl;
	cout << dec << nbn << "  " << hex << nbn << endl;
	cout << dec << npv << "  " << hex << npv << endl;
	cout << dec << nev << "  " << hex << nev << endl;
	cout << dec;

	MGSize nPredictedLength = (8 + 4*nc)*4; // That last 4 is sizeof(int)...
	cout << endl;
	cout << "length = " << llength << " " <<nPredictedLength << endl;


	file.read( (char*)&junk, sizeof(MGSize) );
	file.read( (char*)&junk, sizeof(MGSize) );


	mtabCell.resize( nc);
	mtabNode.resize( npo);

	//file.read( (char*)&junk, sizeof(MGSize) );
	//endswap( &junk);
	//cout << dec << junk << "  " << hex << junk << endl;
	//file.read( (char*)&junk, sizeof(MGSize) );
	//endswap( &junk);
	//cout << dec << junk << "  " << hex << junk << endl;

	MGSize count = 0;

	for ( MGSize k=0; k<4; ++k)
	{
		bar.Init( mtabCell.size() );
		bar.Start();

		for ( MGSize i=0; i<mtabCell.size(); ++i, ++bar)
		{
			MGSize id;
			file.read( (char*)&id, sizeof(MGSize) );
			endswap( &id);

			while ( id > npo )
			{
				file.read( (char*)&id, sizeof(MGSize) );
				endswap( &id);
				++count;
			}
			mtabCell[i].rElem(k) = id;
		}

		bar.Finish();
	}

	cout << "----  count = " << count << endl << endl;

	bar.Init( mtabCell.size() );
	bar.Start();
	for ( MGSize i=0; i<mtabCell.size(); ++i, ++bar)
	{
		if ( mtabCell[i].rElem(0) == mtabCell[i].rElem(1) || mtabCell[i].rElem(0) == mtabCell[i].rElem(2) ||
			 mtabCell[i].rElem(0) == mtabCell[i].rElem(3) || mtabCell[i].rElem(1) == mtabCell[i].rElem(2) ||
			 mtabCell[i].rElem(1) == mtabCell[i].rElem(3) || mtabCell[i].rElem(2) == mtabCell[i].rElem(3) )
		{
			THROW_INTERNAL( " duplicate ids in cell - " << i);
		}

		if ( mtabCell[i].rElem(0) > npo || mtabCell[i].rElem(1) > npo || mtabCell[i].rElem(2) > npo || mtabCell[i].rElem(3) > npo )
		{
			THROW_INTERNAL( " id > max_nnodes in cell - " << i);
		}

	}
	bar.Finish();

	cout << "mtabCell[0]" << endl;
	cout << dec<< mtabCell[0].cElem(0) << "  " << hex << mtabCell[0].cElem(0) << endl;
	cout << dec<< mtabCell[0].cElem(1) << "  " << hex << mtabCell[0].cElem(1) << endl;
	cout << dec<< mtabCell[0].cElem(2) << "  " << hex << mtabCell[0].cElem(2) << endl;
	cout << dec<< mtabCell[0].cElem(3) << "  " << hex << mtabCell[0].cElem(3) << endl;


	cout << "mtabCell[mtabCell.size()-1]" << endl;
	cout << dec<< mtabCell[mtabCell.size()-1].cElem(0) << "  " << hex << mtabCell[mtabCell.size()-1].cElem(0) << endl;
	cout << dec<< mtabCell[mtabCell.size()-1].cElem(1) << "  " << hex << mtabCell[mtabCell.size()-1].cElem(1) << endl;
	cout << dec<< mtabCell[mtabCell.size()-1].cElem(2) << "  " << hex << mtabCell[mtabCell.size()-1].cElem(2) << endl;
	cout << dec<< mtabCell[mtabCell.size()-1].cElem(3) << "  " << hex << mtabCell[mtabCell.size()-1].cElem(3) << endl;


	MGSize lineCheck;
	file.read( (char*)&lineCheck, sizeof(MGSize) );
	endswap( &lineCheck);

	cout << dec << "length = " << llength << " " << lineCheck << endl;

	file.read( (char*)&llength, sizeof(MGSize) );
	endswap( &llength);



	cout << dec;
	for ( MGSize k=0; k<3; ++k)
	{
		bar.Init( mtabNode.size() );
		bar.Start();

		for ( MGSize i=0; i<mtabNode.size(); ++i, ++bar)
		{
			MGFloat x;
			file.read( (char*)&x, sizeof(MGFloat) );
			endswap( &x);
			mtabNode[i].rX(k) = x;
		}

		bar.Finish();
	}

	file.read( (char*)&lineCheck, sizeof(MGSize) );
	endswap( &lineCheck);

	cout << dec << "length = " << llength << " " << lineCheck << endl;


	cout << setprecision(16);
	cout << dec << mtabNode[0].cX(0) << endl;
	cout << dec << mtabNode[1].cX(0) << endl;
	cout << dec << mtabNode[2].cX(0) << endl;
	cout << dec << mtabNode[3].cX(0) << endl;
	cout << dec << mtabNode[4].cX(0) << endl;



	cout << endl;
	cout << "mtabNode[0]" << endl;
	cout << dec << mtabNode[0].cX(0) << endl;
	cout << dec << mtabNode[0].cX(1) << endl;
	cout << dec << mtabNode[0].cX(2) << endl;
	cout << endl;
	cout << "mtabNode[mtabNode.size()-1]" << endl;
	cout << dec << mtabNode[mtabNode.size()-1].cX(0) << endl;
	cout << dec << mtabNode[mtabNode.size()-1].cX(1) << endl;
	cout << dec << mtabNode[mtabNode.size()-1].cX(2) << endl;

	cout << dec;

	/////////////
	MGSize neles = 1 + nc;
	MGSize nelec;


	file.read( (char*)&junk, sizeof(MGSize) );
	endswap( &junk);
	cout << dec << junk << "  " << hex << junk << endl;

	file.read( (char*)&nelec, sizeof(MGSize) );
	endswap( &nelec);
	cout << dec << nelec << "  " << hex << nelec << endl;

	cout << dec;
	if ( nelec == 0 )
		return;  //THROW_INTERNAL( "STOP");

	MGSize nelee = neles + nelec - 1;

	file.read( (char*)&junk, sizeof(MGSize) );
	endswap( &junk);
	cout << dec << junk << "  " << hex << junk << endl;

	file.read( (char*)&llength, sizeof(MGSize) );
	endswap( &llength);
	cout << dec << llength << "  " << hex << llength << endl;

	nPredictedLength = (8 + 4*(nelee-neles-1))*4; // That last 4 is sizeof(int)...
	cout << endl;
	cout << dec << "length = " << llength << " " << nPredictedLength << endl;


	MGSize newn;
	file.read( (char*)&newn, sizeof(MGSize) );
	endswap( &newn);
	cout << dec << newn << "  " << hex << newn << endl;

	file.read( (char*)&newn, sizeof(MGSize) );
	endswap( &newn);
	cout << dec << newn << "  " << hex << newn << endl;

	file.read( (char*)&newn, sizeof(MGSize) );
	endswap( &newn);
	cout << dec << newn << "  " << hex << newn << endl;

	//THROW_INTERNAL( "STOP");
}

void ReadVGRID::ReadBC( istream& file)
{
	istringstream is;

	MGSize	nbf, nbc, npatch, igrid;
	ReadLine( file, is);
	is >> nbf >> nbc >> npatch >> igrid;

	if ( igrid == 1)
		cout << "INVISCID GRID" << endl;
	else if ( igrid == 2)
		cout << "VISCOUS GRID" << endl;
	else
		THROW_INTERNAL("ReadVGRID::ReadBC() - bad grid type");

	ReadLine( file, is);

	mtabBnd.resize( nbf);

	MGSize icur, ipatch, id1, id2, id3;
	for ( MGSize i=0; i<nbf; ++i)
	{
		ReadLine( file, is);
		is >> icur >> ipatch >> id1 >> id2 >> id3;

		mtabBnd[i].first = ipatch;
		mtabBnd[i].second.rElem(0) = id1;
		mtabBnd[i].second.rElem(1) = id2;
		mtabBnd[i].second.rElem(2) = id3;
	}
}


void ReadVGRID::CopyToGrid()
{
	mGrd.IOTabNodeResize( mtabNode.size() );

	vector<MGFloat>	tabx(DIM_3D);

	for ( MGSize i=0; i<mtabNode.size(); ++i)
	{
		for ( MGInt j=0; j<DIM_3D; ++j)
			tabx[j] = mtabNode[i].cX(j);

		mGrd.IOSetNodePos( tabx, i );

	}

	MGSize	ntyp = DIM_3D+1;	// only simplex grid is supported
	vector<MGSize> tabid;

	tabid.resize( ntyp);
	mGrd.IOTabCellResize( ntyp, mtabCell.size() );

	for ( MGSize i=0; i<mtabCell.size(); ++i)
	{
		tabid[0] = mtabCell[i].cElem(0) - 1;
		tabid[1] = mtabCell[i].cElem(1) - 1;
		tabid[2] = mtabCell[i].cElem(2) - 1;
		tabid[3] = mtabCell[i].cElem(3) - 1;
		mGrd.IOSetCellIds( tabid, ntyp, i);
	}


	mGrd.IOTabBFaceResize( 3, mtabBnd.size() );
	tabid.resize( 3 );
	MGSize id = 0;
	for ( MGSize i=0; i<mtabBnd.size(); ++i)
	{
			tabid[0] = mtabBnd[i].second.cElem(0) - 1;
			tabid[1] = mtabBnd[i].second.cElem(1) - 1;
			tabid[2] = mtabBnd[i].second.cElem(2) - 1;
			mGrd.IOSetBFaceIds( tabid, 3, i );
			mGrd.IOSetBFaceSurf( mtabBnd[i].first, 3, i );
	}

}


void ReadVGRID::WriteTEC( const MGString& name)
{
	ProgressBar	bar(40);

	FILE *f = fopen( name.c_str(), "wt" );
	//f = fopen( "out_nodes.dat", "wt" );

	fprintf( f, "VARIABLES = \"X\", \"Y\", \"Z\"");
    fprintf( f, "ZONE T=\"grid\", N=%d, E=%d, F=FEPOINT, ET=TETRAHEDRON\n", (int)mtabNode.size(), (int)mtabCell.size() );

	bar.Init( mtabNode.size() );
	bar.Start();
	for ( MGSize i=0; i<mtabNode.size(); ++i, ++bar)
	{
		fprintf( f, "%24.16lg %24.16lg %24.16lg\n", dco::passive_value(mtabNode[i].cX(0)), dco::passive_value(mtabNode[i].cX(1)), dco::passive_value(mtabNode[i].cX(2)));
	}
	bar.Finish();

	//fclose( f);
	//f = fopen( "out_cells.dat", "wt" );

	bar.Init( mtabCell.size() );
	bar.Start();
	for ( MGSize i=0; i<mtabCell.size(); ++i, ++bar)
	{
		fprintf( f, "%d %d %d %d\n", dco::passive_value(mtabCell[i].cElem(0)), dco::passive_value(mtabCell[i].cElem(1)), dco::passive_value(mtabCell[i].cElem(2)), dco::passive_value(mtabCell[i].cElem(3) ));
	}
	bar.Finish();

	fclose( f);
}



void ReadVGRID::DoRead( const MGString& fname)
{
	if ( mGrd.Dim() != DIM_3D )
		THROW_INTERNAL( "ReadVGRID::DoRead() - bad dim");

	MGString fcogsg = fname + MGString(".cogsg");
	MGString fbc = fname + MGString(".bc");

	ifstream file_cogsg( fcogsg.c_str(), ios::in | ios::binary);
	ifstream file_bc( fbc.c_str(), ios::in );

	try
	{
		if ( ! file_cogsg)
			THROW_FILE( "can not open the file", fcogsg);

		if ( ! file_bc)
			THROW_FILE( "can not open the file", fbc);

		cout << "Reading VGRID '" << fname << "' :" << endl;
		cout.flush();

		//ReadCOGSG( file_cogsg);
		ReadCOGSG_multi( file_cogsg);
		ReadBC( file_bc);

		WriteTEC( "outc.dat");

		CopyToGrid();

		cout <<  "Reading VGRID - FINISHED" << endl;
	}
	catch ( EHandler::Except& e)
	{
		cout << "ERROR: ReadVGRID::DoRead() - file name: " << fname << endl << endl;
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);

		THROW_INTERNAL("END");
	}
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

