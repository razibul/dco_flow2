#include "writetecsurf.h"
#include "libcoreio/store.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace IOSpace {



	
template <>
void WriteTECSurf::ConvertBFaceIds<DIM_2D>( MGString& sbuf, const vector<MGSize>& tabid, const MGSize& n) const
{
	ostringstream os;

	switch ( n)
	{
	case 2:	
		os << tabid[0]+1 << " " << tabid[1]+1;
		break;

	default:
		THROW_INTERNAL( "WriteTECSurf<DIM_2D>::ConvertBFaceIds - unrecognized vface type");
	}

	sbuf = os.str();
}


template <>
void WriteTECSurf::ConvertBFaceIds<DIM_3D>( MGString& sbuf, const vector<MGSize>& tabid, const MGSize& n) const
{
	ostringstream os;

	switch ( n)
	{
	case 3:	// triangle
		os << tabid[0]+1 << " " << tabid[1]+1 << " " << tabid[2]+1 << " " << tabid[2]+1;
		break;

	case 4:	// quadrilateral
		os << tabid[0]+1 << " " << tabid[1]+1 << " " << tabid[2]+1 << " " << tabid[3]+1;
		break;

	default:
		THROW_INTERNAL( "WriteTECSurf<DIM_2D>::ConvertBFaceIds - unrecognized bface type");
	}

	sbuf = os.str();
}

template <Dimension DIM>
void WriteTECSurf::ConvertBFaceIds( MGString& sbuf, const vector<MGSize>& tabid, const MGSize& n) const
{
}



template <>
void WriteTECSurf::WriteHeader<DIM_2D>( ostringstream& obuf, const MGString& title, const MGSize& neq, const MGSize& nnode, const MGSize& nbface)
{
	obuf << "TITLE = \"" << title << "\"" << endl;
	obuf << "VARIABLES = ";
	obuf << "\"X\", \"Y\", \"VnX\", \"VnY\"";
	//file << "\"X\", \"Y\"";

	for ( MGSize i=0; i<neq; ++i)
		obuf << ", \"V" << setw(2) << setfill('0') << i+1 << "\"";
	obuf << endl;

	obuf << "ZONE T=\"" << title << "\", N=" << nnode << ", E=" << nbface 
		 << ", F=FEPOINT, ET=LINESEG\n";


	//file << "TITLE = \"" << title << "\"" << endl;
	//file << "VARIABLES = ";
	//file << "\"X\", \"Y\", \"VnX\", \"VnY\"";
	////file << "\"X\", \"Y\"";

	//for ( MGSize i=0; i<neq; ++i)
	//	file << ", \"V" << setw(2) << setfill('0') << i+1 << "\"";
	//file << endl;

	//file << "ZONE T=\"" << title << "\", N=" << nnode << ", E=" << nbface 
	//	 << ", F=FEPOINT, ET=LINESEG\n";
}


template <>
void WriteTECSurf::WriteHeader<DIM_3D>( ostringstream& obuf, const MGString& title, const MGSize& neq, const MGSize& nnode, const MGSize& nbface)
{
	obuf << "TITLE = \"" << title << "\"" << endl;
	obuf << "VARIABLES = ";
	obuf << "\"X\", \"Y\", \"Z\", \"VnX\", \"VnY\", \"VnZ\"";
	//file << "\"X\", \"Y\", \"Z\"";

	for ( MGSize i=0; i<neq; ++i)
		obuf << ", \"V" << setw(2) << setfill('0') << i+1 << "\"";
	obuf << endl;

	obuf << "ZONE T=\"" << title << "\", N=" << nnode << ", E=" << nbface 
		 << ", F=FEPOINT, ET=QUADRILATERAL\n";


	//file << "TITLE = \"" << title << "\"" << endl;
	//file << "VARIABLES = ";
	//file << "\"X\", \"Y\", \"Z\", \"VnX\", \"VnY\", \"VnZ\"";
	////file << "\"X\", \"Y\", \"Z\"";

	//for ( MGSize i=0; i<neq; ++i)
	//	file << ", \"V" << setw(2) << setfill('0') << i+1 << "\"";
	//file << endl;

	//file << "ZONE T=\"" << title << "\", N=" << nnode << ", E=" << nbface 
	//	 << ", F=FEPOINT, ET=QUADRILATERAL\n";
}


template <Dimension DIM>
void WriteTECSurf::WriteTECascii_( ostream& file)
{
	ostringstream obuf;

	MGString	sbuf;

	MGSize neq = 0;
	MGSize nbface = 0;

	for ( MGSize i=0; i<mGrd.IONumBFaceTypes(); ++i)
		nbface += mGrd.IOSizeBFaceTab( mGrd.IOBFaceType( i) );
	
	if ( mpSol)
	{
		if ( mpSol->IOSize() != mGrd.IOSizeNodeTab() )
			THROW_INTERNAL( "Incompatible vector sizes" );

		neq = mpSol->IOBlockSize();
	}

	WriteHeader<DIM>( obuf, mTitle, neq, mGrd.IOSizeBNodeTab(), nbface);



	// node coordiantes and states

	vector<MGFloat> tabState(neq);
	vector<MGFloat>	tabx(DIM);
	MGSize	idpar;

	for ( MGSize i=0; i<mGrd.IOSizeBNodeTab(); ++i)
	{
		mGrd.IOGetBNodePos( tabx, i);
		for ( MGSize k=0; k<DIM; ++k)
			obuf << setprecision(16) << tabx[k] << " ";

		mGrd.IOGetBNodeVn( tabx, i);
		for ( MGSize k=0; k<DIM; ++k)
			obuf << setprecision(16) << tabx[k] << " ";

		if ( mpSol)
		{
			mGrd.IOGetBNodePId( idpar, i);
			mpSol->IOGetBlockVector( tabState, idpar );
			mpSol->UndimToDim( tabState );

			for ( MGSize k=0; k<neq; ++k)
				obuf << setprecision(16) << tabState[k] << " ";
				//file << setprecision(8) << 0.0 << " ";
		}

		obuf << endl;
	}


	// cells

	vector<MGSize> tabid;

	MGSize nb = mGrd.IONumBFaceTypes();

	for ( MGSize k=0; k<nb; ++k)
	{
		MGSize ntyp = mGrd.IOBFaceType( k);
		MGSize n = mGrd.IOSizeBFaceTab( ntyp);
		tabid.resize(ntyp);

		for ( MGSize i=0; i<n; ++i)
		{
			mGrd.IOGetBFaceBIds( tabid, ntyp, i);

			ConvertBFaceIds<DIM>( sbuf, tabid, ntyp);

			obuf << sbuf << endl;
		}
	}

	file.write( obuf.str().c_str() , obuf.str().size()*sizeof(char) );

}

template <class T>
MGSize search_vector( vector<T>& tab, const T& val)
{
	pair< typename vector<T>::iterator, typename vector<T>::iterator > res = equal_range( tab.begin(), tab.end(), val);

	if ( res.second - res.first < 1)
	{
		THROW_INTERNAL( "search_vector - value not found!");
	}
	else if ( res.second - res.first != 1)
	{
		THROW_INTERNAL( "search_vector - more then one vaule found!\n");
	}

	return (MGSize)(res.first-tab.begin());
}


template <Dimension DIM>
void WriteTECSurf::WriteTECascii( ostream& file)
{
	bool bMerge = false; // merge zones or not


	MGString	sbuf;

	MGSize neq = 0;

	if ( mpSol)
	{
		if ( mpSol->IOSize() != mGrd.IOSizeNodeTab() )
			THROW_INTERNAL( "Incompatible vector sizes" );

		neq = mpSol->IOBlockSize();
	}


	const MGSize nb = mGrd.IONumBFaceTypes();

	vector<MGSize>	tabsurf;

	// find how many surfaces exist
	for ( MGSize k=0; k<nb; ++k)
	{
		const MGSize ntyp = mGrd.IOBFaceType( k);
		const MGSize n = mGrd.IOSizeBFaceTab( ntyp);
		MGSize surfid;

		for ( MGSize i=0; i<n; ++i)
		{
			mGrd.IOGetBFaceSurf( surfid, ntyp, i);

			if ( ! binary_search( tabsurf.begin(), tabsurf.end(), surfid) )
			{
				tabsurf.push_back( surfid);
				sort( tabsurf.begin(), tabsurf.end() );
			}
		}
	}

	// create map of global node id to zone id
	MGSize nsurf = tabsurf.size();
	if ( bMerge)
		nsurf = 1;

	vector< vector<MGSize> >	tabmap;
	tabmap.resize( nsurf);

	vector<MGSize>	tabnbface( nsurf);

	vector<MGSize> tabid;

	for ( MGSize isurf=0; isurf<nsurf; ++isurf)
	//for ( MGSize isurf=0; isurf<1; ++isurf)
	{
		vector<MGSize>& tab = tabmap[isurf];

		tabnbface[isurf] = 0;

		for ( MGSize k=0; k<nb; ++k)
		{
			const MGSize ntyp = mGrd.IOBFaceType( k);
			const MGSize n = mGrd.IOSizeBFaceTab( ntyp);
			MGSize surfid;

			tabid.resize(ntyp);

			for ( MGSize i=0; i<n; ++i)
			{
				mGrd.IOGetBFaceSurf( surfid, ntyp, i);

				//if ( surfid == tabsurf[isurf] )
				if ( surfid == tabsurf[isurf] || bMerge )
				{
					mGrd.IOGetBFaceIds( tabid, ntyp, i);
					
					for ( MGSize j=0; j<ntyp; ++j)
						tab.push_back( tabid[j]);
					
					++(tabnbface[isurf]);
				}
			}
		}

		sort( tab.begin(), tab.end() );
		vector<MGSize>::iterator itrend = unique( tab.begin(), tab.end() );
		tab.erase( itrend, tab.end() );
	}


	ostringstream obuf;


	for ( MGSize isurf=0; isurf<nsurf; ++isurf)
	//for ( MGSize isurf=0; isurf<1; ++isurf)
	{
		obuf << endl;

		ostringstream os;
		os << "surf " << tabsurf[isurf];
		sbuf = os.str();
		WriteHeader<DIM>( obuf, sbuf, neq, tabmap[isurf].size(), tabnbface[isurf]);

		vector<MGSize>& tab = tabmap[isurf];

		//FILE *f = fopen( "qqq.txt", "wt");
		//for ( MGSize i=0; i<tab.size(); ++i)
		//	fprintf( f, "%d %d\n", i, tab[i] );
		//fclose( f);

		// nodes
		vector<MGFloat> tabState(neq);
		vector<MGFloat>	tabx(DIM);

		for ( MGSize i=0; i<tab.size(); ++i)
		{
			mGrd.IOGetNodePos( tabx, tab[i]);
			for ( MGSize k=0; k<DIM; ++k)
				obuf << setprecision(16) << tabx[k] << " ";

			mGrd.IOGetBNodeVn( tabx, i);
			for ( MGSize k=0; k<DIM; ++k)
				obuf << setprecision(16) << tabx[k] << " ";

			if ( mpSol)
			{
				mpSol->IOGetBlockVector( tabState, tab[i] );
				mpSol->UndimToDim( tabState );

				for ( MGSize k=0; k<neq; ++k)
					obuf << setprecision(16) << tabState[k] << " ";
			}

			obuf << endl;		
		}

		// cells
		for ( MGSize k=0; k<nb; ++k)
		{
			const MGSize ntyp = mGrd.IOBFaceType( k);
			const MGSize n = mGrd.IOSizeBFaceTab( ntyp);
			MGSize surfid;

			tabid.resize(ntyp);

			for ( MGSize i=0; i<n; ++i)
			{
				mGrd.IOGetBFaceSurf( surfid, ntyp, i);

				//if ( surfid == tabsurf[isurf] )
				if ( surfid == tabsurf[isurf] || bMerge )
				{
					mGrd.IOGetBFaceIds( tabid, ntyp, i);
					
					for ( MGSize j=0; j<ntyp; ++j)
						tabid[j] = search_vector( tab, tabid[j] );

					ConvertBFaceIds<DIM>( sbuf, tabid, ntyp);

					obuf << sbuf << endl;
					
				}
			}
		}

	}

	file.write( obuf.str().c_str() , obuf.str().size()*sizeof(char) );

	
/*
	for ( MGSize k=0; k<nb; ++k)
	{
		const MGSize ntyp = mGrd.IOBFaceType( k);
		const MGSize n = mGrd.IOSizeBFaceTab( ntyp);

		vector<MGSize>& tab = tabmap[k];

		tabid.resize(ntyp);

		for ( MGSize i=0; i<n; ++i)
		{
			mGrd.IOGetBFaceIds( tabid, ntyp, i);

			for ( MGSize j=0; j<ntyp; ++j)
				tab.push_back( tabid[j] );
		}

		sort
	}
*/
}


inline void WriteTECSurf::DoWrite( ostream& file )
{
	if ( mpSol)
		if ( mpSol->Dim() != mGrd.Dim() )
			THROW_INTERNAL( "WriteTECSurf::DoWrite() - Incompatible dimension of grid and solution");

	if ( mGrd.Dim() == DIM_2D )
	{
		WriteTECascii_<DIM_2D>( file); // with normals - HEAVY !!!
		//WriteTECascii<DIM_2D>( file);
	}
	else if ( mGrd.Dim() == DIM_3D )
	{
		//WriteTECascii_<DIM_3D>( file); // with normals - HEAVY !!!
		WriteTECascii<DIM_3D>( file);
	}
	else
		THROW_INTERNAL( "WriteTECSurf::DoWrite - bad dim");

}


void WriteTECSurf::DoWrite( const MGString& fname)
{
	ofstream file( fname.c_str(), ios::out);

	try
	{
		if ( ! file)
			THROW_FILE( "can not open the file", fname);

		cout << "Writing TEC surf " << fname ;
		cout.flush();

		DoWrite( file);
		cout <<  " - FINISHED" << endl;
	}
	catch ( EHandler::Except& e)
	{
		cout << "ERROR: WriteTECSurf::DoWrite - file name: " << fname << endl << endl;
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);

		THROW_INTERNAL("END");
	}
}






} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

