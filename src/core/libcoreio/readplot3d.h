#ifndef __IO_READPLOT3D_H__
#define __IO_READPLOT3D_H__

#include "libcoresystem/mgdecl.h"
#include "libcorecommon/key.h"
#include "libcoregeom/dimension.h"
#include "libcoregeom/vect.h"

#include "libcoreio/gridfacade.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace IOSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

template<Dimension DIM>
struct StrCellSize
{
};

template<>
struct StrCellSize<DIM_2D>
{
	enum { size = 4 };
};

template<>
struct StrCellSize<DIM_3D>
{
	enum { size = 6 };
};

//////////////////////////////////////////////////////////////////////
// class ReadTEC
//////////////////////////////////////////////////////////////////////

template<Dimension DIM>
class ReadPlot3D
{
public:
	ReadPlot3D( GridReadFacade& grd) : mGrd(grd)	{}

	void	DoRead( const MGString& fname);
	void	DoRead( istream& file);

protected:
	void	Read( istream& file);
	void	Stitch();
	void	CopyToGrid();
	void	RemoveDuplicatePoints( const MGFloat& zero = 1.0e-12);
	
	void	ExtractBoundary();
	void	ExtractBoundary(vector< vector<MGSize> >& tabsurfnode);

private:
	MGSize	NodeId(const MGSize x, const MGSize y, const MGSize z, const MGSize& blockid);
	void	RemoveDuplicatePointsStr(vector< vector<MGSize> >& tabsurfnode);

	GridReadFacade&		mGrd;

	MGString			mFileTitle;

	MGSize	mBlockN;
	vector< Geom::Vect<DIM, MGSize> > mtabBlockN;

	vector< Vect<DIM> >						mtabNode;
	vector< Key<DIM+1> >					mtabCell;
	vector< Key<StrCellSize<DIM>::size> >	mtabstrcell;
	vector< vector<Key<DIM> > >				mtabBnd;
};




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace IO = IOSpace;


#endif // __IO_READPLOT3D_H__
