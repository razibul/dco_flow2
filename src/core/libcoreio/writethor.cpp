
#include "writethor.h"
#include "libcoreio/store.h"
#include "libcorecommon/key.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace IOSpace {


//void WriteTHOR::InitBFaceCells()
//{
//	map< Key<DIM_3D>, MGSize>    mapbface;
//
//	for ( MGSize i=0; i<SizeBFaceTab(); ++i)
//	{
//		mtabBFace[i].rCellId() = SizeCellTab() + 1;
//		CGeomBFace<DIM> face;
//		GetBFace( i, face);
//
//		Key<DIM> key;
//		for ( MGSize in=0; in<DIM; ++in)
//			key.rElem(in) = face.cId(in);
//
//		key.Sort();
//
//		if ( mapbface.find( key) == mapbface.end() )
//			mapbface.insert( typename map< Key<DIM>, MGSize>::value_type( key, i) );
//		else
//			THROW_INTERNAL( "bface already exists inside the 'mapbface'");
//
//	}
//
//	for ( MGSize i=0; i<SizeCellTab(); ++i)
//	{
//		CGeomCell<DIM>   gcell;
//		GetCell( i, gcell);
//
//		for ( MGSize ifc=0; ifc<gcell.SizeFace(); ++ifc)
//		{
//			Key<DIM> facekey;
//			for ( MGSize in=0; in<DIM; ++in)
//				facekey.rElem(in) = gcell.cId( gcell.cFaceConn(ifc,in) );
//
//			facekey.Sort();
//
//			typename map< Key<DIM>, MGSize>::iterator  itr;
//
//			if ( (itr = mapbface.find( facekey)) !=  mapbface.end() )
//				mtabBFace[itr->second].rCellId() = i;
//		}
//	}
//
//}


void WriteTHOR::DoWrite( ostream& file)
{
	const MGSize nbbface = mGrd.IONumBFaceTypes();
	const MGSize nbcell = mGrd.IONumCellTypes();

	if ( nbbface != 1 || nbcell != 1)
		THROW_INTERNAL( "WriteTHOR::DoWrite -- only simplex grid is supported" );

	MGSize ncell=0;
	for ( MGSize it=0; it<nbcell; ++it)
		ncell += mGrd.IOSizeCellTab( mGrd.IOCellType(it) );

	vector<MGSize>	tabsurf;
	MGSize nbface=0;
	for ( MGSize k=0; k<nbbface; ++k)
	{
		const MGSize ntyp = mGrd.IOBFaceType( k);
		const MGSize n = mGrd.IOSizeBFaceTab( ntyp);
		nbface += n;
		MGSize surfid;

		for ( MGSize i=0; i<n; ++i)
		{
			mGrd.IOGetBFaceSurf( surfid, ntyp, i);

			if ( ! binary_search( tabsurf.begin(), tabsurf.end(), surfid) )
			{
				tabsurf.push_back( surfid);
				sort( tabsurf.begin(), tabsurf.end() );
			}
		}
	}

	vector<MGSize>	tabcount( tabsurf.size(), 0 );
	for ( MGSize k=0; k<nbbface; ++k)
	{
		const MGSize ntyp = mGrd.IOBFaceType( k);
		const MGSize n = mGrd.IOSizeBFaceTab( ntyp);
		MGSize surfid;

		for ( MGSize i=0; i<n; ++i)
		{
			mGrd.IOGetBFaceSurf( surfid, ntyp, i);
			
			MGSize ip = (MGSize)( lower_bound( tabsurf.begin(), tabsurf.end(), surfid ) - tabsurf.begin() );
			tabcount[ip] ++;
		}
	}

	// find neighbour cell for bfaces
	map< Key<3>, MGSize > mapface;
	vector<MGInt>	tabneib( nbface, -1 );
	MGSize id = 0;
	for ( MGSize k=0; k<nbbface; ++k)
	{
		const MGSize ntyp = mGrd.IOBFaceType( k);
		const MGSize n = mGrd.IOSizeBFaceTab( ntyp);

		vector<MGSize> tabid( ntyp );

		for ( MGSize i=0; i<n; ++i)
		{
			mGrd.IOGetBFaceIds( tabid, ntyp, i);

			Key<3> key(0,0,0);
			for ( MGSize k=0; k<tabid.size(); ++k)
				key.rElem(k) = tabid[k];
			key.Sort();

			mapface[key] = id;
			
			++id;
		}
	}

	id = 0;
	for ( MGSize it=0; it<nbcell; ++it)
	{
		const MGSize ntyp = mGrd.IOCellType( it);
		const MGSize n = mGrd.IOSizeCellTab( ntyp);

		vector<MGSize> tabid( ntyp );

		for ( MGSize ic=0; ic<n; ++ic)
		{
			mGrd.IOGetCellIds( tabid, ntyp, ic);

			for ( MGSize ifac=0; ifac<tabid.size(); ++ifac)
			{
				Key<3> key(0,0,0);
				for ( MGSize ielem=0; ielem<tabid.size()-1; ++ielem)
					key.rElem(ielem) = tabid[ielem];
				key.Sort();

				map< Key<3>, MGSize >::iterator itr;
				if ( (itr=mapface.find( key)) != mapface.end() )
					tabneib[itr->second] = (MGInt)id;

				rotate( tabid.begin(), tabid.begin()+1, tabid.end() );
			}

			++id;
		}
	}

	for ( MGSize i=0; i< tabneib.size(); ++i)
		if ( tabneib[i] < 0 )
			THROW_INTERNAL( "WriteTHOR::DoWrite -- face neighbours search FIALED");

	// write
	file << mGrd.Dim() << " 0 0" << endl;
	file << ncell << " " << mGrd.IOSizeNodeTab() << " " << nbface << " " << tabsurf.size() << endl;

	// cells
	file << nbcell << endl;

	for ( MGSize it=0; it<nbcell; ++it)
	{
		const MGSize ntyp = mGrd.IOCellType( it);
		const MGSize n = mGrd.IOSizeCellTab( ntyp);

		file << ntyp << " " << n << endl;

		vector<MGSize> tabid( ntyp );

		for ( MGSize ic=0; ic<n; ++ic)
		{
			mGrd.IOGetCellIds( tabid, ntyp, ic);
			file << tabid[0]+1;
			for ( MGSize k=1; k<tabid.size(); ++k)
				file << " " << tabid[k]+1;
			file << endl;
		}
	}

	// bfaces
	for ( MGSize ip=0; ip<tabsurf.size(); ++ip)
	{
		file << tabsurf[ip] << " " << tabcount[ip] << endl;

		MGSize faceid = 0;
		for ( MGSize k=0; k<nbbface; ++k)
		{
			const MGSize ntyp = mGrd.IOBFaceType( k);
			const MGSize n = mGrd.IOSizeBFaceTab( ntyp);
			MGSize surfid;

			vector<MGSize> tabid( ntyp );

			for ( MGSize i=0; i<n; ++i)
			{
				mGrd.IOGetBFaceSurf( surfid, ntyp, i);
			
				if ( surfid == tabsurf[ip] )
				{
					mGrd.IOGetBFaceIds( tabid, ntyp, i);
					file << tabneib[faceid]+1 << " " << ntyp << " ";
					for ( MGSize k=0; k<tabid.size(); ++k)
						file << tabid[k]+1 << " ";
					file << endl;
				}

				++faceid;
			}
		}
	}

	// write update tab
	MGSize colsize = 10;
	for ( MGSize in=0; in<mGrd.IOSizeNodeTab(); )
	{
		for ( MGSize icol=0; icol<colsize && in<mGrd.IOSizeNodeTab(); ++icol, ++in)
			file << setw(8) << in+1 << " ";
		file << endl;
	}

	colsize = 4;
	vector<MGFloat> tabx( mGrd.Dim(), 0.0);
	for ( MGSize idim=0; idim<static_cast<MGSize>( mGrd.Dim()); ++idim)
		for ( MGSize in=0; in<mGrd.IOSizeNodeTab(); )
		{
			for ( MGSize icol=0; icol<colsize && in<mGrd.IOSizeNodeTab(); ++icol, ++in)
			{
				mGrd.IOGetNodePos( tabx, in);
				file << setw(20) << setprecision(10) << scientific << tabx[idim] << " ";
			}
			file << endl;
		}

}


void WriteTHOR::DoWrite( const MGString& fname)
{
	ofstream ofile;

	ofile.open( fname.c_str(), ios::out );

	try
	{
		if ( ! ofile)
			THROW_FILE( "can not open the file", fname);

		cout << "Writing THOR " << fname;
		cout.flush();

		DoWrite( ofile);
		cout <<  " - FINISHED" << endl;

	}
	catch ( EHandler::Except& e)
	{
		cout << "ERROR: WriteTHOR::DoWrite - file name: " << fname << endl << endl;
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);

		THROW_INTERNAL("END");
	}
}

	
} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
