#ifndef __IO_FACADE_H__
#define __IO_FACADE_H__

#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"


using namespace Geom;


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace IOSpace {



//////////////////////////////////////////////////////////////////////
// class DimFacade
//////////////////////////////////////////////////////////////////////
class DimFacade
{
public:
	virtual Dimension	Dim() const = 0;
};

//////////////////////////////////////////////////////////////////////
// class WriteFacade
//////////////////////////////////////////////////////////////////////
class WriteFacade : virtual public DimFacade
{
public:
	//virtual void	IOPrepareWriting() 	{}
	//virtual void	IOFinalizeWriting()	{}

	virtual void	IOGetName( MGString& name) const = 0;
};

//////////////////////////////////////////////////////////////////////
// class ReadFacade
//////////////////////////////////////////////////////////////////////
class ReadFacade : virtual public DimFacade
{
public:
	//virtual void	IOPrepareReading()	{}
	//virtual void	IOFinalizeReading()	{}

	virtual void	IOSetName( const MGString& name) = 0;
};

	
	
	

} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace IO = IOSpace;


#endif // __IO_FACADE_H__

