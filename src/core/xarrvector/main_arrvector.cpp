#include "mgdecl.h"
#include "arrvector.h"


int main()
{
	cout << "Hello" << endl;

	MGSize size = 14 * 65536 + 121;
	MGSize usize = size >> 16;
	//MGSize lsize = (size << 16 ) >> 16;
	MGSize lsize = size & 0x0000ffff;

	//usize >>= 16;
	//lsize <<=  16;

	cout << "MGSize = " <<  sizeof(MGSize) << endl;
	cout << "size_t = " << sizeof(size_t) << endl;
	cout << endl;

	cout << size << endl;
	cout << usize << endl;
	cout << lsize << endl;

	MGSize umask = 0xffff0000;
	MGSize lmask = 0x0000ffff;

	lmask = 0;
	lmask = ~((~lmask) << 16 );

	cout << "umask = " << umask << endl;
	cout << "lmask = " << lmask << endl;
	cout << ((size & umask) >> 16) << " "  << (MGSize)(size & lmask) << endl;

	cout << "-----------------------------" << endl;

	arrvector<int,2>	tab;
	arrvector<int,2>::iterator	itr;
	arrvector<int,2>::const_iterator	citr;

	cout << tab.master_index( size) << endl;
	cout << tab.inner_index( size) << endl;
	cout << tab.chunk_size() << endl;


	tab.resize( 17);

	for ( MGSize i=0; i<tab.size(); ++i)
		tab[i] = i+1;

	for ( MGSize i=0; i<tab.size(); ++i)
		cout << tab[i] << " " ;
	cout << endl;
	cout << endl;

	tab.dump();

	cout << endl;
	cout << endl;

	tab.resize( 22);

	for ( MGSize i=0; i<tab.size(); ++i)
		tab[i] = i+1;

	for ( MGSize i=0; i<tab.size(); ++i)
		cout << tab[i] << " " ;
	cout << endl;
	cout << endl;

	tab.dump();

	cout << endl;
	cout << endl;

	tab.resize( 7);

	for ( MGSize i=0; i<tab.size(); ++i)
		tab[i] = tab.size() - i;

	for ( MGSize i=0; i<tab.size(); ++i)
		cout << tab[i] << " " ;
	cout << endl;
	cout << endl;

	tab.dump();

	cout << endl;
	cout << endl;

	////////////////////////////////////////////////
	// front / back

	cout << tab.front() << endl;
	cout << tab.back() << endl;

	////////////////////////////////////////////////
	// const_iterator / iterator

	cout << "== iterator ==============" << endl;

	for ( citr=tab.begin(); citr!=tab.end(); ++citr)
		cout << *citr << " ";
	cout << endl;

	for ( itr=tab.begin(); itr!=tab.end(); ++itr)
		cout << *itr << " ";
	cout << endl;

	////////////////////////////////////////////////
	// push_back

	tab.push_back( 11);
	tab.push_back( 3);
	tab.push_back( 9);

	for ( citr=tab.begin(); citr!=tab.end(); ++citr)
		cout << *citr << " ";
	cout << endl;

	////////////////////////////////////////////////
	// sort

	std::sort( tab.begin(), tab.end() );

	for ( citr=tab.begin(); citr!=tab.end(); ++citr)
		cout << *citr << " ";
	cout << endl;

	////////////////////////////////////////////////
	// pop_back

	tab.pop_back();
	tab.pop_back();
	tab.pop_back();

	for ( citr=tab.begin(); citr!=tab.end(); ++citr)
		cout << *citr << " ";
	cout << endl;

	return 0;
}