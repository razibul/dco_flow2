#include <time.h> 

#include "libcoresystem/mgdecl.h"
#include "libcorecommon/fgarrvector.h"
#include "libcorecommon/arrbitset.h"



int main()
{
	cout << "Hello" << endl;

	int nmax = 100000000;
	clock_t		start, finish;
	double		totaltime = 0, duration; 
	double		div = CLOCKS_PER_SEC;

/*
	////////////////////////
	arrbitset<2>	tabit;

	tabit.resize( 11);

	tabit.set( 0, true);
	tabit.set( 1, false); 
	tabit.set( 2, true);
	tabit.set( 3, true);
	tabit.set( 4, false);
	tabit.set( 5, false);
	tabit.set( 6, true);
	tabit.set( 7, true);
	tabit.set( 8, false);
	tabit.set( 9, false);
	tabit.set( 10, true);

	for ( MGSize i=0; i<tabit.size(); ++i)
		cout << setw(3) << i << "  " << tabit[i] << endl;
	cout << endl;

	tabit.resize(7);
	//tabit.resize(7,false);

	for ( MGSize i=0; i<tabit.size(); ++i)
		cout << setw(3) << i << "  " << tabit[i] << endl;
	cout << endl;

	tabit.dump();

	return 0;

	////////////////////////
	fgarrvector<int,12>	ftab;

	start = clock(); // start timing 
	for ( int i=0; i<nmax; ++i)
	{
		ftab.insert( rand() );
	}

	finish = clock();	// end timing
	duration = (double)(finish - start) / CLOCKS_PER_SEC; 

	cout << "fgarrvector<int,18> time = " << setprecision(8) << duration << endl;

	////////////////////////
	vector<int>	itab;

	start = clock(); // start timing 
	for ( int i=0; i<nmax; ++i)
	{
		itab.push_back( rand() );
	}

	finish = clock();	// end timing
	duration = (double)(finish - start) / CLOCKS_PER_SEC; 

	cout << "vector<int> time = " << setprecision(8) << duration << endl;


	int k;

	cin >> k;

	return 0;
	////////////////////////
*/

	fgarrvector<int,2>	tab;
	fgarrvector<int,2>::iterator itr;
	fgarrvector<int,2>::const_iterator citr;

	tab.insert( 3);
	tab.insert( 5);
	tab.insert( 7);
	tab.insert( 1);
	tab.dump();
	cout << endl;

	tab.erase( 2);
	tab.erase( 3);
	tab.dump();
	cout << endl;

	for ( itr = tab.begin(); itr != tab.end(); ++itr)
		cout << *itr << " ";
	cout << endl << endl;

	tab.insert( 15);
	tab.insert( 17);
	tab.insert( 11);
	tab.insert( 21);
	tab.insert( 22);
	tab.insert( 24);
	tab.erase( 7);
	tab.dump();
	cout << endl;

	for ( citr = tab.begin(); citr != tab.end(); ++citr)
		cout << *citr << " ";
	cout << endl << endl;

/*
	MGSize size = 14 * 65536 + 121;
	MGSize usize = size >> 16;
	//MGSize lsize = (size << 16 ) >> 16;
	MGSize lsize = size & 0x0000ffff;

	//usize >>= 16;
	//lsize <<=  16;

	cout << "MGSize = " <<  sizeof(MGSize) << endl;
	cout << "size_t = " << sizeof(size_t) << endl;
	cout << endl;

	cout << size << endl;
	cout << usize << endl;
	cout << lsize << endl;

	MGSize umask = 0xffff0000;
	MGSize lmask = 0x0000ffff;

	lmask = 0;
	lmask = ~((~lmask) << 16 );

	cout << "umask = " << umask << endl;
	cout << "lmask = " << lmask << endl;
	cout << ((size & umask) >> 16) << " "  << (MGSize)(size & lmask) << endl;

	cout << "-----------------------------" << endl;

	arrvector<int,2>	tab;
	arrvector<int,2>::iterator	itr;
	arrvector<int,2>::const_iterator	citr;

	cout << tab.master_index( size) << endl;
	cout << tab.inner_index( size) << endl;
	cout << tab.chunk_size() << endl;


	tab.resize( 17);

	for ( MGSize i=0; i<tab.size(); ++i)
		tab[i] = i+1;

	for ( MGSize i=0; i<tab.size(); ++i)
		cout << tab[i] << " " ;
	cout << endl;
	cout << endl;

	tab.dump();

	cout << endl;
	cout << endl;

	tab.resize( 22);

	for ( MGSize i=0; i<tab.size(); ++i)
		tab[i] = i+1;

	for ( MGSize i=0; i<tab.size(); ++i)
		cout << tab[i] << " " ;
	cout << endl;
	cout << endl;

	tab.dump();

	cout << endl;
	cout << endl;

	tab.resize( 7);

	for ( MGSize i=0; i<tab.size(); ++i)
		tab[i] = tab.size() - i;

	for ( MGSize i=0; i<tab.size(); ++i)
		cout << tab[i] << " " ;
	cout << endl;
	cout << endl;

	tab.dump();

	cout << endl;
	cout << endl;

	////////////////////////////////////////////////
	// front / back

	cout << tab.front() << endl;
	cout << tab.back() << endl;

	////////////////////////////////////////////////
	// const_iterator / iterator

	cout << "== iterator ==============" << endl;

	for ( citr=tab.begin(); citr!=tab.end(); ++citr)
		cout << *citr << " ";
	cout << endl;

	for ( itr=tab.begin(); itr!=tab.end(); ++itr)
		cout << *itr << " ";
	cout << endl;

	////////////////////////////////////////////////
	// push_back

	tab.push_back( 11);
	tab.push_back( 3);
	tab.push_back( 9);

	for ( citr=tab.begin(); citr!=tab.end(); ++citr)
		cout << *citr << " ";
	cout << endl;

	////////////////////////////////////////////////
	// sort

	std::sort( tab.begin(), tab.end() );

	for ( citr=tab.begin(); citr!=tab.end(); ++citr)
		cout << *citr << " ";
	cout << endl;

	////////////////////////////////////////////////
	// pop_back

	tab.pop_back();
	tab.pop_back();
	tab.pop_back();

	for ( citr=tab.begin(); citr!=tab.end(); ++citr)
		cout << *citr << " ";
	cout << endl;
*/

	return 0;
}