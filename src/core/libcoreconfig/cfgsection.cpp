#include "cfgsection.h"
#include "libcoreio/store.h"


const char STRCFG_SECTION[]			= "SECTION";
const char STRCFG_ENDSEC[]			= "END_SEC";

const char STRCFG_SUBSECTION[]		= "SUBSECTION";
const char STRCFG_ENDSUBSEC[]		= "END_SUBSEC";

const char STRCFG_GROUP[]			= "GROUP";
const char STRCFG_ENDGROUP[]		= "END_GROUP";

const char STRCFG_SUBGROUP[]		= "SUBGROUP";
const char STRCFG_ENDSUBGROUP[]		= "END_SUBGROUP";

const char STRCFG_NULL[]			= "NULL";



MGString GetSectionName( const MGSize& level)
{
	switch( level)
	{
	case 0:
		return STRCFG_SECTION;

	case 1:
		return STRCFG_SUBSECTION;

	case 2:
		return STRCFG_GROUP;

	case 3:
		return STRCFG_SUBGROUP;

	default:
		return STRCFG_NULL;
	}
}

MGString GetEndSectionName( const MGSize& level)
{
	switch( level)
	{
	case 0:
		return STRCFG_ENDSEC;

	case 1:
		return STRCFG_ENDSUBSEC;

	case 2:
		return STRCFG_ENDGROUP;

	case 3:
		return STRCFG_ENDSUBGROUP;

	default:
		return STRCFG_NULL;
	}
}


//////////////////////////////////////////////////////////////////////
// CfgSection
//////////////////////////////////////////////////////////////////////

bool CfgSection::ReadCFG( istream& f, const MGSize& level)
{
	istringstream	is;

	MGString	sbuf, skey, sid;
	MGString	ssec, sendsec, ssubsec;

	//switch( level)
	//{
	//case 0:
	//	ssec = STRCFG_SECTION;
	//	sendsec = STRCFG_ENDSEC;
	//	ssubsec = STRCFG_SUBSECTION;
	//	break;

	//case 1:
	//	ssec = STRCFG_SUBSECTION;
	//	sendsec = STRCFG_ENDSUBSEC;
	//	ssubsec = STRCFG_GROUP;
	//	break;

	//case 2:
	//	ssec = STRCFG_GROUP;
	//	sendsec = STRCFG_ENDGROUP;
	//	ssubsec = STRCFG_NULL;
	//	break;

	//default:
	//	THROW_INTERNAL( "bad section level inside CfgSection::ReadCFG()" );
	//}

	ssec = GetSectionName( level);
	sendsec = GetEndSectionName( level);
	ssubsec = GetSectionName( level+1);


	mbReady = false;

	if ( level == 0 )
	{
		while ( ! f.eof())
		{
			IO::ReadLine( f, is);
			is >> skey >> sbuf >> sid;

			if ( skey == ssec )
			{
				mName = sbuf;
				mId = sid;
				break;
			}
		}
	}

	while ( ! f.eof())
	{
		skey = STRCFG_NULL;
		IO::ReadLine( f, is);
		is >> skey;
		getline( is, sbuf);


		if ( skey == STRCFG_NULL )
			continue;

		if ( skey == ssubsec )
		{
			IO::StripBlanks( sbuf);
			istringstream	istmp;
			istmp.str(sbuf);

			CfgSection subsec;
			istmp >> subsec.mName >> subsec.mId;
			//subsec.mName = sbuf;

			if ( subsec.ReadCFG( f, level+1) )
				mmapSubSec.insert( map<MGString,CfgSection>::value_type( subsec.mName, subsec) );
		}
		else
		{
			if ( skey != sendsec  )
			{
				IO::StripBlanks( sbuf);
				mMap.insert( map<MGString,MGString>::value_type( skey, sbuf ) );
			}
			else
			{
				mLevel = level;
				mbReady = true;
				break;
			}
		}
	}

	return mbReady;
}



void CfgSection::WriteCFG( ostream& f) const
{
	MGString space, inspace;
	for ( MGSize i=0; i<mLevel; ++i)
		space += "    ";

	f << endl;
	f << space << GetSectionName( mLevel) << " " << mName << " " << mId << endl;

	inspace = space + "    ";

	for ( map<MGString,MGString>::const_iterator itr=mMap.begin(); itr!= mMap.end(); ++itr)
		f << inspace << (*itr).first << "     " << (*itr).second << endl;


	for ( multimap<MGString,CfgSection>::const_iterator i=mmapSubSec.begin(); i!=mmapSubSec.end(); ++i)
		(*i).second.WriteCFG( f);

	f << space << GetEndSectionName( mLevel) << endl;
}



void CfgSection::DumpCFG( ostream& f) const
{
	f << "level " << mLevel << " : " << mName << " " << mId << endl;

	for ( map<MGString,MGString>::const_iterator itr=mMap.begin(); itr!= mMap.end(); ++itr)
		f << (*itr).first << " :    " << (*itr).second << endl;

	if ( mLevel == 0) 
		f << "-- subsections --" << endl;

	for ( multimap<MGString,CfgSection>::const_iterator i=mmapSubSec.begin(); i!=mmapSubSec.end(); ++i)
		(*i).second.WriteCFG( f);

	f << endl;
}


void CfgSection::Clear()
{
	mName = "";
	mMap.clear();
	mbReady = false;
}


