#include "config.h"
#include "libcoreio/store.h"



//const char	STRCFG_BIN[]	= "bin";
//const char	STRCFG_ASCII[]	= "ascii";

//const char	STRCFG_EXPLICIT[]	= "EXPLICIT";
//const char	STRCFG_IMPLICIT[]	= "IMPLICIT";




//////////////////////////////////////////////////////////////////////
// Config
//////////////////////////////////////////////////////////////////////
MGString Config::mstName = "FLOWSOL_CFG";
MGString Config::mstVersion = "0.3";


void Config::ReadCFG( const MGString& name)
{
	istringstream	is;
	MGString	sbuf, sname, sver;

	CfgSection	sec;

	ifstream	f( name.c_str(), ios::in);

	try
	{
		if ( ! f)
			THROW_FILE( "can not open the file", name);

		while ( ! f.eof())
		{
			IO::ReadLine( f, is);
			is >> sname >> sver;
			if ( sname == mstName )
				break;
		}

		if ( sname != mstName || sver != mstVersion )
		{
			ostringstream os;
			os << "incompatible config version (current ver: '" << mstName << " " << mstVersion << "')";
			THROW_INTERNAL( os.str() );
		}

		while ( sec.ReadCFG( f) )
		{
			mmapSec.insert( map<MGString,CfgSection>::value_type( sec.Name(), sec) );
			sec.Clear();
		}

	}
	catch ( EHandler::Except& e)
	{
		cout << "Config::ReadCFG - file name: " << name << endl << endl;
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);

		THROW_INTERNAL("END");
	}


	cout << "Config ready" << endl << endl;
}


void Config::WriteCFG( const MGString& name) const
{
	ofstream	f( name.c_str(), ios::out);

	f << mstName << " " << mstVersion << endl;

	for ( multimap<MGString,CfgSection>::const_iterator i=mmapSec.begin(); i!=mmapSec.end(); ++i)
		(*i).second.WriteCFG( f);
}


void Config::DumpCFG( const MGString& name) const
{
	ofstream	f( name.c_str(), ios::out);

	for ( multimap<MGString,CfgSection>::const_iterator i=mmapSec.begin(); i!=mmapSec.end(); ++i)
		(*i).second.WriteCFG( f);
}




