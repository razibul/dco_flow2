#ifndef __CONFIGBASE_H__
#define __CONFIGBASE_H__

#include "libcoreconfig/config.h"



//////////////////////////////////////////////////////////////////////
// class ConfigBase
//////////////////////////////////////////////////////////////////////
class ConfigBase
{
public:
	ConfigBase() : mpCfgSect( NULL)	{}
	ConfigBase( const CfgSection& cfgsec) : mpCfgSect( &cfgsec )	{}

	virtual void Create( const CfgSection* pcfgsec);
	virtual void PostCreateCheck() const;

	virtual	~ConfigBase()	{}

//protected:
	const CfgSection&	ConfigSect() const			{ return *mpCfgSect; }
	bool				ConfigSectIsValid() const	{ if (mpCfgSect) return true; return false; }

private:
	const CfgSection*	mpCfgSect;
};
//////////////////////////////////////////////////////////////////////



inline void ConfigBase::Create( const CfgSection* pcfgsec)		
{ 
	mpCfgSect = pcfgsec;
}


inline void ConfigBase::PostCreateCheck() const
{ 
	if ( ! mpCfgSect )
		THROW_INTERNAL( "ConfigBase::PostCreateCheck() -- failed" );

}




#endif // __CONFIGBASE_H__
