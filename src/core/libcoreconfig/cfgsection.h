#ifndef __CFGSECTION_H__
#define __CFGSECTION_H__

#include "libcoresystem/mgdecl.h"



//////////////////////////////////////////////////////////////////////
// class CfgSection
//////////////////////////////////////////////////////////////////////
class CfgSection
{
	friend class Config;

public:
	typedef map<MGString,MGString>::const_iterator			const_iterator_rec;
	typedef multimap<MGString,CfgSection>::const_iterator	const_iterator_sec;

	CfgSection() : mbReady(false), mLevel(0)	{}

	bool	ReadCFG( istream& f, const MGSize& level = 0 );
	void	WriteCFG( ostream& f) const;
	void	DumpCFG( ostream& f) const;

	const MGString&		Name()	const	{ return mName;}
	const MGString&		Id()	const	{ return mId;}


	MGSize				GetSectionCount( const MGString& name) const;
	bool				SectionExists( const MGString& key) const; 
	const CfgSection&	GetSection( const MGString& name, const MGSize& id = 0) const;

	bool				KeyExists( const MGString& key) const; 
	const MGString&		ValueString( const MGString& key) const;
	bool				ValueBool( const MGString& key) const;
	MGSize				ValueSize( const MGString& key) const;
	MGFloat				ValueFloat( const MGString& key) const;
	vector<MGSize>		ValueVectorID( const MGString& key) const;
	vector<MGFloat>		ValueVectorFLOAT( const MGString& key) const;

	const_iterator_rec	begin_rec() const	{ return mMap.begin();}
	const_iterator_rec	end_rec() const		{ return mMap.end();}

	const_iterator_sec	begin_sec() const	{ return mmapSubSec.begin();}
	const_iterator_sec	end_sec() const		{ return mmapSubSec.end();}

protected:
	void	Clear();

private:
	bool			mbReady;
	MGSize			mLevel;

	MGString		mName;
	MGString		mId;

	map<MGString,MGString>			mMap;
	multimap<MGString,CfgSection>	mmapSubSec;
};
//////////////////////////////////////////////////////////////////////


inline MGSize CfgSection::GetSectionCount( const MGString& name) const
{
	return mmapSubSec.count( name);
}


inline bool CfgSection::SectionExists( const MGString& name) const
{
	if ( GetSectionCount( name) > 0 )
		return true;

	return false;
}


inline bool CfgSection::KeyExists( const MGString& key) const
{
	if ( mMap.find( key) != mMap.end() )
		return true;

	return false;
}


inline const CfgSection& CfgSection::GetSection( const MGString& name, const MGSize& id) const
{
	multimap<MGString,CfgSection>::const_iterator citr;

	//for ( citr=mmapSubSec.begin(); citr!=mmapSubSec.end(); ++citr)
	//{
	//	MGString aaa = (*citr).first;
	//	cout << (*citr).first << endl;
	//}

	if ( (citr = mmapSubSec.find( name)) == mmapSubSec.end() )
	{
		THROW_INTERNAL( MGString( "section " + name + " not found") );
	}

	for ( MGSize i=0; i<id; ++i)
		++citr;

	if ( citr->first != name )
		THROW_INTERNAL( "section '" << name << "' with icount = " << id << " not found" );

	return (*citr).second; 
}



inline const MGString& CfgSection::ValueString( const MGString& key) const	
{ 
	map<MGString,MGString>::const_iterator citr;
	if ( (citr = mMap.find( key)) == mMap.end() )
	{
		ostringstream os;
		os << "key = " << key << " not found in " << mName << " section";
		cout << os.str() << endl; cout. flush();
		THROW_INTERNAL( os.str() );
	}

	return (*citr).second; 
}


inline bool CfgSection::ValueBool( const MGString& key) const
{
	return ::StrToBool( ValueString(key).c_str() );
}

inline MGSize CfgSection::ValueSize( const MGString& key) const
{
	MGSize	size;
	istringstream is( ValueString(key) );
	is >> size;
	return size;
}

inline MGFloat CfgSection::ValueFloat( const MGString& key) const
{
	MGFloat	d;
	istringstream is( ValueString(key) );
	is >> d;
	return d;
}


inline vector<MGSize> CfgSection::ValueVectorID( const MGString& key) const
{
	vector<MGSize> tab;
	MGString buf = ValueString( key);
	istringstream is( ValueString(key) );

	do
	{
		MGSize id;
		is >> id;
		tab.push_back( id);
	}
	while ( ! is.eof() );

	return tab;
}


inline vector<MGFloat> CfgSection::ValueVectorFLOAT( const MGString& key) const
{
	vector<MGFloat> tab;
	MGString buf = ValueString( key);
	istringstream is( ValueString(key) );

	do
	{
		MGFloat val;
		is >> val;
		tab.push_back( val);
	}
	while ( ! is.eof() );

	return tab;
}



#endif // __CFGSECTION_H__
