#ifndef __CONFIG_H__
#define __CONFIG_H__

#include "libcoresystem/mgdecl.h"
#include "libcoreconfig/cfgsection.h"



//////////////////////////////////////////////////////////////////////
// class Config
//////////////////////////////////////////////////////////////////////
class Config
{
public:
	void	ReadCFG( const MGString& name);
	void	WriteCFG( const MGString& name) const;
	void	DumpCFG( const MGString& name) const;

	MGSize				GetSectionCount( const MGString& name) const;

	const CfgSection&	GetSection( const MGString& name, const MGSize& id = 0) const;

private:
	static MGString	mstName;
	static MGString	mstVersion;

	multimap<MGString,CfgSection>	mmapSec;
};
//////////////////////////////////////////////////////////////////////


inline MGSize Config::GetSectionCount( const MGString& name) const
{
	return mmapSec.count( name);
}


inline const CfgSection& Config::GetSection( const MGString& name, const MGSize& id) const
{
	multimap<MGString,CfgSection>::const_iterator citr;
	if ( (citr = mmapSec.find( name)) == mmapSec.end() )
	{
		THROW_INTERNAL( MGString( "section " + name + " not found") );
	}

	for ( MGSize i=0; i<id; ++i)
		++citr;

	if ( citr->first != name )
		THROW_INTERNAL( "section '" << name << "' with icount = " << id << " not found" );

	return (*citr).second; 
}





#endif // __CONFIG_H__
