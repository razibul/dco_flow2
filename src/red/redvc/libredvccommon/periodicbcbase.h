#ifndef __PERIODICBCBASE_H__
#define __PERIODICBCBASE_H__

#include "libcorecommon/key.h"
#include "libredcore/equation.h"
//#include "libextsparse/blockvector.h"
#include "libcoreconfig/configbase.h"
#include "libredcore/configconst.h"
//#include "libcoregeom/vect.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


	template<MGSize I>
	struct Int2Type
	{
	};

	enum PrdBCType {
		PRD_TRANS,
		PRD_ANGLE,
		PRD_NONE
	};

	PrdBCType	StrToPrd(const MGString& str);
	MGString BCToStr(const PrdBCType& prd);

	//////////////////////////////////////////////////////////////////////
	// class PeriodicKeyData
	//////////////////////////////////////////////////////////////////////

	template<Geom::Dimension DIM>
	struct PeriodicKeyData
	{
		PeriodicKeyData() : mcellid(0)	{}

		MGSize				mcellid;
		Key<DIM, MGFloat>	mtabcoord;

		MGSize				mdatasetid;
	};

	struct PeriodicData
	{
		PeriodicData(
			const MGSize& s, const MGSize& t,
			const Geom::Vect3D& v,
			const Geom::Vect3D& pos, const MGFloat& ang,
			const PrdBCType& typ) : src(s), trg(t), vct(v), type(typ), axispos(pos), angle(ang)	{}

		PrdBCType		type;
		MGSize			src;
		MGSize			trg;
		Geom::Vect3D vct;
		Geom::Vect3D	axispos;
		MGFloat			angle;
		MGFloat			tol;
		bool			isabstol;
	};

	//////////////////////////////////////////////////////////////////////
	// class PeriodicBCBase
	//////////////////////////////////////////////////////////////////////

	template<Geom::Dimension DIM, EQN_TYPE ET>
	class PeriodicBCBase
	{
		enum { ESIZE = EquationDef<DIM, ET>::SIZE };

		typedef EquationDef<DIM, ET>		EqDef;
		typedef typename EqDef::FVec		FVec;
		

	public:

		virtual void GetPeriodicVar(const MGSize& id, FVec& v) const = 0;
		
		void ReadConfig(const CfgSection* pphysect);

	protected:
		typedef PeriodicData				PrdData;

		vector< PrdData> mparams;
	}; 

	template<Geom::Dimension DIM, EQN_TYPE ET>
	void PeriodicBCBase<DIM, ET>::ReadConfig(const CfgSection* pphysect)
	{
		MGSize prdsize = pphysect->GetSectionCount(ConfigStr::Solver::Physics::PERIODIC::NAME);

		for (MGSize i = 0; i < prdsize; ++i)
		{
			const CfgSection& secprd = pphysect->GetSection(ConfigStr::Solver::Physics::PERIODIC::NAME, i);
			MGSize srcid = secprd.ValueSize(ConfigStr::Solver::Physics::PERIODIC::PRD_SRC_ID);
			MGSize trgid = secprd.ValueSize(ConfigStr::Solver::Physics::PERIODIC::PRD_TRG_ID);
			MGString type = secprd.ValueString(ConfigStr::Solver::Physics::PERIODIC::Type::KEY);

			PrdBCType prdtype = StrToPrd(type);
			switch (prdtype)
			{
			case PRD_TRANS:
			{
				//MGFloat trans[] = {
				//	secprd.ValueFloat(ConfigStr::Solver::Physics::PERIODIC::),
				//	secprd.ValueFloat(ConfigStr::Solver::Physics::PERIODIC::VCT_Y),
				//	secprd.ValueFloat(ConfigStr::Solver::Physics::PERIODIC::VCT_Z)
				//};
				vector<MGFloat> tmp = secprd.ValueVectorFLOAT(ConfigStr::Solver::Physics::PERIODIC::PRD_TRANS_VCT);
				Geom::Vect3D vct;
				for (MGSize j = 0; j < DIM; ++j)
				{
					vct.rX(j) = tmp[j];
				}

				mparams.push_back(PrdData(srcid, trgid, vct, Geom::Vect3D(), 0., prdtype));

				break;
			}
			case PRD_ANGLE:
			{
				//MGFloat axis[] = {
				//	secprd.ValueFloat(ConfigStr::Solver::Physics::PERIODIC::VCT_X),
				//	secprd.ValueFloat(ConfigStr::Solver::Physics::PERIODIC::VCT_Y),
				//	secprd.ValueFloat(ConfigStr::Solver::Physics::PERIODIC::VCT_Z)
				//};
				//Geom::Vect<DIM> vct;
				//for (MGSize j = 0; j < DIM; ++j)
				//{
				//	vct.rX(j) = trans[j];
				//}
				vector<MGFloat> tmporg = secprd.ValueVectorFLOAT(ConfigStr::Solver::Physics::PERIODIC::PRD_AXIS_ORIG);
				Geom::Vect3D axisorig;
				for (MGSize j = 0; j < Geom::DIM_3D; ++j)
				{
					axisorig.rX(j) = tmporg[j];
				}
				vector<MGFloat> tmpdir = secprd.ValueVectorFLOAT(ConfigStr::Solver::Physics::PERIODIC::PRD_AXIS_DIR);
				Geom::Vect3D axisdir;
				for (MGSize j = 0; j < Geom::DIM_3D; ++j)
				{
					axisdir.rX(j) = tmpdir[j];
				}
				MGFloat angle = secprd.ValueFloat(ConfigStr::Solver::Physics::PERIODIC::PRD_ALPHA);
				mparams.push_back(PrdData(srcid, trgid, axisdir, axisorig, angle, prdtype));
				break;
			}
			case PRD_NONE:
				//THROW_INTERNAL("Angle periodic bc not implemented.");
				break;
			default:
				THROW_INTERNAL("Unknown periodic bc type.");
				break;
			}

			bool _isabstol = secprd.KeyExists(ConfigStr::Solver::Physics::PERIODIC::ABSTOL);
			MGFloat _tol;
			if (_isabstol)
			{
				_tol = secprd.ValueFloat(ConfigStr::Solver::Physics::PERIODIC::ABSTOL);
			}
			else
			{
				_tol = secprd.ValueFloat(ConfigStr::Solver::Physics::PERIODIC::RELEDGETOL);
			}
			mparams.back().isabstol = _isabstol;
			mparams.back().tol = _tol;
		}
	}

	//////////////////////////////////////////////////////////////////////
	// class PeriodicOp
	//////////////////////////////////////////////////////////////////////

	template<Geom::Dimension DIM>
	struct PeriodicOp
	{
		static void ApplyPrd(const PeriodicData& data, const Geom::Vect<DIM>& pos, Geom::Vect<DIM>& newpos, Int2Type<PRD_TRANS>);
		static void ApplyPrd(const PeriodicData& data, const Geom::Vect<DIM>& pos, Geom::Vect<DIM>& newpos, Int2Type<PRD_ANGLE>);

	private:
		static void RotX(const PeriodicData& data, const Geom::Vect<DIM>& pos, Geom::Vect<DIM>& newpos);
		static void RotY(const PeriodicData& data, const Geom::Vect<DIM>& pos, Geom::Vect<DIM>& newpos);
		static void RotZ(const PeriodicData& data, const Geom::Vect<DIM>& pos, Geom::Vect<DIM>& newpos);
		static void RotAny(const PeriodicData& data, const Geom::Vect<DIM>& pos, Geom::Vect<DIM>& newpos); //NEED TESTING
	};

	template<Geom::Dimension DIM>
	void PeriodicOp<DIM>::ApplyPrd(const PeriodicData& data, const Geom::Vect<DIM>& pos, Geom::Vect<DIM>& newpos, Int2Type<PRD_TRANS>)
	{
		for (MGSize j = 0; j < DIM; ++j)
		{
			newpos.rX(j) = pos.cX(j) + data.vct.cX(j);
		}
	}

	template<Geom::Dimension DIM>
	void PeriodicOp<DIM>::ApplyPrd(const PeriodicData& data, const Geom::Vect<DIM>& pos, Geom::Vect<DIM>& newpos, Int2Type<PRD_ANGLE>)
	{
		if (data.axispos.module() == 0)
		{
			if ((data.vct.cX() == 0) && (data.vct.cY() == 0))
			{
				RotZ(data, pos, newpos);
			}
			else if ((data.vct.cY() == 0) && (data.vct.cZ() == 0))
			{
				RotX(data, pos, newpos);
			}
			else if ((data.vct.cX() == 0) && (data.vct.cZ() == 0))
			{
				RotY(data, pos, newpos);
			}
		}
		else
		{
			RotAny(data, pos, newpos);
		}
	}

	template<Geom::Dimension DIM>
	void PeriodicOp<DIM>::RotY(const PeriodicData& data, const Geom::Vect<DIM>& pos, Geom::Vect<DIM>& newpos)
	{
		Geom::Vect<Geom::DIM_3D> npos;
		MGFloat alfa = data.angle*PI / 180.;

		vector<MGFloat> x(3, 0);
		for (MGSize i = 0; i < DIM; ++i)
		{
			x[i] = pos.cX(i);
		}

		npos.rX() = x[0]*cos(alfa) + x[2]*sin(alfa);
		npos.rZ() = -x[0]*sin(alfa) + x[2]*cos(alfa);
		npos.rY() = x[1];

		for (MGSize i = 0; i < DIM; ++i)
		{
			newpos.rX(i) = npos.cX(i);
		}
	}

	template<Geom::Dimension DIM>
	void PeriodicOp<DIM>::RotX(const PeriodicData& data, const Geom::Vect<DIM>& pos, Geom::Vect<DIM>& newpos)
	{
		Geom::Vect<Geom::DIM_3D> npos;
		MGFloat alfa = data.angle*PI / 180.;

		vector<MGFloat> x(3, 0);
		for (MGSize i = 0; i < DIM; ++i)
		{
			x[i] = pos.cX(i);
		}

		npos.rY() = x[1] * cos(alfa) - x[2] * sin(alfa);
		npos.rZ() = x[1] * sin(alfa) + x[2] * cos(alfa);
		npos.rX() = x[0];

		for (MGSize i = 0; i < DIM; ++i)
		{
			newpos.rX(i) = npos.cX(i);
		}
	}

	template<Geom::Dimension DIM>
	void PeriodicOp<DIM>::RotZ(const PeriodicData& data, const Geom::Vect<DIM>& pos, Geom::Vect<DIM>& newpos)
	{
		Geom::Vect<Geom::DIM_3D> npos;
		MGFloat alfa = data.angle*PI / 180.;

		vector<MGFloat> x(3, 0);
		for (MGSize i = 0; i < DIM; ++i)
		{
			x[i] = pos.cX(i);
		}

		npos.rX() = x[0] * cos(alfa) - x[1] * sin(alfa);
		npos.rY() = x[0] * sin(alfa) + x[1] * cos(alfa);
		npos.rZ() = x[2];

		for (MGSize i = 0; i < DIM; ++i)
		{
			newpos.rX(i) = npos.cX(i);
		}
	}

	template<Geom::Dimension DIM>
	void PeriodicOp<DIM>::RotAny(const PeriodicData& data, const Geom::Vect<DIM>& pos, Geom::Vect<DIM>& newpos)
	{
		MGFloat a = data.axispos.cX(),
			b = data.axispos.cY(),
			c = data.axispos.cZ();

		MGFloat u = data.vct.cX(),
			v = data.vct.cY(),
			w = data.vct.cZ();

		vector<MGFloat> x(3, 0);
		for (MGSize i = 0; i < DIM; ++i)
		{
			x[i] = pos.cX(i);
		}

		MGFloat alfa = data.angle*PI / 180.;

		Geom::Vect<Geom::DIM_3D> npos;
		npos.rX() = (a*(v*v + w*w) - u*(b*v + c*w - u*x[0] - v*x[1] - w*x[2]))*(1 - cos(alfa)) + x[0] * cos(alfa) + (-c*v + b*w - w*x[1] + v*x[2])*sin(alfa);
		npos.rY() = (b*(u*u + w*w) - v*(a*u + c*w - u*x[0] - v*x[1] - w*x[2]))*(1 - cos(alfa)) + x[1] * cos(alfa) + (c*u - a*w + w*x[0] - u*x[2])*sin(alfa);
		npos.rZ() = (c*(v*v + u*u) - w*(a*u + b*v - u*x[0] - v*x[1] - w*x[2]))*(1 - cos(alfa)) + x[2] * cos(alfa) + (-b*u + a*v - v*x[0] + u*x[1])*sin(alfa);

		for (MGSize i = 0; i < DIM; ++i)
		{
			newpos.rX(i) = npos.cX(i);
		}
	}
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __PERIODICBCBASE_H__