#ifndef __TEMPDATABLOCK_H__
#define __TEMPDATABLOCK_H__


#include "libredcore/equation.h" 
#include "libredphysics/physics.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Geom::Dimension DIM, EQN_TYPE ET> 
class TempDataBlock
{
};


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


#endif // __TEMPDATABLOCK_H__
