#ifndef __AUXDATA_H__
#define __AUXDATA_H__

#include "libcoreconfig/configbase.h"
#include "libredphysics/physics.h"

#include "libextsparse/sparsevector.h"

#include "libredcore/processinfo.h"
#include "libcoreio/store.h"
#include "libcoreio/readsol.h"
#include "libcoreio/writesol.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
// class AuxData
//////////////////////////////////////////////////////////////////////
template <MGSize SIZE>
class AuxData : public IO::SolFacade, public ConfigBase
{
public:
	AuxData() : ConfigBase(), mDim(DIM_NONE)		{}

	virtual void Create(  const Dimension& dim, const CfgSection* pcfgsec);
	virtual void PostCreateCheck() const;
	virtual void Init();

	const Sparse::BlockVector<SIZE>&	operator [] ( const  MGSize& i) const	{ return mvecQ[i]; }
	Sparse::BlockVector<SIZE>&			operator [] ( const  MGSize& i)			{ return mvecQ[i]; }

	MGSize		Size() const	{ return mvecQ.Size(); }

	void		Resize( const MGSize& n)	{ return mvecQ.Resize( n); }

protected:
	void	CheckIsNAN( const Sparse::Vector<SIZE>& vec);
	void	CheckQ( const Sparse::Vector<SIZE>& vec);

// AuxData Facade
protected:
	virtual Dimension	Dim() const		{ return mDim;}

	virtual void	IOGetName( MGString& name) const	{ name = mName;}
	virtual void	IOSetName( const MGString& name)	{ mName = name;}

	virtual void	DimToUndim( vector<MGFloat>& tab ) const	{  }
	virtual void	UndimToDim( vector<MGFloat>& tab ) const	{  }
 
	virtual MGSize	IOSize() const		{ return Size();}			
	virtual MGSize	IOBlockSize() const	{ return SIZE;}

	virtual void	IOResize( const MGSize& n)			{ mvecQ.Resize( n); }

	virtual void	IOGetBlockVector( vector<MGFloat>& tabx, const MGSize& id) const;
	virtual void	IOSetBlockVector( const vector<MGFloat>& tabx, const MGSize& id );

 
private:
	Dimension				mDim;
	MGString				mName; 

	Sparse::Vector<SIZE>	mvecQ; 
};
//////////////////////////////////////////////////////////////////////


template <MGSize SIZE>
inline void AuxData<SIZE>::CheckIsNAN( const Sparse::Vector<SIZE>& vec)
{
	for ( MGSize i=0; i<vec.Size(); ++i)
		for ( MGSize j=0; j<SIZE; ++j)
			IS_INFNAN_THROW( vec[i](j), "ERROR in AuxData<SIZE>::CheckIsNAN()");
}


template <MGSize SIZE>
inline void AuxData<SIZE>::CheckQ( const Sparse::Vector<SIZE>& vec)
{
	//rSpaceSol().IsPhysical( vec);
} 

 
template <MGSize SIZE>
inline void AuxData<SIZE>::IOGetBlockVector( vector<MGFloat>& tabx, const MGSize& id) const
{
	if ( tabx.size() != SIZE)
		THROW_INTERNAL( "AuxData<SIZE>::IOGetBlockVector - tabx.size() != SIZE");

	for ( MGSize k=0; k<SIZE; ++k)
		tabx[k] = mvecQ[id](k);
}


template <MGSize SIZE>
inline void AuxData<SIZE>::IOSetBlockVector( const vector<MGFloat>& tabx, const MGSize& id )
{
	if ( tabx.size() != SIZE)
		THROW_INTERNAL( "AuxData<SIZE>::IOSetBlockVector - tabx.size() != SIZE");

	for ( MGSize k=0; k<SIZE; ++k)
		mvecQ[id](k) = tabx[k];
}


template <MGSize SIZE>
inline void AuxData<SIZE>::Create( const Dimension& dim, const CfgSection* pcfgsec)		
{
	mDim = dim;
	ConfigBase::Create( pcfgsec);
}


template <MGSize SIZE>
inline void AuxData<SIZE>::PostCreateCheck() const
{ 
	ConfigBase::PostCreateCheck();
}


template <MGSize SIZE>
inline void AuxData<SIZE>::Init()
{ 
	bool		bBIN = IO::FileTypeToBool(  ConfigSect().ValueString( ConfigStr::Solver::AuxData::FTYPE ) );
	MGString	fname = ConfigSect().ValueString( ConfigStr::Solver::AuxData::FNAME );

	fname = ProcessInfo::ModifyFileName( fname);
	
	IO::ReadSOL	reader( *this);

	reader.DoRead( fname, bBIN );
} 



//////////////////////////////////////////////////////////////////////
// class AuxData - empty specialization
//////////////////////////////////////////////////////////////////////
template <>
class AuxData<0> : public ConfigBase
{
public:
	virtual void Create( const Dimension& dim, const CfgSection* pcfgsec)		{ ConfigBase::Create( pcfgsec); }
	virtual void PostCreateCheck() const										{ ConfigBase::PostCreateCheck(); }

	virtual void Init()		{}

	void		Resize( const MGSize& n)	{}

	const Sparse::BlockVector<0>&	operator [] ( const  MGSize& i) const	{ return mDummy; }

private:
	Sparse::BlockVector<0>	mDummy;
};

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __AUXDATA_H__
