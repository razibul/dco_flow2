#include "solgradbuilder.h"
#include "libcorecommon/factory.h"
#include "libredcore/configconst.h"
#include "libcoreconfig/cfgsection.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Geom::Dimension DIM, EQN_TYPE ET>
bool SolGradBuilder<DIM,ET>::Do()
{
	//cout << "echo from SolGradBuilder<DIM,ET>::Do()" << endl;

	if ( this->cData().cSolGradient().Size() != this->cData().cGrid().SizeNodeTab() )
		THROW_INTERNAL( "this->cData().cSolGradient().Size() != this->cData().cGrid().SizeNodeTab()" );

	MGSize n = this->cData().cGrid().SizeNodeTab();

	vector<MGFloat>	tabOm( n, 0.0 );

	for ( MGSize i=0; i<this->cData().cGrid().SizeNodeTab(); ++i)
		for ( MGSize ie=0; ie<ESIZE; ++ie)
			for ( MGSize k=0; k<DIM; ++k)
				this->rData().rSolGradient()[i][ie].rX(k) = 0.0; 



	CFCell	cfcell;
	for ( MGSize i=0; i<this->cData().cGrid().SizeCellTab(); ++i)
	{
		this->GetFCell( cfcell, i);

		MGFloat vol = cfcell.Volume();

		for ( MGSize k=0; k<cfcell.Size(); ++k)
			tabOm[ cfcell.cId(k) ] += vol;


		for ( MGSize ie=0; ie<ESIZE; ++ie)
		{
			GVec vgrad = cfcell.GradVarOmega( GetVar(ie) );

			for ( MGSize k=0; k<cfcell.Size(); ++k)
				this->rData().rSolGradient()[ cfcell.cId(k) ](ie) += vgrad; 
		}
	}

	for ( MGSize i=0; i<n; ++i)
		for ( MGSize ie=0; ie<ESIZE; ++ie)
		{
			if ( tabOm[i] < 1.0e-20 )
				THROW_INTERNAL( "SolGradBuilder<DIM,ET> -- tabOm["<<i<<"] < 1.0e-20");

			this->rData().rSolGradient()[ i ][ie] *= ( 1.0/( tabOm[i] ) );
		}

	return true;
}


void init_SolGradBuilder()
{

	static ConcCreator< 
		MGString, 
		SolGradBuilder<Geom::DIM_2D, EQN_POISSON>,
		AssistantBase<DIM_2D, EQN_POISSON>
	>	gCreatorSolGradBuilderPoisson2D( "SOLGRADBUILDER_2D_POISSON");

	static ConcCreator< 
		MGString, 
		SolGradBuilder<Geom::DIM_2D, EQN_NS>,
		AssistantBase<DIM_2D, EQN_NS>
	>	gCreatorSolGradBuilderNS2D( "SOLGRADBUILDER_2D_NS");

	static ConcCreator< 
		MGString, 
		SolGradBuilder<Geom::DIM_3D, EQN_NS>,
		AssistantBase<DIM_3D, EQN_NS>
	>	gCreatorSolGradBuilderNS3D( "SOLGRADBUILDER_3D_NS");

	static ConcCreator< 
		MGString, 
		SolGradBuilder<Geom::DIM_2D, EQN_RANS_SA>,
		AssistantBase<DIM_2D, EQN_RANS_SA>
	>	gCreatorSolGradBuilderRansSA2D( "SOLGRADBUILDER_2D_RANS_SA");

	static ConcCreator< 
		MGString, 
		SolGradBuilder<Geom::DIM_3D, EQN_RANS_SA>,
		AssistantBase<DIM_3D, EQN_RANS_SA>
	>	gCreatorSolGradBuilderRansSA3D( "SOLGRADBUILDER_3D_RANS_SA");

}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 
