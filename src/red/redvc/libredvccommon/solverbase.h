#ifndef __SOLVERBASE_H__
#define __SOLVERBASE_H__

#include "libcoreconfig/configbase.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
// SolverBase
//////////////////////////////////////////////////////////////////////
class SolverBase : public ConfigBase
{
public:
	static MGString	Info()	{ return "SolverBase"; }

	SolverBase()	{}
	SolverBase( const CfgSection& cfgsec) : ConfigBase( cfgsec )	{}
	virtual ~SolverBase()	{}

	virtual void	Create( const CfgSection* pcfgsec)	{ ConfigBase::Create( pcfgsec); }
	virtual void	PostCreateCheck() const				{ ConfigBase::PostCreateCheck(); }
	virtual void	Init()		= 0;

	virtual void	Solve()		= 0;
};
//////////////////////////////////////////////////////////////////////


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __SOLVERBASE_H__
