#ifndef __SOLGRADIENT_H__
#define __SOLGRADIENT_H__

#include "libcoreconfig/configbase.h"
#include "libredphysics/physics.h"

#include "libextsparse/sparsevector.h"

#include "libcoreio/store.h"
#include "libcoreio/readsol.h"
#include "libcoreio/writesol.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
// class SolGradient
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, EQN_TYPE ET>
class SolGradient : /*public IO::SolFacade, */public ConfigBase
{
public:
	enum { ESIZE = EquationDef<DIM,ET>::SIZE };
	typedef EquationDef<DIM,ET>		EqDef;
	//typedef typename EqDef::FVec	FVec;
	//typedef typename EqDef::AVec	AVec;
	typedef Geom::Vect<DIM>			GVec;


	SolGradient() : ConfigBase()		{}

	virtual void Create( const CfgSection* pcfgsec, const Physics<DIM,ET>* pphys);
	virtual void PostCreateCheck() const;
	virtual void Init();

	const SVector< ESIZE, GVec >&	operator [] ( const  MGSize& i) const	{ return mtabGrad[i]; }
	SVector< ESIZE, GVec >&			operator [] ( const  MGSize& i)			{ return mtabGrad[i]; }

	MGSize		Size() const	{ return mtabGrad.size(); }

	void		Resize( const MGSize& n)	{ mtabGrad.resize( n); }

//	void		CheckNAN() const			{ CheckIsNAN( mvecQ); }
//
//
//protected:
//	void	CheckIsNAN( const Sparse::Vector<ESIZE>& vec) const;
//	void	CheckQ( const Sparse::Vector<ESIZE>& vec) const;

// SolGradient Facade
protected:
	virtual Dimension	Dim() const						{ return DIM;}

	//virtual void	IOGetName( MGString& name) const	{ name = mSolName;}
	//virtual void	IOSetName( const MGString& name)	{ mSolName = name;}

	//virtual void	DimToUndim( vector<MGFloat>& tab ) const;//	{ mpPhysics->DimToUndim( tab); }
	//virtual void	UndimToDim( vector<MGFloat>& tab ) const;//	{ mpPhysics->UndimToDim( tab); }
 //
	//virtual MGSize	IOSize() const						{ return Size();}			
	//virtual MGSize	IOBlockSize() const					{ return ESIZE;}

	//virtual void	IOResize( const MGSize& n)			{ mvecQ.Resize( n); }

	//virtual void	IOGetBlockVector( vector<MGFloat>& tabx, const MGSize& id) const;
	//virtual void	IOSetBlockVector( const vector<MGFloat>& tabx, const MGSize& id );

 
private:
	//MGString				mSolName; 

	vector< SVector< ESIZE, GVec > >	mtabGrad;
	Sparse::Vector<ESIZE,GVec>	mvecQ; 

};
//////////////////////////////////////////////////////////////////////


//template <Dimension DIM, EQN_TYPE ET>
//inline void SolGradient<DIM,ET>::CheckIsNAN( const Sparse::Vector<ESIZE>& vec) const
//{
//	for ( MGSize i=0; i<vec.Size(); ++i)
//		for ( MGSize j=0; j<ESIZE; ++j)
//		{
//			if ( ISNAN( vec[i](j) ) )
//			{
//				vec[i].Write();
//				THROW_INTERNAL( "NAN :: i='" << i << "'")
//			}
//		}
//
//		//IS_INFNAN_THROW( vec[i](j), "ERROR in SolGradient<ESIZE>::CheckIsNAN()");
//}


//template <Dimension DIM, EQN_TYPE ET>
//inline void SolGradient<DIM,ET>::CheckQ( const Sparse::Vector<ESIZE>& vec) const
//{
//	//rSpaceSol().IsPhysical( vec);
//} 
//
// 
//template <Geom::Dimension DIM, EQN_TYPE ET>
//inline void SolGradient<DIM,ET>::IOGetBlockVector( vector<MGFloat>& tabx, const MGSize& id) const
//{
//	if ( tabx.size() != ESIZE)
//		THROW_INTERNAL( "SolGradient<ESIZE>::IOGetBlockVector - tabx.size() != ESIZE");
//
//	for ( MGSize k=0; k<ESIZE; ++k)
//		tabx[k] = mvecQ[id](k);
//}


//template <Geom::Dimension DIM, EQN_TYPE ET>
//inline void SolGradient<DIM,ET>::IOSetBlockVector( const vector<MGFloat>& tabx, const MGSize& id )
//{
//	if ( tabx.size() != ESIZE)
//		THROW_INTERNAL( "SolGradient<ESIZE>::IOSetBlockVector - tabx.size() != ESIZE");
//
//	for ( MGSize k=0; k<ESIZE; ++k)
//		mvecQ[id](k) = tabx[k];
//}


template <Dimension DIM, EQN_TYPE ET>
inline void SolGradient<DIM,ET>::Create( const CfgSection* pcfgsec, const Physics<DIM,ET>* pphys)		
{
	ConfigBase::Create( pcfgsec);
}


template <Dimension DIM, EQN_TYPE ET>
inline void SolGradient<DIM,ET>::PostCreateCheck() const
{ 
	ConfigBase::PostCreateCheck();
}


template <Dimension DIM, EQN_TYPE ET>
inline void SolGradient<DIM,ET>::Init()
{ 
} 


//template <Dimension DIM, EQN_TYPE ET>
//inline void SolGradient<DIM,ET>::DimToUndim( vector<MGFloat>& tab ) const
//{ 
//	FVec vin, vout;
//	if ( tab.size() != ESIZE)
//		THROW_INTERNAL("SolGradient<DIM,ET>::DimToUndim - tab.size() != ESIZE");
//
//	for ( MGSize i=0; i<ESIZE; ++i)
//		vin(i) = tab[i];
//
//	mpPhysics->DimToUndim( vout, vin, mpPhysics->cRefVar() );
//
//	for ( MGSize i=0; i<ESIZE; ++i)
//		tab[i] = vout(i);
//}
//
//template <Dimension DIM, EQN_TYPE ET>
//inline void SolGradient<DIM,ET>::UndimToDim( vector<MGFloat>& tab ) const
//{ 
//	FVec vin, vout;
//	if ( tab.size() != ESIZE)
//		THROW_INTERNAL("SolGradient<DIM,ET>::UndimToDim - tab.size() != ESIZE");
//
//	for ( MGSize i=0; i<ESIZE; ++i)
//		vin(i) = tab[i];
//
//	mpPhysics->UndimToDim( vout, vin, mpPhysics->cRefVar() );
//
//	for ( MGSize i=0; i<ESIZE; ++i)
//		tab[i] = vout(i);
//}



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __SOLGRADIENT_H__
