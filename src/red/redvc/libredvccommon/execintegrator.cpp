#include "execintegrator.h"
#include "libcorecommon/factory.h"
#include "libredcore/configconst.h"
#include "libcoreconfig/cfgsection.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Geom::Dimension DIM, EQN_TYPE ET>
ExecIntegrator<DIM,ET>::~ExecIntegrator()	
{
	if ( mpIntegrator != 0)
		delete mpIntegrator;
}


template <Geom::Dimension DIM, EQN_TYPE ET>
void ExecIntegrator<DIM,ET>::Create( const CfgSection* pcfgsec, Physics<DIM,ET>* pphys, Data<DIM,ET>* pdata)
{ 
	ExecutorBase<DIM,ET>::Create( pcfgsec, pphys, pdata);

	MGString sdim = Geom::DimToStr( DIM);
	MGString seqtype = EquationDef<DIM,ET>::NameEqnType();


	// create and init Integrator
	const CfgSection* pinteg = & this->ConfigSect().GetSection( ConfigStr::Solver::Executor::Integrator::NAME );

	//MGString sssoltype = pspcsol->ValueString( ConfigStr::Solver::ExecIntegrator::SpaceSol::Type::KEY );
	
	MGString sprefix = "";
	if ( ProcessInfo::IsParallel() )
		sprefix = "MPI_";

	MGString solkey = "INTEGRATOR_" + sdim + "_" + seqtype; // + "_" + sschemetype;
	//cout << "creating '" << solkey << "'" << endl;

	mpIntegrator = Singleton< Factory< MGString,Creator< Integrator<DIM,ET> > > >::GetInstance()->GetCreator( solkey )->Create();
	mpIntegrator->Create( pinteg, pphys, pdata);
}


template <Geom::Dimension DIM, EQN_TYPE ET>
void ExecIntegrator<DIM,ET>::Init()
{
	mpIntegrator->Init();
}



template <Geom::Dimension DIM, EQN_TYPE ET>
bool ExecIntegrator<DIM,ET>::SolveStep( ConvergenceInfo<ESIZE>& info)
{
	cout << Info() << endl;

	//ofstream of( "_dump_q.txt");
	//mpTimeSol->rLocalSolution().Dump( of);
	//THROW_INTERNAL( "STOP");

	// do step
	mpIntegrator->Do();

	return true;
}



void init_ExecIntegrator()
{

	//////////////////////////////////////////////////////////////////////
	// 2D

	//------------------------------------------------------------------//
	static ConcCreator< 
		MGString, 
		ExecIntegrator< Geom::DIM_2D, EQN_RANS_SA >, 
		ExecutorBase<Geom::DIM_2D,EQN_RANS_SA> 
	//>	gCreatorExecutorPoissonFull2D( "EXECINTEGRATOR_2D_RANS_SA_FULL");
	>	gCreatorExecIntegratorRansSAFull2D( "EXECUTOR_2D_RANS_SA_INTEGRATOR");

	//------------------------------------------------------------------//
	static ConcCreator< 
		MGString, 
		ExecIntegrator< Geom::DIM_2D, EQN_NS >, 
		ExecutorBase<Geom::DIM_2D,EQN_NS> 
	//>	gCreatorExecutorPoissonFull2D( "EXECINTEGRATOR_2D_RANS_SA_FULL");
	>	gCreatorExecIntegratorNSFull2D( "EXECUTOR_2D_NS_INTEGRATOR");

	//////////////////////////////////////////////////////////////////////
	// 3D

	//------------------------------------------------------------------//
	static ConcCreator< 
		MGString, 
		ExecIntegrator< Geom::DIM_3D, EQN_RANS_SA >, 
		ExecutorBase<Geom::DIM_3D,EQN_RANS_SA> 
	>	gCreatorExecIntegratorRansSAFull3D( "EXECUTOR_3D_RANS_SA_INTEGRATOR");

}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

