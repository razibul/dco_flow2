#ifndef __COMPFLOWENAT_H__
#define __COMPFLOWENAT_H__

#include "libredcore/equation.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
// class CFlowEnt
//////////////////////////////////////////////////////////////////////
template <Geom::Dimension DIM, MGSize EQSIZE, MGSize AUXSIZE, MGSize SIZE>
class CFlowEnt
{
protected:
	typedef typename EquationTypes<DIM,EQSIZE>::Vec		FVec;
	typedef typename EquationTypes<DIM,EQSIZE>::Mtx		FMtx;

	typedef typename EquationTypes<DIM,EQSIZE>::GVec	GVec;
	typedef typename EquationTypes<DIM,EQSIZE>::VecGrad	FGradVec;

	typedef typename EquationTypes<DIM,AUXSIZE>::Vec	AVec;

	enum { NO_EQNS = EQSIZE };
	enum { NO_AUX  = AUXSIZE };

public:
	CFlowEnt();
	CFlowEnt( const CFlowEnt<DIM,EQSIZE,AUXSIZE,SIZE>& cent);

	const bool&	IsOutlet() const					{ return mbIsOutlet;}
	void		SetOutlet( const bool& b=true)		{ mbIsOutlet = b;}

	const bool&	IsViscWall() const					{ return mbIsViscWall;}
	void		SetViscWall( const bool& b=true)	{ mbIsViscWall = b;}

	const bool&	IsInviscWall() const				{ return mbIsInviscWall;}
	void		SetInviscWall( const bool& b=true)	{ mbIsInviscWall = b;}

	const bool&	IsJacobian() const					{ return mbIsJacobian;}
	void		SetJacobian( const bool& b=true)	{ mbIsJacobian = b;}


	const FVec&		cVar( const MGSize& i) const		{ return mtabVar[i];}
	FVec&			rVar( const MGSize& i)				{ return mtabVar[i];}

	const FGradVec&	cGradVar( const MGSize& i) const	{ return mtabGradVar[i];}
	FGradVec&		rGradVar( const MGSize& i)			{ return mtabGradVar[i];}

	const AVec&		cAux( const MGSize& i) const		{ return mtabAux[i];}
	AVec&			rAux( const MGSize& i)				{ return mtabAux[i];}

	void	Dump( ostream& f, const MGSize& size) const;

private:
	bool	mbIsOutlet;
	bool	mbIsViscWall;
	bool	mbIsInviscWall;

	bool	mbIsJacobian;

	AVec		mtabAux[SIZE];
	FVec		mtabVar[SIZE];
	FGradVec	mtabGradVar[SIZE];
};
//////////////////////////////////////////////////////////////////////




template <Geom::Dimension DIM, MGSize EQSIZE, MGSize AUXSIZE, MGSize SIZE>
inline CFlowEnt<DIM,EQSIZE,AUXSIZE,SIZE>::CFlowEnt()
{
	mbIsJacobian = false;

	mbIsOutlet = false;
	mbIsViscWall = false;
	mbIsInviscWall = false;

	for ( MGSize i=0; i<SIZE; ++i)
	{
		mtabVar[i].Init(0.0);
		mtabGradVar[i].Init( GVec() );
		mtabAux[i].Init(0.0);
	}
}


template <Geom::Dimension DIM, MGSize EQSIZE, MGSize AUXSIZE, MGSize SIZE>
inline CFlowEnt<DIM,EQSIZE,AUXSIZE,SIZE>::CFlowEnt( const CFlowEnt<DIM,EQSIZE,AUXSIZE,SIZE>& cent)
{
	mbIsOutlet = false;

	for ( MGSize i=0; i<SIZE; ++i)
	{
		mtabVar[i] = cent.mtabVar[i];
		mtabGradVar[i] = cent.mtabGradVar[i];
		mtabAux[i] = cent.mtabAux[i];
	}
}



template <Geom::Dimension DIM, MGSize EQSIZE, MGSize AUXSIZE, MGSize SIZE>
inline void CFlowEnt<DIM,EQSIZE,AUXSIZE,SIZE>::Dump( ostream& f, const MGSize& size) const
{
	for ( MGSize i=0; i<size; ++i)
	{
		f << "var[" << i << "]: {" << mtabVar[i](0);
		for ( MGSize k=1; k<EQSIZE; ++k)
			f << ", " << mtabVar[i](k);
		f << "},{";

		for ( MGSize k=0; k<AUXSIZE; ++k)
			f << ", " << mtabAux[i](k) ;
		f << "}\n";
	}
}



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __COMPFLOWENAT_H__
