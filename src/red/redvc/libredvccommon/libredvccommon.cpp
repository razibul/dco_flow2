
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



void init_Solver();
void init_Executor();
void init_ExecIntegrator();
void init_ExecAssistant();
void init_SpaceSolver();
void init_SolGradBuilder();


void RegisterLib_REDVCCOMMON()
{
	init_Solver();
	init_Executor();
	init_ExecIntegrator();
	init_ExecAssistant();
	init_SpaceSolver();
	init_SolGradBuilder();
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

