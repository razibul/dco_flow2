#ifndef __ASSISTANTBASE_H__
#define __ASSISTANTBASE_H__


#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"
#include "libredcore/equation.h"

#include "libcoreconfig/configbase.h"
#include "libredphysics/physics.h"
#include "redvc/libredvccommon/data.h"

#include "redvc/libredvccommon/compflowcell.h"
#include "redvc/libredvccommon/compflowbface.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
//	class AssistantBase
//////////////////////////////////////////////////////////////////////
template <Geom::Dimension DIM, EQN_TYPE ET>
class AssistantBase : public ConfigBase
{
public:
	enum { ESIZE = EquationDef<DIM,ET>::SIZE };
	enum { ASIZE = EquationDef<DIM,ET>::AUXSIZE };

	typedef CFlowCell<DIM,ESIZE,ASIZE>		CFCell;
	typedef CFlowBFace<DIM,ESIZE,ASIZE>		CFBFace;


	static MGString	Info()	
	{
		ostringstream os;
		os << "AssistantBase< " << Geom::DimToStr(DIM) << ", " << EquationDef<DIM,ET>::NameEqnType() << " >";
		return os.str();
	}

	AssistantBase()	{}
	virtual ~AssistantBase()	{};

	virtual void	Create( const CfgSection* pcfgsec, Physics<DIM,ET>* pphys, Data<DIM,ET>* pdata);
	virtual void	PostCreateCheck() const ;
	virtual void	Init() = 0;
	virtual bool	Do() = 0;

protected:
	const Physics<DIM,ET>&	cPhysics() const	{ return *mpPhysics;}
	const Data<DIM,ET>&		cData() const		{ return *mpData;}
	Data<DIM,ET>&			rData()				{ return *mpData;}


	void	GetFCell( CFCell& fcell, const MGSize& i ) const;
	void	GetFBFace( CFBFace& fbface, const MGSize& i ) const;

private:
	Physics<DIM,ET>*			mpPhysics;
	Data<DIM,ET>*				mpData;
};


template <Geom::Dimension DIM, EQN_TYPE ET>
void AssistantBase<DIM,ET>::Create( const CfgSection* pcfgsec, Physics<DIM,ET>* pphys, Data<DIM,ET>* pdata)
{
	ConfigBase::Create( pcfgsec);

	mpPhysics = pphys;
	mpData = pdata;
}


template <Geom::Dimension DIM, EQN_TYPE ET>
inline void AssistantBase<DIM,ET>::PostCreateCheck() const
{ 
	ConfigBase::PostCreateCheck();


	if ( ! mpPhysics )
		THROW_INTERNAL( Info() << "::PostCreateCheck() -- failed : mpPhysics -> NULL" );

	if ( ! mpData )
		THROW_INTERNAL( Info() << "::PostCreateCheck() -- failed : mpData -> NULL" );
}



template <Geom::Dimension DIM, EQN_TYPE ET>
inline void AssistantBase<DIM,ET>::GetFCell( CFCell& fcell, const MGSize& i ) const
{
	cData().cGrid().GetCell( i, fcell );
	for ( MGSize i=0; i<fcell.Size(); ++i)
	{
		fcell.rVar(i) = typename EquationDef<DIM,ET>::FVec( cData().cSolution()[ fcell.cId(i) ] ) ;
		fcell.rAux(i) = typename EquationDef<DIM,ET>::AVec( cData().cAuxData()[ fcell.cId(i) ] ) ;
	}

	if ( cData().cSolution().Size() == cData().cSolGradient().Size() )
		for ( MGSize i=0; i<fcell.Size(); ++i)
			fcell.rGradVar(i) = typename EquationDef<DIM,ET>::FGradVec( cData().cSolGradient()[ fcell.cId(i) ] ) ;
}


template <Geom::Dimension DIM, EQN_TYPE ET>
inline void AssistantBase<DIM,ET>::GetFBFace( CFBFace& fbface, const MGSize& i ) const
{ 
	cData().cGrid().GetBFace( i, fbface);
	for ( MGSize i=0; i<fbface.Size(); ++i)
	{
		fbface.rVar(i) = typename EquationDef<DIM,ET>::FVec( cData().cSolution()[ fbface.cId(i) ] );
		fbface.rAux(i) = typename EquationDef<DIM,ET>::AVec( cData().cAuxData()[ fbface.cId(i) ] ) ;
	}

	if ( cData().cSolution().Size() == cData().cSolGradient().Size() )
		for ( MGSize i=0; i<fbface.Size(); ++i)
			fbface.rGradVar(i) = typename EquationDef<DIM,ET>::FGradVec( cData().cSolGradient()[ fbface.cId(i) ] );

}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __ASSISTANTBASE_H__
