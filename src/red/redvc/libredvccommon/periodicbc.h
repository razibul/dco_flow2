#ifndef __PERIODICBC_H__
#define __PERIODICBC_H__

#include "libcorecommon/key.h"

#include "periodicbcbase.h"
//#include "periodicbcmap.h"

#include "redvc/libredvcgrid/gridsimplex.h"
//#include "grid.h"

#include "libredcore/equation.h"
#include "libredphysics/bc.h"

#include "redvc/libredvccommon/solution.h"

#include "libhfgeom/hf_typedef.h"
#include "libhfgeom/hf_intersection.h"

#include "libcorecommon/progressbar.h"

#include "libredcore/processinfo.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

	template<Dimension DIM, EQN_TYPE ET, bool ISVECTTYPE>
	struct GetU
	{
		typedef EquationDef<DIM, ET>			EqDef;
		typedef typename EqDef::FVec			FVec;

		static void Get(Vect<DIM>& vel, const FVec& v)
		{
			switch (DIM)
			{
			case DIM_3D:
				vel.rX(2) = v[EqDef::template U<2>::ID];
			case DIM_2D:
				vel.rX(1) = v[EqDef::template U<1>::ID];
			case DIM_1D:
				vel.rX(0) = v[EqDef::template U<0>::ID];
			default:
				break;
			}
		}
		static void Set(const Vect<DIM>& vel, FVec& v)
		{
			switch (DIM)
			{
			case DIM_3D:
				v[EqDef::template U<2>::ID] = vel.cX(2);
			case DIM_2D:
				v[EqDef::template U<1>::ID] = vel.cX(1);
			case DIM_1D:
				v[EqDef::template U<0>::ID] = vel.cX(0);
			default:
				break;
			}
		}
	};

	template<Dimension DIM, EQN_TYPE ET>
	struct GetU < DIM, ET, false >
	{
		typedef EquationDef<DIM, ET>			EqDef;
		typedef typename EqDef::FVec			FVec;

		static void Get(Vect<DIM>& vel, const FVec& v)
		{
		}
		static void Set(const Vect<DIM>& vel, FVec& v)
		{
		}
	};


	//////////////////////////////////////////////////////////////////////
	// class PeriodicBCCalc
	//////////////////////////////////////////////////////////////////////
	template<Dimension DIM, EQN_TYPE ET>
	class PeriodicBCCalc : public PeriodicBCBase<DIM, ET>
	{
	public:
		enum { ESIZE = EquationDef<DIM, ET>::SIZE };

		typedef PeriodicBCBase<DIM, ET>			Base;
		typedef GridSimplex<DIM>				TGrid;
		typedef EquationDef<DIM, ET>			EqDef;
		typedef typename EqDef::FVec			FVec;
		typedef HFGeom::GVector<MGFloat, DIM>	HFVector;

		typedef KeyData<MGSize, PeriodicKeyData<DIM> >	PeriodicKey;
		typedef vector<PeriodicKey>						ColPeriodic;

		void Init(const TGrid* grid, const Solution<DIM, ET>* sol, const CfgSection* pphysect)
		{
			mpsol = sol;
			mpGrid = grid;
			this->ReadConfig(pphysect);
			InitPeriodicMap(pphysect);
		}

		const PeriodicKeyData<DIM>&		cPeriodicKey(const MGSize& id) const;

		virtual void GetPeriodicVar(const MGSize& id, FVec& v) const;
	private:
		void InitPeriodicMap(const CfgSection* pphysect);
		void UniquePeriodicMap();
		//	void FindMapping(const MGSize& surfid, const MGSize& targetsurfid, const Vect<DIM>&	vct, const MGFloat& angle = 0.);

		template<MGSize PRDTYPE>
		void FindMapping(const typename Base::PrdData& data, const MGSize& setid);
		//void ApplyPrd(const typename Base::PrdData& data, const Vect<DIM>& pos, Vect<DIM>& newpos, Int2Type<PRD_TRANS>) const;
		//void ApplyPrd(const typename Base::PrdData& data, const Vect<DIM>& pos, Vect<DIM>& newpos, Int2Type<PRD_ANGLE>) const;

		void GetVar(const MGSize& id, FVec& v) const;
		void GetVar(const PeriodicKeyData<DIM>& key, const MGSize& id, FVec& v, Int2Type<PRD_TRANS>) const;
		void GetVar(const PeriodicKeyData<DIM>& key, const MGSize& id, FVec& v, Int2Type<PRD_ANGLE>) const;

		ColPeriodic							mcolperiodic;
		const TGrid*						mpGrid;
		const Solution<DIM, ET>*			mpsol;
	};


	template<Dimension DIM, EQN_TYPE ET>
	void PeriodicBCCalc<DIM, ET >::GetVar(const MGSize& id, FVec& v) const
	{
		const PeriodicKeyData<DIM>& key = this->cPeriodicKey(id);

		switch (this->mparams[key.mdatasetid].type)
		{
		case PRD_TRANS:
		{
			GetVar(key, id, v, Int2Type<PRD_TRANS>());
			break;
		}
		case PRD_ANGLE:
		{
			GetVar(key, id, v, Int2Type<PRD_ANGLE>());
			break;
		}
		}
	}

	template<Dimension DIM, EQN_TYPE ET>
	void PeriodicBCCalc<DIM, ET >::GetVar(const PeriodicKeyData<DIM>& key, const MGSize& id, FVec& v, Int2Type<PRD_TRANS>) const
	{
		for (MGSize i = 0; i < DIM; ++i)
		{
			MGSize nodeid = this->mpGrid->cBFace(key.mcellid).cId(i);
			for (MGSize j = 0; j < ESIZE; ++j)
			{
				v[j] += (*mpsol)[nodeid][j] * key.mtabcoord.cElem(i);
			}
		}
	}

	template<Dimension DIM, EQN_TYPE ET>
	void PeriodicBCCalc<DIM, ET >::GetVar(const PeriodicKeyData<DIM>& key, const MGSize& id, FVec& v, Int2Type<PRD_ANGLE>) const
	{
		typename Base::PrdData data = this->mparams[key.mdatasetid];

		for (MGSize i = 0; i < DIM; ++i)
		{
			MGSize nodeid = this->mpGrid->cBFace(key.mcellid).cId(i);
			for (MGSize j = 0; j < ESIZE; ++j)
			{
				v[j] += (*mpsol)[nodeid][j] * key.mtabcoord.cElem(i);
			}
		}

		Vect<DIM> pos, arrow, root, vel, newarrow;
		pos = mpGrid->cNode(id).cPos();
		
		if (mpGrid->cBFace(key.mcellid).cSurfId() != data.trg)
		{
			data.angle *= -1.;
			swap(data.src, data.trg);
		}

		PeriodicOp<DIM>::ApplyPrd(data, pos, root, Int2Type<PRD_ANGLE>());

		GetU<DIM, ET, EqDef::HASVECTOR>::Get(vel, v);

		arrow = root + vel;
		data.angle *= -1.;
		swap(data.src, data.trg);
		PeriodicOp<DIM>::ApplyPrd(data, arrow, newarrow, Int2Type<PRD_ANGLE>());

		vel = newarrow - pos;
		GetU<DIM, ET, EqDef::HASVECTOR>::Set(vel, v);

	}



	template<Dimension DIM, EQN_TYPE ET>
	void PeriodicBCCalc<DIM, ET >::InitPeriodicMap(const CfgSection* pphysect)
	{
		cout << "Creating periodic bc map" << endl;
		for (MGSize i = 0; i < this->mparams.size(); ++i)
		{
			switch (this->mparams[i].type)
			{
			case PRD_TRANS:
			{
				typename Base::PrdData data = this->mparams[i];
				FindMapping<PRD_TRANS>(data, i);
				data.vct *= -1.;
				swap(data.src, data.trg);
				FindMapping<PRD_TRANS>(data, i);

				break;
			}
			case PRD_ANGLE:
			{
				typename Base::PrdData data = this->mparams[i];
				FindMapping<PRD_ANGLE>(data, i);
				data.angle *= -1.;
				swap(data.src, data.trg);
				FindMapping<PRD_ANGLE>(data, i);

				break;
			}
			case PRD_NONE:
				//THROW_INTERNAL("Angle periodic bc not implemented.");
				break;
			default:
				THROW_INTERNAL("Unknown periodic bc type.");
				break;
			}
		}

		UniquePeriodicMap();

		//sort(mcolperiodic.begin(), mcolperiodic.end());
		//typename ColPeriodic::iterator itr = unique(mcolperiodic.begin(), mcolperiodic.end());
		//mcolperiodic.erase(itr, mcolperiodic.end());
	}

	template<Dimension DIM, EQN_TYPE ET>
	void PeriodicBCCalc<DIM, ET >::UniquePeriodicMap()
	{
		if (mcolperiodic.empty()) return;

		sort(mcolperiodic.begin(), mcolperiodic.end());

		ColPeriodic newperiodic;
		typename ColPeriodic::iterator itr = mcolperiodic.begin()+1;
		typename ColPeriodic::iterator olditr = mcolperiodic.begin();

		typename ColPeriodic::iterator bestitr = mcolperiodic.begin();
		while (itr != mcolperiodic.end())
		{
			//if (itr->first == 1558)
			//{
			//	cout << "break";
			//}
			if (itr->first == olditr->first)
			{
				vector<KeyData<MGFloat, MGSize> > 	tab(2*DIM);
				for (MGSize i = 0; i < DIM; ++i)
				{
					tab[i].first = bestitr->second.mtabcoord.rElem(i);
					tab[i].second = 0;

					tab[DIM + i].first = itr->second.mtabcoord.rElem(i);
					tab[DIM + 1].second = 1;
				}
				sort(tab.begin(), tab.end());

				if (tab[0].second == 0)
				{
					bestitr = itr;
				}
				olditr = itr;
				if (++itr == mcolperiodic.end())
				{
					newperiodic.push_back(*bestitr);
				}
			}
			else
			{
				newperiodic.push_back(*bestitr);

				bestitr = itr;
				olditr = itr;
				++itr;
			}
		}

		mcolperiodic.swap(newperiodic);
	}

	template<Dimension DIM, EQN_TYPE ET>
	template<MGSize PRDTYPE>
	void PeriodicBCCalc<DIM, ET >::FindMapping(const typename Base::PrdData& data, const MGSize& setid)
	{
		ProgressBar bar(40);
		bar.Init(mpGrid->SizeBFaceTab());
		bar.Start();
		for (MGSize inx = 0; inx < mpGrid->SizeBFaceTab(); ++inx, ++bar)
		{
			CGeomBFace<DIM> bface = mpGrid->cBFace(inx);

			if (bface.cSurfId() == data.src)
			{
				for (MGSize trginx = 0; trginx < mpGrid->SizeBFaceTab(); ++trginx)
				{
					CGeomBFace<DIM> bfacetrg = mpGrid->cBFace(trginx);

					if (bfacetrg.cSurfId() == data.trg)
					{
						HFVector tabpos[DIM];
						for (MGSize i = 0; i < DIM; ++i)
						{
							const MGSize& trgnodeid = bfacetrg.cId(i);
							for (MGSize j = 0; j < DIM; ++j)
							{
								tabpos[i].rX(j) = mpGrid->cNode(trgnodeid).cPos().cX(j);
							}
						}

						MGFloat maxedgesize = 0.;
						for (MGSize i = 0; i < DIM-1; ++i)
						{
							MGFloat len = 0.;
							HFVector tmp(tabpos[i] - tabpos[(i + 1) % DIM]);
							for (MGSize ii = 0; ii < DIM; ++ii)
							{
								len += tmp.cX(ii)*tmp.cX(ii);
							}
							len = sqrt(len);
							if (len > maxedgesize)
							{
								maxedgesize = len;
							}
						}

						HFGeom::SubSimplex<MGFloat, DIM - 1, DIM> subb((typename HFGeom::SubSimplex<MGFloat, DIM - 1, DIM>::ARRAY(tabpos)));

						for (MGSize i = 0; i < bface.Size(); ++i)
						{
							HFVector pos;
							Vect<DIM> tmppos;
							const MGSize& nodeid = bface.cId(i);

							PeriodicOp<DIM>::ApplyPrd(data, mpGrid->cNode(nodeid).cPos(), tmppos, Int2Type<PRDTYPE>());
							for (MGSize ii = 0; ii < DIM; ++ii)
							{
								pos.rX(ii) = tmppos.cX(ii);
							}

			/*				if (nodeid == 1558)
							{
								cout << "break";
							}*/

							//for (MGSize j = 0; j < DIM; j++)
							//{
							//	pos.rX(j) = mpGrid->cNode(nodeid).cPos().cX(j) + data.vct.cX(j);
							//}

							HFGeom::SubSimplex<MGFloat, 0, DIM> suba((typename  HFGeom::SubSimplex<MGFloat, 0, DIM>::ARRAY(pos)));

							HFGeom::IntersectionProx<MGFloat, MGFloat, 0, DIM - 1, DIM> inter(suba, subb);
							inter.Execute();

							MGFloat	tabB[DIM];
							inter.GetBBaryCoords(tabB);

							bool bin = true;
							for (MGSize i = 0; i < DIM; ++i)
							{
								tabB[i] /= inter.cDet();

								if (data.isabstol)
								{
									if (tabB[i] < -data.tol)
									{
										bin = false;
									}
								}
								else
								{
									if (tabB[i] < -maxedgesize*data.tol)
									{
										bin = false;
									}
								}
							}
							if (bin)
							{
								PeriodicKey key;
								key.first = nodeid;
								key.second.mcellid = trginx;
								key.second.mdatasetid = setid;
								for (MGSize j = 0; j < DIM; ++j)
								{
									key.second.mtabcoord.rElem(j) = tabB[j];
								}

								mcolperiodic.push_back(key);
							}
						}
					}
				}
			}
		}
		bar.Finish();
	}

	template<Dimension DIM, EQN_TYPE ET>
	const PeriodicKeyData<DIM>& PeriodicBCCalc<DIM, ET >::cPeriodicKey(const MGSize& id) const
	{
		typename ColPeriodic::const_iterator it = lower_bound(mcolperiodic.begin(), mcolperiodic.end(), PeriodicKey(id));
		if ((it == mcolperiodic.end()) || (it->first != id))
		{
			ofstream off(ProcessInfo::ModifyFileName("periodic_nodes.log"));
			//off << "notfound: " << id << endl;
			for (size_t j = 0; j < DIM; j++)
			{
				off.precision(12);
				off << mpGrid->cNode(id).cPos().cX(j) << "\t";
			}
			off << id << endl;
			for (size_t i = 0; i < mcolperiodic.size(); i++)
			{
				for (size_t j = 0; j < DIM; j++)
				{
					off.precision(12);
					off << mpGrid->cNode(mcolperiodic[i].first).cPos().cX(j) << "\t";
				}
				off << mcolperiodic[i].first;
				off << endl;

				//off << mcolperiodic[i].first << "\t" << mcolperiodic[i].second.mcellid << endl;
			}
			off.close();
			
			cout << ProcessInfo::Prompt() << "id: " << id << endl;
			THROW_INTERNAL("Error in PeriodicBCCalc::cPeriodicKey, nodeid could not be found.");
		}
		return it->second;
	}

	template<Dimension DIM, EQN_TYPE ET>
	void PeriodicBCCalc<DIM, ET >::GetPeriodicVar(const MGSize& id, FVec& v) const
	{
		for (MGSize i = 0; i < ESIZE; ++i)
		{
			v[i] = 0.;
		}
		GetVar(id, v);
	}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __PERIODICBC_H__
