#ifndef __COMPGEOMENT_H__
#define __COMPGEOMENT_H__

#include "libcoresystem/mgdecl.h"
#include "redvc/libredvcgrid/node.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



using namespace Geom;


const MGSize GEOMSIZE_SEG	= 2;

const MGSize GEOMSIZE_TRI	= 3;
const MGSize GEOMSIZE_QUAD	= 4;

const MGSize GEOMSIZE_TET	= 4;
const MGSize GEOMSIZE_HEX	= 8;
const MGSize GEOMSIZE_PRISM	= 6;
const MGSize GEOMSIZE_PIRAM	= 5;


template <Dimension DIM>
MGFloat Volume( const Node<DIM> tabnod[], const MGSize& n);

template <Dimension DIM>
MGFloat Area( const Node<DIM> tabnod[], const MGSize& n);

template <Dimension DIM>
MGFloat InRadius( const Node<DIM> tabnod[], const MGSize& n);


//////////////////////////////////////////////////////////////////////
// class CGeomEnt
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, MGSize SIZE>
class CGeomEnt
{
public:
	CGeomEnt();
	CGeomEnt( const CGeomEnt<DIM,SIZE>& cent);

	const MGSize&		Size() const					{ return mType;}

	const MGSize&		cType() const					{ return mType;}
	const MGSize&		cId( const MGSize& i) const		{ return mtabId[i];}
	const MGSize&		cGId( const MGSize& i) const	{ return mtabGlobId[i];}
	const Node<DIM>&	cNode( const MGSize& i) const	{ return mtabNode[i];}

	const Node<DIM>*	cTabNode() const				{ return mtabNode;}

	MGSize&				rType()							{ return mType;}
	MGSize&				rId( const MGSize& i)			{ return mtabId[i];}
	MGSize&				rGId( const MGSize& i)			{ return mtabGlobId[i];}
	Node<DIM>&			rNode( const MGSize& i)			{ return mtabNode[i];}

	MGFloat				Volume() const;
	MGFloat				InRadius() const;

	void	Dump( ostream& f) const;

private:
	MGSize				mType;
	MGSize				mtabId[SIZE];
	MGSize				mtabGlobId[SIZE];
	Node<DIM>			mtabNode[SIZE];
};



template <Dimension DIM, MGSize SIZE>
CGeomEnt<DIM,SIZE>::CGeomEnt() : mType(0)
{
	for ( MGSize i=0; i<SIZE; ++i)
	{
		mtabId[i] = 0;
		mtabGlobId[i] = 0;
		mtabNode[i] = Node<DIM>();
	}
}


template <Dimension DIM, MGSize SIZE>
CGeomEnt<DIM,SIZE>::CGeomEnt( const CGeomEnt<DIM,SIZE>& cent)
{
	mType = cent.mType;
	for ( MGSize i=0; i<SIZE; ++i)
	{
		mtabId[i] = cent.mtabId[i];
		mtabGlobId[i] = cent.mtabGlobId[i];
		mtabNode[i] = cent.mtabNode[i];
	}
}


template <Dimension DIM, MGSize SIZE>
inline MGFloat CGeomEnt<DIM,SIZE>::Volume() const
{
	return RED::Volume( mtabNode, mType);
}

template <Dimension DIM, MGSize SIZE>
inline MGFloat CGeomEnt<DIM,SIZE>::InRadius() const
{
	return RED::InRadius( mtabNode, mType);
}



template <Dimension DIM, MGSize SIZE>
inline void CGeomEnt<DIM,SIZE>::Dump( ostream& f) const
{
	f << "type: " << mType << endl;
	for ( MGSize i=0; i<mType; ++i)
	{
		//f << "node[" << i << "|" << mtabId[i] << "]: {" << mtabNode[i].cPos().cX();
		f << "node[" << i << "|" << mtabId[i] << "|" << mtabGlobId[i] << "]: {" << mtabNode[i].cPos().cX();

		for ( MGSize k=1; k<DIM; ++k)
			f << ", " << mtabNode[i].cPos().cX( (MGInt)k);
		f << "}\n";
	}
}


//////////////////////////////////////////////////////////////////////
/// class CGeomBndEnt
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, MGSize SIZE>
class CGeomBndEnt : public CGeomEnt<DIM,SIZE>
{
public:
	CGeomBndEnt();
	CGeomBndEnt( const CGeomBndEnt<DIM,SIZE>& cent);

	const MGInt&				cSurfId() const					{ return mSurfId;}
	const MGSize&				cCellId() const					{ return mCellId;}
	const MGSize&				cBId( const MGSize& i) const	{ return mtabBndId[i];}
	const BoundaryNode<DIM>&	cBNode( const MGSize& i) const	{ return mtabBndNode[i];}

	MGInt&						rSurfId()						{ return mSurfId;}
	MGSize&						rCellId()						{ return mCellId;}
	MGSize&						rBId( const MGSize& i)			{ return mtabBndId[i];}
	BoundaryNode<DIM>&			rBNode( const MGSize& i)		{ return mtabBndNode[i];}

	MGFloat						Area() const					{ return RED::Area( this->cTabNode(), this->cType() ); }

private:
	MGInt				mSurfId;
	MGSize				mCellId;
	MGSize				mtabBndId[SIZE];
	BoundaryNode<DIM>	mtabBndNode[SIZE];
};


template <Dimension DIM, MGSize SIZE>
CGeomBndEnt<DIM,SIZE>::CGeomBndEnt() : CGeomEnt<DIM,SIZE>()
{
	mSurfId = 0;
	mCellId = 0;
	for ( MGSize i=0; i<SIZE; ++i)
	{
		mtabBndId[i] = 0;
		mtabBndNode[i] = BoundaryNode<DIM>();
	}
}


template <Dimension DIM, MGSize SIZE>
CGeomBndEnt<DIM,SIZE>::CGeomBndEnt( const CGeomBndEnt<DIM,SIZE>& cent) : CGeomEnt<DIM,SIZE>( cent)
{
	mSurfId = cent.mSurfId;
	mCellId = cent.mCellId;
	for ( MGSize i=0; i<SIZE; ++i)
	{
		mtabBndId[i] = cent.mtabBndId[i];
		mtabBndNode[i] = cent.mtabBndNode[i];
	}
}




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __COMPGEOMENT_H__
