#ifndef __BFACECOLLECTION_H__
#define __BFACECOLLECTION_H__


#include "libcoresystem/mgdecl.h"
#include "redvc/libredvcgrid/bface.h"
#include "redvc/libredvcgrid/compgeombface.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



//////////////////////////////////////////////////////////////////////
// class BFaceCollection
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class BFaceCollection
{
public:
	BFaceCollection()	{ THROW_INTERNAL( "Not implemented !!!");}
};


//////////////////////////////////////////////////////////////////////
// class BFaceCollection<DIM_2D>
//////////////////////////////////////////////////////////////////////
template <>
class BFaceCollection<DIM_2D>
{
public:
	MGSize	Size( const MGSize& type) const;

	MGSize	Size() const	{ return mtabEdge.size(); }
	MGSize	NTypes() const	{ return (mtabEdge.size() ? 1 : 0); }
	MGSize	Type( const MGSize& id) const;

	void	GetCBFaceIds( CGeomBFace<DIM_2D>& cbface, const MGSize& i) const;		// only tabId is set
	void	GetCBFaceBndIds( CGeomBFace<DIM_2D>& cbface, const MGSize& i) const;	// both tabId & tabBndId are set
	void	SetCBFaceBndIds( const CGeomBFace<DIM_2D>& cbface, const MGSize& i);

	void	GetBFaceIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const;
	void	SetBFaceIds( const vector<MGSize>& tabid, const MGSize& type, const MGSize& id);

	void	GetBFaceSurf( MGSize& idsurf, const MGSize& type, const MGSize& id ) const;
	void	SetBFaceSurf( const MGSize& idsurf, const MGSize& type, const MGSize& id );

	void	GetBFaceBIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const;

	void	Resize( const MGSize& type, const MGSize& n);

private:
	vector< BFace<DIM_2D, BFACESIZE_EDGE> >	mtabEdge;
};



inline MGSize BFaceCollection<DIM_2D>::Size( const MGSize& type) const
{
	switch ( type)
	{
	case BFACESIZE_EDGE:
		return mtabEdge.size();

	default:
		THROW_INTERNAL( "from BFaceCollection<DIM_2D>::Size - bad cell type" );
	}

	return 0;
}


inline 	MGSize	BFaceCollection<DIM_2D>::Type( const MGSize& id) const
{
	MGSize k=0;
	if ( (id+1) == ( k += mtabEdge.size() ? 1 : 0 ) )
		return BFACESIZE_EDGE;
	else
		THROW_INTERNAL( "error in BFaceCollection<DIM_2D>::Type");

	return 0;
}

//////////////////////////////////////////////////////////////////////
// class BFaceCollection<DIM_3D>
//////////////////////////////////////////////////////////////////////
template <>
class BFaceCollection<DIM_3D>
{
public:
	MGSize	Size( const MGSize& type) const;

	MGSize	Size() const	{ return mtabTri.size() + mtabQuad.size(); }
	MGSize	NTypes() const	{ return (mtabTri.size() ? 1 : 0) + (mtabQuad.size() ? 1 : 0);}
	MGSize	Type( const MGSize& id) const;

	void	GetCBFaceIds( CGeomBFace<DIM_3D>& cbface, const MGSize& i) const;		// only tabId is set
	void	GetCBFaceBndIds( CGeomBFace<DIM_3D>& cbface, const MGSize& i) const;	// both tabId & tabBndId are set
	void	SetCBFaceBndIds( const CGeomBFace<DIM_3D>& cbface, const MGSize& i);

	void	GetBFaceIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const;
	void	SetBFaceIds( const vector<MGSize>& tabid, const MGSize& type, const MGSize& id);

	void	GetBFaceSurf( MGSize& idsurf, const MGSize& type, const MGSize& id ) const;
	void	SetBFaceSurf(  const MGSize& idsurf, const MGSize& type, const MGSize& id );

	void	GetBFaceBIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const;

	void	Resize( const MGSize& type, const MGSize& n);

private:
	vector< BFace<DIM_3D, BFACESIZE_TRI> >	mtabTri;
	vector< BFace<DIM_3D, BFACESIZE_QUAD> >	mtabQuad;
};


inline MGSize BFaceCollection<DIM_3D>::Size( const MGSize& type) const
{
	switch ( type)
	{
	case BFACESIZE_TRI:
		return mtabTri.size();

	case BFACESIZE_QUAD:
		return mtabQuad.size();

	default:
		THROW_INTERNAL( "from BFaceCollection<DIM_3D>::Size - bad cell type" );
	}

	return 0;
}

inline 	MGSize	BFaceCollection<DIM_3D>::Type( const MGSize& id) const
{
	MGSize k=0;
	if ( (id+1) == ( k += mtabTri.size() ? 1 : 0 ) )
		return BFACESIZE_TRI;
	else if ( (id+1) == ( k += mtabQuad.size() ? 1 : 0 ) )
		return BFACESIZE_QUAD;
	else
		THROW_INTERNAL( "error in BFaceCollection<DIM_3D>::Type");

	return 0;
}




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __BFACECOLLECTION_H__
