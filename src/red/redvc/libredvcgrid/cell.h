#ifndef __CELL_H__
#define __CELL_H__


#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"
#include "redvc/libredvcgrid/gridconst.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



using namespace Geom;


//////////////////////////////////////////////////////////////////////
// class Cell
//////////////////////////////////////////////////////////////////////
template<Dimension DIM, MGSize SIZE>
class Cell
{
public:
	MGSize			Size() const				{ return SIZE;}
	const MGSize&	cId( const MGSize& i) const	{ return mtabId[i];}
	MGSize&			rId( const MGSize& i)		{ return mtabId[i];}

	void	DumpIds();

private:
	MGSize	mtabId[SIZE];
};
//////////////////////////////////////////////////////////////////////



template<Dimension DIM, MGSize SIZE>
void Cell<DIM,SIZE>::DumpIds()
{
	char sbuf[1024] = "";
	for ( MGSize i=0; i<Size(); ++i)
		sprintf( sbuf, "%s %d", sbuf, mtabId[i] );
	TRACE( sbuf);
}




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __CELL_H__
