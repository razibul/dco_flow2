#ifndef __GRIDCONST_H__
#define __GRIDCONST_H__


#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



using namespace Geom;


const MGSize CELLSIZE_TRI	= 3;
const MGSize CELLSIZE_QUAD	= 4;

const MGSize CELLSIZE_TET	= 4;
const MGSize CELLSIZE_HEX	= 8;
const MGSize CELLSIZE_PRISM	= 6;
const MGSize CELLSIZE_PIRAM	= 5;

const MGSize CELLNFACE_TET		= 4;
const MGSize CELLNFACE_HEX		= 6;
const MGSize CELLNFACE_PRISM	= 5;
const MGSize CELLNFACE_PIRAM	= 5;



const MGSize BFACESIZE_EDGE	= 2;

const MGSize BFACESIZE_TRI	= 3;
const MGSize BFACESIZE_QUAD	= 4;



//////////////////////////////////////////////////////////////////////
// class GridSizes
//////////////////////////////////////////////////////////////////////
template <MGSize DIM>
class GridSizes
{
};

//////////////////////////////////////////////////////////////////////
// class GridSizes - 2D specialization
//////////////////////////////////////////////////////////////////////
template <>
class GridSizes<DIM_2D>
{
public:
	enum { CELL_MAXSIZE		= CELLSIZE_QUAD };
	enum { BFACE_MAXSIZE	= BFACESIZE_EDGE };
};

//////////////////////////////////////////////////////////////////////
// class GridSizes - 3D specialization
//////////////////////////////////////////////////////////////////////
template <>
class GridSizes<DIM_3D>
{
public:
	enum { CELL_MAXSIZE		= CELLSIZE_HEX };
	enum { BFACE_MAXSIZE	= BFACESIZE_QUAD };
};


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __GRIDCONST_H__
