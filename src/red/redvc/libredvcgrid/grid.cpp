#include "grid.h"

#include "libredcore/configconst.h"
#include "libcoreio/store.h"
#include "libcoreio/readmsh2.h"
#include "libcoreio/writemsh2.h"
#include "libcoreio/writetec.h"
#include "libcorecommon/key.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 




//////////////////////////////////////////////////////////////////////

template <Dimension DIM>
void Grid<DIM>::Create(const CfgSection* pcfgsec)
{
	ConfigBase::Create( pcfgsec);
}

template <Dimension DIM>
void Grid<DIM>::PostCreateCheck() const
{
	ConfigBase::PostCreateCheck();
}


template <Dimension DIM>
void Grid<DIM>::Init()
{
// 	MGString	fname	= ConfigSect().ValueString( ConfigStr::Solver::Grid::FNAME );
// 	bool		bBIN	= IO::FileTypeToBool( ConfigSect().ValueString( ConfigStr::Solver::Grid::FType::KEY ) );
// 
// 	IO::ReadMSH2	reader( *this);
// 	reader.DoRead( fname, bBIN);
// 
// 	IO::WriteMSH2	writer( *this);
// 	writer.DoWrite( "out.msb2", true);
// 	writer.DoWrite( "out.msh2", false);
// 
// 	////////////////////////////////////////

	mMaxAngle = ConfigSect().ValueFloat( ConfigStr::Solver::Grid::MAXANGLE );


	InitBNodes();

	InitNodeVolume();

	printf( " band width = %d / %d\n", CalcBandWidth(), SizeNodeTab() );

	///////////////////////////
	//CheckBFaceVn();
}

template <Dimension DIM>
void Grid<DIM>::InitGlobId()
{
	for ( MGSize i=0; i<SizeNodeTab(); ++i)
		mtabNode[i].rGId() = i;

	mrangeOwnedNodes.first	= 0;
	mrangeOwnedNodes.second	= SizeNodeTab();
}


template <Dimension DIM>
void Grid<DIM>::CheckBFaceVn()
{
	typedef pair< Key<DIM>, MGSize > MyPair;

	map< Key<DIM>, MGSize > mapinface;
	CGeomCell<DIM>	gcell;
	Key<DIM>	key;

	for ( MGSize i=0; i<SizeCellTab(); ++i)
	{
		gcell = cCell( i);

		for ( MGSize ifc=0; ifc<DIM+1; ++ifc)
		{
			for ( MGSize k=0; k<DIM; ++k)
				key.rElem(k) = gcell.cId( CGeomCell<DIM>::cFaceConn( ifc, k) ); 

			key.Sort();

			mapinface.insert( typename map< Key<DIM>, MGSize >::value_type( key,i ) );
		}
	}

	map<MGSize,bool> mapvn;


	for ( MGSize i=0; i<SizeBFaceTab(); ++i)
	{
		CGeomBFace<DIM>		bface = cBFace(i);

		for ( MGSize k=0; k<DIM; ++k)
			key.rElem(k) = bface.cId( k ); 
		
		key.Sort();

		typename map< Key<DIM>, MGSize >::iterator itr = mapinface.find( key);
		if ( itr == mapinface.end() )
			THROW_INTERNAL( "mesh corrupted !!!");

		gcell = cCell( itr->second );


		Vect<DIM>	vct(0.0);

		for ( MGSize k=0; k<DIM+1; ++k)
			vct += gcell.cNode(k).cPos();

		vct /= (MGFloat)(DIM+1);

		Vect<DIM>	vctf(0.0);

		for ( MGSize k=0; k<DIM; ++k)
			vctf += bface.cNode(k).cPos();

		vctf /= (MGFloat)(DIM);

		Vect<DIM>	dvct = vct - vctf;

		if ( dvct * bface.Vn() < 0 )
			mapvn[ bface.cSurfId() ] = false;
		else
			mapvn[ bface.cSurfId() ] = true;
	}

	for ( map<MGSize,bool>::iterator ii=mapvn.begin(); ii!=mapvn.end(); ++ii)
		cout << "surf id " << ii->first << " : " << ii->second << endl;
	cout << endl << " ---- OK ----" << endl;
}


//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
void Grid<DIM>::InitBNodes()
{
	// boundary node is NOT unique - every face has its own proxy to boundary node
	

	if ( SizeBFaceTab() == 0 )
	{
		cout << " ------------- SizeBFaceTab() == 0 ---------------" << endl;
		return;
	}


	CGeomBFace<DIM>		bfac;
	Vect<DIM>	vct;

	MGSize	n=0, idcur;


	for ( MGSize i=0; i<SizeBFaceTab(); ++i)
	{
		//bfac = cBFace( i);
		mcolBFace.GetCBFaceIds( bfac, i);
		n += bfac.Size();
	}
	//cout << "003 " << n << endl;

	mtabBNode.reserve( n);


	// preparing table of references to bnodes and real nodes (used for averaging)
	idcur = 0;
	for ( MGSize i=0; i<SizeBFaceTab(); ++i)
	{
		mcolBFace.GetCBFaceIds( bfac, i);
		for ( MGSize k=0; k<bfac.Size(); ++k)
			bfac.rNode(k) = mtabNode[bfac.cId(k)];
		
		for ( MGSize k=0; k<bfac.Size(); ++k)
		{
			BoundaryNode<DIM> bnod;
			
			bnod.rPId() = bfac.cId(k);
			bnod.rPFId() = i;	// ????
			bnod.rVn() = bfac.Vn();
			mtabBNode.push_back( bnod);

			bfac.rBId(k) = idcur++;
		}

		mcolBFace.SetCBFaceBndIds( bfac, i);
	}
	

	vector< pair< MGSize, MGSize> >	tabid;
	tabid.resize( mtabBNode.size() );
	
	for ( MGSize i=0; i<mtabBNode.size(); ++i)
	{
		tabid[i].first = mtabBNode[i].cPId();
		tabid[i].second = i;
	}

	// Averaging bnode vn by taking into account faces with similar vn	
	sort( tabid.begin(), tabid.end() );

	//ofstream of("__dump.dat");
	//for ( MGSize i=0; i<tabid.size(); ++i)
	//{
	//	of << i << " :    " << tabid[i].first << " - " << tabid[i].second << endl;
	//}
	//of.close();

	vector< pair< MGSize, MGSize> >::iterator	itr, itri, itrb, itre;
	pair< MGSize, MGSize>	elem(0,0);
	
	elem.first = tabid[0].first;
	itre = upper_bound( tabid.begin(), tabid.end(), elem);
	if ( itre == tabid.end() )
		THROW_INTERNAL( "itrb = upper_bound(...) not found");
	

	do
	{
		elem.first = itre->first + 1;
		itrb = itre;		

		itre = lower_bound( tabid.begin(), tabid.end(), elem);

		for ( itr=itrb; itr!=itre; ++itr)
		{
			
			BoundaryNode<DIM>	&pnod = mtabBNode[itr->second];
			CGeomBFace<DIM>		pfac;
			
			mcolBFace.GetCBFaceIds( pfac, pnod.cPFId() );
			for ( MGSize k=0; k<pfac.Size(); ++k)
				pfac.rNode(k) = mtabNode[pfac.cId(k)];

			Vect<DIM>	vnpf = pfac.Vn();

			MGFloat				wgh = vnpf.module();
			Vect<DIM>	vsum = vnpf;

			for ( itri=itrb; itri!=itre; ++itri)
			{
				if ( itr != itri)
				{
					BoundaryNode<DIM>	&nod = mtabBNode[itri->second];
					CGeomBFace<DIM>		fac;

					mcolBFace.GetCBFaceIds( fac, nod.cPFId() );
					for ( MGSize k=0; k<fac.Size(); ++k)
						fac.rNode(k) = mtabNode[fac.cId(k)];

					Vect<DIM>	vnf = fac.Vn();
				
					if ( vnf.versor() * vnpf.versor() > mMaxAngle )
					{
						wgh += vnf.module();
						vsum += vnf;
					}
				}
			}
			
			vsum /= wgh;
			pnod.rVn() = vsum;
		}

	}
	while ( itre != tabid.end() );

}

/*
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
void Grid<DIM>::InitBNodes()
{
	set<MGSize>			setbnod;
	map<MGSize,MGSize>	mapbnod;
	map<MGSize,MGSize>::iterator	itr;
	vector<MGFloat>		tabWgh;

	CGeomBFace<DIM>		bfac;
	Vect<DIM>	vct;

	MGSize	n, id, idcur;

	for ( MGSize i=0; i<SizeBFaceTab(); ++i)
	{
		//bfac = cBFace( i);
		mcolBFace.GetCBFaceIds( bfac, i);
		for ( MGSize k=0; k<bfac.Size(); ++k)
			setbnod.insert( bfac.cId(k) );
	}
	n = setbnod.size();
	printf( "BNode no = %d\n", setbnod.size() );
	setbnod.clear();

	mtabBNode.resize( n);
	tabWgh.resize( n);
	idcur = 0;
	for ( MGSize i=0; i<SizeBFaceTab(); ++i)
	{
//		bfac = cBFace( i);
		mcolBFace.GetCBFaceIds( bfac, i);
		for ( MGSize k=0; k<bfac.Size(); ++k)
			bfac.rNode(k) = mtabNode[bfac.cId(k)];

		vct = bfac.Vn();
		for ( MGSize k=0; k<bfac.Size(); ++k)
		{
			if ( (itr = mapbnod.find( bfac.cId(k) )) != mapbnod.end() )
			{
				id = (*itr).second;
			}
			else
			{
				id = idcur;
				mapbnod.insert( map<MGSize,MGSize>::value_type( bfac.cId(k), idcur++) );
				mtabBNode[id].rPId() = bfac.cId(k);
				mtabBNode[id].rVn() = Vect<DIM>();
				tabWgh[id] = 0.0;
			}
			
			bfac.rBId(k) = id;

			mtabBNode[id].rVn() += vct;
			tabWgh[id] += vct.module();
		}

		mcolBFace.SetCBFaceBndIds( bfac, i);
	}

	for ( MGSize i=0; i<mtabBNode.size(); ++i)
	{
		//mtabBNode[i].rVn() = mtabBNode[i].cVn().versor();
		mtabBNode[i].rVn() = mtabBNode[i].cVn() / tabWgh[i];
	}

}
*/


template <Dimension DIM>
void Grid<DIM>::InitNodeVolume()
{
	//char	sbuf[1024];
	CGeomCell<DIM>	gcell;
	MGFloat	vol, voltot = 0.0;

	for ( MGSize i=0; i<SizeCellTab(); ++i)
	{
		gcell = cCell( i);
		voltot += vol = gcell.Volume();
		//sprintf( sbuf, "cell id = %d   vol = %lg", i, vol);
		//TRACE( sbuf);
		//sprintf( sbuf, "Type = %d", gcell.cType() );
		//TRACE( sbuf);
		//sprintf( sbuf, "Node ids: %d %d %d", gcell.cId(0), gcell.cId(1), gcell.cId(2) );
		//TRACE( sbuf);
		//TRACE( "\n");
		
		vol /= gcell.Size();
		for ( MGSize k=0; k<gcell.Size(); ++k)
			mtabNode[gcell.cId(k)].rVolume() += vol;
	}

	printf( "Grid volume (cell sum) = %lg\n", voltot);

	voltot = 0.0;
	for ( MGSize i=0; i< mtabNode.size(); ++i)
	{
		mtabNode[i].rVolume() = mtabNode[i].cVolume() == 0.0 ? 1.0e-16 : mtabNode[i].cVolume();
		voltot += mtabNode[i].cVolume();
	}

	printf( "Grid volume (nod sum) = %lg\n", voltot);
}


template <Dimension DIM>
MGSize Grid<DIM>::CalcBandWidth()
{
	MGSize	bret=0, b;
	CGeomCell<DIM>	gcell;

	for ( MGSize i=0; i<SizeCellTab(); ++i)
	{
		gcell = cCell( i);
		for ( MGSize j=0; j<gcell.Size(); ++j)
			for ( MGSize k=j+1; k<gcell.Size(); ++k)
			{
				b = ::abs( (MGInt) gcell.cId(k) - (MGInt)gcell.cId(j) );
				if ( b > bret) 
					bret = b;
			}
	}

	return bret;
}

//////////////////////////////////////////////////////////////////////
template class Grid<DIM_2D>;
template class Grid<DIM_3D>;

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

