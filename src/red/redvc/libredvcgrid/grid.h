#ifndef __GRID_H__
#define __GRID_H__

#include "libcoresystem/mgdecl.h"
#include "libcoreio/gridfacade.h"
#include "redvc/libredvcgrid/node.h"
#include "redvc/libredvcgrid/cellcollection.h"
#include "redvc/libredvcgrid/bfacecollection.h"
#include "redvc/libredvcgrid/compgeomcell.h"
#include "redvc/libredvcgrid/compgeombface.h"

#include "libcoreconfig/configbase.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



//////////////////////////////////////////////////////////////////////
// class Grid
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class Grid : public ConfigBase, public IO::GridBndFacade, public IO::GridReadFacade
{
public:
	Grid() : ConfigBase(), mRefLength(1.0)	{}

	virtual	 ~Grid()					{}

	virtual void Create(const CfgSection* pcfgsec);
	virtual void PostCreateCheck() const;
	virtual void Init();

	void	InitGlobId();

	MGString	Name() const	{ return mGrdName;}

	MGSize	SizeNodeTab() const		{ return mtabNode.size(); }
	MGSize	SizeCellTab() const		{ return mcolCell.Size(); }
	MGSize	SizeBNodeTab() const	{ return mtabBNode.size(); }
	MGSize	SizeBFaceTab() const	{ return mcolBFace.Size(); }

	const Node<DIM>&			cNode( const MGSize& i) const;
	CGeomCell<DIM>				cCell( const MGSize& i) const;
	const BoundaryNode<DIM>&	cBNode( const MGSize& i) const;
	CGeomBFace<DIM>				cBFace( const MGSize& i) const;

	void	GetCell( const MGSize& i, CGeomCell<DIM>& cell) const;
	void	GetBFace( const MGSize& i, CGeomBFace<DIM>& face) const;

	MGSize	CalcBandWidth();

	void	CheckBFaceVn();


protected:

	virtual Dimension	Dim() const		{ return DIM;}

	virtual MGSize	IOSizeNodeTab() const										{ return mtabNode.size(); }
	virtual MGSize	IOSizeCellTab( const MGSize& type) const					{ return mcolCell.Size( type);}
	virtual MGSize	IOSizeBFaceTab( const MGSize& type) const					{ return mcolBFace.Size( type);}

	virtual void	IOGetNodePos( vector<MGFloat>& tabx, const MGSize& id) const;
	virtual void	IOGetCellIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const;
	virtual void	IOGetBFaceIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const;
	virtual void	IOGetBFaceSurf( MGSize& idsurf, const MGSize& type, const MGSize& id)const;

	virtual MGSize	IONumCellTypes() const										{ return mcolCell.NTypes();}
	virtual MGSize	IONumBFaceTypes() const										{ return mcolBFace.NTypes();}
	virtual MGSize	IOCellType( const MGSize& i) const							{ return mcolCell.Type( i); }
	virtual MGSize	IOBFaceType( const MGSize& i) const							{ return mcolBFace.Type( i);}

	virtual void	IOTabNodeResize( const MGSize& n)							{ mtabNode.resize( n);}
	virtual void	IOTabCellResize( const MGSize& type, const MGSize& n)		{ mcolCell.Resize( type, n);}
	virtual void	IOTabBFaceResize( const MGSize& type, const MGSize& n)		{ mcolBFace.Resize( type, n);}

	virtual void	IOSetNodePos( const vector<MGFloat>& tabx, const MGSize& id);
	virtual void	IOSetCellIds( const vector<MGSize>& tabid, const MGSize& type, const MGSize& id );
	virtual void	IOSetBFaceIds( const vector<MGSize>& tabid, const MGSize& type, const MGSize& id );
	virtual void	IOSetBFaceSurf( const MGSize& idsurf, const MGSize& type, const MGSize& id );

	virtual void	IOGetName( MGString& name) const							{ name = mGrdName;}
	virtual void	IOSetName( const MGString& name)							{ mGrdName = name;}

	////////////////////
	virtual MGSize	IOSizeBNodeTab() const										{ return mtabBNode.size();}

	virtual void	IOGetBNodePos( vector<MGFloat>& tabx, const MGSize& id) const;
	virtual void	IOGetBNodeVn( vector<MGFloat>& tabx, const MGSize& id) const;
	virtual void	IOGetBNodePId( MGSize& idparent, const MGSize& id) const	{ idparent = mtabBNode[id].cPId();}
	virtual void	IOGetBFaceBIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const;


private:

	void	InitBNodes();
	void	InitNodeVolume();

private:

	MGString		mGrdName;

	MGFloat			mMaxAngle;
	MGFloat			mRefLength;

	pair<MGSize,MGSize>			mrangeOwnedNodes;
	vector< Node<DIM> >			mtabNode;
	vector< BoundaryNode<DIM> >	mtabBNode;
	CellCollection<DIM>			mcolCell;
	BFaceCollection<DIM>		mcolBFace;
};
//////////////////////////////////////////////////////////////////////



template <Dimension DIM>
inline void Grid<DIM>::GetCell( const MGSize& i, CGeomCell<DIM>& cell) const
{
	mcolCell.GetCCellIds( cell, i);
	for ( MGSize k=0; k<cell.Size(); ++k)
		cell.rNode(k) = mtabNode[cell.cId(k)];

	cell.CalcVn();
}


template <Dimension DIM>
inline void Grid<DIM>::GetBFace( const MGSize& i, CGeomBFace<DIM>& face) const
{
	mcolBFace.GetCBFaceBndIds( face, i);
	for ( MGSize k=0; k<face.Size(); ++k)
	{
		face.rNode(k) = mtabNode[face.cId(k)];
		face.rBNode(k) = mtabBNode[face.cBId(k)];
		ASSERT( face.cBNode(k).cPId() == face.cId(k) );
	}
}





template <Dimension DIM>
inline const Node<DIM>& Grid<DIM>::cNode( const MGSize& i) const
{
	return mtabNode[i];
}

template <Dimension DIM>
inline const BoundaryNode<DIM>& Grid<DIM>::cBNode( const MGSize& i) const
{
	return mtabBNode[i];
}

template <Dimension DIM>
inline CGeomCell<DIM> Grid<DIM>::cCell( const MGSize& i) const
{
	CGeomCell<DIM>	cell;
	GetCell( i, cell);
	
	return cell;
	//CGeomCell<DIM>	cell;
	//mcolCell.GetCCellIds( cell, i);
	//for ( MGSize k=0; k<cell.Size(); ++k)
	//	cell.rNode(k) = mtabNode[cell.cId(k)];

	//cell.CalcVn();

	//return cell;
}


template <Dimension DIM>
inline CGeomBFace<DIM> Grid<DIM>::cBFace( const MGSize& i) const
{
	CGeomBFace<DIM>	face;
	GetBFace( i, face);

	return face;
	//CGeomBFace<DIM>	face;
	//mcolBFace.GetCBFaceBndIds( face, i);
	//for ( MGSize k=0; k<face.Size(); ++k)
	//{
	//	face.rNode(k) = mtabNode[face.cId(k)];
	//	face.rBNode(k) = mtabBNode[face.cBId(k)];
	//	ASSERT( face.cBNode(k).cPId() == face.cId(k) );
	//}

	//return face;
}

//////////////////////////////////////////////////////////////////////


template <Dimension DIM>
inline void Grid<DIM>::IOGetNodePos( vector<MGFloat>& tabx, const MGSize& id) const
{
	ASSERT( tabx.size() == DIM);
	for ( MGSize i=0; i<DIM; ++i)
		tabx[i] = mtabNode[id].cPos().cX(i) * mRefLength;
}


template <Dimension DIM>
inline void Grid<DIM>::IOGetCellIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const
{
	ASSERT( tabid.size() == type);
	mcolCell.GetCellIds( tabid, type, id);
}


template <Dimension DIM>
inline void Grid<DIM>::IOGetBFaceIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const
{
	ASSERT( tabid.size() == type);
	mcolBFace.GetBFaceIds( tabid, type, id);
}

template <Dimension DIM>
inline void Grid<DIM>::IOGetBFaceSurf(MGSize& idsurf, const MGSize& type, const MGSize& id) const
{
	mcolBFace.GetBFaceSurf( idsurf, type, id);
}

template <Dimension DIM>
inline void Grid<DIM>::IOSetNodePos( const vector<MGFloat>& tabx, const MGSize& id )
{
	ASSERT( tabx.size() == DIM);
	for ( MGSize i=0; i<DIM; ++i)
		mtabNode[id].rPos().rX(i) = tabx[i] / mRefLength;
}


template <Dimension DIM>
inline void Grid<DIM>::IOSetCellIds( const vector<MGSize>& tabid, const MGSize& type, const MGSize& id )
{
	ASSERT( tabid.size() == type);
	mcolCell.SetCellIds( tabid, type, id);
}


template <Dimension DIM>
inline void Grid<DIM>::IOSetBFaceIds( const vector<MGSize>& tabid, const MGSize& type, const MGSize& id )
{
	mcolBFace.SetBFaceIds( tabid, type, id);
}

template <Dimension DIM>
inline void Grid<DIM>::IOSetBFaceSurf( const MGSize& idsurf, const MGSize& type, const MGSize& id )
{
	mcolBFace.SetBFaceSurf( idsurf, type, id);
}


//////////////////////

template <Dimension DIM>
inline void Grid<DIM>::IOGetBNodePos( vector<MGFloat>& tabx, const MGSize& id) const
{
	ASSERT( tabx.size() == DIM);
	for ( MGSize i=0; i<DIM; ++i)
		tabx[i] = mtabNode[ mtabBNode[id].cPId() ].cPos().cX(i) * mRefLength;
}

template <Dimension DIM>
inline void Grid<DIM>::IOGetBNodeVn( vector<MGFloat>& tabx, const MGSize& id) const
{
	ASSERT( tabx.size() == DIM);
	for ( MGSize i=0; i<DIM; ++i)
		tabx[i] = mtabBNode[id].cVn().cX( (MGInt)i);
}

template <Dimension DIM>
inline void Grid<DIM>::IOGetBFaceBIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const
{
	ASSERT( tabid.size() == type);
	mcolBFace.GetBFaceBIds( tabid, type, id);
}




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __GRID_H__
