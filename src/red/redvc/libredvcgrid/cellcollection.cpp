#include "cellcollection.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



//////////////////////////////////////////////////////////////////////
// class CellCollection<DIM_2D>
//////////////////////////////////////////////////////////////////////

void CellCollection<DIM_2D>::GetCCellIds( CGeomCell<DIM_2D>& ccell, const MGSize& id) const
{
	MGSize i = id;
	if ( i < mtabTri.size() )
	{
		ccell.rType() = CELLSIZE_TRI;
		for ( MGSize k=0; k<CELLSIZE_TRI; ++k)
			ccell.rId(k) = mtabTri[i].cId(k);
	}
	else if  ( (i-=mtabTri.size()) < mtabQuad.size() )
	{
		ccell.rType() = CELLSIZE_QUAD;
		for ( MGSize k=0; k<CELLSIZE_QUAD; ++k)
			ccell.rId(k) = mtabQuad[i].cId(k);
	}
	else
		THROW_INTERNAL( "CellCollection<DIM_2D>::GetCellIds: out of range");
}

void CellCollection<DIM_2D>::GetCellIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const
{
	ASSERT( tabid.size() == type);
	switch ( type)
	{
	case CELLSIZE_TRI:
		for ( MGSize k=0; k<CELLSIZE_TRI; ++k)
			tabid[k] = mtabTri[id].cId(k);
		break;

	case CELLSIZE_QUAD:
		for ( MGSize k=0; k<CELLSIZE_QUAD; ++k)
			tabid[k] = mtabQuad[id].cId(k);
		break;

	default:
		THROW_INTERNAL( "from CellCollection<DIM_2D>::GetCellIds - bad cell type" );
	}
}

void CellCollection<DIM_2D>::SetCellIds( const vector<MGSize>& tabid, const MGSize& type, const MGSize& id)
{
	ASSERT( tabid.size() == type);
	switch ( type)
	{
	case CELLSIZE_TRI:
		for ( MGSize k=0; k<CELLSIZE_TRI; ++k)
			mtabTri[id].rId(k) = tabid[k];
		break;

	case CELLSIZE_QUAD:
		for ( MGSize k=0; k<CELLSIZE_QUAD; ++k)
			mtabQuad[id].rId(k) = tabid[k];
		break;

	default:
		THROW_INTERNAL( "from CellCollection<DIM_2D>::GetCellIds - bad cell type" );
	}
}

void CellCollection<DIM_2D>::Resize( const MGSize& type, const MGSize& n)
{
	switch ( type)
	{
	case CELLSIZE_TRI:
		mtabTri.resize( n);
		break;

	case CELLSIZE_QUAD:
		mtabQuad.resize( n);
		break;

	default:
		THROW_INTERNAL( "from CellCollection<DIM_2D>::TabResize - bad cell type" );
	}
}



//////////////////////////////////////////////////////////////////////
// class CellCollection<DIM_3D>
//////////////////////////////////////////////////////////////////////

void CellCollection<DIM_3D>::GetCCellIds( CGeomCell<DIM_3D>& ccell, const MGSize& id) const
{
	MGSize i = id;
	if ( i < mtabTet.size() )
	{
		ccell.rType() = CELLSIZE_TET;
		for ( MGSize k=0; k<CELLSIZE_TET; ++k)
			ccell.rId(k) = mtabTet[i].cId(k);
	}
	else if  ( (i-=mtabTet.size()) < mtabHex.size() )
	{
		ccell.rType() = CELLSIZE_HEX;
		for ( MGSize k=0; k<CELLSIZE_HEX; ++k)
			ccell.rId(k) = mtabHex[i].cId(k);
	}
	else if  ( (i-=mtabHex.size()) < mtabPrism.size() )
	{
		ccell.rType() = CELLSIZE_PRISM;
		for ( MGSize k=0; k<CELLSIZE_PRISM; ++k)
			ccell.rId(k) = mtabPrism[i].cId(k);
	}
	else if  ( (i-=mtabPrism.size()) < mtabPiram.size() )
	{
		ccell.rType() = CELLSIZE_PIRAM;
		for ( MGSize k=0; k<CELLSIZE_PIRAM; ++k)
			ccell.rId(k) = mtabPiram[i].cId(k);
	}
	else
		THROW_INTERNAL( "CellCollection<DIM_3D>::GetCellIds: out of range");
}


void CellCollection<DIM_3D>::GetCellIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const
{
	ASSERT( tabid.size() == type);
	switch ( type)
	{
	case CELLSIZE_TET:
		for ( MGSize k=0; k<CELLSIZE_TET; ++k)
			tabid[k] = mtabTet[id].cId(k);
		break;

	case CELLSIZE_HEX:
		for ( MGSize k=0; k<CELLSIZE_HEX; ++k)
			tabid[k] = mtabHex[id].cId(k);
		break;

	case CELLSIZE_PRISM:
		for ( MGSize k=0; k<CELLSIZE_PRISM; ++k)
			tabid[k] = mtabPrism[id].cId(k);
		break;

	case CELLSIZE_PIRAM:
		for ( MGSize k=0; k<CELLSIZE_PIRAM; ++k)
			tabid[k] = mtabPiram[id].cId(k);
		break;

	default:
		THROW_INTERNAL( "from CellCollection<DIM_3D>::GetCellIds - bad cell type" );
	}
}

void CellCollection<DIM_3D>::SetCellIds( const vector<MGSize>& tabid, const MGSize& type, const MGSize& id)
{
	ASSERT( tabid.size() == type);
	switch ( type)
	{
	case CELLSIZE_TET:
		for ( MGSize k=0; k<CELLSIZE_TET; ++k)
			mtabTet[id].rId(k) = tabid[k];
		break;

	case CELLSIZE_HEX:
		for ( MGSize k=0; k<CELLSIZE_HEX; ++k)
			mtabHex[id].rId(k) = tabid[k];
		break;

	case CELLSIZE_PRISM:
		for ( MGSize k=0; k<CELLSIZE_PRISM; ++k)
			mtabPrism[id].rId(k) = tabid[k];
		break;

	case CELLSIZE_PIRAM:
		for ( MGSize k=0; k<CELLSIZE_PIRAM; ++k)
			mtabPiram[id].rId(k) = tabid[k];
		break;


	default:
		THROW_INTERNAL( "from CellCollection<DIM_3D>::GetCellIds - bad cell type" );
	}
}


void CellCollection<DIM_3D>::Resize( const MGSize& type, const MGSize& n)
{
	switch ( type)
	{
	case CELLSIZE_TET:
		mtabTet.resize( n);
		break;

	case CELLSIZE_HEX:
		mtabHex.resize( n);
		break;

	case CELLSIZE_PRISM:
		mtabPrism.resize( n);
		break;

	case CELLSIZE_PIRAM:
		mtabPiram.resize( n);
		break;

	default:
		THROW_INTERNAL( "from CellCollection<DIM_3D>::TabResize - bad cell type" );
	}
}



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

