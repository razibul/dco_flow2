#ifndef __COMPGEOMBFACE_H__
#define __COMPGEOMBFACE_H__

#include "libcoresystem/mgdecl.h"
#include "redvc/libredvcgrid/gridconst.h"
#include "redvc/libredvcgrid/compgeoment.h"
#include "redvc/libredvcgrid/compgeomcell.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



//const MGFloat GHOSTCELL_EPS = 1.0e-6;
const MGFloat GHOSTCELL_EPS = 1.0e-6;


//////////////////////////////////////////////////////////////////////
// class CGeomBFace
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class CGeomBFace
{
};

//////////////////////////////////////////////////////////////////////
// class CGeomBFace<DIM_2D>
//////////////////////////////////////////////////////////////////////
template <>
class CGeomBFace<DIM_2D> : public CGeomBndEnt<DIM_2D,BFACESIZE_EDGE>
{
public:
	CGeomBFace() : CGeomBndEnt<DIM_2D,BFACESIZE_EDGE>()		{}
	CGeomBFace( const CGeomBFace<DIM_2D>& cbface) : CGeomBndEnt<DIM_2D,BFACESIZE_EDGE>( cbface)	{}

	Vect<DIM_2D>	Vn() const;

	void	GetStringIds( char sbuf[]) const;

	void	CreateGCell( CGeomCell<DIM_2D>& cell, const MGSize& id) const;

};


inline Vect<DIM_2D> CGeomBFace<DIM_2D>::Vn() const
{
	Vect<DIM_2D> vct, v1;
	v1 = cNode(1).cPos() - cNode(0).cPos();
	vct.rX() = -v1.cY();
	vct.rY() = v1.cX();
	return vct;
}


inline void CGeomBFace<DIM_2D>::GetStringIds( char sbuf[]) const
{
	sprintf( sbuf, "%d %d", cBId(0)+1, cBId(1)+1 );
}


inline void CGeomBFace<DIM_2D>::CreateGCell( CGeomCell<DIM_2D>& cell, const MGSize& id) const
{
	MGFloat	r, dist;
	Vect2D	vn, vnn, vt, vv, vvn;

	vt = cNode(1).cPos() - cNode(0).cPos();

	//vn = Vn();
	vn = cBNode(id).cVn();
	r = 2*vn.module() / vt.module();
	if ( vn.module() < 0.9 )
	{
		vn = Vn();
		r = 0;
	}

	vnn = vn.versor();
	dist = GHOSTCELL_EPS*vt.module();

	cell.rType() = CELLSIZE_TRI;

	//cell.rNode(0) = Node<DIM_2D>( cNode(id).cPos() - GHOSTCELL_EPS*cBNode(id).cVn()*vt.module() );
	cell.rNode(0) = Node<DIM_2D>( cNode(id).cPos() - vnn*dist );
	cell.rNode(1) = cNode(1);
	cell.rNode(2) = cNode(0);

	cell.CalcVn();

	//if ( cell.cNode(0).cPos().cX() > 1.5 )
	//	cout << cell.cNode(0).cPos().cX() << endl;
}


//////////////////////////////////////////////////////////////////////
// class CGeomBFace<DIM_3D>
//////////////////////////////////////////////////////////////////////
template <>
class CGeomBFace<DIM_3D> : public CGeomBndEnt<DIM_3D,BFACESIZE_QUAD>
{
public:
	CGeomBFace() : CGeomBndEnt<DIM_3D,BFACESIZE_QUAD>()		{}
	CGeomBFace( const CGeomBFace<DIM_3D>& cbface) : CGeomBndEnt<DIM_3D,BFACESIZE_QUAD>( cbface)	{}

	Vect<DIM_3D>	Vn() const;

	void	GetStringIds( char sbuf[]) const;

	void	CreateGCell( CGeomCell<DIM_3D>& cell, const MGSize& id) const;
};


inline Vect<DIM_3D> CGeomBFace<DIM_3D>::Vn() const
{
	Vect<DIM_3D> vct, v1, v2;
	v1 = cNode(1).cPos() - cNode(0).cPos();
	v2 = cNode(2).cPos() - cNode(0).cPos();
	vct = 0.5* (v1 % v2);
	return vct;
}

inline void CGeomBFace<DIM_3D>::GetStringIds( char sbuf[]) const
{
	switch ( cType() )
	{
	case CELLSIZE_TRI:
		{
			sprintf( sbuf, "%d %d %d %d", 
					cBId(0)+1, cBId(1)+1, cBId(2)+1, cBId(2)+1 );
			break;
		}
	case CELLSIZE_QUAD:
		{
			sprintf( sbuf, "%d %d %d %d", 
					cBId(0)+1, cBId(1)+1, cBId(2)+1, cBId(3)+1 );
			break;
		}
	default:
		{
			char stmp[128];
			sprintf( stmp, "Unrecognized bface type: %d", cType() );
			THROW_INTERNAL( stmp);
		}
	}
}

inline void CGeomBFace<DIM_3D>::CreateGCell( CGeomCell<DIM_3D>& cell, const MGSize& id) const
{
	MGFloat	s;
	Vect3D	vn, vnn, vt, vv, vvn;


	//vn = Vn();
	vn = cBNode(id).cVn();
	if ( vn.module() < 0.9 )
	{
		vn = Vn();
	}


	vnn = vn.versor();
	s =   (cNode(2).cPos() - cNode(1).cPos()).module() 
		+ (cNode(1).cPos() - cNode(0).cPos()).module()
		+ (cNode(0).cPos() - cNode(2).cPos()).module();
	s /= 3.0;

	cell.rType() = CELLSIZE_TET; 

	MGFloat le = max( 1.0e-9, GHOSTCELL_EPS*s);

	cell.rNode(0) = Node<DIM_3D>( cNode(id).cPos() - le*vnn );

	//cell.rNode(0) = Node<DIM_3D>( cNode(id).cPos() - GHOSTCELL_EPS*s*vnn );
	cell.rNode(1) = cNode(2);
	cell.rNode(2) = cNode(0);
	cell.rNode(3) = cNode(1);

	cell.CalcVn();
}



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __COMPGEOMBFACE_H__
