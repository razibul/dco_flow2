#include "compgeomcell.h"
#include "libcoregeom/geom.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



MGSize	CGeomCell<DIM_2D>::mtabFaceConn[DIM_2D+1][DIM_2D] = { {2, 1}, {0,2}, {1,0} };

void CGeomCell<DIM_2D>::CalcVn()
{
	switch ( Size() )
	{
	case CELLSIZE_TRI:
		mtabVn[0] = Geom::VnSeg( cNode(1).cPos(), cNode(2).cPos() );
		mtabVn[1] = Geom::VnSeg( cNode(2).cPos(), cNode(0).cPos() );
		mtabVn[2] = Geom::VnSeg( cNode(0).cPos(), cNode(1).cPos() );
		break;

	case CELLSIZE_QUAD:
		mtabVn[0] = Geom::VnSeg( cNode(0).cPos(), cNode(1).cPos() );
		mtabVn[1] = Geom::VnSeg( cNode(1).cPos(), cNode(2).cPos() );
		mtabVn[2] = Geom::VnSeg( cNode(2).cPos(), cNode(3).cPos() );
		mtabVn[3] = Geom::VnSeg( cNode(3).cPos(), cNode(0).cPos() );
		break;

	default:
		THROW_INTERNAL( "CalcVn: unknown cell type");
	}
}





MGSize	CGeomCell<DIM_3D>::mtabFaceConn[DIM_3D+1][DIM_3D] = { {1,2,3}, {0,3,2}, {0,1,3}, {0,2,1}};


void CGeomCell<DIM_3D>::CalcVn()
{
	switch ( Size() )
	{
	case CELLSIZE_TET:
		{
			//mtabVn[0] = Geom::VnFac( cNode(1).cPos(), cNode(2).cPos(), cNode(3).cPos() );
			//mtabVn[1] = Geom::VnFac( cNode(2).cPos(), cNode(0).cPos(), cNode(3).cPos() );
			//mtabVn[2] = Geom::VnFac( cNode(0).cPos(), cNode(1).cPos(), cNode(3).cPos() );
			//mtabVn[3] = Geom::VnFac( cNode(0).cPos(), cNode(2).cPos(), cNode(1).cPos() );

			mtabVn[0] = Geom::VnFac( cNode(3).cPos(), cNode(2).cPos(), cNode(1).cPos() );
			mtabVn[1] = Geom::VnFac( cNode(3).cPos(), cNode(0).cPos(), cNode(2).cPos() );
			mtabVn[2] = Geom::VnFac( cNode(3).cPos(), cNode(1).cPos(), cNode(0).cPos() );
			mtabVn[3] = Geom::VnFac( cNode(1).cPos(), cNode(2).cPos(), cNode(0).cPos() );

			// correction
			Vect3D vsum = mtabVn[0] + mtabVn[1] + mtabVn[2] + mtabVn[3];
			MGFloat asum = mtabVn[0].module() + mtabVn[1].module() + mtabVn[2].module() + mtabVn[3].module();
			vsum = vsum / asum;
		
			mtabVn[0] -= vsum * mtabVn[0].module();
			mtabVn[1] -= vsum * mtabVn[1].module();
			mtabVn[2] -= vsum * mtabVn[2].module();
			mtabVn[3] -= vsum * mtabVn[3].module();

			break;
		}
	case CELLSIZE_HEX:
	case CELLSIZE_PRISM:
	case CELLSIZE_PIRAM:
	default:
		THROW_INTERNAL( "CalcVn: unknown cell type");
	}
}



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

