#ifndef __COMPGEOMCELL_H__
#define __COMPGEOMCELL_H__

#include "libcoresystem/mgdecl.h"
#include "redvc/libredvcgrid/gridconst.h"
#include "redvc/libredvcgrid/compgeoment.h"

#include "libcoregeom/geom.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



//////////////////////////////////////////////////////////////////////
// class CGeomCell
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class CGeomCell
{
};


//////////////////////////////////////////////////////////////////////
// class CGeomCell<DIM_2D>
//////////////////////////////////////////////////////////////////////
template <>
class CGeomCell<DIM_2D> : public CGeomEnt<DIM_2D,CELLSIZE_QUAD>
{
public:
	CGeomCell() : CGeomEnt<DIM_2D,CELLSIZE_QUAD>()		{}
	CGeomCell( const CGeomCell<DIM_2D>& ccell);

	MGSize	SizeFace() const;
	void	CalcVn();

	const Vect2D&	Vn( const MGSize& i) const	{ return mtabVn[i];}

	void	GetStringIds( char sbuf[]) const;
	void	Dump( ostream& f) const;


	static MGSize&	cFaceConn( const MGSize& ifc, const MGSize& in)	
													{ return mtabFaceConn[ifc][in];}

private:
	Vect2D	mtabVn[CELLSIZE_QUAD];

	static MGSize	mtabFaceConn[DIM_2D+1][DIM_2D];
};


inline CGeomCell<DIM_2D>::CGeomCell( const CGeomCell<DIM_2D>& ccell) : CGeomEnt<DIM_2D,CELLSIZE_QUAD>( ccell)	
{
	for ( MGSize i=0; i<CELLSIZE_QUAD; ++i)
		mtabVn[i] = ccell.mtabVn[i];
}



inline MGSize CGeomCell<DIM_2D>::SizeFace() const
{
	return Size();
}

inline void CGeomCell<DIM_2D>::GetStringIds( char sbuf[]) const
{
	switch ( cType() )
	{
	case CELLSIZE_TRI:
		{
			sprintf( sbuf, "%d %d %d %d", 
					cId(0)+1, cId(1)+1, cId(2)+1, cId(2)+1 );
			break;
		}
	case CELLSIZE_QUAD:
		{
			sprintf( sbuf, "%d %d %d %d", 
					cId(0)+1, cId(1)+1, cId(2)+1, cId(3)+1 );
			break;
		}
	default:
		{
			char stmp[128];
			sprintf( stmp, "Unrecognized cell type: %d", cType() );
			THROW_INTERNAL( stmp);
		}
	}
}


inline void CGeomCell<DIM_2D>::Dump( ostream& f) const
{
	CGeomEnt<DIM_2D,CELLSIZE_QUAD>::Dump( f);

	for ( MGSize i=0; i<SizeFace(); ++i)
		f << "vn: [" << Vn(i).cX() << ", " << Vn(i).cY() << "]\n";

}


//////////////////////////////////////////////////////////////////////
// class CGeomCell<DIM_3D>
//////////////////////////////////////////////////////////////////////
template <>
class CGeomCell<DIM_3D> : public CGeomEnt<DIM_3D,CELLSIZE_HEX>
{
public:
	CGeomCell() : CGeomEnt<DIM_3D,CELLSIZE_HEX>()		{}
	CGeomCell( const CGeomCell<DIM_3D>& ccell);

	MGSize	SizeFace() const;
	void	CalcVn();

	const Vect3D&	Vn( const MGSize& i) const	{ return mtabVn[i];}

	void	GetStringIds( char sbuf[]) const;
	void	Dump( ostream& f) const;


	static MGSize&	cFaceConn( const MGSize& ifc, const MGSize& in)	
													{ return mtabFaceConn[ifc][in];}

private:
	Vect3D	mtabVn[CELLSIZE_HEX];

	static MGSize	mtabFaceConn[DIM_3D+1][DIM_3D];
};


inline CGeomCell<DIM_3D>::CGeomCell( const CGeomCell<DIM_3D>& ccell) : CGeomEnt<DIM_3D,CELLSIZE_HEX>( ccell)
{
	for ( MGSize i=0; i<CELLSIZE_HEX; ++i)
		mtabVn[i] = ccell.mtabVn[i];
}



inline MGSize CGeomCell<DIM_3D>::SizeFace() const
{
	switch ( Size() )
	{
	case CELLSIZE_TET:
		return CELLNFACE_TET;

	case CELLSIZE_HEX:
		return CELLNFACE_HEX;

	case CELLSIZE_PRISM:
		return CELLNFACE_PRISM;

	case CELLSIZE_PIRAM:
		return CELLNFACE_PIRAM;
	}

	ASSERT( 0);
	return 0;
}


inline void CGeomCell<DIM_3D>::GetStringIds( char sbuf[]) const
{
	switch ( cType() )
	{
	case CELLSIZE_TET:
		{
			sprintf( sbuf, "%d %d %d %d %d %d %d %d", 
					cId(0)+1, cId(1)+1, cId(2)+1, cId(2)+1,
					cId(3)+1, cId(3)+1, cId(3)+1, cId(3)+1 );
			break;
		}
	case CELLSIZE_HEX:
		{
			sprintf( sbuf, "%d %d %d %d %d %d %d %d", 
					cId(0)+1, cId(1)+1, cId(2)+1, cId(3)+1,
					cId(4)+1, cId(5)+1, cId(6)+1, cId(7)+1 );
			break;
		}
	case CELLSIZE_PRISM:
		{
			sprintf( sbuf, "%d %d %d %d %d %d %d %d", 
					cId(0)+1, cId(1)+1, cId(2)+1, cId(2)+1,
					cId(3)+1, cId(4)+1, cId(5)+1, cId(5)+1 );
			break;
		}
	case CELLSIZE_PIRAM:
		{
			sprintf( sbuf, "%d %d %d %d %d %d %d %d", 
					cId(0)+1, cId(1)+1, cId(2)+1, cId(3)+1,
					cId(4)+1, cId(4)+1, cId(4)+1, cId(4)+1 );
			break;
		}
	default:
		{
			char stmp[128];
			sprintf( stmp, "Unrecognized cell type: %d", cType() );
			THROW_INTERNAL( stmp);
		}
	}
}



inline void CGeomCell<DIM_3D>::Dump( ostream& f) const
{
	CGeomEnt<DIM_3D,CELLSIZE_HEX>::Dump( f);

	for ( MGSize i=0; i<SizeFace(); ++i)
		f << "vn: [" << Vn(i).cX() << ", " << Vn(i).cY() << ", " << Vn(i).cZ() << "]\n";

}



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __COMPGEOMCELL_H__
