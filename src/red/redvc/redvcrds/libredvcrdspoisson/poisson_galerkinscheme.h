#ifndef __POISSONPoissonGalerkinScheme_H__
#define __POISSONPoissonGalerkinScheme_H__

#include "redvc/libredvccommon/compflowcell.h"
#include "redvc/libredvccommon/compflowbface.h"

#include "redvc/libredvccommon/flowfunc.h"

#include "redvc/redvcrds/libredvcrdscommon/scldascheme.h"
#include "redvc/redvcrds/libredvcrdscommon/scnscheme.h"
#include "redvc/redvcrds/libredvcrdscommon/scpsischeme.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 




//////////////////////////////////////////////////////////////////////
// class PoissonPoissonGalerkinScheme
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class PoissonGalerkinScheme : public FlowFunc<DIM, EQN_POISSON, EquationDef<DIM,EQN_POISSON>::SIZE>
{
	typedef EquationDef<DIM,EQN_POISSON> FEqns;

	enum { EQSIZE = EquationDef<DIM,EQN_POISSON>::SIZE };
	enum { AUXSIZE = EquationDef<DIM,EQN_POISSON>::AUXSIZE };

	typedef Vect<DIM>				GVec;
	typedef typename FEqns::FVec	FVec;
	typedef typename FEqns::FMtx	FMtx;

	typedef CFlowCell<DIM,EQSIZE,AUXSIZE>	CFCell;
	typedef CFlowBFace<DIM,EQSIZE,AUXSIZE>	CFBFace;




	template <MGSize D>
	class GetZ
	{
	public:
		MGFloat operator () ( const FVec& var) const
		{
			return var( D);
		}
	};


public:
	PoissonGalerkinScheme()	{}

	PoissonGalerkinScheme( const FVec& var) : mRefVar(var)	{}

	virtual void Init( const Physics<DIM,EQN_POISSON>& physics);

	virtual void	CalcResiduum( const CFCell& fcell, FVec tabres[]);
	virtual void	CalcResJacobian( const CFCell& fcell, FVec tabres[], FMtx mtxres[DIM+1][DIM+1]);


protected:

	void	CalcGradU( GVec& vec, const CFCell& fcell) const;

private:
	FVec		mRefVar;
	FVec		mvarZm;
	
	MGFloat		mLapC;
	MGFloat		mLapPhi;
	MGFloat		mConv;

};
//////////////////////////////////////////////////////////////////////

template <Dimension DIM>
void PoissonGalerkinScheme<DIM>::Init( const Physics<DIM,EQN_POISSON>& physics)
{
	mLapC	= physics.cLapC();
	mLapPhi	= physics.cLapPhi();
	mConv	= physics.cConv();

	physics.DimToUndim( mRefVar, physics.cRefVar(), physics.cRefVar() );
	//mRefVar = physics.cRefVar();
}


template <Dimension DIM>
inline void PoissonGalerkinScheme<DIM>::CalcGradU( GVec& vec, const CFCell& fcell) const
{
	vec = fcell.GradVar( GetZ<0>() );
}



template <Dimension DIM>
inline void PoissonGalerkinScheme<DIM>::CalcResiduum( const CFCell& fcell, FVec tabres[])		
{
    const MGFloat volume = fcell.Volume();

	//FVec	varQm;
	//varQm.Init( 0.0);
	//for ( MGSize i=0; i<=DIM; ++i)
	//	varQm += fcell.cVar(i);

	//varQm /= (MGFloat)(DIM+1);




	//GVec	tabvct[DIM];

	//for ( MGSize idim=0; idim<DIM; ++idim)
	//{
	//	tabvct[idim] = GVec(0.0);

	//	for ( MGSize k=0; k<=DIM; ++k)
	//	{
	//		MGFloat var = fcell.cGradVar(k)[0].cX(idim);
	//		tabvct[idim] += var * fcell.Vn( k);
	//		
	//	}

	//	tabvct[idim] /= ( (MGFloat)DIM * volume  );
	//}

	//MGFloat Lap =  tabvct[0].cX(0) + tabvct[1].cX(1);
	////if ( fabs(Lap) > 1.0e-5 )
	////cout  << "Lap = " << Lap << endl;


	//GVec gradu;

	//CalcGradU( gradu, fcell);
	//
 //   MGFloat tabdres[DIM+1];

 //   for ( MGSize i=0; i<=DIM; ++i)
 //   {
 //       GVec vn = fcell.Vn( i);
 //       tabdres[i] = gradu * vn / DIM;
 //   }

	//for ( MGSize i=0; i<DIM+1; ++i)
	//{
	//	tabres[i](0) = tabdres[i];
	//	tabres[i](0) -= 1.0 * volume / MGFloat( DIM + 1 );
	//}
	//return;

	/////////////////////////////////////////////////////////

    MGFloat tabK[DIM+1], tabU[DIM+1];
    //MGFloat tabFi[DIM+1];
	GVec gradu, vecU;

	CalcGradU( gradu, fcell);
	

	MGFloat G = gradu.module();

	vecU = gradu;
	//vecU = vecU.versor();
	vecU = vecU / max(1.0,G);

    MGFloat tabdres[DIM+1];

    for ( MGSize i=0; i<=DIM; ++i)
    {
        GVec vn = fcell.Vn( i);

		tabdres[i]	= gradu * vn /  (MGFloat)DIM;
        tabK[i]		= vecU * vn / (MGFloat)DIM;
        tabU[i]		= max( 0.,fcell.cVar(i)[0]);
    }

	for (MGSize i = 0; i < DIM + 1; ++i)
		tabres[i](0) = tabdres[i];


 //   ScLDAscheme<DIM>::template Calc<MGFloat>( tabK, tabU, tabFi);
 //   //ScNscheme<DIM>::template Calc<MGFloat>( tabK, tabU, tabFi);

	//for ( MGSize i=0; i<DIM+1; ++i)
	//{
	//	tabres[i](0) = 0.0;
	//	
	//	if ( ! fcell.IsOutlet() )
	//		tabres[i](0) += ( mLapC + mLapPhi * fabs(fcell.cVar(i)[0]) ) * tabdres[i];

	//	//tabres[i](0) -= 1.0  * volume / MGFloat( DIM + 1 );
	//	tabres[i](0) -= ( (1.0 - mConv ) +  0.9576*mConv / max( 1.0, G*G) )  * volume / MGFloat( DIM + 1 );

	//	tabres[i](0) += mConv * tabFi[i];
	//}

}


template <Dimension DIM>
inline void PoissonGalerkinScheme<DIM>::CalcResJacobian( const CFCell& fcell, FVec tabres[], FMtx mtxres[DIM+1][DIM+1])		
{
	THROW_INTERNAL("Not implemented !!!");
}




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __POISSONPoissonGalerkinScheme_H__
