#include "poisson_galerkinscheme.h"
#include "libcorecommon/factory.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


void init_FlowFuncPoissonRDS()
{ 
	static ConcCreator< 
		MGString, 
		PoissonGalerkinScheme< Geom::DIM_2D >,
		FlowFunc<Geom::DIM_2D, EQN_POISSON, EquationDef<Geom::DIM_2D,EQN_POISSON>::SIZE> 
	>	gCreatorFlowFuncPoissonRDSLDA2D( "FLOWFUNC_2D_POISSON_RDS_MTX_LDA");

	static ConcCreator< 
		MGString, 
		PoissonGalerkinScheme< Geom::DIM_3D >,
		FlowFunc<Geom::DIM_3D, EQN_POISSON, EquationDef<Geom::DIM_3D,EQN_POISSON>::SIZE> 
	>	gCreatorFlowFuncPoissonRDSLDA3D( "FLOWFUNC_3D_POISSON_RDS_MTX_LDA");
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

