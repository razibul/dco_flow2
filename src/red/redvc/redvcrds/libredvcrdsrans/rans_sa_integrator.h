#ifndef __RANSSAINTEGRATOR_H__ 
#define __RANSSAINTEGRATOR_H__ 

#include "redvc/libredvccommon/integrator.h"
#include "libredphysics/ranssaequations.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
// class RANSSAIntegrator
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class RANSSAIntegrator : public Integrator<DIM, EQN_RANS_SA>
{
	typedef EquationDef<DIM,EQN_RANS_SA> EqDef;

	enum { EQSIZE = EquationDef<DIM,EQN_RANS_SA>::SIZE };
	enum { AUXSIZE = EquationDef<DIM,EQN_RANS_SA>::AUXSIZE };

	typedef Vect<DIM>				GVec;
	typedef typename EqDef::FVec	FVec;
	typedef typename EqDef::FMtx	FMtx;

	typedef SVector<DIM>			FSubVec;
	typedef SMatrix<DIM>			FSubMtx;

	typedef CFlowCell<DIM,EQSIZE,AUXSIZE>	CFCell;
	typedef CFlowBFace<DIM,EQSIZE,AUXSIZE>	CFBFace;

public:

	virtual ~RANSSAIntegrator()	{};

	virtual void	Create( const CfgSection* pcfgsec, Physics<DIM,EQN_RANS_SA>* pphys, Data<DIM,EQN_RANS_SA>* pdata);
	virtual void	PostCreateCheck() const;
	virtual void	Init();
	virtual bool	Do();

protected:

	template <MGSize D>
	class GetZ
	{
	public:
		MGFloat operator () ( const FVec& var) const
		{
			const FVec varZ = EqnRansSA<DIM>::ConservToZ( var);
			return varZ( D);
		}
	};

	void	CalcTensor( FSubMtx& mtx, const CFCell& fcell) const;

private:
	FVec		mRefVar;
};


template <Dimension DIM>
void RANSSAIntegrator<DIM>::Create( const CfgSection* pcfgsec, Physics<DIM,EQN_RANS_SA>* pphys, Data<DIM,EQN_RANS_SA>* pdata)
{
	Integrator<DIM, EQN_RANS_SA>::Create( pcfgsec, pphys, pdata);
}

template <Dimension DIM>
void RANSSAIntegrator<DIM>::PostCreateCheck() const
{
}

template <Dimension DIM>
void RANSSAIntegrator<DIM>::Init()
{
	this->cPhysics().DimToUndim( mRefVar, this->cPhysics().cRefVar(), this->cPhysics().cRefVar() );
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __RANSSAINTEGRATOR_H__ 
