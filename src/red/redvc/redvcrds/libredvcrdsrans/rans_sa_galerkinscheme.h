#ifndef __RANS0GALERKINSCHEME_H__ 
#define __RANS0GALERKINSCHEME_H__ 

#include "redvc/redvcrds/libredvcrdsns/ns_galerkinscheme.h"
#include "redvc/redvcrds/libredvcrdsns/ns_galerkincrdscheme.h"
#include "libredphysics/ranssaequations.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
// class RANSSAGalerkinScheme
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, class MTXRDS>
class RANSSAGalerkinScheme : public FlowFunc<DIM, EQN_RANS_SA, EquationDef<DIM,EQN_RANS_SA>::SplitingFlowTurb::template Block<1>::VBSIZE >
{
	typedef EquationDef<DIM,EQN_RANS_SA> FEqns;
	typedef FlowFunc<DIM, EQN_RANS_SA, FEqns::SplitingFlowTurb::template Block<1>::VBSIZE >	TBase;
	typedef typename TBase::BVec	BVec;
	typedef typename TBase::BMtx	BMtx;

	enum { EQSIZE = EquationDef<DIM,EQN_RANS_SA>::SIZE };
	enum { AUXSIZE = EquationDef<DIM,EQN_RANS_SA>::AUXSIZE };

	typedef Vect<DIM>				GVec;
	typedef typename FEqns::FVec	FVec;
	typedef typename FEqns::FMtx	FMtx;

	typedef CFlowCell<DIM,EQSIZE,AUXSIZE>	CFCell;
	typedef CFlowBFace<DIM,EQSIZE,AUXSIZE>	CFBFace;

public:
	virtual void	Init( const Physics<DIM,EQN_RANS_SA>& physics);

	virtual void	CalcDT( const CFCell& fcell, MGFloat& dt );
	virtual void	CalcResiduum( const CFCell& cell, BVec tabres[] );
	virtual void	CalcResJacobian( const CFCell& cell, BVec tabres[], BMtx mtxres[DIM+1][DIM+1] );

private:
	MGFloat		mRe;
	MGFloat		mPr;
	FVec		mRefVar;
};


template <Dimension DIM, class MTXRDS>
void RANSSAGalerkinScheme<DIM,MTXRDS>::Init( const Physics<DIM,EQN_RANS_SA>& physics)
{
	mRe = physics.cRe();
	mPr = physics.cPr();

	physics.DimToUndim( mRefVar, physics.cRefVar(), physics.cRefVar() );
	//mRefVar = physics.cRefVar();
}


template <Dimension DIM, class MTXRDS>
inline void RANSSAGalerkinScheme<DIM,MTXRDS>::CalcDT( const CFCell& fcell, MGFloat& dt )
{
	FVec	var;
	var.Init( 0.0);
	for ( MGSize i=0; i<=DIM; ++i)
		var += fcell.cVar(i);

	MGFloat umax = sqrt( EqnEuler<DIM>::SqrU( var) ) / EqnEulerComm<DIM>::Ro( var) + EqnEulerComm<DIM>::C( var);
	MGFloat vol = fcell.Volume() / fcell.Size();

	MGFloat cellh = 0;
	for ( MGSize k=0; k<fcell.Size(); ++k)
	{
		MGFloat a = fcell.Vn(k).module();
		MGFloat h = vol / a;

		if ( k==0 || h < cellh )
			cellh = h;
	}

	MGFloat c = EqnEulerComm<DIM>::C( var);
	MGFloat cinf = EqnEulerComm<DIM>::C( this->mRefVar);
	MGFloat c2 = c*c;
	MGFloat	cinf2 = cinf*cinf;

	MGFloat temp = c2 / cinf2;
	MGFloat tempinf = 295.0;
	MGFloat sconst = 110.33;

	MGFloat mu = sqrt( temp*temp*temp) * ( 1.0 + sconst/tempinf) / (temp + sconst/tempinf);
	
	/////////////////////////////////////
	const MGFloat Ret = mRe;
	const MGFloat cv1 = 7.1;

    MGFloat chi = mRe / Ret * var(FEqns::ID_K) / mu;
    chi = max( chi, 1.0e-4);

    const MGFloat fv1 = chi*chi*chi / (chi*chi*chi + cv1*cv1*cv1);


	MGFloat qtmp = max( var(FEqns::ID_K), 0. );
    MGFloat muT = mRe / Ret * fv1 * qtmp;
	/////////////////////////////////////
	
	
	MGFloat nu = (mu /*+ muT*/) / ( EqnEulerComm<DIM>::Ro( var) * this->mRe );

	//dt =  cellh*cellh / nu;
	//dt =  ::sqrt( vol) / umax;

	dt = min( cellh / umax, cellh*cellh / nu );
	//dt =  cellh / umax;
}


template <Dimension DIM, class MTXRDS>
inline void RANSSAGalerkinScheme<DIM,MTXRDS>::CalcResiduum( const CFCell& fcell, BVec tabres[])		
{
	////////////////////////////////////
	// calc mean molecular mu
	//const MGFloat cb1 = 0.1355;
	//const MGFloat cb2 = 0.622;
	//const MGFloat ck  = 0.41;
	const MGFloat cv1 = 7.1;
	//const MGFloat cw1 = cb1 / (ck*ck) + (1 + cb2) / csigma;
	//const MGFloat cw2 = 0.3;
	//const MGFloat cw3 = 2.;

	FVec	varZm, varQm;

	varZm.Init( 0.0);

	for ( MGSize i=0; i<=DIM; ++i)
		varZm += EqnRansSA<DIM>::ConservToZ( fcell.cVar(i) );

	varZm /= (MGFloat)(DIM+1);

	varZm(FEqns::ID_K)   = max( 0.0, varZm(FEqns::ID_K) );
	varZm(FEqns::ID_RHO) = max( 1.0e-10, varZm(FEqns::ID_RHO) );
	varZm(FEqns::ID_P)   = max( 1.0e-10, varZm(FEqns::ID_P) );

	varQm = EqnRansSA<DIM>::ZToConserv( varZm);

	MGFloat c = EqnEulerComm<DIM>::C( varQm);
	MGFloat cinf = EqnEulerComm<DIM>::C( mRefVar);
	MGFloat c2 = c*c;
	MGFloat	cinf2 = cinf*cinf;

	MGFloat temp = c2 / cinf2;
	MGFloat tempinf = 295.0;
	MGFloat sconst = 110.33;

	/////////////////////////////////////
	//MGFloat	mu = 1.0;
	//mu = sqrt( temp*temp*temp) * ( 1.0 + sconst/tempinf) / (temp + sconst/tempinf);

	//MGFloat chi = mRe * varZm(FEqns::ID_K) / ( mu * varZm(FEqns::ID_RHO) );
	//chi = max( chi, 1.0e-4);

	//const MGFloat fv1 = chi*chi*chi / (chi*chi*chi + cv1*cv1*cv1);

	//MGFloat muT = fv1 * varQm(FEqns::ID_K) * mRe;
	//muT = 0.0;

	/////////////////////////////////////
	MGFloat mu = 1.0;
    mu = sqrt( temp*temp*temp) * ( 1.0 + sconst/tempinf) / (temp + sconst/tempinf);

	const MGFloat Ret = mRe;

    MGFloat chi = mRe / Ret * varQm(FEqns::ID_K) / mu;
    chi = max( chi, 1.0e-4);

    const MGFloat fv1 = chi*chi*chi / (chi*chi*chi + cv1*cv1*cv1);


	MGFloat qtmp = max( varQm(FEqns::ID_K), 0. );
    MGFloat muT = mRe / Ret * fv1 * qtmp;
	/////////////////////////////////////

	typedef typename EquationDef<DIM,EQN_RANS_SA>::SplitingFlowTurb::template Block<1> TMapper;

	typename EquationDef<DIM,EQN_NS>::FVec	tabNSres[DIM+1];
	typename EquationDef<DIM,EQN_NS>::FVec	refNSVar;

	TMapper::GlobalToLocal( refNSVar, mRefVar);

	MTXRDS func( mRe, mPr, refNSVar, muT);


	CFlowCell<DIM,EquationDef<DIM,EQN_NS>::SIZE,EquationDef<DIM,EQN_NS>::AUXSIZE> cfNSCell( static_cast< CGeomCell<DIM> >( fcell ) );

	for ( MGSize i=0; i<fcell.Size(); ++i)
		TMapper::GlobalToLocal( cfNSCell.rVar(i), fcell.cVar(i) );

	if ( fcell.IsOutlet() )
		cfNSCell.SetOutlet(true);

	func.CalcResiduum( cfNSCell, tabNSres);

	for ( MGSize i=0; i<fcell.Size(); ++i)
	{
		tabres[i].Init( 0.0);
		TMapper::LocalToGlobal( tabres[i], tabNSres[i] );
	}


	//for ( MGSize i=0; i<fcell.Size(); ++i)
	//	for ( MGSize ie=0; ie<EQSIZE; ++ie)
	//		if ( ISNAN( tabres[i](ie) ) || ISINF( tabres[i](ie) )  )
	//			tabres[i](ie) = 0.0;



// 	for ( MGSize ie=0; ie<EQSIZE; ++ie)
// 	{
// 		bool bOk = true;
// 
// 	    for ( MGSize i=0; i<fcell.Size(); ++i)
// 			if ( ISNAN( tabres[i](ie) ) || ISINF( tabres[i](ie) )  )
// 			{
// 				bOk = false;
// 				break;
// 			}
// 
// 		if ( ! bOk )
// 		{
// 			for ( MGSize i=0; i<fcell.Size(); ++i)
// 				tabres[i](ie) = 0.0;
// 		}
// 	}


}

template <Dimension DIM, class MTXRDS>
inline void RANSSAGalerkinScheme<DIM,MTXRDS>::CalcResJacobian( const CFCell& fcell, BVec tabres[], BMtx mtxres[DIM+1][DIM+1])		
{
	THROW_INTERNAL("Not implemented !!!");
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __RANS0GALERKINSCHEME_H__ 
