#ifndef __RANS0GALERKINSCHEME_H__ 
#define __RANS0GALERKINSCHEME_H__ 

#include "redvc/redvcrds/libredvcrdsns/ns_galerkinscheme.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
// class RANSGalerkinScheme
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class RANSGalerkinScheme : public FlowFunc<DIM, EQN_RANS_2E, EquationDef<DIM,EQN_RANS_2E>::SplitingFlowTurb::template Block<1>::VBSIZE >
{
	typedef EquationDef<DIM,EQN_RANS_2E> FEqns;
	typedef FlowFunc<DIM, EQN_RANS_2E, FEqns::SplitingFlowTurb::template Block<1>::VBSIZE >	TBase;
	typedef typename TBase::BVec	BVec;
	typedef typename TBase::BMtx	BMtx;

	enum { EQSIZE = EquationDef<DIM,EQN_RANS_2E>::SIZE };
	enum { AUXSIZE = EquationDef<DIM,EQN_RANS_2E>::AUXSIZE };

	typedef Vect<DIM>				GVec;
	typedef typename FEqns::FVec	FVec;
	typedef typename FEqns::FMtx	FMtx;

	typedef CFlowCell<DIM,EQSIZE,AUXSIZE>	CFCell;
	typedef CFlowBFace<DIM,EQSIZE,AUXSIZE>	CFBFace;

public:
	virtual void	Init( const Physics<DIM,EQN_RANS_2E>& physics);

	virtual void	CalcResiduum( const CFCell& cell, BVec tabres[] );
	virtual void	CalcResJacobian( const CFCell& cell, BVec tabres[], BMtx mtxres[DIM+1][DIM+1] );

private:
	MGFloat		mRe;
	MGFloat		mPr;
	FVec		mRefVar;
};


template <Dimension DIM>
void RANSGalerkinScheme<DIM>::Init( const Physics<DIM,EQN_RANS_2E>& physics)
{
	mRe = physics.cRe();
	mPr = physics.cPr();
	mRefVar = physics.cRefVar();
}


template <Dimension DIM>
inline void RANSGalerkinScheme<DIM>::CalcResiduum( const CFCell& fcell, BVec tabres[])		
{
	typedef	EquationDef<Geom::DIM_2D,EQN_RANS_2E>::SplitingFlowTurb::Block<1> TMapper;

	typename EquationDef<DIM,EQN_NS>::FVec	tabNSres[DIM+1];
	typename EquationDef<DIM,EQN_NS>::FVec	refNSVar;

	TMapper::GlobalToLocal( refNSVar, mRefVar);

	GalerkinScheme< DIM, RDSchemeMtx< DIM, MtxLDAscheme<DIM> > > func( mRe, mPr, refNSVar);


	CFlowCell<DIM,EquationDef<DIM,EQN_NS>::SIZE,EquationDef<DIM,EQN_NS>::AUXSIZE> cfNSCell( static_cast< CGeomCell<DIM> >( fcell ) );

	for ( MGSize i=0; i<fcell.Size(); ++i)
		TMapper::GlobalToLocal( cfNSCell.rVar(i), fcell.cVar(i) );



	func.CalcResiduum( cfNSCell, tabNSres);

	for ( MGSize i=0; i<fcell.Size(); ++i)
	{
		tabres[i].Init( 0.0);
		TMapper::LocalToGlobal( tabres[i], tabNSres[i] );
	}

}

template <Dimension DIM>
inline void RANSGalerkinScheme<DIM>::CalcResJacobian( const CFCell& fcell, BVec tabres[], BMtx mtxres[DIM+1][DIM+1])		
{
	THROW_INTERNAL("Not implemented !!!");
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __RANS0GALERKINSCHEME_H__ 
