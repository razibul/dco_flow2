#include "rans_galerkinscheme.h"
#include "libcorecommon/factory.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//template class RANSGalerkinScheme<DIM_2D>;


void init_FlowFuncRANS2eqFlowRDS()
{
	static ConcCreator< 
		MGString, 
		RANSGalerkinScheme<DIM_2D>,
		FlowFunc<Geom::DIM_2D, EQN_RANS_2E, EquationDef<Geom::DIM_2D,EQN_RANS_2E>::SplitingFlowTurb::Block<1>::VBSIZE > 
	>	gCreatorFlowFuncRANS2eqRDSLDA2D( "FLOWFUNC_2D_RANS_2E_RDS_MTX_LDA");

}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

