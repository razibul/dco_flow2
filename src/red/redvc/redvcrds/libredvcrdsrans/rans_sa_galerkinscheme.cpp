#include "rans_sa_galerkinscheme.h"
#include "libcorecommon/factory.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//template class RANSGalerkinScheme<DIM_2D>;


void init_FlowFuncRansSAFlowRDS()
{
	static ConcCreator< 
		MGString, 
		RANSSAGalerkinScheme<DIM_2D, GalerkinCRDScheme< Geom::DIM_2D, RDSchemeCRDPreT< Geom::DIM_2D, CRDMtxNscheme<Geom::DIM_2D>, CRDScNscheme<DIM_2D> > > >,
		FlowFunc<Geom::DIM_2D, EQN_RANS_SA, EquationDef<Geom::DIM_2D,EQN_RANS_SA>::SplitingFlowTurb::Block<1>::VBSIZE > 
	>	gCreatorFlowFuncRansSARdsPretCrdN2D( "FLOWFUNC_2D_RANS_SA_RDS_PRET_CRDMTX_N");

	static ConcCreator< 
		MGString, 
		RANSSAGalerkinScheme<DIM_2D, GalerkinCRDScheme< Geom::DIM_2D, RDSchemeCRDPreT< Geom::DIM_2D, CRDMtxLDANscheme<Geom::DIM_2D>, CRDScNscheme<DIM_2D> > > >,
		FlowFunc<Geom::DIM_2D, EQN_RANS_SA, EquationDef<Geom::DIM_2D,EQN_RANS_SA>::SplitingFlowTurb::Block<1>::VBSIZE > 
	>	gCreatorFlowFuncRansSARdsPretCrdLDAN2D( "FLOWFUNC_2D_RANS_SA_RDS_PRET_CRDMTX_LDAN");




	static ConcCreator< 
		MGString, 
		RANSSAGalerkinScheme<DIM_2D, GalerkinCRDScheme< Geom::DIM_2D, RDSchemeCRDPreT< Geom::DIM_2D, CRDMtxLDAscheme<Geom::DIM_2D>, CRDScLDAscheme<DIM_2D> > > >,
		FlowFunc<Geom::DIM_2D, EQN_RANS_SA, EquationDef<Geom::DIM_2D,EQN_RANS_SA>::SplitingFlowTurb::Block<1>::VBSIZE > 
	>	gCreatorFlowFuncRansSARdsPretCrdLDA2D( "FLOWFUNC_2D_RANS_SA_RDS_PRET_CRDMTX_LDA");

	static ConcCreator< 
		MGString, 
		RANSSAGalerkinScheme<DIM_3D, GalerkinCRDScheme< Geom::DIM_3D, RDSchemeCRDPreT< Geom::DIM_3D, CRDMtxLDAscheme<Geom::DIM_3D>, CRDScLDAscheme<DIM_3D> > > >,
		FlowFunc<Geom::DIM_3D, EQN_RANS_SA, EquationDef<Geom::DIM_3D,EQN_RANS_SA>::SplitingFlowTurb::Block<1>::VBSIZE > 
	>	gCreatorFlowFuncRansSARdsPretCrdLDA3D( "FLOWFUNC_3D_RANS_SA_RDS_PRET_CRDMTX_LDA");


	// CRD
	static ConcCreator< 
		MGString, 
		RANSSAGalerkinScheme<DIM_2D, GalerkinCRDScheme< Geom::DIM_2D, RDSchemeCRDMtx< DIM_2D, CRDMtxLDAscheme<DIM_2D> > > >,
		FlowFunc<Geom::DIM_2D, EQN_RANS_SA, EquationDef<Geom::DIM_2D,EQN_RANS_SA>::SplitingFlowTurb::Block<1>::VBSIZE > 
	>	gCreatorFlowFuncRansSARdsCrdLDA2D( "FLOWFUNC_2D_RANS_SA_RDS_CRDMTX_LDA");

	static ConcCreator< 
		MGString, 
		RANSSAGalerkinScheme<DIM_3D, GalerkinCRDScheme< Geom::DIM_3D, RDSchemeCRDMtx< DIM_3D, CRDMtxLDAscheme<DIM_3D> > > >,
		FlowFunc<Geom::DIM_3D, EQN_RANS_SA, EquationDef<Geom::DIM_3D,EQN_RANS_SA>::SplitingFlowTurb::Block<1>::VBSIZE > 
	>	gCreatorFlowFuncRansSARdsCrdLDA3D( "FLOWFUNC_3D_RANS_SA_RDS_CRDMTX_LDA");




	//// PRET
	//static ConcCreator< 
	//	MGString, 
	//	RANSSAGalerkinScheme<DIM_2D, RDSchemePreT< Geom::DIM_2D, MtxLDAscheme<Geom::DIM_2D>, ScPSIscheme<DIM_2D> > >,
	//	FlowFunc<Geom::DIM_2D, EQN_RANS_SA, EquationDef<Geom::DIM_2D,EQN_RANS_SA>::SplitingFlowTurb::Block<1>::VBSIZE > 
	//>	gCreatorFlowFuncRansSARdsPreTLDAPSI2D( "FLOWFUNC_2D_RANS_SA_RDS_PRET_LDA_PSI");

	//static ConcCreator< 
	//	MGString, 
	//	RANSSAGalerkinScheme<DIM_2D, RDSchemePreT< Geom::DIM_2D, MtxLDAscheme<Geom::DIM_2D>, ScLDAscheme<DIM_2D> > >,
	//	FlowFunc<Geom::DIM_2D, EQN_RANS_SA, EquationDef<Geom::DIM_2D,EQN_RANS_SA>::SplitingFlowTurb::Block<1>::VBSIZE > 
	//>	gCreatorFlowFuncRansSARdsPreTLDALDA2D( "FLOWFUNC_2D_RANS_SA_RDS_PRET_LDA_LDA");


	//static ConcCreator< 
	//	MGString, 
	//	RANSSAGalerkinScheme<DIM_3D, RDSchemePreT< Geom::DIM_3D, MtxLDAscheme<Geom::DIM_3D>, ScLDAscheme<DIM_3D> > >,
	//	FlowFunc<Geom::DIM_3D, EQN_RANS_SA, EquationDef<Geom::DIM_3D,EQN_RANS_SA>::SplitingFlowTurb::Block<1>::VBSIZE > 
	//>	gCreatorFlowFuncRansSARdsPreTLDALDA3D( "FLOWFUNC_3D_RANS_SA_RDS_PRET_LDA_LDA");

	//static ConcCreator< 
	//	MGString, 
	//	RANSSAGalerkinScheme<DIM_3D, RDSchemePreT< Geom::DIM_3D, MtxLDAscheme<Geom::DIM_3D>, ScPSIscheme<DIM_3D> > >,
	//	FlowFunc<Geom::DIM_3D, EQN_RANS_SA, EquationDef<Geom::DIM_3D,EQN_RANS_SA>::SplitingFlowTurb::Block<1>::VBSIZE > 
	//>	gCreatorFlowFuncRansSARdsPreTLDAPSI3D( "FLOWFUNC_3D_RANS_SA_RDS_PRET_LDA_PSI");



	// STD 2D
	static ConcCreator< 
		MGString, 
		RANSSAGalerkinScheme<DIM_2D, GalerkinScheme< Geom::DIM_2D, RDSchemeMtx< DIM_2D, MtxLDAscheme<DIM_2D> > > >,
		FlowFunc<Geom::DIM_2D, EQN_RANS_SA, EquationDef<Geom::DIM_2D,EQN_RANS_SA>::SplitingFlowTurb::Block<1>::VBSIZE > 
	>	gCreatorFlowFuncRansSARdsLDA2D( "FLOWFUNC_2D_RANS_SA_RDS_MTX_LDA");

	static ConcCreator< 
		MGString, 
		RANSSAGalerkinScheme<DIM_2D, GalerkinScheme< Geom::DIM_2D, RDSchemeMtx< DIM_2D, MtxNscheme<DIM_2D> > > >,
		FlowFunc<Geom::DIM_2D, EQN_RANS_SA, EquationDef<Geom::DIM_2D,EQN_RANS_SA>::SplitingFlowTurb::Block<1>::VBSIZE > 
	>	gCreatorFlowFuncRansSARdsN2D( "FLOWFUNC_2D_RANS_SA_RDS_MTX_N");

	static ConcCreator< 
		MGString, 
		RANSSAGalerkinScheme<DIM_2D, GalerkinScheme< Geom::DIM_2D, RDSchemeMtx< DIM_2D, MtxLDANscheme<DIM_2D> > > >,
		FlowFunc<Geom::DIM_2D, EQN_RANS_SA, EquationDef<Geom::DIM_2D,EQN_RANS_SA>::SplitingFlowTurb::Block<1>::VBSIZE > 
	>	gCreatorFlowFuncRansSARdsLDAN2D( "FLOWFUNC_2D_RANS_SA_RDS_MTX_LDAN");



	// STD 3D
	static ConcCreator< 
		MGString, 
		RANSSAGalerkinScheme<DIM_3D, GalerkinScheme< Geom::DIM_3D, RDSchemeMtx< DIM_3D, MtxLDAscheme<DIM_3D> > > >,
		FlowFunc<Geom::DIM_3D, EQN_RANS_SA, EquationDef<Geom::DIM_3D,EQN_RANS_SA>::SplitingFlowTurb::Block<1>::VBSIZE > 
	>	gCreatorFlowFuncRansSARdsLDA3D( "FLOWFUNC_3D_RANS_SA_RDS_MTX_LDA");

	static ConcCreator< 
		MGString, 
		RANSSAGalerkinScheme<DIM_3D, GalerkinScheme< Geom::DIM_3D, RDSchemeMtx< DIM_3D, MtxNscheme<DIM_3D> > > >,
		FlowFunc<Geom::DIM_3D, EQN_RANS_SA, EquationDef<Geom::DIM_3D,EQN_RANS_SA>::SplitingFlowTurb::Block<1>::VBSIZE > 
	>	gCreatorFlowFuncRansSARdsN3D( "FLOWFUNC_3D_RANS_SA_RDS_MTX_N");

	static ConcCreator< 
		MGString, 
		RANSSAGalerkinScheme<DIM_3D, GalerkinScheme< Geom::DIM_3D, RDSchemeMtx< DIM_3D, MtxLDANscheme<DIM_3D> > > >,
		FlowFunc<Geom::DIM_3D, EQN_RANS_SA, EquationDef<Geom::DIM_3D,EQN_RANS_SA>::SplitingFlowTurb::Block<1>::VBSIZE > 
	>	gCreatorFlowFuncRansSARdsLDAN3D( "FLOWFUNC_3D_RANS_SA_RDS_MTX_LDAN");

}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

