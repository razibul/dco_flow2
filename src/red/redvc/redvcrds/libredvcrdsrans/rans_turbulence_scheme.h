#ifndef __RANS0GALERKINSCHEME_H__ 
#define __RANS0GALERKINSCHEME_H__ 

#include "redvc/redvcrds/libredvcrdsns/ns_galerkinscheme.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
// class RANSGalerkinScheme
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class RANSTurbulenceScheme : public FlowFunc<DIM, EQN_RANS_2E, EquationDef<DIM,EQN_RANS_2E>::SplitingFlowTurb::template Block<2>::VBSIZE >
{
	typedef EquationDef<DIM,EQN_RANS_2E> FEqns;
	typedef FlowFunc<DIM, EQN_RANS_2E, FEqns::SplitingFlowTurb::template Block<2>::VBSIZE >	TBase;
	typedef typename TBase::BVec	BVec;
	typedef typename TBase::BMtx	BMtx;


	enum { EQSIZE = FEqns::SIZE };
	enum { AUXSIZE = FEqns::AUXSIZE };

	typedef Vect<DIM>				GVec;
	typedef typename FEqns::FVec	FVec;
	typedef typename FEqns::FMtx	FMtx;

	typedef CFlowCell<DIM,EQSIZE,AUXSIZE>	CFCell;
	typedef CFlowBFace<DIM,EQSIZE,AUXSIZE>	CFBFace;

public:
	virtual void	Init( const Physics<DIM,EQN_RANS_2E>& physics);

	virtual void	CalcResiduum( const CFCell& cell, BVec tabres[] );
	virtual void	CalcResJacobian( const CFCell& cell, BVec tabres[], BMtx mtxres[DIM+1][DIM+1] );

private:
	MGFloat		mRe;
	MGFloat		mPr;
	FVec		mRefVar;
};


template <Dimension DIM>
void RANSTurbulenceScheme<DIM>::Init( const Physics<DIM,EQN_RANS_2E>& physics)
{
	mRe = physics.cRe();
	mPr = physics.cPr();
	mRefVar = physics.cRefVar();
}


template <Dimension DIM>
inline void RANSTurbulenceScheme<DIM>::CalcResiduum( const CFCell& fcell, BVec tabres[])		
{
	for ( MGSize i=0; i<fcell.Size(); ++i)
		tabres[i].Init( 1.0e-16);
}

template <Dimension DIM>
inline void RANSTurbulenceScheme<DIM>::CalcResJacobian( const CFCell& fcell, BVec tabres[], BMtx mtxres[DIM+1][DIM+1])		
{
	THROW_INTERNAL("Not implemented !!!");
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __RANS0GALERKINSCHEME_H__ 
