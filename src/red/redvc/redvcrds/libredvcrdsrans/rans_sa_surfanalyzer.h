#ifndef __RANSSA_SURFANALYZER_H__ 
#define __RANSSA_SURFANALYZER_H__ 

#include "libcoresystem/mgdecl.h" 
#include "redvc/libredvccommon/surfanalyzerbase.h"
#include "libredphysics/ranssaequations.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Dimension DIM>
struct SurfVar
{
	Vect<DIM>	mvecN;
	Vect<DIM>	mvecFf;
	MGFloat		mCp;
	MGFloat		mYp;
};


//////////////////////////////////////////////////////////////////////
// class RansSASurfAnalyzer 
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class RansSASurfAnalyzer : public AssistantBase<DIM,EQN_RANS_SA>
{
	typedef EquationDef<DIM,EQN_RANS_SA> EqDef;

	enum { EQSIZE = EqDef::SIZE };
	enum { AUXSIZE = EqDef::AUXSIZE };

	typedef Vect<DIM>				GVec;
	typedef typename EqDef::FVec	FVec;
	typedef typename EqDef::FMtx	FMtx;
	typedef SVector<EQSIZE,GVec>	FGradVec;


	typedef SVector<DIM>			FSubVec;
	typedef SMatrix<DIM>			FSubMtx;

	typedef CFlowCell<DIM,EQSIZE,AUXSIZE>	CFCell;
	typedef CFlowBFace<DIM,EQSIZE,AUXSIZE>	CFBFace;

public:

	virtual ~RansSASurfAnalyzer()	{};

	virtual void	Create( const CfgSection* pcfgsec, Physics<DIM,EQN_RANS_SA>* pphys, Data<DIM,EQN_RANS_SA>* pdata);
	virtual void	PostCreateCheck() const;
	virtual void	Init();
	virtual bool	Do();

protected:

	void	ExportTEC( const MGString& fname);
	void	ExportMergedTEC( const MGString& fname);


private:
	FVec		mRefVar;
	vector< SurfVar<DIM> > mtabSurfData;
};


template <Dimension DIM>
void RansSASurfAnalyzer<DIM>::Create( const CfgSection* pcfgsec, Physics<DIM,EQN_RANS_SA>* pphys, Data<DIM,EQN_RANS_SA>* pdata)
{
	AssistantBase<DIM,EQN_RANS_SA>::Create( pcfgsec, pphys, pdata);
}

template <Dimension DIM>
void RansSASurfAnalyzer <DIM>::PostCreateCheck() const
{
	AssistantBase<DIM,EQN_RANS_SA>::PostCreateCheck();
}

template <Dimension DIM>
void RansSASurfAnalyzer<DIM>::Init()
{
	this->cPhysics().DimToUndim( mRefVar, this->cPhysics().cRefVar(), this->cPhysics().cRefVar() );

	mtabSurfData.resize( this->cData().cGrid().SizeBNodeTab() );
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __RANSSA_SURFANALYZER_H__ 
