#include "rans_sa_turbulence_scheme.h"
#include "rans_sa_turbulence_scheme_vera.h"
#include "libcorecommon/factory.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



void init_FlowFuncRansSATurbulenceRDS()
{
	static ConcCreator< 
		MGString, 
		RANSSATurbulenceScheme_verA< DIM_3D,ScNscheme<DIM_3D>, RANSSATurbulenceSchemeBase_verA<DIM_3D> >,
		FlowFunc<Geom::DIM_3D, EQN_RANS_SA, EquationDef<Geom::DIM_3D,EQN_RANS_SA>::SplitingFlowTurb::Block<2>::VBSIZE > 
	>	gCreatorFlowFuncRansSATurbRdsN3DverA( "FLOWFUNC_3D_RANS_SA_RDS_A_SC_N");


	static ConcCreator< 
		MGString, 
		RANSSATurbulenceScheme_verA< DIM_2D,ScPSIscheme<DIM_2D>, RANSSATurbulenceSchemeBase_verA<DIM_2D> >,
		FlowFunc<Geom::DIM_2D, EQN_RANS_SA, EquationDef<Geom::DIM_2D,EQN_RANS_SA>::SplitingFlowTurb::Block<2>::VBSIZE > 
	>	gCreatorFlowFuncRansSATurbRdsPSI2DverA( "FLOWFUNC_2D_RANS_SA_RDS_A_SC_PSI");

	static ConcCreator< 
		MGString, 
		RANSSATurbulenceScheme_verA< DIM_2D,ScNscheme<DIM_2D>, RANSSATurbulenceSchemeBase_verA<DIM_2D> >,
		FlowFunc<Geom::DIM_2D, EQN_RANS_SA, EquationDef<Geom::DIM_2D,EQN_RANS_SA>::SplitingFlowTurb::Block<2>::VBSIZE > 
	>	gCreatorFlowFuncRansSATurbRdsN2DverA( "FLOWFUNC_2D_RANS_SA_RDS_A_SC_N");



	static ConcCreator< 
		MGString, 
		RANSSATurbulenceScheme< DIM_2D, ScNscheme<DIM_2D>, RANSSATurbulenceSchemeBase<DIM_2D> >,
		FlowFunc<Geom::DIM_2D, EQN_RANS_SA, EquationDef<Geom::DIM_2D,EQN_RANS_SA>::SplitingFlowTurb::Block<2>::VBSIZE > 
	>	gCreatorFlowFuncRansSATurbRdsN2D( "FLOWFUNC_2D_RANS_SA_RDS_SC_N");

	static ConcCreator< 
		MGString, 
		RANSSATurbulenceScheme< DIM_2D,ScLDAscheme<DIM_2D>, RANSSATurbulenceSchemeBase<DIM_2D> >,
		FlowFunc<Geom::DIM_2D, EQN_RANS_SA, EquationDef<Geom::DIM_2D,EQN_RANS_SA>::SplitingFlowTurb::Block<2>::VBSIZE > 
	>	gCreatorFlowFuncRansSATurbRdsLDA2D( "FLOWFUNC_2D_RANS_SA_RDS_SC_LDA");

	static ConcCreator< 
		MGString, 
		RANSSATurbulenceScheme< DIM_2D,ScPSIscheme<DIM_2D>, RANSSATurbulenceSchemeBase<DIM_2D> >,
		FlowFunc<Geom::DIM_2D, EQN_RANS_SA, EquationDef<Geom::DIM_2D,EQN_RANS_SA>::SplitingFlowTurb::Block<2>::VBSIZE > 
	>	gCreatorFlowFuncRansSATurbRdsPSI2D( "FLOWFUNC_2D_RANS_SA_RDS_SC_PSI");




	static ConcCreator< 
		MGString, 
		RANSSATurbulenceScheme< DIM_3D,ScNscheme<DIM_3D>, RANSSATurbulenceSchemeBase<DIM_3D> >,
		FlowFunc<Geom::DIM_3D, EQN_RANS_SA, EquationDef<Geom::DIM_3D,EQN_RANS_SA>::SplitingFlowTurb::Block<2>::VBSIZE > 
	>	gCreatorFlowFuncRansSATurbRdsN3D( "FLOWFUNC_3D_RANS_SA_RDS_SC_N");

	static ConcCreator< 
		MGString, 
		RANSSATurbulenceScheme< DIM_3D,ScLDAscheme<DIM_3D>, RANSSATurbulenceSchemeBase<DIM_3D> >,
		FlowFunc<Geom::DIM_3D, EQN_RANS_SA, EquationDef<Geom::DIM_3D,EQN_RANS_SA>::SplitingFlowTurb::Block<2>::VBSIZE > 
	>	gCreatorFlowFuncRansSATurbRdsLDA3D( "FLOWFUNC_3D_RANS_SA_RDS_SC_LDA");

	static ConcCreator< 
		MGString, 
		RANSSATurbulenceScheme< DIM_3D,ScPSIscheme<DIM_3D>, RANSSATurbulenceSchemeBase<DIM_3D> >,
		FlowFunc<Geom::DIM_3D, EQN_RANS_SA, EquationDef<Geom::DIM_3D,EQN_RANS_SA>::SplitingFlowTurb::Block<2>::VBSIZE > 
	>	gCreatorFlowFuncRansSATurbRdsPSI3D( "FLOWFUNC_3D_RANS_SA_RDS_SC_PSI");

///////////////////////
// using node gradient

	static ConcCreator< 
		MGString, 
		RANSSATurbulenceScheme< DIM_2D, ScNscheme<DIM_2D>, RANSSATurbulenceSchemeBase_MOD<DIM_2D> >,
		FlowFunc<Geom::DIM_2D, EQN_RANS_SA, EquationDef<Geom::DIM_2D,EQN_RANS_SA>::SplitingFlowTurb::Block<2>::VBSIZE > 
	>	gCreatorFlowFuncRansSATurbRdsNGradN2D( "FLOWFUNC_2D_RANS_SA_RDS_NGRAD_SC_N");

	static ConcCreator< 
		MGString, 
		RANSSATurbulenceScheme< DIM_2D,ScLDAscheme<DIM_2D>, RANSSATurbulenceSchemeBase_MOD<DIM_2D> >,
		FlowFunc<Geom::DIM_2D, EQN_RANS_SA, EquationDef<Geom::DIM_2D,EQN_RANS_SA>::SplitingFlowTurb::Block<2>::VBSIZE > 
	>	gCreatorFlowFuncRansSATurbRdsNGradLDA2D( "FLOWFUNC_2D_RANS_SA_RDS_NGRAD_SC_LDA");

	static ConcCreator< 
		MGString, 
		RANSSATurbulenceScheme< DIM_2D,ScPSIscheme<DIM_2D>, RANSSATurbulenceSchemeBase_MOD<DIM_2D> >,
		FlowFunc<Geom::DIM_2D, EQN_RANS_SA, EquationDef<Geom::DIM_2D,EQN_RANS_SA>::SplitingFlowTurb::Block<2>::VBSIZE > 
	>	gCreatorFlowFuncRansSATurbRdsNGradPSI2D( "FLOWFUNC_2D_RANS_SA_RDS_NGRAD_SC_PSI");




	static ConcCreator< 
		MGString, 
		RANSSATurbulenceScheme< DIM_3D,ScNscheme<DIM_3D>, RANSSATurbulenceSchemeBase_MOD<DIM_3D> >,
		FlowFunc<Geom::DIM_3D, EQN_RANS_SA, EquationDef<Geom::DIM_3D,EQN_RANS_SA>::SplitingFlowTurb::Block<2>::VBSIZE > 
	>	gCreatorFlowFuncRansSATurbRdsNGradN3D( "FLOWFUNC_3D_RANS_SA_RDS_NGRAD_SC_N");

	static ConcCreator< 
		MGString, 
		RANSSATurbulenceScheme< DIM_3D,ScLDAscheme<DIM_3D>, RANSSATurbulenceSchemeBase_MOD<DIM_3D> >,
		FlowFunc<Geom::DIM_3D, EQN_RANS_SA, EquationDef<Geom::DIM_3D,EQN_RANS_SA>::SplitingFlowTurb::Block<2>::VBSIZE > 
	>	gCreatorFlowFuncRansSATurbRdsNGradLDA3D( "FLOWFUNC_3D_RANS_SA_RDS_NGRAD_SC_LDA");

	static ConcCreator< 
		MGString, 
		RANSSATurbulenceScheme< DIM_3D,ScPSIscheme<DIM_3D>, RANSSATurbulenceSchemeBase_MOD<DIM_3D> >,
		FlowFunc<Geom::DIM_3D, EQN_RANS_SA, EquationDef<Geom::DIM_3D,EQN_RANS_SA>::SplitingFlowTurb::Block<2>::VBSIZE > 
	>	gCreatorFlowFuncRansSATurbRdsNGradPSI3D( "FLOWFUNC_3D_RANS_SA_RDS_NGRAD_SC_PSI");

}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

