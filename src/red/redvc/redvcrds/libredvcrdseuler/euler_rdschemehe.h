#ifndef __EULERRDSCHEMEHE_H__
#define __EULERRDSCHEMEHE_H__


#include "redvc/libredvccommon/compflowcell.h"
#include "redvc/libredvccommon/compflowbface.h"

#include "libredphysics/eulerequations.h"

#include "redvc/libredvccommon/flowfunc.h"

#include "redvc/redvcrds/libredvcrdscommon/scnscheme.h"
//#include "redvc/redvcrds/libredvcrdscommon/decompmtx.h"

#include "decomphe.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



//////////////////////////////////////////////////////////////////////
/// class RDSchemeHE
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, class MTXRDS, class SCRDS>
class RDSchemeHE : public FlowFunc<DIM, EQN_EULER, EquationDef<DIM,EQN_EULER>::SIZE>
{

	enum { EQSIZE = EquationDef<DIM,EQN_EULER>::SIZE };
	enum { AUXSIZE = EquationDef<DIM,EQN_EULER>::AUXSIZE };

	typedef Vect<DIM>									GVec;
	typedef typename EquationDef<DIM,EQN_EULER>::FVec	FVec;
	typedef typename EquationDef<DIM,EQN_EULER>::FMtx	FMtx;

	typedef CFlowCell<DIM,EQSIZE,AUXSIZE>				CFCell;

	typedef SVector<EQSIZE-2>				FSubVec;
	typedef SMatrix<EQSIZE-2>				FSubMtx;


public:
	RDSchemeHE()	{}
	RDSchemeHE( const FVec& var) : mRefVar(var)	{}

	virtual void	Init( const Physics<DIM,EQN_EULER>& physics);

	virtual void	CalcResiduum( const CFCell& fcell, FVec tabres[]);
	virtual void	CalcResJacobian( const CFCell& fcell, FVec tabres[], FMtx mtxres[DIM+1][DIM+1]);

	void	ApplyPrecond( FVec& res, const FVec& vsimpl )	{}

protected:
	void	Initialize( const CFCell& fcell);
	void	FinalizeRes( FVec tabres[]);
	void	FinalizeResJacob( FVec tabres[], FMtx mtxres[DIM+1][DIM+1]);

private:
	DecompHE<DIM>	mDecomp;

	FVec	mRefVar;
	FVec	mvarZm, mvarVm;
	FVec	mtabsV[DIM+1];
	FVec	mtabsX[DIM+1];


	FVec	mtabsFi[DIM+1];
	FMtx	mmtxsJ[DIM+1][DIM+1];

	struct RDSMtxCalc : public MTXRDS {}	mtxcalc;
	struct RDSScCalc  : public SCRDS  {}	sccalc;

};
//////////////////////////////////////////////////////////////////////


template <Dimension DIM, class MTXRDS, class SCRDS>
inline void RDSchemeHE<DIM,MTXRDS,SCRDS>::Init( const Physics<DIM,EQN_EULER>& physics)
{
}


template <Dimension DIM, class MTXRDS, class SCRDS>
inline void RDSchemeHE<DIM,MTXRDS,SCRDS>::Initialize( const CFCell& fcell)
{
	FVec	var;

	mvarZm.Init( 0.0);
	for ( MGSize i=0; i<=DIM; ++i)
	{
		mvarZm += var = EqnEuler<DIM>::ConservToZ( fcell.cVar(i) );
		mtabsV[i] = var;
	}

	mvarZm /= (MGFloat)(DIM+1);

	mvarZm(EquationDef<DIM,EQN_EULER>::ID_RHO) = max( 1.0e-10, mvarZm(EquationDef<DIM,EQN_EULER>::ID_RHO) );
	mvarZm(EquationDef<DIM,EQN_EULER>::ID_P)   = max( 1.0e-10, mvarZm(EquationDef<DIM,EQN_EULER>::ID_P) );

	mvarVm = EqnEuler<DIM>::ZToSimp( mvarZm);


	FMtx	smtxM_VZ;
	EqnEuler<DIM>::CJacobVZ( smtxM_VZ, mvarZm);

	for ( MGSize i=0; i<=DIM; ++i)
		mtabsV[i] = smtxM_VZ * mtabsV[i];


	mDecomp.Init( mvarVm);

	for ( MGSize i=0; i<=DIM; ++i)
		mDecomp.ConvertVtoX( mtabsX[i], mtabsV[i]);

}


template <Dimension DIM, class MTXRDS, class SCRDS>
inline void RDSchemeHE<DIM,MTXRDS,SCRDS>::FinalizeRes( FVec tabres[])		
{
	//for ( MGSize i=0; i<=DIM; ++i)
	//{
	//	decomp.ConvertVtoQ( mtabsFi[i], mtabsFi[i]);
	//	tabres[i] = mtabsFi[i];
	//}
}

template <Dimension DIM, class MTXRDS, class SCRDS>
inline void RDSchemeHE<DIM,MTXRDS,SCRDS>::FinalizeResJacob( FVec tabres[], FMtx mtxres[DIM+1][DIM+1])		
{
	//for ( MGSize i=0; i<=DIM; ++i)
	//{
	//	decomp.ConvertVtoQ( mtabsFi[i], mtabsFi[i]);
	//	tabres[i] = mtabsFi[i];

	//	for ( MGSize k=0; k<=DIM; ++k)
	//		decomp.ConvertMtxVtoQ( mtxres[i][k], mmtxsJ[i][k]);
	//}
}


template <Dimension DIM, class MTXRDS, class SCRDS>
inline void RDSchemeHE<DIM,MTXRDS,SCRDS>::CalcResiduum( const CFCell& fcell, FVec tabres[])		
{
	FVec	svect;

	MGFloat	tabk[DIM+1], tabfi[DIM+1], tabu[DIM+1];
	FSubMtx	smtxAp, smtxAm;

	FSubVec	tabsXSub[DIM+1];
	FSubVec	tabsFiSub[DIM+1];
	FSubMtx	tabKp[DIM+1];
	FSubMtx tabKm[DIM+1];

	MGFloat	e=0.0, vnmod;
	GVec	vn, lam;

	Initialize( fcell);


	//////////////////////////////////////////////
	// scalar equations ( entropy, enthalpy)

	lam = mDecomp.Lambda();

	for ( MGSize i=0; i<=DIM; ++i)
		tabk[i] = fcell.Vn( i) * lam / (MGFloat)DIM;

	for ( MGSize neq=0; neq<2; ++neq)
	{
		for ( MGSize i=0; i<=DIM; ++i)
			tabu[i] = mtabsX[i](neq);

		sccalc.Calc( tabk, tabu, tabfi);

		for ( MGSize i=0; i<=DIM; ++i)
			mtabsFi[i](neq) = tabfi[i];
	}

	//////////////////////////////////////////////
	// matrix subsystem ( acoustic)

	for ( MGSize i=0; i<=DIM; ++i)
	{
		vn = fcell.Vn(i).versor();

		mDecomp.Calc( smtxAp, smtxAm, vn, e);

		vnmod = fcell.Vn(i).module() / (MGFloat)DIM;


		tabKp[i] = vnmod * smtxAp;
		tabKm[i] = vnmod * smtxAm;

		tabsXSub[i].Resize( DIM);
		tabsFiSub[i].Resize( DIM);

		for ( MGSize k=0; k<DIM; ++k)
			tabsXSub[i](k) = mtabsX[i](k+2);
	}

	mtxcalc.Calc( DIM, tabKp, tabKm, tabsXSub, tabsFiSub); 

	for ( MGSize i=0; i<=DIM; ++i)
		for ( MGSize k=0; k<DIM; ++k)
			mtabsFi[i](k+2) = tabsFiSub[i](k);



	for ( MGSize i=0; i<=DIM; ++i)
		mDecomp.ConvertXtoQ( tabres[i], mtabsFi[i], mtabsV[i]);
		//mDecomp.ConvertXtoQ( tabres[i], mtabsFi[i], EqnEuler<DIM>::ConservToSimp(fcell.cVar(i) )  );

}



template <Dimension DIM, class MTXRDS, class SCRDS>
inline void RDSchemeHE<DIM,MTXRDS,SCRDS>::CalcResJacobian( const CFCell& fcell, FVec tabres[], FMtx mtxres[DIM+1][DIM+1])		
{
	THROW_INTERNAL("Not implemented !!!");
}



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



#endif // __EULERRDSCHEMEHE_H__
