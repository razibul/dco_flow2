#ifndef __NSSURFANALYZER_H__ 
#define __NSSURFANALYZER_H__ 

#include "redvc/libredvccommon/assistantbase.h"
#include "libredphysics/nsequations.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//template <Dimension DIM>
//struct SurfVar
//{
//	Vect<DIM>	mvecN;
//	Vect<DIM>	mvecFf;
//	MGFloat		mCp;
//};


//////////////////////////////////////////////////////////////////////
// class NSSurfAnalyzer 
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class NSSurfAnalyzer : public AssistantBase<DIM,EQN_NS>
{
	typedef EquationDef<DIM,EQN_NS> EqDef;

	enum { EQSIZE = EquationDef<DIM,EQN_NS>::SIZE };
	enum { AUXSIZE = EquationDef<DIM,EQN_NS>::AUXSIZE };

	typedef Vect<DIM>				GVec;
	typedef typename EqDef::FVec	FVec;
	typedef typename EqDef::FMtx	FMtx;
	typedef SVector<EQSIZE,GVec>	FGradVec;


	typedef SVector<DIM>			FSubVec;
	typedef SMatrix<DIM>			FSubMtx;

	typedef CFlowCell<DIM,EQSIZE,AUXSIZE>	CFCell;
	typedef CFlowBFace<DIM,EQSIZE,AUXSIZE>	CFBFace;

	struct SurfVar
	{
		Vect<DIM>	mvecN;
		Vect<DIM>	mvecFf;
		MGFloat		mCp;
	};


public:

	virtual ~NSSurfAnalyzer ()	{};

	virtual void	Create( const CfgSection* pcfgsec, Physics<DIM,EQN_NS>* pphys, Data<DIM,EQN_NS>* pdata);
	virtual void	PostCreateCheck() const;
	virtual void	Init();
	virtual bool	Do();

//protected:
//
//	void	CalcTensor( FSubMtx& mtx, const CFCell& fcell) const;
//
//private:
//	FVec		mRefVar;
//
protected:

	void	ExportTEC( const MGString& fname);
	void	ExportMergedTEC( const MGString& fname);


private:
	FVec		mRefVar;
	vector< SurfVar > mtabSurfData;

};



template <Dimension DIM>
void NSSurfAnalyzer <DIM>::Create( const CfgSection* pcfgsec, Physics<DIM,EQN_NS>* pphys, Data<DIM,EQN_NS>* pdata)
{
	AssistantBase<DIM,EQN_NS>::Create( pcfgsec, pphys, pdata);
}

template <Dimension DIM>
void NSSurfAnalyzer <DIM>::PostCreateCheck() const
{
	AssistantBase<DIM,EQN_NS>::PostCreateCheck();
}

template <Dimension DIM>
void NSSurfAnalyzer <DIM>::Init()
{
	this->cPhysics().DimToUndim( mRefVar, this->cPhysics().cRefVar(), this->cPhysics().cRefVar() );

	mtabSurfData.resize( this->cData().cGrid().SizeBNodeTab() );
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __NSSURFANALYZER_H__ 
