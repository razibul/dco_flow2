#include "ns_surfanalyzer.h"
#include "libcorecommon/factory.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 




template <Dimension DIM>
void NSSurfAnalyzer<DIM>::ExportMergedTEC( const MGString& fname)
{
	ofstream file( fname.c_str() );

	const typename Data<DIM,EQN_NS>::TGrid& grid = this->cData().cGrid();

	MGString title = "surface analyzer";

	MGSize nviscbface = 0;
	vector<MGSize> tabviscnode;
	
	typedef pair< MGSize, pair< SurfVar, MGSize > > TData;
	typedef map< MGSize, TData > TMapVData;	// PId,  <id,  <Data, count >  >

	TMapVData mapvdata;	

	for ( MGSize i=0; i<this->cData().cGrid().SizeBFaceTab(); ++i)
	{
		CFBFace	cfbface;
		this->GetFBFace( cfbface, i);

		if ( this->cPhysics().cBC( cfbface.cSurfId() ) ==  BC_VISCWALL )
		{
			++nviscbface;

			for ( MGSize j=0; j<cfbface.Size(); ++j)
			{
				MGSize idb = cfbface.cBId(j);
				MGSize idp = this->cData().cGrid().cBNode( idb ).cPId();

				const SurfVar& surfV = mtabSurfData[ cfbface.cBId(j) ] ;

				typename TMapVData::iterator itr = mapvdata.find( idp);
				//if ( ( itr = mapvdata.find( idp) ) == mapvdata.end() )
				if ( itr == mapvdata.end() )
				{
					mapvdata.insert( typename TMapVData::value_type( idp, pair< MGSize, pair< SurfVar, MGSize > >( 0, pair< SurfVar, MGSize >( mtabSurfData[idb], 1) ) ) );
				}
				else
				{
					itr->second.second.first.mvecFf += mtabSurfData[idb].mvecFf;
					itr->second.second.first.mvecN += mtabSurfData[idb].mvecN;
					itr->second.second.first.mCp += mtabSurfData[idb].mCp;
					itr->second.second.second += 1;
				}

				tabviscnode.push_back( idp ); 
			}
		}
	}
	
	sort( tabviscnode.begin(), tabviscnode.end() );
	tabviscnode.erase( unique( tabviscnode.begin(), tabviscnode.end() ), tabviscnode.end() );



	file << "TITLE = \"" << title << " " << this->cPhysics().cRe() <<  "\"" << endl;
	file << "VARIABLES = ";
	if ( DIM == DIM_2D)
	{
		file << "\"X\", \"Y\", \"VnX\", \"VnY\", \"surfid\", ";
		file << "\"cp\", \"cf\", \"fX\", \"fY\"";
		file << endl;
		file << "ZONE T=\"" << title << "\", N=" << tabviscnode.size() << ", E=" << nviscbface << ", F=FEPOINT, ET=LINESEG\n";
	}
	else
	if ( DIM == DIM_3D)
	{
		file << "\"X\", \"Y\", \"Z\", \"VnX\", \"VnY\", \"VnZ\", \"surfid\", ";
		file << "\"cp\", \"cf\", \"fX\", \"fY\", \"fZ\"";
		file << endl;
		file << "ZONE T=\"" << title << "\", N=" << tabviscnode.size() << ", E=" << nviscbface << ", F=FEPOINT, ET=TRIANGLE\n";
	}

	if ( mapvdata.size() != tabviscnode.size() )
		THROW_INTERNAL( "RansSASurfAnalyzer<"<<DIM<<">::ExportMergedTEC :: bnode size problem");

	MGSize id = 0;
	for ( typename TMapVData::iterator itr=mapvdata.begin(); itr!=mapvdata.end(); ++itr)
	{
		itr->second.first = ++id;

		const Node<DIM>& node = grid.cNode( itr->first );
		SurfVar surfV = itr->second.second.first;
		surfV.mCp    *= 1.0/static_cast<MGFloat>(itr->second.second.second);
		surfV.mvecFf *= 1.0/static_cast<MGFloat>(itr->second.second.second);
		surfV.mvecN  *= 1.0/static_cast<MGFloat>(itr->second.second.second);

		for ( MGSize idim=0; idim<DIM; ++idim)
			file << setprecision(16) << node.cPos().cX(idim) << " ";

		for ( MGSize idim=0; idim<DIM; ++idim)
			//file << setprecision(16) << 0.0 << " ";
			file << setprecision(16) << surfV.mvecN.cX(idim) << " ";

		//file << cfbface.cSurfId() << " ";
		file << 0 << " ";

		file << setprecision(16) << surfV.mCp << " ";
		file << setprecision(16) << surfV.mvecFf.module() << " ";

		for ( MGSize idim=0; idim<DIM; ++idim)
			file << setprecision(16) << surfV.mvecFf.cX(idim) << " ";

		file << endl;
	}


	for ( MGSize i=0; i<grid.SizeBFaceTab(); ++i)
	{
		CFBFace	cfbface;
		this->GetFBFace( cfbface, i);

		if ( this->cPhysics().cBC( cfbface.cSurfId() ) ==  BC_VISCWALL )
		{
			for ( MGSize in=0; in<cfbface.Size(); ++in)
			{
				MGSize idb = cfbface.cBId(in);
				MGSize idp = this->cData().cGrid().cBNode( idb ).cPId();

				file << mapvdata[idp].first << " ";
			}

			file << endl;
		}
	}

}

template <Dimension DIM>
void NSSurfAnalyzer<DIM>::ExportTEC( const MGString& fname)
{
	ofstream file( fname.c_str() );

	const typename Data<DIM,EQN_RANS_SA>::TGrid& grid = this->cData().cGrid();

	MGString title = "surface analyzer";

	MGSize nviscbface = 0;
	for ( MGSize i=0; i<this->cData().cGrid().SizeBFaceTab(); ++i)
	{
		CFBFace	cfbface;
		this->GetFBFace( cfbface, i);

		if ( this->cPhysics().cBC( cfbface.cSurfId() ) ==  BC_VISCWALL )
			++nviscbface;
	}


	file << "TITLE = \"" << title << " " << this->cPhysics().cRe() <<  "\"" << endl;
	file << "VARIABLES = ";
	if ( DIM == DIM_2D)
	{
		file << "\"X\", \"Y\", \"VnX\", \"VnY\", \"surfid\", ";
		file << "\"cp\", \"cf\", \"fX\", \"fY\"";
		file << endl;
		file << "ZONE T=\"" << title << "\", N=" << 2*nviscbface << ", E=" << nviscbface << ", F=FEPOINT, ET=LINESEG\n";
	}
	else
	if ( DIM == DIM_3D)
	{
		file << "\"X\", \"Y\", \"Z\", \"VnX\", \"VnY\", \"VnZ\", \"surfid\", ";
		file << "\"cp\", \"cf\", \"fX\", \"fY\", \"fZ\"";
		file << endl;
		file << "ZONE T=\"" << title << "\", N=" << 3*nviscbface << ", E=" << nviscbface << ", F=FEPOINT, ET=TRIANGLE\n";
	}


	for ( MGSize i=0; i<this->cData().cGrid().SizeBFaceTab(); ++i)
	{
		CFBFace	cfbface;
		this->GetFBFace( cfbface, i);

		CFCell cfcell;
		this->GetFCell( cfcell, cfbface.cCellId() );

		MGFloat area = cfbface.Area();
		MGFloat volume = cfcell.Volume();
		MGFloat h = DIM * volume / area;

		if ( this->cPhysics().cBC( cfbface.cSurfId() ) ==  BC_VISCWALL )
		{
			for ( MGSize j=0; j<cfbface.Size(); ++j)
			{
				const BoundaryNode<DIM>& bnode = grid.cBNode( cfbface.cBId(j) );
				const MGSize in = bnode.cPId();
				const Node<DIM>& node = grid.cNode(in);
				GVec vn = bnode.cVn();
				vn = vn.versor();
	
				const SurfVar& surfV = mtabSurfData[ cfbface.cBId(j) ] ;

				for ( MGSize idim=0; idim<DIM; ++idim)
					file << setprecision(16) << node.cPos().cX(idim) << " ";

				for ( MGSize idim=0; idim<DIM; ++idim)
					file << setprecision(16) << vn.cX(idim) << " ";

				file << cfbface.cSurfId() << " ";

				file << setprecision(16) << surfV.mCp << " ";
				file << setprecision(16) << surfV.mvecFf.module() << " ";

				for ( MGSize idim=0; idim<DIM; ++idim)
					file << setprecision(16) << surfV.mvecFf.cX(idim) << " ";

				file << endl;

			}
		}
	}


	for ( MGSize i=0; i<grid.SizeBFaceTab(); ++i)
	{
		CFBFace	cfbface;
		this->GetFBFace( cfbface, i);

		for ( MGSize in=0; in<cfbface.Size(); ++in)
			file << cfbface.cBId(in) + 1 << " ";
		file << endl;
	}

}





template <Dimension DIM>
bool NSSurfAnalyzer<DIM>::Do()
{

	if ( this->cData().cSolution().Size() != this->cData().cGrid().SizeNodeTab() )
		THROW_INTERNAL( "NSSurfAnalyzer :: Solution not initialized properly - size = "<<this->cData().cSolution().Size()<< " should be "<<this->cData().cGrid().SizeNodeTab() );

	if ( this->cData().cSolGradient().Size() != this->cData().cGrid().SizeNodeTab() )
		THROW_INTERNAL( "NSSurfAnalyzer :: SolGradient not initialized properly - size = "<<this->cData().cSolGradient().Size()<< " should be "<<this->cData().cGrid().SizeNodeTab() );

	if ( mtabSurfData.size() != this->cData().cGrid().SizeBNodeTab() )
		THROW_INTERNAL( "NSSurfAnalyzer :: mtabSurfData not initialized properly - size = "<<mtabSurfData.size()<< " should be "<<this->cData().cGrid().SizeBNodeTab() );


	const typename Data<DIM,EQN_NS>::TGrid& grid = this->cData().cGrid();

	MGSize nnode = grid.SizeBNodeTab();
	MGSize nbface = grid.SizeBFaceTab();

	cout << " ----- echo from -- RansNSAnalyzer<"<<DIM<<">::Do -----" << endl;


	GVec gvecGFf;
	GVec gvecGFp;

	for ( MGSize i=0; i<this->cData().cGrid().SizeBFaceTab(); ++i)
	{
		CFBFace	cfbface;
		this->GetFBFace( cfbface, i);

		CFCell cfcell;
		this->GetFCell( cfcell, cfbface.cCellId() );

		MGFloat area = cfbface.Area();
		MGFloat volume = cfcell.Volume();
		MGFloat h = DIM * volume / area;

		GVec gvecCfm;
		GVec gvecCpm;

		for ( MGSize j=0; j<cfbface.Size(); ++j)
		{
			const BoundaryNode<DIM>& bnode = grid.cBNode( cfbface.cBId(j) );
			const MGSize in = bnode.cPId();
			const Node<DIM>& node = grid.cNode(in);
			GVec vn = bnode.cVn();
			vn = vn.versor();

			FVec vecq = FVec( this->cData().cSolution()[ in ] );
			FGradVec vecgradq = this->cData().cSolGradient()[ in ];

			MGFloat c = EqnEulerComm<DIM>::C( vecq);
			MGFloat cinf = EqnEulerComm<DIM>::C( this->mRefVar);
			MGFloat c2 = c*c;
			MGFloat	cinf2 = cinf*cinf;

			MGFloat temp = c2 / cinf2;
			MGFloat tempinf = 295.0;
			MGFloat sconst = 110.33;

			MGFloat mu = sqrt( temp*temp*temp) * ( 1.0 + sconst/tempinf) / (temp + sconst/tempinf);


			FSubMtx mtxT;
			FSubVec vecN, vecF;
			for ( MGSize idim=0; idim<DIM; ++idim)
				vecN(idim) = vn.cX(idim);


			typedef typename EquationDef<DIM,EQN_RANS_SA>::SplitingFlowTurb::template Block<1> TMapper;

			typename EquationDef<DIM,EQN_NS>::FVec	nsvecq;
			SVector<EquationDef<DIM,EQN_NS>::SIZE,GVec>	nsvecgradq;

			TMapper::GlobalToLocal( nsvecq, vecq);
			TMapper::GlobalToLocal( nsvecgradq, vecgradq);

			//cout << "ns:" << endl;
			//nsvecq.Write();
			//for ( MGSize i=0; i<EquationDef<DIM,EQN_NS>::SIZE; ++i)
			//	nsvecgradq(i).Write();


			EqnNS<DIM>::CalcTensor( mtxT, nsvecq, nsvecgradq);

			vecF = mtxT * vecN;

			GVec gvecF, gvecTmp;
			for ( MGSize idim=0; idim<DIM; ++idim)
				gvecF.rX(idim) = vecF(idim);

			gvecTmp = (gvecF * vn) * vn;
			gvecF = gvecF - gvecTmp;

			FVec var = EqnEuler<DIM>::ConservToSimp( cfbface.cVar(j) );
			FVec varR = EqnEuler<DIM>::ConservToSimp( mRefVar );
			

			SurfVar& surfV = mtabSurfData[ cfbface.cBId(j) ] ;

			surfV.mvecN = vn;
			surfV.mvecFf = gvecF * ( 2.0 * mu / this->cPhysics().cRe() );
			surfV.mCp = 2.0 * ( var(EqDef::ID_P) - varR(EqDef::ID_P) );

			gvecCpm -= surfV.mCp * vn;
			gvecCfm += surfV.mvecFf;
		}

		if ( this->cPhysics().cBC( cfbface.cSurfId() ) ==  BC_VISCWALL )
		{
			gvecGFf += gvecCfm * area / MGFloat( cfbface.Size() );
			gvecGFp += gvecCpm * area / MGFloat( cfbface.Size() );
		}

	}


	gvecGFp.Write();
	gvecGFf.Write();

	ExportMergedTEC( "surfan.dat");



	return true;
}





/*
template <Dimension DIM>
bool NSSurfAnalyzer<DIM>::Do()
{
	const typename Data<DIM,EQN_NS>::TGrid& grid = this->cData().cGrid();

	MGString title = "surfaceanalyzer";
	MGSize nnode = grid.SizeBNodeTab();
	MGSize nbface = grid.SizeBFaceTab();

	cout << " ----- echo from -- NSSurfAnalyzer<"<<DIM<<">::Do -----" << endl;

	ofstream file( "surfanal.dat");

	file << "TITLE = \"" << title << "\"" << endl;
	file << "VARIABLES = ";
	if ( DIM == DIM_2D)
	{
		file << "\"surfid\", \"X\", \"Y\", \"VnX\", \"VnY\",";
		file << "\"mu\", \"f\", \"cf\", \"fX\", \"fY\"";
		file << endl;
		file << "ZONE T=\"" << title << "\", N=" << nnode << ", E=" << nbface << ", F=FEPOINT, ET=LINESEG\n";
	}
	else
	if ( DIM == DIM_3D)
	{
		file << "\"surfid\", \"X\", \"Y\", \"Z\", \"VnX\", \"VnY\", \"VnZ\", ";
		file << "\"mu\", \"f\", \"cf\", \"fX\", \"fY\", \"fZ\"";
		file << endl;
		file << "ZONE T=\"" << title << "\", N=" << nnode << ", E=" << nbface << ", F=FEPOINT, ET=TRIANGLE\n";
	}


	FVec vref = EqnEuler<DIM>::ConservToSimp( this->cPhysics().cDefVar( 0) );


	for ( MGSize ibn=0; ibn<grid.SizeBNodeTab(); ++ibn)
	{
		const BoundaryNode<DIM>& bnode = grid.cBNode(ibn);
		const MGSize in = bnode.cPId();
		const Node<DIM>& node = grid.cNode(in);
		GVec vn = bnode.cVn();
		vn = vn.versor();

		FVec vecq = FVec( this->cData().cSolution()[ in ] );
		FGradVec vecgradq = this->cData().cSolGradient()[ in ];

	
		MGFloat c = EqnEulerComm<DIM>::C( vecq);
		MGFloat cinf = EqnEulerComm<DIM>::C( this->mRefVar);
		MGFloat c2 = c*c;
		MGFloat	cinf2 = cinf*cinf;

		MGFloat temp = c2 / cinf2;
		MGFloat tempinf = 295.0;
		MGFloat sconst = 110.33;

		MGFloat mu = sqrt( temp*temp*temp) * ( 1.0 + sconst/tempinf) / (temp + sconst/tempinf);


		FSubMtx mtxT;
		FSubVec vecN, vecF;
		for ( MGSize idim=0; idim<DIM;++idim)
			vecN(idim) = vn.cX(idim);

		EqnNS<DIM>::CalcTensor( mtxT, vecq, vecgradq);

		vecF = mtxT * vecN;

		CFBFace	cfbface;
		this->GetFBFace( cfbface, bnode.cPFId() );

		file << cfbface.cSurfId() << " ";

		for ( MGSize idim=0; idim<DIM;++idim)
			file << setprecision(16) << node.cPos().cX(idim) << " ";

		for ( MGSize idim=0; idim<DIM;++idim)
			file << setprecision(16) << vn.cX(idim) << " ";


		MGFloat f = sqrt( Dot(vecF,vecF));
		file << setprecision(16) << mu << " ";

		file << setprecision(16) << f << " ";

		file << setprecision(16) << 2.0 * f * mu / this->cPhysics().cRe() << " ";

		for ( MGSize idim=0; idim<DIM;++idim)
			file << setprecision(16) << vecF(idim) << " ";

		//for ( MGSize idim=0; idim<DIM; ++idim)
		//	for ( MGSize jdim=0; jdim<DIM; ++jdim)
		//		file << setprecision(16) << mtxT(idim,jdim) << " ";

		//file << "  ";
		//for ( MGSize ie=0 ; ie<EQSIZE; ++ie)
		//{
		//	for ( MGSize idim=0; idim<DIM; ++idim)
		//		file << setprecision(16) << vecgradq(ie).cX(idim) << " ";
		//	file << "  ";
		//}


		file << endl;
	}


	for ( MGSize i=0; i<grid.SizeBFaceTab(); ++i)
	{
		CFBFace	cfbface;
		this->GetFBFace( cfbface, i);

		for ( MGSize in=0; in<cfbface.Size(); ++in)
			file << cfbface.cBId(in) + 1 << " ";
		file << endl;
	}


	return true;
}
*/

void init_SurfAnalyzerNSFlowRDS()
{
	static ConcCreator< 
		MGString, 
		NSSurfAnalyzer<DIM_2D>,
		AssistantBase<Geom::DIM_2D, EQN_NS> 
	>	gCreatorSurfAnalyzerNS2D( "SURFANALYZER_2D_NS");

	static ConcCreator< 
		MGString, 
		NSSurfAnalyzer<DIM_3D>,
		AssistantBase<Geom::DIM_3D, EQN_NS> 
	>	gCreatorSurfAnalyzerNS3D( "SURFANALYZER_3D_NS");

}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 
 