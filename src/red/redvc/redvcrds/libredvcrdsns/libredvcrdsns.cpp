
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


void init_FlowFuncNSRDS();
void init_IntegratorNSFlowRDS();
void init_SurfAnalyzerNSFlowRDS();

void RegisterLib_REDVCRDSNS()
{
	init_FlowFuncNSRDS();
	init_IntegratorNSFlowRDS();
	init_SurfAnalyzerNSFlowRDS();
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

