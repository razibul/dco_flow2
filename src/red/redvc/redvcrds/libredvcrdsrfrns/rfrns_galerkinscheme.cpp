#include "rfrns_galerkinscheme.h"

#include "libcorecommon/factory.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



void init_FlowFuncRfrNSRDS()
{
	// 2D

	static ConcCreator<
		MGString,
		RfrGalerkinScheme< Geom::DIM_2D, RfrRDSchemeMtx< DIM_2D, MtxLDAscheme<DIM_2D> > >,
		FlowFunc<Geom::DIM_2D, EQN_RFR_NS, EquationDef<Geom::DIM_2D,EQN_RFR_NS>::SIZE>
	>	gCreatorFlowFuncRfrNSlamRDSLDA2D( "FLOWFUNC_2D_NS_RDS_MTX_LDA");

	

	// 3D

	static ConcCreator<
		MGString,
		RfrGalerkinScheme< Geom::DIM_3D, RfrRDSchemeMtx< DIM_3D, MtxLDAscheme<DIM_3D> > >,
		FlowFunc<Geom::DIM_3D, EQN_RFR_NS, EquationDef<Geom::DIM_3D,EQN_RFR_NS>::SIZE>
	>	gCreatorFlowFuncRfrNSlamRDSLDA3D( "FLOWFUNC_3D_NS_RDS_MTX_LDA");


}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

