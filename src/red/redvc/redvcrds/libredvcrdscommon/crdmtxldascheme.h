#ifndef __CRDMTXLDASCHEME_H__ 
#define __CRDMTXLDASCHEME_H__ 


#include "libcoresystem/mgdecl.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Dimension DIM>
class CRDMtxLDAscheme
{
public:

	template <class MATRIX, class VECTOR>
	inline void Calc( const MGSize& nmtx, 
		 		  MATRIX tabKp[DIM+1], MATRIX tabKm[DIM+1], 
				  const VECTOR tabU[DIM+1], const VECTOR& crdFi, VECTOR tabFi[DIM+1] )
	{//LDA
		MATRIX	mtxK(nmtx,nmtx);
		MATRIX	skp(nmtx,nmtx,0.0);
		VECTOR	fi(nmtx,0.0);

		for ( MGSize i=0; i<=DIM; ++i)
			skp += tabKp[i];

		skp.Invert();

		VecMultMtxVec( fi, skp, crdFi);

		for ( MGSize i=0; i<=DIM; ++i)
			VecMultMtxVec( tabFi[i], tabKp[i], fi);
	}



	template <class MATRIX, class VECTOR>
	inline void Calc( const MGSize& nmtx, 
		 		  const MATRIX tabKp[DIM+1], const MATRIX tabKm[DIM+1], 
				  const VECTOR tabU[DIM+1], VECTOR tabFi[DIM+1], MATRIX mtxJ[DIM+1][DIM+1] )
	{//LDA

		THROW_INTERNAL( "NOT IMPLEMENTED");

		MATRIX	tabK[DIM+1];
		MATRIX	mtxK(nmtx,nmtx);
		MATRIX	skp(nmtx,nmtx,0.0);
		VECTOR	fi(nmtx,0.0);
		VECTOR	vtmp(nmtx,0.0);

		for ( MGSize i=0; i<=DIM; ++i)
		{
			skp += tabKp[i];
			MtxSumMtxMtx( tabK[i], tabKp[i], tabKm[i]);
			VecAddMultMtxVec( vtmp, tabK[i], tabU[i]);
		}

		skp.Invert();

		VecMultMtxVec( fi, skp, vtmp);

		for ( MGSize i=0; i<=DIM; ++i)
			VecMultMtxVec( tabFi[i], tabKp[i], fi);


		// picard jacobian
		for ( MGSize i=0; i<=DIM; ++i)
		{
			MtxMultMtxMtx( mtxK, skp, tabK[i] );

			for ( MGSize j=0; j<=DIM; ++j)
				MtxMultMtxMtx( mtxJ[j][i], tabKp[j], mtxK);
		}
	}

};




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


#endif // __CRDMTXLDASCHEME_H__ 
