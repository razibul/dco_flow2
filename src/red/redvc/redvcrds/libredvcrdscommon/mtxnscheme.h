#ifndef __MTXNSCHEME_H__
#define __MTXNSCHEME_H__

#include "libcoresystem/mgdecl.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Dimension DIM>
class MtxNscheme
{
public:

	template <class MATRIX, class VECTOR>
	inline void Calc( const MGSize& nmtx, 
		 		  MATRIX tabKp[DIM+1], MATRIX tabKm[DIM+1], 
				  const VECTOR tabU[DIM+1], VECTOR tabFi[DIM+1] )
	{// N
		MATRIX	mtxK( nmtx,nmtx, 0.0);
		MATRIX	skm( nmtx,nmtx, 0.0);
		VECTOR	uin( nmtx,0.0);
		VECTOR	vtmp( nmtx,0.0);

		//const MGFloat eps = 1.0e-14;

		for ( MGSize i=0; i<=DIM; ++i)
		{
			//for ( MGSize k=0; k<nmtx; ++k)
			//{
			//	tabKp[i](k,k) += eps;
			//	tabKm[i](k,k) -= eps;
			//}
			skm += tabKm[i];
			//skmu += tabKm[i]*tabU[i];
			VecAddMultMtxVec( vtmp, tabKm[i], tabU[i]);
		}

		skm.Invert();
		//skmu = skm * skmu;
		VecMultMtxVec( uin, skm, vtmp);

		for ( MGSize i=0; i<=DIM; ++i)
		{
			//tabFi[i] = tabKp[i] * ( tabU[i] - skmu);
			VecMultMtxVec( tabFi[i], tabKp[i], tabU[i] - uin);
		}
	}



	template <class MATRIX, class VECTOR>
	inline void Calc( const MGSize& nmtx, 
		 		  const MATRIX tabKp[DIM+1], const MATRIX tabKm[DIM+1], 
				  const VECTOR tabU[DIM+1], VECTOR tabFi[DIM+1], MATRIX mtxJ[DIM+1][DIM+1] )
	{// N
		MATRIX	mtxK( nmtx,nmtx, 0.0);
		MATRIX	skm( nmtx,nmtx, 0.0);
		VECTOR	skmu( nmtx,0.0);
		VECTOR	vtmp( nmtx,0.0);

		for ( MGSize i=0; i<=DIM; ++i)
		{
			skm += tabKm[i];
			//skmu += tabKm[i]*tabU[i];
			VecAddMultMtxVec( vtmp, tabKm[i], tabU[i]);
		}


		skm.Invert();
		//skmu = skm * skmu;
		VecMultMtxVec( skmu, skm, vtmp);

		for ( MGSize i=0; i<=DIM; ++i)
		{
			//tabFi[i] = tabKp[i] * ( tabU[i] - skmu);
			VecMultMtxVec( tabFi[i], tabKp[i], tabU[i] - skmu);
		}


		// picard jacobian
		skm *= -1.0;

		for ( MGSize i=0; i<=DIM; ++i)
		{
			MtxMultMtxMtx( mtxK, skm, tabKm[i] );

			for ( MGSize j=0; j<=DIM; ++j)
			{
				MATRIX	mtx( mtxK);

				if ( i == j)
				{
					for ( MGSize k=0; k<nmtx; ++k)
						mtx(k,k) += 1.0;
				}

				MtxMultMtxMtx( mtxJ[j][i], tabKp[j], mtx);
			}
		}

	}


};



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __MTXNSCHEME_H__
