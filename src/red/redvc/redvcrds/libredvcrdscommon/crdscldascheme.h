#ifndef __CRDSCLDASCHEME_H__
#define __CRDSCLDASCHEME_H__

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



template <Dimension DIM>
class CRDScLDAscheme
{
public:

	template <class T>
	static void Calc( const T tabk[], const T tabu[], const T& crdfi, T tabfi[])
	{// LDA
		T	tabkp[DIM+1];
		T	fi=0, skp=0.0, tmp;

		for ( MGSize i=0; i<=DIM; ++i)
		{
			tabkp[i] = MAX( 0, tabk[i]); 
			skp += tabkp[i];
			fi += tabk[i]*tabu[i];
		}

		//tmp = fi / skp;
		tmp = crdfi / skp;
		if ( ISNAN(tmp) || ISINF(tmp) )
		{
			for ( MGSize i=0; i<=DIM; ++i)
				tabfi[i] = 0.0;
			return;
		}

		IS_INFNAN_THROW( tmp, "ScLDAscheme" );

		for ( MGSize i=0; i<=DIM; ++i)
		{
			tabfi[i] = tmp * tabkp[i];
		}
	}



};



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __CRDSCLDASCHEME_H__
