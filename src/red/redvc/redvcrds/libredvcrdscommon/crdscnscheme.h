#ifndef __CRDSCNSCHEME_H__
#define __CRDSCNSCHEME_H__

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



template <Dimension DIM>
class CRDScNscheme
{
public:

	template <class T>
	static void Calc( const T tabk[], const T tabu[], const T& crdfi, T tabfi[])
	{// N
		T	tabkp[DIM+1];
		T	tabdi[DIM+1];
		T	fi=0.0, skp=0.0, tmp=0.0;
		T	e = 1.0e-9;

		for ( MGSize i=0; i<=DIM; ++i)
		{
			//tabkp[i] = MAX( e, tabk[i]); 
			tabkp[i] = MAX( 0, tabk[i]); 
			skp += tabkp[i];
			tmp += tabkp[i]*tabu[i];
		}

		//tmp = fi / skp;
		tmp /=  skp;
		fi = crdfi/ skp;

		//if ( ISNAN(tmp) || ISINF(tmp) )
		//{
		//	for ( MGSize i=0; i<=DIM; ++i)
		//		tabfi[i] = 0.0;
		//	return;
		//}

		IS_INFNAN_THROW( tmp, "ScLDAscheme" );

		for ( MGSize i=0; i<=DIM; ++i)
		{
			tabfi[i] = tabkp[i] * fi;
			tabdi[i] = tabkp[i] * ( tabu[i] - tmp);
		}

		//T sd = 0.0;
		//for ( MGSize i=0; i<=DIM; ++i)
		//	sd += tabdi[i];
		//for ( MGSize i=0; i<=DIM; ++i)
		//	tabdi[i] -= sd/3.;



		for ( MGSize i=0; i<=DIM; ++i)
			tabfi[i] += tabdi[i];
	}



};



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __CRDSCNSCHEME_H__
