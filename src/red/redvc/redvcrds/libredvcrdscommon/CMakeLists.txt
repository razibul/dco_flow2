LIST ( APPEND redvcrdscommon_files
	libredvcrdscommon.cpp
	crdmtxldascheme.h
	crdmtxnscheme.h
	crdmtxldanscheme.h
	mtxldanscheme.h
	mtxldascheme.h
	mtxnscheme.h
	crdscldascheme.h
	scldascheme.h
	scnscheme.h
	scpsischeme.h 
) 


INCLUDE_DIRECTORIES (
  ${FLOW2_SOURCE_DIR}/${NAME_CORE}
  ${FLOW2_SOURCE_DIR}/${NAME_RED}
) 

ADD_LIBRARY ( redvcrdscommon ${redvcrdscommon_files} )
SET_PROPERTY(TARGET redvcrdscommon PROPERTY FOLDER "red")
