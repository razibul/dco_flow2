#ifndef __PROCESSINFO_H__
#define __PROCESSINFO_H__

#include "libcoresystem/mgdecl.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
// class ProcessInfo
//////////////////////////////////////////////////////////////////////
class ProcessInfo
{
public:
	static void	Init( const MGInt& rank, const MGInt& n)	{ mProcId=rank; mNProc=n;}

	static const MGInt&	cProcId()							{ return mProcId;}
	static const MGInt&	cNumProc()							{ return mNProc;}

	static bool			IsParallel()						{ return mNProc > 1; }
	
	static MGString		ModifyFileName( const MGString& name);
	static MGString		Prefix();
	static MGString		Prompt();
	
private:
	static MGInt		mProcId;	// id of current proccess
	static MGInt		mNProc;		// number of all proccesses
};


inline MGString ProcessInfo::ModifyFileName( const MGString& name)
{
	if ( mNProc == 1)
		return name;
		
	ostringstream of;
	MGString sbuf = name;
	MGString sext = name;
	
	sbuf.erase( sbuf.find_last_of('.'));
	sext.erase( 0, sext.find_last_of('.') );
	of << sbuf << "." << setfill('0') << setw(3) << mProcId+1 << sext;
	return of.str();
}


inline MGString ProcessInfo::Prefix()
{
	if ( mNProc == 1)
		return MGString( "");

	return MGString( "MPI");
}

inline MGString ProcessInfo::Prompt()
{
	if ( mNProc == 1)
		return MGString( "");

	ostringstream of;

	of << ": " << mProcId << "/" << mNProc << " - ";
	return of.str();
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


#endif // __PROCESSINFO_H__
