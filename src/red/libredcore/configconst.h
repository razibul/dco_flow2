#ifndef __CONFIGCONST_H__
#define __CONFIGCONST_H__

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


namespace ConfigStr
{

//////////////////////////////////////////////////////////////////////
// section MASTER
	namespace Master
	{
		const char NAME[]		= "MASTER";
		const char CASE[]		= "MST_NAME";
		const char DESRIPT[]	= "MST_DESCRIPT";
	}

//////////////////////////////////////////////////////////////////////
// section SOLVER
	namespace Solver
	{
		const char NAME[] = "SOLVER";

		namespace Dim
		{
			const char KEY[]		= "SOL_DIM";
			namespace Value
			{
				const char	DIM_NONE[]	= "NONE";
				const char	DIM_0D[]	= "0D";
				const char	DIM_1D[]	= "1D";
				const char	DIM_2D[]	= "2D";
				const char	DIM_3D[]	= "3D";
				const char	DIM_4D[]	= "4D";
			}
		}

		namespace EqType
		{
			const char KEY[]		= "SOL_EQNTYPE";
			namespace Value
			{
				const char	NONE[]			= "NONE";
				const char	ADVECT[]		= "ADVECT";
				const char	POISSON[]		= "POISSON";
				const char	EULER[]			= "EULER";
				const char	NS[]			= "NS";
				const char	RANS_SA[]		= "RANS_SA";
				const char	RANS_KOMEGA[]	= "RANS_KOMEGA";
				const char	RANS_2E[]		= "RANS_2E";
			}
		}

		namespace SolType
		{
			const char KEY[]	= "SOL_SOLTYPE";
			namespace Value
			{
				const char DEFAULT[]	= "DEFAULT";
			}
		}

		const char MAXNITER[]	= "SOL_MAXNITER";			// max. number of iter.
		const char NSTORE[]		= "SOL_NSTORE";				// number of iter. for storing	
		const char CONVEPS[]	= "SOL_CONVEPS";			// convergence criterion


//////////////////////////////////////////////////////////////////////
// section SOLVER :: subsection GRID
		namespace Grid
		{
			const char NAME[]		= "GRID";
			const char FNAME[]		= "GRD_FNAME";
			namespace FType
			{
				const char KEY[]	= "GRD_FTYPE";
				namespace Value
				{
					const char ASCII[]	= "ASCII";
					const char BIN[]	= "BIN";
				}
			}
			const char MAXANGLE[]	= "GRD_MAXANGLE";
		}

//////////////////////////////////////////////////////////////////////
// section SOLVER :: subsection AUXDATA
		namespace AuxData
		{
			const char NAME[]		= "AUXDATA";
			const char FNAME[]		= "AUX_FNAME";
			const char FTYPE[]		= "AUX_FTYPE";
		}

//////////////////////////////////////////////////////////////////////
// section SOLVER :: subsection SOLUTION
		namespace Solution
		{
			const char NAME[]			= "SOLUTION";
			const char RESTART[]		= "TIM_RESTART";
			const char FNAME[]			= "TIM_FNAME";
			const char FTYPE[]			= "TIM_FTYPE";
			const char WRITE_TEC[]		= "SOL_WRITE_TEC";
			const char WRITE_TECSURF[]	= "SOL_WRITE_TECSURF";
		}

//////////////////////////////////////////////////////////////////////
// section SOLVER :: subsection PHYSICS
		namespace Physics
		{
			const char NAME[]				= "PHYSICS";
			const char REF_LENGTH[]			= "VAR_REF_LENGTH";
			const char REF_AREA[]			= "VAR_REF_AREA";
			const char REF_REYNOLDS[]		= "VAR_REF_REYNOLDS";
			const char REF_PRANDTL[]		= "VAR_REF_PRANDTL";
			const char REF_PRANDTL_TURB[]	= "VAR_REF_PRANDTL_TURB";
			const char REF_MACH[]			= "VAR_REF_MACH";

			const char REF_TRANSL_VELO[]	= "REF_TRANSL_VELO";
			const char REF_ROT_OMEGA[]		= "REF_ROT_OMEGA";
			const char REF_ROT_AXIS_ORIG[]	= "REF_ROT_AXIS_ORIG";
			const char REF_ROT_AXIS_DIR[]	= "REF_ROT_AXIS_DIR";


			const char COEF_POISSON_LAPC[]		= "COEF_POISSON_LAPC";
			const char COEF_POISSON_LAPPHI[]	= "COEF_POISSON_LAPPHI";
			const char COEF_POISSON_CONV[]		= "COEF_POISSON_CONV";

			namespace RFR_Def
			{
				const char NAME[]			= "ROT_FR";

				const char RFR_TRANSL_VELO[]	= "RFR_TRANSL_VELO";
				const char RFR_ROT_OMEGA[]		= "RFR_ROT_OMEGA";
				const char RFR_ROT_AXIS_ORIG[]	= "RFR_ROT_AXIS_ORIG";
				const char RFR_ROT_AXIS_DIR[]	= "RFR_ROT_AXIS_DIR";
			}

			namespace Var
			{
				const char NAME[]			= "VAR";
				const char REFERENCE[]		= "reference";

				namespace Type
				{
					const char KEY[]		= "VAR_TYPE";
					namespace Value
					{
						const char RPUV[]		= "VAR_RPUV";
						const char RRERURV[]	= "VAR_RRERURV";
						const char MRPA[]		= "VAR_MRPA";
					}
				}

				const char RO[]			= "VAR_RO";
				const char P[]			= "VAR_P";
				const char UX[]			= "VAR_UX";
				const char UY[]			= "VAR_UY";
				const char UZ[]			= "VAR_UZ";
				const char ROE[]		= "VAR_ROE";
				const char ROUX[]		= "VAR_ROUX";
				const char ROUY[]		= "VAR_ROUY";
				const char ROUZ[]		= "VAR_ROUZ";
				const char ALPHA[]		= "VAR_ALPHA";
				const char MACH[]		= "VAR_MACH";
				const char NUT[]		= "VAR_NUT";
				const char K[]			= "VAR_K";
				const char OMEGA[]		= "VAR_OMEGA";
			}

			namespace PERIODIC
			{
				const char NAME[]		= "PERIODIC";
				const char ID[]			= "PRD_ID";
				const char ABSTOL[]		= "PRD_ABSTOL";
				const char RELEDGETOL[] = "PRD_REL_EDGETOL";
				

				namespace Type
				{
					const char KEY[]	= "PRD_TYPE";
					namespace Value
					{
						const char TRANS[] = "TRANS";
						const char ANGLE[] = "ROT";
					}
				}

				const char PRD_SRC_ID[]		= "PRD_SOURCE_ID";
				const char PRD_TRG_ID[]		= "PRD_TARGET_ID";
				const char PRD_TRANS_VCT[]  = "PRD_TRANS_VCT";

				const char PRD_ALPHA[]		= "PRD_AXIS_ANGLE";
				const char PRD_AXIS_ORIG[]	= "PRD_AXIS_ORIG";
				const char PRD_AXIS_DIR[]	= "PRD_AXIS_DIR";
			}

			namespace BC
			{
				const char KEY[]			= "BC";
				namespace Value
				{
					const char	FARFIELD[]		= "FARFIELD";
					const char	INVISCWALL[]	= "INVISCWALL";
					const char	SYMETRY[]		= "SYMETRY";
					const char	VISCWALL[]		= "VISCWALL";
					const char	EXTRAPOL[]		= "EXTRAPOL";
					const char	DEF[]			= "DEF";
					const char	PERIODIC[]		= "PERIODIC";
				}
			}
		}

//////////////////////////////////////////////////////////////////////
// section SOLVER :: subsection EXECUTOR
		namespace Executor
		{
			const char NAME[]		= "EXECUTOR";

			namespace BlockDef
			{
				const char KEY[]		= "BLOCKDEF";
				namespace Value
				{
					const char FULL[] = "FULL";
				}
			}

			//-------------------------------------------------------
			// SPACESOL section

			namespace SpaceSol
			{	
				const char NAME[]	= "SPACESOL";

				namespace Type
				{
					const char KEY[]	= "SPC_TYPE";
					namespace Value
					{
						const char	NONE[]	= "NONE";
						const char	RDS[]	= "RDS";
					}
				}

				namespace InitDTType
				{
					const char KEY[]	= "SPC_INITDT";
					namespace Value
					{
						const char	NONE[]			= "NONE";
						const char	STANDARD[]		= "STANDARD";
						const char	EXPERIMENTAL[]	= "EXPERIMENTAL";
					}
				}

				const char NUMDIFF_EPS[]	= "SPC_NUMDIFF_EPS";
				const char SMOOTH_DT[]		= "SPC_SMOOTH_DT";


				namespace FlowFunc
				{	
					const char NAME[]	= "FLOWFUNC";

					namespace FuncType
					{
						const char KEY[]	= "FNC_TYPE";
						namespace Value
						{
							const char	NONE[]			= "NONE";
							const char	MTX_LDAN[]		= "MTX_LDAN";
							const char	MTX_LDA[]		= "MTX_LDA";
							const char	MTX_N[]			= "MTX_N";
							const char	HE_LDA_PSI[]	= "HE_LDA_PSI";
							const char	HE_LDA_LDA[]	= "HE_LDA_LDA";
							const char	HE_LDA_N[]		= "HE_LDA_N";
							const char	HE_LDAN_PSI[]	= "HE_LDAN_PSI";
							const char	HE_N_PSI[]		= "HE_N_PSI";
							const char	HE_N_N[]		= "HE_N_N";
							const char	SM_LDAN_PSI[]	= "SM_LDAN_PSI";
							const char	SM_LDA_PSI[]	= "SM_LDA_PSI";
							const char	SM_LDA_LDA[]	= "SM_LDA_LDA";
						}
					}
				}
			}


			//-------------------------------------------------------
			// TIMESOL section

			namespace TimeSol
			{	
				const char NAME[]	= "TIMESOL";
				namespace Type
				{
					const char KEY[]		= "TIM_TYPE";
					namespace Value
					{
						const char	EXPLICIT[]	= "EXPLICIT";
						const char	IMPLICIT[]	= "IMPLICIT";
						const char	PETSC[]		= "PETSC";
                        const char  TRILINOS[]  = "TRILINOS";
					}
				}

				namespace JacobType
				{
					const char KEY[]		= "TIM_JACOBTYPE";
					namespace Value
					{
						const char	NONE[]			= "NONE";
						const char	NUMERICAL[]		= "NUMERICAL";
						const char	ANALYTICAL[]	= "ANALYTICAL";
						const char	PICARD[]		= "PICARD";
					}
				}

				const char LINMAXITER[]	= "TIM_LINMAXITER";
				const char LINDKSPACE[]	= "TIM_LINDKSPACE";
				const char LINMINEPS[]	= "TIM_LINMINEPS";

				const char MAXNITER[]	= "TIM_MAXNITER";
				const char NSTORE[]		= "TIM_NSTORE";
				const char CONVEPS[]	= "TIM_CONVEPS";
				const char CFL[]		= "TIM_CFL";
				const char MINCFL[]		= "TIM_MINCFL";
				const char MAXCFL[]		= "TIM_MAXCFL";
				const char NCFLGROWTH[]	= "TIM_NCFLGROWTH";
				const char NCFLSTART[]	= "TIM_NCFLSTART";

                namespace Trilinos
                {
                    const char NAME[]	= "TIM_TRILINOS";
                    namespace PrecondType
                    {
                        const char KEY[]	= "TIM_PRE_TYPE";
                        namespace Value
                        {
                            const char	NONE[]			= "TIM_PRE_NONE";
                            const char	AZTECOO[]		= "TIM_PRE_AZTECOO";
                            const char	IFPACK[]		= "TIM_PRE_IFPACK";
                            const char	ML[]			= "TIM_PRE_ML";
                        }

                        const char TIM_PRE_ILU_FILL[]	= "TIM_PRE_ILU_FILL";
                        const char TIM_PRE_ILU_DROP[]	= "TIM_PRE_ILU_DROP";
                        const char TIM_PRE_ILU_ATRSH[]	= "TIM_PRE_ILU_ATRSH";
                        const char TIM_PRE_ILU_RTRSH[]	= "TIM_PRE_ILU_RTRSH";
                        const char TIM_PRE_ILU_RELAX[]	= "TIM_PRE_ILU_RELAX";
                        const char TIM_PRE_ILU_OVERLAP[]	= "TIM_PRE_ILU_OVERLAP";

                        const char TIM_PRE_ML_TYPE[]	= "TIM_PRE_ML_TYPE";
                    }
                }
			}

			//-------------------------------------------------------
			// INTEGRATOR section

			namespace Integrator
			{	
				const char NAME[]	= "INTEGRATOR";

				namespace Type
				{
					const char KEY[]	= "INTEG_TYPE";
					namespace Value
					{
						const char	NONE[]			= "NONE";
					}
				}
			}

			namespace Assistant
			{	
				const char TYPE[]	= "AST_TYPE";
				const char FREQ[]	= "AST_FREQ";

			}

		}
	
	}
}
 


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __CONFIGCONST_H__
