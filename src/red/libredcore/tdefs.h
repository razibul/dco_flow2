#ifndef __TDEFS_H__
#define __TDEFS_H__

#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"
#include "libcorecommon/key.h"
#include "libcoregeom/vect.h"
#include "libcoregeom/metric.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Geom::Dimension DIM>
class Cell;

template <Geom::Dimension DIM>
class Node;

template <Geom::Dimension DIM>
class Face;

template <Geom::Dimension DIM>
class BndCell;

template <Geom::Dimension DIM>
class BndNode;


typedef Key<2>	GEdge;
typedef Key<2>	GBndEdge;



//////////////////////////////////////////////////////////////////////
//  class TDefs
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class TDefs
{
public:
	typedef Vect<MGFloat,DIM>	GVect;
	typedef Metric<MGFloat,DIM>	GMetric;

	typedef Node<DIM>			GNode;
	typedef Cell<DIM>			GCell;
	typedef Face<DIM>			GFace;

	typedef BndNode<DIM>		GBndNode;
	typedef BndCell<DIM>		GBndCell;

};
//////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////
////  class GEdge
////////////////////////////////////////////////////////////////////////
//class GEdge : public Key<2>
//{
//public:
//	GEdge() : Key<2>()											{}
//	GEdge( const Key<2>& key) : Key<2>(key)						{}
//	GEdge( const MGSize& i1, const MGSize& i2) : Key<2>(i1,i2)	{}
//
//};
////////////////////////////////////////////////////////////////////////

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __TDEFS_H__
