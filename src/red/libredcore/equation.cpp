#include "equation.h"
#include "libredcore/configconst.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

MGString EquationToStr( const EQN_TYPE& et)
{
	switch (et)
	{
	case EQN_ADVECT:
		return MGString( ConfigStr::Solver::EqType::Value::ADVECT );
	case EQN_EULER:
		return MGString( ConfigStr::Solver::EqType::Value::EULER );
	case EQN_NS:
		return MGString( ConfigStr::Solver::EqType::Value::NS );
	case EQN_RANS_SA:
		return MGString( ConfigStr::Solver::EqType::Value::RANS_SA );
	case EQN_RANS_KOMEGA:
		return MGString( ConfigStr::Solver::EqType::Value::RANS_KOMEGA );
	case EQN_RANS_2E:
		return MGString( ConfigStr::Solver::EqType::Value::RANS_2E );
	}

	return MGString( "unknown");
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

