#ifndef __MASTER_H__
#define __MASTER_H__

#include "libcoresystem/mgdecl.h"
#include "libcoreconfig/config.h"
#include "redvc/libredvccommon/solverbase.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



//////////////////////////////////////////////////////////////////////
// Master
//////////////////////////////////////////////////////////////////////
class Master
{
public:
	Master()	{}
	~Master();

	void	ReadConfig( const MGString& fname);
	
	void	Create();
	void	Init();

	void	Solve();

protected:

protected:

	Config			mConfig;

	vector<SolverBase*>	mtabSolver;
};
//////////////////////////////////////////////////////////////////////



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __MASTER_H__
