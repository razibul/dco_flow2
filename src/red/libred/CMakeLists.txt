LIST ( APPEND red_files
    master.cpp  
    master.h  
    setup.cpp  
    setup.h
) 


INCLUDE_DIRECTORIES (
  ${FLOW2_SOURCE_DIR}/${NAME_CORE}
  ${FLOW2_SOURCE_DIR}/${NAME_RED}
)

ADD_LIBRARY ( red ${red_files} )
