#include <mpi.h>

#include "mpigridinfo.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


template <Dimension DIM>
void MPIGridInfo<DIM>::ComputeRanges( vector<MGSize>& vtxdist)
{
	vector<MGSize> tabout( ProcessInfo::cNumProc(), 0);
	vector<MGSize> tabin ( ProcessInfo::cNumProc(), 0);

	for ( MGSize i=0; i<ProcessInfo::cNumProc(); ++i)
		tabout[i] = mGrid.SizeNodeTab() - mComm.InterfSize();

	MPI_Alltoall( &tabout[0], 1, MPI_UNSIGNED, &tabin[0], 1, MPI_UNSIGNED, MPI_COMM_WORLD);

	vtxdist.resize( ProcessInfo::cNumProc() + 1 );
	vtxdist[0] = 0;

	for ( MGSize i=1, n1=0; i<=ProcessInfo::cNumProc(); ++i)
	{
		n1 += tabin[i-1];
		vtxdist[i] = n1;
	}

// 	for ( MGSize i=0; i<=ProcessInfo::cNumProc(); ++i)
// 		cout << ProcessInfo::Prefix() << " vtxdist[" << i << "] = " << vtxdist[i] << endl;
} 




template <Dimension DIM>
void MPIGridInfo<DIM>::BuildGlobId()
{
	vector<MGSize> vtxdist;

	ComputeRanges( vtxdist );

	ASSERT( !vtxdist.empty() );
	
	mtabIdGlob.resize( mGrid.SizeNodeTab(), 0 );
	
	MGSize id2 = vtxdist[ ProcessInfo::cProcId() ];
	mrangeOwnedIds.first = id2;

	vector<MGSize> tabinterf;
	mComm.SetTabDNode( tabinterf);
		
	sort( tabinterf.begin(), tabinterf.end() );
	
	for( MGSize inode=0; inode<mGrid.SizeNodeTab(); ++inode)
		if( ! binary_search( tabinterf.begin(), tabinterf.end(), inode ) )
		{
			mtabIdGlob[inode] = id2;
			++id2;
		}

	mrangeOwnedIds.second = id2;

	// do the communication

	vector< vector<MGSize> > tabSend;
	vector< vector<MGSize> > tabReceive;

	tabReceive.resize( mComm.cTabReceive().size() ); 
	for ( MGSize ip=0; ip<mComm.cTabReceive().size(); ++ip )
		tabReceive[ ip ].resize( mComm.cTabReceive()[ip].mtabNodeId.size(), 0 );

	tabSend.resize( mComm.cTabSend().size() );
	for ( MGSize ip=0; ip<mComm.cTabSend().size(); ++ip )
		tabSend[ ip ].resize( mComm.cTabSend()[ip].mtabNodeId.size(), 0 );

	
	// pack data
	for ( MGSize i=0; i<mComm.cTabSend().size(); ++i)
	{
// 		cout << ProcessInfo::Prompt() << "mSend.mtabProc[i].mtabData.size() = " << mSend.mtabProc[i].mtabData.size() << endl;
// 		cout << ProcessInfo::Prompt() << "mSend.mtabProc[i].mtabNodeId.size() = " << mSend.mtabProc[i].mtabNodeId.size() << endl;

		for ( MGSize k=0; k<mComm.cTabSend()[i].mtabNodeId.size(); ++k)
		{
			MGSize id = mComm.cTabSend()[i].mtabNodeId[k].first;
			tabSend[i][k] = mtabIdGlob[id];
		}
	}

	// exchange data
	static MGInt	tag = 8000;
	if ( ++tag > 10000 )
		tag = 1;

	vector<MPI_Request>	tabreqs( mComm.cTabSend().size() + mComm.cTabReceive().size() );
	vector<MPI_Status>	tabstats( mComm.cTabSend().size() + mComm.cTabReceive().size() );

	for ( MGSize i=0; i<mComm.cTabReceive().size(); ++i)
		MPI_Irecv( &tabReceive[i][0], tabReceive[i].size(), MPI_UNSIGNED, mComm.cTabReceive()[i].mProcId, tag, MPI_COMM_WORLD, &tabreqs[i] );

	for ( MGSize i=0; i<mComm.cTabSend().size(); ++i)
		MPI_Isend( &tabSend[i][0], tabSend[i].size(), MPI_UNSIGNED, mComm.cTabSend()[i].mProcId, tag, MPI_COMM_WORLD, &tabreqs[i+mComm.cTabReceive().size()] );


	MPI_Waitall( tabreqs.size(), &tabreqs[0], &tabstats[0]);

	cout << "AFTER WAITALL" << endl;

	// unpack data
	for ( MGSize i=0; i<mComm.cTabReceive().size(); ++i)
	{
		for ( MGSize k=0; k<mComm.cTabReceive()[i].mtabNodeId.size(); ++k)
		{
			MGSize id = mComm.cTabReceive()[i].mtabNodeId[k].first;
			mtabIdGlob[id] = tabReceive[i][k];
	
		}
	}

	
// 	MGString fname = ProcessInfo::ModifyFileName( "globid.txt" );
// 	ofstream of( fname.c_str() );
// 	of << mtabIdGlob.size() << endl;
// 	for ( MGSize i=0; i<mtabIdGlob.size(); ++i)
// 		of << i << "    " << mtabIdGlob[i] << endl;
	
//	cout << "unpacked" << endl;

}





template class MPIGridInfo<DIM_2D>;
template class MPIGridInfo<DIM_3D>;

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
