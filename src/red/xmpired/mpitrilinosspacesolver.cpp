#include "mpitrilinosspacesolver.h"
#include "libcorecommon/factory.h"

/*<< Trilinos includes */
#include "Epetra_CrsGraph.h"
#include "Epetra_BlockMap.h"
#include "Epetra_Vector.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
void MPITRILINOSSpaceSolver<DIM,ET,MAPPER>::Create( const CfgSection* pcfgsec, Physics<DIM,ET>* pphys, Data<DIM,ET>* pdata)
{
    SpaceSolver<DIM,ET,MAPPER>::Create( pcfgsec, pphys, pdata);
}

template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
void MPITRILINOSSpaceSolver<DIM,ET,MAPPER>::TRILINOSInitOwnedIdRange( pair<MGSize,MGSize>& range, vector<MGSize>& tabg) const
{
    range = this->cData().cGrid().cOwnedIds();

    tabg.resize( this->cData().cGrid().SizeNodeTab() );
    for ( MGSize i=0; i<tabg.size(); ++i)
        tabg[i] = this->cData().cGrid().cNode(i).cGId();
}


template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
void MPITRILINOSSpaceSolver<DIM,ET,MAPPER>::TRILINOSResizeGraphAndMatrix( const Epetra_BlockMap& map, Epetra_CrsGraph*& graph, Epetra_VbrMatrix*& mp_A ) const
{
    vector<vector<int> > nodeConn( this->cData().cGrid().SizeNodeTab() );
    for ( MGSize ic=0; ic<this->cData().cGrid().SizeCellTab(); ++ic)
    {
        const CGeomCell<DIM>& cgcell = this->cData().cGrid().cCell( ic );
        for ( MGSize i=0; i<cgcell.Size(); ++i)
        {
            for ( MGSize j=0; j<cgcell.Size(); ++j)
            {
                nodeConn[ cgcell.cId(i) ].push_back( this->cData().cGrid().cNode(cgcell.cId(j)).cGId() );
            }
        }
    }

    for(MGSize in=0; in<nodeConn.size(); ++in)
    {
        sort( nodeConn[in].begin(), nodeConn[in].end() );
        nodeConn[in].erase( unique( nodeConn[in].begin(), nodeConn[in].end() ), nodeConn[in].end() );
    }

    std::vector<int> nnz_per_row(nodeConn.size());
    std::vector<int>::iterator iter1 = nnz_per_row.begin();
    vector<vector<int> >::iterator iter2 = nodeConn.begin();
    for( ; iter2 != nodeConn.end(); ++iter1, ++iter2)
    {
        *iter1 = iter2->size();
    }

    pair<MGSize, MGSize> nodeRange = this->cData().cGrid().cOwnedIds();
    cout << ProcessInfo::Prompt();
    cout << " nodeRange = " << nodeRange.first << " - " << nodeRange.second << endl;

    mp_A = new Epetra_VbrMatrix( Copy, map, nnz_per_row.data() );
    //graph = new Epetra_CrsGraph ( Copy, map, nnz_per_row.data(), true ); // Construct new graph structure

    int err = 0;
    int globid = 0, locglobid = 0;
    //for ( MGSize locid = 0; locid < this->cData().cGrid().SizeNodeTab(); ++locid)
    //{
    //    globid = this->cData().cGrid().cNode(locid).cGId();
    //    //locglobid = globid - nodeRange.first;
    //
    //    if ( globid < nodeRange.first || globid >= nodeRange.second )
    //        continue;
    //
    //    //err = graph->InsertGlobalIndices( globid, nnz_per_row[locid], nodeConn[locid].data() );
    //    err = mp_A->InsertGlobalIndices( globid, nnz_per_row[locid], nodeConn[locid].data() );
    //    //std::cout << "Process " << ProcessInfo::cProcId() << " inserts to global row " << globid << " Local row " << locglobid << " | err: " << err << std::endl;
    //}
    //graph->FillComplete();
    //mp_A->FillComplete();

    //mp_A = new Epetra_VbrMatrix( Copy, *graph ); // fill blocks
    Epetra_SerialDenseMatrix dmat(BSIZE,BSIZE,false);
    for ( MGSize locid = 0; locid < this->cData().cGrid().SizeNodeTab(); ++locid)
    {
        globid = this->cData().cGrid().cNode(locid).cGId();
        //locglobid = globid - nodeRange.first;

        if ( globid < nodeRange.first || globid >= nodeRange.second )
            continue;

        //err = mp_A->BeginReplaceGlobalValues( globid, nnz_per_row[locid], nodeConn[locid].data() );
        err = mp_A->BeginInsertGlobalValues( globid, nnz_per_row[locid], nodeConn[locid].data() );
        for(MGSize i=0; i<nnz_per_row[locid]; ++i)
        {
            err = mp_A->SubmitBlockEntry( dmat );
        }
        err = mp_A->EndSubmitEntries();
        //cout << "End Submit " << err << " j " << locid << endl;
    }
    mp_A->FillComplete();
}

template <Geom::Dimension DIM, EQN_TYPE ET, class MAPPER>
void MPITRILINOSSpaceSolver<DIM,ET,MAPPER>::TRILINOSResNumJacob( Epetra_Vector*& p_RHS, Epetra_VbrMatrix*& p_A, const vector<MGFloat>& tabDT)
{
    vector<MGFloat>	tabvol;
    MGFloat	vol;
    CGeomCell<DIM>	cgcell;
    tabvol.resize( this->cData().cGrid().SizeNodeTab(), 0.0);

    for ( MGSize i=0; i< this->cData().cGrid().SizeCellTab(); ++i)
    {
        cgcell = this->cData().cGrid().cCell( i);
        vol = cgcell.Volume() / cgcell.Size();
        for ( MGSize k=0; k<cgcell.Size(); ++k)
            tabvol[cgcell.cId(k)] += vol;
    }

    MGFloat volmin = tabvol[0];
    for ( MGSize i=1; i<tabvol.size(); ++i)
        //if ( volmin > tabvol[i] )
        volmin += tabvol[i];

    volmin /= tabvol.size();

    BVec	tabres[DIM+1];
    BMtx	mtxres[DIM+1][DIM+1];

    CFCell	cfcell;
    CFBFace	cfbface;

    pair<MGSize, MGSize> nodeRange = this->cData().cGrid().cOwnedIds();

    p_RHS->PutScalar(0.0);
    p_A->PutScalar(0.0);

    int index[DIM+1]; //nodal inices
    MGFloat y[ (1+DIM) * BSIZE ];
    Epetra_SerialDenseMatrix vv(BSIZE, BSIZE, false);
    int grow;
    int err;

    // boundary conditions
    for ( MGSize i=0; i<this->cData().cGrid().SizeBFaceTab(); ++i)
    {
        this->GetFBFace( cfbface, i);

        this->ResNJacobianBFace( tabres, mtxres, cfbface); //numerical jacobian

        for ( MGSize k=0; k<cfbface.Size(); ++k)
        {
            index[k] = this->cData().cGrid().cNode( cfbface.cId(k) ).cGId();

            //if ( index[k] < nodeRange.first || index[k] >= nodeRange.second ) //<< This part is unnecessary since epetra will not sum to rows of other processors
            //    for ( MGSize ieq=0; ieq<BSIZE; ++ieq)
            //        y[ieq*DIM + k] = 0.0;
            //else
            for ( MGSize ieq=0; ieq<BSIZE; ++ieq)
                y[ieq*DIM + k] = tabres[k](ieq);
        }
        for ( MGSize ieq=0; ieq<BSIZE; ++ieq)
        {
            err = p_RHS->SumIntoGlobalValues(DIM, ieq, (y+ieq*DIM), index); //<< this will result in an err = 1 for interface cells
            //cout << err << endl;
            //ASSERT( err == 0 );
        }

        for ( MGSize j=0; j<cfbface.Size(); ++j)
        {
            grow = this->cData().cGrid().cNode( cfbface.cId(j) ).cGId();
            if ( grow < nodeRange.first || grow >= nodeRange.second )
                continue;

            err = p_A->BeginSumIntoGlobalValues(grow, cfbface.Size(), index);
            //cout << "Begin Submit " << err << endl;
            for ( MGSize k=0; k<cfbface.Size(); ++k)
            {
                for( MGSize m=0; m<BSIZE; ++m)
                    for( MGSize l=0; l<BSIZE; ++l)
                    {
                        vv(m, l) = mtxres[j][k](m, l);
                    }
                err = p_A->SubmitBlockEntry(vv);
                //cout << "Submit : " << err << " k " << k << endl;
            }
            err = p_A->EndSubmitEntries();
            //cout << "End Submit " << err << " j " << j << endl;
        }
    }

    // cell residuals
    for ( MGSize i=0; i<this->cData().cGrid().SizeCellTab(); ++i)
    {
        this->GetFCell( cfcell, i);

        cfcell.CalcVn();

        this->ResNJacobianCell( tabres, mtxres, cfcell); //numerical jacobian

        for ( MGSize k=0; k<cfcell.Size(); ++k)
        {
            index[k] = this->cData().cGrid().cNode( cfcell.cId(k) ).cGId();

            //if ( index[k] < nodeRange.first || index[k] >= nodeRange.second ) //<< This part is unnecessary since epetra will not sum to rows of other processors
            //    for ( MGSize ieq=0; ieq<BSIZE; ++ieq)
            //        y[ieq*DIM + k] = 0.0;
            //else
            for ( MGSize ieq=0; ieq<BSIZE; ++ieq)
                y[ieq*(DIM+1) + k] = tabres[k](ieq);
        }
        for ( MGSize ieq=0; ieq<BSIZE; ++ieq)
        {
            err = p_RHS->SumIntoGlobalValues(DIM+1, ieq, (y+ieq*(DIM+1)), index); //<< this will result in an err = 1 for interface cells
            //cout << err << endl;
            //ASSERT( err == 0 );
        }

        for ( MGSize j=0; j<cfcell.Size(); ++j)
        {
            grow = this->cData().cGrid().cNode( cfcell.cId(j) ).cGId();
            if ( grow < nodeRange.first || grow >= nodeRange.second )
                continue;

            err = p_A->BeginSumIntoGlobalValues(grow, cfcell.Size(), index);
            //cout << "Begin Submit " << err << endl;
            for ( MGSize k=0; k<cfcell.Size(); ++k)
            {
                for( MGSize m=0; m<BSIZE; ++m)
                    for( MGSize l=0; l<BSIZE; ++l)
                    {
                        vv(m, l) = mtxres[j][k](m, l);
                    }
                err = p_A->SubmitBlockEntry(vv);
                //cout << "Submit : " << err << " k " << k << endl;
            }
            err = p_A->EndSubmitEntries();
            //cout << "End Submit " << err << " j " << j << endl;
        }
    }

    //diagonal elements
    vv.Scale(0.0);
    for ( MGSize i=0; i<this->cData().cGrid().SizeNodeTab(); ++i)
    {
        grow = this->cData().cGrid().cNode( i ).cGId();
        if ( grow < nodeRange.first || grow >= nodeRange.second )
            continue;

        MGFloat val = 1.0 /  tabDT[i];
        for ( MGSize ieq=0; ieq<BSIZE; ++ieq)
            vv(ieq, ieq) = val;

        err = p_A->BeginSumIntoGlobalValues(grow, 1, &grow);
        err = p_A->SubmitBlockEntry(vv);
        err = p_A->EndSubmitEntries();
    }

//    Epetra_Vector vec2(p_RHS->Map(), true);
//    p_RHS->PutScalar(1.0);
//    vec2.PutScalar(0.0);

//    p_A->Apply(*p_RHS, vec2);

//    cout << *p_RHS << endl;
//    cout << vec2 << endl;
//    cout << *p_A << endl;
//    THROW_INTERNAL("STOP");
}

void init_MPITRILINOSSpaceSolver()
{
    //////////////////////////////////////////////////////////////////////
    // 2D

    // Euler
    static ConcCreator<
            MGString,
            MPITRILINOSSpaceSolver< Geom::DIM_2D, EQN_EULER, EquationDef<Geom::DIM_2D,EQN_EULER>::SplitingFull::Block<1> >,
            SpaceSolverBase< Geom::DIM_2D, EQN_EULER, EquationDef<Geom::DIM_2D,EQN_EULER>::SplitingFull::Block<1> >
            >   gCreatorMPITRILINOSSpaceSolverEulerRDS2D( "MPITRILINOS_SPACESOL_2D_EULER_RDS");

    // NS
    static ConcCreator<
            MGString,
            MPITRILINOSSpaceSolver< Geom::DIM_2D, EQN_NS, EquationDef<Geom::DIM_2D,EQN_NS>::SplitingFull::Block<1> >,
            SpaceSolverBase< Geom::DIM_2D, EQN_NS, EquationDef<Geom::DIM_2D,EQN_NS>::SplitingFull::Block<1> >
            >   gCreatorMPITRILINOSSpaceSolverNSRDS2D( "MPITRILINOS_SPACESOL_2D_NS_RDS");

    // RANS SA with splitting
    static ConcCreator<
            MGString,
            MPITRILINOSSpaceSolver< Geom::DIM_2D, EQN_RANS_SA, EquationDef<Geom::DIM_2D,EQN_RANS_SA>::SplitingFlowTurb::Block<1> >,
            SpaceSolverBase< Geom::DIM_2D, EQN_RANS_SA, EquationDef<Geom::DIM_2D,EQN_RANS_SA>::SplitingFlowTurb::Block<1> >
            >   gCreatorMPITRILINOSSpaceSolverRANS1eqFlowRDS2D( "MPITRILINOS_SPACESOL_2D_RANS_SA_RDS");

    static ConcCreator<
            MGString,
            MPITRILINOSSpaceSolver< Geom::DIM_2D, EQN_RANS_SA, EquationDef<Geom::DIM_2D,EQN_RANS_SA>::SplitingFlowTurb::Block<2> >,
            SpaceSolverBase< Geom::DIM_2D, EQN_RANS_SA, EquationDef<Geom::DIM_2D,EQN_RANS_SA>::SplitingFlowTurb::Block<2> >
            >   gCreatorMPITRILINOSSpaceSolverRANS1eqTurbRDS2D( "MPITRILINOS_SPACESOL_2D_RANS_SA_RDS");


    //////////////////////////////////////////////////////////////////////
    // 3D

    // Euler
    static ConcCreator<
            MGString,
            MPITRILINOSSpaceSolver< Geom::DIM_3D, EQN_EULER, EquationDef<Geom::DIM_3D,EQN_EULER>::SplitingFull::Block<1> >,
            SpaceSolverBase< Geom::DIM_3D, EQN_EULER, EquationDef<Geom::DIM_3D,EQN_EULER>::SplitingFull::Block<1> >
            >   gCreatorMPITRILINOSSpaceSolverEulerRDS3D( "MPITRILINOS_SPACESOL_3D_EULER_RDS");

    // NS
    static ConcCreator<
            MGString,
            MPITRILINOSSpaceSolver< Geom::DIM_3D, EQN_NS, EquationDef<Geom::DIM_3D,EQN_NS>::SplitingFull::Block<1> >,
            SpaceSolverBase< Geom::DIM_3D, EQN_NS, EquationDef<Geom::DIM_3D,EQN_NS>::SplitingFull::Block<1> >
            >   gCreatorMPITRILINOSSpaceSolverNSRDS3D( "MPITRILINOS_SPACESOL_3D_NS_RDS");

    // RANS SA with splitting
    static ConcCreator<
            MGString,
            MPITRILINOSSpaceSolver< Geom::DIM_3D, EQN_RANS_SA, EquationDef<Geom::DIM_3D,EQN_RANS_SA>::SplitingFlowTurb::Block<1> >,
            SpaceSolverBase< Geom::DIM_3D, EQN_RANS_SA, EquationDef<Geom::DIM_3D,EQN_RANS_SA>::SplitingFlowTurb::Block<1> >
            >   gCreatorMPITRILINOSSpaceSolverRANS1eqFlowRDS3D( "MPITRILINOS_SPACESOL_3D_RANS_SA_RDS");

    static ConcCreator<
            MGString,
            MPITRILINOSSpaceSolver< Geom::DIM_3D, EQN_RANS_SA, EquationDef<Geom::DIM_3D,EQN_RANS_SA>::SplitingFlowTurb::Block<2> >,
            SpaceSolverBase< Geom::DIM_3D, EQN_RANS_SA, EquationDef<Geom::DIM_3D,EQN_RANS_SA>::SplitingFlowTurb::Block<2> >
            >   gCreatorMPITRILINOSSpaceSolverRANS1eqTurbRDS3D( "MPITRILINOS_SPACESOL_3D_RANS_SA_RDS");

}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

