#include <mpi.h>

#ifdef WITH_PETSC
//#include "petsc/petscksp.h"
#include "petscksp.h"
#endif // WITH_PETSC

#include "libcoresystem/mgdecl.h"

#include "libcorecommon/logo.h"
#include "libcorecommon/factory.h"
#include "libcoreconfig/config.h"
#include "libredcore/processinfo.h"

#include "mpisetup.h"
#include "libred/master.h"


INIT_VERSION ( "0.1.0","'mpi version - wip'" );

using namespace RED;

int main ( int argc, char* argv[] )
{

    try
    {
        CheckTypeSizes();
        MPIRegisterLibraries();

        //Singleton< FactoryList<MGString> >::GetInstance()->PrintInfo();

        MGInt  numtasks, rank, rc;

        rc = MPI_Init ( &argc, &argv );
        if ( rc != MPI_SUCCESS )
        {
            printf ( "Error starting MPI program. Terminating.\n" );
            MPI_Abort ( MPI_COMM_WORLD, rc );
        }

        MPI_Comm_size ( MPI_COMM_WORLD, &numtasks );
        MPI_Comm_rank ( MPI_COMM_WORLD, &rank );

        ProcessInfo::Init ( rank, numtasks );

        if(ProcessInfo::cProcId() == 0)
        {
            PrintLogo();
            cout << "====  ver.: " << VERSION_NUMBER << "  build: " << VERSION_DATE << " " << VERSION_TIME << "====" << endl;
#ifdef WITH_PETSC
            cout << "==== Compiled WITH_PETSC " << endl;
#endif // WITH_PETSC
#ifdef WITH_TRILINOS
            cout << "==== Compiled WITH_TRILINOS" << endl;
#endif // WITH_TRILINOS
            cout <<endl;
        }
        MPI_Barrier(MPI_COMM_WORLD);
        cout << "Number of tasks = " << numtasks << " My rank = " << rank << endl;
        if(ProcessInfo::cProcId() == 0)
            cout <<endl;

#ifdef WITH_PETSC
        //-----Initialize PETSC
        PetscInitialize( NULL, NULL, PETSC_NULL, PETSC_NULL); // PETSC start - this should be the first call to PETSc
#endif // WITH_PETSC

        Master	master;

        if ( argc == 2 )
            master.ReadConfig ( argv[1] );
        else
            master.ReadConfig ( "solv.cfg" );

        master.Create();
        master.Init();
        master.Solve();

#ifdef WITH_PETSC
        PetscFinalize();
#endif // WITH_PETSC

        MPI_Finalize();
    }
    catch ( EHandler::Except& e )
    {
        TRACE_EXCEPTION ( e );
        TRACE_TO_STDERR ( e );
    }
    catch ( exception &e )
    {
        TRACE ( e.what() );
        fprintf ( stderr, "%s\n", e.what() );
    }
    catch ( ... )
    {
        cout << "unknow problem ::  ..." << endl;
    }

    return 0;
}
