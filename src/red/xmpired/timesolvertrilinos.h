#ifndef __TIMESOLVERIMPLICITTRILINOS_H__
#define __TIMESOLVERIMPLICITTRILINOS_H__

#include "libredcore/processinfo.h"
#include "libredconvergence/timesolverbase.h"

#include "libredcore/configconst.h"
#include "libredconvergence/cflmanager.h"

/*<< Trilinos stuff*/
#include "AztecOO.h"

#include "Epetra_MpiComm.h"
#include "Epetra_BlockMap.h"
#include "Epetra_CrsGraph.h"

#include "Epetra_VbrMatrix.h"
#include "Epetra_VbrRowMatrix.h"

#include "Epetra_MultiVector.h"
#include "Epetra_LinearProblem.h"

#include "Teuchos_ParameterList.hpp"

#include "Ifpack_ConfigDefs.h"
#include "Ifpack.h"
#include "Ifpack_AdditiveSchwarz.h"

#include "ml_epetra_preconditioner.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


//////////////////////////////////////////////////////////////////////
//  class TimeSolverImplicit
//////////////////////////////////////////////////////////////////////
template <MGSize BSIZE>
class TimeSolverImplicitTrilinos : public TimeSolverBase<BSIZE>
{
    typedef typename Sparse::Vector<BSIZE>::BlockVec    BlockVec;
    
public:
    TimeSolverImplicitTrilinos() :
        mp_epetra_mpi_comm(NULL),
        mp_epetra_map(NULL),
        mp_epetra_csr_graph(NULL),
        mp_A(NULL),
        mp_Arow(NULL),
        mp_DQ(NULL),
        mp_RHS(NULL),
        mp_Preconditioner(NULL),
        mp_Problem(NULL),
        mp_AztecOOSolver(NULL),
        TimeSolverBase<BSIZE>() {}
    virtual ~TimeSolverImplicitTrilinos()
    {
        if(mp_epetra_mpi_comm != NULL) delete mp_epetra_mpi_comm;
        if(mp_epetra_csr_graph != NULL) delete mp_epetra_csr_graph;
        if(mp_epetra_map != NULL) delete mp_epetra_map;
        if(mp_A != NULL) delete mp_A;
        if(mp_Arow != NULL) delete mp_Arow;
        if(mp_DQ != NULL) delete mp_DQ;
        if(mp_RHS != NULL) delete mp_RHS;
        if(mp_Preconditioner != NULL) delete mp_Preconditioner;
        if(mp_Problem != NULL) delete mp_Problem;
        if(mp_AztecOOSolver != NULL) delete mp_AztecOOSolver;
    }

    virtual void    Create( const CfgSection* pcfgsec, SpaceSolverFacade<BSIZE>* pssol);
    virtual void    PostCreateCheck() const;
    virtual void    Init();

    virtual void    DoStep( ConvergenceInfo<BSIZE>& info);

protected:
    void Iterate();
private:
    vector<MGFloat> mtabLocDT;

    CFLManager      mCFLManager;

    // ownership info
    pair<MGSize,MGSize> mRange;
    vector<MGSize>      mtabGlobId;

    //
    const CfgSection     * mp_confSect;
    MGString p_PrecondType;

    // Trilinos stuff << There seems not to be empty def constructors ...
    Epetra_MpiComm       * mp_epetra_mpi_comm;
    Epetra_BlockMap      * mp_epetra_map;
    Epetra_CrsGraph      * mp_epetra_csr_graph;
    Epetra_VbrMatrix     * mp_A;
    Epetra_VbrRowMatrix  * mp_Arow;
    Epetra_Vector        * mp_DQ;
    Epetra_Vector        * mp_RHS;
    Epetra_Operator      * mp_Preconditioner;
    Epetra_LinearProblem * mp_Problem;
    AztecOO              * mp_AztecOOSolver;
};


template <MGSize BSIZE>
void TimeSolverImplicitTrilinos<BSIZE>::Create( const CfgSection* pcfgsec, SpaceSolverFacade<BSIZE>* pssol)
{
    TimeSolverBase<BSIZE>::Create( pcfgsec, pssol);

    this->mp_epetra_mpi_comm = new Epetra_MpiComm( MPI_COMM_WORLD );
}

template <MGSize BSIZE>
void TimeSolverImplicitTrilinos<BSIZE>::PostCreateCheck() const
{
    TimeSolverBase<BSIZE>::PostCreateCheck();

    // do check
}


template <MGSize BSIZE>
void TimeSolverImplicitTrilinos<BSIZE>::Init()
{
    const MGSize nsol = this->cSpaceSol().SolutionSize();

    this->mvecQ.Resize( nsol);
    mtabLocDT.resize( nsol);

    MGSize nstart = 0;
    if ( this->ConfigSect().KeyExists( ConfigStr::Solver::Executor::TimeSol::NCFLSTART ) )
        nstart = this->ConfigSect().ValueSize( ConfigStr::Solver::Executor::TimeSol::NCFLSTART );

    mCFLManager.Init( this->ConfigSect().ValueFloat( ConfigStr::Solver::Executor::TimeSol::CFL),
                      this->ConfigSect().ValueFloat( ConfigStr::Solver::Executor::TimeSol::MAXCFL ),
                      this->ConfigSect().ValueSize( ConfigStr::Solver::Executor::TimeSol::NCFLGROWTH ),
                      nstart);
    mCFLManager.Reset();

    this->rSpaceSol().TRILINOSInitOwnedIdRange( mRange, mtabGlobId );

    ////-----Create TRILINOS stuff
    this->mp_epetra_map = new Epetra_BlockMap( -1, mRange.second - mRange.first, BSIZE, 0, *mp_epetra_mpi_comm );
    this->mp_DQ = new Epetra_Vector( *mp_epetra_map, false );
    this->mp_RHS = new Epetra_Vector( *mp_epetra_map, false );

    this->rSpaceSol().TRILINOSResizeGraphAndMatrix(*mp_epetra_map, mp_epetra_csr_graph, mp_A);
    mp_Arow = new Epetra_VbrRowMatrix( mp_A );

    //this->mp_Problem = new Epetra_LinearProblem(mp_A, mp_DQ ,mp_RHS);
    this->mp_AztecOOSolver = new AztecOO();

    if(ProcessInfo::cProcId() == 0)
        cout << " Before matrix set " << endl;
    sleep(5);
    MPI_Barrier(MPI_COMM_WORLD);
    this->mp_AztecOOSolver->SetUserMatrix(mp_Arow, false);
    if(ProcessInfo::cProcId() == 0)
        cout << " After matrix set " << endl;
    sleep(5);
    MPI_Barrier(MPI_COMM_WORLD);

    this->mp_AztecOOSolver->SetLHS(mp_DQ);
    this->mp_AztecOOSolver->SetRHS(mp_RHS);

    this->mp_AztecOOSolver->SetAztecOption(AZ_diagnostics,AZ_all);
    this->mp_AztecOOSolver->SetAztecOption(AZ_output,20);
    this->mp_AztecOOSolver->SetAztecOption(AZ_solver,AZ_gmres);
    this->mp_AztecOOSolver->SetAztecOption(AZ_conv, AZ_noscaled);
    this->mp_AztecOOSolver->SetAztecOption(AZ_kspace,this->ConfigSect().ValueSize( ConfigStr::Solver::Executor::TimeSol::LINDKSPACE ));


    mp_confSect = & this->ConfigSect().GetSection( ConfigStr::Solver::Executor::TimeSol::Trilinos::NAME );
    p_PrecondType = mp_confSect->ValueString( ConfigStr::Solver::Executor::TimeSol::Trilinos::PrecondType::KEY );

    if(p_PrecondType == ConfigStr::Solver::Executor::TimeSol::Trilinos::PrecondType::Value::NONE )
    {
        this->mp_AztecOOSolver->SetAztecOption(AZ_precond,AZ_none);
    }
    else if(p_PrecondType == ConfigStr::Solver::Executor::TimeSol::Trilinos::PrecondType::Value::AZTECOO )
    {
        this->mp_AztecOOSolver->SetAztecOption(AZ_precond,AZ_dom_decomp);
        this->mp_AztecOOSolver->SetAztecOption(AZ_subdomain_solve,AZ_ilut);
        this->mp_AztecOOSolver->SetAztecParam(AZ_ilut_fill,
              mp_confSect->ValueFloat( ConfigStr::Solver::Executor::TimeSol::Trilinos::PrecondType::TIM_PRE_ILU_FILL ));
        this->mp_AztecOOSolver->SetAztecParam(AZ_drop,
              mp_confSect->ValueFloat( ConfigStr::Solver::Executor::TimeSol::Trilinos::PrecondType::TIM_PRE_ILU_DROP ));
        this->mp_AztecOOSolver->SetAztecParam(AZ_athresh,
              mp_confSect->ValueFloat( ConfigStr::Solver::Executor::TimeSol::Trilinos::PrecondType::TIM_PRE_ILU_ATRSH ));
        this->mp_AztecOOSolver->SetAztecParam(AZ_rthresh,
              mp_confSect->ValueFloat( ConfigStr::Solver::Executor::TimeSol::Trilinos::PrecondType::TIM_PRE_ILU_RTRSH));
        this->mp_AztecOOSolver->SetAztecParam(AZ_omega,
              mp_confSect->ValueFloat( ConfigStr::Solver::Executor::TimeSol::Trilinos::PrecondType::TIM_PRE_ILU_RELAX ));
    }
    else if(p_PrecondType == ConfigStr::Solver::Executor::TimeSol::Trilinos::PrecondType::Value::IFPACK )
    {
        Ifpack Factory;
        Teuchos::ParameterList List;
        string PrecType = "ILUT";
        int OverlapLevel = 1;
        //mp_Arow = new Epetra_VbrRowMatrix( mp_A );
        Ifpack_Preconditioner *prec = Factory.Create(PrecType, mp_Arow, OverlapLevel);
        this->mp_Preconditioner = prec;
        List.set("fact: ilut level-of-fill",
                 mp_confSect->ValueFloat( ConfigStr::Solver::Executor::TimeSol::Trilinos::PrecondType::TIM_PRE_ILU_FILL ));
        List.set("fact: drop tolerance",
                 mp_confSect->ValueFloat( ConfigStr::Solver::Executor::TimeSol::Trilinos::PrecondType::TIM_PRE_ILU_DROP ));
        List.set("fact: absolute threshold",
                 mp_confSect->ValueFloat( ConfigStr::Solver::Executor::TimeSol::Trilinos::PrecondType::TIM_PRE_ILU_ATRSH ));
        List.set("fact: relative threshold",
                 mp_confSect->ValueFloat( ConfigStr::Solver::Executor::TimeSol::Trilinos::PrecondType::TIM_PRE_ILU_RTRSH ));
        List.set("fact: relax value",
                 mp_confSect->ValueFloat( ConfigStr::Solver::Executor::TimeSol::Trilinos::PrecondType::TIM_PRE_ILU_RELAX ));
        List.set("schwarz: combine mode", "Add");// "Add", "Zero", "Insert", "InsertAdd", "Average", "AbsMax"
        prec->SetParameters(List);
    }
    else if(p_PrecondType == ConfigStr::Solver::Executor::TimeSol::Trilinos::PrecondType::Value::ML )
    {
        Teuchos::ParameterList MLList;
        ML_Epetra::SetDefaults(mp_confSect->ValueString( ConfigStr::Solver::Executor::TimeSol::Trilinos::PrecondType::TIM_PRE_ML_TYPE ),MLList);
        MLList.set("ML output",1);
        MLList.set("PDE equations", (int)BSIZE);
        //mp_Arow = new Epetra_VbrRowMatrix( mp_A );
        this->mp_Preconditioner = new ML_Epetra::MultiLevelPreconditioner( *mp_Arow , MLList, false);
    }
}

template <MGSize BSIZE>
void TimeSolverImplicitTrilinos<BSIZE>::DoStep( ConvergenceInfo<BSIZE>& info)
{
    //CheckQ( mvecQ); // checking for non-physical values
    //CheckIsNAN( mvecQ); //checking for NAN

    MGFloat cfl = mCFLManager.cCFL();
    mCFLManager.Push();

    this->rSpaceSol().InitDT( mtabLocDT, cfl);
    // compute residuum & jacobian
    this->rSpaceSol().TRILINOSResNumJacob( mp_RHS, mp_A, mtabLocDT);

    // check RHS for NAN
    double *val;
    this->mp_RHS->ExtractView( &val );
    for ( MGSize i=0; i<this->mp_epetra_map->NumMyPoints(); i++)
    {
        if ( ISNAN( val[i] ) )
            THROW_INTERNAL( "TimeSolverImplicitPETSC<BSIZE>::DoStep -- mRHS is NAN :: i='" << i/BSIZE << "'" << " ieq='" << i%BSIZE << "'");
    }

    // init solver
    mp_DQ->PutScalar(0.0);
    mp_RHS->Scale(-1.0);

    Iterate();

    if ( ProcessInfo::cProcId() == 0 )
    {
        cout << resetiosflags(ios_base::scientific);
        cout << "cfl = " << cfl << " max_iter = " << this->mp_AztecOOSolver->NumIters() << " true eps = " << this->mp_AztecOOSolver->TrueResidual() << " scaled eps = " << this->mp_AztecOOSolver->ScaledResidual() << " time = " << this->mp_AztecOOSolver->SolveTime() <<endl;
    }

    MGFloat  *ptab;
    this->mp_DQ->ExtractView(&ptab);

    MGSize index = 0;
    for (MGSize i=0; i<this->mvecQ.Size(); ++i)
    {
        MGSize globid = mtabGlobId[i];
        if ( globid < mRange.first || globid >= mRange.second )
            continue;

        for( MGSize ieq=0; ieq<BSIZE; ++ieq)
        {
            if ( ISNAN( ptab[index]) )
                THROW_INTERNAL( "TimeSolverImplicitPETSC<BSIZE>::DoStep -- vecDQ is NAN :: i='" << i << "'" << " ieq='" << ieq << "'");

            this->mvecQ[i](ieq) += ptab[index];
            ++index;
        }
    }

    this->rSpaceSol().ApplyStrongBC( this->mvecQ );

    MGFloat errRes, errDQ;

    this->mp_RHS->Norm2( &errRes );
    //this->mp_DQ->Norm2( &errDQ );

    for(MGSize i=0; i<BSIZE; ++i)
    {
        info.mErrResL2[i] = errRes;
        //info.mErrDQLinf[i] = errDQ;
    }
}

template <MGSize BSIZE>
void TimeSolverImplicitTrilinos<BSIZE>::Iterate()
{
    MGSize max_iter 	= this->ConfigSect().ValueSize( ConfigStr::Solver::Executor::TimeSol::LINMAXITER );
    MGFloat gmres_eps 	= this->ConfigSect().ValueFloat( ConfigStr::Solver::Executor::TimeSol::LINMINEPS );

    if(p_PrecondType == ConfigStr::Solver::Executor::TimeSol::Trilinos::PrecondType::Value::IFPACK )
    {
        (dynamic_cast<Ifpack_Preconditioner*>(mp_Preconditioner))->Compute();
        this->mp_AztecOOSolver->SetAztecOption(AZ_precond,AZ_user_precond);
        this->mp_AztecOOSolver->SetPrecOperator(this->mp_Preconditioner);
    }
    else if(p_PrecondType == ConfigStr::Solver::Executor::TimeSol::Trilinos::PrecondType::Value::ML )
    {
        (dynamic_cast<ML_Epetra::MultiLevelPreconditioner*>(mp_Preconditioner))->ComputePreconditioner();
        this->mp_AztecOOSolver->SetAztecOption(AZ_precond,AZ_user_precond);
        this->mp_AztecOOSolver->SetPrecOperator(this->mp_Preconditioner);
    }

    this->mp_AztecOOSolver->Iterate(max_iter,gmres_eps);

    if(p_PrecondType == ConfigStr::Solver::Executor::TimeSol::Trilinos::PrecondType::Value::ML )
    {
        (dynamic_cast<ML_Epetra::MultiLevelPreconditioner*>(mp_Preconditioner))->DestroyPreconditioner();
    }
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

#endif // __TIMESOLVERIMPLICITTRILINOS_H__

