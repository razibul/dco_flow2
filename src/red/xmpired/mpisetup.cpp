#include "mpisetup.h"
#include "libred/setup.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


void init_MPISolver();
void init_MPIPETScSpaceSolver();
void init_MPIPETScTimeSolverImplicit();
void init_MPITrilinosTimeSolverImplicit();
void init_MPITRILINOSSpaceSolver();



void RegisterLib_MPI()
{
	init_MPISolver();

#ifdef WITH_PETSC
	init_MPIPETScSpaceSolver();
	init_MPIPETScTimeSolverImplicit();
#endif // WITH_PETSC

#ifdef WITH_TRILINOS
    init_MPITRILINOSSpaceSolver();
    init_MPITrilinosTimeSolverImplicit();
#endif // WITH_TRILINOS

}


void MPIRegisterLibraries()
{
	RegisterLibraries();
	RegisterLib_MPI();
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

