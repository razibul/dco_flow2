#ifndef __MPI_PERIODICBC_H__
#define __MPI_PERIODICBC_H__

//#include "libcoresystem/mgdecl.h"
//#include "libcoregeom/dimension.h"
//
//#include "libredcore/processinfo.h"
//
#include "redvc/libredvcgrid/gridsimplex.h"
#include "libredcore/interface.h"
//
//#include "communicator.h"

#include "libredcore/equation.h"
#include "redvc/libredvccommon/periodicbcbase.h"

#include "libhfgeom/hf_typedef.h"
#include "libhfgeom/hf_intersection.h"

#include <fstream>

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

	template<Dimension DIM>
	struct LocInterface
	{
		MGSize	tabnode[DIM];
		MGFloat	tabcoord[DIM];
		//MGSize remprocid;
	};

	template<Dimension DIM>
	struct LocBFace
	{
		Vect<DIM>	mpos[DIM];
	};

	template<Dimension DIM>
	struct NodeKey
	{
		MGSize locid;
		//MGSize globid;
		MGSize procid;
		Vect<DIM> pos;
	};


	template<typename T>
	class MPIAllGather
	{/*
		template<typename V, typename DUMMY = void>
		struct MPITypeSwitch
		{
			static MPI_Datatype type();
		};

		template<typename DUMMY>
		struct MPITypeSwitch<MGSize, DUMMY>
		{
			static MPI_Datatype type()	{ return MPI_INT; }
		};

		template<typename DUMMY>
		struct MPITypeSwitch<MGFloat, DUMMY>
		{
			static MPI_Datatype type()	{ return MPI_DOUBLE; }
		};*/
		static MPI_Datatype	mpitype()		{ return MPI_DATATYPE_NULL; }
	public:
		static void DoAllGather(vector<T>& out, vector<T>& in, vector<MGSize>& sizetab)
		{
			vector<MGSize> outsize(ProcessInfo::cNumProc(), out.size());
			vector<MGSize> insize(ProcessInfo::cNumProc(), 0);

			MPI_Alltoall(&outsize[0], 1, MPI_UNSIGNED, &insize[0], 1, MPI_UNSIGNED, MPI_COMM_WORLD);

			MGSize maxsize = *std::max_element(insize.begin(), insize.end());
			MGSize sizeglob = ProcessInfo::cNumProc() * maxsize;

			vector<MGSize> sizeoutoffset(ProcessInfo::cNumProc(), out.size());
			sizetab.resize(ProcessInfo::cNumProc(), 0);
			MPI_Alltoall(&sizeoutoffset[0], 1, MPI_UNSIGNED, &sizetab[0], 1, MPI_UNSIGNED, MPI_COMM_WORLD);

			out.resize(maxsize, 0);

			cout << "Proc: " << ProcessInfo::cProcId() << "\tbef: " << out.size() << "\taft: " << sizeglob << endl;

			in.resize(sizeglob, 0);
			//MPI_Allgather(&out[0], maxsize, MPITypeSwitch<T>::type(), &in[0], maxsize, MPITypeSwitch<T>::type(), MPI_COMM_WORLD);
			MPI_Allgather(&out[0], maxsize, mpitype(), &in[0], maxsize, mpitype(), MPI_COMM_WORLD);
		}
	};

	template<>
	MPI_Datatype	MPIAllGather<MGSize>::mpitype()		{ return MPI_INT; }
	template<>
	MPI_Datatype	MPIAllGather<MGFloat>::mpitype()	{ return MPI_DOUBLE; }

	//////////////////////////////////////////////////////////////////////
	// class MPIPeriodicBC
	//////////////////////////////////////////////////////////////////////

	template<Dimension DIM, EQN_TYPE ET>
	class MPIPeriodicBC : public PeriodicBCBase<DIM, ET>
	{
	public:
		enum { ESIZE = EquationDef<DIM, ET>::SIZE };

		typedef GridSimplex<DIM>			TGrid;
		typedef EquationDef<DIM, ET>		EqDef;
		typedef typename EqDef::FVec		FVec;

		typedef KeyData<MGSize, PeriodicKeyData<DIM> >	PeriodicKey;
		typedef vector<PeriodicKey>						ColPeriodic;

		void Init(TGrid* grid, Solution<DIM, ET>* sol, Interface* interf, const CfgSection* pphysect);

		virtual void GetPeriodicVar(const MGSize& id, FVec& v) const;

	private:
		void InitLocInterface();
		void FindMapping(
			const MGSize& srcid,
			const MGSize& trgid,
			const Vect<DIM>& vct,
			const MGFloat& angle,
			vector< KeyData<MGSize, NodeKey<DIM> > >& globtabkey,
			vector < KeyData<MGSize, Key<DIM> >	>& globtabbfacekey);

		void CollectSurf(
			const MGSize& srcid,
			const MGSize& trgid,
			vector< KeyData<MGSize, NodeKey<DIM> > >& globtabkey,
			vector < KeyData<MGSize, Key<DIM> >	>& globtabbfacekey);

		void SetGlobalInterface(
			vector< KeyData<MGSize, NodeKey<DIM> > >& globtabkey,
			vector < KeyData<MGSize, Key<DIM> >	>& globtabbfacekey);

		void SetGlobalInterface(const vector<Key<3> >& mapping);

		ColPeriodic										mcolperiodic;
		TGrid*											mpGrid;
		Solution<DIM, ET>*								mpsol;
		vector< KeyData<MGSize, LocInterface<DIM> > >	mcollocinterace;

		Interface*										mpinterface;
	};

	template<Dimension DIM, EQN_TYPE ET>
	void MPIPeriodicBC<DIM, ET>::Init(TGrid* grid, Solution<DIM, ET>* sol, Interface* interf, const CfgSection* pphysect)
	{
		mpsol = sol;
		mpGrid = grid;
		mpinterface = interf;
		this->ReadConfig(pphysect);
		InitLocInterface();
	}

	template<Dimension DIM, EQN_TYPE ET>
	void MPIPeriodicBC<DIM, ET>::SetGlobalInterface(const vector<Key<3> >& mapping)
	{
		MGSize interfsize = mpinterface->Size();
		mpinterface->IOTabIntfResize(interfsize + mapping.size());

		MGSize id = interfsize;
		for (MGSize i = 0; i < mapping.size(); ++i)
		{
			vector<MGSize> tab;
			tab.push_back(mapping[i].cFirst());
			tab.push_back(mapping[i].cSecond());
			tab.push_back(mapping[i].cThird());
			mpinterface->IOSetIds(tab, id++);
		}
	}

	template<Dimension DIM, EQN_TYPE ET>
	void MPIPeriodicBC<DIM, ET>::GetPeriodicVar(const MGSize& id, FVec& v) const
	{
		typename vector< KeyData<MGSize, LocInterface<DIM> > >::const_iterator citr = lower_bound(mcollocinterace.begin(), mcollocinterace.end(), KeyData<MGSize, LocInterface<DIM> >(id));
		if ((citr != mcollocinterace.end()) && (citr->first == id) )
		{
			for (MGSize i = 0; i < ESIZE; ++i)
			{
				v[i] = 0.;
			}

			for (MGSize i = 0; i < DIM; ++i)
			{
				for (MGSize j = 0; j < ESIZE; ++j)
				{
					v[j] += (*mpsol)[citr->second.tabnode[i]][j] * citr->second.tabcoord[i];
				}
			}
		}
		else
		{
			THROW_INTERNAL("MPIPeriodicBC::GetPeriodicVar id not found");
		}
	}

	template<Dimension DIM, EQN_TYPE ET>
	void MPIPeriodicBC<DIM, ET>::CollectSurf(
		const MGSize& srcid,
		const MGSize& trgid,
		vector< KeyData<MGSize, NodeKey<DIM> > >& globtabkey,
		vector < KeyData<MGSize, Key<DIM> >	>& globtabbfacekey)
	{
		for (MGSize inx = 0; inx < mpGrid->SizeBFaceTab(); ++inx)
		{
			CGeomBFace<DIM> bface = mpGrid->cBFace(inx);
			if (bface.cSurfId() == trgid)
			{
				MGSize tmp[DIM];
				for (MGSize i = 0; i < DIM; ++i)
				{
					MGSize globnodeid = bface.cGId(i);
					if (mpGrid->IsOwned(globnodeid))
					{
						//KeyData<MGSize, pair<MGSize, Vect<DIM> > > key;
						//key.first = globnodeid;
						//key.second.first = ProcessInfo::cProcId();
						//key.second.second = bface.cNode(i).cPos();
						KeyData<MGSize, NodeKey<DIM> > key;
						key.first = globnodeid;
						//key.second.globid = globnodeid;
						key.second.locid = bface.cId(i);
						key.second.procid = ProcessInfo::cProcId();
						key.second.pos = bface.cNode(i).cPos();

						globtabkey.push_back(key);

						tmp[i] = globnodeid;
					}

				}

				KeyData<MGSize, Key<DIM> > key;
				key.first = inx;
				for (MGSize i = 0; i < DIM; ++i)
				{
					key.second.rElem(i) = tmp[i];
				}
				globtabbfacekey.push_back(key);
			}
		}

		sort(globtabkey.begin(), globtabkey.end());
		typename vector< KeyData<MGSize, NodeKey<DIM> > >::iterator uitr = unique(globtabkey.begin(), globtabkey.end());
		globtabkey.erase(uitr, globtabkey.end());

		//////////////////////////////////////////////////////////////
		vector< MGFloat>	globtabpos;
		vector< MGSize>		globtabnode;
		vector< MGSize>		globtabbface;

		vector< MGFloat>	tabpos;
		vector< MGSize>		tabnode;
		vector< MGSize>		tabbface;

		for (MGSize i = 0; i < globtabkey.size(); ++i)
		{
			tabnode.push_back(globtabkey[i].first);
			tabnode.push_back(globtabkey[i].second.procid);
			tabnode.push_back(globtabkey[i].second.locid);	//block by global node id, process id and local node id: [globid procid locid1] [globid2 procid2 locid2]
			//tabnode.push_back(globtabkey[i].second.first); //block by global node id and process id: [globid procid] [globid2 procid2]
			for (MGSize j = 0; j < DIM; ++j)
			{
				tabpos.push_back(globtabkey[i].second.pos.cX(j)); //block by DIM e.g.: [x y z] [x2 y2 z2] ...
			}
		}

		for (MGSize i = 0; i < globtabbfacekey.size(); ++i)
		{
			tabbface.push_back(globtabbfacekey[i].first);
			for (MGSize j = 0; j < DIM; ++j)
			{
				tabbface.push_back(globtabbfacekey[i].second.cElem(j));
			}
		}

		vector<MGSize> tabnodesize;
		MPIAllGather<MGSize>::DoAllGather(tabnode, globtabnode, tabnodesize);
		vector<MGSize> tabpossize;
		MPIAllGather<MGFloat>::DoAllGather(tabpos, globtabpos, tabpossize);
		vector<MGSize> tabbfacesize;
		MPIAllGather<MGSize>::DoAllGather(tabbface, globtabbface, tabbfacesize);


		globtabbfacekey.clear();
		MGSize maxsize = globtabbface.size() / ProcessInfo::cNumProc();

		vector<MGSize>::const_iterator it = globtabbface.begin();

		for (MGSize i = 0; i < tabbfacesize.size(); ++i)
		{
			MGSize count = 0;
			while (count < tabbfacesize[i])
			{
				KeyData<MGSize, Key<DIM> > key;
				key.first = *it++;
				++count;

				for (MGSize j = 0; j < DIM; ++j)
				{
					key.second.rElem(j) = *it++;
					++count;
				}
				globtabbfacekey.push_back(key);
			}
			it += maxsize - tabbfacesize[i];
		}

		globtabkey.clear();
		maxsize = globtabnode.size() / ProcessInfo::cNumProc();

		it = globtabnode.begin();

		for (MGSize i = 0; i < tabnodesize.size(); ++i)
		{
			MGSize count = 0;
			while (count < tabnodesize[i])
			{
				KeyData<MGSize, NodeKey<DIM> > key;
				key.first = *it++;
				++count;

				key.second.procid = *it++;
				++count;

				key.second.locid = *it++;
				++count;

				globtabkey.push_back(key);
			}
			it += maxsize - tabnodesize[i];
		}

		maxsize = globtabpos.size() / ProcessInfo::cNumProc();

		vector<MGFloat>::const_iterator itpos = globtabpos.begin();

		MGSize idx = 0;
		for (MGSize i = 0; i < tabpossize.size(); ++i)
		{
			MGSize count = 0;
			while (count < tabpossize[i])
			{
				for (MGSize j = 0; j < DIM; j++)
				{
					globtabkey[idx].second.pos.rX(j) = *itpos++;
					++count;
				}
				++idx;
			}
			itpos += maxsize - tabpossize[i];
		}

		sort(globtabkey.begin(), globtabkey.end());
		typename vector< KeyData<MGSize, NodeKey<DIM> > >::iterator uit = unique(globtabkey.begin(), globtabkey.end());
		globtabkey.erase(uit, globtabkey.end());

		sort(globtabbfacekey.begin(), globtabbfacekey.end());
		typename vector < KeyData<MGSize, Key<DIM> > >::iterator uitbface = unique(globtabbfacekey.begin(), globtabbfacekey.end());
		globtabbfacekey.erase(uitbface, globtabbfacekey.end());
	}

	template<Dimension DIM, EQN_TYPE ET>
	void MPIPeriodicBC<DIM, ET>::InitLocInterface()
	{


	//	for (MGSize i = 0; i < this->mparams.size(); ++i)
		{
			switch (this->mparams[0].type)
			{
			case PRD_TRANS:
			{
				MGSize srcid = this->mparams[0].src;
				MGSize trgid = this->mparams[0].trg;
				Vect<DIM> vct = this->mparams[0].vct;

				vector< KeyData<MGSize, NodeKey<DIM> > >	globtabkey; //glob node id, owner proc id, position
				vector < KeyData<MGSize, Key<DIM> >	>		globtabbfacekey; //loc face id, glob node ids
				CollectSurf(srcid, trgid, globtabkey, globtabbfacekey);
				FindMapping(srcid, trgid, vct, 0, globtabkey, globtabbfacekey);
				SetGlobalInterface(globtabkey, globtabbfacekey);

				//globtabkey.clear();
				//globtabbfacekey.clear();

				//vct *= -1;
				//CollectSurf(trgid, srcid, globtabkey, globtabbfacekey);
				//FindMapping(trgid, srcid, vct, 0, globtabkey, globtabbfacekey);
				//SetGlobalInterface(globtabkey, globtabbfacekey);
				//////////////////////////////////////////////////////////////

				break;
			}
			case PRD_ANGLE:
				THROW_INTERNAL("Angle periodic bc not implemented.");
				break;
			case PRD_NONE:
				//THROW_INTERNAL("Angle periodic bc not implemented.");
				break;
			default:
				THROW_INTERNAL("Unknown periodic bc type.");
				break;
			}
		}
	}

	template<Dimension DIM, EQN_TYPE ET>
	void MPIPeriodicBC<DIM, ET >::FindMapping(
		const MGSize& srcid,
		const MGSize& trgid,
		const Vect<DIM>& vct, 
		const MGFloat& angle,
		vector< KeyData<MGSize, NodeKey<DIM> > >& globtabkey,
		vector < KeyData<MGSize, Key<DIM> >	>& globtabbfacekey)
	{
		typedef KeyData<MGSize, NodeKey<DIM> >	GlobKey;
		typedef vector< GlobKey >				ColGlobKey;
		
		typedef HFGeom::GVector<MGFloat, DIM> Vector;

		for (MGSize inx = 0; inx < mpGrid->SizeBFaceTab(); ++inx)
		{
			CGeomBFace<DIM> bface = mpGrid->cBFace(inx);

			if (bface.cSurfId() == srcid)
			{
				for (MGSize trgbfaceid = 0; trgbfaceid < globtabbfacekey.size(); ++trgbfaceid)
				{
					Vector tabpos[DIM];
					MGSize trgcellid = trgbfaceid;	//id IN vector, not id in grid (local nor global)
					for (MGSize i = 0; i < DIM; ++i)
					{
						const MGSize trgnodeid = globtabbfacekey[trgbfaceid].second.cElem(i);

						typename ColGlobKey::const_iterator cit = lower_bound(globtabkey.begin(), globtabkey.end(), GlobKey(trgnodeid));
						if ((cit != globtabkey.end()) && (cit->first == trgnodeid))
						{
							for (MGSize j = 0; j < DIM; ++j)
							{
								tabpos[i].rX(j) = cit->second.pos.cX(j);
							}
						}
						else
						{
							THROW_INTERNAL("Unknown nodeid in MPIPeriodicBC<DIM, ET >::FindMapping\n");
						}
					}

					MGFloat maxedgesize = 0.;
					for (MGSize i = 0; i < DIM - 1; ++i)
					{
						MGFloat len = 0.;
						Vector tmp(tabpos[i] - tabpos[(i + 1) % DIM]);
						for (MGSize ii = 0; ii < DIM; ++ii)
						{
							len += tmp.cX(ii)*tmp.cX(ii);
						}
						len = sqrt(len);
						if (len > maxedgesize)
						{
							maxedgesize = len;
						}
					}

					HFGeom::SubSimplex<MGFloat, DIM - 1, DIM> subb((typename HFGeom::SubSimplex<MGFloat, DIM - 1, DIM>::ARRAY(tabpos)));

					for (MGSize i = 0; i < bface.Size(); ++i)
					{
						if (mpGrid->IsOwned(bface.cGId(i)))
						{
							Vector pos;
							const MGSize& nodeid = bface.cId(i);

							for (MGSize j = 0; j < DIM; j++)
							{
								pos.rX(j) = mpGrid->cNode(nodeid).cPos().cX(j) + vct.cX(j);
							}

							HFGeom::SubSimplex<MGFloat, 0, DIM> suba((typename  HFGeom::SubSimplex<MGFloat, 0, DIM>::ARRAY(pos)));

							HFGeom::IntersectionProx<MGFloat, MGFloat, 0, DIM - 1, DIM> inter(suba, subb);
							inter.Execute();

							MGFloat	tabB[DIM];
							inter.GetBBaryCoords(tabB);

							bool bin = true;
							for (MGSize i = 0; i < DIM; ++i)
							{
								tabB[i] /= inter.cDet();
								if (tabB[i] < -maxedgesize*1e-4)
								{
									bin = false;
								}
							}
							if (bin)
							{
								PeriodicKey key;
								key.first = nodeid;
								key.second.mcellid = trgcellid;
								for (MGSize j = 0; j < DIM; ++j)
								{
									key.second.mtabcoord.rElem(j) = tabB[j];
								}

								mcolperiodic.push_back(key);
							}
						}
					}					
				}
			}
		}
	}

	template<Dimension DIM, EQN_TYPE ET>
	void MPIPeriodicBC<DIM, ET>::SetGlobalInterface(
		vector< KeyData<MGSize, NodeKey<DIM> > >& globtabkey,
		vector< KeyData<MGSize, Key<DIM> >	>& globtabbfacekey)
	{
		typedef KeyData<MGSize, NodeKey<DIM> >	GlobKey;
		typedef vector< GlobKey >				ColGlobKey;

		vector< KeyData<MGSize, MGSize> >	nodemap;



		for (MGSize  i = 0; i < mcolperiodic.size(); ++i)
		{
			const PeriodicKey& key = mcolperiodic[i];

			//MGSize localnodeid = key.first;
			MGSize bfaceid = key.second.mcellid;

		//	KeyData< MGSize, LocInterface<DIM> >	interfkey;
		//	interfkey.first = key.first;	//local nodeid

			for (MGSize j = 0; j < DIM; ++j)
			{
				MGSize globnodeid = globtabbfacekey[bfaceid].second.cElem(j);
				typename ColGlobKey::const_iterator cit = lower_bound(globtabkey.begin(), globtabkey.end(), GlobKey(globnodeid));
				if ((cit != globtabkey.end()) && (cit->first == globnodeid))
				{
					//nodemap.push_back(KeyData<MGSize, MGSize>(cit - globtabkey.begin(), 0));
					if (cit->second.procid != ProcessInfo::cProcId())
					{
						nodemap.push_back(KeyData<MGSize, MGSize>(cit->first, 0));
					}
					else
					{
						//key.second.tab
				//		interfkey.second.tabnode[j] = cit->second.locid;
					}
					//interfkey.second.tabnode[j] = cit - globtabkey.begin();
				//	interfkey.second.tabcoord[j] = key.second.mtabcoord.rElem(j);
				}
				else
				{
					THROW_INTERNAL("Error in MPIPeriodicBC<DIM, ET>::SetGlobalInterface: nodeid not found\n");
				}
			}
		//	mcollocinterace.push_back(interfkey);
		}

		sort(nodemap.begin(), nodemap.end());
		typename vector< KeyData<MGSize, MGSize> >::iterator uit = unique(nodemap.begin(), nodemap.end());
		nodemap.erase(uit, nodemap.end());

		MGSize sizenodetabold = mpGrid->IOSizeNodeTab();
		mpGrid->IOTabNodeResize(sizenodetabold + nodemap.size());
		mpsol->Resize(sizenodetabold + nodemap.size());

		//for (MGSize i = 0; i < nodemap.size(); ++i)
		//{
		//	nodemap[i].second = sizenodetabold + i;
		//	vector<MGFloat> pos;
		//	typename ColGlobKey::const_iterator cit = lower_bound(globtabkey.begin(), globtabkey.end(), GlobKey(nodemap[i].first));
		//	if ((cit != globtabkey.end()) && (cit->first == nodemap[i].first))
		//	{
		//		for (MGSize j = 0; j < DIM; ++j)
		//		{
		//			pos[j] = cit->second.pos.cX(j);
		//		}
		//	}
		//	mpGrid->IOSetNodePos(pos, nodemap[i].second);
		//}

		//for (MGSize i = 0; i < mcolperiodic.size(); ++i)
		//{
		//	const PeriodicKey& key = mcolperiodic[i];

		//	MGSize bfaceid = key.second.mcellid;

		//	KeyData< MGSize, LocInterface<DIM> >	interfkey;
		//	interfkey.first = key.first;	//local nodeid

		//	for (MGSize j = 0; j < DIM; ++j)
		//	{
		//		MGSize globnodeid = globtabbfacekey[bfaceid].second.cElem(j);
		//		typename ColGlobKey::const_iterator cit = lower_bound(globtabkey.begin(), globtabkey.end(), GlobKey(globnodeid));
		//		if ((cit != globtabkey.end()) && (cit->first == globnodeid))
		//		{
		//			if (cit->second.procid != ProcessInfo::cProcId())
		//			{
		//				typename vector< KeyData<MGSize, MGSize> >::iterator it = lower_bound(nodemap.begin(), nodemap.end(), KeyData<MGSize, MGSize>(cit->first));
		//				if ((it != nodemap.end()) && (it->first == cit->first))
		//				{
		//					interfkey.second.tabnode[j] = it->second;
		//				}
		//				else
		//				{
		//					THROW_INTERNAL("Error in MPIPeriodicBC<DIM, ET>::SetGlobalInterface: nodeid not found\n");
		//				}
		//			}
		//			else
		//			{
		//				interfkey.second.tabnode[j] = cit->second.locid;
		//			}
		//			interfkey.second.tabcoord[j] = key.second.mtabcoord.cElem(j);
		//		}
		//		else
		//		{
		//			THROW_INTERNAL("Error in MPIPeriodicBC<DIM, ET>::SetGlobalInterface: nodeid not found\n");
		//		}
		//	}
		//	mcollocinterace.push_back(interfkey);
		//}

		//vector< Key<3> > globinterfinput;
		//for (MGSize i = 0; i < nodemap.size(); ++i)
		//{
		//	Key<3> key;
		//	MGSize globnodeid = globtabkey[nodemap[i].first].first;
		//	typename ColGlobKey::const_iterator cit = lower_bound(globtabkey.begin(), globtabkey.end(), GlobKey(globnodeid));
		//	if ((cit != globtabkey.end()) && (cit->first == globnodeid) && (cit->second.procid != ProcessInfo::cProcId()))
		//	{
		//		key.rFirst() = nodemap[i].second;
		//		key.rSecond() = cit->second.procid;
		//		key.rThird() = cit->second.locid;
		//	}
		//	globinterfinput.push_back(key);
		//}

		//SetGlobalInterface(globinterfinput);


		//for (MGSize i = 0; i < mcollocinterace.size(); ++i)
		//{
		//	for (MGSize j = 0; j < DIM; ++j)
		//	{
		//		MGSize nodeid = mcollocinterace[i].second.tabnode[j];

		//		typename vector< KeyData<MGSize, MGSize> >::iterator it = lower_bound(nodemap.begin(), nodemap.end(), KeyData<MGSize, MGSize>(nodeid));
		//		if ((it != nodemap.end()) && (it->first == nodeid))
		//		{
		//			vector<MGFloat> pos;
		//			for (MGSize k = 0; k < DIM; k++)
		//			{
		//				pos.push_back(globtabkey[nodeid].second.pos.cX(k));
		//			}
		//			mpGrid->IOSetNodePos(pos, it->second);
		//			mcollocinterace[i].second.tabnode[j] = it->second;
		//		}
		//	}
		//}

		//MGSize interfsize = mpinterface->Size();
		//mpinterface->IOTabIntfResize(interfsize + nodemap.size());
	}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


#endif // __MPI_PERIODICBC_H__
