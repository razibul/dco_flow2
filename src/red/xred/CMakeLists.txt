LIST ( APPEND xred_files
	main.cpp
) 


INCLUDE_DIRECTORIES (
  ${FLOW2_SOURCE_DIR}/${NAME_CORE}
  ${FLOW2_SOURCE_DIR}/${NAME_AUX}
  ${FLOW2_SOURCE_DIR}/${NAME_RED}
)


LINK_DIRECTORIES (
  ${FLOW2_BINARY_DIR}/${NAME_CORE}/libcoresystem 
  ${FLOW2_BINARY_DIR}/${NAME_CORE}/libcorecommon 
  ${FLOW2_BINARY_DIR}/${NAME_CORE}/libcoreconfig
  ${FLOW2_BINARY_DIR}/${NAME_CORE}/libcoreio
  ${FLOW2_BINARY_DIR}/${NAME_CORE}/libcoregeom
  ${FLOW2_BINARY_DIR}/${NAME_AUX}/libextsparse
  ${FLOW2_BINARY_DIR}/${NAME_RED}/libredcore
  ${FLOW2_BINARY_DIR}/${NAME_RED}/libredconvergence
  ${FLOW2_BINARY_DIR}/${NAME_RED}/libredphysics
  ${FLOW2_BINARY_DIR}/${NAME_RED}/libred
  ${FLOW2_BINARY_DIR}/${NAME_RED}/redvc/libredvccommon
  ${FLOW2_BINARY_DIR}/${NAME_RED}/redvc/libredvcgrid
  ${FLOW2_BINARY_DIR}/${NAME_RED}/redvc/redvcrds/libredvcrdscommon
  ${FLOW2_BINARY_DIR}/${NAME_RED}/redvc/redvcrds/libredvcrdspoisson
  ${FLOW2_BINARY_DIR}/${NAME_RED}/redvc/redvcrds/libredvcrdseuler
  ${FLOW2_BINARY_DIR}/${NAME_RED}/redvc/redvcrds/libredvcrdsns
  ${FLOW2_BINARY_DIR}/${NAME_RED}/redvc/redvcrds/libredvcrdsrans
  ${FLOW2_BINARY_DIR}/${NAME_RED}/redvc/redvcrds/libredvcrdsrfreuler
)

IF ( WITH_LIB_OPENCL )
  LINK_DIRECTORIES ( ${OPENCL_LIB_DIR} )
ENDIF ( WITH_LIB_OPENCL )


ADD_EXECUTABLE ( xred ${xred_files} )


##############################################################
# !!!  the order of the libraries in the list is IMPORTANT !!!
TARGET_LINK_LIBRARIES ( xred 
	red
	redvcrdsrfreuler
	redvccommon 
	redvcrdsrans
	redvcrdsns
	redvcrdseuler
	redvcrdspoisson
	redvcgrid 
	redphysics 
	redconvergence 
	redcore 
	extsparse 
	coreio 
	coregeom 
	coreconfig 
	corecommon 
	coresystem 
) 

IF ( WITH_LIB_OPENCL )
  TARGET_LINK_LIBRARIES ( xred ${OPENCL_LIB} )
ENDIF ( WITH_LIB_OPENCL )

# add the install targets
install (TARGETS xred DESTINATION bin)

