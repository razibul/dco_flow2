#include "libcoresystem/mgdecl.h"

#include "libcorecommon/factory.h"
#include "libcoreconfig/config.h"

#include "libred/setup.h"
#include "libred/master.h"
#include "libcorecommon/logo.h"


INIT_VERSION("0.1.1","'new layout - wip'");


using namespace RED;



/////////////////////////////////
#include "redvc/libredvcgrid/compgeomcell_integrator.h"

using namespace Geom;

//template <Dimension DIM>
//class CellInegrator
//{
//	typedef Vect<DIM>	GVec;
//
//public:
//	CellInegrator( const CGeomCell<DIM>& gcell, const MGSize& order=1 ) : mGCell(gcell), mOrder(order)	{}
//
//
//	template <class TIN, class TOUT, class FUNC>		// TOUT FUNC( GVec pos, TIN u);
//	TOUT	VolumeIntegral( const TIN tabu[DIM+1] )
//	{
//		ASSERT( mGCell.SizeFace() == mGCell.Size() );
//
//		GaussQuadrature<DIM> gquad( mOrder);
//
//		TOUT res;
//		for ( MGSize ipnt=0; ipnt<gquad.Size(); ++ipnt)
//		{
//			Vect<DIM> pos = mGCell.cNode(0).cPos() * gquad.BaryCoordinates(ipnt)[0];
//			TIN u = tabu[0] * gquad.BaryCoordinates(ipnt)[0];
//			for ( MGSize k=1; k<=DIM; ++k)
//			{
//				pos += mGCell.cNode(k).cPos() * gquad.BaryCoordinates(ipnt)[k];
//				u += tabu[k] * gquad.BaryCoordinates(ipnt)[k];
//			}
//
//			FUNC func;
//			TOUT val = func( pos, u);
//			res = ipnt==0 ? val * gquad.Weight(ipnt) : res + val * gquad.Weight(ipnt);
//			//integ += val * gquad.Weight(ipnt);
//		}
//
//		MGFloat vol = mGCell.Volume();
//		res *= vol;
//
//		return res;
//	}
//
//
//	template <class TIN, class TOUT, class FUNC>
//	TOUT	BoundaryIntegral( const TIN tabu[DIM+1] )	// TOUT FUNC( GVec pos, TIN u, GVec vn);
//	{
//		ASSERT( mGCell.SizeFace() == mGCell.Size() );
//
//		Geom::GaussQuadrature< static_cast<Dimension>(DIM-1) > gquad( mOrder);
//
//		TOUT res;
//		for ( MGSize ifac=0; ifac<mGCell.SizeFace(); ++ifac)
//		{
//			GVec vn = mGCell.Vn(ifac);
//			MGFloat area = vn.module();
//			vn *= -1./area;
//
//			TOUT flux; 
//			for ( MGSize ipnt=0; ipnt<gquad.Size(); ++ipnt )
//			{
//				Vect<DIM> pos = mGCell.cNode( mGCell.cFaceConn(ifac,0) ).cPos() * gquad.BaryCoordinates(ipnt)[0];
//				TIN u = tabu[mGCell.cFaceConn(ifac,0)]*gquad.BaryCoordinates(ipnt)[0];
//				for ( MGSize idim=1; idim<DIM; ++idim)
//				{
//					pos += mGCell.cNode( mGCell.cFaceConn(ifac,idim) ).cPos() * gquad.BaryCoordinates(ipnt)[idim];
//					u += tabu[ mGCell.cFaceConn(ifac,idim) ] * gquad.BaryCoordinates(ipnt)[idim];
//				}
//
//				FUNC func;
//				TOUT tmp = func( pos, u, vn) * gquad.Weight(ipnt);
//
//				flux = ipnt == 0 ? tmp : tmp + flux;
//				//flux += func( pos, u, vn) * gquad.Weight(ipnt);
//
//				//for ( MGSize idim=0; idim<DIM; ++idim)
//				//	cout << gauss.BaryCoordinates(ipnt)[idim] << " ";
//				//cout << " -- " << gauss.Weight(ipnt) << endl;
//
//			}
//
//			res = ifac==0 ? area * flux : res + area * flux;
//			//res += flux;
//		}
//
//		return res;
//	}
//
//
//private:
//	const CGeomCell<DIM>& mGCell;
//	MGSize mOrder;
//};




	class Func1
	{
	public:
		MGFloat operator () ( const Vect3D& pos, const MGFloat& u) const
		{	return pos.cX()*pos.cX() + 3.0*pos.cX()*pos.cY() + 2.*pos.cY()*pos.cY() + pos.cZ()*pos.cZ();	}
	};

	class Func2
	{
	public:
		Vect3D operator () ( const Vect3D& pos, const MGFloat& u, const Vect3D& vn) const
		{	return u * vn;	}
	};


void Test_CellInegrator()
{
	CGeomCell<DIM_3D> gcell;

	gcell.rType() = 4;
	gcell.rNode(0).rPos() = Vect3D( 0., 0., 0); 
	gcell.rNode(1).rPos() = Vect3D( 1., 0., 0.); 
	gcell.rNode(2).rPos() = Vect3D( 0., 1., 0.); 
	gcell.rNode(3).rPos() = Vect3D( 0., 0., 1.); 
	gcell.CalcVn();

	MGFloat tabu[] = { 1., 1., 1., 2.};

	CellInegrator<DIM_3D> cint( gcell, 4);
	MGFloat res = cint.VolumeIntegral<MGFloat, MGFloat, Func1>( tabu);
	cout << res << endl;

	Vect3D vres = cint.BoundaryIntegral<MGFloat, Vect3D, Func2>( tabu);
	cout << vres << endl;
}


int main( int argc, char* argv[])
{
    PrintLogo();
    cout << "====  ver.: " << VERSION_NUMBER << "  build: " << VERSION_DATE << " " << VERSION_TIME << "====" << endl;

// 	cout << "test" << endl;
// 	
// 	typedef EquationDef<Geom::DIM_2D,EQN_NS> TEq;
// 
// 	cout << TEq::SIZE << endl;
// 	cout << TEq::ID_RHO << endl;
// 	cout << TEq::ID_P << endl;
// 	cout << TEq::U<0>::ID << endl;
// 	cout << TEq::U<1>::ID << endl;
// 
// 
// 	return 1;


	try
	{
		CheckTypeSizes();
		RegisterLibraries();


		cout << "ver.: " << VERSION_NUMBER << "  build: " << VERSION_DATE << " " << VERSION_TIME << endl << endl;
		///////////////////////////////////////////////////////////////////////

		//Singleton< FactoryList<MGString> >::GetInstance()->PrintInfo();

		/////////////
		
		//Test_CellInegrator();
		//return 0;
		/////////////

		Master	master;
		
		if ( argc == 2)
			master.ReadConfig( argv[1]);
		else
			master.ReadConfig( "solv.cfg");

		master.Create();
		master.Init();
		master.Solve();

	}
	catch ( EHandler::Except& e)
	{
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);
	}
	catch( exception &e) 
	{
		TRACE( e.what() );
		fprintf( stderr, "%s\n", e.what() );
	}
	catch( ...) 
	{
		cout << "unknow problem ::  ..." << endl;
	}

	return 0;
}
