#include "libcoresystem/mgdecl.h"
#include "libcoregeom/predicates.h"
#include "libcorecommon/factory.h"

#include "setup.h"
#include "master.h"

#include "metricoperators.h"

#include "Eigen/Eigen"
#include "libcorecommon/progressbar.h"



INIT_VERSION("0.1","'xestimator - wip'");


void TestEigen()
{
	using namespace Eigen;

	const int SIZE = 10;

	Matrix<double,SIZE,SIZE>  A1 = MatrixXd::Random(SIZE,SIZE);
	Matrix<double,SIZE,SIZE>  M1 = A1 + A1.transpose();
	Matrix<double,SIZE,SIZE>  Aout;

	//cout << M1 << endl;
	
	SMatrix<SIZE> m1, a1, aout;

	for ( int i=0; i<SIZE; ++i)
		for ( int k=0; k<SIZE; ++k)
		{
			a1(i,k) = A1(i,k);
			m1(i,k) = M1(i,k);
		}
	
	int n = 10000;
	ProgressBar	bar(40);

	Matrix<double,SIZE,SIZE>  A;
	bar.Init( n );
	bar.Start();
	for ( int i=0; i<n; ++i)
	{
		A = M1.inverse();
		//A += M1;
		A = (M1 + A) * M1;
		Aout = A;
	}
	bar.Finish();

	cout << Aout << endl << endl;

	SMatrix<SIZE> a;
	bar.Init( n );
	bar.Start();
	for ( int i=0; i<n; ++i)
	{
		a = m1;
		a.Invert();
		a = (m1 + a) * m1;
		//a += m1;
		aout = a;
	}
	bar.Finish();

	aout.Write();
	
}

//void TestEigen()
//{
//	using namespace Eigen;
//
//	MatrixXd A1 = MatrixXd::Random(4,4);
//	MatrixXd M1 = A1 + A1.transpose();
//	MatrixXd A2 = MatrixXd::Random(4,4);
//	MatrixXd M2 = A2 + A2.transpose();
//
//	M1 << 8., 4., 7., 6., 4., 12., 9., 8., 7., 9., 10., 6., 6., 8., 6., 14.;
//	M2 << 16.,3.,6.,6.,3.,8.,4.,5.,6.,4.,4.,2.,6.,5.,2.,14.;
//
//	cout << "matrix M1:" << endl << M1 << endl << endl;
//	cout << "matrix M2:" << endl << M2 << endl << endl;
//
//	MatrixXd A = M1.inverse() * M2;
//	cout << "matrix A:" << endl << A << endl << endl;
//
//	EigenSolver<MatrixXd> es(A);
//
//	MatrixXd P = es.eigenvectors().real();
//	MatrixXd D = es.eigenvalues().real().asDiagonal();
//	cout << "matrix P:" << endl << P << endl << endl;
//	cout << "matrix D:" << endl << D << endl << endl;
//	cout << "P * D * P^(-1) = " << endl << P * D * P.inverse() << endl;	
//	cout << endl;
//	MatrixXd D1 = P.transpose() * M1 * P;
//	MatrixXd D2 = P.transpose() * M2 * P;
//	cout << "PT * M1 * P = " << endl << D1 << endl;	
//	cout << "PT * M2 * P = " << endl << D2 << endl;	
//
//	for ( int i=0; i<D.rows(); ++i)
//		D(i,i) = max( D1(i,i), D2(i,i) ); 
//
//	MatrixXd Pi = P.inverse();
//	cout << endl << "Finally, P * D * P^(-1) = " << endl << Pi.transpose() * D * Pi << endl;	
//}

int main( int argc, char* argv[])
{
	//TestEigen();
	//return 1;

	//SMatrix<DIM_2D> m1, m2, mred;

	//m1(0,0) = 5.59874608435016;
	//m1(1,0) = m1(0,1) = 1.49749999997269;
	//m1(1,1) = 0.411253915649840;

	//m2(0,0) = 0.5;
	//m2(1,0) = m2(0,1) = 0.;
	//m2(1,1) = 0.02;

	//mred = MetricIntersection<DIM_2D>()( m1, m2);
	//m1.Write();
	//m2.Write();
	//mred.Write();

	//return 0;

	try
	{
		CheckTypeSizes();
		RegisterLibraries();


		cout << "ver.: " << VERSION_NUMBER << "  build: " << VERSION_DATE << " " << VERSION_TIME << endl << endl;
		///////////////////////////////////////////////////////////////////////
	
		exactinit();	// initialization of geom predicates
		///////////////////////////////////////////////////////////////////////
		

		Singleton< FactoryList<MGString> >::GetInstance()->PrintInfo();

		Master master;

		if ( argc == 2)
			master.ReadConfig( argv[1]);
		else
			master.ReadConfig( "estimator.cfg");

		master.Create();
		master.Init();
		master.Execute();

		return 0;
	}
	catch ( EHandler::Except& e)
	{
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);
	}
	catch( exception &e) 
	{
		TRACE( e.what() );
		cout << endl << "System exception: " << e.what() << endl;
	}
	catch( ...) 
	{
		TRACE( "Unknown exception thrown" );
		cout << endl << "Unknown exception thrown" << endl;
	}

	return 0;
} 

