#include "estcspace.h"
#include "configconst.h"
#include "gradient.h"
#include "smoother.h"
#include "metricoperators.h"


template <Dimension DIM> 
void EstCSpace<DIM>::InitMetric( const Space<DIM,EMatrix>& metric)
{
	if ( mpGrid->SizeNodeTab() != metric.Size() )
		THROW_INTERNAL( "EstCSpace<DIM>::InitMetric() -- incompatible grid and init metric sizes" );

	mMetric.rTab() = metric.cTab();
}


template <Dimension DIM> 
void EstCSpace<DIM>::AddMetric( const Space<DIM,EMatrix>& metric)
{
	if ( mpGrid->SizeNodeTab() != metric.Size() )
		THROW_INTERNAL( "EstCSpace<DIM>::InitMetric() -- incompatible grid and init metric sizes" );

	if ( mMetric.Size() != metric.Size() )
		THROW_INTERNAL( "EstCSpace<DIM>::InitMetric() -- incompatible metric sizes" );

	//mMetric.rTab() = metric.cTab();

	for ( MGSize i=0; i<mMetric.Size(); ++ i)
	{
		MetricIntersection<DIM> inter;

		mMetric.rTab()[i] = inter( mMetric.cTab()[i], metric.cTab()[i] );
	}

}


template <Dimension DIM> 
void EstCSpace<DIM>::Limit()
{
	MGFloat hmin = ConfigSect().ValueFloat( ConfigStr::Estimator::CSpace::H_MIN );
	MGFloat hmax = ConfigSect().ValueFloat( ConfigStr::Estimator::CSpace::H_MAX );
	MGFloat armax = ConfigSect().ValueFloat( ConfigStr::Estimator::CSpace::AR_MAX );

	MGFloat coeff = ConfigSect().ValueFloat( ConfigStr::Estimator::CSpace::SCALE );
	coeff = pow( coeff, 1. / static_cast<MGFloat>(DIM) );


	MetricSpcNormalize<DIM> norm( mpGrid);
	norm.Execute( mMetric);

	for ( MGSize i=0; i<mMetric.Size(); ++ i)
		mMetric.rTab()[i] *= coeff;

	//hack: works only for long_shot nozzle
	MGFloat xmax = 5e-3;
	MGFloat xmin = 5e-3;
	MGFloat xmid = 1.;

	//MGFloat a = (hmax - hmin) / (xmin * xmin - 2 * xmid*xmin + xmid * xmid);
	//MGFloat b = -(2 * hmax - 2 * hmin)*xmid / (xmin * xmin - 2 * xmid*xmin + xmid * xmid);
	//MGFloat c = (hmin*xmin * xmin - 2 * hmin*xmid*xmin + hmax*xmid * xmid)
	//	/ (xmin * xmin - 2 * xmid*xmin + xmid * xmid);
	MGFloat locHmax = hmax;

	MGSize ind = 0;
	for (typename EstGrid<DIM>::ColNode::const_iterator citr = mpGrid->cColNode().begin(); citr != mpGrid->cColNode().end(); ++citr)
	{
		//const MGFloat& xpos = citr->cPos().cX();
		//if (xpos < 0.01)
		//{
		//	locHmax = 0.001;
		//}
		//else
		//{
		//	locHmax = hmax;
		//}

		MetricLimit<DIM> limiter(hmin, locHmax, armax);
		mMetric.rTab()[ind] = limiter(mMetric.cTab()[ind]);

		++ind;
	}

}

template <Dimension DIM> 
void EstCSpace<DIM>::Smooth()
{
	MGSize niter	= ConfigSect().ValueSize( ConfigStr::Estimator::CSpace::SMTH_NITER );
	MGFloat wgh		= ConfigSect().ValueFloat( ConfigStr::Estimator::CSpace::SMTH_WEIGHT );

	for ( MGSize i=0; i<mMetric.Size(); ++i)
	{
		EMatrix mtx;
		DecomposeROOT( mtx, mMetric.cTab()[i] );
		mMetric.rTab()[i] = mtx;
	}

	for ( MGSize iter=0; iter<niter; ++iter)
	{
		Smoother< DIM, EMatrix > smooth( *mpGrid, mMetric);
		smooth.Calculate();

		for ( MGSize i=0; i<mMetric.Size(); ++i)
			mMetric.rTab()[i] = (1. - wgh) * mMetric.cTab()[i]  +  wgh * smooth.cSSpace().cTab()[i];
	}

	for ( MGSize i=0; i<mMetric.Size(); ++i)
	{
		EMatrix mtx;
		DecomposeSQR( mtx, mMetric.cTab()[i] );
		mMetric.rTab()[i] = mtx;
	}

}


template <Dimension DIM> 
void EstCSpace<DIM>::ExportTEC( const MGString& fname ) const
{
	IO::WriteTEC	writetec( *mpGrid, &mMetric);
	writetec.DoWrite( fname );
}


template <Dimension DIM> 
void EstCSpace<DIM>::ExportCSPACE() const
{
	MGString ftype = ConfigSect().ValueString( ConfigStr::Estimator::CSpace::FType::KEY );
	MGString fname = ConfigSect().ValueString( ConfigStr::Estimator::CSpace::FNAME );

	if ( ftype == MGString( ConfigStr::Estimator::CSpace::FType::Value::TECPLOT ) )
		ExportTEC( fname);
	else
	if ( ftype == MGString( ConfigStr::Estimator::CSpace::FType::Value::MEANDROS ) )
		ExportMEANDROS( fname);
	else
		THROW_INTERNAL( "ExportCSPACE -- undefined filetype '"<<ftype<<"'");

}



template <Dimension DIM> 
void EstCSpace<DIM>::ExportMEANDROS( const MGString& fname ) const
{
	ofstream file;


	try
	{
		MGString fnameNode = fname + MGString(".node");

		cout << "Writing MEANDR '" << fnameNode << "'" << endl;//std::flush;

		if ( mMetric.Size() != mpGrid->SizeNodeTab() )
			THROW_INTERNAL( "mMetric.Size() != mpGrid->SizeNodeTab()");


		/////////////////////////////////////////////////////////////
		// write node file

		file.open( fnameNode.c_str(), ios::out );

		if ( ! file)
			THROW_FILE( "can not open the file", fnameNode);

		file << mpGrid->SizeNodeTab() << endl;


		MGSize ind=1;
		for ( typename EstGrid<DIM>::ColNode::const_iterator citr=mpGrid->cColNode().begin(); citr!=mpGrid->cColNode().end(); ++citr)
		{
			file << ind;
			for ( MGSize idim=0; idim<DIM; ++idim)
				file << " " << setprecision(17) << citr->cPos().cX(idim);
			file << endl;
			++ind;
		}

		file.close();

		/////////////////////////////////////////////////////////////
		// write metric file
		MGString fnameMetric = fname + MGString(".metric");

		cout << "Writing MEANDR '" << fnameMetric << "'" << endl;//std::flush;

		file.open( fnameMetric.c_str(), ios::out );

		if ( ! file)
			THROW_FILE( "can not open the file", fnameNode);

		file << mMetric.Size() << endl;

		for ( MGSize i=0; i<mMetric.Size(); ++i)
		{
			file  << setprecision(17) << i+1;

			if ( DIM == DIM_2D)
			{
				file << " " << mMetric.cTab()[i](0,0);
				file << " " << mMetric.cTab()[i](0,1);
				file << " " << mMetric.cTab()[i](1,1);
			}
			else
			if ( DIM == DIM_3D)
			{
				file << " " << mMetric.cTab()[i](0,0);
				file << " " << mMetric.cTab()[i](0,1);
				file << " " << mMetric.cTab()[i](0,2);
				file << " " << mMetric.cTab()[i](1,1);
				file << " " << mMetric.cTab()[i](1,2);
				file << " " << mMetric.cTab()[i](2,2);
			}
			else
				THROW_INTERNAL( "unknown DIM");

			file << endl;

		}

		file.close();
	}
	catch ( EHandler::Except& e)
	{
		cout << "ERROR: ReadMEANDR<DIM>::DoRead - file name: " << fname << endl << endl;
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);

		THROW_INTERNAL("END");
	}

}



template <Dimension DIM> 
void EstCSpace<DIM>::ExportBndMEANDROS( const MGString& fname ) const
{
	ofstream file;


	try
	{
		MGString fnameNode = fname + MGString(".node");

		cout << "Writing MEANDR '" << fnameNode << "'" << endl;//std::flush;

		if ( mMetric.Size() != mpGrid->SizeNodeTab() )
			THROW_INTERNAL( "mMetric.Size() != mpGrid->SizeNodeTab()");


		/////////////////////////////////////////////////////////////
		// write node file

		file.open( fnameNode.c_str(), ios::out );

		if ( ! file)
			THROW_FILE( "can not open the file", fnameNode);

		//file << mpGrid->SizeNodeTab() << endl;
		file << mpGrid->cTabBNodes().size() << endl;

		MGSize ind;
		ind=1;
		for ( MGSize i=0; i<mpGrid->cTabBNodes().size(); ++i)
		{
			file << ind;
			for ( MGSize idim=0; idim<DIM; ++idim)
				file << " " << setprecision(17) << mpGrid->cNode( mpGrid->cTabBNodes()[i] ).cPos().cX(idim);
			file << endl;
			++ind;
		}
		file.close();

		//MGSize ind=1;
		//for ( typename EstGrid<DIM>::ColNode::const_iterator citr=mpGrid->cColNode().begin(); citr!=mpGrid->cColNode().end(); ++citr)
		//{
		//	file << ind;
		//	for ( MGSize idim=0; idim<DIM; ++idim)
		//		file << " " << setprecision(17) << citr->cPos().cX(idim);
		//	file << endl;
		//	++ind;
		//}
		//file.close();

		/////////////////////////////////////////////////////////////
		// write metric file
		MGString fnameMetric = fname + MGString(".metric");

		cout << "Writing MEANDR '" << fnameMetric << "'" << endl;//std::flush;

		file.open( fnameMetric.c_str(), ios::out );

		if ( ! file)
			THROW_FILE( "can not open the file", fnameNode);

		//file << mMetric.Size() << endl;
		file << mpGrid->cTabBNodes().size() << endl;

		ind=1;
		for ( MGSize j=0; j<mpGrid->cTabBNodes().size(); ++j)
		{
			MGSize i = mpGrid->cTabBNodes()[j] - 1;

			file  << setprecision(17) << ind;

			if ( DIM == DIM_2D)
			{
				file << " " << mMetric.cTab()[i](0,0);
				file << " " << mMetric.cTab()[i](0,1);
				file << " " << mMetric.cTab()[i](1,1);
			}
			else
			if ( DIM == DIM_3D)
			{
				file << " " << mMetric.cTab()[i](0,0);
				file << " " << mMetric.cTab()[i](0,1);
				file << " " << mMetric.cTab()[i](0,2);
				file << " " << mMetric.cTab()[i](1,1);
				file << " " << mMetric.cTab()[i](1,2);
				file << " " << mMetric.cTab()[i](2,2);
			}
			else
				THROW_INTERNAL( "unknown DIM");

			file << endl;
			++ind;
		}

		file.close();

		//for ( MGSize i=0; i<mMetric.Size(); ++i)
		//{
		//	file  << setprecision(17) << i+1;

		//	if ( DIM == DIM_2D)
		//	{
		//		file << " " << mMetric.cTab()[i](0,0);
		//		file << " " << mMetric.cTab()[i](0,1);
		//		file << " " << mMetric.cTab()[i](1,1);
		//	}
		//	else
		//	if ( DIM == DIM_3D)
		//	{
		//		file << " " << mMetric.cTab()[i](0,0);
		//		file << " " << mMetric.cTab()[i](0,1);
		//		file << " " << mMetric.cTab()[i](0,2);
		//		file << " " << mMetric.cTab()[i](1,1);
		//		file << " " << mMetric.cTab()[i](1,2);
		//		file << " " << mMetric.cTab()[i](2,2);
		//	}
		//	else
		//		THROW_INTERNAL( "unknown DIM");

		//	file << endl;

		//}

		//file.close();
	}
	catch ( EHandler::Except& e)
	{
		cout << "ERROR: ReadMEANDR<DIM>::DoRead - file name: " << fname << endl << endl;
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);

		THROW_INTERNAL("END");
	}

}


template class EstCSpace<DIM_2D>;
template class EstCSpace<DIM_3D>;




