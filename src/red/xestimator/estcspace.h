#ifndef __ESTCSPACE_H__
#define __ESTCSPACE_H__

#include "estgrid.h"
#include "funcspace.h"

#include "libcoreconfig/configbase.h"
#include "libgreen/gridgeom.h" 
#include "libcoreio/writetec.h"
#include "libgreen/gridgeom.h" 


//////////////////////////////////////////////////////////////////////
//	class EstCSpace
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class EstCSpace : public ConfigBase
{
public:
	typedef SVector<DIM,MGFloat>	EVector;
	typedef SMatrix<DIM,MGFloat>	EMatrix;


	EstCSpace() : mpGrid(NULL)	{}
	virtual ~EstCSpace()			{}

	virtual void	Create( const CfgSection* pcfgsec, const EstGrid<DIM>* pgrid);
	virtual void	PostCreateCheck() const;
	virtual void	Init()						{}

	void	InitMetric( const Space<DIM,EMatrix>& metric);
	void	AddMetric( const Space<DIM,EMatrix>& metric);

	void	Limit();
	void	Smooth();

	void	ExportCSPACE() const;

	void	ExportTEC( const MGString& fname ) const;
	void	ExportMEANDROS( const MGString& fname ) const;

	void	ExportBndMEANDROS( const MGString& fname ) const;

private:
	const EstGrid<DIM>*		mpGrid;
	Space<DIM,EMatrix>		mMetric;
};



template <Dimension DIM> 
void EstCSpace<DIM>::Create( const CfgSection* pcfgsec, const EstGrid<DIM>* pgrid)	
{ 
	ConfigBase::Create( pcfgsec); 
	mpGrid = pgrid; 
}

template <Dimension DIM> 
void EstCSpace<DIM>::PostCreateCheck() const
{ 
	ConfigBase::PostCreateCheck(); 

	if ( ! mpGrid )
		THROW_INTERNAL( "EstCSpace<DIM>::PostCreateCheck() -- failed : 'mpGrid == NULL'" );
}




#endif // __ESTCSPACE_H__

