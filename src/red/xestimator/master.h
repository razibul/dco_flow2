#ifndef __EST_MASTER_H__
#define __EST_MASTER_H__

#include "libcoresystem/mgdecl.h"
#include "libcoreconfig/config.h" 

#include "estimatorbase.h" 


//////////////////////////////////////////////////////////////////////
// Master
//////////////////////////////////////////////////////////////////////
class Master
{
public:
	Master() : mpEstimator(NULL)	{}
	~Master();

	void	ReadConfig( const MGString& fname);
	
	void	Create();
	void	Init();

	void	Execute();

protected:

protected:
	Config			mConfig;
	EstimatorBase*	mpEstimator;
};
//////////////////////////////////////////////////////////////////////




#endif // __EST_MASTER_H__
