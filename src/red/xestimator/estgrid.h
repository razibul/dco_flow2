#ifndef __ESTGRID_H__
#define __ESTGRID_H__

#include "libcoresystem/mgdecl.h"
#include "libcoreio/gridfacade.h"
#include "libgreen/grid.h"



//////////////////////////////////////////////////////////////////////
//	class EstGrid
//////////////////////////////////////////////////////////////////////
template <Dimension DIM> 
class EstGrid : public GREEN::Grid<DIM>, public IO::GridWriteFacade, public IO::GridReadFacade
{
	typedef GREEN::Grid<DIM> TGrid;

public:

	void	PostReadInitialization();
	void	FindBoundary();


	const vector< Key<DIM> >&	cTabBFaces() const		{ return mtabBFace; }
	const vector<MGSize>&		cTabBNodes() const		{ return mtabBNode; }



public:
	virtual Dimension	Dim() const						{ return DIM; }
	virtual void	IOSetName( const MGString& name)	{ mGridName = name; }
	virtual void	IOGetName( MGString& name) const	{ name = mGridName; }

	virtual void	IOTabNodeResize( const MGSize& n)													{ TGrid::rColNode().resize( n); }
	virtual void	IOTabCellResize( const MGSize& type, const MGSize& n)								{ TGrid::rColCell().resize( n); }

	virtual void	IOSetNodePos( const vector<MGFloat>& tabx, const MGSize& id )						
		{
			typename TGrid::GVect vpos;
			for ( MGSize i=0; i<DIM; ++i)
				vpos.rX(i) = tabx[i];

			typename TGrid::GNode &node = this->rNode(id+1);
			node.rId() = id+1;
			node.rMasterId() = 0;
			node.rPos() = vpos;
		}
		//{ TGrid::IOSetNodePos( tabx, id+1 ); }
	virtual void	IOSetCellIds( const vector<MGSize>& tabid, const MGSize& type, const MGSize& id )	
		{ 
			ASSERT(type==DIM+1); 
			//TGrid::IOSetCellIds( tabid, id+1 ); 
			typename TGrid::GCell &cell = this->rCell(id+1);
			cell.rId() = id+1;
			for ( MGSize i=0; i<=DIM; ++i)
				cell.rNodeId(i) = tabid[i]+1;
		}

	virtual void	IOTabBFaceResize( const MGSize& type, const MGSize& n)								{ THROW_INTERNAL("should not be used..."); }
	virtual void	IOSetBFaceIds( const vector<MGSize>& tabid, const MGSize& type, const MGSize& id )	{ THROW_INTERNAL("should not be used..."); }
	virtual void	IOSetBFaceSurf( const MGSize& idsurf, const MGSize& type, const MGSize& id )		{ THROW_INTERNAL("should not be used..."); }


	virtual MGSize	IOSizeNodeTab() const							{ return TGrid::SizeNodeTab(); }
	virtual MGSize	IOSizeCellTab( const MGSize& type) const		{ return TGrid::SizeCellTab(); }
	virtual MGSize	IOSizeBFaceTab( const MGSize& type) const		{ return 0; }

	virtual void	IOGetNodePos( vector<MGFloat>& tabx, const MGSize& id) const
		{
			ASSERT( tabx.size() == DIM);
			for ( MGSize i=0; i<DIM; ++i)
				tabx[i] = TGrid::cNode(id+1).cPos().cX(i);
		}

	virtual void	IOGetCellIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const
		{
			ASSERT( type == DIM+1);
			ASSERT( tabid.size() == type);
			for ( MGSize k=0; k<DIM+1; ++k)
				tabid[k] = TGrid::cCell(id+1).cNodeId(k)-1;
		}

	virtual void	IOGetBFaceIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const	{ THROW_INTERNAL("should not be used..."); }
	virtual void	IOGetBFaceSurf( MGSize& idsurf, const MGSize& type, const MGSize& id) const			{ THROW_INTERNAL("should not be used..."); }

	virtual MGSize	IONumCellTypes() const						{ return 1; }
	virtual MGSize	IONumBFaceTypes() const						{ return 1; }
	virtual MGSize	IOCellType( const MGSize& i) const			{ return DIM+1; }
	virtual MGSize	IOBFaceType( const MGSize& i) const			{ return DIM; }


private:
	MGString	mGridName;

	vector< Key<DIM> >	mtabBFace;	// NOT oriented !!!
	vector<MGSize>		mtabBNode;

};





#endif // __ESTGRID_H__
