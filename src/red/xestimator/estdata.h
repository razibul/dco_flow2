#ifndef __ESTDATA_H__
#define __ESTDATA_H__

#include "estgrid.h"

#include "solution.h"
#include "configconst.h"

#include "libcoreconfig/configbase.h"
#include "libcoreio/readtec.h"
#include "libcoreio/writetec.h"


//////////////////////////////////////////////////////////////////////
//	class EstimatorData
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, MGSize ESIZE> 
class EstimatorData : public ConfigBase
{
public:
	typedef SVector<DIM,MGFloat>	EVector;
	typedef SMatrix<DIM,MGFloat>	EMatrix;


	EstimatorData()				{}
	virtual ~EstimatorData()	{}

	virtual void	Create( const CfgSection* pcfgsec)		{ ConfigBase::Create( pcfgsec); }
	virtual void	PostCreateCheck() const					{ ConfigBase::PostCreateCheck(); }
	virtual void	Init();


	const EstGrid<DIM>&			cGrid()		{ return mEGrid; }
	const Solution<DIM,ESIZE>&	cSol()		{ return mSol; }

private:
	EstGrid<DIM>		mEGrid;		// uses indexing 1,...,n
	Solution<DIM,ESIZE>	mSol;		// uses indexing 0,...,n-1
};



template <Dimension DIM, MGSize ESIZE> 
void EstimatorData<DIM,ESIZE>::Init()
{
	MGString fname = ConfigSect().ValueString( ConfigStr::Estimator::Data::FNAME );


	IO::ReadTEC	readtec( mEGrid, &mSol);
	readtec.DoRead( fname);

	mEGrid.PostReadInitialization();

	//IO::WriteTEC	writetec( mEGrid, &mSol);
	//writetec.DoWrite( "_est_initout.dat" );
}


#endif // __ESTDATA_H__
