#include "setup.h"

void init_Estimator();
void init_ExFunc();
void init_MetricBuildFunc();

void RegisterLibraries()
{
	init_Estimator();
	init_ExFunc();
	init_MetricBuildFunc();
}
