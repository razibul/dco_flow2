#ifndef __METRICGRADBUILDER_H__
#define __METRICGRADBUILDER_H__

#include "metricbuilder.h"
#include "metricoperators.h"


//////////////////////////////////////////////////////////////////////
//	class MetricGradBuilder
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class MetricGradBuilder : public MetricBuilder<DIM>
{
public:
	typedef SVector<DIM,MGFloat>	EVector;
	typedef SMatrix<DIM,MGFloat>	EMatrix;

	MetricGradBuilder()				{}
	virtual ~MetricGradBuilder()	{}

	virtual void	Create( const CfgSection* pcfgsec, Space<DIM,MGFloat>* pestFunc, Space<DIM,EVector>* pestGrad, Space<DIM,EMatrix>* pestHess, Space<DIM,EMatrix>* pmspace )
		{ 
			MetricBuilder<DIM>::Create( pcfgsec, pestFunc, pestGrad, pestHess, pmspace); 
		}

	virtual void	PostCreateCheck() const;
	virtual void	Init()						{}
	virtual void	Calculate();
};


template <Dimension DIM> 
void MetricGradBuilder<DIM>::PostCreateCheck() const
{ 
	MetricBuilder<DIM>::PostCreateCheck(); 
}


template <Dimension DIM> 
void MetricGradBuilder<DIM>::Calculate()
{
	//if ( this->mpEstimate->cGradSpc().Size() != this->mpEstimate->cGrid().SizeNodeTab() )
	//	THROW_INTERNAL( "incompatible grid and hspace sizes !!!");

	const MGFloat psqrt_coeff = this->ConfigSect().ValueFloat( ConfigStr::Estimator::Estimate::Metric::GRAD_PSQRT_COEFF );
	const MGFloat iso_coeff = this->ConfigSect().ValueFloat( ConfigStr::Estimator::Estimate::Metric::GRAD_ISO_COEFF );

	this->mpMSpace->rTab().resize( this->mpestGrad->Size() );

	for ( MGSize k=0; k<this->mpestGrad->Size(); ++k)
	{
		MGFloat val = this->mpestFunc->cTab()[k];
		const EVector&	vec = this->mpestGrad->cTab()[k];

		Geom::Vect<DIM,MGFloat> grad;
		for ( MGSize i=0; i<DIM; ++i)
			grad.rX(i) = vec(i);

		MGFloat dgrad = sqrt( grad * grad);

		EMatrix	mtxLI, mtxL;
		EVector vecD;

		// gradient metric
		EMatrix			mtxG;

		for ( MGSize ir=0; ir<DIM; ++ir)
		{
			mtxG(ir,ir) = vec[ir] * vec[ir];

			for ( MGSize ic=ir+1; ic<DIM; ++ic)
				mtxG(ic,ir) = mtxG(ir,ic) = vec[ir] * vec[ic];
		}

		mtxG *= 1. / max( pow( dgrad, psqrt_coeff ), 1.0e-20 );
		//mtxG *= 1. / (1. + fabs(val) );

		for ( MGSize ir=0; ir<DIM; ++ir)
			mtxG(ir,ir) += iso_coeff;


		this->mpMSpace->rTab()[k] = mtxG;
	}

}




#endif // __METRICGRADBUILDER_H__
