#ifndef __EXFUNC_H__
#define __EXFUNC_H__

#include "exfuncbase.h"



//////////////////////////////////////////////////////////////////////
// ExFuncPoisson
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, RED::EQN_TYPE ET>
class ExFuncPoisson : public ExFuncBase<DIM,ET>
{
public:
	static MGString	Info()	{ return "ExFuncPoisson"; }

	ExFuncPoisson()			{}
	ExFuncPoisson( const CfgSection& cfgsec) : ExFuncBase<DIM,ET>( cfgsec )	{}
	virtual ~ExFuncPoisson()	{}

	virtual void	Create( const CfgSection* pcfgsec)		{ ExFuncBase<DIM,ET>::Create( pcfgsec); }
	virtual void	PostCreateCheck() const					{ ExFuncBase<DIM,ET>::PostCreateCheck(); }
	virtual void	Init()									{}

	virtual MGFloat	Calculate( const MGFloat tab[])
	{
		return tab[0]*tab[0];
		//return 70. / tab[0];
	}
};


//////////////////////////////////////////////////////////////////////
// ExFuncDensity
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, RED::EQN_TYPE ET>
class ExFuncDensity : public ExFuncBase<DIM,ET>
{
public:
	static MGString	Info()	{ return "ExFuncDensity"; }

	ExFuncDensity()			{}
	ExFuncDensity( const CfgSection& cfgsec) : ExFuncBase<DIM,ET>( cfgsec )	{}
	virtual ~ExFuncDensity()	{}

	virtual void	Create( const CfgSection* pcfgsec)		{ ExFuncBase<DIM,ET>::Create( pcfgsec); }
	virtual void	PostCreateCheck() const					{ ExFuncBase<DIM,ET>::PostCreateCheck(); }
	virtual void	Init()									{}

	virtual MGFloat	Calculate( const MGFloat tab[])
	{
		typedef RED::EquationDef<DIM,ET> TDef;

		return tab[ TDef::ID_RHO ];
	}
};


//////////////////////////////////////////////////////////////////////
// ExFuncEnergy
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, RED::EQN_TYPE ET>
class ExFuncEnergy : public ExFuncBase<DIM,ET>
{
public:
	static MGString	Info()	{ return "ExFuncEnergy"; }

	ExFuncEnergy()			{}
	ExFuncEnergy( const CfgSection& cfgsec) : ExFuncBase<DIM,ET>( cfgsec )	{}
	virtual ~ExFuncEnergy()	{}

	virtual void	Create( const CfgSection* pcfgsec)		{ ExFuncBase<DIM,ET>::Create( pcfgsec); }
	virtual void	PostCreateCheck() const					{ ExFuncBase<DIM,ET>::PostCreateCheck(); }
	virtual void	Init()									{}

	virtual MGFloat	Calculate( const MGFloat tab[])
	{
		typedef RED::EquationDef<DIM,ET> TDef;

		return tab[ TDef::ID_P ];
	}
};


//////////////////////////////////////////////////////////////////////
// ExFuncVelocity
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, RED::EQN_TYPE ET>
class ExFuncVelocity : public ExFuncBase<DIM,ET>
{
public:
	static MGString	Info()	{ return "ExFuncVelocity"; }

	ExFuncVelocity()			{}
	ExFuncVelocity( const CfgSection& cfgsec) : ExFuncBase<DIM,ET>( cfgsec )	{}
	virtual ~ExFuncVelocity()	{}

	virtual void	Create( const CfgSection* pcfgsec)		{ ExFuncBase<DIM,ET>::Create( pcfgsec); }
	virtual void	PostCreateCheck() const					{ ExFuncBase<DIM,ET>::PostCreateCheck(); }
	virtual void	Init()									{}

	virtual MGFloat	Calculate( const MGFloat tab[])
	{
		typedef RED::EquationDef<DIM,ET> TDef;

		MGFloat u2 = 0.0;
		for ( MGSize i=0; i<DIM; ++i)
			u2 += tab[ TDef::template U<0>::ID + i ] * tab[ TDef::template U<0>::ID + i ];

		return sqrt( u2) / tab[ TDef::ID_RHO ];
	}
};



//////////////////////////////////////////////////////////////////////
// ExFuncVelocitySqr
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, RED::EQN_TYPE ET>
class ExFuncVelocitySqr : public ExFuncBase<DIM,ET>
{
public:
	static MGString	Info()	{ return "ExFuncVelocitySqr"; }

	ExFuncVelocitySqr()			{}
	ExFuncVelocitySqr( const CfgSection& cfgsec) : ExFuncBase<DIM,ET>( cfgsec )	{}
	virtual ~ExFuncVelocitySqr()	{}

	virtual void	Create( const CfgSection* pcfgsec)		{ ExFuncBase<DIM,ET>::Create( pcfgsec); }
	virtual void	PostCreateCheck() const					{ ExFuncBase<DIM,ET>::PostCreateCheck(); }
	virtual void	Init()									{}

	virtual MGFloat	Calculate( const MGFloat tab[])
	{
		typedef RED::EquationDef<DIM,ET> TDef;

		MGFloat u2 = 0.0;
		for ( MGSize i=0; i<DIM; ++i)
			u2 += tab[ TDef::template U<0>::ID + i ] * tab[ TDef::template U<0>::ID + i ];

		return 0.5 * u2 / ( tab[ TDef::ID_RHO ] * tab[ TDef::ID_RHO ] );
	}
};


//////////////////////////////////////////////////////////////////////
// ExFuncPressure
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, RED::EQN_TYPE ET>
class ExFuncPressure : public ExFuncBase<DIM,ET>
{
public:
	static MGString	Info()	{ return "ExFuncPressure"; }

	ExFuncPressure()			{}
	ExFuncPressure( const CfgSection& cfgsec) : ExFuncBase<DIM,ET>( cfgsec )	{}
	virtual ~ExFuncPressure()	{}

	virtual void	Create( const CfgSection* pcfgsec)		{ ExFuncBase<DIM,ET>::Create( pcfgsec); }
	virtual void	PostCreateCheck() const					{ ExFuncBase<DIM,ET>::PostCreateCheck(); }
	virtual void	Init()									{}

	virtual MGFloat	Calculate( const MGFloat tab[])
	{
		typedef RED::EquationDef<DIM,ET> TDef;

		const MGFloat FLOW_K = 1.4;

		MGFloat u2 = 0.0;
		for ( MGSize i=0; i<DIM; ++i)
			u2 += tab[ TDef::template U<0>::ID + i ] * tab[ TDef::template U<0>::ID + i ];

		MGFloat p = (FLOW_K - 1)*( tab[ TDef::ID_P] - 0.5*u2/tab[ TDef::ID_RHO] );
		return p;
	}
};




//////////////////////////////////////////////////////////////////////
// ExFuncMach
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, RED::EQN_TYPE ET>
class ExFuncMach : public ExFuncBase<DIM,ET>
{
public:
	static MGString	Info()	{ return "ExFuncMach"; }

	ExFuncMach()			{}
	ExFuncMach( const CfgSection& cfgsec) : ExFuncBase<DIM,ET>( cfgsec )	{}
	virtual ~ExFuncMach()	{}

	virtual void	Create( const CfgSection* pcfgsec)		{ ExFuncBase<DIM,ET>::Create( pcfgsec); }
	virtual void	PostCreateCheck() const					{ ExFuncBase<DIM,ET>::PostCreateCheck(); }
	virtual void	Init()									{}

	virtual MGFloat	Calculate( const MGFloat tab[])
	{
		typedef RED::EquationDef<DIM,ET> TDef;

		const MGFloat FLOW_K = 1.4;

		MGFloat u2 = 0.0;
		for ( MGSize i=0; i<DIM; ++i)
			u2 += tab[ TDef::template U<0>::ID + i ] * tab[ TDef::template U<0>::ID + i ];

		MGFloat p = (FLOW_K - 1)*( tab[ TDef::ID_P] - 0.5*u2/tab[ TDef::ID_RHO] );
		MGFloat v = ::sqrt( u2)/tab[ TDef::ID_RHO];
		MGFloat c = ::sqrt( FLOW_K*p/tab[ TDef::ID_RHO] );
		//return log(p)-1.4*log(tab[0]);
		return v / c;
	}
};

//////////////////////////////////////////////////////////////////////
// ExFuncEntropy
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, RED::EQN_TYPE ET>
class ExFuncEntropy : public ExFuncBase<DIM, ET>
{
public:
	static MGString	Info()	{ return "ExFuncEntropy"; }

	ExFuncEntropy()			{}
	ExFuncEntropy(const CfgSection& cfgsec) : ExFuncBase<DIM, ET>(cfgsec)	{}
	virtual ~ExFuncEntropy()	{}

	virtual void	Create(const CfgSection* pcfgsec)		{ ExFuncBase<DIM, ET>::Create(pcfgsec); }
	virtual void	PostCreateCheck() const					{ ExFuncBase<DIM, ET>::PostCreateCheck(); }
	virtual void	Init()									{}

	virtual MGFloat	Calculate(const MGFloat tab[])
	{
		typedef RED::EquationDef<DIM, ET> TDef;

		const MGFloat FLOW_K = 1.4;

		MGFloat u2 = 0.0;
		for (MGSize i = 0; i<DIM; ++i)
			u2 += tab[TDef::template U<0>::ID + i] * tab[TDef::template U<0>::ID + i];

		MGFloat p = (FLOW_K - 1)*(tab[TDef::ID_P] - 0.5*u2 / tab[TDef::ID_RHO]);
		MGFloat v = ::sqrt(u2) / tab[TDef::ID_RHO];
		MGFloat c = ::sqrt(FLOW_K*p / tab[TDef::ID_RHO]);

		//MGFloat mi = asin(c / v);
		//MGFloat theta = atan(tab[TDef::template U<0>::ID + 1] / tab[TDef::template U<0>::ID]);
		//return mi + theta;
		return log(p)-1.4*log(tab[0]);
		//return v / c;
	}
};

//////////////////////////////////////////////////////////////////////
// ExFuncTurbViscosity
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, RED::EQN_TYPE ET>
class ExFuncTurbViscosity : public ExFuncBase<DIM,ET>
{
public:
	static MGString	Info()	{ return "ExFuncTurbViscosity"; }

	ExFuncTurbViscosity()			{}
	ExFuncTurbViscosity( const CfgSection& cfgsec) : ExFuncBase<DIM,ET>( cfgsec )	{}
	virtual ~ExFuncTurbViscosity()	{}

	virtual void	Create( const CfgSection* pcfgsec)		{ ExFuncBase<DIM,ET>::Create( pcfgsec); }
	virtual void	PostCreateCheck() const					{ ExFuncBase<DIM,ET>::PostCreateCheck(); }
	virtual void	Init()									{}

	virtual MGFloat	Calculate( const MGFloat tab[])
	{
		typedef RED::EquationDef<DIM,ET> TDef;

		return tab[ TDef::ID_K ];
	}
};



#endif // __EXFUNC_H__
