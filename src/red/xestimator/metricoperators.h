#ifndef __METRICOPERATORS_H__
#define __METRICOPERATORS_H__


#include "libgreen/gridgeom.h" 
#include "estgrid.h"
#include "funcspace.h"

template <MGSize N>
class Factorial
{
public:
	enum { VAL = N * Factorial<N-1>::VAL };
};

template <>
class Factorial<1>
{
public:
	enum { VAL = 1 };
};


//////////////////////////////////////////////////////////////////////
//	class MetricIntersection
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class MetricIntersection
{
public:
	typedef SVector<DIM,MGFloat>	EVector;
	typedef SMatrix<DIM,MGFloat>	EMatrix;

	// using Eigen
	EMatrix	operator() ( const EMatrix& mtx1, const EMatrix& mtx2 );


	//EMatrix	operator() ( const EMatrix& mtx1, const EMatrix& mtx2 )
	//{
	//	EMatrix	mtxN, mtx;
	//	EMatrix	mtxPIT, mtxPI, mtxP, mtxPT, mtxDp1, mtxDp2;
	//	EVector vecD;

	//	mtxN = mtx1;
	//	mtxN.Invert();
	//	mtxN = mtxN * mtx2;

	//mtxN.Write();

	//	mtxN.Decompose( vecD, mtxP);
	//	

	//	mtxPT = mtxP;
	//	mtxPT.Transp();

	//mtxP.Write();
	//mtxPT.Write();

	//	mtxPI = mtxP;
	//	mtxPI.Invert();
	//	mtxPIT = mtxPI;
	//	mtxPIT.Transp();

	//mtxPI.Write();
	//mtxPIT.Write();

	//cout << "--------------------------" << endl;

	//mtx.Assemble( mtxP, vecD, mtxPI);
	//mtx.Write();

	//cout << "--------------------------" << endl;

	//	mtxDp1 = mtxPT * mtx1 * mtxP;
	//	mtxDp2 = mtxPT * mtx2 * mtxP;

	//	for ( MGSize i=0; i<DIM; ++i)
	//		vecD(i) = max( mtxDp1(i,i), mtxDp2(i,i) );

	//	mtxN.Assemble( mtxPIT, vecD, mtxPI );
	//	return mtxN;
	//}


	//EMatrix	operator() ( const EMatrix& mtx1, const EMatrix& mtx2 )
	//{
	//	EMatrix	mtx;
	//	EMatrix	mtxDp1, mtxLI1, mtxL1;
	//	EMatrix	mtxDp2, mtxLI2, mtxL2;
	//	EVector vecD1, vecD2;

	//	mtx = mtx1;
	//	mtx.Decompose( vecD1, mtxL1);
	//	mtxLI1 = mtxL1;
	//	mtxLI1.Invert();

	//	mtx = mtx2;
	//	mtx.Decompose( vecD2, mtxL2);
	//	mtxLI2 = mtxL2;
	//	mtxLI2.Invert();

	//	mtxDp1 = mtxLI1 * mtx2 * mtxL1;
	//	mtxDp2 = mtxLI2 * mtx1 * mtxL2;


	//	for ( MGSize i=0; i<DIM; ++i)
	//	{
	//		vecD1(i) = 1.0 / sqrt( max( mtxDp1(i,i), vecD1(i) ) );
	//		vecD2(i) = 1.0 / sqrt( max( mtxDp2(i,i), vecD2(i) ) );
	//		//vecD1(i) = max( mtxDp1(i,i), vecD1(i) );
	//		//vecD2(i) = max( mtxDp2(i,i), vecD2(i) );
	//	}

	//	mtxDp1.Assemble( mtxL1, vecD1, mtxLI1 );
	//	mtxDp2.Assemble( mtxL2, vecD2, mtxLI2 );

	//	mtx = 0.5 * ( mtxDp1 + mtxDp2 );

	//	mtx = mtx * mtx;
	//	mtx.Invert();

	//	//mtxDp1.Invert();
	//	//mtxDp2.Invert();
	//	//mtx = 0.5 * ( mtxDp1 + mtxDp2 );
	//	//mtx.Invert();

	//	return mtx;
	//}
};



//////////////////////////////////////////////////////////////////////
//	class MetricLimit
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class MetricLimit
{
public:
	typedef SVector<DIM,MGFloat>	EVector;
	typedef SMatrix<DIM,MGFloat>	EMatrix;

	MetricLimit( const MGFloat& lmin, const MGFloat& lmax, const MGFloat& armax ) : mLmin(lmin), mLmax(lmax), mARmax( armax)	{}

	EMatrix	operator() ( const EMatrix& mtx )
	{

		MGFloat	hmin_inv = 1.0/(mLmin*mLmin);
		MGFloat	hmax_inv = 1.0/(mLmax*mLmax);

		MGFloat	lmax, lar;

		EMatrix	mtxM, mtxD, mtxLI, mtxL;

		mtxM = mtx;
		mtxM.Decompose( mtxD, mtxL);

		for ( MGSize i=0; i<DIM; ++i)
		{
			if ( mtxD(i,i) > hmin_inv)
				mtxD(i,i) = hmin_inv;

			if ( mtxD(i,i) < hmax_inv)
				mtxD(i,i) = hmax_inv;
		}

		if ( mARmax > 0)
		{
			lmax = mtxD(0,0);
			for ( MGSize i=1; i<DIM; ++i)
				if ( mtxD(i,i) > lmax)
					lmax = mtxD(i,i);

			lar = lmax / ( mARmax * mARmax);

			for ( MGSize i=0; i<DIM; ++i)
			{
				if ( mtxD(i,i) < lar)
					mtxD(i,i) = lar;
			}
		}


		mtxLI = mtxL;
		mtxLI.Invert();

		mtxM = mtxL * mtxD * mtxLI;

		return mtxM;
	}

private:
	MGFloat mLmin;
	MGFloat mLmax;
	MGFloat mARmax;
};



//////////////////////////////////////////////////////////////////////
//	class MetricSpcNormalize
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class MetricSpcNormalize
{
public:
	typedef SVector<DIM,MGFloat>	EVector;
	typedef SMatrix<DIM,MGFloat>	EMatrix;

	MetricSpcNormalize( const EstGrid<DIM>* pgrid ) : mpGrid(pgrid)		{}

	void	Execute( Space<DIM,EMatrix>& spc )
	{
		if ( mpGrid->SizeNodeTab() != spc.Size() )
			THROW_INTERNAL( "MetricSpcNormalize<DIM>::operator() -- incompatible grid and metric sizes" );

		vector<MGFloat> tabdet( spc.Size(), 0.0);

		for ( MGSize i=0; i<spc.Size(); ++i)
		{
			EMatrix	mtxtmp = spc.cTab()[i];
			EMatrix	mtxL;
			EVector vecD;


			for ( MGSize k=0; k<DIM; ++k)
				for ( MGSize j=0; j<DIM; ++j)
					IS_INFNAN_THROW( mtxtmp(k,j), "");

			mtxtmp.Decompose( vecD, mtxL );

			for ( MGSize k=0; k<DIM; ++k)
				IS_INFNAN_THROW( vecD(k), "");

			MGFloat det = vecD[0];
			for ( MGSize k=1; k<DIM; ++k)
				det *= vecD[k];

			//tabdet[i] = spc.cTab()[i].Determinant();
			tabdet[i] = det;
			if ( tabdet[i] < 0.0 )
			{
				cout << "i = " << i << endl;
				cout << setprecision(17) << det << endl;
				cout << spc.cTab()[i].Determinant() << endl;

				vecD.Write();
				spc.cTab()[i].Write();

				cout << "MetricSpcNormalize<DIM>::operator() -- negative determinant = " << tabdet[i] << endl;

				tabdet[i] = 0.0;

				//THROW_INTERNAL( "MetricSpcNormalize<DIM>::operator() -- negative determinant = " << tabdet[i] );
			}
		}

		MGFloat totvol = 0.0;
		MGFloat totparvol = 0.0;

		for ( typename EstGrid<DIM>::ColCell::const_iterator citr = mpGrid->cColCell().begin(); citr != mpGrid->cColCell().end(); ++citr)
		{
			const typename EstGrid<DIM>::GCell& cell = *citr;
			Simplex<DIM> smpx = GREEN::GridGeom<DIM>::CreateSimplex( cell, *mpGrid);

			MGFloat vol = smpx.Volume();
			totvol += vol;

			MGFloat det = 0.0;
			for ( MGSize k=0; k<EstGrid<DIM>::GCell::SIZE; ++k)
				det += tabdet[ cell.cNodeId( k) - 1 ];
			det /= MGFloat( DIM + 1);

			MGFloat parvol = sqrt( det ) * vol;
			totparvol += parvol;

		}

		//MGFloat coeff = static_cast<MGFloat>( mpGrid->SizeCellTab() ) / static_cast<MGFloat>( DIM) / totparvol;
		MGFloat volreg = sqrt( static_cast<MGFloat>( DIM+1) ) / ( static_cast<MGFloat>( Factorial<DIM>::VAL ) * pow( static_cast<MGFloat>( DIM), DIM/2. ) );
		MGFloat coeff = static_cast<MGFloat>( mpGrid->SizeCellTab() ) * volreg / totparvol;
		coeff = pow( coeff*coeff, 1. / static_cast<MGFloat>(DIM) );

		for ( MGSize i=0; i<spc.Size(); ++i)
			spc.rTab()[ i] *= coeff;

	}

private:
	const EstGrid<DIM>*		mpGrid;
};




template< class ELEM_TYPE, MGSize MAX_SIZE>
inline void DecomposeROOT( SMatrix< MAX_SIZE, ELEM_TYPE>& mtxout, const SMatrix< MAX_SIZE, ELEM_TYPE>& mtx)
{
	// TODO :: check if matrix is symetric
	SVector< MAX_SIZE, ELEM_TYPE> vecD;
	SMatrix< MAX_SIZE, ELEM_TYPE> mtxtmp = mtx;
	SMatrix< MAX_SIZE, ELEM_TYPE> mtxN;

	mtxtmp.Decompose( vecD, mtxN, true);


	for ( MGSize k=0; k<MAX_SIZE; ++k)	// TODO :: check if it should not be Size() ???
		vecD(k) = 1.0 / ::sqrt( vecD(k));
		//mtxD(k,k) = 1.0 / ::pow( mtxD(k,k),0.4);

	SMatrix< MAX_SIZE, ELEM_TYPE> mtxNT = mtxN;
	mtxNT.Transp();

	mtxout.Assemble( mtxN, vecD, mtxNT );
	return;
}


template< class ELEM_TYPE, MGSize MAX_SIZE>
inline void DecomposeSQR( SMatrix< MAX_SIZE, ELEM_TYPE>& mtxout, const SMatrix< MAX_SIZE, ELEM_TYPE>& mtx)
{
	// TODO :: check if matrix is symetric
	SVector< MAX_SIZE, ELEM_TYPE> vecD;
	SMatrix< MAX_SIZE, ELEM_TYPE> mtxtmp = mtx;
	SMatrix< MAX_SIZE, ELEM_TYPE> mtxN;

	mtxtmp.Decompose( vecD, mtxN, true);


	for ( MGSize k=0; k<MAX_SIZE; ++k)	// TODO :: check if it should not be Size() ???
		vecD(k) = 1.0 / ( vecD(k) * vecD(k));
		//mtxD(k,k) = 1.0 / ::pow( mtxD(k,k), 1.0/0.4);

	SMatrix< MAX_SIZE, ELEM_TYPE> mtxNT = mtxN;
	mtxNT.Transp();

	mtxout.Assemble( mtxN, vecD, mtxNT);
	return;
}


#endif // __METRICOPERATORS_H__

