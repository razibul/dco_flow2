#include "master.h"
#include "configconst.h"
#include "libcorecommon/factory.h"


Master::~Master()	
{
}


void Master::ReadConfig( const MGString& fname)	
{ 
	mConfig.ReadCFG( fname);
	mConfig.WriteCFG( "_dump.cfg");
}



void Master::Create()
{
	//cout << ConfigStr::Solver::NAME << " count = " << mConfig.GetSectionCount( ConfigStr::Estimator::NAME ) << endl;

	MGSize count = mConfig.GetSectionCount( ConfigStr::Estimator::NAME );

	const CfgSection* cfgsec = &mConfig.GetSection( ConfigStr::Estimator::NAME );

	MGString sdim		= cfgsec->ValueString( ConfigStr::Estimator::Dim::KEY );
	MGString seqtype	= cfgsec->ValueString( ConfigStr::Estimator::EqType::KEY );

	MGString eekey = "ESTIMATOR_" + sdim + "_" + seqtype;
	cout << "creating '" << eekey << "'" << endl;


	EstimatorBase* ptr = Singleton< Factory< MGString,Creator<EstimatorBase> > >::GetInstance()->GetCreator( eekey )->Create( );
	ptr->Create( cfgsec);

	mpEstimator = ptr;


	// do postcreate check
	if ( ! mpEstimator )
		THROW_INTERNAL( "Master::PostCreateCheck() -- failed :  mpEstimator is NULL'" );

	mpEstimator->PostCreateCheck();
}


void Master::Init()
{
	mpEstimator->Init();
}


void Master::Execute()
{
	mpEstimator->Execute();
	//mpEstimator->ExecuteBnd();
}
