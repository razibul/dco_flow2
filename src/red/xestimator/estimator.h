#ifndef __ESTIMATOR_H__
#define __ESTIMATOR_H__

#include "estimatorbase.h" 
#include "estdata.h"
#include "estimatebase.h" 
#include "estcspace.h" 


#include "libcoreio/readtec.h"
#include "libcoreio/writetec.h"

#include "libredcore/equation.h"




//////////////////////////////////////////////////////////////////////
//	class Estimator
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, RED::EQN_TYPE ET>
class Estimator : public EstimatorBase
{
public:
	enum { ESIZE = RED::EquationDef<DIM,ET>::SIZE };

	Estimator()	{}
	virtual ~Estimator();
	//void	Init( const MGString& fname)	{ mData.Init( fname); }

	virtual void	Create( const CfgSection* pcfgsec);
	virtual void	PostCreateCheck() const;
	virtual void	Init();

	virtual void	Execute();

private:
	void	ExecuteFull();
	void	ExecuteBnd();

private:
	EstimatorData<DIM,ESIZE>	mData;
	EstCSpace<DIM>				mCSpace;

	vector< EstimateBase<DIM,ET>* >	mtabEstimate;
};


#endif // __ESTIMATOR_H__
