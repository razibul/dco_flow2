#ifndef __FUNCSPACE_H__
#define __FUNCSPACE_H__


#include "estgrid.h"

#include "libcoreio/store.h"
#include "libcoreio/writesol.h"


//////////////////////////////////////////////////////////////////////
//	class IOBlock - after apropriate specialization used for export
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, class T>
class IOBlock
{
public:
	IOBlock( const T& t) : mVar(t)	{}

	static MGSize Size()	{ return 0; }

	void	Prepare()						{}

	MGFloat	Var( const MGSize& i) const		{ return 0.0; }

private:
	T	mVar;
};

//////////////////////////////////////////////////////////////////////
// MGFloat
template <Dimension DIM>
class IOBlock< DIM, MGFloat >
{
public:
	IOBlock( const MGFloat& t) : mVar(t)	{}

	static MGSize Size()	{ return 1; }

	void	Prepare()						{}
	MGFloat	Var( const MGSize& i) const		{ return mVar; }

private:
	MGFloat	mVar;
};


//////////////////////////////////////////////////////////////////////
// SVector
template <Dimension DIM>
class IOBlock< DIM, SVector<DIM,MGFloat> >
{
public:
	IOBlock( const SVector<DIM,MGFloat>& t) : mVar(t)	{}

	static MGSize Size()	{ return DIM; }

	void	Prepare()						{}
	MGFloat	Var( const MGSize& i) const		{ return mVar(i); }

private:
	SVector<DIM,MGFloat>	mVar;
};

//////////////////////////////////////////////////////////////////////
// SMatrix
template <Dimension DIM>
class IOBlock< DIM, SMatrix<DIM,MGFloat> >
{
public:
	IOBlock( const SMatrix<DIM,MGFloat>& t) : mMtx(t)	{}

	static MGSize Size()	{ return DIM*DIM + DIM; }

	void	Prepare()						
	{
		SMatrix<DIM,MGFloat> mtxtmp = mMtx;
		SMatrix<DIM,MGFloat> mtxN;

		mtxtmp.Decompose( mvecD, mtxN, true);
	}

	MGFloat	Var( const MGSize& i) const		
	{ 
		if ( i < DIM*DIM )
			return mMtx[i];
		else
			return mvecD( i - DIM*DIM );
	}

private:
	SMatrix<DIM,MGFloat>	mMtx;
	SVector<DIM,MGFloat>	mvecD;
};



//////////////////////////////////////////////////////////////////////
//	class Space
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, class T>
class Space : public IO::SolFacade
{
public:
	Space()		{}

	MGSize				Size() const	{ return mTab.size();}			
	const vector<T>&	cTab() const	{ return mTab; }
	vector<T>&			rTab()			{ return mTab; }


	virtual Dimension	Dim() const								{ return DIM;}

	virtual void	IOGetName( MGString& name) const			{ name = "";}
	virtual void	IOSetName( const MGString& name)			{}

	virtual void	DimToUndim( vector<MGFloat>& tab ) const	{}
	virtual void	UndimToDim( vector<MGFloat>& tab ) const	{}
 
	virtual MGSize	IOSize() const								{ return mTab.size();}			
	virtual MGSize	IOBlockSize() const							{ return IOBlock<DIM,T>::Size();}

	virtual void	IOResize( const MGSize& n)					{ THROW_INTERNAL( "Should never be called"); mTab.resize( n); }

	virtual void	IOGetBlockVector( vector<MGFloat>& tabx, const MGSize& id) const;
	virtual void	IOSetBlockVector( const vector<MGFloat>& tabx, const MGSize& id );

private:
	vector<T>			mTab;
};


template <Dimension DIM, class T>
void Space<DIM,T>::IOGetBlockVector( vector<MGFloat>& tabx, const MGSize& id) const
{
	IOBlock<DIM,T> block( mTab[id] );
	block.Prepare();
	for ( MGSize i=0; i<block.Size(); ++i)
		tabx[i] = block.Var(i);
}

template <Dimension DIM, class T>
void Space<DIM,T>::IOSetBlockVector( const vector<MGFloat>& tabx, const MGSize& id )
{
	THROW_INTERNAL( "Should never be called"); 
}


#endif // __FUNCSPACE_H__
