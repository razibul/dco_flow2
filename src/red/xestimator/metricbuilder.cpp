#include "metricbuilder.h"
#include "metricgradbuilder.h"
#include "metrichessbuilder.h"
#include "libcorecommon/factory.h"

//////////////////////////////////////////////////////////////////////
void init_MetricBuildFunc()
{
	static ConcCreator< MGString, MetricHessBuilder<DIM_2D>, MetricBuilder<DIM_2D> >		gCreatorMetricHessBuilder2D( "METBUILD_2D_HESSIAN");
	static ConcCreator< MGString, MetricGradBuilder<DIM_2D>, MetricBuilder<DIM_2D> >		gCreatorMetricGradBuilder2D( "METBUILD_2D_GRADIENT");

	static ConcCreator< MGString, MetricHessBuilder<DIM_3D>, MetricBuilder<DIM_3D> >		gCreatorMetricHessBuilder3D( "METBUILD_3D_HESSIAN");
	static ConcCreator< MGString, MetricGradBuilder<DIM_3D>, MetricBuilder<DIM_3D> >		gCreatorMetricGradBuilder3D( "METBUILD_3D_GRADIENT");
}
