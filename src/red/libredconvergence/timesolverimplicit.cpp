#include "timesolverimplicit.h"
#include "libcorecommon/factory.h"
#include "libredconvergence/spacesolverfacade.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//ConcCreator< MGString, TimeSolverExplicit< EquationDef<Geom::DIM_2D,EQN_EULER>::SIZE > >, TimeSolverBase< EquationDef<Geom::DIM_2D,EQN_EULER>::SIZE > > >	
//	gCreatorTimeSolverEulerExplicit( "TIMESOL_2D_EULER_EXPLICIT");



void init_TimeSolverImplicit()
{
	static ConcCreator< MGString, TimeSolverImplicit<1>, TimeSolverBase<1> >	gCreatorTimeSolverImplicitBSize1( "TIMESOL_BSIZE_1_IMPLICIT");
	static ConcCreator< MGString, TimeSolverImplicit<2>, TimeSolverBase<2> >	gCreatorTimeSolverImplicitBSize2( "TIMESOL_BSIZE_2_IMPLICIT");
	static ConcCreator< MGString, TimeSolverImplicit<4>, TimeSolverBase<4> >	gCreatorTimeSolverImplicitBSize4( "TIMESOL_BSIZE_4_IMPLICIT");
	static ConcCreator< MGString, TimeSolverImplicit<5>, TimeSolverBase<5> >	gCreatorTimeSolverImplicitBSize5( "TIMESOL_BSIZE_5_IMPLICIT");
	static ConcCreator< MGString, TimeSolverImplicit<6>, TimeSolverBase<6> >	gCreatorTimeSolverImplicitBSize6( "TIMESOL_BSIZE_6_IMPLICIT");
	static ConcCreator< MGString, TimeSolverImplicit<7>, TimeSolverBase<7> >	gCreatorTimeSolverImplicitBSize7( "TIMESOL_BSIZE_7_IMPLICIT");
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

