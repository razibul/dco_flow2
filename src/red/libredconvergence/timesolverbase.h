#ifndef __TIMESOLVER_H__
#define __TIMESOLVER_H__


#include "libcoreconfig/configbase.h"
#include "libredconvergence/spacesolverfacade.h"
#include "libredconvergence/convergenceinfo.h"
#include "libextsparse/sparsevector.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

template <MGSize BSIZE>
class SpaceSolverFacade;


//////////////////////////////////////////////////////////////////////
//	class TimeSolverBase
//////////////////////////////////////////////////////////////////////
template <MGSize BSIZE>
class TimeSolverBase : public ConfigBase
{
public:
	static MGString	Info()	
	{
		ostringstream os;
		os << "TimeSolverBase< " <<  BSIZE << " >";
		return os.str();
	}

	TimeSolverBase() : ConfigBase(), mpSpaceSol(NULL)		{}

	virtual void	Create( const CfgSection* pcfgsec, SpaceSolverFacade<BSIZE>* pssol);
	virtual void	PostCreateCheck() const;
	virtual void	Init()	{}

	virtual void	DoStep( ConvergenceInfo<BSIZE>& info)	{}

	Sparse::Vector<BSIZE>&	rLocalSolution()	{ return mvecQ;}

protected:
	const SpaceSolverFacade<BSIZE>&	cSpaceSol() const	{ return *mpSpaceSol;}
	SpaceSolverFacade<BSIZE>&		rSpaceSol() const	{ return *mpSpaceSol;}


	Sparse::Vector<BSIZE>	mvecQ;

private:
	SpaceSolverFacade<BSIZE>*	mpSpaceSol;
};
//////////////////////////////////////////////////////////////////////


template <MGSize BSIZE>
inline void TimeSolverBase<BSIZE>::Create( const CfgSection* pcfgsec, SpaceSolverFacade<BSIZE>* pssol)
{ 
	ConfigBase::Create( pcfgsec);

	mpSpaceSol = pssol;
}

template <MGSize BSIZE>
inline void TimeSolverBase<BSIZE>::PostCreateCheck() const
{ 
	ConfigBase::PostCreateCheck();

	if ( ! mpSpaceSol )
		THROW_INTERNAL( Info() << "::PostCreateCheck() -- failed : mpSpaceSol -> NULL" );
}



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __TIMESOLVER_H__
