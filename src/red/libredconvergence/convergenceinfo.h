#ifndef __CONVERGENCEINFO_H__
#define __CONVERGENCEINFO_H__

#include "libcoresystem/mgdecl.h"
#include "libredcore/equation.h"
#include "libextsparse/blockvector.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
//	class ConvergenceInfo
//////////////////////////////////////////////////////////////////////
template <MGSize SIZE>
class ConvergenceInfo
{
public:
	typedef Sparse::BlockVector<SIZE>	Vect;

	ConvergenceInfo()	{ Reset();}

	void Reset()
	{
		for ( MGSize i=0; i<SIZE; ++i)
			mErrResL2[i] = mErrResLinf[i] = mErrDQL2[i] = mErrDQLinf[i] = 0.0;
	}

	template <class MAPPER>
	void	Update( const ConvergenceInfo<MAPPER::VBSIZE>& locinfo)
	{
		MAPPER::LocalToGlobal( mErrResL2, locinfo.mErrResL2 );
		MAPPER::LocalToGlobal( mErrResLinf, locinfo.mErrResLinf );
		MAPPER::LocalToGlobal( mErrDQL2, locinfo.mErrDQL2 );
		MAPPER::LocalToGlobal( mErrDQLinf, locinfo.mErrDQLinf );
	}


public:
	Vect		mErrResL2;
	Vect		mErrResLinf;
	Vect		mErrDQL2;
	Vect		mErrDQLinf;

	//MGFloat		mErrResL2[SIZE];
	//MGFloat		mErrResLinf[SIZE];
	//MGFloat		mErrDQL2[SIZE];
	//MGFloat		mErrDQLinf[SIZE];
};


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __CONVERGENCEINFO_H__
