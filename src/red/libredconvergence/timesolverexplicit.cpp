#include "timesolverexplicit.h"
#include "libcorecommon/factory.h"
#include "libredconvergence/spacesolverfacade.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//ConcCreator< MGString, TimeSolverExplicit< EquationDef<Geom::DIM_2D,EQN_EULER>::SIZE > >, TimeSolverBase< EquationDef<Geom::DIM_2D,EQN_EULER>::SIZE > > >	
//	gCreatorTimeSolverEulerExplicit( "TIMESOL_2D_EULER_EXPLICIT");

void init_TimeSolverExplicit()
{
	static ConcCreator< MGString, TimeSolverExplicit<1>, TimeSolverBase<1> >	gCreatorTimeSolverExplicitBSize1( "TIMESOL_BSIZE_1_EXPLICIT");
	static ConcCreator< MGString, TimeSolverExplicit<2>, TimeSolverBase<2> >	gCreatorTimeSolverExplicitBSize2( "TIMESOL_BSIZE_2_EXPLICIT");
	static ConcCreator< MGString, TimeSolverExplicit<4>, TimeSolverBase<4> >	gCreatorTimeSolverExplicitBSize4( "TIMESOL_BSIZE_4_EXPLICIT");
	static ConcCreator< MGString, TimeSolverExplicit<5>, TimeSolverBase<5> >	gCreatorTimeSolverExplicitBSize5( "TIMESOL_BSIZE_5_EXPLICIT");
	static ConcCreator< MGString, TimeSolverExplicit<6>, TimeSolverBase<6> >	gCreatorTimeSolverExplicitBSize6( "TIMESOL_BSIZE_6_EXPLICIT");
	static ConcCreator< MGString, TimeSolverExplicit<7>, TimeSolverBase<7> >	gCreatorTimeSolverExplicitBSize7( "TIMESOL_BSIZE_7_EXPLICIT");
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

