#include "libcoresystem/mgdecl.h"
#include "libcoregeom/vect.h"
#include "libcoregeom/gaussweights.h"

#include "libcorecommon/smatrix.h"

#include "redvc/libredvcgrid/compgeomcell.h"


INIT_VERSION("0.1","''");

using namespace Geom;
using namespace RED;





template <Dimension DIM>
void testGQuad( MGSize order)
{
	GaussQuadrature<DIM> gquad(order);

	cout << "Order = " << order << endl;

	MGFloat sumwgh = 0;

	for (MGSize i=0; i<gquad.Size(); ++i )
	{
		//cout << " weight = " << gquad.Weight( i) << endl;
		sumwgh += gquad.Weight( i);
		MGFloat sumbary = 0.;

		for ( MGSize k=0; k<=DIM; ++k )
		{
			sumbary += gquad.BaryCoordinates(i)[k];
		}

		cout << "  Sum of barycentric coordinates for point " << i << " = " << sumbary << endl;
	}

	cout << "Sum of weights = " << sumwgh << endl;

}


template <Dimension DIM>
MGFloat funPoly( const Vect<DIM>& pos, const MGSize& deg )
{
	MGFloat val = 0.0;

	for ( MGSize i=0; i<DIM; ++i)
	{
		MGFloat p = 1.;
		for ( MGSize k=0; k<deg; ++k)
			p *= pos.cX(i);
		val += p;
	}

	return val + 0.5;
}


template <Dimension DIM>
void integ( const MGSize& order, const MGSize& deg)
{
	Vect<DIM> tabx[DIM+1];

	for ( MGSize i=0; i<=DIM; ++i)
		tabx[i] = Vect<DIM>( 0.0);

	for ( MGSize i=1; i<=DIM; ++i)
		tabx[i].rX(i-1) = 1.0;

	//for ( MGSize i=0; i<=DIM; ++i)
	//	cout << tabx[i];
	//cout << endl;

	GaussQuadrature<DIM> gquad(order);

	MGFloat integ = 0.0;
	for ( MGSize i=0; i<gquad.Size(); ++i)
	{
		Vect<DIM> pos( 0.0);
		for ( MGSize k=0; k<=DIM; ++k)
			pos += tabx[k] * gquad.BaryCoordinates(i)[k];

		MGFloat val = funPoly<DIM>( pos, deg);
		integ += val * gquad.Weight(i);
	}

	MGFloat area = 1.0;
	for ( MGSize id=1; id<=DIM; ++id)
		area /= (MGFloat)id;
	integ *= area;

	cout << order << "  " << setprecision( 16) << "integral = " << integ << endl;
}

void funtest_BaryW()
{
		for ( MGSize io=1; io<=4; ++io)
			testGQuad<DIM_1D>( io);
		cout << endl;

		for ( MGSize io=1; io<=6; ++io)
			testGQuad<DIM_2D>( io);
		cout << endl;

		for ( MGSize io=1; io<=6; ++io)
			testGQuad<DIM_3D>( io);
		cout << endl;
}







int main( int argc, char* argv[])
{
// 	cout << "test" << endl;
// 	
// 	typedef EquationDef<Geom::DIM_2D,EQN_NS> TEq;
// 
// 	cout << TEq::SIZE << endl;
// 	cout << TEq::ID_RHO << endl;
// 	cout << TEq::ID_P << endl;
// 	cout << TEq::U<0>::ID << endl;
// 	cout << TEq::U<1>::ID << endl;
// 
// 
// 	return 1;


	try
	{
		CheckTypeSizes();

		cout << "ver.: " << VERSION_NUMBER << "  build: " << VERSION_DATE << " " << VERSION_TIME << endl << endl;
		///////////////////////////////////////////////////////////////////////

		//CGeomCell<DIM_2D> gcell;

		//gcell.rNode(0).rPos() = Vect2D( 0., 0.); 
		//gcell.rNode(1).rPos() = Vect2D( 1., 0.); 
		//gcell.rNode(2).rPos() = Vect2D( 0., 1.); 

		//class Func1
		//{
		//public:
		//	MGFloat operator () ( const Vect2D& pos, const MGFloat& u)
		//	{
		//		return pos.cX()*pos.cX() + 3.0*pos.cX()*pos.cY() + 2.*pos.cY()*pos.cY();
		//	}
		//};

		//MGFloat tabu[] = { 1., 1., 1.};

		//CellInegrator<DIM_2D> cint( gcell);

		//MGFloat res = cint.VolumeIntegral<MGFloat, MGFloat, Func1>( tabu);

		//cout << res << endl;
		//
		//return 0;

		//SMatrix<4> mtx;
		//MGSize n = 4;
		//for ( MGSize i=0; i<n; ++i)
		//	for ( MGSize k=0; k<n; ++k)
		//		mtx(i,k) = i*k+k+1;

		//mtx.Write();

		//HessReduct<MGFloat, SMatrix<4> >  hred( mtx);
		//hred.Execute();

		//return 0;

		//testGQuad<DIM_3D>( 4);
		//return 0;

		//funtest_BaryW();
		
		//for ( MGSize ideg=1; ideg<=8; ++ideg)
		//{
		//	cout << "deg = " << ideg << endl;
		//	for ( MGSize io=1; io<=7; ++io)
		//		integ<DIM_1D>( io, ideg);
		//	cout << endl;
		//}

		//for ( MGSize ideg=1; ideg<=6; ++ideg)
		//{
		//	cout << "deg = " << ideg << endl;
		//	for ( MGSize io=1; io<=6; ++io)
		//		integ<DIM_2D>( io, ideg);
		//	cout << endl;
		//}

		for ( MGSize ideg=1; ideg<=6; ++ideg)
		{
			cout << "deg = " << ideg << endl;
			for ( MGSize io=1; io<=6; ++io)
				integ<DIM_3D>( io, ideg);
			cout << endl;
		}


	}
	catch ( EHandler::Except& e)
	{
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);
	}
	catch( exception &e) 
	{
		TRACE( e.what() );
		fprintf( stderr, "%s\n", e.what() );
	}
	catch( ...) 
	{
		cout << "unknow problem ::  ..." << endl;
	}

	return 0;
}
