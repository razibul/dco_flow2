#ifndef __EQNRANSSA_H__
#define __EQNRANSSA_H__

#include "eulerequations.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
// class EqnRansSA 
//////////////////////////////////////////////////////////////////////
template <Geom::Dimension DIM>
class EqnRansSA
{
	typedef EquationDef<DIM,EQN_RANS_SA> ETRansSA;

public:
	template <class VECTOR> static MGFloat SqrU( const VECTOR& var);

	// Conversion of the variables	
	template <class VECTOR> static VECTOR SimpToConserv( const VECTOR& vsmp)
	{
		VECTOR vcns;

		vcns = EqnEuler<DIM>::SimpToConserv( vsmp);
		vcns( ETRansSA::ID_K)   = vsmp( ETRansSA::ID_RHO) * vsmp( ETRansSA::ID_K);

		return vcns;
	}

	template <class VECTOR>	static VECTOR ConservToSimp( const VECTOR& vcns)
	{
		VECTOR vsmp;

		vsmp = EqnEuler<DIM>::ConservToSimp( vcns);
		vsmp( ETRansSA::ID_K)   = vcns( ETRansSA::ID_K) / vcns( ETRansSA::ID_RHO);

		return vsmp;
	}

	template <class VECTOR>	static VECTOR ConservToZ( const VECTOR& vcns)
	{
		VECTOR vz;

		vz = EqnEuler<DIM>::ConservToZ( vcns);
		vz( ETRansSA::ID_K) = vcns( ETRansSA::ID_K) / vz( ETRansSA::ID_RHO);

		return vz;
	}

	template <class VECTOR>	static VECTOR ZToConserv( const VECTOR& vz)
	{
		VECTOR vcns;

		vcns = EqnEuler<DIM>::ZToConserv( vz);
		vcns( ETRansSA::ID_K) = vz( ETRansSA::ID_K) * vz( ETRansSA::ID_RHO);

		return vcns;
	}

	template <class VECTOR>	static VECTOR SimpToZ( const VECTOR& vsmp)
	{
		VECTOR vz;

		vz = EqnEuler<DIM>::SimpToZ( vsmp);
		vz( ETRansSA::ID_K) = vsmp( ETRansSA::ID_K) * vz( ETRansSA::ID_RHO);

		return vz;
	}

	template <class VECTOR>	static VECTOR ZToSimp( const VECTOR& vz)
	{
		VECTOR vsmp;

		vsmp = EqnEuler<DIM>::ZToSimp( vz);
		vsmp( ETRansSA::ID_K) = vz( ETRansSA::ID_K) / vz( ETRansSA::ID_RHO);

		return vsmp;
	}

};


//// Conversion of the variables	
//template <class VECTOR> 
//inline VECTOR EqnRansSA<Geom::DIM_2D>::SimpToConserv( const VECTOR& vsmp)
//{
//	VECTOR vcns;
//
//	vcns = EqnEuler<Geom::DIM_2D>::SimpToConserv( vsmp);
//	vcns( ETRansSA::ID_K)   = vsmp( ETRansSA::ID_RHO) * vsmp( ETRansSA::ID_K);
//
//	return vcns;
//}
//
//template <class VECTOR> 
//inline VECTOR EqnRansSA<Geom::DIM_2D>::ConservToSimp( const VECTOR& vcns)
//{
//	VECTOR vsmp;
//
//	vsmp = EqnEuler<Geom::DIM_2D>::SimpToConserv( vcns);
//	vsmp( ETRansSA::ID_K)   = vcns( ETRansSA::ID_K) / vcns( ETRansSA::ID_RHO);
//
//	return vsmp;
//}



////////////////////////////////////////////////////////////////////////
//// class EqnRansSAComm
////////////////////////////////////////////////////////////////////////
//template <Geom::Dimension DIM>
//class EqnRansSAComm
//{
//	typedef EquationDef<DIM,EQN_RANS_SA> ETRansSA;
//	//typedef EquationDef<DIM,EQN_EULER> ETEuler;
//
//public:
//
//	template <class VECTOR> static VECTOR DimToUndim( const VECTOR& vin, const VECTOR& vref)
//	{
//		VECTOR vout;
//		MGFloat u2 = EqnEuler<DIM>::SqrU( vref);
//
//		vout( ETEuler::ID_RHO)	= vin( ETEuler::ID_RHO) / vref( ETEuler::ID_RHO);
//		vout( ETEuler::ID_P)	= vin( ETEuler::ID_P) * vref( ETEuler::ID_RHO) / u2;
//		u2 = ::sqrt( u2);
//		for ( MGSize i=2; i<DIM+2; ++i)
//			vout( i) = vin(i) / u2;
//
//		return vout;
//	}
//
//	template <class VECTOR> static VECTOR UndimToDim( const VECTOR& vin, const VECTOR& vref)
//	{
//		VECTOR vout;
//		MGFloat u2 = EqnEuler<DIM>::SqrU( vref);
//
//		vout( ETEuler::ID_RHO)	= vin( ETEuler::ID_RHO) * vref( ETEuler::ID_RHO);
//		vout( ETEuler::ID_P)	= vin( ETEuler::ID_P) * u2 / vref( ETEuler::ID_RHO);
//		u2 = ::sqrt( u2);
//		for ( MGSize i=2; i<DIM+2; ++i)
//			vout( i) = vin(i) * u2;
//
//		return vout;
//	}
//
//};


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __EQNRANSSA_H__
