#ifndef __PHYSICS_H__
#define __PHYSICS_H__


#include "libredphysics/physicscommon.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//template<Geom::Dimension DIM, EQN_TYPE ET>
//class PeriodicBCCalc
//{
//	typedef EquationDef<DIM, ET>		EqDef;
//	typedef typename EqDef::FVec		FVec;
//
//public:
//	void GetPeriodicVar( const MGSize& id, FVec& v);
//};


//////////////////////////////////////////////////////////////////////
//	class Physics
//////////////////////////////////////////////////////////////////////
template <Geom::Dimension DIM, EQN_TYPE ET>
class Physics
{
};
//////////////////////////////////////////////////////////////////////


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


#include "advectphysics.h"
#include "poissonphysics.h"
#include "eulerphysics.h"
#include "nsphysics.h"
#include "ransphysics.h"
#include "ranssaphysics.h"
#include "ranskomegaphysics.h"

#include "rfreulerphysics.h"
#include "rfrnsphysics.h"

//#include "redvc/libredvcgrid/periodicbc.h"


#endif // __PHYSICS_H__
