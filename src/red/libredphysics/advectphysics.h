#ifndef __ADVECTPHYSICS_H__
#define __ADVECTPHYSICS_H__


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
//	class Physics for Advection
//////////////////////////////////////////////////////////////////////
template <Geom::Dimension DIM>
class Physics<DIM,EQN_ADVECT> : public PhysicsCommon<DIM,EQN_ADVECT>
{
	enum { ESIZE = PhysicsCommon<DIM,EQN_ADVECT>::ESIZE };

	typedef typename PhysicsCommon<DIM,EQN_ADVECT>::EqDef	EqDef;
	typedef typename PhysicsCommon<DIM,EQN_ADVECT>::FVec	FVec;
	typedef typename PhysicsCommon<DIM,EQN_ADVECT>::AVec	AVec;
	typedef typename PhysicsCommon<DIM,EQN_ADVECT>::GVec	GVec;

public:
	Physics()	: PhysicsCommon<DIM,EQN_ADVECT>()	{}

	virtual void Create( const CfgSection* pcfgsec);
	virtual void PostCreateCheck() const;
	virtual void Init();

	void	ApplyBC(FVec& vecout, AVec& avecout, const FVec& vecin, const AVec& avecin, const MGSize& surfid, const GVec& vpos, const GVec& vnn, const MGFloat& dy, const typename PhysicsCommon<DIM,EQN_ADVECT>::PrdBCBase* pbc, const MGSize& nodeid) const;

	template <class VECTOR>
	bool	IsPhysical( const VECTOR& vec);

};
//////////////////////////////////////////////////////////////////////


template <Geom::Dimension DIM>
inline void Physics<DIM,EQN_ADVECT>::Create( const CfgSection* pcfgsec)		
{ 
	PhysicsCommon<DIM,EQN_ADVECT>::Create( pcfgsec);
}


template <Geom::Dimension DIM>
inline void Physics<DIM,EQN_ADVECT>::PostCreateCheck() const
{ 
	PhysicsCommon<DIM,EQN_ADVECT>::PostCreateCheck();

	// do check
}

template <Geom::Dimension DIM>
template <class VECTOR>
bool Physics<DIM,EQN_ADVECT>::IsPhysical( const VECTOR& vec)
{
	return true;
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __ADVECTPHYSICS_H__
