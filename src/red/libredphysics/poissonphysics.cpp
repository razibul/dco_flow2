#include "physics.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
//	class Physics for Poisson
//////////////////////////////////////////////////////////////////////


template <Geom::Dimension DIM>
void Physics<DIM,EQN_POISSON>::InitFVec( FVec& vec, const CfgSection& sec)
{
	FVec	vs( ESIZE, 0.0);

	if ( strcmp( sec.ValueString( ConfigStr::Solver::Physics::Var::Type::KEY).c_str(), ConfigStr::Solver::Physics::Var::Type::Value::RPUV) == 0 )
	{
		vs( 0) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::RO);;
		//vs.Write();
	}
	else
	{
		THROW_INTERNAL( "unrecognized type of variable: '" << sec.ValueString( ConfigStr::Solver::Physics::Var::Type::KEY) << "'" );
	}

	vec = vs;
	//vec.Write();
}


template <Geom::Dimension DIM>
void Physics<DIM,EQN_POISSON>::Init()
{
	mRefLength = this->ConfigSect().ValueFloat( ConfigStr::Solver::Physics::REF_LENGTH );

	mcoeffLapC		= this->ConfigSect().ValueFloat( ConfigStr::Solver::Physics::COEF_POISSON_LAPC );
	mcoeffLapPhi	= this->ConfigSect().ValueFloat( ConfigStr::Solver::Physics::COEF_POISSON_LAPPHI );
	mcoeffConv		= this->ConfigSect().ValueFloat( ConfigStr::Solver::Physics::COEF_POISSON_CONV );


	// parent class Init()
	PhysicsCommon<DIM,EQN_POISSON>::Init();
}


template <Geom::Dimension DIM>
bool Physics<DIM,EQN_POISSON>::IsStrongBC( const MGSize& id) const
{
	BC	bc = this->cBC( id);

	switch ( bc)
	{
	case BC_DEF:
	case BC_FARFIELD:
			return true;
	}

	return false;
}


template <Geom::Dimension DIM>
bool Physics<DIM,EQN_POISSON>::IsEqStrongBC( const MGSize& id, const MGSize& ie) const
{
	BC	bc = this->cBC( id);

	switch ( bc)
	{
	case BC_DEF:
	case BC_FARFIELD:
			return true;
	}

	return false;
}


template <Geom::Dimension DIM>
void Physics<DIM, EQN_POISSON>::ApplyBC(FVec& vecout, AVec& avecout, const FVec& vecin, const AVec& avecin, const MGSize& surfid, const GVec& vpos, const GVec& vnn, const MGFloat& dy, const typename PhysicsCommon<DIM,EQN_POISSON>::PrdBCBase* pbc, const MGSize& nodeid) const
{
	BC	bc = this->cBC( surfid);
	MGSize	varid = this->cVarId( surfid);


	switch ( bc)
	{
	case BC_EXTRAPOL:
		{
			vecout = vecin;
			vecout[0] += dy;
			break;
		}
	case BC_OUTLET:
	case BC_SYMMETRY:
		{
			vecout = vecin;

			break;
		}
	case BC_FARFIELD:
		{
			vecout = this->cDefVar( 0);
			break;
		}
	case BC_PERIODIC:
	{
			FVec tmp;
			pbc->GetPeriodicVar(nodeid, tmp);
			for (MGSize i = 0; i < ESIZE; ++i)
			{
				vecout(i) = tmp(i);
			}
			break;
	}
	case BC_DEF:
		{
			vecout = this->cDefVar( varid);
			break;
		}
	default:
		{
			char sbuf[1024];
			sprintf( sbuf, "BC not implemented! %d", bc);
			THROW_INTERNAL( sbuf);
		}
	}
}

//////////////////////////////////////////////////////////////////////
template class Physics<Geom::DIM_2D, EQN_POISSON>;
template class Physics<Geom::DIM_3D, EQN_POISSON>;

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

