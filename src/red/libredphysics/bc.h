#ifndef __BC_H__
#define __BC_H__

#include "libcoresystem/mgdecl.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


enum BC
{
	BC_NONE			= 0,
	BC_FARFIELD		= 1,
	BC_INVISCWALL	= 2,
	BC_VISCWALL		= 3,
	BC_SYMMETRY		= 4,
	BC_EXTRAPOL		= 5,
	BC_DEF			= 6,
	BC_INLET		= 7,
	BC_OUTLET		= 8,
	BC_PERIODIC		= 9
};



BC	StrToBC( const MGString& str);
MGString BCToStr( const BC& bc);


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __BC_H__
