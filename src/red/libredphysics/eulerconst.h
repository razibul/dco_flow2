#ifndef __EULERCONST_H__
#define __EULERCONST_H__

#include "libcoresystem/mgdecl.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

extern const MGFloat	FLOW_K;
extern const MGFloat	SOLV_ZERO;
extern const MGFloat	P_LLR_EPS;

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __EULERCONST_H__
