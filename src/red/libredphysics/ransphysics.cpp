#include "libredcore/configconst.h"
#include "libredphysics/physics.h"
#include "libredphysics/eulerequations.h"
#include "libredphysics/ransphysics.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
//	class Physics for RANS with 2 turbulence equations
//////////////////////////////////////////////////////////////////////


// HACK :: this the same as for Euler...
template <Geom::Dimension DIM>
void Physics<DIM,EQN_RANS_2E>::InitFVec( FVec& vec, const CfgSection& sec)
{
	typedef	EquationDef<Geom::DIM_2D,EQN_RANS_2E>::SplitingFlowTurb::Block<1> TMapper;
	typedef TMapper::FVec	TFlowVec;

	TFlowVec vs, vout;

	vec.Init( 0.0);

	if ( strcmp( sec.ValueString( ConfigStr::Solver::Physics::Var::Type::KEY ).c_str(), ConfigStr::Solver::Physics::Var::Type::Value::MRPA ) == 0 )
	{
		MGFloat ma = sec.ValueFloat( ConfigStr::Solver::Physics::Var::MACH );
		MGFloat alpha = M_PI / 180.0 * sec.ValueFloat( ConfigStr::Solver::Physics::Var::ALPHA );
		MGFloat ro =  sec.ValueFloat( ConfigStr::Solver::Physics::Var::RO );
		MGFloat p =  sec.ValueFloat( ConfigStr::Solver::Physics::Var::P );

		MGFloat c = ::sqrt( FLOW_K * p / ro);

		vs( EqDef::ID_RHO) = ro;
		vs( EqDef::ID_P) = p;
		vs( EqDef::template U<0>::ID) = cos( alpha) * ma * c;
		vs( EqDef::template U<1>::ID) = sin( alpha) * ma * c;
		if ( DIM >= Geom::DIM_3D)
			vs( EqDef::template U<2>::ID) = 0;

		//vs.Write();
	}
	else if ( strcmp( sec.ValueString( ConfigStr::Solver::Physics::Var::Type::KEY).c_str(), ConfigStr::Solver::Physics::Var::Type::Value::RPUV) == 0 )
	{
		vs( EqDef::ID_RHO) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::RO);;
		vs( EqDef::ID_P) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::P);
		vs( EqDef::template U<0>::ID) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::UX );
		vs( EqDef::template U<1>::ID) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::UY );
		if ( DIM >= Geom::DIM_3D)
			vs( EqDef::template U<2>::ID) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::UZ );

		//vs.Write();
	}
	else if ( strcmp( sec.ValueString( ConfigStr::Solver::Physics::Var::Type::KEY).c_str(), ConfigStr::Solver::Physics::Var::Type::Value::RRERURV) == 0 )
	{
		vec( EqDef::ID_RHO) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::RO);;
		vec( EqDef::ID_P) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::ROE);
		vec( EqDef::template U<0>::ID) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::ROUX );
		vec( EqDef::template U<1>::ID) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::ROUY );
		if ( DIM >= Geom::DIM_3D)
			vs( EqDef::template U<2>::ID) = sec.ValueFloat( ConfigStr::Solver::Physics::Var::ROUZ);

		vs = EqnEuler<DIM>::ConservToSimp( vs);
		//vs.Write();
	}
	else
	{
		THROW_INTERNAL( "unrecognized type of variable: '" << sec.ValueString( ConfigStr::Solver::Physics::Var::Type::KEY) << "'" );
	}

	vout = EqnEuler<DIM>::SimpToConserv( vs);
	//vec.Write();


	TMapper::LocalToGlobal( vec, vout);

}


template <Geom::Dimension DIM>
void Physics<DIM,EQN_RANS_2E>::Init()
{
	//if ( strcmp( sec.Name().c_str(), STR_CFGSEC_SPACE_NAME) != 0)
	//	THROW_INTERNAL( "Bad section used for CfgSpaceSolver");

	mRefLength = this->ConfigSect().ValueFloat( ConfigStr::Solver::Physics::REF_LENGTH );
	mRefRe = this->ConfigSect().ValueFloat( ConfigStr::Solver::Physics::REF_REYNOLDS );
	mRefPr = this->ConfigSect().ValueFloat( ConfigStr::Solver::Physics::REF_PRANDTL );

	cout << endl << "Re = " << mRefRe << endl;
	cout << "Prandtl = " << mRefPr << endl << endl;

	// parent class Init()
	PhysicsCommon<DIM,EQN_RANS_2E>::Init();
}


template <Geom::Dimension DIM>
void Physics<DIM, EQN_RANS_2E>::ApplyBC(FVec& vecout, AVec& avecout, const FVec& vecin, const AVec& avecin, const MGSize& surfid, const GVec& vpos, const GVec& vnn, const MGFloat& dy, const typename PhysicsCommon<DIM,EQN_RANS_2E>::PrdBCBase* pbc, const MGSize& nodeid) const
{
	MGFloat	vmod, vvmod;
	GVec	vt, vv, vvn;

	BC	bc = this->cBC( surfid);
	MGSize	varid = this->cVarId( surfid);


	switch ( bc)
	{
	case BC_VISCWALL:
		{
			vecout = EqnEuler<DIM>::ConservToSimp( vecin );

			for ( MGSize k=0; k<DIM; ++k)
				vecout(2+k) *= 1.0e-10;
				//vecout(2+k) = 0;
				//vecout(2+k) = -vecin(2+k);

			//for ( MGSize k=0; k<DIM; ++k)
			//	vv.rX((MGInt)k) = vecin(2+k);
			//vecout(1) += 0.5*vecout(0)*(vv*vv);

			vecout = EqnEuler<DIM>::SimpToConserv( vecout);

			break; 
		}
	case BC_SYMMETRY:
	case BC_INVISCWALL:
		{
			for ( MGSize k=0; k<DIM; ++k)
				vv.rX((MGInt)k) = vecin(2+k);

			vmod = vv.module();
			vvn = (vv*vnn)*vnn;
			vv = vv - vvn;

			vvmod = vv.module();
			if ( vvmod > ZERO )
				vv *= vmod / vvmod;


			vecout(0) = vecin(0);
			vecout(1) = vecin(1);

			for ( MGSize k=0; k<DIM; ++k)
				vecout(2+k) = vv.cX( (MGInt)k );

			break;
		}
	case BC_FARFIELD:
		{
			vecout = this->cDefVar( 0);
			break;
		}
	case BC_EXTRAPOL:
		{
			vecout = vecin;
			break;
		}
	case BC_PERIODIC:
	{
			FVec tmp;
			pbc->GetPeriodicVar(nodeid, tmp);
			for (MGSize i = 0; i < ESIZE; ++i)
			{
				vecout(i) = tmp(i);
			}
			break;
	}
	case BC_DEF:
		{
			vecout = this->cDefVar( varid);
			break;
		}
	default:
		{
			char sbuf[1024];
			sprintf( sbuf, "BC not implemented! %d", bc);
			THROW_INTERNAL( sbuf);
		}
	}

}


//////////////////////////////////////////////////////////////////////
template class Physics<Geom::DIM_2D, EQN_RANS_2E>;
template class Physics<Geom::DIM_3D, EQN_RANS_2E>;

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

