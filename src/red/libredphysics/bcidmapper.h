#ifndef __BCIDMAPPER_H__
#define __BCIDMAPPER_H__


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Dimension DIM>
class BCIdMapper
{
public:
	static MGSize	Id( const MGSize& id);
};

template <>
inline MGSize BCIdMapper<DIM_2D>:: Id( const MGSize& id)
{
	switch ( id)
	{
	case 0:
		return 2;
	case 1:
		return 1;
	}

	return 9999;
}

template <>
inline MGSize BCIdMapper<DIM_3D>:: Id( const MGSize& id)
{
	switch ( id)
	{
	case 0:
		return 2;
	case 1:
		return 3;
	case 2:
		return 1;
	}

	return 9999;
}



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __BCIDMAPPER_H__
