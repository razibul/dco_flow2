#include "physics.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
//	class Physics for Advection
//////////////////////////////////////////////////////////////////////

template <Geom::Dimension DIM>
void Physics<DIM,EQN_ADVECT>::Init()
{
}

template <Geom::Dimension DIM>
void Physics<DIM, EQN_ADVECT>::ApplyBC(FVec& vecout, AVec& avecout, const FVec& vecin, const AVec& avecin, const MGSize& surfid, const GVec& vpos, const GVec& vnn, const MGFloat& dy, const typename PhysicsCommon<DIM,EQN_ADVECT>::PrdBCBase* pbc, const MGSize& nodeid) const
{
}

//////////////////////////////////////////////////////////////////////
template class Physics<Geom::DIM_2D, EQN_ADVECT>;
template class Physics<Geom::DIM_3D, EQN_ADVECT>;

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

