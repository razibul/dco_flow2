#ifndef __POISSONPHYSICS_H__
#define __POISSONPHYSICS_H__

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace RED {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
//	class Physics for Advection
//////////////////////////////////////////////////////////////////////
template <Geom::Dimension DIM>
class Physics<DIM,EQN_POISSON> : public PhysicsCommon<DIM,EQN_POISSON>
{
	enum { ESIZE = PhysicsCommon<DIM,EQN_POISSON>::ESIZE };

	typedef typename PhysicsCommon<DIM,EQN_POISSON>::EqDef	EqDef;
	typedef typename PhysicsCommon<DIM,EQN_POISSON>::FVec	FVec;
	typedef typename PhysicsCommon<DIM,EQN_POISSON>::AVec	AVec;
	typedef typename PhysicsCommon<DIM,EQN_POISSON>::GVec	GVec;

public:
	Physics()	: PhysicsCommon<DIM,EQN_POISSON>()	{}

	virtual void Create( const CfgSection* pcfgsec);
	virtual void PostCreateCheck() const;
	virtual void Init();

	void	ApplyBC(FVec& vecout, AVec& avecout, const FVec& vecin, const AVec& avecin, const MGSize& surfid, const GVec& vpos, const GVec& vnn, const MGFloat& dy, const typename PhysicsCommon<DIM,EQN_POISSON>::PrdBCBase* pbc, const MGSize& nodeid) const;

	template <class VECTOR>
	bool	IsPhysical( const VECTOR& vec);

	bool			IsStrongBC( const MGSize& id) const;
	bool			IsEqStrongBC( const MGSize& id, const MGSize& ie) const;

	MGFloat				MaxSpeed( const FVec& vec) const	{ return 1.0; }
	const MGFloat&		cRefLength() const					{ return mRefLength;}
	const MGFloat&		cLapC() const						{ return mcoeffLapC;}
	const MGFloat&		cLapPhi() const						{ return mcoeffLapPhi;}
	const MGFloat&		cConv() const						{ return mcoeffConv;}

protected:
	void	InitFVec( FVec& vec, const CfgSection& sec);

private:
	MGFloat		mRefLength;
	MGFloat		mcoeffLapC;
	MGFloat		mcoeffLapPhi;
	MGFloat		mcoeffConv;
};
//////////////////////////////////////////////////////////////////////


template <Geom::Dimension DIM>
inline void Physics<DIM,EQN_POISSON>::Create( const CfgSection* pcfgsec)		
{ 
	PhysicsCommon<DIM,EQN_POISSON>::Create( pcfgsec);
}


template <Geom::Dimension DIM>
inline void Physics<DIM,EQN_POISSON>::PostCreateCheck() const
{ 
	PhysicsCommon<DIM,EQN_POISSON>::PostCreateCheck();

	// do check
}

template <Geom::Dimension DIM>
template <class VECTOR>
bool Physics<DIM,EQN_POISSON>::IsPhysical( const VECTOR& vec)
{
	return true;
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace RED
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __POISSONPHYSICS_H__
