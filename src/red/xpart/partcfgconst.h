#ifndef __PARTCFGCONST_H__
#define __PARTCFGCONST_H__

#include "libcoresystem/mgdecl.h"

namespace CfgConst
{
	namespace Part
	{
		const char NAME[]     = "PART";
		namespace Dim
		{
			const char KEY[] = "PART_DIM";
			namespace Value
			{
				const char	DIM_NONE[] = "NONE";
				const char	DIM_0D[] = "0D";
				const char	DIM_1D[] = "1D";
				const char	DIM_2D[] = "2D";
				const char	DIM_3D[] = "3D";
				const char	DIM_4D[] = "4D";
			}
		}

		const char NPROC[]    = "PART_NPROC";
		const char GRIDNAME[] = "PART_FGRID";

		namespace SOLUTION
		{
			const char KEY[] = "SOLUTION";
		}

		namespace PERIODIC
		{
			const char NAME[]   = "DATA";
			//const char DATA[] = "DATA";

			//namespace Type
			//{
			//	const char KEY[] = "PRD_TYPE";
			//	namespace Value
			//	{
			//		const char TRANS[] = "PRD_TRANS";
			//		const char ANGLE[] = "PRD_ANGLE";
			//	}
			//}

			//const char SRC_ID[]     = "PRD_SOURCE_ID";
			//const char TRG_ID[]     = "PRD_TARGET_ID";
			//const char VCT_X[]      = "PRD_VCT_X";
			//const char VCT_Y[]      = "PRD_VCT_Y";
			//const char VCT_Z[]      = "PRD_VCT_Z";
			//const char ANGLE[]      = "PRD_ANGLE";
			//const char ANGLE_AXIS[] = "PRD_ANGLE_AXIS";
		}
	}
}

#endif // __PARTCFGCONST_H__
