
#include "partitioner.h"

#include "libcoreio/writetec.h"
#include "libcoreio/writemsh2.h"
#include "libcoreio/writeintf.h"
#include "libcoreio/writetecsurf.h"

#include "libhfgeom/hf_typedef.h"
#include "libhfgeom/hf_intersection.h"

//#include "partcfgconst.h"
#include "libredcore/configconst.h"
#include "libcorecommon/progressbar.h"

extern "C"
{
	void METIS_PartMeshNodal(int *, int *, idxtype *, int *, int *, int *, int *, idxtype *, idxtype *);
	void METIS_PartMeshDual(int *, int *, idxtype *, int *, int *, int *, int *, idxtype *, idxtype *);
}

template <Dimension DIM>
void Partitioner<DIM>::Init(const CfgSection* config)
{
	mpconfig = config;
	ReadConfig();
}


template <Dimension DIM>
void Partitioner<DIM>::CreatePartions( const MGSize& nproc)
{
	int ne = mGrid.SizeCellTab();
	int nn = mGrid.SizeNodeTab();
	int	etype;
	int	numflag = 0;
	int npart = nproc;
	int edgecut;

	if ( DIM == DIM_2D )
		etype = 1;
	else if ( DIM == DIM_3D )
		etype = 2;
	else
		THROW_INTERNAL( "bad dimesion inside Partitioner<DIM>::CreatePartions");
	
	vector<idxtype>	tabElmnts( (DIM+1) * ne );
	vector<idxtype>	tabEPart( ne );
	mtabNPart.resize( nn );

	for ( MGSize i=0; i<(MGSize)ne; ++i)
	{
		RED::Cell<DIM,DIM+1> cell = mGrid.cCell( i);

		for ( MGSize k=0; k<(DIM+1); ++k)
			tabElmnts[ i*(DIM+1) + k ] = cell.cId( k);
	}

	cout << "before calling METIS\n";

	METIS_PartMeshNodal( &ne, &nn, &tabElmnts[0], &etype, &numflag, &npart, &edgecut, &tabEPart[0], &mtabNPart[0] );
	//METIS_PartMeshDual( &ne, &nn, &tabElmnts[0], &etype, &numflag, &npart, &edgecut, &tabEPart[0], &mtabNPart[0] );
	cout << "after calling METIS\n";


	// creating new grids
	mtabGrid.resize( nproc, PartGrid<DIM>( mGrid.Name()) );

	// redistributing cells
	for ( MGSize i=0; i<(MGSize)ne; ++i)
	{
		const RED::Cell<DIM,DIM+1>& cell = mGrid.cCell( i);

		vector<MGSize> tab;
		for ( MGSize k=0; k<(DIM+1); ++k)
			tab.push_back( mtabNPart[cell.cId( k)] );

		sort( tab.begin(), tab.end() );
		vector<MGSize>::iterator new_end = unique( tab.begin(), tab.end() );
		vector<MGSize>::iterator last = new_end;
		--last;

		for ( vector<MGSize>::iterator itr=tab.begin(); itr!=new_end; ++itr)
		{
			if ( last == tab.begin() )
				mtabGrid[*itr].PushBackInCell( cell);
			else
				mtabGrid[*itr].PushBackExCell( cell);
		}

		if ( last != tab.begin() )
			mtabInterf.push_back( cell);
	}


	cout << "interf size = " << mtabInterf.size() << endl;
	cout << "CELLS" << endl;
	for ( MGSize i=0; i<mtabGrid.size(); ++i)
	{
		cout << "proc = " << i+1  << endl;
		cout << " " << mtabGrid[i].SizeCellTab() << endl;
		cout << " " << mtabGrid[i].SizeInCellTab() << endl;
		cout << " " << mtabGrid[i].SizeExCellTab() << endl;
	}
	
	// redistributing boundary faces
	for ( MGSize i=0; i<mGrid.SizeBFaceTab(); ++i)
	{
		RED::BFace<DIM,DIM> face = mGrid.cBFace( i);

		vector<MGSize> tab;
		for ( MGSize k=0; k<DIM; ++k)
			tab.push_back( mtabNPart[face.cId( k)] );

		sort( tab.begin(), tab.end() );
		vector<MGSize>::iterator new_end = unique( tab.begin(), tab.end() );

		for (vector<MGSize>::iterator itr = tab.begin(); itr != new_end; ++itr)
		{
			mtabGrid[*itr].PushBackBFace(face);
		}
	}

	CreatePeriodicInterface();

	cout << "BFACES" << endl;
	for ( MGSize i=0; i<mtabGrid.size(); ++i)
		cout << i+1 << " " << mtabGrid[i].SizeBFaceTab() << endl;

	CreateInterface();
	

	for ( MGSize i=0; i<mtabGrid.size(); ++i)
	{
		mtabGrid[i].SetupNodes( mGrid);
		//mtabGrid[i].CreateNodeMap();
		mtabGrid[i].ResetCellIds();
		mtabGrid[i].ResetBFaceIds();
	}

	// node map must be ready for every grid before calling UpdateInterface
	for ( MGSize i=0; i<mtabGrid.size(); ++i)
	{
		mtabGrid[i].rNProc() = nproc;
		mtabGrid[i].UpdateInterface( mtabGrid );
	}
}

template<Dimension DIM>
template<MGSize PRDTYPE>
//void Partitioner<DIM>::FindPeriodicBFaces(const MGSize& srcid, const MGSize& trgid, const Geom::Vect<DIM>& shift)
void Partitioner<DIM>::FindPeriodicBFaces(const RED::PeriodicData& data, const MGSize& setid)
{
	typedef HFGeom::GVector<MGFloat, DIM> HFVector;

	ProgressBar bar(40);
	bar.Init(mGrid.SizeBFaceTab());
	bar.Start();
	for (MGSize inx = 0; inx < mGrid.SizeBFaceTab(); ++inx, ++bar)
	{
		const RED::BFace<DIM, DIM>& bface = mGrid.cBFace(inx);

		if (bface.cSurfId() == data.src)
		{
			for (MGSize trginx = 0; trginx < mGrid.SizeBFaceTab(); ++trginx)
			{
				const RED::BFace<DIM, DIM>& bfacetrg = mGrid.cBFace(trginx);

				if (bfacetrg.cSurfId() == data.trg)
				{
					HFVector tabpos[DIM];
					for (MGSize i = 0; i < DIM; ++i)
					{
						const MGSize& trgnodeid = bfacetrg.cId(i);
						for (MGSize j = 0; j < DIM; ++j)
						{
							tabpos[i].rX(j) = mGrid.cNode(trgnodeid).cX(j);
						}
					}

					MGFloat maxedgesize = 0.;
					for (MGSize i = 0; i < DIM - 1; ++i)
					{
						MGFloat len = 0.;
						HFVector tmp(tabpos[i] - tabpos[(i + 1) % DIM]);
						for (MGSize ii = 0; ii < DIM; ++ii)
						{
							len += tmp.cX(ii)*tmp.cX(ii);
						}
						len = sqrt(len);
						if (len > maxedgesize)
						{
							maxedgesize = len;
						}
					}

					HFGeom::SubSimplex<MGFloat, DIM - 1, DIM> subb((typename HFGeom::SubSimplex<MGFloat, DIM - 1, DIM>::ARRAY(tabpos)));

					for (MGSize i = 0; i < bface.Size(); ++i)
					{
						HFVector pos;
						Vect<DIM> tmppos;
						const MGSize& nodeid = bface.cId(i);

						RED::PeriodicOp<DIM>::ApplyPrd(data, mGrid.cNode(nodeid), tmppos, RED::Int2Type<PRDTYPE>());
						for (MGSize ii = 0; ii < DIM; ++ii)
						{
							pos.rX(ii) = tmppos.cX(ii);
						}
						//Vector pos;
						//const MGSize& nodeid = bface.cId(i);

						//for (MGSize j = 0; j < DIM; j++)
						//{
						//	pos.rX(j) = mGrid.cNode(nodeid).cX(j) + shift.cX(j);
						//}



						HFGeom::SubSimplex<MGFloat, 0, DIM> suba((typename  HFGeom::SubSimplex<MGFloat, 0, DIM>::ARRAY(pos)));

						HFGeom::IntersectionProx<MGFloat, MGFloat, 0, DIM - 1, DIM> inter(suba, subb);
						inter.Execute();

						MGFloat	tabB[DIM];
						inter.GetBBaryCoords(tabB);

						bool bin = true;
						for (MGSize i = 0; i<DIM; ++i)
						{
							tabB[i] /= inter.cDet();
							if (data.isabstol)
							{
								if (tabB[i] < -data.tol)
								{
									bin = false;
								}
							}
							else
							{
								if (tabB[i] < -maxedgesize*data.tol)
								{
									bin = false;
								}
							}
							//cout << tabB[i] << "\t";
							//if (tabB[i] < -maxedgesize*1e-4)
							//{
							//	bin = false;
							//}
						}
						if (bin)
						{
							//PeriodicKey<DIM>& key = mtabperiodicdata[nodeid];
							PeriodicKey<DIM> key;
						
							key.mtrgcellid = trginx;
							key.mcolcellid.push_back(inx);
							for (MGSize j = 0; j < DIM; ++j)
							{
								key.mbcoord.rElem(j) = tabB[j];
							}
							
							mtabperiodicdata.push_back(Make_KeyData(nodeid, key));

							//if (fabs(mGrid.cNode(nodeid).cX() - 2.30865668867)<1e-10 &&
							//	fabs(mGrid.cNode(nodeid).cY() - 2.75987697028)<1e-10 &&
							//	fabs(mGrid.cNode(nodeid).cZ() - 6)<1e-10
							//	)
							//{
							//	cout << "Hit: " << nodeid << endl;
							//}

						}
					}
				}
			}
		}
	}

	bar.Finish();
	//sort(mtabperiodicdata.begin(), mtabperiodicdata.end());
	//typename vector< KeyData<MGSize, PeriodicData<DIM> > >::iterator itr = unique(mtabperiodicdata.begin(), mtabperiodicdata.end());
	//mtabperiodicdata.erase(itr, mtabperiodicdata.end());
}


template<Dimension DIM>
void Partitioner<DIM>::UniquePeriodicMap()
{
	if (mtabperiodicdata.empty()) return;
	sort(mtabperiodicdata.begin(), mtabperiodicdata.end());

	typedef vector< KeyData<MGSize, PeriodicKey<DIM> > > ColPeriodic;

	ColPeriodic newperiodic;
	typename ColPeriodic::iterator itr = mtabperiodicdata.begin() + 1;
	typename ColPeriodic::iterator olditr = mtabperiodicdata.begin();

	typename ColPeriodic::iterator bestitr = mtabperiodicdata.begin();

	vector<MGSize>	tabneighbcell;
	tabneighbcell.push_back(bestitr->second.mcolcellid[0]);

	ProgressBar bar(40);
	bar.Init(mtabperiodicdata.size());
	bar.Start();

	while (itr != mtabperiodicdata.end())
	{
		//if (itr->first == 1558)
		//{
		//	cout << "break";
		//}
		if (itr->first == olditr->first)
		{
			tabneighbcell.push_back(itr->second.mcolcellid[0]);
			vector<KeyData<MGFloat, MGSize> > 	tab(2 * DIM);
			for (MGSize i = 0; i < DIM; ++i)
			{
				tab[i].first = bestitr->second.mbcoord.rElem(i);
				tab[i].second = 0;

				tab[DIM + i].first = itr->second.mbcoord.rElem(i);
				tab[DIM + 1].second = 1;
			}
			sort(tab.begin(), tab.end());

			if (tab[0].second == 0)
			{
				bestitr = itr;
			}
			olditr = itr;
			if (++itr == mtabperiodicdata.end())
			{
				bestitr->second.mcolcellid.swap(tabneighbcell);
				newperiodic.push_back(*bestitr);
			}
		}
		else
		{
			bestitr->second.mcolcellid.swap(tabneighbcell);
			tabneighbcell.clear();
			newperiodic.push_back(*bestitr);

			bestitr = itr;
			olditr = itr;
			++itr;
		}
		++bar;
	}
	bar.Finish();
	mtabperiodicdata.swap(newperiodic);
}

template<Dimension DIM>
void Partitioner<DIM>::ReadConfig()
{
	using namespace RED::ConfigStr::Solver::Physics;

	MGSize prdsize = mpconfig->GetSectionCount(PERIODIC::NAME);

	for (MGSize i = 0; i < prdsize; ++i)
	{
		const CfgSection& secprd = mpconfig->GetSection(PERIODIC::NAME, i);
		MGSize srcid = secprd.ValueSize(PERIODIC::PRD_SRC_ID);
		MGSize trgid = secprd.ValueSize(PERIODIC::PRD_TRG_ID);
		MGString type = secprd.ValueString(PERIODIC::Type::KEY);

		RED::PrdBCType prdtype = RED::StrToPrd(type);
		switch (prdtype)
		{
		case RED::PRD_TRANS:
		{
			vector<MGFloat> tmp = secprd.ValueVectorFLOAT(PERIODIC::PRD_TRANS_VCT);
			Geom::Vect3D vct;
			for (MGSize j = 0; j < DIM; ++j)
			{
				vct.rX(j) = tmp[j];
			}

			mparams.push_back(RED::PeriodicData(srcid, trgid, vct, Geom::Vect3D(), 0., prdtype));

			break;
		}
		case RED::PRD_ANGLE:
		{
			vector<MGFloat> tmporg = secprd.ValueVectorFLOAT(PERIODIC::PRD_AXIS_ORIG);
			Geom::Vect3D axisorig;
			for (MGSize j = 0; j < Geom::DIM_3D; ++j)
			{
				axisorig.rX(j) = tmporg[j];
			}
			vector<MGFloat> tmpdir = secprd.ValueVectorFLOAT(PERIODIC::PRD_AXIS_DIR);
			Geom::Vect3D axisdir;
			for (MGSize j = 0; j < Geom::DIM_3D; ++j)
			{
				axisdir.rX(j) = tmpdir[j];
			}
			MGFloat angle = secprd.ValueFloat(PERIODIC::PRD_ALPHA);
			mparams.push_back(RED::PeriodicData(srcid, trgid, axisdir, axisorig, angle, prdtype));
			break;
		}
		case RED::PRD_NONE:
			//THROW_INTERNAL("Angle periodic bc not implemented.");
			break;
		default:
			THROW_INTERNAL("Unknown periodic bc type.");
			break;
		}

		bool _isabstol = secprd.KeyExists(PERIODIC::ABSTOL);
		MGFloat _tol;
		if (_isabstol)
		{
			_tol = secprd.ValueFloat(PERIODIC::ABSTOL);
		}
		else
		{
			_tol = secprd.ValueFloat(PERIODIC::RELEDGETOL);
		}
		mparams.back().isabstol = _isabstol;
		mparams.back().tol = _tol;
	}
}

template<Dimension DIM>
void Partitioner<DIM>::CreatePeriodicInterface()
{
	//using namespace CfgConst::Part;
	using namespace RED::ConfigStr::Solver::Physics;

	cout << "Creating periodic bc map" << endl;
	for (MGSize i = 0; i < this->mparams.size(); ++i)
	{
		switch (this->mparams[i].type)
		{
		case RED::PRD_TRANS:
		{
			typename RED::PeriodicData data = this->mparams[i];
			FindPeriodicBFaces<RED::PRD_TRANS>(data, i);
			data.vct *= -1.;
			swap(data.src, data.trg);
			FindPeriodicBFaces<RED::PRD_TRANS>(data, i);

			break;
		}
		case RED::PRD_ANGLE:
		{
			typename RED::PeriodicData data = this->mparams[i];
			FindPeriodicBFaces<RED::PRD_ANGLE>(data, i);
			data.angle *= -1.;
			swap(data.src, data.trg);
			FindPeriodicBFaces<RED::PRD_ANGLE>(data, i);

			break;
		}
		case RED::PRD_NONE:
			//THROW_INTERNAL("Angle periodic bc not implemented.");
			break;
		default:
			THROW_INTERNAL("Unknown periodic bc type.");
			break;
		}
	}

	cout << "Found: " << mtabperiodicdata.size() << endl;
	UniquePeriodicMap();
	cout << "After unique: " << mtabperiodicdata.size() << endl;
	//MGSize nprd = mpconfig->GetSectionCount(PERIODIC::DATA);

	//for (MGSize i = 0; i < nprd; ++i)
	//{
	//	const CfgSection& data = mpconfig->GetSection(PERIODIC::DATA, i);
	//	MGSize srcsurfid = data.ValueSize(PERIODIC::SRC_ID);
	//	MGSize trgsurfid = data.ValueSize(PERIODIC::TRG_ID);

	//	MGFloat trans[] = {
	//		data.ValueFloat(PERIODIC::VCT_X),
	//		data.ValueFloat(PERIODIC::VCT_Y),
	//		data.ValueFloat(PERIODIC::VCT_Z)
	//	};
	//	Geom::Vect<DIM> shift;
	//	for (MGSize j = 0; j < DIM; ++j)
	//	{
	//		shift.rX(j) = trans[j];
	//	}
	//	FindPeriodicBFaces(srcsurfid, trgsurfid, shift);
	//	shift *= -1.;
	//	FindPeriodicBFaces(trgsurfid, srcsurfid, shift);
	//}






	//ofstream off("prddata_dump.txt");
	//for (typename map<MGSize, PeriodicData<DIM> >::const_iterator it = mtabperiodicdata.begin();
	//	it != mtabperiodicdata.end();
	//	++it)
	//{
	//	off << it->first << "\t" << it->second.mtrgcellid;
	//	for (MGSize j = 0; j < DIM; ++j)
	//	{
	//		off << "\t" << it->second.mbcoord.cElem(j);
	//	}
	//	off << endl;
	//}
	//off.close();
	vector< set< Key<DIM> > > tabbfacekey;

	//Key<DIM> tmpkey;
	//for (MGSize j = 0; j < DIM; ++j)
	//{
	//	tmpkey.rElem(j) = mGrid.cBFace(19308).cId(j);
	//}
	//tmpkey.Sort();


	tabbfacekey.resize(mtabGrid.size());
	for (MGSize i = 0; i < mtabGrid.size(); ++i)
	{
		for (MGSize k = 0; k < mtabGrid[i].SizeBFaceTab(); ++k)
		{
			Key<DIM> key;
			for (MGSize j = 0; j < DIM; ++j)
			{
				key.rElem(j) = mtabGrid[i].cBFace(k).cId(j);
			}
			key.Sort();

			tabbfacekey[i].insert(key);

			//if (tmpkey == key)
			//{
			//	cout << "Hit!!!: proc: "<< i << endl;
			//}
		}
		//sort(tabbfacekey[i].begin(), tabbfacekey[i].end());
	}

	//for (typename map<MGSize, PeriodicKey<DIM> >::const_iterator it = mtabperiodicdata.begin();
	//	it != mtabperiodicdata.end();
	//	++it)

	//for (MGSize i = 0; i < mtabperiodicdata.size(); ++i)
	for (typename vector< KeyData<MGSize, PeriodicKey<DIM> > >::const_iterator it = mtabperiodicdata.begin();
		it != mtabperiodicdata.end();
		++it)
	{
		//const RED::BFace<DIM, DIM>& trgface = mGrid.cBFace(mtabperiodicdata[i].second.mtrgcellid);
		//const MGSize nodeid = mtabperiodicdata[i].first;
		//const RED::BFace<DIM, DIM>& face = mGrid.cBFace(mtabperiodicdata[i].second.mcellid);
		const RED::BFace<DIM, DIM>& trgface = mGrid.cBFace(it->second.mtrgcellid);
		const MGSize nodeid = it->first;

		Key<DIM> key;
		for (MGSize j = 0; j < DIM; ++j)
		{
			key.rElem(j) = trgface.cId(j);
		}
		key.Sort();
		
		vector<MGSize> vctkey;
		for (MGSize k = 0; k < it->second.mcolcellid.size(); ++k)
		{
			const RED::BFace<DIM, DIM>& face = mGrid.cBFace(it->second.mcolcellid[k]);
			for (MGSize j = 0; j < DIM; ++j)
			{
				vctkey.push_back(face.cId(j));
			}
		}
		sort(vctkey.begin(), vctkey.end());
		vector<MGSize>::iterator iter = unique(vctkey.begin(), vctkey.end());
		vctkey.erase(iter, vctkey.end());

		for (MGSize j = 0; j < mtabGrid.size(); ++j)
		{
			MGSize ownednodes = count_if(
				vctkey.begin(),
				vctkey.end(),
				[&](const MGSize& id){ return mtabNPart[id] == j; });

			pair< typename set< Key<DIM> >::iterator, bool> ret;
			ret.second = false;
			if (ownednodes != 0)
				ret= tabbfacekey[j].insert(key);

			//bool isfound = binary_search(tabbfacekey[j].begin(), tabbfacekey[j].end(), key);
			//if (!isfound)
			



			//if ((ret.second)/* && (mtabNPart[nodeid] == j)*/ )

			//if (tmpkey == key)
			//{
			//	cout << "Hit!!!: proc: " << j << "\t" << ret.second << "\t" << ownednodes << "\t" << nodeid << "\t" << mtabNPart[nodeid] << endl << "\t";
			//	for (size_t k = 0; k < vctkey.size(); k++)
			//	{
			//		cout << vctkey[k] << "\t";
			//	}
			//	cout << endl;
			//}


			if ((ret.second) && (ownednodes > 0))
			{
				mtabGrid[j].PushBackExBFace(trgface);
				mtabbfaceInterf.push_back(trgface);

				for (MGSize k = 0; k < trgface.Size(); ++k)
				{
					pair<MGSize, MGSize>	par;
					MGSize idpcur = mtabNPart[trgface.cId(k)];

					par.first = trgface.cId(k);
					par.second = idpcur;

					mtabGrid[j].PushBackIntfNode(par);
				}

			}
		}
	}

	//typedef HFGeom::GVector<MGFloat, DIM> Vector;

	//vector< 
	//	KeyData<MGSize, RED::BFace<DIM, DIM> >
	//>	tabtrgbface, tabsrcbface;

	//for (MGSize i = 0; i < mtabGrid.size(); ++i)
	//{
	//	for (MGSize j = 0; j < mtabGrid[i].SizeBFaceTab(); ++j)
	//	{
	//		const RED::BFace<DIM, DIM>& face = mtabGrid[i].cBFace(j);
	//		if (face.cSurfId() == trgsurfid)
	//		{
	//			tabtrgbface.push_back(KeyData<MGSize, RED::BFace<DIM, DIM> >(i, face));
	//		}
	//		else if (face.cSurfId() == srcsurfid)
	//		{
	//			tabsrcbface.push_back(KeyData<MGSize, RED::BFace<DIM, DIM> >(i, face));
	//		}
	//	}
	//}

	//for (MGSize i = 0; i < tabsrcbface.size(); ++i)
	//{
	//	for (MGSize j = 0; j < tabtrgbface.size(); ++j)
	//	{
	//		Vector tabpos[DIM];
	//		for (MGSize ii = 0; ii < DIM; ++ii)
	//		{
	//			const MGSize& trgnodeid = tabtrgbface[j].second.cId(ii);
	//			for (MGSize jj = 0; jj < DIM; ++jj)
	//			{
	//				//tabpos[ii].rX(jj) = mtabGrid[tabtrgbface[j].first].cNode(trgnodeid).cX(jj);
	//				tabpos[ii].rX(jj) = mGrid.cNode(trgnodeid).cX(jj);
	//			}
	//		}

	//		MGFloat maxedgesize = 0.;
	//		for (MGSize i = 0; i < DIM - 1; ++i)
	//		{
	//			MGFloat len = 0.;
	//			Vector tmp(tabpos[i] - tabpos[(i + 1) % DIM]);
	//			for (MGSize ii = 0; ii < DIM; ++ii)
	//			{
	//				len += tmp.cX(ii)*tmp.cX(ii);
	//			}
	//			len = sqrt(len);
	//			if (len > maxedgesize)
	//			{
	//				maxedgesize = len;
	//			}
	//		}
	//		HFGeom::SubSimplex<MGFloat, DIM - 1, DIM> subb((typename HFGeom::SubSimplex<MGFloat, DIM - 1, DIM>::ARRAY(tabpos)));

	//		for (MGSize k = 0; k < tabsrcbface[i].second.Size(); ++k)
	//		{
	//			Vector pos;
	//			const MGSize& nodeid = tabsrcbface[i].second.cId(k);

	//			for (MGSize ii = 0; ii < DIM; ++ii)
	//			{
	//				//tabpos[ii].rX(jj) = mtabGrid[tabtrgbface[j].first].cNode(nodeid).cX(jj);
	//				pos.rX(ii) = mGrid.cNode(nodeid).cX(ii) + shift.cX(ii);
	//			}

	//			HFGeom::SubSimplex<MGFloat, 0, DIM> suba((typename  HFGeom::SubSimplex<MGFloat, 0, DIM>::ARRAY(pos)));

	//			HFGeom::IntersectionProx<MGFloat, MGFloat, 0, DIM - 1, DIM> inter(suba, subb);
	//			inter.Execute();

	//			MGFloat	tabB[DIM];
	//			inter.GetBBaryCoords(tabB);

	//			bool bin = true;
	//			for (MGSize i = 0; i<DIM; ++i)
	//			{
	//				tabB[i] /= inter.cDet();
	//				//cout << tabB[i] << "\t";
	//				if (tabB[i] < -maxedgesize*1e-4)
	//				{
	//					bin = false;
	//				}
	//			}

	//			if (bin)
	//			{
	//				cout << "Hit!: " << tabsrcbface[i].first << "\t" << tabtrgbface[j].first << endl;
	//				//PeriodicKey key;
	//				//key.first = nodeid;
	//				////key.second.first = trginx;
	//				//key.second.mcellid = trginx;
	//				//for (MGSize j = 0; j < DIM; ++j)
	//				//{
	//				//	//key.second.second.rElem(j) = tabB[j];
	//				//	key.second.mtabcoord.rElem(j) = tabB[j];
	//				//}

	//				//mcolperiodic.push_back(key);
	//			}
	//		}

	//	}
	//}
}

template <Dimension DIM>
void Partitioner<DIM>::CreateInterface()
{
	for ( MGSize i=0; i<mtabInterf.size(); ++i)
	{
		const RED::Cell<DIM,DIM+1>& cell = mtabInterf[i];

		for ( MGSize j=0; j<cell.Size(); ++j)
		{
			MGSize idproc = mtabNPart[ cell.cId(j) ];
			pair<MGSize,MGSize>	par;

			for ( MGSize k=0; k<cell.Size(); ++k)
			{
				MGSize idpcur = mtabNPart[ cell.cId(k) ];
				if ( k != j && idproc != idpcur )
				{
					par.first = cell.cId(k);
					par.second = idpcur;

					mtabGrid[idproc].PushBackIntfNode( par);
				}
			}
			

		}
	}
	//for (MGSize i = 0; i<mtabbfaceInterf.size(); ++i)
	//{
	//	const RED::BFace<DIM, DIM>& cell = mtabbfaceInterf[i];

	//	for (MGSize j = 0; j<cell.Size(); ++j)
	//	{
	//		MGSize idproc = mtabNPart[cell.cId(j)];
	//		pair<MGSize, MGSize>	par;

	//		for (MGSize k = 0; k<cell.Size(); ++k)
	//		{
	//			MGSize idpcur = mtabNPart[cell.cId(k)];
	//			if (k != j && idproc != idpcur)
	//			{
	//				par.first = cell.cId(k);
	//				par.second = idpcur;

	//				mtabGrid[idproc].PushBackIntfNode(par);
	//			}
	//		}


	//	}
	//}
}



template <Dimension DIM>
void Partitioner<DIM>::ExportGrids( const MGString& name)
{
	// msh2 files
	bool bbin = false;
	MGString sbuf = name;
	MGSize ind = sbuf.find_last_of('.');
	sbuf.erase( ind);

	for ( MGSize i=0; i<mtabGrid.size(); ++i)
	{
		ostringstream of;
		of << sbuf << "." << setfill('0') << setw(3) << i+1 << ".msh2";
		cout << of.str() << endl;


		ofstream ofile( of.str().c_str(), ios::out );;

		IO::WriteMSH2 writerMSH2( mtabGrid[i] );
		writerMSH2.DoWrite( ofile, bbin );

		IO::WriteINTF writerINTF( mtabGrid[i] );
		writerINTF.DoWrite( ofile, bbin);

		// solution
		const vector<PartSol>&	tabsol = mtabGrid[i].cTabSolution();
		for ( MGSize isol=0; isol<tabsol.size(); ++isol)
		{
			MGString sname;
			tabsol[isol].IOGetName( sname);
			MGString sext = sname.substr( sname.find_last_of("."));
			MGString score = sname.substr( 0, sname.find_last_of("."));

			ostringstream of;
			of << score << "." << setfill('0') << setw(3) << i+1 << sext;
			cout << of.str() << endl;

			IO::WriteSOL	solwriter( tabsol[isol] );
			solwriter.DoWrite( of.str().c_str(), bbin );
		}

		//if ( mtabGrid[i].cSolution().BlockSize() > 0 )
		//{
		//	ostringstream of;
		//	of << sbuf << "." << setfill('0') << setw(3) << i+1 << ".daux";
		//	cout << of.str() << endl;

		//	IO::WriteSOL	solwriter( mtabGrid[i].rSolution() );
		//	solwriter.DoWrite( of.str().c_str(), bbin );
		//}
	}

	// tecplot files
	for ( MGSize i=0; i<mtabGrid.size(); ++i)
	{
		ostringstream of;
		of << "out" << "." << setfill('0') << setw(3) << i+1 << ".dat";
		cout << of.str() << endl;

		//if ( mtabGrid[i].cSolution().BlockSize() > 0 )
		//{
		//	IO::WriteTEC writer( mtabGrid[i], &mtabGrid[i].cSolution() );
		//	writer.DoWrite( of.str() );
		//}
		//else
		{
			IO::WriteTEC writer( mtabGrid[i] );
			writer.DoWrite( of.str() );

			of.clear();
			of.str("");
			of << "out_surf" << "." << setfill('0') << setw(3) << i + 1 << ".dat";
			cout << of.str() << endl;

			IO::WriteTECSurf surfwriter(mtabGrid[i]);
			surfwriter.DoWrite(of.str());
		}
	}
}



template class Partitioner<DIM_2D>;
template class Partitioner<DIM_3D>;

