#include "libcoresystem/mgdecl.h"

#include "partgrid.h"
#include "partitioner.h"

#include "libcoreio/readmsh2.h"
#include "libcoreio/writemsh2.h"
#include "libcoreio/readsol.h"
#include "libcoreio/writesol.h"
#include "libcoreio/writetec.h"

#include "libcoreconfig/config.h"
#include "partcfgconst.h"

INIT_VERSION("0.1","''");

template <Dimension DIM>
void CreatePartitions(const MGSize& nproc, const bool& bBin, const MGString& name, const vector<MGString>& tabsolname, const CfgSection* cfg)
{
	PartGrid<DIM>		grid;
	vector<PartSol>&	tabsol = grid.rTabSolution();
	
	IO::ReadMSH2	reader( grid, "test");
	reader.DoRead( name, bBin);

	tabsol.resize( tabsolname.size() );
	for ( MGSize i=0; i<tabsolname.size(); ++i)
	{
		IO::ReadSOL	solreader( tabsol[i] );
		solreader.ReadHEADER( tabsolname[i], bBin );

		tabsol[i].SetDim( solreader.cDim() );
		tabsol[i].SetBlockSize( solreader.cBlockSize() );
		tabsol[i].Resize( solreader.cSize() );

		solreader.DoRead( tabsolname[i], bBin );

		tabsol[i].IOSetName( tabsolname[i]);
		//IO::WriteTEC	tecwriter( grid, &sol);
		//tecwriter.DoWrite( "part_out.dat");
	}

	//IO::WriteMSH2	writer( grid);
	//writer.DoWrite( "part_out.msh2", false);
	
	Partitioner<DIM>	part( grid);
	
	part.Init(cfg);
	part.CreatePartions( nproc);
	part.ExportGrids( name);
}



int main( int argc, char* argv[])
{
	try
	{
		if ( argc < 2 )
		{
			cout << "part [cfg_name]" << endl;
			return 1;
		}
		MGString cfgname = MGString(argv[1]);

		Config cfg;
		cfg.ReadCFG(cfgname);
		cfg.DumpCFG("_dump.cfg");

		const CfgSection& partcfg = cfg.GetSection(CfgConst::Part::NAME);

		MGSize nproc = partcfg.ValueSize(CfgConst::Part::NPROC);
		MGString name = partcfg.ValueString(CfgConst::Part::GRIDNAME);

		vector<MGString> tabsolname;

		if (partcfg.SectionExists(CfgConst::Part::SOLUTION::KEY))
		{
			CfgSection::const_iterator_rec	citrr;
			const CfgSection& solsec = partcfg.GetSection(CfgConst::Part::SOLUTION::KEY);
			for (citrr = solsec.begin_rec(); citrr != solsec.end_rec(); ++citrr)
			{
				tabsolname.push_back(MGString((*citrr).second));
			}
		}
		//for(cfg.GetSection(CfgConst::Part::NAME).GetSection(CfgConst::Part::SOLUTION::KEY).begin_sec()
		//istringstream is( argv[1]);
		//is >> nproc;

		//name = MGString( argv[2] );

		

		//if (ar)

		//for (MGSize i=4; i<(MGSize)argc; ++i)
		//	tabsolname.push_back( MGString( argv[i] ) );
		
		cout << "partitioner started for " << name << endl;
		cout << "nproc = " << nproc << endl;

		bool bBin = IO::ReadMSH2::IsBin( name);
		Dimension dim = IO::ReadMSH2::ReadDim( name, bBin);
		Dimension cfgdim = StrToDim( partcfg.ValueString(CfgConst::Part::Dim::KEY));
		if (dim != cfgdim)
		{
			THROW_INTERNAL("Config dim differs from mesh dim");
		}


		const CfgSection* pprdsec = &partcfg.GetSection(CfgConst::Part::PERIODIC::NAME);

		if ( dim == DIM_2D)
		{
			CreatePartitions<DIM_2D>(nproc, bBin, name, tabsolname, pprdsec);
		}
		else if ( dim == DIM_3D)
		{
			CreatePartitions<DIM_3D>(nproc, bBin, name, tabsolname, pprdsec);
		}
		else
			THROW_INTERNAL( "unknown dimesionality");
	
		
	}
	catch ( EHandler::Except& e)
	{
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);
	}
	catch( exception &e) 
	{
		TRACE( e.what() );
		fprintf( stderr, "%s\n", e.what() );
	}

	return 0;
}
