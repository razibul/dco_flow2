#ifndef __PARTITIONER_H__
#define __PARTITIONER_H__

#include "partgrid.h"
#include "libcorecommon/key.h"

#include "libcoreconfig/config.h"

#include "redvc/libredvccommon/periodicbcbase.h"

typedef int idxtype;

template<Dimension DIM>
struct PeriodicKey
{
	vector<MGSize>		mcolcellid;
	MGSize				mtrgcellid;
	Key<DIM, MGFloat>	mbcoord;
};

//////////////////////////////////////////////////////////////////////
// class PartGrid
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class Partitioner
{
public:
	Partitioner( const PartGrid<DIM>& grid) : mGrid( grid)	{}

	void Init(const CfgSection* config);
	void ReadConfig();

	void CreatePartions( const MGSize& nproc);
	
	void CreateInterface();

	void CreatePeriodicInterface();
	

	void ExportGrids( const MGString&name);

private:
	//void FindPeriodicBFaces(const MGSize& srcid, const MGSize& trgid, const Geom::Vect<DIM>& shift);
	template<MGSize PRDTYPE>
	void FindPeriodicBFaces(const RED::PeriodicData& data, const MGSize& setid);

	void UniquePeriodicMap();

	const PartGrid<DIM>& 		mGrid;
	
	vector< PartGrid<DIM> >			mtabGrid;
	
	vector< RED::Cell<DIM,DIM+1> >	mtabInterf;
	vector< RED::BFace<DIM, DIM> >	mtabbfaceInterf;

	vector<idxtype>					mtabNPart;

	vector< KeyData<MGSize, PeriodicKey<DIM> > > mtabperiodicdata;
	vector< RED::PeriodicData>		mparams;

	const CfgSection*					mpconfig;
};
//////////////////////////////////////////////////////////////////////


#endif // __PARTITIONER_H__

