#include "libcoresystem/mgdecl.h"

#include "partgrid.h"
#include "departitioner.h"

#include "libcoreio/readmsh2.h"
#include "libcoreio/writemsh2.h"
#include "libcoreio/readsol.h"
#include "libcoreio/writesol.h"
#include "libcoreio/writetec.h"



INIT_VERSION("0.1","''");

template <Dimension DIM>
void MergePartitions( const MGSize& nproc, const MGString& name, const vector<MGString>& tabsolname )
{
	DePartitioner<DIM>	depart( nproc);

	depart.InitGrids( name);

	if ( tabsolname.size() > 0 )
	{
		depart.SetWithSol();
		depart.InitSols( tabsolname);
	}

	depart.Merge();

	depart.ExportTec( "_out_tec.dat");
	depart.ExportGrid( name);

	depart.ExportSol( tabsolname);
}



int main( int argc, char* argv[])
{
	try
	{
		if ( argc < 3 )
		{
			cout << "depart [nproc] [msh2_name] [[sol_name] ...]" << endl;
			return 1;
		}
		
		MGSize nproc;
		MGString name;
		vector<MGString> tabsolname;

		istringstream is( argv[1]);
		is >> nproc;

		name = MGString( argv[2] );
		
		for (MGSize i=3; i<(MGSize)argc; ++i)
			tabsolname.push_back( MGString( argv[i] ) );
		
		
		cout << "departitioner started for " << name << endl;
		cout << "nproc = " << nproc << endl;

		MGString name0 = ModifyFileName( name, 0);
		bool bBin = IO::ReadMSH2::IsBin( name0);
		Dimension dim = IO::ReadMSH2::ReadDim( name0, bBin);

		if ( dim == DIM_2D)
		{
			MergePartitions<DIM_2D>( nproc, name, tabsolname);
		}
		else if ( dim == DIM_3D)
		{
			MergePartitions<DIM_3D>( nproc, name, tabsolname);
		}
		else
			THROW_INTERNAL( "unknown dimesionality");
	
		
	}
	catch ( EHandler::Except& e)
	{
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);
	}
	catch( exception &e) 
	{
		TRACE( e.what() );
		fprintf( stderr, "%s\n", e.what() );
	}

	return 0;
}
