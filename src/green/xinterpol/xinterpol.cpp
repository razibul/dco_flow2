#include "libcoresystem/mgdecl.h"

#include "libcoreio/readmsh2.h"
#include "libcoreio/writemsh2.h"
#include "libcoreio/writetec.h" 


//////////////////////////////////////////////////////////////////////
int main( int argc, char* argv[])
{
	try
	{

		//---------------------------------------------------------------------
	
		//IO::ReadMSH2	readMSH2( grid);
		//readMSH2.DoRead( "grid.msh2", false);

		//IO::WriteTEC	writeTEC( grid);
		//writeTEC.DoWrite( "out.dat");

		//---------------------------------------------------------------------

		return 0;

	}
	catch ( EHandler::Except& e)
	{
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);
	}
	catch( exception &e) 
	{
		TRACE( e.what() );
		cout << endl << "System exception: " << e.what() << endl;
	}
	catch( ...) 
	{
		TRACE( "Unknown exception thrown" );
		cout << endl << "Unknown exception thrown" << endl;
	}

	return 0;
} 
 