#ifndef __READMEANDR_H__
#define __READMEANDR_H__


#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"
#include "libgreen/gridcontext.h"
#include "libcoregeom/metric.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


using namespace Geom;

template <Dimension DIM>
class Grid;


//////////////////////////////////////////////////////////////////////
// class ReadMEANDR
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class ReadMEANDR
{
public:
	ReadMEANDR( Grid<DIM>& grd, GridContext<Metric<DIM> >& cxmet) : mGrid(grd), mMetricCx(cxmet)	{}

	void	DoRead( const MGString& fname);

private:
	Grid<DIM>&		mGrid;
	GridContext<Metric<DIM> >&	mMetricCx;
};
//////////////////////////////////////////////////////////////////////

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __READMEANDR_H__
