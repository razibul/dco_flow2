#include "grdcell.h"
#include "libgreen/grid.h"
#include "libgreen/gridgeom.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



template <> 
void Cell<DIM_2D>::Dump( const Grid<DIM_2D>& grd,  ofstream& f) const
{
	f << "CELL ID: " << cId() << endl;
	f << "CELL area: " << setprecision(12) << GridGeom<DIM_2D>::CreateSimplex( cId(), grd ).Volume() << endl;
	f << "NODES:\n";

	for ( MGSize i=0; i<SIZE; ++i)
	{
		f << "node " << i << "[" << cNodeId(i) << "] = " 
			<< setprecision(8) << grd.cNode( cNodeId(i)).cPos().cX() << " "
			<< setprecision(8) << grd.cNode( cNodeId(i)).cPos().cY() << endl;
	}

	f << "NEIGHBOURS:\n";
	for ( MGSize i=0; i<SIZE; ++i)
	{
		f << "cell " << i << " = cell_id: " << cCellId(i).first << "  | face_id: " << cCellId(i).second << endl;
	}

	f << endl;
} 




//template <> 
//MGFloat Cell<DIM_3D>::Volume( const Grid<DIM_3D>& grd) const
//{
//	return Geom::VolumeTET( grd.cNode( cNodeId(0)).cPos(), grd.cNode( cNodeId(1)).cPos(), 
//							grd.cNode( cNodeId(2)).cPos(), grd.cNode( cNodeId(3)).cPos() );
//}
//
//
//template <> 
//void Cell<DIM_3D>::GetCellCenter( GVect& vc, const Grid<DIM_3D>& grd) const
//{
//	vc = grd.cNode( cNodeId(0)).cPos();
//
//	for ( MGSize i=1; i<SIZE; ++i)
//		vc += grd.cNode( cNodeId(i)).cPos();
//
//	vc /= (MGFloat)( SIZE);
//} 


template <> 
void Cell<DIM_3D>::Dump( const Grid<DIM_3D>& grd,  ofstream& f) const
{
	f << "CELL ID: " << cId() << endl;
	f << "CELL volume: " << setprecision(12) << GridGeom<DIM_3D>::CreateSimplex( cId(), grd ).Volume() << endl;
	f << "NODES:\n";

	for ( MGSize i=0; i<SIZE; ++i)
	{
		f << "node " << i << "[" << cNodeId(i) << "] = " 
			<< setprecision(8) << grd.cNode( cNodeId(i)).cPos().cX() << " "
			<< setprecision(8) << grd.cNode( cNodeId(i)).cPos().cY() << " "
			<< setprecision(8) << grd.cNode( cNodeId(i)).cPos().cZ() << endl;
	}

	f << "NEIGHBOURS:\n";
	for ( MGSize i=0; i<SIZE; ++i)
	{
		f << "cell " << i << " = cell_id: " << cCellId(i).first << "  | face_id: " << MGSize(cCellId(i).second) << endl;
	}

	f << endl;
} 



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

