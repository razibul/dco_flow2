#include "cspaceconst.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



const char	CSP_END[]				= "END";
const char	CSP_HEADER[]			= "HEADER";
const char	CSP_GLOBAL[]			= "GLOBAL";

const char	CSP_GLOB_FILE[]			= "FILE";
const char	CSP_GLOB_FILESCALE[]	= "FILESCALE";
const char	CSP_GLOB_TECFILESCALE[]	= "TECFILESCALE";
const char	CSP_GLOB_UNIFORM[]		= "UNIFORM";
const char	CSP_GLOB_USERDEF[]		= "USERDEF";


const char	CSP_SEC_VERTEXSRC[]		= "SEC_VERTEXSRC";
const char	CSP_SEC_EDGESRC[]		= "SEC_EDGESRC";
const char	CSP_SEC_FACESRC[]		= "SEC_FACESRC";
const char	CSP_SEC_BREPSRC[]		= "SEC_BREPSRC";

const char	CSP_VERTEXSRC[]			= "VERTEXSRC";
const char	CSP_EDGESRC[]			= "EDGESRC";
const char	CSP_FACESRC[]			= "FACESRC";
const char	CSP_BREPSRC[]			= "BREPSRC";

const char	CSP_VRTX[]			= "VRTX";
const char	CSP_EDGE[]			= "EDGE";
const char	CSP_FACE[]			= "FACE";

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

