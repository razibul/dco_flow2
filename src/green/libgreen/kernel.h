#ifndef __KERNEL_H__
#define __KERNEL_H__

#include "libcoresystem/mgdecl.h"
#include "libgreen/tdefs.h"
#include "libgreen/localizator.h"
#include "libgreen/generatordata.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Dimension DIM> 
class Grid;

template <Dimension DIM> 
class Generator;

template <Dimension DIM> 
class EdgePipe;

//template <Dimension DIM>
//class GeneratorData;


template <Dimension DIM>
class Kernel
{
};


//////////////////////////////////////////////////////////////////////
// class Kernel<DIM_1D>
//////////////////////////////////////////////////////////////////////
template <>
class Kernel<DIM_1D>
{
	typedef TDefs<DIM_1D>::GVect	GVect;
	typedef TDefs<DIM_1D>::GMetric	GMetric;

	typedef TDefs<DIM_1D>::GNode	GNode;
	typedef TDefs<DIM_1D>::GCell	GCell;
	
	friend class Generator<DIM_1D>;

public:
	Kernel( Grid<DIM_1D>& grd, const GeneratorData<DIM_1D>& gdat) : mpGrd(&grd), mrGDat(gdat)	{}

	void	InitBoundingBox( const GVect& vmin, const GVect& vmax);
	void	InitCube();
	void	InitCube( const GVect& v1, const GVect& v2);

	MGSize	InsertPoint( const GVect& vct, const MGSize& icell, const GMetric& met);

private:
	const GeneratorData<DIM_1D>&	mrGDat;

	Box<DIM_1D>			mBox;

	Grid<DIM_1D>		*mpGrd;
};
//////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////
// class Kernel<DIM_2D>
//////////////////////////////////////////////////////////////////////
template <>
class Kernel<DIM_2D>
{
	typedef TDefs<DIM_2D>::GVect	GVect;
	typedef TDefs<DIM_2D>::GMetric	GMetric;

	typedef TDefs<DIM_2D>::GNode	GNode;
	typedef TDefs<DIM_2D>::GCell	GCell;
	typedef TDefs<DIM_2D>::GFace	GFace;
	
	friend class Generator<DIM_2D>;

public:
	Kernel( Grid<DIM_2D>& grd, const GeneratorData<DIM_2D>& gdat) : mpGrd(&grd), mLoc(&grd), mrGDat(gdat)	{}
	
	
//protected:

	void	InitBoundingBox( const GVect& vmin, const GVect& vmax);
	void	InitCube();


	MGSize	FindCell( const GVect& vct);
	
	bool	FindBall( vector<MGSize>& tabball, const MGSize& inod, const MGSize& icstart=0);

	bool	FindFacePipe( vector<MGSize>& tabcell, const Key<2>& edge);


	// finds cells defining the cavity for point vct
	bool	FindCavity( vector<MGSize>& tabcell, const MGSize& icell, const GVect& vct);

	// finds cells defining the cavity for point vct (using space with metric defined in Generator ??? )
	bool	FindCavityMetric( vector<MGSize>& tabcell, const MGSize& icell, const GVect& vct, const GMetric& met);

	// correction of the cavity to enforce star-shape properties (using painting)
	void	CavityCorrection( const vector<MGSize>& tabcell, const GVect& vct);

	// finds cavity boundary faces necessary for the point insertion
	bool	CavityBoundary( vector<GFace>& tabface, const vector<MGSize>& tabcell);


	MGSize	InsertPoint( const GVect& vct);
	MGSize	InsertPointMetric( const GVect& vct, const GMetric& met);

// operators	



	Grid<DIM_2D>&		rGrid()		{ return *mpGrd;}

private:

	MGSize	InsertPoint( vector<MGSize>& tabccell, vector<GFace>& tabcface, const GVect& vct);


private:
	const GeneratorData<DIM_2D>&	mrGDat;

	Grid<DIM_2D>		*mpGrd;

	Localizator<DIM_2D>	mLoc;
	Box<DIM_2D>			mBox;


	vector<GCell*>		tabstack;
	vector<GCell*>		tabcavcell;
};
//////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////
// class Kernel<DIM_3D>
//////////////////////////////////////////////////////////////////////
template <>
class Kernel<DIM_3D>
{
	typedef TDefs<DIM_3D>::GVect	GVect;
	typedef TDefs<DIM_3D>::GMetric	GMetric;

	typedef TDefs<DIM_3D>::GNode	GNode;
	typedef TDefs<DIM_3D>::GCell	GCell;
	typedef TDefs<DIM_3D>::GFace	GFace;
	
	friend class Generator<DIM_3D>;
	friend class Operator3D_RestoreEdge;

public:
	//Kernel( Grid<DIM_3D>& grd, const GeneratorData<DIM_3D>& gdat) : mpGrd(&grd), mLoc(&grd), mrGDat(gdat)	{}
	Kernel( Grid<DIM_3D>& grd, GeneratorData<DIM_3D>& gdat) : mpGrd(&grd), mLoc(&grd,35,50), mrGDat(gdat), mbUseTree(true)	{}
	
	
//protected:

	void	InitBoundingBox( const GVect& vmin, const GVect& vmax);
	void	InitCube();


	MGSize	FindCell( const GVect& vct);
	
	bool	FindBase( vector<MGSize>& tabbase, const GVect& vct, const MGSize& icstart=0);

	bool	FindBall( vector<MGSize>& tabball, const MGSize& inod, const MGSize& icstart=0);

	// tabnod must be sorted
	// ball must be connected
	bool	FindBall( vector<MGSize>& tabball, const vector<MGSize>& tabnod, const MGSize& icstart=0);
	
	bool	FindEdgeShell( vector<MGSize>& tabcell, const Key<2>& edge, const MGSize& icstart1=0, const MGSize& icstart2=0);

	bool	FindEdgePipe( vector<MGSize>& tabcell, const Key<2>& edge);


	bool	FindFaceShell( vector<MGSize>& tabcell, const Key<3>& face, const MGSize& icstart1=0);

	//bool	FindFacePipe( vector<MGSize>& tabcell, const Key<3>& face);
	bool	FindFacePipe( vector<MGSize>& tabcell, vector< Key<2> >& tabedge, const Key<3>& face, const MGSize& icell=0);


	bool	SetAllCellsModified( const bool& b=true);

	bool	FindModifiedCells( vector<MGSize>& tabcell);

	bool	FindBadCells( vector<MGSize>& tabcell, const MGFloat& thresh);

	bool	FindBadCellEdges( vector< Key<2> >& tabedge, const MGFloat& thresh);

	bool	FindBadEdges( vector< Key<2> >& tabedge, const MGFloat& coefMin, const MGFloat& coefMax);


	bool	CheckCellVolume( vector<GCell>& tabcell );		// returns true if all are positive 
	MGFloat	MinCellVolume( vector<GCell>& tabcell );
	MGFloat	CheckCellQuality( vector<GCell>& tabcell );		// returns max BETA


	//bool	FindCavity( vector<MGSize>& tabcell, vector<GFace>& tabface, 
	//					const MGSize& icell, const GVect& vct, const GMetric& met);


	// finds cells defining the cavity for point vct
	bool	FindCavity( vector<MGSize>& tabcell, const MGSize& icell, const GVect& vct);
	bool	FindCavityBase( vector<MGSize>& tabcell, const vector<MGSize>& tabbase, const MGSize& icell, const GVect& vct);
	bool	FindCavityProtect( vector<MGSize>& tabcell, const MGSize& icell, const GVect& vct, const set< Key<3> >& setf);

	// finds cells defining the cavity for point vct (using space with metric defined in Generator ??? )
	bool	FindCavityMetric( vector<MGSize>& tabcell, const MGSize& icell, const GVect& vct, const GMetric& met);
	bool	FindCavityMetricBase( vector<MGSize>& tabcell, const vector<MGSize>& tabbase, const MGSize& icell, const GVect& vct, const GMetric& met);
	bool	FindCavityMetricProtect( vector<MGSize>& tabcell, const MGSize& icell, const GVect& vct, const GMetric& met, const set< Key<3> >& setf);

	// correction of the cavity to enforce star-shape properties (using painting)
	void	CavityCorrection( const vector<MGSize>& tabcell, const GVect& vct, const bool& bmin=true);
	void	CavityCorrectionQ( const vector<MGSize>& tabcell, const GVect& vct, const bool& bmin=true);

	// finds cavity boundary faces necessary for the point insertion
	bool	CavityBoundary( vector<GFace>& tabface, vector<MGSize>& tabcell);


	MGSize	InsertPoint( const GVect& vct);
	MGSize	InsertPointMetric( const GVect& vct, const GMetric& met);
	MGSize	InsertPointProtect( const GVect& vct, const set< Key<2> >& sete, const set< Key<3> >& setf );
	MGSize	InsertPointMetricProtect( const GVect& vct, const GMetric& met, const set< Key<3> >& setf);
	
	void	EraseNode( const MGSize& id);

	MGSize	InsertCell( const GCell& cell);
	void	EraseCell( const MGSize& id);
	void	EraseCellSafe( const MGSize& id);

	void	InsertCellIntoTree( const MGSize& id);
	void	RemoveCellFromTree( const MGSize& id);

	void	FlagExternalCells( vector< Key<3> >& tabbndfac);

// operators	
	bool	Flip23( vector<MGSize>& tabcell);
	bool	Flip32( vector<MGSize>& tabcell);
	bool	Flip44( vector<MGSize>& tabcell, const Key<2>& edge);

	bool	OperatorOP1( const Key<2>& edge, const GVect& vct);		// insert point on the edge
	bool	OperatorOP2( const Key<3>& face, const GVect& vct);		// insert point on the face
	bool	OperatorOP3( const MGSize& cellid, const GVect& vct);	// insert point in the tet
	bool	OperatorOP5( const Key<2>& edge);						// remove the edge by swapping
	bool	OperatorOP6( const Key<3>& face);						// remove the face by swapping
	bool	OperatorOP7( const MGSize& nodeid, const GVect& dv);	// relocate point

	bool	CollapseEdge( const Key<2>& edge, const MGSize& inrm);

	MGSize	InsertFacePoint( vector<MGSize>& tabcell, const GVect& vct);


	void	InsertMetric( const GMetric& met, const MGSize& id)		{ mrGDat.InsertMetric( met, id); }

	Grid<DIM_3D>&				rGrid()				{ return *mpGrd;}
	const Grid<DIM_3D>&			cGrid() const		{ return *mpGrd;}

	GeneratorData<DIM_3D>&			rGenData()			{ return mrGDat; }
	const GeneratorData<DIM_3D>&	cGenData() const	{ return mrGDat; }

	const Localizator<DIM_3D>&	cLocal() const		{ return mLoc;}

	void	DisableTree()							{ mbUseTree = false; }


	const Box<DIM_3D>&			cBox() const		{ return mBox;}
	Box<DIM_3D>&				rBox()				{ return mBox;}
//private:

	void	CavityBoundaryProtectEdges( vector<GFace>& tabcface, vector<MGSize>& tabccell, const GVect& vct, const set< Key<2> >& sete);
	void	CavityBoundaryProtectFaces( vector<GFace>& tabcface, vector<MGSize>& tabccell, const GVect& vct, const set< Key<3> >& setf);

	MGSize	InsertPoint( vector<MGSize>& tabccell, vector<GFace>& tabcface, const GVect& vct);
	MGSize	InsertPoint( vector<MGSize>& tabnewcell, vector<MGSize>& tabccell, vector<GFace>& tabcface, const GVect& vct);


private:
	//const GeneratorData<DIM_3D>&	mrGDat;
	GeneratorData<DIM_3D>&	mrGDat;

	Grid<DIM_3D>		*mpGrd;

	bool				mbUseTree;
	Localizator<DIM_3D>	mLoc;
	Box<DIM_3D>			mBox;

	vector<GCell*>		tabstack;
	vector<GCell*>		tabcavcell;
};
//////////////////////////////////////////////////////////////////////


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __KERNEL_H__

