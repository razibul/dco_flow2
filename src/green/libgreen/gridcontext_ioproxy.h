#ifndef __GRIDCONTEXTIOPROXY_H__
#define __GRIDCONTEXTIOPROXY_H__


#include "libcoreio/solfacade.h"
#include "libgreen/gridcontext.h"
#include "libcorecommon/smatrix.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template< MGSize MAX_SIZE, class ELEM_TYPE=MGFloat>
class IOGreenCxSMtxProxy : public IO::SolFacade
{
public:
	IOGreenCxSMtxProxy( GridContext< SMatrix<MAX_SIZE,ELEM_TYPE> >* pcx) : mpContext(pcx)	{}


	virtual Dimension	Dim() const		{ return Dimension(MAX_SIZE);}

	virtual void	IOGetName( MGString& name) const		{ name = mName;}
	virtual void	IOSetName( const MGString& name)		{ mName = name;}

	virtual MGSize	IOSize() const							{ return mpContext->Size(); }
	virtual MGSize	IOBlockSize() const						{ return MAX_SIZE*MAX_SIZE;}

	virtual void	IOResize( const MGSize& n)				{ mpContext->Resize(n); }

	virtual void	IOGetBlockVector( vector<MGFloat>& tabx, const MGSize& id) const
		{
			const SMatrix<MAX_SIZE,ELEM_TYPE>& mtx = mpContext->cData(id+1);

			for ( MGSize i=0; i<MAX_SIZE*MAX_SIZE; ++i)
				tabx[i] = mtx[i];
		}

	virtual void	IOSetBlockVector( const vector<MGFloat>& tabx, const MGSize& id )
		{
			SMatrix<MAX_SIZE,ELEM_TYPE>& mtx = mpContext->rData(id+1);

			for ( MGSize i=0; i<MAX_SIZE*MAX_SIZE; ++i)
				mtx[i] = tabx[i];
		}

	virtual void	DimToUndim( vector<MGFloat>& tab ) const	{}
	virtual void	UndimToDim( vector<MGFloat>& tab ) const	{}


private:
	MGString			mName;
	GridContext< SMatrix<MAX_SIZE,ELEM_TYPE> >*		mpContext;
};




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __GRIDCONTEXTIOPROXY_H__
