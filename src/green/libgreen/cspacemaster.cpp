#include "cspacemaster.h"

#include "libgreen/controlspace.h"
#include "libgreen/cspaceuniform.h"
#include "libgreen/cspacegrid.h"

#include "libgreen/cspacegridglob.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 




template <Dimension DIM>
CSpaceMaster<DIM>::~CSpaceMaster()
{ 
	if ( mpGlobCSpace) 
		delete mpGlobCSpace;
}


template <Dimension DIM>
void CSpaceMaster<DIM>::InitGlobalCSpFileMEANDROS( const MGString& fname, const MGFloat& scale)
{
	//GridControlSpace<DIM,DIM> *pgrd = new GridControlSpace<DIM,DIM>();
	//pgrd->InitFileMEANDR( fname);
	//pgrd->ApplyScale( scale);
	//mpGlobCSpace = pgrd;

	GridGlobControlSpace<DIM> *pgrd = new GridGlobControlSpace<DIM>();
	pgrd->InitFileMEANDR( fname);
	pgrd->ApplyScale( scale);
	mpGlobCSpace = pgrd;
}


template <Dimension DIM>
void CSpaceMaster<DIM>::InitGlobalCSpFileTEC( const MGString& fname, const MGFloat& scale)
{
	GridGlobControlSpace<DIM> *pgrd = new GridGlobControlSpace<DIM>();
	pgrd->InitFileTEC( fname);
	pgrd->PostInitTEC();
	pgrd->ApplyScale( scale);
	mpGlobCSpace = pgrd;
}

template <Dimension DIM>
void CSpaceMaster<DIM>::InitGlobalCSpUniform( const MGFloat& hsize)
{
	GMetric met;
	mpGlobCSpace = new UniformControlSpace<DIM>(  hsize );
}



template class CSpaceMaster<DIM_2D>;
template class CSpaceMaster<DIM_3D>;

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

