#ifndef __OPERATOR3D_COLLAPSEEDGE_H__
#define __OPERATOR3D_COLLAPSEEDGE_H__

#include "libcoresystem/mgdecl.h"
#include "libgreen/tdefs.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Dimension DIM> 
class Grid;

template <Dimension DIM> 
class Kernel;


//////////////////////////////////////////////////////////////////////
// class Operator3D_CollapseEdge
//////////////////////////////////////////////////////////////////////
class Operator3D_CollapseEdge
{
	typedef TDefs<DIM_3D>::GVect	GVect;
	typedef TDefs<DIM_3D>::GMetric	GMetric;

	typedef TDefs<DIM_3D>::GNode	GNode;
	typedef TDefs<DIM_3D>::GCell	GCell;
	typedef TDefs<DIM_3D>::GFace	GFace;

public:
	Operator3D_CollapseEdge( Kernel<DIM_3D>& kernel) : mKernel(kernel), mErrorCode(0), mQBefore(0.0), mQAfter(0.0), mVolMinAfter(0.0)	{}

	void	Reset();

	void	Init( const MGSize& idnod, const MGSize& idtarget);
	void	Init( const MGSize& idnod, const MGSize& idtarget, const vector<MGSize>& tabball);

	void	Execute();

	const MGSize&	Error() const		{ return mErrorCode;}
	const MGFloat&	QBefore() const		{ return mQBefore;}
	const MGFloat&	QAfter() const		{ return mQAfter;}
	const MGFloat&	VolMinAfter() const	{ return mVolMinAfter;}

private:
	Kernel<DIM_3D>		&mKernel;

	Key<2>			mEdge;

	vector<MGSize>	mtabBall;
	vector<MGSize>	mtabShell;
	vector<MGSize>	mtabCompl;


	MGSize			mErrorCode;
	MGFloat			mQBefore;
	MGFloat			mQAfter;
	MGFloat			mVolMinAfter;
};


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __OPERATOR3D_COLLAPSEEDGE_H__
