#ifndef __DESTROYER_H__
#define __DESTROYER_H__


#include "libcoresystem/mgdecl.h"
#include "libgreen/tdefs.h"
#include "libcoreconfig/configbase.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Dimension DIM>
class ControlSpace;

template <Dimension DIM> 
class Grid;

template <Dimension DIM> 
class BndGrid;

template <Dimension DIM> 
class Kernel;



//////////////////////////////////////////////////////////////////////
// class DestroyerV2
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class DestroyerV2 : public ConfigBase
{
};



//////////////////////////////////////////////////////////////////////
// class EdgeDestrV2
//////////////////////////////////////////////////////////////////////
class EdgeDestrV2
{
public:
	typedef TDefs<DIM_3D>::GVect	GVect;
	typedef TDefs<DIM_3D>::GMetric	GMetric;

	typedef TDefs<DIM_3D>::GNode	GNode;
	typedef TDefs<DIM_3D>::GCell	GCell;
	typedef TDefs<DIM_3D>::GFace	GFace;

public:
	EdgeDestrV2( const ControlSpace<DIM_3D>* pcspace, Kernel<DIM_3D>* pkernel ) : mpCSpace(pcspace), mpKernel(pkernel)	{}

	MGSize	RemoveSPnts();
	MGSize	RemoveSPntsPP( const MGSize& niter);

	void	GetPPs( vector<MGSize>& tab);
	void	GetSPs( vector<MGSize>& tab);

	MGFloat	FindDistance();
	GVect	FindNormalVect();

public:

	DestroyerV2<DIM_3D>			*mpParent;

	const ControlSpace<DIM_3D>	*mpCSpace;
	Kernel<DIM_3D>				*mpKernel;

	Key<2>	mEdge;

	vector<MGSize>		mtabInNode;
	vector< Key<2> >	mtabSubEdge;

	vector<MGSize>		mtabPP;
};





//////////////////////////////////////////////////////////////////////
// class DestroyerV2
//////////////////////////////////////////////////////////////////////
template <>
class DestroyerV2<DIM_2D> : public ConfigBase
{
public:
	DestroyerV2( const BndGrid<DIM_2D>& bgrd, const ControlSpace<DIM_2D>& cspace, Kernel<DIM_2D>& kernel) 
		: mpBndGrd(&bgrd), mpCSpace(&cspace), mpKernel(&kernel)		{}
private:
	const BndGrid<DIM_2D>		*mpBndGrd;
	const ControlSpace<DIM_2D>	*mpCSpace;
	Kernel<DIM_2D>				*mpKernel;
};
//////////////////////////////////////////////////////////////////////




//////////////////////////////////////////////////////////////////////
// class DestroyerV2
//////////////////////////////////////////////////////////////////////
template <>
class DestroyerV2<DIM_3D> : public ConfigBase
{
	typedef TDefs<DIM_3D>::GVect	GVect;
	typedef TDefs<DIM_3D>::GMetric	GMetric;

	typedef TDefs<DIM_3D>::GNode	GNode;
	typedef TDefs<DIM_3D>::GCell	GCell;
	typedef TDefs<DIM_3D>::GFace	GFace;

	friend class FaceCDT;
	friend class EdgeDestrV2;
	friend class SPointDestructor;
	friend class SPointPushDestructor;

public:
	DestroyerV2( const BndGrid<DIM_3D>& bgrd, const ControlSpace<DIM_3D>& cspace, Kernel<DIM_3D>& kernel) 
		: mpBndGrd(&bgrd), mpCSpace(&cspace), mpKernel(&kernel)		{}


	void	Init( const vector< KeyData< Key<2>, vector< Key<2> > > >& tabedge);
	void	Purge();

	void	PurgeEC();
	MGSize	PurgePP();
	MGSize	PPTabSize()		{ return mtabPP.size(); }


	void	Optimise();

	void	UpdateBoundaryGrid( BndGrid<DIM_3D>& bgrid);

	void	ExportExtraPnts( const MGString& name);

	MGSize CountSPoints() const;

private:
	const BndGrid<DIM_3D>		*mpBndGrd;
	const ControlSpace<DIM_3D>	*mpCSpace;
	Kernel<DIM_3D>				*mpKernel;

	MGSize				mTotN;
	vector<EdgeDestrV2>	mtabDEdge;
	vector< KeyData< MGSize, Key<2> > >	mtabINParent;

	vector<MGSize>		mtabPP;
};
//////////////////////////////////////////////////////////////////////


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __DESTROYER_H__
