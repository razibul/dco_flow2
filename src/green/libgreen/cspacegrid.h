#ifndef __CSPACEGRID_H__
#define __CSPACEGRID_H__

#include "libcoresystem/mgdecl.h"
#include "libextget/geomentity.h"

#include "libgreen/controlspace.h"
#include "libgreen/grid.h"
#include "libgreen/generatordata.h"
#include "libgreen/kernel.h"
#include "libgreen/gridcontext.h"
#include "libgreen/interpolator.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Dimension DIM>
class BndGrid;


template <Dimension DIM>
class CSpaceMaster;

////\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
//namespace ControlSpaceSpace {
////\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//



//////////////////////////////////////////////////////////////////////
//	class GridControlSpace
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, Dimension UNIDIM>
class GridControlSpace : public DualControlSpace<DIM,UNIDIM>
{
	typedef typename TDefs<DIM>::GVect		GVect;
	typedef typename TDefs<DIM>::GMetric	GMetric;

	typedef typename TDefs<UNIDIM>::GVect	UVect;
	typedef typename TDefs<UNIDIM>::GMetric	UMetric;

	typedef typename UMetric::SMtx			USMtx;
	typedef typename GMetric::SMtx			SMtx;

	friend class CSpaceMaster<UNIDIM>;

public:
	GridControlSpace( const GET::GeomEntity<DIM,UNIDIM> *pgent=NULL) : mpGEnt(pgent), mInterpol(&mGrid)		{}

	virtual ~GridControlSpace()		{}

	virtual GMetric	GetSpacing( const GVect& vct) const;
	virtual UMetric	GetGlobSpacing( const GVect& vct) const;

	void	Init( const GridContext<UMetric>& cxmet, const BndGrid<DIM>& bgrid);
	void	InitFileMEANDR( const MGString& fname);
	void	ExportFileMEANDR( const MGString& fname);

	void	ApplyScale( const MGFloat& scale);

private:
	const GET::GeomEntity<DIM,UNIDIM>	*mpGEnt;
	Grid<DIM>							mGrid;

	GridContext<USMtx>		mUMetricCx;
	Interpolator<DIM>		mInterpol;
};




////\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
//} // end of namespace ControlSpace
////\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
//
//namespace CSPACE = ControlSpaceSpace;



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __CSPACEGRID_H__
