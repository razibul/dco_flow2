#include "reconstructor.h"
#include "libgreen/grid.h"
#include "libgreen/bndgrid.h"
#include "libgreen/kernel.h"
#include "libgreen/gridgeom.h"
//
//#include "writegrdtec.h"

#include "libgreen/operator2d_flip22.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 





void Reconstructor<DIM_2D>::Init( const bool& bout)
{
	mpBndGrd->FindFaces( mtabBndFaces);

	FindMissingFaces( bout);
}

void Reconstructor<DIM_2D>::FindMissingFaces( const bool& bout)
{
	typedef vector< Key<2> >	TKeyTab;

	TKeyTab	tabgrde;
	TKeyTab::iterator	itr;

	mpKernel->rGrid().FindFaces( tabgrde);

	// TODO :: tabgrde should be already sorted; check this
	sort( tabgrde.begin(), tabgrde.end() );

	mtabMissing.clear();
	
	for ( itr = mtabBndFaces.begin(); itr != mtabBndFaces.end(); ++itr)
	{
		Key<2>	key = *itr;
		key.Sort();

		if ( key.cFirst() == key.cSecond() )
		{
			continue;
		}

		bool bFound = binary_search( tabgrde.begin(), tabgrde.end(), key);
		if ( !bFound)
		{
			mtabMissing.push_back( key);
			//key.Dump();
		}
	}

	if ( bout)
	{
		cout << "found " << mtabMissing.size() << " missing edges" << endl;
		TRACE( "found " << mtabMissing.size() << " missing edges" );

		for ( MGSize i=0; i<mtabMissing.size(); ++i)
			cout << "( " << mtabMissing[i].cFirst() << ", " << mtabMissing[i].cSecond() << ")" << endl;
	}
}







void Reconstructor<DIM_2D>::Reconstruct( const bool& bout)
{
	if ( bout)
		cout << "trying to reconstruct missing edges\n";

	typedef vector< Key<2> >	TKeyTab;
	TKeyTab::iterator	itr;

	for ( itr = mtabMissing.begin(); itr != mtabMissing.end(); ++itr)
	{
		// find face pipe
		vector<MGSize>	pipe;

		if ( ! mpKernel->FindFacePipe( pipe, *itr) )
		{
			itr->Dump();
			TRACE( "empty pipe !!!");
			continue;
		}

		// start swapping
		Operator2D_Flip22	flip( mpKernel->rGrid() );

		GVect	v1 = mpKernel->rGrid().cNode( (*itr).cFirst() ).cPos();
		GVect	v2 = mpKernel->rGrid().cNode( (*itr).cSecond() ).cPos();

		MGSize icur = 0;
		MGSize ic1o=0, ic2o=0;
		do
		{

			MGSize ic1 = pipe[icur];
			MGSize ic2 = pipe[icur+1];

			if ( flip.Check( ic1, ic2 ) && ( (ic1!=ic1o || ic2!=ic2o) && (ic1!=ic2o || ic2!=ic1o) ) )
			{
				flip.Execute();
				icur = 0;
				ic1o = ic1;
				ic2o = ic2;

				mpKernel->FindFacePipe( pipe, *itr);

				if ( pipe.size() == 0)
					break;

			}
			else
			{
				++icur;
			}
		}
		while ( true);
	}

	// check again the boundary integrity
	// at this point there should be no miising faces
	FindMissingFaces( bout);

	MGSize k;
	for ( k=0, itr = mtabMissing.begin(); itr != mtabMissing.end(); ++k, ++itr)
	{
		vector<MGSize>	pipe;

		mpKernel->FindFacePipe( pipe, *itr);
		cout << k << "  pipe.size = " << pipe.size() << endl;

		cout << "edge ( ";
		//cout << mpKernel->rGrid().cNode( itr->cFirst() ).cPos().cX();
		cout << itr->cFirst();
		cout << ", ";
		//cout << mpKernel->rGrid().cNode( itr->cSecond() ).cPos().cX();
		cout << itr->cSecond();
		cout << " )" << endl;
	}
}




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

