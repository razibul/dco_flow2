#ifndef __CSPACECOMBINE_H__
#define __CSPACECOMBINE_H__


#include "libgreen/controlspace.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
//	class CombineControlSpace
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class CombineControlSpace : public ControlSpace<DIM>
{
	typedef typename TDefs<DIM>::GVect		GVect;
	typedef typename TDefs<DIM>::GMetric	GMetric;

public:
	CombineControlSpace( const ControlSpace<DIM>& csin, const ControlSpace<DIM>& csout) : mCSin(csin), mCSout( csout)	{}

	virtual GMetric	GetSpacing( const GVect& vct) const;

private:
	const ControlSpace<DIM>&		mCSin;
	const ControlSpace<DIM>&		mCSout;
};



template <Dimension DIM>
inline typename TDefs<DIM>::GMetric CombineControlSpace<DIM>::GetSpacing( const GVect& vct) const
{
	GMetric metin  = mCSin.GetSpacing( vct);
	GMetric metout = mCSout.GetSpacing( vct);

	SMatrix<DIM>	mtxMin, mtxMout, mtxLout, mtxLTout, mtxM;

	mtxMin = metin;
	mtxMout = metout;
	metout.EigenDecompose( mtxLTout);
	mtxLout = mtxLTout;
	mtxLout.Transp();

	mtxM = mtxLout * mtxMin * mtxLTout;

	GMetric met = mtxM;
	//met.LimitMetric( 0.008, 10, 0);

	return met;
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __CSPACECOMBINE_H__

