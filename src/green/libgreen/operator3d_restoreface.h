#ifndef __OPERATOR3D_RESTOREFACE_H__
#define __OPERATOR3D_RESTOREFACE_H__


#include "libcoresystem/mgdecl.h"
#include "libgreen/tdefs.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Dimension DIM> 
class Grid;

template <Dimension DIM>
class ControlSpace;

template <Dimension DIM> 
class Kernel;


//////////////////////////////////////////////////////////////////////
// class Operator3D_RestoreFace
//////////////////////////////////////////////////////////////////////
class Operator3D_RestoreFace
{
	typedef TDefs<DIM_3D>::GVect	GVect;
	typedef TDefs<DIM_3D>::GMetric	GMetric;

	typedef TDefs<DIM_3D>::GNode	GNode;
	typedef TDefs<DIM_3D>::GCell	GCell;
	typedef TDefs<DIM_3D>::GFace	GFace;

public:
	Operator3D_RestoreFace( const ControlSpace<DIM_3D>& cspace, Kernel<DIM_3D>& kernel) : mCSpace(cspace), mKernel(kernel), mErrorCode(0)	{}

	const MGSize&	Error() const		{ return mErrorCode;}

	void	Reset();

	void	Init( const Key<2>& edgeL, const Key<2>& edgeR);

	void	ExecuteInternal();
	void	ExecuteExternal();

private:
	const ControlSpace<DIM_3D>&	mCSpace;
	Kernel<DIM_3D>&				mKernel;

	MGSize			mErrorCode;
};



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __OPERATOR3D_RESTOREFACE_H__

