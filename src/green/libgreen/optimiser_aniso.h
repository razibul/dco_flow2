#ifndef __OPTIMISER_ANISO_H__
#define __OPTIMISER_ANISO_H__


#include "libcoresystem/mgdecl.h"
#include "libgreen/tdefs.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Dimension DIM> 
class Grid;

template <Dimension DIM> 
class BndGrid;

template <Dimension DIM> 
class Kernel;





//////////////////////////////////////////////////////////////////////
// class OptimiserAniso
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class OptimiserAniso
{
public:
	OptimiserAniso( Kernel<DIM>& kernel) 
		: mpKernel(&kernel)		{}

	void	DoOptimise();

private:
	Kernel<DIM>		*mpKernel;
};


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __OPTIMISER_ANISO_H__
