#ifndef __BMSH_H__
#define __BMSH_H__


#include "libgreen/tdefs.h"
#include "libgreen/gridmaster.h"
#include "libextget/geotopomaster.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



//////////////////////////////////////////////////////////////////////
//	class BMeshNode
//////////////////////////////////////////////////////////////////////
template <Dimension DIM> 
class BMeshNode
{
public:
	typedef typename TDefs<DIM>::GVect	GVect;

public:
	Dimension	mLocalDim;
	MGSize		mEntId;

	GVect		mMasterPos;
	GVect		mLocalPos;
};


//////////////////////////////////////////////////////////////////////
//	class BMesh
//////////////////////////////////////////////////////////////////////
template <Dimension DIM> 
class BMesh
{
public:
	typedef typename TDefs<DIM>::GVect	GVect;

	BMesh( const GET::GeoTopoMaster<DIM>& getm, const GridMaster<DIM>& grdm) : mGeTMaster(getm), mGrdMaster(grdm), mtabTopo(DIM+1)	{}

	void	Init( const MGSize& topoid);
	void	DoWrite( const MGString& fname);

private:
	void	InitTabNode();

	void	InitTabNodeLoc0D();

	template <Dimension LDIM> 
	void	InitTabNodeLoc();

	void	InitFaceOrient( const MGSize& topoid);

	template <Dimension LDIM> 
	void	WriteGridCells( ostream& file);

	void	WriteGrids( ostream& file);


private:
	const GET::GeoTopoMaster<DIM>&	mGeTMaster;
	const GridMaster<DIM>&			mGrdMaster;

	vector< vector< MGSize> >		mtabTopo;
	map<MGSize,bool>				mmapFaceOrient;

	vector< BMeshNode<DIM> >		mtabNode;
	map< MGSize, MGSize >			mmapMaster;

	//vector< pair< MGSize, MGSize > >	mtabMasterMap;
};


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __BMSH_H__

