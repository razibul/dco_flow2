#ifndef __WRITEGRDTEC_H__
#define __WRITEGRDTEC_H__


#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"
#include "libextget/geomentity.h"
#include "libcorecommon/fgvector.h"

#include "libcoreio/store.h"
#include "libcorecommon/key.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

const char	TECELEM_SEGMENT[]	= "LINESEG";
const char	TECELEM_TRIANGLE[]	= "TRIANGLE";
const char	TECELEM_TETRA[]		= "TETRAHEDRON";


template <Dimension DIM>
class Grid;

//////////////////////////////////////////////////////////////////////
// class WriteGrdTEC
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class WriteGrdTEC
{
public:
	WriteGrdTEC( const Grid<DIM>* pgrd) : mpGrd(pgrd)	{}

	void	DoWrite( const MGString& fname, const MGString& gname="");
	void	WriteCells( const MGString& name, vector<MGSize>& tab, const MGString& zone="CELLS");
	void	WriteExploded( const MGString& name, vector<MGSize>& tab, const MGFloat& coeff = 1.0);

	void	WriteEdges( const MGString& name, const vector< Key<2> >& tab, const MGString& zone="EDGES");
	void	WriteFaces( const MGString& name, const vector< Key<3> >& tab, const MGString& zone="FACES");

	template <Dimension UDIM>
	void	DoWrite( const MGString& fname, const GET::GeomEntity<DIM,UDIM>* pgent);

	template <Dimension UDIM>
	void	DoWrite(  ostream& file, const GET::GeomEntity<DIM,UDIM>* pgent);

protected:
	MGString	ElementType() const;

private:
	const Grid<DIM>	*mpGrd;

	MGString			mFName;

	MGString			mTitle;
	vector<MGString>	mtabVars;

	MGString			mZoneT;
	MGString			mElemT;

	MGSize				mnNode;
	MGSize				mnElem;

};
//////////////////////////////////////////////////////////////////////


template <>
inline MGString WriteGrdTEC<DIM_1D>::ElementType() const
{
	return MGString( TECELEM_SEGMENT);
}

template <>
inline MGString WriteGrdTEC<DIM_2D>::ElementType() const
{
	return MGString( TECELEM_TRIANGLE);
}

template <>
inline MGString WriteGrdTEC<DIM_3D>::ElementType() const
{
	return MGString( TECELEM_TETRA);
}


template <Dimension DIM>
template <Dimension UDIM>
void WriteGrdTEC<DIM>::DoWrite( ostream& file, const GET::GeomEntity<DIM,UDIM>* pgent)
{
	MGSize gid = 0;
	if ( pgent)
		gid = pgent->cId();

	file << "TITLE = \"" << "grid" << "\"" << endl;
	file << "VARIABLES = ";

	for ( MGSize k=0; k<DIM; ++k)
		file << ", \"U" << setw(1) << setfill('0') << k+1 << "\"";

	for ( MGSize k=0; k<UDIM; ++k)
		file << ", \"X" << setw(1) << setfill('0') << k+1 << "\"";

	file << ", \"ID\"";
	file << endl;

	file	<< "ZONE T=\"" << "grid " << gid << "\""
			<< ", N=" << mpGrd->SizeNodeTab() 
			<< ", E=" << mpGrd->SizeCellTab() 
			<< ", F=FEPOINT, ET=" << ElementType() << endl;

	MGSize wid = 1;
	vector<MGSize>	tabid( mpGrd->cColNode().size() );

	for ( typename Grid<DIM>::ColNode::const_iterator itrn = mpGrd->cColNode().begin(); itrn != mpGrd->cColNode().end(); ++itrn)
	{
		Vect<DIM>	vctu = (*itrn).cPos();
		Vect<UDIM>	vctx(0.0);

		tabid[ (*itrn).cId() ] = wid++;

		if ( pgent)
			pgent->GetCoord( vctx, vctu);

		for ( MGSize k=0; k<DIM; ++k)
			file << setprecision(8) << vctu.cX( k) << " ";

		for ( MGSize k=0; k<UDIM; ++k)
			file << setprecision(8) << vctx.cX( k) << " ";

		file << (*itrn).cId() << " ";

		file << endl;
	}

	for ( typename Grid<DIM>::ColCell::const_iterator itrc = mpGrd->cColCell().begin(); itrc != mpGrd->cColCell().end(); ++itrc)
	{
		for ( MGSize k=0; k<=DIM; ++k)
			file << tabid[ (*itrc).cNodeId(k) ] << " ";

		file << endl;
	}

}


template <Dimension DIM>
template <Dimension UDIM>
void WriteGrdTEC<DIM>::DoWrite( const MGString& fname, const GET::GeomEntity<DIM,UDIM>* pgent)
{
	ofstream file( fname.c_str(), ios::out);

	try
	{
		if ( ! file)
			THROW_FILE( "can not open the file", fname);

		cout << "Writing TEC " << fname;
		cout.flush();

		DoWrite( file, pgent);
		cout <<  " - FINISHED" << endl;
	}
	catch ( EHandler::Except& e)
	{
		cout << "ERROR: WriteGrdTEC::DoWrite - file name: " << fname << endl << endl;
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);

		THROW_INTERNAL("END");
	}
}


//////////////////////////////////////////////////////////////////////


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __WRITEGRDTEC_H__
