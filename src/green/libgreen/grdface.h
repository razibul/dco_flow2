#ifndef __GRDFACE_H__
#define __GRDFACE_H__

#include "libcoresystem/mgdecl.h"
#include "libgreen/tdefs.h"
#include "libgreen/basecell.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
// class FaceNeighbours
//////////////////////////////////////////////////////////////////////
class FaceNeighbours
{
public:
	FaceNeighbours()	{ Reset();}

	void	Reset();

	const GNeDef&	cCellLo() const					{ return mtabCellIds[0];}
	GNeDef&			rCellLo()						{ return mtabCellIds[0];}

	const GNeDef&	cCellUp() const					{ return mtabCellIds[1];}
	GNeDef&			rCellUp()						{ return mtabCellIds[1];}

private:
	GNeDef	mtabCellIds[2];
};
//////////////////////////////////////////////////////////////////////

inline void FaceNeighbours::Reset()
{
	mtabCellIds[0] = GNeDef( 0, 0);
	mtabCellIds[1] = GNeDef( 0, 0);
}



//////////////////////////////////////////////////////////////////////
// class Face
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class Face : public FaceNeighbours, public BaseCell< static_cast<Dimension>( DIM-1) >
{
public:
	Face() : FaceNeighbours(), BaseCell< static_cast<Dimension>( DIM-1) >()	{}

};
//////////////////////////////////////////////////////////////////////



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __GRDFACE_H__

