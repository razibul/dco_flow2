#ifndef __RECFACE_CDT_H__
#define __RECFACE_CDT_H__

#include "libcoresystem/mgdecl.h"
#include "libgreen/tdefs.h"
#include "libgreen/grid.h"
#include "libgreen/kernel.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Dimension DIM>
class ReconstructorCDT;


//////////////////////////////////////////////////////////////////////
// class SubFaceCDT
//////////////////////////////////////////////////////////////////////
class SubFaceCDT
{
public:
	SubFaceCDT() : mCellId(0)	{}
	SubFaceCDT(const Key<3>& face, const MGSize& idcell) : mFace(face), mCellId(idcell)	{}

	const Key<3>&	cFace()	const		{ return mFace; }

	const MGSize&	cCellId() const			{ return mCellId; }
	MGSize&			rCellId()				{ return mCellId; }

private:
	Key<3>	mFace;
	MGSize	mCellId;
};

inline bool operator == ( const SubFaceCDT& e1, const SubFaceCDT& e2 )
{
	return ( e1.cFace() == e2.cFace() );
}

inline bool operator < ( const SubFaceCDT& e1, const SubFaceCDT& e2 )
{
	return ( e1.cFace() < e2.cFace() );
}



//////////////////////////////////////////////////////////////////////
// class FaceCDT
//////////////////////////////////////////////////////////////////////
class FaceCDT
{
	typedef TDefs<DIM_3D>::GVect	GVect;
	typedef TDefs<DIM_3D>::GNode	GNode;
	typedef TDefs<DIM_3D>::GCell	GCell;
	typedef TDefs<DIM_3D>::GFace	GFace;

public:
	FaceCDT() : mpRec(NULL), mCellId(0), mbDebug(false)	{}

	FaceCDT( const Key<3>& face, ReconstructorCDT<DIM_3D>* prec) : mpRec(prec), mFace(face), mCellId(0), mbDebug(false)		{}


	const Key<3>&	cFace()	const		{ return mFace; }

	const vector<MGSize>&		cTabInNodes() const		{ return mtabInNodes; }
	const vector< Key<2> >&		cTabBEdges() const		{ return mtabBEdges; }
	const vector<SubFaceCDT>&	cTabSubFaces() const	{ return mtabSubFaces; }


	bool	IsRecoverd();

	bool	FindFacePipeExtended();
	void	GenerateFaceGrid( Grid<DIM_3D>& fcGrid, Kernel<DIM_3D>& fcKernel );

	void	FindLocGridFaces( map< Key<3>, Key<3> >& mapface, Kernel<DIM_3D>& fcKernel);
	void	SyncCavities( Grid<DIM_3D>& fcGrid, Kernel<DIM_3D>& fcKernel );

	void	Reconstruct();


	void	InitTabInNodes( const vector<MGSize>& tabn)		{ mtabInNodes = tabn; }
	void	InitTabBEdges( const vector< Key<2> >& tabe)	{ mtabBEdges = tabe; }

	void	UpdateBndSubFaces();
	void	Dump();

private:
	ReconstructorCDT<DIM_3D>	*mpRec;

	Key<3>	mFace;
	MGSize	mCellId;

	vector<MGSize>		mtabInNodes;
	vector< Key<2> >	mtabBEdges;

	vector<SubFaceCDT>	mtabSubFaces;
	vector<MGSize>		mtabXCells;
	vector< Key<2> >	mtabXEdge;
	vector<MGSize>		mtabXSlivers;

	bool mbDebug;
};





//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __RECFACE_CDT_H__
