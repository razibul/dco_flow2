#ifndef __SPOINTPUSHDESTRUCTOR_H__
#define __SPOINTPUSHDESTRUCTOR_H__

#include "libcoresystem/mgdecl.h"
#include "libgreen/tdefs.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Dimension DIM>
class DestroyerV2;

template <Dimension DIM> 
class Grid;


//////////////////////////////////////////////////////////////////////
// class SPointPushDestructor
//////////////////////////////////////////////////////////////////////
class SPointPushDestructor
{
public:
	typedef TDefs<DIM_3D>::GVect	GVect;
	typedef TDefs<DIM_3D>::GMetric	GMetric;

	typedef TDefs<DIM_3D>::GNode	GNode;
	typedef TDefs<DIM_3D>::GCell	GCell;
	typedef TDefs<DIM_3D>::GFace	GFace;


public:
	SPointPushDestructor( const MGSize& inod) : mINode(inod), mIPNode(0)	{}

	void	Init(  const vector< Key<2> >& tabsedge, const GVect& vn, const MGFloat& dist);
	bool	Delete( const MGSize& niter);
	void	UpdateSubEdges( vector< Key<2> >& tabsedge);


	bool	InsertPushNode( const MGFloat& coeff);
	void	Triangulate( Grid<DIM_3D>& fcGrid, vector< Key<3> >& tabbface, const vector< Key<2> >& tabbe, const string& stxt="");
	void	Triangulate2( Grid<DIM_3D>& fcGrid, vector< Key<3> >& tabbface, const vector< Key<2> >& tabbe, const string& stxt="");
	void	SyncCavities( Grid<DIM_3D>& fcGridL, Grid<DIM_3D>& fcGridR);


	void	TriangulateFaces( vector< Key<3> >& tabbface, const vector< Key<2> >& tabbe, const string& stxt="");


public:
	DestroyerV2<DIM_3D>	*mpParent;

	MGSize		mINode;
	Key<2>		mEdge;

	MGSize		mIPNode;
	GVect		mPPnt;

	MGSize		mINodeL;
	MGSize		mINodeR;

	Key<2>		mEdgeL;
	Key<2>		mEdgeR;

	GVect		mVn;
	MGFloat		mDist;

	vector<MGSize>			mtabBall;
	vector< Face<DIM_3D> >	mtabBFace;
	vector< Key<2> >		mtabBEL;
	vector< Key<2> >		mtabBER;

};


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __SPOINTPUSHDESTRUCTOR_H__
