#ifndef __INTERPOLATOR_H__
#define __INTERPOLATOR_H__


#include "libcoresystem/mgdecl.h"
#include "libgreen/tdefs.h"
#include "libgreen/localizator.h"
#include "libgreen/gridgeom.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Dimension DIM> 
class Grid;


//////////////////////////////////////////////////////////////////////
//	class Interpolator
//////////////////////////////////////////////////////////////////////
template <Dimension DIM> 
class Interpolator
{
	typedef typename TDefs<DIM>::GVect		GVect;

	typedef typename TDefs<DIM>::GCell		GCell;

public:
	Interpolator( Grid<DIM>* pgrd) : mLocal(), mpGrid(pgrd)		{ mLocal.Init(mpGrid); }

	bool	FindCoeff( vector< pair<MGSize,MGFloat> >& tabcoeff, const GVect& vct, const bool& bcrash=true) const;

	template <class T> 
	T	FindValue( const GVect& vct) const;

	Localizator<DIM>&	rLocalizator()		{ return mLocal; }

private:
	Grid<DIM>			*mpGrid;
	Localizator<DIM>	mLocal;
};



template <Dimension DIM> 
bool Interpolator<DIM>::FindCoeff( vector< pair<MGSize,MGFloat> >& tabcoeff, const GVect& vct, const bool& bcrash) const
{
	MGSize icell = mLocal.Localize( vct, bcrash);

	if ( icell == 0)
		return false;

	const GCell& cell = mpGrid->cCell( icell);
	Simplex<DIM> splx( GridGeom<DIM>::CreateSimplex( icell, *mpGrid) );

	//if ( splx.IsInside( vct, -1.0e-4) == false)
	if ( splx.IsInside( vct) == false)
	{
		if ( bcrash )
		{
			THROW_INTERNAL( "Localizator failed inside Interpolator<DIM>::FindCoeff");
		}
		else 
			return false;
	}

	MGFloat	a = splx.Volume();

	tabcoeff.resize( GCell::SIZE);

	for ( MGSize i=0; i<GCell::SIZE; ++i)
	{
		tabcoeff[i].first = cell.cNodeId( i);
		tabcoeff[i].second = - splx.FaceSmpxVolume( vct, i) / a;
	}

	//MGFloat	a = Geom::VolumeTRI( tabv[0], tabv[1], tabv[2] );
	//MGFloat	a0 = Geom::VolumeTRI( tabv[1], tabv[2], vct );
	//MGFloat	a1 = Geom::VolumeTRI( tabv[2], tabv[0], vct );
	//MGFloat	a2 = Geom::VolumeTRI( tabv[0], tabv[1], vct );

	return true;
}



template <Dimension DIM> 
template <class T> 
T Interpolator<DIM>::FindValue( const GVect& vct) const
{
	//vector< pair<MGSize,MGFloat> > tabcoeff;
	//FindCoeff( tabcoeff, vct);
	return T();
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __INTERPOLATOR_H__
