#ifndef __METRICINTERPOL_H__
#define __METRICINTERPOL_H__

#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"
#include "libcorecommon/smatrix.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


using namespace Geom;



//////////////////////////////////////////////////////////////////////
//	class MetricInterpol
//////////////////////////////////////////////////////////////////////
template <class ELEM_TYPE, Dimension DIM> 
class MetricInterpol
{
	typedef SMatrix<DIM,ELEM_TYPE>	SMtx;
	typedef SVector<DIM,ELEM_TYPE>	SVect;

public:
	void	Reset()		{ mMatrix.Init( 0.0); }
	void	Add( const SMtx& mtx, const ELEM_TYPE& coeff = 1.0);
	void	GetResult( SMtx& mtx);

protected:
	void	PreComp( SMtx& mtxres, const SMtx& mtx);
	void	PostComp( SMtx& mtxres, const SMtx& mtx);

private:
	SMtx	mMatrix;
};



template <class ELEM_TYPE, Dimension DIM> 
void MetricInterpol<ELEM_TYPE,DIM>::Add( const SMtx& mtx, const ELEM_TYPE& coeff)
{
	SMtx mtxtmp;
	PreComp( mtxtmp, mtx);
	mMatrix += coeff * mtxtmp;
}

template <class ELEM_TYPE, Dimension DIM> 
void MetricInterpol<ELEM_TYPE,DIM>::GetResult( SMtx& mtx)
{
	PostComp( mtx, mMatrix);
}


template <class ELEM_TYPE, Dimension DIM> 
void MetricInterpol<ELEM_TYPE,DIM>::PreComp( SMtx& mtxout, const SMtx& mtx)
{
	// TODO :: check if matrix is symetric
	SMtx mtxD;	// TODO :: should be SVector
	SMtx mtxtmp = mtx;
	SMtx mtxN, mtxNT;

	mtxtmp.Decompose( mtxD, mtxN, true);


	for ( MGSize k=0; k<DIM; ++k)	// TODO :: check if it should not be Size() ???
		mtxD(k,k) = 1.0 / ::sqrt( mtxD(k,k));
		//mtxD(k,k) = 1.0 / ::pow( mtxD(k,k),0.4);

	mtxNT = mtxN;
	mtxNT.Transp();

	mtxout = mtxN * mtxD * mtxNT;
	return;
}

template <class ELEM_TYPE, Dimension DIM> 
void MetricInterpol<ELEM_TYPE,DIM>::PostComp( SMtx& mtxout, const SMtx& mtx)
{
	// TODO :: check if matrix is symetric
	SMtx mtxD;	// TODO :: should be SVector
	SMtx mtxtmp = mtx;
	SMtx mtxN, mtxNT;

	mtxtmp.Decompose( mtxD, mtxN, true);


	for ( MGSize k=0; k<DIM; ++k)	// TODO :: check if it should not be Size() ???
		mtxD(k,k) = 1.0 / ( mtxD(k,k) * mtxD(k,k));
		//mtxD(k,k) = 1.0 / ::pow( mtxD(k,k), 1.0/0.4);

	mtxNT = mtxN;
	mtxNT.Transp();

	mtxout = mtxN * mtxD * mtxNT;
	return;
}





//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __METRICINTERPOL_H__
