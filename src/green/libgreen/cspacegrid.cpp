#include "cspacegrid.h"
#include "libgreen/bndgrid.h"
#include "libcorecommon/progressbar.h"
#include "libgreen/writegrdtec.h"
#include "libgreen/reconstructor.h"
#include "libgreen/kernel.h"
#include "libgreen/readmeandr.h"
#include "libgreen/writemeandr.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


bool go = false;




//template< class ELEM_TYPE, MGSize MAX_SIZE>
//inline void DecomposeROOT( SMatrix< MAX_SIZE, ELEM_TYPE>& mtxout, const SMatrix< MAX_SIZE, ELEM_TYPE>& mtx)
//{
//	// TODO :: check if matrix is symetric
//	SMatrix< MAX_SIZE, ELEM_TYPE> mtxD;	// TODO :: should be SVector
//	SMatrix< MAX_SIZE, ELEM_TYPE> mtxtmp = mtx;
//	SMatrix< MAX_SIZE, ELEM_TYPE> mtxN;
//
//	mtxtmp.Decompose( mtxD, mtxN, true);
//
//
//	for ( MGSize k=0; k<MAX_SIZE; ++k)	// TODO :: check if it should not be Size() ???
//		mtxD(k,k) = 1.0 / ::sqrt( mtxD(k,k));
//
//	mtxN.Transp();
//
//	mtxout = mtxD * mtxN;
//	return;
//}
//
//template< class ELEM_TYPE, MGSize MAX_SIZE>
//inline void DecomposeSQR( SMatrix< MAX_SIZE, ELEM_TYPE>& mtxout, const SMatrix< MAX_SIZE, ELEM_TYPE>& mtx)
//{
//
//	// TODO :: check if matrix is symetric
//	SMatrix< MAX_SIZE, ELEM_TYPE> mtxNT;
//	SMatrix< MAX_SIZE, ELEM_TYPE> mtxN;
//
//	mtxN = mtx;
//
//	SVector< ELEM_TYPE, MAX_SIZE> vecD;
//	for ( MGSize i=0; i<MAX_SIZE; ++i)
//	{
//		MGFloat sum = 0.0;
//		for ( MGSize j=0; j<MAX_SIZE; ++j)
//			sum += mtxN(i,j)*mtxN(i,j);
//
//		//vecD(i) = ::sqrt( sum);
//		for ( MGSize j=0; j<MAX_SIZE; ++j)
//			mtxN(i,j) /= sum;
//	}
//
//	mtxNT = mtxN;
//	mtxNT.Transp();
//	mtxout = mtxNT*mtxN;
//	return;
//
//}


template< class ELEM_TYPE, MGSize MAX_SIZE>
inline void DecomposeROOT( SMatrix< MAX_SIZE, ELEM_TYPE>& mtxout, const SMatrix< MAX_SIZE, ELEM_TYPE>& mtx)
{
	// TODO :: check if matrix is symetric
	SVector< MAX_SIZE, ELEM_TYPE> vecD;
	SMatrix< MAX_SIZE, ELEM_TYPE> mtxtmp = mtx;
	SMatrix< MAX_SIZE, ELEM_TYPE> mtxN;

	mtxtmp.Decompose( vecD, mtxN, true);


	for ( MGSize k=0; k<MAX_SIZE; ++k)	// TODO :: check if it should not be Size() ???
		vecD(k) = 1.0 / ::sqrt( vecD(k));
		//mtxD(k,k) = 1.0 / ::pow( mtxD(k,k),0.4);

	SMatrix< MAX_SIZE, ELEM_TYPE> mtxNT = mtxN;
	mtxNT.Transp();

	mtxout.Assemble( mtxN, vecD, mtxNT );
	return;
}

//template< class ELEM_TYPE, MGSize MAX_SIZE>
//inline void DecomposeSQR( SMatrix< MAX_SIZE, ELEM_TYPE>& mtxout, const SMatrix< MAX_SIZE, ELEM_TYPE>& mtx)
//{
//	// TODO :: check if matrix is symetric
//	SMatrix< MAX_SIZE, ELEM_TYPE> mtxtmp = mtx;
//
//	mtxtmp.Invert();
//
//	mtxout = mtxtmp * mtxtmp;
//	return;
//}


template< class ELEM_TYPE, MGSize MAX_SIZE>
inline void DecomposeSQR( SMatrix< MAX_SIZE, ELEM_TYPE>& mtxout, const SMatrix< MAX_SIZE, ELEM_TYPE>& mtx)
{
	// TODO :: check if matrix is symetric
	SVector< MAX_SIZE, ELEM_TYPE> vecD;
	SMatrix< MAX_SIZE, ELEM_TYPE> mtxtmp = mtx;
	SMatrix< MAX_SIZE, ELEM_TYPE> mtxN;

	mtxtmp.Decompose( vecD, mtxN, true);


	for ( MGSize k=0; k<MAX_SIZE; ++k)	// TODO :: check if it should not be Size() ???
		vecD(k) = 1.0 / ( vecD(k) * vecD(k));
		//mtxD(k,k) = 1.0 / ::pow( mtxD(k,k), 1.0/0.4);

	SMatrix< MAX_SIZE, ELEM_TYPE> mtxNT = mtxN;
	mtxNT.Transp();

	mtxout.Assemble( mtxN, vecD, mtxNT);
	return;
}


//template <Dimension DIM, Dimension UNIDIM>
//typename TDefs<UNIDIM>::GMetric	GridControlSpace<DIM,UNIDIM>::GetGlobSpacing( const GVect& vct) const
//{
//	//cout << "-------------\n";
//	vector< pair<MGSize,MGFloat> > tabcoeff;
//
//	mInterpol.FindCoeff( tabcoeff, vct);
//
//	UMetric retmet;
//	retmet.Reset();
//
//	UMetric::SMtx smtxret;
//	smtxret.Init( 0.0 );
//
//	for ( MGSize i=0; i<tabcoeff.size(); ++i)
//	{
//		//cout << "  id = " << tabcoeff[i].first << endl;
//		MGSize idmet = mGrid.cNode( tabcoeff[i].first ).cMasterId();
//		UMetric met = mUMetricCx.cData( idmet);
//		//met.Write();
//
//		UMetric::SMtx smtxD, smtxL, smtxLI, smtx=met;
//
//		smtx.Decompose( smtxD, smtxL, false);
//		smtxLI = smtxL;
//		smtxLI.Invert();
//
//		for (MGSize k=0; k<UNIDIM; ++k)
//			smtxD(k,k) = 1.0/ ::sqrt( smtxD(k,k) );
//
//		smtx = smtxL * smtxD * smtxLI;
//
//		smtxret += tabcoeff[i].second * smtx;
//	}
//
//	//cout << "  ---- 1 \n";
//
//	UMetric::SMtx smtxD, smtxL, smtxLI, smtx=smtxret;
//
//	smtx.Decompose( smtxD, smtxL, false);
//	smtxLI = smtxL;
//	smtxLI.Invert();
//
//	for (MGSize k=0; k<UNIDIM; ++k)
//		smtxD(k,k) = 1.0/ ( smtxD(k,k) * smtxD(k,k) );
//
//	smtx = smtxL * smtxD * smtxLI;
//
//	return UMetric(smtx);
//}


template <Dimension DIM, Dimension UNIDIM>
typename TDefs<UNIDIM>::GMetric	GridControlSpace<DIM,UNIDIM>::GetGlobSpacing( const GVect& vct) const
{
	//cout << "-------------\n";
	vector< pair<MGSize,MGFloat> > tabcoeff;

	mInterpol.FindCoeff( tabcoeff, vct);

	UMetric retmet;
	retmet.Reset();

	typename UMetric::SMtx smtxret;
	smtxret.Init( 0.0 );

	MGFloat wsum = 0.0;
	for ( MGSize i=0; i<tabcoeff.size(); ++i)
	{
		MGSize idmet = mGrid.cNode( tabcoeff[i].first ).cMasterId();
		USMtx smtx = mUMetricCx.cData( idmet);


		//SVector<UNIDIM> vecD;
		//SMatrix<UNIDIM> mtxN;
		//SMatrix<UNIDIM> mtxtmp = smtx;
		//mtxtmp.Decompose( vecD, mtxN, true);

		//MGFloat wmin = *min_element( &vecD[0], &vecD[UNIDIM] );
		//MGFloat wmax = *max_element( &vecD[0], &vecD[UNIDIM] );

		//MGFloat w = pow( wmax/wmin, 3.0);


		//smtx.Write();

		//DecomposeROOT( smtx, smtx);

		//smtx.Write();
		//DecomposeSQR( smtx, smtx);
		//smtx.Write();

		smtxret += tabcoeff[i].second * smtx;
		wsum += tabcoeff[i].second;
	}

	//smtxret *= 1.0/ wsum;

	DecomposeSQR( smtxret, smtxret );

	return UMetric( smtxret);
}


template <Dimension DIM, Dimension UNIDIM>
typename TDefs<DIM>::GMetric	GridControlSpace<DIM,UNIDIM>::GetSpacing( const GVect& vct) const
{
	cout << "DIM = " << DIM << " UNIDIM = " << UNIDIM << endl;
	THROW_INTERNAL( "Error - should never be called");
	return GMetric();
}

template <>
TDefs<DIM_2D>::GMetric	GridControlSpace<DIM_2D,DIM_2D>::GetSpacing( const GVect& vct) const
{
	return GetGlobSpacing( vct);
}

template <>
TDefs<DIM_3D>::GMetric	GridControlSpace<DIM_3D,DIM_3D>::GetSpacing( const GVect& vct) const
{
	return GetGlobSpacing( vct);
}


template <>
TDefs<DIM_1D>::GMetric	GridControlSpace<DIM_1D,DIM_2D>::GetSpacing( const GVect& vct) const
{
	UMetric umet = GetGlobSpacing( vct);

	if ( ! mpGEnt)
		THROW_INTERNAL( "unknown GEnt");


	TDefs<DIM_2D>::GVect tab[DIM_1D];
	mpGEnt->GetFstDeriv( tab, vct);

	GMetric	met;

	met[0] = umet.Distance( tab[0].versor() );

	return met;
}

template <>
TDefs<DIM_1D>::GMetric	GridControlSpace<DIM_1D,DIM_3D>::GetSpacing( const GVect& vct) const
{
	UMetric umet = GetGlobSpacing( vct);

	if ( ! mpGEnt)
		THROW_INTERNAL( "unknown GEnt");


	TDefs<DIM_3D>::GVect tab[DIM_1D];
	mpGEnt->GetFstDeriv( tab, vct);

	GMetric	met;

	met[0] = umet.Distance( tab[0].versor() );

	return met;
}


template <>
TDefs<DIM_2D>::GMetric	GridControlSpace<DIM_2D,DIM_3D>::GetSpacing( const GVect& vct) const
{
	UMetric umet = GetGlobSpacing( vct);
	//TDefs<DIM_3D>::GMetric	met3d = GetGlobSpacing( vct);

	if ( ! mpGEnt)
		THROW_INTERNAL( "unknown GEnt");


	TDefs<DIM_3D>::GVect vu, vv, tab[DIM_2D];

	mpGEnt->GetFstDeriv( tab, vct);


	if ( tab[0].module() < ZERO)
		tab[0].rX() = tab[0].rY() = tab[0].rZ() = 1.0e-6;

	if ( tab[1].module() < ZERO)
		tab[1].rX() = tab[1].rY() = tab[1].rZ() = 1.0e-6;


	vu = tab[0].versor();
	vv = tab[1].versor();
	

	SMatrix<3> smet, smdir, smdirt, sres;

	smet = umet;
	smdir.Resize(3,2);
	smdir(0,0) = vu.cX();
	smdir(1,0) = vu.cY();
	smdir(2,0) = vu.cZ();
	smdir(0,1) = vv.cX();
	smdir(1,1) = vv.cY();
	smdir(2,1) = vv.cZ();

	smdirt = smdir;
	smdirt.Transp();

	sres = smdirt * smet * smdir;


	GMetric	met;
	met[0] = sres(0,0);
	met[1] = 0.5*(sres(0,1) + sres(1,0));
	met[2] = sres(1,1);

	//met.InitIso( 0.005);

	return met;

}



template <Dimension DIM, Dimension UNIDIM>
void GridControlSpace<DIM,UNIDIM>::Init( const GridContext<UMetric>& cxmet, const BndGrid<DIM>& bnd)
{
	cout << "DIM = " << DIM << " UNIDIM = " << UNIDIM << endl;
	THROW_INTERNAL( "Not implemented");
}


template <>
void GridControlSpace<DIM_1D,DIM_2D>::Init( const GridContext<UMetric>& cxmet, const BndGrid<DIM_1D>& bnd)
{
	GeneratorData<DIM_1D> kerdata;
	Kernel<DIM_1D> kernel( mGrid, kerdata);

	GVect	vmin, vmax;

	bnd.FindMinMax( vmin, vmax);
	
	kernel.InitCube( bnd.cNode1().cPos(), bnd.cNode2().cPos() );

	USMtx smtx1( cxmet.cData( bnd.cNode1().cMasterId() ) );
	DecomposeROOT( smtx1, smtx1);
	mGrid.rNode(1).rMasterId() = mUMetricCx.Insert( smtx1 );

	USMtx smtx2( cxmet.cData( bnd.cNode2().cMasterId() ) );
	DecomposeROOT( smtx2, smtx2);
	mGrid.rNode(2).rMasterId() = mUMetricCx.Insert( smtx2 );

	//mGrid.rNode(1).rMasterId() = mUMetricCx.Insert( cxmet.cData( bnd.cNode1().cMasterId() ) );
	//mGrid.rNode(2).rMasterId() = mUMetricCx.Insert( cxmet.cData( bnd.cNode2().cMasterId() ) );

	// ?????
	GMetric met;
	met.InitIso( 0.05);

	SMtx smtx( met);
	DecomposeROOT( smtx, smtx);

	//mMetricCx.Insert( smtx );
	//mMetricCx.Insert( smtx );

}


template <>
void GridControlSpace<DIM_1D,DIM_3D>::Init( const GridContext<UMetric>& cxmet, const BndGrid<DIM_1D>& bnd)
{
	GeneratorData<DIM_1D> kerdata;
	Kernel<DIM_1D> kernel( mGrid, kerdata);

	GVect	vmin, vmax;

	bnd.FindMinMax( vmin, vmax);
	
	kernel.InitCube( bnd.cNode1().cPos(), bnd.cNode2().cPos() );

	USMtx smtx1( cxmet.cData( bnd.cNode1().cMasterId() ) );
	DecomposeROOT( smtx1, smtx1);
	mGrid.rNode(1).rMasterId() = mUMetricCx.Insert( smtx1 );

	USMtx smtx2( cxmet.cData( bnd.cNode2().cMasterId() ) );
	DecomposeROOT( smtx2, smtx2);
	mGrid.rNode(2).rMasterId() = mUMetricCx.Insert( smtx2 );

	//// ?????
	//GMetric met;
	//met.InitIso( 0.05);
	//mMetricCx.Insert( met );
	//mMetricCx.Insert( met );

//	THROW_INTERNAL( "Not implemented");
}



template <>
void GridControlSpace<DIM_2D,DIM_3D>::Init( const GridContext<UMetric>& cxmet, const BndGrid<DIM_2D>& bnd)
{
	GeneratorData<DIM_2D> kerdata;
	Kernel<DIM_2D> kernel( mGrid, kerdata);


	ProgressBar	bar(40);

	bar.Init( bnd.SizeNodeTab() );
	bar.Start();


	GVect	vmin, vmax;

	bnd.FindMinMax( vmin, vmax);
	
	kernel.InitBoundingBox( vmin, vmax);

	kernel.InitCube();

	for ( Grid<DIM_2D>::ColNode::iterator itr = mGrid.rColNode().begin(); itr != mGrid.rColNode().end(); ++itr)
	{
		UMetric	met;
		met.InitIso(1.0);

		USMtx smtx( met);
		DecomposeROOT( smtx, smtx);

		MGSize idm = mUMetricCx.Insert( smtx );
		//mMetricCx.Insert( idm, met );
		itr->rMasterId() = idm;
	}



	vector<MGSize>	tabid(bnd.SizeNodeTab());
	for ( MGSize i=1; i<bnd.SizeNodeTab()+1; ++i)
		tabid[i-1] = i;

	random_shuffle( tabid.begin(), tabid.end() );


	for ( MGSize ii=0; ii<tabid.size(); ++ii, ++bar)
	{
		MGSize i = tabid[ii];

		GVect vct = bnd.cNode(i).cPos();


		MGSize inod = kernel.InsertPoint( vct );

		if ( ! inod)
		{
			char sbuf[512];
			sprintf( sbuf, "CRASH :: kernel failed to insert new point with coordinates [%lg %lg %lg]", 
							vct.cX(), vct.cY(), vct.cZ() );

			THROW_INTERNAL( sbuf);
		}

		USMtx smtx( cxmet.cData( bnd.cNode(i).cMasterId() ) );
		DecomposeROOT( smtx, smtx);

		MGSize idm = mUMetricCx.Insert( smtx );
		//mMetricCx.Insert( idm, cxmet.cData( bnd.cNode(i).cMasterId() ) );
		mGrid.rNode(inod).rMasterId() = idm;

		//mpGrd->rNode(inod).rMasterId() = bnd.cNode(i).cMasterId();


		//wtec.DoWrite( "_tmp_grd.plt");


		//fprintf( f, "node id = %d\n", i);
		//smtx = SMatrix<MGFloat,3>( bnd.cNode(i).cMetric());
		//smtx.Decompose( smtxD, smtxL);
		//smtxD.Write( f);
		//smtxL.Write( f);
	}
	
	bar.Finish();

	WriteGrdTEC<DIM_2D> wtec( &mGrid);
	wtec.DoWrite( "cspacegrid_2d.plt");
}


template <>
void GridControlSpace<DIM_2D,DIM_2D>::Init( const GridContext<UMetric>& cxmet, const BndGrid<DIM_2D>& bnd)
{
	GeneratorData<DIM_2D> kerdata;
	Kernel<DIM_2D> kernel( mGrid, kerdata);


	ProgressBar	bar(40);

	bar.Init( bnd.SizeNodeTab() );
	bar.Start();


	GVect	vmin, vmax;

	bnd.FindMinMax( vmin, vmax);
	
	kernel.InitBoundingBox( vmin, vmax);

	kernel.InitCube();

	for ( Grid<DIM_2D>::ColNode::iterator itr = mGrid.rColNode().begin(); itr != mGrid.rColNode().end(); ++itr)
	{
		GMetric	met;
		met.InitIso(1.0);

		SMtx smtx( met);
		DecomposeROOT( smtx, smtx);

		MGSize idm = mUMetricCx.Insert( smtx );
		//mMetricCx.Insert( idm, smtx );
		itr->rMasterId() = idm;
	}



	vector<MGSize>	tabid(bnd.SizeNodeTab());
	for ( MGSize i=1; i<bnd.SizeNodeTab()+1; ++i)
		tabid[i-1] = i;

	random_shuffle( tabid.begin(), tabid.end() );


	for ( MGSize ii=0; ii<tabid.size(); ++ii, ++bar)
	{
		MGSize i = tabid[ii];

		GVect vct = bnd.cNode(i).cPos();


		MGSize inod = kernel.InsertPoint( vct );

		if ( ! inod)
		{
			char sbuf[512];
			sprintf( sbuf, "CRASH :: kernel failed to insert new point with coordinates [%lg %lg %lg]", 
							vct.cX(), vct.cY(), vct.cZ() );

			THROW_INTERNAL( sbuf);
		}

		USMtx smtx( cxmet.cData( bnd.cNode(i).cMasterId() ) );
		DecomposeROOT( smtx, smtx);

		MGSize idm = mUMetricCx.Insert( smtx );
		//mMetricCx.Insert( idm, smtx );
		mGrid.rNode(inod).rMasterId() = idm;

		//mpGrd->rNode(inod).rMasterId() = bnd.cNode(i).cMasterId();


		//wtec.DoWrite( "_tmp_grd.plt");


		//fprintf( f, "node id = %d\n", i);
		//smtx = SMatrix<MGFloat,3>( bnd.cNode(i).cMetric());
		//smtx.Decompose( smtxD, smtxL);
		//smtxD.Write( f);
		//smtxL.Write( f);
	}
	
	bar.Finish();

	WriteGrdTEC<DIM_2D> wtec( &mGrid);
	wtec.DoWrite( "cspacegrid_2d.plt");

}



template <>
void GridControlSpace<DIM_3D,DIM_3D>::Init( const GridContext<UMetric>& cxmet, const BndGrid<DIM_3D>& bnd)
{
	GeneratorData<DIM_3D> kerdata;
	Kernel<DIM_3D> kernel( mGrid, kerdata);


	ProgressBar	bar(40);

	bar.Init( bnd.SizeNodeTab() );
	bar.Start();


	GVect	vmin, vmax;

	bnd.FindMinMax( vmin, vmax);
	
	kernel.InitBoundingBox( vmin, vmax);

	kernel.InitCube();

	// setting metric for corner nodes of the bounding box
	for ( Grid<DIM_3D>::ColNode::iterator itr = mGrid.rColNode().begin(); itr != mGrid.rColNode().end(); ++itr)
	{
		GMetric	met;
		met.InitIso(1.0);

		SMtx smtx( met);
		DecomposeROOT( smtx, smtx);

		MGSize idm = mUMetricCx.Insert( smtx );
		//mMetricCx.Insert( idm, smtx );
		itr->rMasterId() = idm;
	}



	// triangulation of the points
	vector<MGSize>	tabid(bnd.SizeNodeTab());
	for ( MGSize i=1; i<bnd.SizeNodeTab()+1; ++i)
		tabid[i-1] = i;

	random_shuffle( tabid.begin(), tabid.end() );


	for ( MGSize ii=0; ii<tabid.size(); ++ii, ++bar)
	{
		MGSize i = tabid[ii];

		GVect vct = bnd.cNode(i).cPos();


		MGSize inod = kernel.InsertPoint( vct );

		if ( ! inod)
		{
			char sbuf[512];
			sprintf( sbuf, "CRASH :: kernel failed to insert new point with coordinates [%lg %lg %lg]", 
							vct.cX(), vct.cY(), vct.cZ() );

			THROW_INTERNAL( sbuf);
		}

		USMtx smtx( cxmet.cData( bnd.cNode(i).cMasterId() ) );
		DecomposeROOT( smtx, smtx);

		MGSize idm = mUMetricCx.Insert( smtx );
		//mMetricCx.Insert( idm, smtx );
		mGrid.rNode(inod).rMasterId() = idm;

		//mpGrd->rNode(inod).rMasterId() = bnd.cNode(i).cMasterId();


		//wtec.DoWrite( "_tmp_grd.plt");


		//fprintf( f, "node id = %d\n", i);
		//smtx = SMatrix<MGFloat,3>( bnd.cNode(i).cMetric());
		//smtx.Decompose( smtxD, smtxL);
		//smtxD.Write( f);
		//smtxL.Write( f);
	}
	
	bar.Finish();

	WriteGrdTEC<DIM_3D> wtec( &mGrid);
	wtec.DoWrite( "cspacegrid_3d.plt");

}


template <Dimension DIM, Dimension UNIDIM>
void GridControlSpace<DIM,UNIDIM>::InitFileMEANDR( const MGString& fname)
{
	THROW_INTERNAL( "Not implemented");
}

template <>
void GridControlSpace<DIM_2D,DIM_2D>::InitFileMEANDR( const MGString& fname)
{
	GridContext<UMetric>	cxUMetric;
	ReadMEANDR<DIM_2D>	reader( mGrid, cxUMetric );
	reader.DoRead( fname);

	for ( MGSize i=1; i<=cxUMetric.Size(); ++i)
	{
		USMtx smtx(cxUMetric.cData(i));
		DecomposeROOT( smtx, smtx);
		mUMetricCx.Insert( i, smtx);
	}
}

template <>
void GridControlSpace<DIM_3D,DIM_3D>::InitFileMEANDR( const MGString& fname)
{
	GridContext<UMetric>	cxUMetric;
	ReadMEANDR<DIM_3D>	reader( mGrid, cxUMetric );
	reader.DoRead( fname);

	for ( MGSize i=1; i<=cxUMetric.Size(); ++i)
	{
		USMtx smtx(cxUMetric.cData(i));
		DecomposeROOT( smtx, smtx);
		mUMetricCx.Insert( i, smtx);
	}
}


template <Dimension DIM, Dimension UNIDIM>
void GridControlSpace<DIM,UNIDIM>::ExportFileMEANDR( const MGString& fname)
{
	THROW_INTERNAL( "Not implemented");
}

template <>
void GridControlSpace<DIM_2D,DIM_2D>::ExportFileMEANDR( const MGString& fname)
{
	GridContext<UMetric>	cxUMetric;

	for ( MGSize i=1; i<=mUMetricCx.Size(); ++i)
	{
		USMtx smtx( mUMetricCx.cData(i) );
		DecomposeSQR( smtx, smtx);
		cxUMetric.Insert( i, UMetric(smtx) );
	}

	WriteMEANDR<DIM_2D>	writer( mGrid, cxUMetric );
	writer.DoWrite( fname);
}

template <>
void GridControlSpace<DIM_3D,DIM_3D>::ExportFileMEANDR( const MGString& fname)
{
	GridContext<UMetric>	cxUMetric;

	for ( MGSize i=1; i<=mUMetricCx.Size(); ++i)
	{
		USMtx smtx( mUMetricCx.cData(i) );
		DecomposeSQR( smtx, smtx);
		cxUMetric.Insert( i, UMetric(smtx) );
	}

	WriteMEANDR<DIM_3D>	writer( mGrid, cxUMetric );
	writer.DoWrite( fname);
}




template <Dimension DIM, Dimension UNIDIM>
void GridControlSpace<DIM,UNIDIM>::ApplyScale( const MGFloat& scale)
{
	MGFloat c = 1. / sqrt( scale);
	for ( MGSize i=1; i<=mUMetricCx.Size(); ++i)
	{
		USMtx smtx( mUMetricCx.cData(i) );
		smtx *= c;
		mUMetricCx.Insert( i, smtx);
	}
}


template class GridControlSpace<DIM_1D,DIM_2D>;
template class GridControlSpace<DIM_2D,DIM_2D>;

template class GridControlSpace<DIM_1D,DIM_3D>;
template class GridControlSpace<DIM_2D,DIM_3D>;
template class GridControlSpace<DIM_3D,DIM_3D>;

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

