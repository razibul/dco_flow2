#ifndef __OPERATOR3D_RELOCATENODEE_H__
#define __OPERATOR3D_RELOCATENODEE_H__

#include "libcoresystem/mgdecl.h"
#include "libgreen/tdefs.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Dimension DIM> 
class Grid;

template <Dimension DIM> 
class Kernel;


//////////////////////////////////////////////////////////////////////
// class Operator3D_RelocateNode
//////////////////////////////////////////////////////////////////////
class Operator3D_RelocateNode
{
	typedef TDefs<DIM_3D>::GVect	GVect;
	typedef TDefs<DIM_3D>::GMetric	GMetric;

	typedef TDefs<DIM_3D>::GNode	GNode;
	typedef TDefs<DIM_3D>::GCell	GCell;
	typedef TDefs<DIM_3D>::GFace	GFace;

public:
	Operator3D_RelocateNode( Kernel<DIM_3D>& kernel) : mKernel(kernel), mErrorCode(0), mQBefore(0.0), mQAfter(0.0), mVolMinBefore(0.0), mVolMinAfter(0.0)	{}


	void	Reset();

	void	Init( const MGSize& idnod, const GVect& pos);

	void	Execute();

	const MGSize&	Error() const			{ return mErrorCode;}
	const MGFloat&	QBefore() const			{ return mQBefore;}
	const MGFloat&	QAfter() const			{ return mQAfter;}
	const MGFloat&	VolMinBefore() const	{ return mVolMinBefore;}
	const MGFloat&	VolMinAfter() const		{ return mVolMinAfter;}

private:
	Kernel<DIM_3D>	&mKernel;

	MGSize			mIdNode;
	GVect			mPos;

	vector<MGSize>	mtabBall;

	MGSize			mErrorCode;

	MGFloat			mQBefore;
	MGFloat			mQAfter;

	MGFloat			mVolMinBefore;
	MGFloat			mVolMinAfter;

};


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __OPERATOR3D_RELOCATENODEE_H__
