#include "kernel.h"
#include "libgreen/grid.h"
#include "libgreen/gridgeom.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



void Kernel<DIM_1D>::InitBoundingBox( const GVect& vmin, const GVect& vmax)
{
	mBox.rVMax() = vmax;
	mBox.rVMin() = vmin;
}

void Kernel<DIM_1D>::InitCube( const GVect& v1, const GVect& v2)
{
	mpGrd->InsertNode( GNode( v1 ) );
	mpGrd->InsertNode( GNode( v2 ) );

	GCell	cell;

	// cell 1
	cell.rNodeId( 0) = 1;
	cell.rNodeId( 1) = 2;

	mpGrd->InsertCell( cell);
}

void Kernel<DIM_1D>::InitCube()
{
}

MGSize Kernel<DIM_1D>::InsertPoint( const GVect& vct, const MGSize& icell, const GMetric& met)
{
	MGSize ic = icell==0 ? 1 : icell;
	

	do
	{
		Simplex<DIM_1D> simp = GridGeom<DIM_1D>::CreateSimplex( mpGrd->cCell( ic), *mpGrd);
		if ( simp.IsInside( vct) )
			break;
		
		++ic;
	}
	while ( ic <= mpGrd->SizeCellTab() );
	//while ( ic < mpGrd->SizeNodeTab() );

	if ( ! (GridGeom<DIM_1D>::CreateSimplex( mpGrd->cCell( ic), *mpGrd)).IsInside( vct) )
		THROW_INTERNAL( "cell not found !!!" );

	GCell	&oldcell = mpGrd->rCell( ic);

	const MGSize in1 = oldcell.cNodeId( 0);
	const MGSize in2 = oldcell.cNodeId( 1);

	MGSize inod = mpGrd->InsertNode( GNode( vct ) );

	mpGrd->EraseCell( ic);

	GCell	cell;

	// cell 1
	cell.rNodeId( 0) = in1;
	cell.rNodeId( 1) = inod;
	mpGrd->InsertCell( cell);

	// cell 2
	cell.rNodeId( 0) = inod;
	cell.rNodeId( 1) = in2;
	mpGrd->InsertCell( cell);

	return inod;
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

