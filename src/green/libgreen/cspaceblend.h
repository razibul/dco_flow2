#ifndef __CSPACEBLEND_H__
#define __CSPACEBLEND_H__


#include "libgreen/controlspace.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
//	class BlendControlSpace
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class BlendControlSpace : public ControlSpace<DIM>
{
	typedef typename TDefs<DIM>::GVect		GVect;
	typedef typename TDefs<DIM>::GMetric	GMetric;

public:
	BlendControlSpace( const ControlSpace<DIM>* pcs1, const ControlSpace<DIM>* pcs2) : mpCS1(pcs1), mpCS2(pcs2)	{}

	virtual GMetric	GetSpacing( const GVect& vct) const;

private:
	const ControlSpace<DIM>*	mpCS1;
	const ControlSpace<DIM>*	mpCS2;
};



template <Dimension DIM>
inline typename TDefs<DIM>::GMetric BlendControlSpace<DIM>::GetSpacing( const GVect& vct) const
{
	GMetric met1  = mpCS1->GetSpacing( vct);
	GMetric met2 = mpCS2->GetSpacing( vct);
	GMetric met;

	IntersectMetric( met, met1, met2);

	return met;
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __CSPACEBLEND_H__
