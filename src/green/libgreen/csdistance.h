#ifndef __CSDISTANCE_H__
#define __CSDISTANCE_H__

#include "libcoresystem/mgdecl.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

class CSDistance
{
public:
	CSDistance( const MGFloat& h=1.0, const MGFloat& a=0.0) : mBaseCoeff(h), mGrowth(a)	{}

	MGFloat	Coeff( const MGFloat& x) const	{ return  1.0 + mGrowth / mBaseCoeff * x;}

private:
	MGFloat	mBaseCoeff;
	MGFloat	mGrowth;
};



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __CSDISTANCE_H__
