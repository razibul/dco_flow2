#ifndef __READTEC_H__
#define __READTEC_H__

#include "libcoresystem/mgdecl.h"
#include "libcoregeom/dimension.h"

#include "libgreen/tdefs.h"
#include "libgreen/gridcontext.h"

#include "libgreen/grid_ioproxy.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

using namespace Geom;


template <Dimension DIM>
class Grid;



//////////////////////////////////////////////////////////////////////
// class ReadTEC
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class ReadTEC
{
	typedef typename TDefs<DIM>::GMetric::SMtx	SMtx;

public:
	ReadTEC( IOReadGreenProxy<DIM>& grdproxy, GridContext<SMtx>* pcxt=NULL) : mGrdProxy(grdproxy), mpContext(pcxt)	{}

	void	DoRead( const MGString& fname);

protected:
	void	ReadHeader( istream& file);
	void	ReadNodes( istream& file);
	void	ReadCells( istream& file);

	void	SetTitle( const MGString& str)		{ mTitle = str;}
	void	SetVariables( const MGString& str);

private:
	IOReadGreenProxy<DIM>&			mGrdProxy;
	GridContext<SMtx>*	mpContext;

	MGString			mFName;

	MGString			mTitle;
	vector<MGString>	mtabVars;

	MGString			mZoneT;
	MGString			mElemT;

	MGSize				mnNode;
	MGSize				mnElem;
};
//////////////////////////////////////////////////////////////////////

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __READTEC_H__
