#ifndef __RECONSTRUCTOR_CDT_H__
#define __RECONSTRUCTOR_CDT_H__


#include "libcoresystem/mgdecl.h"
#include "libgreen/tdefs.h"

#include "libgreen/recface_cdt.h"
#include "libgreen/protectentities.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



template <Dimension DIM>
class ControlSpace;

template <Dimension DIM> 
class Grid;

template <Dimension DIM> 
class BndGrid;

template <Dimension DIM> 
class Kernel;



//////////////////////////////////////////////////////////////////////
// class EdgeCDT
//////////////////////////////////////////////////////////////////////
class EdgeCDT
{
	typedef TDefs<DIM_3D>::GVect	GVect;
	typedef TDefs<DIM_3D>::GNode	GNode;
	typedef TDefs<DIM_3D>::GCell	GCell;
	typedef TDefs<DIM_3D>::GFace	GFace;

public:
	EdgeCDT() : mBEdgeId(0), mType(0), mIdAcuNode(0), mCellId(0)		{}
	EdgeCDT( const Key<2>& edge, const MGSize& ibe, const MGSize& t=0, const MGSize& idn=0) : mEdge(edge), mBEdgeId(ibe), mType(t), mIdAcuNode(idn), mCellId(0)		{}

	const Key<2>&	cEdge()	const		{ return mEdge; }
	const bool		IsNew() const		{ return mType == 0; }

	const MGSize&	cType() const		{ return mType; }
	const MGSize&	cIdAcuNode() const	{ return mIdAcuNode; }

	const MGSize&	cCellId() const			{ return mCellId; }
	MGSize&			rCellId()				{ return mCellId; }

	const MGSize&	cBndEdgeId() const		{ return mBEdgeId; }

private:
	Key<2>	mEdge;
	MGSize	mBEdgeId;

	MGSize	mType;
	MGSize	mIdAcuNode;

	MGSize	mCellId;
};


inline bool operator == ( const EdgeCDT& e1, const EdgeCDT& e2 )
{
	return ( e1.cEdge() == e2.cEdge() );
}

inline bool operator < ( const EdgeCDT& e1, const EdgeCDT& e2 )
{
	return ( e1.cEdge() < e2.cEdge() );
}



//////////////////////////////////////////////////////////////////////
// class ReconstructorCDT
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class ReconstructorCDT
{
};



//////////////////////////////////////////////////////////////////////
// class ReconstructorCDT
//////////////////////////////////////////////////////////////////////
template <>
class ReconstructorCDT<DIM_3D>
{
	typedef TDefs<DIM_3D>::GVect	GVect;
	typedef TDefs<DIM_3D>::GMetric	GMetric;

	typedef TDefs<DIM_3D>::GNode	GNode;
	typedef TDefs<DIM_3D>::GCell	GCell;
	typedef TDefs<DIM_3D>::GFace	GFace;

	friend class FaceCDT;

public:
	ReconstructorCDT( const BndGrid<DIM_3D>& bgrd, const ControlSpace<DIM_3D>& cspace, Kernel<DIM_3D>& kernel) 
		: mpBndGrd(&bgrd), mpCSpace(&cspace), mpKernel(&kernel)		{}

	void	Init();
	void	Reconstruct();
	void	UpdateBoundaryGrid( BndGrid<DIM_3D>& bgrid);

	vector< Key<2> >&	rTabBnSubEdges()	{ return mtabBndSubEdges;}
	vector< Key<3> >&	rTabBnSubFaces()	{ return mtabBndSubFaces;}

	const vector< KeyData< Key<2>, vector< Key<2> > > >&	cTabCompEdge() const	{ return mtabCompEdge;}


protected:
	void	InitEdges();
	void	FindMissingEdges();

	void	InitCompEdges();
	MGSize	ReconstructEdges();

	void	FlagAcuteNodes( const MGFloat& angle);
	MGSize	SplitEdges();

	MGSize	InsertPoint( const GVect& vct);

//	void	FindGuiltyPoint( MGSize& inod, const Key<2>& edge);
	void	FindSplitPoint_T1( GVect& vct, const Key<2>& edge, const MGSize& ipnt);
	bool	FindSplitPoint_T2( GVect& vct, const Key<2>& edge, const MGSize& ipnt, const MGSize& idnacu);

	bool	TestEdgeEncrochement( MGFloat& ccos, const Key<2>& edge, const MGSize& ipnt);
	bool	FindEdgePipeExtended( bool& brec, MGSize& inodep, vector<MGSize>& tabnodep,  vector<MGSize>& tabcell, EdgeCDT& edgecdt, const MGSize& icstart);

	
	void	SearchForDegeneracies();
	
	void	InitFaces();
	void	FindMissingFaces();

	bool	FindSubFacePipe( bool& brec, vector<MGSize>& tabcell, Key<3>& facecdt, const MGSize& icell);
	bool	FindFacePipeExtended( FaceCDT& facecdt);

	void	GenerateFaceGrid( FaceCDT& facecdt);

private:
	const BndGrid<DIM_3D>		*mpBndGrd;
	const ControlSpace<DIM_3D>	*mpCSpace;
	Kernel<DIM_3D>				*mpKernel;

	vector<bool>		mtabIsAcute;
	//vector<MGSize>		mtabNodeCellId;

	// edges
	vector< Key<2> >	mtabBndEdges;
	vector<EdgeCDT>		mtabSplitEdges;
	vector<MGSize>		mtabRecEdges;		// id of the face in mtabSplitEdges

	vector< KeyData< Key<2>, vector< Key<2> > > >	mtabCompEdge;

	// faces
	vector< Key<3> >	mtabBndFaces;
	vector<FaceCDT>		mtabSplitFaces;
	vector< Key<3> >	mtabSubFacesMissing;

	vector<SubFaceCDT>	mtabSubFaces;

	vector< Key<2> >	mtabBndSubEdges;
	vector< Key<3> >	mtabBndSubFaces;
};
//////////////////////////////////////////////////////////////////////


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __RECONSTRUCTOR_CDT_H__
