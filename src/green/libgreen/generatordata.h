#ifndef __GENERATORDATA_H__
#define __GENERATORDATA_H__

#include "libcoresystem/mgdecl.h"
#include "libgreen/tdefs.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

template <Dimension DIM>
class Generator;


//////////////////////////////////////////////////////////////////////
// class GeneratorData
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class GeneratorData
{
	typedef typename TDefs<DIM>::GMetric	GMetric;

public:
	void	InsertMetric( const GMetric& met, const MGSize& id);
	
	const GMetric&	cMetric( const MGSize& id) const		{ return mtabMetric[id];};

protected:
	vector<GMetric>&	rColMetric()	{ return mtabMetric;}

private:
	vector<GMetric>	mtabMetric;
};



template <Dimension DIM>
inline void GeneratorData<DIM>::InsertMetric( const GMetric& met, const MGSize& id)
{
	if ( id >= mtabMetric.size() )
		mtabMetric.resize( id + 1);

	mtabMetric[id] = met;
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __GENERATORDATA_H__
