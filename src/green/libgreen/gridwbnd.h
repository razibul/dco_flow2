#ifndef __GRIDWBND_H__
#define __GRIDWBND_H__

#include "libgreen/grid.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
//	class GridWBnd
//////////////////////////////////////////////////////////////////////
template <Dimension DIM> 
class GridWBnd : public Grid<DIM>
{
	friend class Generator<DIM>;

public:
	GridWBnd() : Grid<DIM>()		{}

	const BndGrid<DIM>&	cBndGrid() const		{ return mBndGrid;}
	BndGrid<DIM>&		rBndGrid()				{ return mBndGrid;}


private:
	BndGrid<DIM> mBndGrid;
};





//////////////////////////////////////////////////////////////////////
//	class GridWBnd
//////////////////////////////////////////////////////////////////////
template <> 
class GridWBnd<DIM_0D> : public Grid<DIM_0D>
{
public:
	GridWBnd() : Grid<DIM_0D>()		{}

private:
};


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __GRIDWBND_H__
