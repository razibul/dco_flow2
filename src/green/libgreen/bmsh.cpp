#include "bmsh.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 




template <Dimension DIM> 
void AddTopoEnts( const GET::TopoEntity<DIM_0D>& tent, const GET::GeoTopoMaster<DIM>& getMaster, vector< vector<MGSize> >& tabTopo )
{
}


template <Dimension DIM, Dimension LDIM> 
void AddTopoEnts( const GET::TopoEntity<LDIM>& tent, const GET::GeoTopoMaster<DIM>& getMaster, vector< vector<MGSize> >& tabTopo )
{
	for ( MGSize iloop=0; iloop<tent.cTab().size(); ++iloop)
	{
		const GET::TopoLoop	&loop = tent.cTab()[iloop].first;
		const bool &bloop = tent.cTab()[iloop].second;

		for ( MGSize iedg=0; iedg<loop.size(); ++iedg)
		{
			MGSize ide = loop[iedg].first;
			tabTopo[LDIM-1].push_back( ide);

			const GET::TopoEntity< static_cast<Dimension>(LDIM-1) >& loctent = getMaster.cTopo().template cTEnt< static_cast<Dimension>(LDIM-1) >( ide);
			//AddTopoEnts< DIM, static_cast<Dimension>(LDIM-1) >( loctent, getMaster, tabTopo );
			AddTopoEnts( loctent, getMaster, tabTopo );
		}
	}

}





template <Dimension DIM> 
void BMesh<DIM>::Init( const MGSize& topoid)
{
	mtabTopo[DIM].push_back( topoid);

	const GET::TopoEntity<DIM>& tent = mGeTMaster.cTopo().template cTEnt<DIM>( topoid);

	AddTopoEnts<DIM>( tent, mGeTMaster, mtabTopo );

	for ( MGSize idim=0; idim<mtabTopo.size(); ++idim)
	{
		sort( mtabTopo[idim].begin(), mtabTopo[idim].end() );
		mtabTopo[idim].erase( unique( mtabTopo[idim].begin(), mtabTopo[idim].end() ), mtabTopo[idim].end() );

	}

	InitFaceOrient( topoid);
	InitTabNode();

	// dump
	for ( MGSize idim=0; idim<mtabTopo.size(); ++idim)
	{
		cout << setw(4) << idim << " : ";
		for ( MGSize i=0; i<mtabTopo[idim].size(); ++i)
			cout << " " << mtabTopo[idim][i];

		cout << endl;
	}
}


template <Dimension DIM> 
void BMesh<DIM>::InitFaceOrient( const MGSize& topoid)
{
	const GET::TopoEntity<DIM>& tent = mGeTMaster.cTopo().template cTEnt<DIM>( topoid);

	for ( MGSize iloop=0; iloop<tent.cTab().size(); ++iloop)
	{
		const GET::TopoLoop	&loop = tent.cTab()[iloop].first;
		const bool &bloop = tent.cTab()[iloop].second;

		for ( MGSize iedg=0; iedg<loop.size(); ++iedg)
		{
			MGSize ide = loop[iedg].first;
			bool bedge = loop[iedg].second;

			if ( ! bloop ) 
				bedge = ! bedge;

			mmapFaceOrient[ide] = bedge;
		}
	}
}


template <Dimension DIM> 
void BMesh<DIM>::InitTabNodeLoc0D()
{
	for ( MGSize i=0; i<mtabTopo[0].size(); ++i)
	{
		const MGSize ide = mtabTopo[0][i];
		const GridWBnd<DIM_0D> &grid = mGrdMaster.template cGrid<DIM_0D>( ide);

		const MGSize idm = grid.cMasterId();

		if ( idm == 0 )
			THROW_INTERNAL( "BMesh<DIM>::InitTabNodeLoc0D :: invalid master id ");

		if ( mmapMaster.find( idm) != mmapMaster.end() )
			THROW_INTERNAL( "BMesh<DIM>::InitTabNodeLoc0D :: duplicate master id found ");

		BMeshNode<DIM> bnode;

		bnode.mLocalDim = DIM_0D;
		bnode.mEntId = ide;
		bnode.mMasterPos = mGrdMaster.cMasterPosCx().cData( idm);

		mtabNode.push_back( bnode);
		mmapMaster.insert( map<MGSize,MGSize>::value_type( idm, mtabNode.size()-1 ) );
	}
}


template <Dimension DIM> 
template <Dimension LDIM> 
void BMesh<DIM>::InitTabNodeLoc()
{
	for ( MGSize i=0; i<mtabTopo[LDIM].size(); ++i)
	{
		const MGSize ide = mtabTopo[LDIM][i];
		const GridWBnd<LDIM> &grid = mGrdMaster.template cGrid<LDIM>( ide);
		
		for ( typename GridWBnd<LDIM>::ColNode::const_iterator itr = grid.cColNode().begin(); itr != grid.cColNode().end(); ++itr)
		{
			const MGSize idm = itr->cMasterId();

			if ( idm == 0 )
				THROW_INTERNAL( "BMesh<DIM>::InitTabNodeLoc<" << LDIM << " :: invalid master id ");

			if ( mmapMaster.find( idm) != mmapMaster.end() )
				continue;

			BMeshNode<DIM> bnode;

			bnode.mLocalDim = LDIM;
			bnode.mEntId = ide;
			bnode.mMasterPos = mGrdMaster.cMasterPosCx().cData( idm);

			for ( MGSize i=0; i<LDIM; ++i)
				bnode.mLocalPos.rX(i) = itr->cPos().cX(i);

			mtabNode.push_back( bnode);
			mmapMaster.insert( map<MGSize,MGSize>::value_type( idm, mtabNode.size()-1 ) );
		}
	}
}


template <> 
void BMesh<DIM_2D>::InitTabNode()
{
	InitTabNodeLoc0D();
	InitTabNodeLoc<DIM_1D>();
}

template <> 
void BMesh<DIM_3D>::InitTabNode()
{
	InitTabNodeLoc0D();
	InitTabNodeLoc<DIM_1D>();
	InitTabNodeLoc<DIM_2D>();
}




template <Dimension DIM> 
template <Dimension LDIM> 
void BMesh<DIM>::WriteGridCells( ostream& file)
{
	file << "GRID_" << setw(1) << LDIM << "D" << endl;
	file << mtabTopo[LDIM].size() << endl;

	for ( MGSize i=0; i<mtabTopo[LDIM].size(); ++i)
	{
		const MGSize ide = mtabTopo[LDIM][i];
		const GridWBnd<LDIM> &grid = mGrdMaster.template cGrid<LDIM>( ide);

		file << endl;
		file << ide << " " << grid.cColCell().size_valid() << endl;
		for ( typename GridWBnd<LDIM>::ColCell::const_iterator itr = grid.cColCell().begin(); itr != grid.cColCell().end(); ++itr)
		{
			MGSize tabnid[TDefs<LDIM>::GCell::SIZE];
			for ( MGSize in=0; in<TDefs<LDIM>::GCell::SIZE; ++in)
				tabnid[in] = itr->cNodeId(in);

			if ( LDIM == DIM - 1 )
				if ( mmapFaceOrient[ide] == false )
					swap( tabnid[TDefs<LDIM>::GCell::SIZE-1], tabnid[TDefs<LDIM>::GCell::SIZE-2] );

			for ( MGSize in=0; in<TDefs<LDIM>::GCell::SIZE; ++in)
			{
				//const typename TDefs<LDIM>::GNode& node = grid.cNode( itr->cNodeId(in) );
				const typename TDefs<LDIM>::GNode& node = grid.cNode( tabnid[in] );

				map<MGSize,MGSize>::const_iterator citr = mmapMaster.find( node.cMasterId() );
				if ( citr == mmapMaster.end() )
					THROW_INTERNAL( "BMesh<DIM>::WriteGridCells<" << LDIM << "> :: master node not found");

				file << citr->second + 1 << " ";
				//MGSize id = citr->second;
			}
			file << endl;
		}

	}
}


template <> 
void BMesh<DIM_2D>::WriteGrids( ostream& file)
{
	WriteGridCells<DIM_1D>( file);
}

template <> 
void BMesh<DIM_3D>::WriteGrids( ostream& file)
{
	WriteGridCells<DIM_1D>( file);
	WriteGridCells<DIM_2D>( file);
}


template <Dimension DIM> 
void BMesh<DIM>::DoWrite( const MGString& fname )
{
	ofstream ofile;
	ofile.open( fname.c_str(), ios::out );

	try
	{
		if ( ! ofile)
			THROW_FILE( "can not open the file", fname);


		cout << "\nWriting BMSH " << fname << endl;
		cout.flush();

		// header
		ofile << "BMSH_VER 1.0" << endl;
		ofile << setw(1) << DIM << "D" << endl;
		ofile << mGeTMaster.cName() << " - " << mGeTMaster.cDescription() << endl;

		// nodes
		ofile << mtabNode.size() << endl;
		for ( MGSize i=0; i<mtabNode.size(); ++i)
		{
			const BMeshNode<DIM>& bnode = mtabNode[i];

			for ( MGSize idim=0; idim<DIM; ++idim)
				ofile << setw(24) << setprecision(16) << bnode.mMasterPos.cX(idim) << " ";

			ofile << " " << bnode.mLocalDim << " " << bnode.mEntId << "  ";

			for ( MGSize idim=0; idim<static_cast<MGSize>( bnode.mLocalDim); ++idim)
				ofile << setw(24) << setprecision(16) << bnode.mLocalPos.cX(idim) << " ";

			ofile << endl;
		}

		// cells
		WriteGrids( ofile);

		cout << "Writing BMSH " << fname <<  " - FINISHED" << endl;
	}
	catch ( EHandler::Except& e)
	{
		cout << "ERROR: BMesh<DIM>::DoWrite - file name: " << fname << endl << endl;
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);

		THROW_INTERNAL("END");
	}
}


//template <Dimension DIM> 
//template <Dimension LDIM> 
//void BMesh<DIM>::AddTopoEnts( const GET::TopoEntity<LDIM>& tent )
//{
//	for ( MGSize iloop=0; iloop<tent.cTab().size(); ++iloop)
//	{
//		const GET::TopoLoop	&loop = tent.cTab()[iloop].first;
//		const bool &bloop = tent.cTab()[iloop].second;
//
//		for ( MGSize iedg=0; iedg<loop.size(); ++iedg)
//		{
//			MGSize ide = loop[iedg].first;
//			mtabTopo[LDIM-1].push_back( ide);
//
//			const GET::TopoEntity<static_cast<Dimension>(LDIM-1)>& loctent = mGeTMaster.cTopo().template cTEnt<static_cast<Dimension>(LDIM-1)>( ide);
//			//AddTopoEnts<static_cast<Dimension>(LDIM-1)>( loctent );
//		}
//	}
//
//}
//
//
//template <Dimension DIM> 
//template <> 
//void BMesh<DIM>::AddTopoEnts( const GET::TopoEntity<DIM_0D>& tent )
//{
//}


template class BMesh<DIM_2D>;
template class BMesh<DIM_3D>;


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

