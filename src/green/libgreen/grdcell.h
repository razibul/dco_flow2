#ifndef __GRDCELL_H__
#define __GRDCELL_H__

#include "libcoresystem/mgdecl.h"
#include "libgreen/tdefs.h"
#include "libcoregeom/simplex.h"
#include "libgreen/basecell.h"
#include "libgreen/grdface.h"
#include "libcorecommon/key.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

template <Dimension DIM> 
class Grid;

//////////////////////////////////////////////////////////////////////
//	class Cell
//////////////////////////////////////////////////////////////////////
template <Dimension DIM> 
class Cell : public BaseCell<DIM>, public Base
{
	typedef typename TDefs<DIM>::GCell	GCell;
	typedef typename TDefs<DIM>::GFace	GFace;

public:
	Cell()		{ Reset(); }
 
	bool		IsPainted() const				{ return mbFlags.test(0);}
	void		Paint()							{ mbFlags.set(0,true);}
	void		WashPaint()						{ mbFlags.set(0,false);}

	bool		IsExternal() const				{ return mbFlags.test(1);}
	void		SetExternal( const bool& flag)	{ mbFlags.set(1,flag);}

	bool		IsModified() const				{ return mbFlags.test(2);}
	void		SetModified( const bool& flag)	{ mbFlags.set(2,flag);}

	bool		IsGood() const					{ return mbFlags.test(3);}
	void		SetGood( const bool& flag)		{ mbFlags.set(3,flag);}

	bool		IsTree() const					{ return mbFlags.test(4);}
	void		SetTree( const bool& flag)		{ mbFlags.set(4,flag);}

	const MGSize&	cFaceNodeId( const MGSize& ifc, const MGSize& in) const;	
	const MGSize&	cEdgeNodeId( const MGSize& iec, const MGSize& in) const;


	void		GetFaceNeisVNOut( FaceNeighbours& fac, const MGSize& i) const;
	void		GetFaceNeisVNIn( FaceNeighbours& fac, const MGSize& i) const;

	void		GetFaceVNOut( GFace& fac, const MGSize& i) const;
	void		GetFaceVNIn( GFace& fac, const MGSize& i) const;


	Key<DIM>	FaceKey( const MGSize& i) const;
	Key<2>		FaceKeyL( const MGSize& i) const;	// it is valid only for DIM = 3D

	Key<2>		EdgeKey( const MGSize& i) const;	// it is valid only for DIM = 3D


	void		Reset();

	void		Dump( const Grid<DIM>& grd, ofstream& f=cout) const; 

private:

	// 0 - color
	// 1 - external
	// 2 - modified
	// 3 - good
	// 4 - tree
	bitset<8>	mbFlags;

};
//////////////////////////////////////////////////////////////////////



template <Dimension DIM> 
inline void Cell<DIM>::Reset()
{
	BaseCell<DIM>::Reset();

	mbFlags.reset();
}

//////////////////////////////////////////////////////////////////////
// DIM_2D
//////////////////////////////////////////////////////////////////////
template <> 
inline const MGSize& Cell<DIM_2D>::cFaceNodeId( const MGSize& ifc, const MGSize& in) const	
{ 
	return cNodeId( Simplex<DIM_2D>::cFaceConn(ifc,in) );
}


template <> 
inline void Cell<DIM_2D>::GetFaceVNOut( GFace& fac, const MGSize& i) const
{
	fac.rNodeId(0) = cFaceNodeId(i,0);
	fac.rNodeId(1) = cFaceNodeId(i,1);

	GNeDef	neup = cCellId(i);
	GNeDef	nelo;
	nelo.first = cId();
	nelo.second = i;

	fac.rCellLo() = nelo;
	fac.rCellUp() = neup;
}

template <> 
inline void Cell<DIM_2D>::GetFaceVNIn( GFace& fac, const MGSize& i) const
{
	fac.rNodeId(0) = cFaceNodeId(i,1);
	fac.rNodeId(1) = cFaceNodeId(i,0);

	GNeDef	neup = cCellId(i);
	GNeDef	nelo;
	nelo.first = cId();
	nelo.second = i;

	fac.rCellLo() = neup;
	fac.rCellUp() = nelo;
}

template <> 
inline Key<2> Cell<DIM_2D>::FaceKey( const MGSize& i) const
{
	return Key<2>( cFaceNodeId( i, 0), cFaceNodeId( i, 1) );
}


//////////////////////////////////////////////////////////////////////
// DIM_3D
//////////////////////////////////////////////////////////////////////

template <> 
inline const MGSize& Cell<DIM_3D>::cFaceNodeId( const MGSize& ifc, const MGSize& in) const	
{ 
	return cNodeId( Simplex<DIM_3D>::cFaceConn(ifc,in) );
}

template <> 
inline const MGSize& Cell<DIM_3D>::cEdgeNodeId( const MGSize& iec, const MGSize& in) const	
{ 
	return cNodeId( Simplex<DIM_3D>::cEdgeConn(iec,in) );
}



template <> 
inline void Cell<DIM_3D>::GetFaceNeisVNOut( FaceNeighbours& fac, const MGSize& i) const
{
	GNeDef	neup = cCellId(i);
	GNeDef	nelo;
	nelo.first = cId();
	nelo.second = i;

	fac.rCellLo() = nelo;
	fac.rCellUp() = neup;
}


template <> 
inline void Cell<DIM_3D>::GetFaceNeisVNIn( FaceNeighbours& fac, const MGSize& i) const
{
	GNeDef	neup = cCellId(i);
	GNeDef	nelo;
	nelo.first = cId();
	nelo.second = i;

	fac.rCellLo() = neup;
	fac.rCellUp() = nelo;
}

template <> 
inline void Cell<DIM_3D>::GetFaceVNOut( GFace& fac, const MGSize& i) const
{
	fac.rNodeId(0) = cFaceNodeId(i,0);
	fac.rNodeId(1) = cFaceNodeId(i,1);
	fac.rNodeId(2) = cFaceNodeId(i,2);

	GNeDef	neup = cCellId(i);
	GNeDef	nelo;
	nelo.first = cId();
	nelo.second = i;

	fac.rCellLo() = nelo;
	fac.rCellUp() = neup;
}


template <> 
inline void Cell<DIM_3D>::GetFaceVNIn( GFace& fac, const MGSize& i) const
{
	fac.rNodeId(0) = cFaceNodeId(i,0);
	fac.rNodeId(1) = cFaceNodeId(i,2);
	fac.rNodeId(2) = cFaceNodeId(i,1);

	GNeDef	neup = cCellId(i);
	GNeDef	nelo;
	nelo.first = cId();
	nelo.second = i;

	fac.rCellLo() = neup;
	fac.rCellUp() = nelo;
}


template <> 
inline Key<2> Cell<DIM_3D>::FaceKeyL( const MGSize& i) const
{
	MGSize	k = 0;
	Key<2>	key;

	for ( MGSize inc=0; inc<DIM_3D; ++inc)
		if ( Simplex<DIM_3D>::cFaceConn(i,inc) != 3 )
			key.rElem( k++) = cFaceNodeId(i,inc);

	key.Sort();

	return key;
}

template <> 
inline Key<3> Cell<DIM_3D>::FaceKey( const MGSize& i) const
{
	return Key<3>( cFaceNodeId( i, 0), cFaceNodeId( i, 1), cFaceNodeId( i, 2) );
}


template <> 
inline Key<2> Cell<DIM_3D>::EdgeKey( const MGSize& i) const
{
	return Key<2>( cEdgeNodeId( i, 0), cEdgeNodeId( i, 1) );
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __GRDCELL_H__
