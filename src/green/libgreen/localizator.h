#ifndef __LOCALIZATORWALK_H__
#define __LOCALIZATORWALK_H__

#include "libcoresystem/mgdecl.h"
#include "libgreen/tdefs.h"
#include "libcoregeom/tree.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Dimension DIM> 
class Grid;

//////////////////////////////////////////////////////////////////////
//	class Localizator
//////////////////////////////////////////////////////////////////////
template <Dimension DIM> 
class Localizator
{
	typedef typename TDefs<DIM>::GVect		GVect;
	typedef typename TDefs<DIM>::GMetric	GMetric;

	typedef typename TDefs<DIM>::GNode		GNode;
	typedef typename TDefs<DIM>::GCell		GCell;
	typedef typename TDefs<DIM>::GFace		GFace;

public:
	Localizator( const MGSize& treesize=35, const MGSize& frq=90) 
		: mpGrid(NULL), mCellTree(treesize), mTreeFreq(frq), mTreeCounter(0)			{}

	Localizator( Grid<DIM>* pgrd, const MGSize& treesize=35, const MGSize& frq=90) 
		: mpGrid(pgrd), mCellTree(treesize), mTreeFreq(frq), mTreeCounter(0)		{}

	void			Init( Grid<DIM>* pgrd)	{ mpGrid = pgrd;}

	const MGSize&	cCounter() const		{ return mTreeCounter;}
	void			IncCounter()			{ ++mTreeCounter;}

	bool			TimeToInsert() const	{ return ( mTreeCounter % mTreeFreq == 0 );}


	void	InitBox( const Box<DIM>& box)						{ mCellTree.InitBox( box);}
	void	TreeInsert( const GVect& pos, const MGSize& dat)	{ mCellTree.Insert( pos, dat);}
	void	TreeInsertSafe( const GVect& pos, const MGSize& dat);
	void	TreeErase( const GVect& pos, const MGSize& dat)		{ mCellTree.Erase( pos, dat);}

	MGSize	Localize( const GVect& vct, const bool& bcrash=true) const;

private:
	Grid<DIM>			*mpGrid;			// applies modification to the grid by painting cells (inside walk algo)

	Tree<MGSize,DIM>	mCellTree;
	MGSize				mTreeFreq;			// insert every mTreeFreq point (used inside TomeToInsert())
	MGSize				mTreeCounter;		// number of tried points (used inside TomeToInsert())

};
//////////////////////////////////////////////////////////////////////


template <Dimension DIM> 
inline void Localizator<DIM>::TreeInsertSafe( const GVect& pos, const MGSize& dat)
{
	typename Tree<MGSize,DIM>::TPair	res;
	if ( mCellTree.GetClosestItem( res, pos) )
		if ( (res.first - pos).module() == 0.0 )
		{
			TRACE( "WARNING :: duplicate point in Localizator");
			return;
		}

	mCellTree.Insert( pos, dat);
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __LOCALIZATORWALK_H__
