#include "optimiser_smooth.h"
#include "libgreen/grid.h"
#include "libgreen/kernel.h"
#include "libcorecommon/progressbar.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Dimension DIM>
void OptimiserSmooth<DIM>::DoOptimise( const MGSize& maxiter )
{
}



template class OptimiserSmooth<DIM_2D>;
template class OptimiserSmooth<DIM_3D>;

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


