#include "recface_cdt.h"
#include "libgreen/gridgeom.h"
#include "libgreen/reconstructor_cdt.h"

#include "libgreen/writegrdtec.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



bool FaceCDT::IsRecoverd()
{
	vector< Key<2> > tabbnd;
	vector< Key<2> > taball;
	vector< Key<2> > tabinn;
	vector< Key<2> > tabdup;

	vector< Key<2> >::iterator itrend;

	// face boundary edges

	//for ( MGSize i=0; i<3; ++i)
	//	for ( MGSize k=0; k<mtabSplitEdge[i]->Size(); ++k)
	//	{
	//		Key<2> key = mtabSplitEdge[i]->cSubEdge( k);
	//		tabbnd.push_back( key);
	//	}

	tabbnd = mtabBEdges;
	sort( tabbnd.begin(), tabbnd.end() );

	// all face edges
	for ( MGSize i=0; i<mtabSubFaces.size(); ++i)
	{
		Key<3> face = mtabSubFaces[i].cFace();
		for ( MGSize k=0; k<3; ++k)
		{
			Key<2> edge( face.cFirst(), face.cSecond() );
			edge.Sort();

			taball.push_back( edge);

			face.Rotate(1);
		}
	}

	sort( taball.begin(), taball.end() );

	tabinn.resize( taball.size() );
	itrend = set_difference( taball.begin(), taball.end(), tabbnd.begin(), tabbnd.end(), tabinn.begin() );
	tabinn.erase( itrend, tabinn.end() );


	itrend = unique( taball.begin(), taball.end() );
	taball.erase( itrend, taball.end() );

	tabdup.resize( taball.size() );
	itrend = set_difference( tabinn.begin(), tabinn.end(), taball.begin(), taball.end(), tabdup.begin() );
	tabdup.erase( itrend, tabdup.end() );

	itrend = unique( tabinn.begin(), tabinn.end() );
	tabinn.erase( itrend, tabinn.end() );


	//mtabTmpEdges.clear();

	// search for all face boundary edges
	bool bBnd = true;
	for ( MGSize i=0; i<tabbnd.size(); ++i)
	{
		Key<2> edge = tabbnd[i];

		//if ( find( taball.begin(), taball.end(), edge) == taball.end() )
		if ( ! binary_search( taball.begin(), taball.end(), edge) )
		{
			bBnd = false;
			//mtabTmpEdges.push_back( edge );
		}
	}

	// test all face inner edges
	bool bInn = true;

	for ( vector< Key<2> >::iterator itr=tabinn.begin(); itr!=tabinn.end(); ++itr)
	{
		Key<2> edge = *itr;

		//if ( find( tabdup.begin(), tabdup.end(), edge) == tabdup.end() )
		if ( ! binary_search( tabdup.begin(), tabdup.end(), edge) )
		{
			bInn = false;
			//mtabTmpEdges.push_back( edge );
		}
	}


	if ( bBnd && bInn )
	{
		//mControlSum.first = mtabSplitEdge[0]->SizeInNodes() + mtabSplitEdge[1]->SizeInNodes() + mtabSplitEdge[2]->SizeInNodes();
		//mControlSum.second = mtabINodes.size();
		return true;
	}

	//cout << "mtabTmpEdges.size = " << mtabTmpEdges.size() << endl;

	//cout << "tabbnd.size = " << tabbnd.size() << endl;
	//cout << "taball.size = " << taball.size() << endl;
	//cout << "tabinn.size = " << tabinn.size() << endl;
	//cout << "tabdup.size = " << tabdup.size() << endl;

	//cout << endl;


	//ofstream of( "_dump.txt");

	//of << "tabbnd.size = " << tabbnd.size() << endl;
	//for ( MGSize i=0;i<tabbnd.size();++i)
	//	of << tabbnd[i].cFirst() << " " << tabbnd[i].cSecond() << endl;
	//of << endl;

	//of << "taball.size = " << taball.size() << endl;
	//for ( MGSize i=0;i<taball.size();++i)
	//	of << taball[i].cFirst() << " " << taball[i].cSecond() << endl;
	//of << endl;

	//of << "tabinn.size = " << tabinn.size() << endl;
	//for ( MGSize i=0;i<tabinn.size();++i)
	//	of << tabinn[i].cFirst() << " " << tabinn[i].cSecond() << endl;
	//of << endl;

	//of << "tabdup.size = " << tabdup.size() << endl;
	//for ( MGSize i=0;i<tabdup.size();++i)
	//	of << tabdup[i].cFirst() << " " << tabdup[i].cSecond() << endl;


	//THROW_INTERNAL( "STOP");

	return false;
}


bool FaceCDT::FindFacePipeExtended()
{
	Kernel<DIM_3D> *pKernel = mpRec->mpKernel;

	vector<MGSize> tabpipe;
	vector<MGSize> tabcell;

	bool brec = false;
	const Key<3>& face = cFace();


	//{

	//	Key<3> kk = cFace();
	//	kk.Sort();
	//	if ( kk == Key<3>( 1811, 4380, 8908 ) )
	//	{
	//		mpRec->mpKernel->rGrid().UpdateNodeCellIds();

	//		for ( Grid<DIM_3D>::ColCell::iterator itr=mpRec->mpKernel->rGrid().rColCell().begin(); itr!=mpRec->mpKernel->rGrid().rColCell().end(); ++itr)
	//			itr->WashPaint();
	//	}
	//}


	// array with all boundary nodes
	vector<MGSize> tabebnode;
	tabebnode.reserve( mtabInNodes.size() + 3);

	tabebnode.resize( mtabInNodes.size() ); // ???
	copy( mtabInNodes.begin(), mtabInNodes.end(), tabebnode.begin() );
	tabebnode.push_back( face.cFirst() );
	tabebnode.push_back( face.cSecond() );
	tabebnode.push_back( face.cThird() );

	sort( tabebnode.begin(), tabebnode.end() );



	GVect vf1 = pKernel->cGrid().cNode( face.cFirst() ).cPos();
	GVect vf2 = pKernel->cGrid().cNode( face.cSecond() ).cPos();
	GVect vf3 = pKernel->cGrid().cNode( face.cThird() ).cPos();

	SimplexFace<DIM_3D> tri( vf1, vf2, vf3 );

	MGSize iicstart = pKernel->cGrid().cNode( face.cFirst() ).cCellId();

	tabcell.clear();


	//for ( vector<MGSize>::iterator itrp=tabcell.begin(); itrp!=tabcell.end(); ++itrp)
	//	pKernel->rGrid().rCell( *itrp).WashPaint();

	// finding a ball for the first node of the face
	vector<MGSize>	tabball;
	

	// don't use iicstart -- cell neighbour info somehow gets corrupted...
	if ( ! pKernel->FindBall( tabball, face.cFirst() ) )	
	//if ( ! pKernel->FindBall( tabball, face.cFirst(), iicstart ) )	
	{
		cout << "failed to find an edge pipe [" << face.cFirst() << ", " << face.cSecond() << ", " << face.cThird() << "]\n";
		TRACE( "failed to find an edge pipe [" << face.cFirst() << ", " << face.cSecond() << ", " << face.cThird() << "]\n"  );
		
		return false;
	}


	// check if the face is existing
	for ( vector<MGSize>::iterator itr = tabball.begin(); itr != tabball.end(); ++itr)
	{
		const GCell& cell = pKernel->cGrid().cCell( *itr);
		for ( MGSize i=0; i<GCell::SIZE; ++i)
		{
			Key<3> kface = cell.FaceKey(i);
			kface.Sort();
			if ( kface == face )
			{
				//cout << "the face is existing" << endl;
				mtabSubFaces.push_back( SubFaceCDT( face, *itr ) );
				brec = true;
				return false;
			}
		}
	}

	vector<GCell*>	tabstack;
	
	// searching for pipe starting cell/cells
	for ( vector<MGSize>::iterator itr = tabball.begin(); itr != tabball.end(); ++itr)
	{
		GCell& cell = pKernel->rGrid().rCell( *itr);
		Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( cell, pKernel->cGrid() );
		
		MGSize in;
		for ( in=0; in<=GCell::SIZE; ++in)
			if ( cell.cNodeId( in) == face.cFirst() )
				break;
				
		ASSERT( cell.cNodeId( in) == face.cFirst() );
		ASSERT( cell.cId() == *itr);
		

		for ( MGSize ie=0; ie<GCell::ESIZE; ++ie)
		{
			MGSize	idn1 = cell.cNodeId( Simplex<DIM_3D>::cEdgeConn(ie,0) );
			MGSize	idn2 = cell.cNodeId( Simplex<DIM_3D>::cEdgeConn(ie,1) );

			GVect	ve1 = pKernel->cGrid().cNode( idn1 ).cPos();
			GVect	ve2 = pKernel->cGrid().cNode( idn2 ).cPos();

			//if ( idn1 == face.cFirst() || idn2 == face.cFirst() )
			//	continue;

			if ( (binary_search( tabebnode.begin(), tabebnode.end(), idn1 ) && binary_search( tabebnode.begin(), tabebnode.end(), idn2 ) )
				 || (tri.CrossedVol( ve1, ve2) > 0.0 ) )
			{
				cell.Paint();
				tabstack.push_back( &cell);
				tabcell.push_back( *itr);
				break;
			}
		}
	}

	//{
	//	Key<3> kk = cFace();
	//	kk.Sort();
	//	if ( kk == Key<3>( 1811, 4380, 8908 ) )
	//	{
	//		cout << " face.cFirst() = " << face.cFirst() << endl;
	//		cout << " FindFacePipeExtended - Key<3>( 1811, 4380, 8908 ) :: ! itr->IsRecoverd() "  << endl;

	//		cout << "tabstack.size = " << tabstack.size() << endl;
	//		cout << "tabcell.size = " << tabcell.size() << endl;

 //			WriteGrdTEC<DIM_3D>	write( & mpRec->mpKernel->cGrid() );
	//		write.WriteCells( "facecdt_start_tabcell.dat", tabcell);
	//		write.WriteCells( "facecdt_start_tabball.dat", tabball);

	//		MGSize inod = face.cFirst();

	//		// brute force
	//		cout << "FindBall -- brute force is used !!!" << endl;
	//		vector<MGSize> tabc;
	//		for ( Grid<DIM_3D>::ColCell::const_iterator itr=mpRec->mpKernel->cGrid().cColCell().begin(); itr!=mpRec->mpKernel->cGrid().cColCell().end(); ++itr)
	//		{
	//			const GCell& cell = *itr;

	//			for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
	//				if ( cell.cNodeId( ifc ) == inod)
	//				{
	//					tabc.push_back( cell.cId() );
	//					break;
	//				}
	//		}

	//		write.WriteCells( "facecdt_start_tabc.dat", tabc);

	//	}
	//}



	// searching for remaining cells
	while ( tabstack.size() )
	{
		GCell &cell = *tabstack.back();
		Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( cell, pKernel->cGrid() );

		tabstack.pop_back();


		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		{
			MGSize ic = cell.cCellId( ifc).first;
			
			if ( ic )
			{
				GCell &cellnei = pKernel->rGrid().rCell( ic);
				
				if ( cellnei.IsPainted() )
					continue;

				for ( MGSize in=0; in<GCell::SIZE; ++in)
					if ( binary_search( tabebnode.begin(), tabebnode.end(), cellnei.cNodeId(in) ) )
					{
						cellnei.Paint();
						tabstack.push_back( &cellnei);
						tabcell.push_back( ic);
						break;
					}

				if ( cellnei.IsPainted() )
					continue;

				for ( MGSize ie=0; ie<GCell::ESIZE; ++ie)
				{
					GVect	ve1 = pKernel->cGrid().cNode( cellnei.cNodeId( Simplex<DIM_3D>::cEdgeConn(ie,0) ) ).cPos();
					GVect	ve2 = pKernel->cGrid().cNode( cellnei.cNodeId( Simplex<DIM_3D>::cEdgeConn(ie,1) ) ).cPos();

					if ( tri.CrossedVol( ve1, ve2) > 0.0 )
					{
						cellnei.Paint();
						tabstack.push_back( &cellnei);
						tabcell.push_back( ic);
						break;
					}
				}
					
			}
		}	
	}


	// cleaning cell flags
	for ( vector<MGSize>::iterator itrp=tabcell.begin(); itrp!=tabcell.end(); ++itrp)
		pKernel->rGrid().rCell( *itrp).WashPaint();



	mtabSubFaces.clear();
	mtabXEdge.clear();
	mtabXSlivers.clear();

	// filter cells
	for ( MGSize ic=0; ic<tabcell.size(); ++ic)
	{
		const GCell &cell = pKernel->cGrid().cCell( tabcell[ic] );

		if ( binary_search( tabebnode.begin(), tabebnode.end(), cell.cNodeId(0) ) &&
			 binary_search( tabebnode.begin(), tabebnode.end(), cell.cNodeId(1) ) &&
			 binary_search( tabebnode.begin(), tabebnode.end(), cell.cNodeId(2) ) &&
			 binary_search( tabebnode.begin(), tabebnode.end(), cell.cNodeId(3) ) )
		{
			mtabXSlivers.push_back( tabcell[ic] );
		}

		bool bcont = false;
		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		{
			Key<3> key = cell.FaceKey( ifc);
			key.Sort();

			if ( binary_search( tabebnode.begin(), tabebnode.end(), key.cElem(0) ) &&
				 binary_search( tabebnode.begin(), tabebnode.end(), key.cElem(1) ) &&
				 binary_search( tabebnode.begin(), tabebnode.end(), key.cElem(2) ) )
			{
				tabpipe.push_back( tabcell[ic] );
				mtabSubFaces.push_back( SubFaceCDT( key, tabcell[ic] ) );

				bcont = true;
				break;
			}
		}

		if ( bcont)
			continue;

		for ( MGSize ie=0; ie<GCell::ESIZE; ++ie)
		{
			MGSize in1 = cell.cNodeId( Simplex<DIM_3D>::cEdgeConn(ie,0) );
			MGSize in2 = cell.cNodeId( Simplex<DIM_3D>::cEdgeConn(ie,1) );

			if ( binary_search( tabebnode.begin(), tabebnode.end(), in1 ) ||
				 binary_search( tabebnode.begin(), tabebnode.end(), in2 ) )
				 continue;

			GVect	ve1 = pKernel->cGrid().cNode( in1 ).cPos();
			GVect	ve2 = pKernel->cGrid().cNode( in2 ).cPos();

			if ( tri.CrossedVol( ve1, ve2) > 0.0 )
			{
				tabpipe.push_back( tabcell[ic] );
				mtabXCells.push_back( tabcell[ic] );
				Key<2> ke( in1, in2);
				ke.Sort();
				mtabXEdge.push_back( ke);
				break;
			}
		}

	}

	sort( mtabSubFaces.begin(), mtabSubFaces.end() );
	mtabSubFaces.erase( unique( mtabSubFaces.begin(), mtabSubFaces.end() ), mtabSubFaces.end() );

	sort( mtabXEdge.begin(), mtabXEdge.end() );
	mtabXEdge.erase( unique( mtabXEdge.begin(), mtabXEdge.end() ), mtabXEdge.end() );

	if ( mtabXSlivers.size() > 0 )
	{
		cout << "mtabXSlivers.size() = " << mtabXSlivers.size() << endl;
		mbDebug = true;
		Dump();
	}

	//if ( ! IsRecoverd() )
	//{
	//	if ( mtabXEdge.size() == 0 )
	//	{
	//		cout << "mtabXSlivers.size() = " << mtabXSlivers.size() << endl;
	//		cout << "mtabXEdge.size = " << mtabXEdge.size() << endl;
	//	}
	////	//Dump();
	//}

	//if ( tabpipe.size() == 0)
	//	return false;

	//{
	//	Key<3> kk = cFace();
	//	kk.Sort();
	//	if ( kk == Key<3>( 1811, 4380, 8908 ) )
	//	{
	//		cout << " FindFacePipeExtended - Key<3>( 1811, 4380, 8908 ) :: ! itr->IsRecoverd() "  << endl;

	//		cout << "mtabSubFaces.size() = " << mtabSubFaces.size() << endl;
	//		cout << "mtabXSlivers.size() = " << mtabXSlivers.size() << endl;
	//		cout << "mtabXEdge.size = " << mtabXEdge.size() << endl;
	//		cout << "tabebnode.size = " << tabebnode.size() << endl;

 //			WriteGrdTEC<DIM_3D>	write( & mpRec->mpKernel->cGrid() );
	//		write.WriteCells( "facecdt_tabcell.dat", tabcell);
	//		write.WriteCells( "facecdt_tabpipe.dat", tabpipe);

	//	}
	//}


	return true;
}


void FaceCDT::Dump()
{
	ofstream	f( "facecdt_theface.dat" );

	Kernel<DIM_3D> *pKernel = mpRec->mpKernel;
	const Key<3>& face = cFace();

	GVect vf1 = pKernel->cGrid().cNode( face.cFirst() ).cPos();
	GVect vf2 = pKernel->cGrid().cNode( face.cSecond() ).cPos();
	GVect vf3 = pKernel->cGrid().cNode( face.cThird() ).cPos();

 	f << "VARIABLES = \"X\", \"Y\", \"Z\"\n";
	f << "ZONE T=\"" << "face" << "\", N=" << 3 << ", E=" << 1 << ", F=FEPOINT, ET=TRIANGLE\n";
 	f << vf1.cX() << " " << vf1.cY() << " " << vf1.cZ()  << endl;
 	f << vf2.cX() << " " << vf2.cY() << " " << vf2.cZ()  << endl;
 	f << vf3.cX() << " " << vf3.cY() << " " << vf3.cZ()  << endl;
	f << "1 2 3" << endl;
 
	vector< Key<3> > tabsubf;
	for ( MGSize i=0; i<mtabSubFaces.size(); ++i)
		tabsubf.push_back( mtabSubFaces[i].cFace() );

 	WriteGrdTEC<DIM_3D>	write( &pKernel->cGrid() );
 	write.WriteCells( "facecdt_cells.dat", mtabXCells);
	write.WriteFaces( "facecdt_subfaces.dat", tabsubf);
}


void FaceCDT::GenerateFaceGrid( Grid<DIM_3D>& fcGrid, Kernel<DIM_3D>& fcKernel )
{
	Kernel<DIM_3D> *pKernel = mpRec->mpKernel;

	const Key<3>& face = cFace();

	GVect vf1 = pKernel->cGrid().cNode( face.cFirst() ).cPos();
	GVect vf2 = pKernel->cGrid().cNode( face.cSecond() ).cPos();
	GVect vf3 = pKernel->cGrid().cNode( face.cThird() ).cPos();

		
	Box<DIM_3D> box = pKernel->cBox();
	GVect dv = box.cVMax() - box.cVMin();
	dv *= 0.2;
	box.rVMax() = box.cVMax() + dv;
	box.rVMin() = box.cVMin() - dv;

	fcKernel.rBox() = box;
	fcKernel.InitCube();

	for ( MGSize i=0; i<3; ++i)
	{
		GVect vct = pKernel->cGrid().cNode( face.cElem(i) ).cPos();
		MGSize inod = fcKernel.InsertPoint( vct );

		if ( ! inod)
			THROW_INTERNAL( "CRASH :: fcKernel failed to insert new point with coordinates [" << vct.cX() << ", " << vct.cY() << ", " << vct.cZ() << "]" );

		fcGrid.rNode(inod).rMasterId() = face.cElem(i);
	}

	for ( MGSize i=0; i<mtabInNodes.size(); ++i)
	{
		GVect vct = pKernel->cGrid().cNode( mtabInNodes[i] ).cPos();
		MGSize inod = fcKernel.InsertPoint( vct );

		if ( ! inod)
			THROW_INTERNAL( "CRASH :: fcKernel failed to insert new point with coordinates [" << vct.cX() << ", " << vct.cY() << ", " << vct.cZ() << "]" );

		fcGrid.rNode(inod).rMasterId() = mtabInNodes[i];
	}

	vector< Key<3> > tabbface;
	set< Key<3> > setface;

	for ( Grid<DIM_3D>::ColCell::const_iterator itc=fcGrid.cColCell().begin(); itc!= fcGrid.cColCell().end(); ++itc)
	{
		const GCell& cell = *itc;

		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		{
			Key<3> face = cell.FaceKey( ifc);
			face.Sort();

			bool bok = true;
			for ( MGSize in=0; in<3; ++in)
				if ( face.cElem(in) <= 8)
				{
					bok = false;
					break;
				}

			if ( bok)
			{
				tabbface.push_back( face);
				setface.insert( face);
			}

		}
	}

	sort( tabbface.begin(), tabbface.end() );
	tabbface.erase( unique( tabbface.begin(), tabbface.end() ), tabbface.end() );

	mtabSubFaces.clear();
	for ( MGSize i=0; i<tabbface.size(); ++i)
	{
		Key<3> face;
		for ( MGSize k=0; k<3; ++k)
			face.rElem(k) = fcGrid.cNode( tabbface[i].cElem(k) ).cMasterId();
		mtabSubFaces.push_back( SubFaceCDT( face, 0 ) );
	}

	////
	//WriteGrdTEC<DIM_3D> wtec( &fcGrid);
	//wtec.WriteFaces( "facecdt_grid_subfaces.dat", tabbface);
	
	/////////////////////////////////////////////////////////////////////////
	// 2nd phase

	// array with all boundary nodes
	vector<MGSize> tabebnode;
	tabebnode.reserve( mtabInNodes.size() + 3);

	tabebnode.resize( mtabInNodes.size() ); // ???
	copy( mtabInNodes.begin(), mtabInNodes.end(), tabebnode.begin() );
	tabebnode.push_back( face.cFirst() );
	tabebnode.push_back( face.cSecond() );
	tabebnode.push_back( face.cThird() );

	sort( tabebnode.begin(), tabebnode.end() );

	vector<MGSize> tabnode;
	for ( MGSize i=0; i<mtabXCells.size(); ++i)
	{
		const GCell& cell = pKernel->cGrid().cCell( mtabXCells[i] );

		for ( MGSize in=0; in<GCell::SIZE; ++in)
			if ( ! binary_search( tabebnode.begin(), tabebnode.end(), cell.cNodeId(in) ) )
				tabnode.push_back( cell.cNodeId(in) );
	}

	sort( tabnode.begin(), tabnode.end() );
	tabnode.erase( unique( tabnode.begin(), tabnode.end() ), tabnode.end() );


	set< Key<2> > setedge;

	for ( MGSize i=0; i<tabnode.size(); ++i)
	{
		GVect vct = pKernel->cGrid().cNode( tabnode[i] ).cPos();
		//MGSize inod = fcKernel.InsertPoint( vct );
		MGSize inod = fcKernel.InsertPointProtect( vct, setedge, setface);

		if ( ! inod)
			THROW_INTERNAL( "CRASH :: fcKernel failed to insert new point with coordinates [" << vct.cX() << ", " << vct.cY() << ", " << vct.cZ() << "]" );

		fcGrid.rNode(inod).rMasterId() = tabnode[i];
	}


	//for ( MGSize i=0; i<tabbface.size(); ++i)
	//{
		//Key<3> key( fcGrid.cNode( tabbface[i].cFirst() ).cMasterId(), fcGrid.cNode( tabbface[i].cSecond() ).cMasterId(), fcGrid.cNode( tabbface[i].cThird() ).cMasterId() );
		//key.Sort();

		//MGSize ic = mpRec->mtabNodeCellId[ key.cFirst() ];

		//bool brec = false;
		//vector<MGSize> tabpipe;

		//FindSubFacePipe( brec, tabpipe, key, ic);

		//if ( ! brec)
		//{
		//	if ( tabpipe.size() == 0 )
		//		cout << "tabpipe.size() == 0" << endl;

		//	mpRec->mtabSubFacesMissing.push_back( key);
		//}

	//}

	if ( mbDebug)
	{
		WriteGrdTEC<DIM_3D> wtec( &fcGrid);
		wtec.DoWrite( "facecdt_grid.dat");
	}

}



void FaceCDT::FindLocGridFaces( map< Key<3>, Key<3> >& mapface, Kernel<DIM_3D>& fcKernel)
{
	Grid<DIM_3D>& fcGrid = fcKernel.rGrid();

	mapface.clear();

	for ( Grid<DIM_3D>::ColCell::const_iterator itr = fcGrid.cColCell().begin(); itr != fcGrid.cColCell().end(); ++itr)
	{
		for ( MGSize k=0; k<GCell::SIZE; ++k)
		{
			Key<3> keyloc = (*itr).FaceKey( k);
			Key<3> keyglob;
			for ( MGSize in=0; in<3; ++in)
				keyglob.rElem(in) = fcGrid.cNode( keyloc.cElem(in) ).cMasterId();

			keyloc.Sort();
			keyglob.Sort();

			if ( mapface.find( keyglob ) == mapface.end() )
				mapface.insert( map< Key<3>, Key<3> >::value_type( keyglob, keyloc) );
		}
	}
}

void FaceCDT::SyncCavities( Grid<DIM_3D>& fcGrid, Kernel<DIM_3D>& fcKernel )
{
	Kernel<DIM_3D> *pKernel = mpRec->mpKernel;

	// paint x cells
	for ( MGSize i=0; i<mtabXCells.size(); ++i)
	{
		GCell& cell = pKernel->rGrid().rCell( mtabXCells[i] );
		cell.Paint();
	}

	// find cav boundary
	vector< Key<3> > tabcavfaces;

	//typedef KeyData< Key<3>, pair<GNeDef,GNeDef> > TFaceInfo;
	//vector<TFaceInfo> tabcavbnd;

	map<Key<3>,GNeDef> mapcellinfo;

	for ( MGSize i=0; i<mtabXCells.size(); ++i)
	{
		GCell& cell = pKernel->rGrid().rCell( mtabXCells[i] );

		if ( ! cell.IsPainted() )
			THROW_INTERNAL( "FaceCDT::SyncCavities :: the cell is not painted !!!");

		for ( MGSize k=0; k<GCell::SIZE; ++k)
		{
			MGSize icnext = cell.cCellId(k).first;
			if ( icnext == 0 )
				THROW_INTERNAL( "FaceCDT::SyncCavities :: ... !!!");

			GCell& cellnext = pKernel->rGrid().rCell( icnext );

			if ( ! cellnext.IsPainted() )
			{
				Key<3> key = cell.FaceKey(k);
				key.Sort();
				tabcavfaces.push_back( key );

				mapcellinfo.insert( map<Key<3>,GNeDef>::value_type( key, cell.cCellId(k) ) );
			}
		}
	}

	//WriteGrdTEC<DIM_3D> wtec( &pKernel->cGrid());
	//wtec.WriteFaces( "facecdt_grid_bndfaces.dat", tabcavfaces);


	map< Key<3>, Key<3> >	mapface;

	FindLocGridFaces( mapface, fcKernel);

	
	// which faces are missing
	MGSize count = 0;
	map< Key<3>, Key<3> >::iterator itrmp;
	vector< Key<3> > tablocface;

	vector< Key<3> > tabmiss;

	for ( MGSize i=0; i<tabcavfaces.size(); ++i)
		if ( (itrmp=mapface.find( tabcavfaces[i] )) != mapface.end() )
		{
			tablocface.push_back( itrmp->second);
		}
		else
		//if ( ! binary_search( tabtmp.begin(), tabtmp.end(), tabcavfaces[i] ) )
		{
			tabmiss.push_back( tabcavfaces[i] );
			++count;
		}


	if ( tabmiss.size() > 0 )
	{
		Dump();

		set< Key<3> > setedge;
		set< Key<3> > setface;

		for ( MGSize i=0; i<mtabSubFaces.size(); ++i)
		{
			Key<3> key = mtabSubFaces[i].cFace();
			key.Sort();

			if ( mapface.find(key) == mapface.end() )
				THROW_INTERNAL( "FaceCDT::SyncCavities :: CRASH !!!");

			Key<3> lockey = mapface[key];
			lockey.Sort();
			setface.insert( lockey);
		}

		for ( MGSize i=0; i<tablocface.size(); ++i)
			setface.insert( tablocface[i] );

		cout <<	endl;
		cout << "found missing bnd faces = " << count << endl;
		cout <<	endl;
		THROW_INTERNAL( "FaceCDT::SyncCavities :: CRASH !!!");
	}

	//WriteGrdTEC<DIM_3D> wtecGlob( &pKernel->cGrid());
	//if ( tabmiss.size() > 0 )
	//	wtec.WriteFaces( "facecdt_grid_bndfaces_missing.dat", tabmiss);

	//cout <<	endl;
	//cout << "found missing bnd faces = " << count << endl;
	//cout <<	endl;


	//WriteGrdTEC<DIM_3D> wtecloc( &fcGrid);
	//wtecloc.WriteFaces( "facecdt_grid_locbndfaces.dat", tablocface);
	//wtecloc.DoWrite( "facecdt_grid_locbefore.dat");


	// build local boundary
	fcKernel.FlagExternalCells( tablocface );

	////
	//vector< Key<2> > tabe;
	//Key<2> ke;
	//for ( MGSize i=0; i<tablocface.size(); ++i)
	//{
	//	ke = Key<2>( tablocface[i].cFirst(), tablocface[i].cSecond() ); ke.Sort();
	//	tabe.push_back( ke);

	//	ke = Key<2>( tablocface[i].cSecond(), tablocface[i].cThird() ); ke.Sort();
	//	tabe.push_back( ke);

	//	ke = Key<2>( tablocface[i].cThird(), tablocface[i].cFirst() ); ke.Sort();
	//	tabe.push_back( ke);
	//}

	//sort( tabe.begin(), tabe.end() );
	//ofstream of("edge.txt");
	//for ( MGSize i=0; i<tabe.size(); ++i)
	//	of << tabe[i].cFirst() << " " << tabe[i].cSecond() << endl;
	////

	////
	//vector<MGSize>	tabidc;
	//for ( Grid<DIM_3D>::ColCell::iterator itr = fcGrid.rColCell().begin(); itr != fcGrid.rColCell().end(); ++itr)
	//	if ( ! (*itr).IsExternal() )
	//		tabidc.push_back( itr->cId() );
	//wtecloc.WriteCells( "facecdt_incells.dat", tabidc);
	////

	for ( Grid<DIM_3D>::ColCell::iterator itr = fcGrid.rColCell().begin(); itr != fcGrid.rColCell().end(); ++itr)
	{
		if ( (*itr).IsExternal() )
			fcGrid.EraseCellSafe( itr->cId() );
			//fcGrid.rColCell().erase( itr);
	}

	//WriteGrdTEC<DIM_3D> wtecloc( &fcGrid);
	//wtecloc.DoWrite( "facecdt_grid_inside.dat");


	for ( MGSize i=0; i<mtabXCells.size(); ++i)
		pKernel->rGrid().EraseCell( mtabXCells[i] );
		//pKernel->rGrid().EraseCellSafe( mtabXCells[i] );

	mtabXCells.clear();

	vector<MGSize> tabcellid( fcGrid.cColCell().size(), 0 );
	for ( Grid<DIM_3D>::ColCell::iterator itr = fcGrid.rColCell().begin(); itr != fcGrid.rColCell().end(); ++itr)
	{
		GCell cell;
		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		{
			cell.rNodeId( ifc) = fcGrid.cNode( itr->cNodeId( ifc) ).cMasterId();
			cell.rCellId( ifc) = itr->cCellId( ifc);
		}
		MGSize id = pKernel->rGrid().InsertCell( cell);

		mtabXCells.push_back( id);
		tabcellid[ itr->cId() ] = id;
	}

	for ( MGSize i=0; i<mtabXCells.size(); ++i)
	{
		GCell& cell = pKernel->rGrid().rCell( mtabXCells[i] );

		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		{
			MGSize inext = cell.cCellId(ifc).first;

			if (inext)
			{
				cell.rCellId(ifc).first = tabcellid[ cell.cCellId(ifc).first ];
			}
			else
			{
				// sync with cavity boundary face
				Key<3> fkey = cell.FaceKey( ifc);
				fkey.Sort();

				map<Key<3>,GNeDef>::iterator itrinfo = mapcellinfo.find( fkey);
				if ( itrinfo == mapcellinfo.end() )
				{
					Dump();

					WriteGrdTEC<DIM_3D> wtec( &pKernel->cGrid());
					wtec.WriteFaces( "facecdt_grid_bndfaces.dat", tabcavfaces);

					WriteGrdTEC<DIM_3D> wtecloc( &fcGrid);
					wtecloc.DoWrite( "facecdt_grid_inside.dat");

					THROW_INTERNAL( "FaceCDT::SyncCavities :: face not found !!!");
				}

				cell.rCellId(ifc) = itrinfo->second;

				MGSize icold = cell.cCellId(ifc).first;
				if ( icold)
				{
					GCell& cellnext = pKernel->rGrid().rCell( icold );
					cellnext.rCellId( cell.cCellId(ifc).second).first = cell.cId();
					cellnext.rCellId( cell.cCellId(ifc).second).second = ifc;
				}
			}
		}
	}

	//pKernel->cGrid().CheckConnectivity();

	//wtec.DoWrite( "facecdt_grid_after.dat");

}



void FaceCDT::UpdateBndSubFaces()
{
	for ( MGSize i=0; i<mtabSubFaces.size(); ++i)
	{
		Key<3> key = mtabSubFaces[i].cFace();
		key.Sort();
		mpRec->mtabBndSubFaces.push_back( key );
	}
}


void FaceCDT::Reconstruct()
{
	Grid<DIM_3D>			fcGrid;
	GeneratorData<DIM_3D>	fcGData;
	Kernel<DIM_3D>			fcKernel(fcGrid,fcGData);

	//Dump();
	GenerateFaceGrid( fcGrid, fcKernel );
	SyncCavities( fcGrid, fcKernel );

	if ( mbDebug)
		THROW_INTERNAL("STOP");
}





//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

