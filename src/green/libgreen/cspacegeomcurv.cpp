#include "cspacegeomcurv.h"
#include "Eigen/Eigen"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 








//////////////////////////////////////////////////////////////////////
// 1D metric

// in 2D

template <>
inline TDefs<DIM_1D>::GMetric GeomCurvControlSpace<DIM_1D,DIM_2D>::GetSpacing( const GVect& vct) const
{
	MGFloat coeff = mCurvCoeff;

	UVect tab[DIM_1D];

	mpGEnt->GetFstDeriv( tab, vct);
	Vect3D	vfst( tab[0].cX(),tab[0].cY(), 0 );

	mpGEnt->GetSndDeriv( tab, vct);
	Vect3D	vsnd( tab[0].cX(),tab[0].cY(), 0 );

	Vect3D	v = vfst % vsnd;
	MGFloat d = vfst.module();
	MGFloat curv = v.module() / ( d*d );


	MGFloat ecurv = curv * curv  / (coeff * coeff);

	MGFloat espac = d * d;

	GMetric	met;

	//met[0] = max( espac, ecurv);
	met[0] = max( ecurv, ZERO);

	return met;
}


// in 3D

template <>
inline TDefs<DIM_1D>::GMetric GeomCurvControlSpace<DIM_1D,DIM_3D>::GetSpacing( const GVect& vct) const
{
	//MGFloat coeff = 10.2;
	MGFloat coeff = mCurvCoeff;

	UVect tab[DIM_1D];

	mpGEnt->GetFstDeriv( tab, vct);
	Vect3D	vfst(tab[0].cX(),tab[0].cY(),tab[0].cZ());

	mpGEnt->GetSndDeriv( tab, vct);
	Vect3D	vsnd(tab[0].cX(),tab[0].cY(),tab[0].cZ());

	Vect3D	v = vfst % vsnd;
	MGFloat d = vfst.module();
	MGFloat curv = v.module() / ( d*d );


	MGFloat ecurv = curv * curv  / (coeff * coeff);

	MGFloat espac = d * d;

	GMetric	met;

	//met[0] = max( espac, ecurv);
	met[0] = max( ecurv, ZERO);

	return met;
}




//////////////////////////////////////////////////////////////////////
// 2D metric

template <>
inline TDefs<DIM_2D>::GMetric GeomCurvControlSpace<DIM_2D,DIM_2D>::GetSpacing( const GVect& vct) const
{
	return GMetric();
}


#define __COMMENT__
#ifndef __COMMENT__

template <>
inline TDefs<DIM_2D>::GMetric GeomCurvControlSpace<DIM_2D,DIM_3D>::GetSpacing( const GVect& vct) const
{
	MGFloat coeff = 1.0/(mCurvCoeff);

	SMatrix<2>	mtxQ, mtxT, mtxD, mtxL, mtxLT, mtxLI, mtxN, mtxNT, mtxNI, mtxNIT;

	UVect tab[DIM_3D];

	/////////////////////////////////////////////////////////////
	// find first fundamental form
	mpGEnt->GetFstDeriv( tab, vct);

	if ( tab[0].module() < ZERO)
		tab[0].rX() = tab[0].rY() = tab[0].rZ() = ZERO;//1.0e-6;

	if ( tab[1].module() < ZERO)
		tab[1].rX() = tab[1].rY() = tab[1].rZ() = ZERO;//1.0e-6;


	GMetric	metspac;

	metspac[0] = tab[0]*tab[0];
	metspac[1] = 0.0;			// TODO :: check this !!!!!!
	metspac[1] = tab[0]*tab[1];
	metspac[2] = tab[1]*tab[1];

	metspac.EigenDecompose( mtxN);

	/////////////////////////////////////////////////////////////
	// find second fundamental form (on the tangent plane)

	UVect vn = tab[0] % tab[1];
	vn /= ::sqrt( vn * vn );

	mpGEnt->GetSndDeriv( tab, vct);

	mtxQ(0,0) = vn * tab[0];
	mtxQ(1,1) = vn * tab[1];
	mtxQ(1,0) = mtxQ(0,1) = vn * tab[2];

	mtxNI = mtxNT = mtxN;
	mtxNT.Transp();
	mtxNI.Invert();
	mtxNIT = mtxNI;
	mtxNIT.Transp();

	mtxT = mtxNIT * mtxQ * mtxNI;


	{
		SMatrix<2>	mxAI, mxA = metspac;

		if ( mxA.Determinant() < 1.0e-7 )
		{
			mtxT = SMatrix<2>( 2, 2, 0.0);
			mtxT(0,0) = mtxT(1,1) = 10.e-7;
			return GMetric(mtxT);
		}


		using namespace Eigen;
		typedef Matrix<MGFloat,2,2> EigenMatrix;

		SMatrix<2>	mtx1=metspac, mtx2=mtxQ;
		EigenMatrix emtxM1, emtxM2, emtxM1_inv;


		for ( MGSize i=0; i<2; ++i)
			for ( MGSize j=0; j<2; ++j)
			{
				emtxM1(i,j) = mtx1(i,j);
				emtxM2(i,j) = mtx2(i,j);
				//emtxM1_inv(i,j) = tmp(i,j);
			}

		emtxM1_inv = emtxM1.inverse();

		EigenMatrix emtxA = emtxM1_inv * emtxM2;
		//EigenMatrix emtxA = emtxM1.inverse() * emtxM2;

		EigenSolver<MatrixXd> es(emtxA);

		EigenMatrix P = es.eigenvectors().real();
		EigenMatrix D = es.eigenvalues().real().asDiagonal();

		EigenMatrix D1 = P.transpose() * emtxM1 * P;
		EigenMatrix D2 = P.transpose() * emtxM2 * P;

		for ( int i=0; i<D.rows(); ++i)
			D(i,i) = max( D1(i,i), D2(i,i) ); 

		EigenMatrix Pi = P.inverse();
		emtxA = Pi.transpose() * D * Pi;	

		for ( MGSize i=0; i<2; ++i)
			for ( MGSize j=0; j<2; ++j)
				mtxQ(i,j) = coeff * emtxA(i,j);

		SMatrix<2>	mtxQT = mtxQ;
		mtxQT.Transp();

		mtxT = mtxQT*mtxQ;
	}


	///////////////////////////////////////////////////////////////
	//// apply limiting to curvature metric
	//mtxT.Decompose( mtxD, mtxL, true);

	//MGFloat ar = 1.0 /  10.0;
	//mtxD *= coeff;

	//mtxD(0,0) = mtxD(0,0) * mtxD(0,0);
	//mtxD(1,1) = mtxD(1,1) * mtxD(1,1);

	////if ( mtxD(0,0) >  mtxD(1,1) )
	////	mtxD(1,1) = max( ar*ar*mtxD(0,0), mtxD(1,1) );
	////else
	////	mtxD(0,0) = max( ar*ar*mtxD(1,1), mtxD(0,0) );

	//mtxD(0,0) = fabs( mtxD(0,0) );
	//mtxD(1,1) = fabs( mtxD(1,1) );

	//MGFloat dd = max( fabs(mtxD(0,0)), fabs(mtxD(1,1)) );
	//mtxD(0,0) = mtxD(1,1) = dd;

	/////////////////////////////////////////////////////////////

	
	mtxLT = mtxL;
	mtxLT.Transp();

	//mtxT = mtxNT  * mtxT *   mtxN;

	//mtxT = mtxNT  * mtxL * mtxD * mtxLT *   mtxN;

	////mtxT = (1.0/ (coeff * coeff)) * mtxT;


	///////
	{
		SMatrix<2>	mtxM, mtxD, mtxLI, mtxL;

		mtxM = mtxT;
		mtxM.Decompose( mtxD, mtxL);

		for ( MGSize i=0; i<2; ++i)
		{
			if ( mtxD(i,i) > 1.0e4)
				mtxD(i,i) = 1.0e4;

			if ( mtxD(i,i) < ZERO)
				mtxD(i,i) = ZERO;
		}

		mtxLI = mtxL;
		mtxLI.Invert();

		mtxT = mtxL * mtxD * mtxLI;
	}
	///////

	GMetric metcurv(mtxT);
	return metcurv;


	//GMetric	met;
	//IntersectMetric( met, metspac, metcurv);

	//return met;


//????????????????

	///////////////////////////////////////////////////////////////
	//// find second fundamental form (on the tangent plane)

	//UVect vn = tab[0] % tab[1];
	//vn /= ::sqrt( vn * vn );

	//mpGEnt->GetSndDeriv( tab, vct);

	//mtxQ(0,0) = vn * tab[0];
	//mtxQ(1,1) = vn * tab[1];
	//mtxQ(1,0) = mtxQ(0,1) = vn * tab[2];

	//mtxNI = mtxNT = mtxN;
	//mtxNT.Transp();
	//mtxNI.Invert();
	//mtxNIT = mtxNI;
	//mtxNIT.Transp();

	//mtxT = mtxNIT * mtxQ * mtxNI * mtxNIT * mtxQ * mtxNI;
	//
	///////////////////////////////////////////////////////////////
	//// apply limiting to curvature metric
	//mtxT.Decompose( mtxD, mtxL, true);

	////MGFloat dd = max( fabs(mtxD(0,0)), fabs(mtxD(1,1)) );
	////mtxD(0,0) = mtxD(1,1) = dd;

	///////////////////////////////////////////////////////////////

	//
	//mtxLT = mtxL;
	//mtxLT.Transp();

	//mtxT = mtxNT  * mtxL * mtxD * mtxLT *   mtxN;

	//mtxT = (1.0/ (coeff * coeff)) * mtxT;

	//GMetric metcurv(mtxT);


	//GMetric	met;
	//IntersectMetric( met, metspac, metcurv);

	//return met;
}


#else //__COMMENT__

template <>
inline TDefs<DIM_2D>::GMetric GeomCurvControlSpace<DIM_2D,DIM_3D>::GetSpacing( const GVect& vct) const
{
	MGFloat coeff = 1.0/(mCurvCoeff);

	SMatrix<2>	mtxQ, mtxT, mtxD, mtxL, mtxLT, mtxLI, mtxN, mtxNT, mtxNI, mtxNIT;

	UVect tab[DIM_3D];

	/////////////////////////////////////////////////////////////
	// find first fundamental form
	mpGEnt->GetFstDeriv( tab, vct);

	if ( tab[0].module() < ZERO)
		tab[0].rX() = tab[0].rY() = tab[0].rZ() = ZERO;//1.0e-6;

	if ( tab[1].module() < ZERO)
		tab[1].rX() = tab[1].rY() = tab[1].rZ() = ZERO;//1.0e-6;


	GMetric	metspac;

	metspac[0] = tab[0]*tab[0];
	metspac[1] = 0.0;			// TODO :: check this !!!!!!
	metspac[1] = tab[0]*tab[1];
	metspac[2] = tab[1]*tab[1];

	metspac.EigenDecompose( mtxN);

	/////////////////////////////////////////////////////////////
	// find second fundamental form (on the tangent plane)

	UVect vn = tab[0] % tab[1];
	vn /= ::sqrt( vn * vn );

	mpGEnt->GetSndDeriv( tab, vct);

	mtxQ(0,0) = vn * tab[0];
	mtxQ(1,1) = vn * tab[1];
	mtxQ(1,0) = mtxQ(0,1) = vn * tab[2];

	mtxNI = mtxNT = mtxN;
	mtxNT.Transp();
	mtxNI.Invert();
	mtxNIT = mtxNI;
	mtxNIT.Transp();

	mtxT = mtxNIT * mtxQ * mtxNI;


	/////////////////////////////////////////////////////////////
	// apply limiting to curvature metric
	mtxT.Decompose( mtxD, mtxL, true);

	MGFloat ar = 1.0 /  10.0;
	mtxD *= coeff;

	mtxD(0,0) = mtxD(0,0) * mtxD(0,0);
	mtxD(1,1) = mtxD(1,1) * mtxD(1,1);

	//if ( mtxD(0,0) >  mtxD(1,1) )
	//	mtxD(1,1) = max( ar*ar*mtxD(0,0), mtxD(1,1) );
	//else
	//	mtxD(0,0) = max( ar*ar*mtxD(1,1), mtxD(0,0) );

	/////////////////////////////////////////////////////////////

	
	mtxLT = mtxL;
	mtxLT.Transp();


	mtxT = mtxNT  * mtxL * mtxD * mtxLT *   mtxN;

	{
		SMatrix<2>	mtxM, mtxD, mtxLI, mtxL;

		mtxM = mtxT;
		mtxM.Decompose( mtxD, mtxL);

		for ( MGSize i=0; i<2; ++i)
		{
			if ( mtxD(i,i) > 1.0e6)
				mtxD(i,i) = 1.0e6;

			if ( mtxD(i,i) < ZERO)
				mtxD(i,i) = ZERO;
		}

		mtxLI = mtxL;
		mtxLI.Invert();

		mtxT = mtxL * mtxD * mtxLI;
	}
	///////

	GMetric metcurv(mtxT);
	return metcurv;
}
#endif // __COMMENT__




template <Dimension DIM, Dimension UNIDIM>
typename TDefs<UNIDIM>::GMetric GeomCurvControlSpace<DIM,UNIDIM>::GetGlobSpacing( const GVect& vct) const
{
	UMetric umet;
	umet.InitIso( 10000000.0);

	return umet;
}


template <>
TDefs<DIM_2D>::GMetric GeomCurvControlSpace<DIM_1D,DIM_2D>::GetGlobSpacing( const GVect& vct) const
{
	UVect tab[DIM_1D];

	mpGEnt->GetFstDeriv( tab, vct);
	Vect2D	vfst( tab[0].cX(), tab[0].cY() );

	MGFloat d = max( vfst * vfst, 1.0e-6);;
	GMetric locmet = GetSpacing( vct);

	UMetric umet;
	umet.InitIso(  1.0 / sqrt(  locmet[0] / d ) );

	return umet;
}

template <>
TDefs<DIM_3D>::GMetric GeomCurvControlSpace<DIM_1D,DIM_3D>::GetGlobSpacing( const GVect& vct) const
{
	UVect tab[DIM_1D];

	mpGEnt->GetFstDeriv( tab, vct);
	Vect3D	vfst( tab[0].cX(), tab[0].cY(), tab[0].cZ() );

	mpGEnt->GetSndDeriv( tab, vct);
	Vect3D	vsnd(tab[0].cX(),tab[0].cY(),tab[0].cZ());

	if ( vsnd.module() < ZERO || vfst.module() < ZERO)
	{
		UMetric umet;

		umet.InitIso( 1.0 / 1.0e-6 );
		return umet;
	}

	Vect3D vt = vfst;
	Vect3D vn = vsnd - (vsnd * vt.versor()) * vt.versor();
	Vect3D vp = vt % vn;

	vt = vt.versor();
	vn = vn.versor();
	vp = vp.versor();


	if ( vp.module() < ZERO)
	{
		UMetric umet;

		umet.InitIso( 1.0 / 1.0e-6 );
		return umet;
	}

	SMatrix<3> N(3,3), NT;

	MGFloat d = max( vfst * vfst, 1.0e-6);
	GMetric locmet = GetSpacing( vct);
	MGFloat e =  sqrt( d / max( locmet[0], 1.0e-6) );

	for ( MGSize i=0; i<3; ++i)
	{
		//N(0,i) = vt.cX(i) * 1.0e-6;// / e;
		//N(1,i) = vn.cX(i) * 1.0e-6;// / e;
		//N(2,i) = vp.cX(i) * 1.0e-6;
		N(0,i) = vt.cX(i) / e;
		N(1,i) = vn.cX(i) / e;
		N(2,i) = vp.cX(i) / e / 100.0;
	}

	NT = N;
	NT.Transp();

	UMetric umet;

	umet.InitIso( e );
	umet = NT * N;

	return umet;
}



template class GeomCurvControlSpace<DIM_1D,DIM_2D>;
template class GeomCurvControlSpace<DIM_2D,DIM_2D>;

template class GeomCurvControlSpace<DIM_1D,DIM_3D>;
template class GeomCurvControlSpace<DIM_2D,DIM_3D>;


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

