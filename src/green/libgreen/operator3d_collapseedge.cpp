#include "operator3d_collapseedge.h"

#include "libgreen/grid.h"
#include "libgreen/kernel.h"
#include "libgreen/gridgeom.h"

#include "libgreen/writegrdtec.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 




void Operator3D_CollapseEdge::Reset()
{
	mErrorCode = 0;
	mQBefore = mQAfter = 0.0;

	mEdge = Key<2>( 0, 0);

	mtabBall.clear();
	mtabShell.clear();
	mtabCompl.clear();
}



void Operator3D_CollapseEdge::Init( const MGSize& idnod, const MGSize& idtarget, const vector<MGSize>& tabball)
{
	//cout << " Operator3D_CollapseEdge::Init " << endl;

	mEdge = Key<2>( idnod, idtarget);

	mtabBall = tabball;

	// just in case ...
	sort( mtabBall.begin(), mtabBall.end() );
	mtabBall.erase( unique( mtabBall.begin(), mtabBall.end() ), mtabBall.end() );


	// find shell and complement
	for ( MGSize ic=0; ic<mtabBall.size(); ++ic)
	{
		const GCell& cell = mKernel.cGrid().cCell( mtabBall[ic] );
		
		bool bis = true;
		for ( MGSize k=0; k<GCell::SIZE; ++k)
			if ( cell.cNodeId(k) == idtarget )
				bis = false;

		if ( bis )
			mtabCompl.push_back( mtabBall[ic] );
		else
			mtabShell.push_back( mtabBall[ic] );
	}

	//cout << idnod << " --> " << idtarget << endl;
	//cout << mtabBall.size() << " / " << mtabCompl.size() << " / " << mtabShell.size() << endl;

	// create proposition cells for left and right collapse
	vector<GCell> tabcell(mtabCompl.size());

	for ( MGSize i=0; i<mtabCompl.size(); ++i)
	{
		tabcell[i] = mKernel.cGrid().cCell( mtabCompl[i] );
		for ( MGSize k=0; k<GCell::SIZE; ++k)
			if ( tabcell[i].cNodeId(k) == idnod )
				tabcell[i].rNodeId(k) = idtarget;
	}

	// check quality
	MGFloat vola, qa;
	for ( MGSize i=0; i<tabcell.size(); ++i)
	{
		Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( tabcell[i], mKernel.cGrid());

		MGFloat vol = tet.Volume();
		if ( i==0 || vol < vola )
			vola = vol;

		MGFloat q = tet.QMeasureBETAmax();	// TODO :: check quality measure
		if ( i==0 || q < qa )
			qa = q;
	}

	mQAfter = qa;
	mVolMinAfter = vola;

	if ( mVolMinAfter <= 0.0 )
	//if ( mVolMinAfter <= 1.0e-8 )
		mErrorCode = 5;
}



void Operator3D_CollapseEdge::Execute()
{
	//if ( mErrorCode != 0)
	//	return;

	MGSize idnod = mEdge.cFirst();
	MGSize idtarget = mEdge.cSecond();

	// expand the complement cells
	for ( MGSize i=0; i<mtabCompl.size(); ++i)
	{
		mKernel.RemoveCellFromTree( mtabCompl[i] );

		GCell& cell = mKernel.rGrid().rCell( mtabCompl[i] );
		for ( MGSize k=0; k<GCell::SIZE; ++k)
			if ( cell.cNodeId(k) == idnod )
				cell.rNodeId(k) = idtarget;
	}


	// update connectivity
	for ( MGSize i=0; i<mtabShell.size(); ++i)
	{
		MGSize kold=GCell::SIZE, knew=GCell::SIZE;

		const GCell& cell = mKernel.cGrid().cCell( mtabShell[i] );
		for ( MGSize k=0; k<GCell::SIZE; ++k)
		{
			if ( cell.cNodeId(k) == idnod )
				kold = k;
			if ( cell.cNodeId(k) == idtarget )
				knew = k;
		}

		if ( knew == GCell::SIZE || kold == GCell::SIZE )
			THROW_INTERNAL( "EdgeDestr::EdgeCollapse :: problem");

		//if ( cell.cCellId(kold).first == 0 || cell.cCellId(knew).first == 0 )
		//	THROW_INTERNAL( "EdgeDestr::EdgeCollapse :: problem - NULL");

		if ( cell.cCellId(kold).first != 0 )
		{
			GCell& cellold = mKernel.rGrid().rCell( cell.cCellId(kold).first );
			cellold.rCellId( cell.cCellId(kold).second ).first  = cell.cCellId(knew).first;
			cellold.rCellId( cell.cCellId(kold).second ).second = cell.cCellId(knew).second;
		}

		if ( cell.cCellId(knew).first != 0 )
		{
			GCell& cellnew = mKernel.rGrid().rCell( cell.cCellId(knew).first );
			cellnew.rCellId( cell.cCellId(knew).second ).first  = cell.cCellId(kold).first;
			cellnew.rCellId( cell.cCellId(knew).second ).second = cell.cCellId(kold).second;
		}
	}

	// update aux data and check volumes
	for ( MGSize ic=0; ic<mtabCompl.size(); ++ic)
	{
		mKernel.InsertCellIntoTree( mtabCompl[ic]);

		const GCell& cell = mKernel.cGrid().cCell( mtabCompl[ic] );

		Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( cell, mKernel.cGrid());
		MGFloat vol = tet.Volume();

		if ( vol <= 0.0 )
			THROW_INTERNAL( "EdgeDestr::EdgeCollapse :: problem - volume = " << vol );

		for ( MGSize k=0; k<GCell::SIZE; ++k)
			mKernel.rGrid().rNode( cell.cNodeId(k) ).rCellId() = cell.cId();
	}


	for ( MGSize i=0; i<mtabShell.size(); ++i)
	{
		//Simplex<DIM_3D> tet = GridGeom<DIM_3D>::CreateSimplex( mtabShell[i], mKernel.cGrid());
		//GVect vpos = tet.Center();

		//if (	vpos.cX() > 9.0 && vpos.cX() < 9.7 &&
		//		vpos.cY() > 15.4 && vpos.cY() < 16 &&
		//		vpos.cZ() > 3.5 && vpos.cZ() < 4.5 )
		//{
		//	THROW_INTERNAL( "--- found ---");
		//}

		//mpKernel.rGrid().EraseCell( mtabShell[i] );
		mKernel.EraseCell( mtabShell[i] );
	}


	mKernel.EraseNode( idnod);
}

/*
void Operator3D_CollapseEdge::Init( const Key<2>& e, const MGSize& idn)
{
	mEdge = e;
	mIdNode = idn;

	Key<2> edge = e;

	MGSize idf = 0;

	if ( edge.cFirst() == idn )
		idf = edge.cSecond();
	else
	if ( edge.cSecond() == idn )
		idf = edge.cFirst();
	else
	{
		mErrorCode = 1;
		return;
	}

	GVect vctn = mKernel.rGrid().cNode( idn).cPos();
	GVect vctf = mKernel.rGrid().cNode( idf).cPos();

	vector<MGSize> tabball;

	mKernel.FindEdgeShell( mtabShell, edge);
	mKernel.FindBall( tabball, idn);

	sort( mtabShell.begin(), mtabShell.end() );
	sort( tabball.begin(), tabball.end() );

	mtabCell.resize( tabball.size() );

	vector<MGSize>::iterator iend = set_difference( tabball.begin(), tabball.end(), 
													  mtabShell.begin(), mtabShell.end(),
													  mtabCell.begin() );
	mtabCell.erase( iend, mtabCell.end() );



	//////
	//WriteGrdTEC<DIM_3D> writeTEC( &( mKernel.rGrid()) );

	//writeTEC.WriteCells( "__shell.dat", mtabShell);
	//writeTEC.WriteCells( "__ball.dat", mtabCell);


	//ofstream f( "__edge.dat");

	//GVect	v1 = mKernel.rGrid().cNode( edge.cFirst() ).cPos();
	//GVect	v2 = mKernel.rGrid().cNode( edge.cSecond() ).cPos();

	//f << "VARIABLES = \"X\", \"Y\", \"Z\", \"ID\"\n";
	//f << "ZONE T=\"edge\"\n";
	//f << v1.cX() << " " << v1.cY() << " " << v1.cZ() << " " << edge.cFirst() << endl;
	//f << v2.cX() << " " << v2.cY() << " " << v2.cZ() << " " << edge.cSecond() << endl;
	//////


	// quality of old cells
	MGFloat qbefore = 0.0;

	for ( MGSize i=0; i<mtabCell.size(); ++i)
	{
		const GCell& cell = mKernel.rGrid().cCell( mtabCell[i] );

		vector<GVect> tabv(GCell::SIZE);

		for ( MGSize d=0; d<GCell::SIZE; ++d)
			tabv[d] = mKernel.rGrid().cNode( cell.cNodeId(d) ).cPos();

		Simplex<DIM_3D> tet( tabv);

		MGFloat q = tet.QMeasureBETAmin();

		if ( q > qbefore )
			qbefore = q;
	}

	mQBefore = qbefore;


	// quality of new cells
	MGFloat qafter = 0.0;

	for ( MGSize i=0; i<mtabCell.size(); ++i)
	{
		const GCell& cell = mKernel.rGrid().cCell( mtabCell[i] );

		vector<GVect> tabv(GCell::SIZE);

		for ( MGSize d=0; d<GCell::SIZE; ++d)
			if ( cell.cNodeId(d) == idn )
				tabv[d] = vctf;
			else
				tabv[d] = mKernel.rGrid().cNode( cell.cNodeId(d) ).cPos();

		Simplex<DIM_3D> tet( tabv);

		MGFloat q = tet.QMeasureBETAmin();

		if ( q > qafter )
			qafter = q;

		//if ( tet.Volume() <= 0.0 )
		//if ( tet.Volume() <= 1.0e-8 )
		if ( tet.Volume() <= 0.0 )
		{
			mtabBadCell.push_back( mtabCell[i]);
			
			if ( cell.IsExternal() )
				mbExternal = true;
			else
				mbInternal = true;

			mErrorCode = 5;
		}
		else
		if ( (! cell.IsExternal()) && q > 10000.0 )
		{
			mtabBadCell.push_back( mtabCell[i]);

			if ( cell.IsExternal() )
				mbExternal = true;
			else
				mbInternal = true;

			mErrorCode = 10;
		}

		//	cout << "Operator3D_CollapseEdge -- negative volume cell found" << endl;
	}

	mQAfter = qafter;


}



void Operator3D_CollapseEdge::InitPoint()
{
	//vector<MGSize>	tabbad;

	MGSize idf = 0;

	if ( mEdge.cFirst() == mIdNode )
		idf = mEdge.cSecond();
	else
	if ( mEdge.cSecond() == mIdNode )
		idf = mEdge.cFirst();
	else
	{
		mErrorCode = 1;
		return;
	}

	GVect vctn = mKernel.rGrid().cNode( mIdNode).cPos();
	GVect vctf = mKernel.rGrid().cNode( idf).cPos();

	//for ( MGSize i=0; i<mtabCell.size(); ++i)
	//{
	//	const GCell& cell = mKernel.rGrid().cCell( mtabCell[i] );

	//	vector<GVect> tabv(GCell::SIZE);

	//	for ( MGSize d=0; d<GCell::SIZE; ++d)
	//		if ( cell.cNodeId(d) == mIdNode )
	//			tabv[d] = vctf;
	//		else
	//			tabv[d] = mKernel.rGrid().cNode( cell.cNodeId(d) ).cPos();

	//	Simplex<DIM_3D> tet( tabv);

	//	//MGFloat q = tet.QMeasureBETA();


	//	////if ( tet.Volume() <= 0.0 )
	//	////if ( tet.Volume() <= 1.0e-8 )
	//	if ( tet.Volume() <= 0.0 )
	//	{
	//		tabbad.push_back( mtabCell[i] );
	//	}
	//}

	// internal edges
	vector< Key<2> > tabEdge;
	vector< Key<2> > tabBndEdge;

	for ( MGSize i=0; i<mtabBadCell.size(); ++i)
	{
		const GCell& cell = mKernel.rGrid().cCell( mtabBadCell[i] );

		if ( cell.IsExternal() )
			continue;

		for ( MGSize d=0; d<GCell::SIZE; ++d)
		{
			MGSize icn = cell.cCellId( d).first;
			const GCell& celln = mKernel.rGrid().cCell( cell.cCellId( d).first );

			Key<3> fkey = cell.FaceKey( d);

			Key<2> e1( fkey.cFirst(),  fkey.cSecond() );	e1.Sort();
			Key<2> e2( fkey.cSecond(), fkey.cThird() );		e2.Sort();
			Key<2> e3( fkey.cThird(),  fkey.cFirst() );		e3.Sort();

			if ( e1.cFirst() == mIdNode || e1.cSecond() == mIdNode)
				tabEdge.push_back( e1);

			if ( e2.cFirst() == mIdNode || e2.cSecond() == mIdNode)
				tabEdge.push_back( e2);

			if ( e3.cFirst() == mIdNode || e3.cSecond() == mIdNode)
				tabEdge.push_back( e3);

			if ( celln.IsExternal() )
			{
				tabBndEdge.push_back( e1);
				tabBndEdge.push_back( e2);
				tabBndEdge.push_back( e3);
			}
		}
	}

	vector< Key<2> > tabInEdge;

	sort( tabEdge.begin(), tabEdge.end() );
	tabEdge.erase( unique( tabEdge.begin(), tabEdge.end() ), tabEdge.end() );

	sort( tabBndEdge.begin(), tabBndEdge.end() );
	tabBndEdge.erase( unique( tabBndEdge.begin(), tabBndEdge.end() ), tabBndEdge.end() );


	tabInEdge.resize( tabEdge.size() );
	tabInEdge.erase( set_difference( tabEdge.begin(), tabEdge.end(), tabBndEdge.begin(), tabBndEdge.end(), tabInEdge.begin() ), tabInEdge.end() );



	WriteGrdTEC<DIM_3D> writeTEC( &( mKernel.rGrid()) );

	writeTEC.WriteCells( "__shell.dat", mtabShell, "shell");
	writeTEC.WriteCells( "__ball.dat", mtabCell, "ball");
	writeTEC.WriteCells( "__badcell.dat", mtabBadCell, "bad cells");
	writeTEC.WriteEdges( "__badedges.dat", tabInEdge, "badEdges" );

	ofstream f( "__edge.dat");

	GVect	v1 = mKernel.rGrid().cNode( mEdge.cFirst() ).cPos();
	GVect	v2 = mKernel.rGrid().cNode( mEdge.cSecond() ).cPos();

	f << "VARIABLES = \"X\", \"Y\", \"Z\", \"ID\"\n";
	f << "ZONE T=\"edge\"\n";
	f << v1.cX() << " " << v1.cY() << " " << v1.cZ() << " " << mEdge.cFirst() << endl;
	f << v2.cX() << " " << v2.cY() << " " << v2.cZ() << " " << mEdge.cSecond() << endl;


	//for ( MGSize i=0; i<mtabCell.size(); ++i)
	//	cout << mtabCell[i] << endl;

	//cout << endl;
	//for ( MGSize i=0; i<tabbad.size(); ++i)
	//	cout << tabbad[i] << endl;

	cout << endl << mtabBadCell.size() << endl;

	//if ( mErrorCode == 5 )
	if ( mtabBadCell.size() > 1)
		THROW_INTERNAL( "STOP");
}


void Operator3D_CollapseEdge::Execute()
{
	if ( mErrorCode != 0)
		return;


	MGSize idf = 0;

	if ( mEdge.cFirst() == mIdNode )
		idf = mEdge.cSecond();
	else
	if ( mEdge.cSecond() == mIdNode )
		idf = mEdge.cFirst();
	else
	{
		THROW_INTERNAL( "Operator3D_CollapseEdge :: corrupted mEdge");
	}


	/////////////////////////////////////////////////////////////
	// remove cells from the tree
	for ( MGSize ic=0; ic<mtabShell.size(); ++ic)
		mKernel.RemoveCellFromTree( mtabShell[ic] );

	for ( MGSize ic=0; ic<mtabCell.size(); ++ic)
		mKernel.RemoveCellFromTree( mtabCell[ic] );

	/////////////////////////////////////////////////////////////
	// update cell connectivity
	for ( MGSize ic=0; ic<mtabShell.size(); ++ic)
	{
		GCell&	cell = mKernel.rGrid().rCell( mtabShell[ic] );

		MGSize icn, icf;

		bool bFa = false, bFr = false;
		for ( MGSize i=0; i<GCell::SIZE; ++i)
		{
			if ( cell.cNodeId(i) == mIdNode )
			{
				bFa = true;
				icn = i;
			}	

			if ( cell.cNodeId(i) == idf )
			{
				bFr = true;
				icf = i;
			}
		}
		
		ASSERT( bFa && bFr);

		GCell&	cellf = mKernel.rGrid().rCell( cell.cCellId( icf).first );
		GCell&	celln = mKernel.rGrid().rCell( cell.cCellId( icn).first );

		// update connectivity
		cellf.rCellId( cell.cCellId( icf).second ) = cell.cCellId( icn);
		celln.rCellId( cell.cCellId( icn).second ) = cell.cCellId( icf);

	}



	/////////////////////////////////////////////////////////////
	// update ball
	for ( MGSize i=0; i<mtabCell.size(); ++i)
	{
		GCell& cell = mKernel.rGrid().rCell( mtabCell[i] );

		for ( MGSize d=0; d<GCell::SIZE; ++d)
			if ( cell.cNodeId(d) == mIdNode )
				cell.rNodeId(d) = idf;
	}


	/////////////////////////////////////////////////////////////
	// insert ball cells into the tree with new center
	for ( MGSize ic=0; ic<mtabCell.size(); ++ic)
		mKernel.InsertCellIntoTree( mtabCell[ic] );


	/////////////////////////////////////////////////////////////
	// remove shell
	for ( MGSize ic=0; ic<mtabShell.size(); ++ic)
		mKernel.EraseCell( mtabShell[ic] );

	mKernel.EraseNode( mIdNode);

	//mKernel.rGrid().CheckConnectivity();

}

*/

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

