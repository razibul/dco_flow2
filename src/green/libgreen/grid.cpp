#include "grid.h"
#include "libcoreio/store.h"
#include "libcoregeom/geom.h"
#include "libgreen/gridgeom.h"
#include "libgreen/writegrdtec.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

template <Dimension DIM> 
void Grid<DIM>::RebuildConnectivity( vector< Key<DIM> >* ptabbndkey )
{
	cout << "Grid<DIM_" << DIM << "D>::RebuildConnectivity()" << endl;

	typedef pair< Key<DIM>, GNeDef > FaceInfo;
	vector< FaceInfo >	tabface;

	tabface.reserve( (DIM+1)*SizeCellTab() );

	for ( typename ColCell::const_iterator itr = mtabCell.begin(); itr != mtabCell.end(); ++itr)
	{
		for ( MGSize k=0; k<GCell::SIZE; ++k)
		{
			Key<DIM> key = (*itr).FaceKey( k);
			key.Sort();

			GNeDef	ndef( itr->cId(), k);
			tabface.push_back( FaceInfo( key, ndef) );
		}
	}

	sort( tabface.begin(), tabface.end() );


	vector< Key<DIM> >	tabbnd;

	for ( typename vector<FaceInfo>::iterator itr=tabface.begin(); itr!=tabface.end(); ++itr)
	{
		typename vector<FaceInfo>::iterator itrnext = itr+1;

		if ( itrnext != tabface.end() )
			if ( itr->first == itrnext->first )
			{
				// double case
				// update
				const GNeDef& neA = itr->second;
				const GNeDef& neB = itrnext->second;
				rCell( neA.first ).rCellId( neA.second ) = neB; 
				rCell( neB.first ).rCellId( neB.second ) = neA; 

				itr = itrnext;
				continue;
			}

		// single case - boundary
		tabbnd.push_back( itr->first );
	}

	if ( ptabbndkey )
		(*ptabbndkey) = tabbnd;

	CheckConnectivity();

	///////////////////////////////////////////////////////////
	ofstream f( "_bnd_after_reconstr_.dat" );

	cout << "\nWriting TEC from RebuildConnectivity" << std::flush;

	f << "TITLE = \"boundary grid\"\n";
	f << "VARIABLES = ";
	f << "\"X" << 0 << "\"";
	for ( MGSize i=1; i<DIM; ++i)
		f << ", \"X" << i << "\"";
	f << "\n";

	if ( DIM == DIM_2D )
		f << "ZONE T=\"bnd grid\", N=" << SizeNodeTab() << ", E=" << tabbnd.size() << ", F=FEPOINT, ET=LINESEG\n";
	else if ( DIM == DIM_3D )
		f << "ZONE T=\"bnd grid\", N=" << SizeNodeTab() << ", E=" << tabbnd.size() << ", F=FEPOINT, ET=TRIANGLE\n";

	for ( typename Grid<DIM>::ColNode::const_iterator itrn = cColNode().begin(); itrn != cColNode().end(); ++itrn)
	{
		for ( MGSize k=0; k<DIM; ++k)
			f << setprecision(8) << (*itrn).cPos().cX( k) << " ";
		f << endl;
	}

	for ( MGSize i=0; i<tabbnd.size(); ++i)
	{
		for ( MGSize k=0; k<DIM; ++k)
			f << tabbnd[i].cElem(k) << " ";
		f << endl;
	}

	cout << " - FINISHED\n";
	///////////////////////////////////////////////////////////

}




template <Dimension DIM> 
void Grid<DIM>::UpdateNodeCellIds()
{
	for ( typename Grid<DIM>::ColCell::const_iterator itrc = mtabCell.begin(); itrc != mtabCell.end(); ++itrc)
		for ( MGSize k=0; k<GCell::SIZE; ++k)
			rNode( itrc->cNodeId(k) ).rCellId() = itrc->cId();
}


template <Dimension DIM> 
void Grid<DIM>::UpdateNodeCellIds( const vector<MGSize>& tabcell)
{
	for ( MGSize ic=0; ic<tabcell.size(); ++ic)
	{
		const GCell& cell = cCell( tabcell[ic] );

		for ( MGSize k=0; k<GCell::SIZE; ++k)
			rNode( cell.cNodeId(k) ).rCellId() = cell.cId();
	}
}


template <Dimension DIM> 
void Grid<DIM>::Dump( const MGString& fname) const
{
	ofstream f( fname.c_str() );

	for ( typename ColCell::const_iterator itr = mtabCell.begin(); itr != mtabCell.end(); ++itr)
	{
		const GCell &c = *itr;

		c.Dump( *this, f);
	}
}


template <Dimension DIM> 
void Grid<DIM>::CheckConnectivity() const
{
	//cout << "CheckConnectivity\n";
	
	//FILE *f = gtrace.Open();
	ofstream f( "_trace.txt" );
	
	MGFloat	volume = 0.0;
	bool	bOk = true;

	//printf( "CellEnd() = %d\n", CellEnd() );
	for ( typename ColCell::const_iterator itr = mtabCell.begin(); itr != mtabCell.end(); ++itr)
	{
		const GCell &cell = *itr;
		MGFloat cvol = GridGeom<DIM>::CreateSimplex( cell, *this).Volume();

		volume += cvol;

		if ( cell.cId() != itr.index() )
			f << "bad cell id\n";

		for ( MGSize in=0; in<GCell::SIZE; ++in)
		{
			if ( ! mtabNode.is_valid( cell.cNodeId(in) ) )
			{
				bOk = false;
				f << "\nconnectivity error 0 - node ids are corrupted\n";
				f << "node_id = " << cell.cNodeId(in) << endl;
				cell.Dump( *this, f);
			}
		}


		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		{
			MGSize idfc = cell.cCellId( ifc).first;
			Key<DIM> key1 = cell.FaceKey( ifc);
			key1.Sort();

			if ( idfc)
			{
				const GCell &neicell = cCell(idfc);
				Key<DIM> key2 = neicell.FaceKey( cell.cCellId( ifc).second);
				key2.Sort();

				if ( ! mtabCell.is_valid( idfc ) )
				{
					bOk = false;
					f << "\nconnectivity error 0 - cell neighbour is invalid\n";
					f << "cell_id = " << idfc << endl;
					cell.Dump( *this, f);
				}

				if ( neicell.cCellId( cell.cCellId( ifc).second).first != cell.cId() )
				{
					bOk = false;
					f << "\nconnectivity error 1 - cells ids are corrupted\n";
					f << "cell_face_id = " << cell.cCellId( ifc).second << endl;
					cell.Dump( *this, f);
					neicell.Dump( *this, f);
					//Dump( "_grd_conn.dump" );
				}
				
				if ( ! (key1 == key2) )
				{
					bOk = false;
					f << "\nconnectivity error 2 - face nodes are different\n";
					f << "cell_face_id = " << cell.cCellId( ifc).second << endl;
					cell.Dump( *this, f);
					neicell.Dump( *this, f);
					//Dump( "_grd_conn.dump" );
				}
				

				if ( neicell.cCellId( cell.cCellId( ifc).second).second != ifc )
				{
					bOk = false;
					f << "connectivity error 3\n";
				}

			}
		}
	}

	MGFloat boxvolume = 0.0;
	
	//if ( DIM == DIM_2D)
	//{
	//	boxvolume = Geom::VolumeQUAD( cNode(1).cPos(), cNode(2).cPos(), cNode(3).cPos(), cNode(4).cPos() );
	//}
	//else 
	//if ( DIM == DIM_3D)
	//{
	//	boxvolume = Geom::VolumeHEX( cNode(1).cPos(), cNode(2).cPos(), cNode(3).cPos(), cNode(4).cPos(),
	//								 cNode(5).cPos(), cNode(6).cPos(), cNode(7).cPos(), cNode(8).cPos() );
	//}

	f << "sum of the cell volumes = " << setprecision(12) << volume << endl;
	f << "box volume = " << setprecision(12) << boxvolume << endl;
	
	
	if ( ! bOk)
		THROW_INTERNAL( "Grid<" << DIM << ">::CheckConnectivity() -- CRASH !!!");

	return;

	////////
	//vector<MGSize> tabb;
	//for ( ColCell::const_iterator itr = mtabCell.begin(); itr != mtabCell.end(); ++itr)
	//{
	//	const GCell &cell = *itr;

	//	for ( MGSize in=0; in<GCell::SIZE; ++in)
	//	{
	//		if ( cell.cNodeId(in) == 1405 )
	//			tabb.push_back( itr->cId() );
	//	}
	//}

	//cout << "tabb.size() = " << tabb.size() << endl; 

	//WriteGrdTEC<DIM_3D> writeTEC( this );
	//writeTEC.WriteCells( "__1405.dat", tabb );


	//tabb.clear();
	//tabb.push_back( 3215);
	//writeTEC.WriteCells( "__3215.dat", tabb );


}


template <Dimension DIM> 
void Grid<DIM>::CheckQuality() const
{
	const MGFloat treshBad = 5.0;

	MGFloat avAlpha = 0.0;
	MGFloat avBeta = 0.0;
	MGFloat avShape = 0.0;
	MGFloat avInterp = 0.0;
	
	pair<MGFloat,MGFloat> limAlpha;
	pair<MGFloat,MGFloat> limBeta;
	pair<MGFloat,MGFloat> limAngle;
	pair<MGFloat,MGFloat> limShape;
	pair<MGFloat,MGFloat> limInterp;


	vector<MGFloat> tabscale;
	const MGSize ns = 40;
	MGFloat xmax = ::log( 1.0e10);
	MGFloat dx = xmax / (MGFloat)(ns);

	for ( MGSize i=0; i<ns; ++i)
		tabscale.push_back( ::exp( (MGFloat)i * dx) );


	vector<MGSize> tabhist;
	tabhist.resize( tabscale.size(), 0);

	vector<MGSize> tabBad;


	bool bFirst = true;

	for ( typename ColCell::const_iterator itr = mtabCell.begin(); itr != mtabCell.end(); ++itr)
	{
		const GCell &cell = *itr;
		Simplex<DIM> simp = GridGeom<DIM>::CreateSimplex( cell, *this);

		MGFloat vol = simp.Volume();

		MGFloat a = simp.QMeasureALPHA();
		MGFloat b = simp.QMeasureBETAmin();

		MGFloat ashape = simp.QMeasureShape();
		MGFloat ainterp = simp.QMeasureInterp();

		MGFloat amin = simp.QMeasureMinAngle();
		MGFloat amax = simp.QMeasureMaxAngle();

		if ( vol <= 0.0)
		{
			cout << "volume problem -  id = " << itr->cId() << " vol = " << vol << " alpha = " << a << " beta = " << b << endl;
			cout << " angle " << amin << " " << amax << endl;
		}

		if ( b > treshBad)
			tabBad.push_back( itr->cId() );

		if ( bFirst)
		{
			limAlpha.first = limAlpha.second = a;
			limBeta.first = limBeta.second = b;
			limAngle.first = amin;
			limAngle.second = amax;
			limShape.first = ashape;
			limShape.second = ashape;
			limInterp.first = ainterp;
			limInterp.second = ainterp;
			bFirst = false;
		}

		if ( a > limAlpha.second)
			limAlpha.second = a;

		if ( a < limAlpha.first)
			limAlpha.first = a;

		if ( b > limBeta.second)
			limBeta.second = b;

		if ( b < limBeta.first)
			limBeta.first = b;

		if ( amin < limAngle.first)
			limAngle.first = amin;

		if ( amax > limAngle.second)
			limAngle.second = amax;

		if ( ashape > limShape.second)
			limShape.second = ashape;

		if ( ashape < limShape.first)
			limShape.first = ashape;

		if ( ainterp > limInterp.second)
			limInterp.second = ainterp;

		if ( ainterp < limInterp.first)
			limInterp.first = ainterp;

		avAlpha += a;
		avBeta += b;
		avShape += ashape;
		avInterp += ainterp;


		MGSize k;
		for ( k=0; k<tabscale.size(); ++k)
			if ( b < tabscale[k] )
				break;

		if ( k<tabscale.size() )
			++( tabhist[k] );
		else
			++( tabhist[tabscale.size()-1] );

	}

	cout << setprecision(16);
	cout << "ALPHA  = " << limAlpha.first << " - " << limAlpha.second << endl;
	cout << "BETA   = " << limBeta.first << " - " << limBeta.second << endl;
	cout << "SHAPE  = " << limShape.first << " - " << limShape.second << endl;
	cout << "INTERP = " << limInterp.first << " - " << limInterp.second << endl;

	cout << "Min Angle  = " << limAngle.first * 180/M_PI << endl;
	cout << "Max Angle  = " << limAngle.second * 180/M_PI << endl;

	cout << "average ALPHA  = " << avAlpha / mtabCell.size_valid() << endl;
	cout << "average BETA   = " << avBeta / mtabCell.size_valid() << endl;
	cout << "average SHAPE  = " << avShape / mtabCell.size_valid() << endl;
	cout << "average INTERP = " << avInterp / mtabCell.size_valid() << endl;


	ofstream of("_histogram.dat");
	for ( MGSize i=0; i<tabscale.size(); ++i)
		of  << tabscale[i] << " " << tabhist[i] << endl;

	of.close();

	cout << "no of bad cells (BETA > " << treshBad << ") = " << tabBad.size() << endl;
	if ( tabBad.size() > 0 )
	{
		WriteGrdTEC<DIM>	wtec( this);
		wtec.WriteCells( "_bad_cells.dat", tabBad );
	}

}


template <Dimension DIM> 
void Grid<DIM>::FindFaces( vector< Key<DIM> >& tabfac) const
{
	vector< Key<DIM> >	tabtmp;

	tabfac.clear();

	for ( typename ColCell::const_iterator itr = mtabCell.begin(); itr != mtabCell.end(); ++itr)
	{
		for ( MGSize k=0; k<GCell::SIZE; ++k)
		{
			Key<DIM> key = (*itr).FaceKey( k);
			key.Sort();
			tabtmp.push_back( key);
		}
	}

	sort( tabtmp.begin(), tabtmp.end() );
	tabtmp.erase( unique( tabtmp.begin(), tabtmp.end() ), tabtmp.end() );

	tabfac.resize( tabtmp.size() );
	copy( tabtmp.begin(), tabtmp.end(), tabfac.begin() );
}



template <Dimension DIM> 
void Grid<DIM>::FindInternalEdges( vector< Key<2> >& tabfac) const
{
	vector< Key<2> >	tabtmp;

	tabfac.clear();

	for ( typename ColCell::const_iterator itr = mtabCell.begin(); itr != mtabCell.end(); ++itr)
	{
		if ( (*itr).IsExternal() )
			continue;

		for ( MGSize k=0; k<GCell::SIZE; ++k)
		{
			if ( mtabCell[ (*itr).cCellId( k).first ].IsExternal() )
				continue;

			GFace face;
			itr->GetFaceVNOut( face, k);

			for ( MGSize ie=0; ie<GFace::ESIZE; ++ie)
			{
				Key<2> key = Key<2>( face.cNodeId( SimplexFace<DIM>::cEdgeConn(ie,0)), face.cNodeId( SimplexFace<DIM>::cEdgeConn(ie,1)) );
				key.Sort();
				tabtmp.push_back( key);
			}

		}
	}

	sort( tabtmp.begin(), tabtmp.end() );
	tabtmp.erase( unique( tabtmp.begin(), tabtmp.end() ), tabtmp.end() );

	tabfac.resize( tabtmp.size() );
	copy( tabtmp.begin(), tabtmp.end(), tabfac.begin() );
}


template <Dimension DIM> 
void Grid<DIM>::FindNullFaces( vector< Key<DIM> >& tabfac) const
{
	vector< Key<DIM> >	tabtmp;

	tabfac.clear();

	for ( typename ColCell::const_iterator itr = mtabCell.begin(); itr != mtabCell.end(); ++itr)
	{
		for ( MGSize k=0; k<GCell::SIZE; ++k)
		{
			if ( itr->cCellId(k).first == 0 )
			{
				Key<DIM> key = (*itr).FaceKey( k);
				key.Sort();
				tabtmp.push_back( key);
			}
		}
	}

	sort( tabtmp.begin(), tabtmp.end() );
	tabtmp.erase( unique( tabtmp.begin(), tabtmp.end() ), tabtmp.end() );

	tabfac.resize( tabtmp.size() );
	copy( tabtmp.begin(), tabtmp.end(), tabfac.begin() );
}



//////////////////////////////////////////////////////////////////////
// 2D specialization

template <>
void Grid<DIM_2D>::FindEdges( vector< Key<1> >& tabedg) const
{
	THROW_INTERNAL( "Grid<DIM_2D>::FindEdges : not valid for 2D !");
}


//////////////////////////////////////////////////////////////////////
// 3D specialization


template <>
void Grid<DIM_3D>::FindEdges( vector< Key<2> >& tabedg) const
{
	typedef vector< Key<2> >	TKeyTab;

	TKeyTab	tabtmp;

	tabedg.clear();

	for ( ColCell::const_iterator itr = mtabCell.begin(); itr != mtabCell.end(); ++itr)
	{
		for ( MGSize k=0; k<GCell::ESIZE; ++k)
		{
			Key<2> key = (*itr).EdgeKey( k);
			key.Sort();
			tabtmp.push_back( key);
		}
	}

	sort( tabtmp.begin(), tabtmp.end() );
	TKeyTab::iterator itrend = unique( tabtmp.begin(), tabtmp.end() );
	tabtmp.erase( itrend, tabtmp.end() );

	tabedg.resize( tabtmp.size() );
	copy( tabtmp.begin(), tabtmp.end(), tabedg.begin() );
}



template class Grid<DIM_2D>;
template class Grid<DIM_3D>;


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

