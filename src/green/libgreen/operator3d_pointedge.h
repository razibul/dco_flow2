#ifndef __OPERATOR3D_POINTEDGE_H__
#define __OPERATOR3D_POINTEDGE_H__

#include "libcoresystem/mgdecl.h"
#include "libgreen/tdefs.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



template <Dimension DIM> 
class Grid;

template <Dimension DIM> 
class BndGrid;

template <Dimension DIM> 
class Kernel;


//////////////////////////////////////////////////////////////////////
// class Operator3D_PointEdge
//////////////////////////////////////////////////////////////////////
class Operator3D_PointEdge
{
	typedef TDefs<DIM_3D>::GVect	GVect;
	typedef TDefs<DIM_3D>::GMetric	GMetric;

	typedef TDefs<DIM_3D>::GNode	GNode;
	typedef TDefs<DIM_3D>::GCell	GCell;
	typedef TDefs<DIM_3D>::GFace	GFace;

public:
	Operator3D_PointEdge( Kernel<DIM_3D>& kernel) : mKernel(kernel), mErrorCode(0), mIdNode(0)		{}

	const MGSize&	Error() const		{ return mErrorCode;}

	void	Init( const vector<MGSize>& tabcell, const GVect& pos);

	void	Execute( vector<MGSize>& tabcell);


	const MGSize&		cIdNode() const			{ return mIdNode;}

protected:
	void	BuildCells( vector<GCell>& tabcell);

private:
	Kernel<DIM_3D>		&mKernel;

	MGSize			mErrorCode;

	MGSize			mIdNode;
	GVect			mPointPos;

	Key<2>			mEdge;
	vector<MGSize>	mtabOCell;
	vector<MGSize>	mtabENode;

	Key<2>			mNewEdge;
	vector<GCell>	mtabNCell;
};


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __OPERATOR3D_POINTEDGE_H__
