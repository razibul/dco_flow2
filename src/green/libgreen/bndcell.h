#ifndef __BNDCELL_H__
#define __BNDCELL_H__

#include "libcoresystem/mgdecl.h"
#include "libcorecommon/entity.h"
#include "libgreen/basecell.h"
#include "libgreen/tdefs.h"
#include "libcorecommon/key.h"
#include "libcoregeom/triangle3d.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

//////////////////////////////////////////////////////////////////////
// class BndCell
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>class BndCell : public Entity, public BaseCell<Dimension(DIM-1)>
{
	typedef BaseCell<Dimension(DIM-1)>	TBase;
public:
	BndCell() : TBase()		{}

	const MGSize&	cTopoId() const					{ return mTopoId;}
	MGSize&			rTopoId()						{ return mTopoId;}


	// it has sens only for 3D bndcells
	Key<2>	EdgeKey( const MGSize& i) const;

protected:

private:
	MGSize	mTopoId;
};
//////////////////////////////////////////////////////////////////////



template <> 
inline Key<2> BndCell<DIM_3D>::EdgeKey( const MGSize& i) const
{	
	return Key<2>( cNodeId( Geom::Triangle3D::cFaceConn(i,0) ), cNodeId( Geom::Triangle3D::cFaceConn(i,1) ) );

	//MGSize k = 0;
	//Key<2>	key;

	//for ( MGSize inc=0; inc<DIM_2D; ++inc)
	//		key.rElem( k++) = cNodeId( Geom::Triangle3D::cFaceConn(i,inc) );

	//return key;
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __BNDCELL_H__

