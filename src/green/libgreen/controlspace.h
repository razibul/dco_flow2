#ifndef __CONTROLSPACE_H__
#define __CONTROLSPACE_H__

#include "libgreen/tdefs.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//template <Dimension DIM, Dimension UNIDIM>
//class GeomEntity;


//////////////////////////////////////////////////////////////////////
//	class ControlSpace
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class ControlSpace
{
	typedef typename TDefs<DIM>::GVect		GVect;
	typedef typename TDefs<DIM>::GMetric	GMetric;

public:
	ControlSpace()				{}
	virtual ~ControlSpace()		{}

	virtual GMetric	GetSpacing( const GVect& vct) const = 0;
};



//////////////////////////////////////////////////////////////////////
//	class DualControlSpace
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, Dimension UNIDIM>
class DualControlSpace : public ControlSpace<DIM>
{
	typedef typename TDefs<DIM>::GVect		GVect;
	typedef typename TDefs<DIM>::GMetric	GMetric;

	typedef typename TDefs<UNIDIM>::GVect	UVect;
	typedef typename TDefs<UNIDIM>::GMetric	UMetric;

public:
	DualControlSpace()				{}
	virtual ~DualControlSpace()		{}

	virtual GMetric	GetSpacing( const GVect& vct) const = 0;
	virtual UMetric	GetGlobSpacing( const GVect& vct) const = 0;
};






//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __CONTROLSPACE_H__
