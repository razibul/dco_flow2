#ifndef __CONFIGCONST_H__
#define __CONFIGCONST_H__

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


namespace ConfigStr
{

//////////////////////////////////////////////////////////////////////
// section MASTER
	namespace Master
	{
		const char NAME[]		= "MASTER";
		const char CASE[]		= "MST_NAME";
		const char DESRIPT[]	= "MST_DESCRIPT";

		const char WITH_CURVATURE[] = "MST_WITH_CURVATURE";
		const char CURVCOEFF[]      = "MST_CURVCOEFF";
		const char CURVEXCEPT[]      = "MST_CURVEXCEPT";

		namespace Dim
		{
			const char KEY[]		= "MST_DIM";
			namespace Value
			{
				const char	DIM_NONE[]	= "NONE";
				const char	DIM_0D[]	= "0D";
				const char	DIM_1D[]	= "1D";
				const char	DIM_2D[]	= "2D";
				const char	DIM_3D[]	= "3D";
				const char	DIM_4D[]	= "4D";
			}
		}



//////////////////////////////////////////////////////////////////////
// section MASTER :: GENERATOR
		namespace Generator
		{
			const char NAME[] = "GENERATOR";

			namespace TriangType
			{
				const char KEY[]		= "GEN_TRIANGTYPE";
				namespace Value
				{
					const char	GENTT_NONE[]	= "NONE";
					const char	GENTT_EDGES[]	= "EDGES";
					const char	GENTT_CELLC[]	= "CELL_CENTER";
					const char	GENTT_CIRCC[]	= "CIRCUM_CENTER";
				}
			}

			const char GEN_MAXITER[]		= "GEN_MAXITER";
			const char GEN_MAXPROPBEGIN[]	= "GEN_MAXPROPBEGIN";
			const char GEN_MAXPROPEND[]		= "GEN_MAXPROPEND";
			const char GEN_CONSTALPHA[]		= "GEN_CONSTALPHA";
			const char GEN_CONSTBETA[]		= "GEN_CONSTBETA";

			const char GEN_NITERDELAUNAY[]	= "GEN_NITERDELAUNEY";

			const char GEN_SCALE[]			= "GEN_SCALE";

			const char GEN_WITH_BDIST_CHECK[]	= "GEN_WITH_BDIST_CHECK";
			const char GEN_BND_CONSTALPHA[]		= "GEN_BND_CONSTALPHA";

			namespace Reconstructor
			{
				const char NAME[]		= "RECONSTRUCTOR";
			}

			namespace Destroyer
			{
				const char NAME[]			= "DESTROYER";

				const char DSTR_PP_NITER[]	= "DSTR_PP_NITER";
			}

			namespace Optimiser
			{
				const char NAME[]		= "OPTIMISER";
			}

//////////////////////////////////////////////////////////////////////
// section MASTER :: GENERATOR :: GENERATOR_1D
			namespace Generator1D
			{
				const char NAME[]		= "GENERATOR_1D";
			}

//////////////////////////////////////////////////////////////////////
// section MASTER :: GENERATOR :: GENERATOR_2D
			namespace Generator2D
			{
				const char NAME[]		= "GENERATOR_2D";

				const char GEN2D_TRIANG_TYPE[]	= "";

			}

//////////////////////////////////////////////////////////////////////
// section MASTER :: GENERATOR :: GENERATOR_3D
			namespace Generator3D
			{
				const char NAME[]		= "GENERATOR_3D";
			}
		}
	}


}
 


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __CONFIGCONST_H__
