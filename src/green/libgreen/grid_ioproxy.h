#ifndef __GRIDIOPROXY_H__
#define __GRIDIOPROXY_H__


#include "libcoreio/gridfacade.h"
#include "libgreen/grid.h"
#include "libgreen/bndgrid.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
//	class IOReadGreenProxy
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class IOReadGreenProxy : public IO::GridReadFacade
{
public:
	IOReadGreenProxy( Grid<DIM>* pgrid, BndGrid<DIM>* pbndgrid) : mpGrid(pgrid), mpBndGrid(pbndgrid)	{}

//protected:

	virtual Dimension	Dim() const		{ return DIM;}

	virtual void	IOTabNodeResize( const MGSize& n)							
		{ 
			mpGrid->rColNode().resize( n);
		}

	virtual void	IOTabCellResize( const MGSize& type, const MGSize& n)		
		{
			if ( mpGrid)
			{
				if ( type == DIM+1 ) 
					mpGrid->rColCell().resize( n); 
				else if ( n > 0 )
					THROW_INTERNAL( "cell type '"<<type<<"' can not be handled");
			}
		}

	virtual void	IOTabBFaceResize( const MGSize& type, const MGSize& n)		
		{ 
			if ( mpBndGrid )
			{
				if ( type == DIM ) 
					mpBndGrid->rColCell().resize( n);
				else if ( n > 0 )
					THROW_INTERNAL( "bface type '"<<type<<"' can not be handled");
			}
		}

	virtual void	IOSetNodePos( const vector<MGFloat>& tabx, const MGSize& id)
		{
			typename Grid<DIM>::GVect vpos;
			for ( MGSize i=0; i<DIM; ++i)
				vpos.rX(i) = tabx[i];

			typename Grid<DIM>::GNode &node = mpGrid->rNode(id+1);
			node.rId() = id+1;
			node.rMasterId() = 0;
			node.rPos() = vpos;
		}

	virtual void	IOSetCellIds( const vector<MGSize>& tabid, const MGSize& type, const MGSize& id )
		{
			if ( !mpGrid)
				return;
				//THROW_INTERNAL( "mpGrid is NULL");

			typename Grid<DIM>::GCell &cell = mpGrid->rCell(id+1);
			cell.rId() = id+1;
			for ( MGSize i=0; i<=DIM; ++i)
				cell.rNodeId(i) = tabid[i]+1;

		}

	virtual void	IOSetBFaceIds( const vector<MGSize>& tabid, const MGSize& type, const MGSize& id )
		{
			if ( !mpBndGrid)
				return;
				//THROW_INTERNAL( "mpBndGrid is NULL");

			typename BndGrid<DIM>::GBndCell &bcell = mpBndGrid->rCell(id+1);
			bcell.rId() = id+1;
			for ( MGSize i=0; i<DIM; ++i)
				bcell.rNodeId(i) = tabid[i]+1;
		}

	virtual void	IOSetBFaceSurf( const MGSize& idsurf, const MGSize& type, const MGSize& id )
		{
			if ( !mpBndGrid)
				return;
				//THROW_INTERNAL( "mpBndGrid is NULL");

			mpBndGrid->rCell(id+1).rTopoId() = idsurf;
		}

	virtual void	IOGetName( MGString& name) const							{ name = mGrdName;}
	virtual void	IOSetName( const MGString& name)							{ mGrdName = name;}


	virtual void	IOPrepareReading()	{}
	virtual void	IOFinalizeReading()	
		{
			// init bndnodes and setup GrdIds
		}

private:
	MGString		mGrdName;
	Grid<DIM>*		mpGrid;
	BndGrid<DIM>*	mpBndGrid;
};




//////////////////////////////////////////////////////////////////////
//	class IOWriteGreenProxy
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class IOWriteGreenProxy : public IO::GridWriteFacade
{
public:
	IOWriteGreenProxy( Grid<DIM>* pgrid, BndGrid<DIM>* pbndgrid) : mpGrid(pgrid), mpBndGrid(pbndgrid)	
		{ THROW_INTERNAL( "not implemented"); }


	virtual Dimension	Dim() const		{ return DIM;}

	virtual void	IOGetName( MGString& name) const	{ name = mGrdName;}

	virtual MGSize	IOSizeNodeTab() const										
		{ 
			return mpGrid->SizeNodeTab(); 
		}

	virtual MGSize	IOSizeCellTab( const MGSize& type) const					
		{ 
			if ( type == DIM+1 && mpGrid )
				return mpGrid->SizeCellTab();
			else
				return 0;
		}

	virtual MGSize	IOSizeBFaceTab( const MGSize& type) const					
		{ 
			if ( type == DIM && mpBndGrid )
				return mpBndGrid->SizeCellTab();
			else
				return 0;
		}

	virtual MGSize	IONumCellTypes() const										{ return 1;}
	virtual MGSize	IONumBFaceTypes() const										{ return 1;}
	virtual MGSize	IOCellType( const MGSize& i) const							{ ASSERT(i==0); return DIM+1; }
	virtual MGSize	IOBFaceType( const MGSize& i) const							{ ASSERT(i==0); return DIM;}

	virtual void	IOGetNodePos( vector<MGFloat>& tabx, const MGSize& id) const	{}
	virtual void	IOGetCellIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const	{}
	virtual void	IOGetBFaceIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const	{}
	virtual void	IOGetBFaceSurf( MGSize& idsurf, const MGSize& type, const MGSize& id) const		{}

	virtual void	IOPrepareWriting()	{}
	virtual void	IOFinalizeWriting()	{}


private:
	MGString		mGrdName;
	Grid<DIM>*		mpGrid;
	BndGrid<DIM>*	mpBndGrid;
};


/*
template <Dimension DIM>
inline void IOReadGreenProxy<DIM>::IOGetNodePos( vector<MGFloat>& tabx, const MGSize& id) const
{
	ASSERT( tabx.size() == DIM);
	for ( MGSize i=0; i<DIM; ++i)
		tabx[i] = mtabNode[id].cPos().cX(i) * mRefLength;
}


template <Dimension DIM>
inline void IOReadGreenProxy<DIM>::IOGetCellIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const
{
	ASSERT( type == DIM+1);
	ASSERT( tabid.size() == type);

	for ( MGSize k=0; k<=DIM; ++k)
		tabid[k] = mtabCell[id].cId(k);
}


template <Dimension DIM>
inline void IOReadGreenProxy<DIM>::IOGetBFaceIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const
{
	ASSERT( type == DIM);
	ASSERT( tabid.size() == type);

	for ( MGSize k=0; k<DIM; ++k)
		tabid[k] = mtabBFace[id].cId(k);
}

template <Dimension DIM>
inline void IOReadGreenProxy<DIM>::IOGetBFaceSurf(MGSize& idsurf, const MGSize& type, const MGSize& id) const
{
	ASSERT( type == DIM);
	idsurf = mtabBFace[id].cSurfId();
}

template <Dimension DIM>
inline void IOReadGreenProxy<DIM>::IOSetNodePos( const vector<MGFloat>& tabx, const MGSize& id )
{
	ASSERT( tabx.size() == DIM);
	for ( MGSize i=0; i<DIM; ++i)
		mtabNode[id].rPos().rX(i) = tabx[i] / mRefLength;
}


template <Dimension DIM>
inline void IOReadGreenProxy<DIM>::IOSetCellIds( const vector<MGSize>& tabid, const MGSize& type, const MGSize& id )
{
	ASSERT( type == DIM+1);
	ASSERT( tabid.size() == type);

	for ( MGSize k=0; k<=DIM; ++k)
		mtabCell[id].rId(k) = tabid[k];
}


template <Dimension DIM>
inline void IOReadGreenProxy<DIM>::IOSetBFaceIds( const vector<MGSize>& tabid, const MGSize& type, const MGSize& id )
{
	ASSERT( type == DIM);
	ASSERT( tabid.size() == type);

	for ( MGSize k=0; k<DIM; ++k)
		mtabBFace[id].rId(k) = tabid[k];
}

template <Dimension DIM>
inline void IOReadGreenProxy<DIM>::IOSetBFaceSurf( const MGSize& idsurf, const MGSize& type, const MGSize& id )
{
	ASSERT( type == DIM);
	mtabBFace[id].rSurfId() = idsurf;
}


//////////////////////

template <Dimension DIM>
inline void IOReadGreenProxy<DIM>::IOGetBNodePos( vector<MGFloat>& tabx, const MGSize& id) const
{
	ASSERT( tabx.size() == DIM);
	for ( MGSize i=0; i<DIM; ++i)
		tabx[i] = mtabNode[ mtabBNode[id].cPId() ].cPos().cX(i) * mRefLength;
}

template <Dimension DIM>
inline void IOReadGreenProxy<DIM>::IOGetBNodeVn( vector<MGFloat>& tabx, const MGSize& id) const
{
	ASSERT( tabx.size() == DIM);
	for ( MGSize i=0; i<DIM; ++i)
		tabx[i] = mtabBNode[id].cVn().cX( (MGInt)i);
}

template <Dimension DIM>
inline void IOReadGreenProxy<DIM>::IOGetBFaceBIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const
{
	ASSERT( type == DIM);
	ASSERT( tabid.size() == type);

	for ( MGSize k=0; k<DIM; ++k)
		tabid[k] = mtabBFace[id].cBId(k);
}

*/


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __GRIDIOPROXY_H__
