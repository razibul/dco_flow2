#include "spointdestructor.h"

#include "libgreen/destroyer_v2.h"
#include "libgreen/grid.h"
#include "libgreen/kernel.h"
#include "libgreen/bndgrid.h"
#include "libgreen/gridgeom.h"
#include "libgreen/controlspace.h"

#include "libgreen/operator3d_collapseedge.h"

#include "libgreen/writegrdtec.h"
#include "libcorecommon/progressbar.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



void SPointDestructor::FindComplement( vector<MGSize>& tabcomp, vector<MGSize>& tabshell, const MGSize& idtarget)
{
	// find shell and complement
	for ( MGSize ic=0; ic<mtabBall.size(); ++ic)
	{
		const GCell& cell = mpParent->mpKernel->cGrid().cCell( mtabBall[ic] );
		
		bool bis = true;
		for ( MGSize k=0; k<GCell::SIZE; ++k)
			if ( cell.cNodeId(k) == idtarget )
				bis = false;

		if ( bis )
			tabcomp.push_back( mtabBall[ic] );
		else
			tabshell.push_back( mtabBall[ic] );
	}
}


void SPointDestructor::Analyze( MGSize& flag, const MGSize& idtarget)
{
	//bool bdbg = false;
	//if ( mINode == 24828)
	//	bdbg = true;


	//if ( idtarget != mINodeL && idtarget != mINodeR )
	//{
	//	flag = 0;
	//	return;
	//}


	for ( MGSize ic=0; ic<mtabBFace.size(); ++ic)
	{
		const Face<DIM_3D>& face = mtabBFace[ic];
		
		bool bis = true;

		for ( MGSize k=0; k<Face<DIM_3D>::SIZE; ++k)
		{
			if ( face.cNodeId(k) == idtarget )
				bis = false;
		}


		if ( bis )
		{

			vector<MGSize> tabfin;
			tabfin.reserve(2);

			for ( MGSize k=0; k<Face<DIM_3D>::SIZE; ++k)
				if ( face.cNodeId(k) != mINode )
					tabfin.push_back( face.cNodeId(k) );

			//cout << tabfin.size() << endl;
		
			//tabcomp.push_back( mtabBall[ic] );
			vector< KeyData< MGSize, Key<2> > >::iterator itr;

			if ( tabfin.size() != 2 )
				THROW_INTERNAL( "SPointDestructor::Analyze - crash");

			if ( tabfin[0] == mINodeL || tabfin[1] == mINodeL || tabfin[0] == mINodeR || tabfin[1] == mINodeR )
				continue;

			MGSize idmiss = 0;
			MGSize nmiss = 0;

			itr = lower_bound( mpParent->mtabINParent.begin(), mpParent->mtabINParent.end(), KeyData< MGSize, Key<2> >( tabfin[0]) );
			if ( itr->first != tabfin[0] )
			{
				idmiss = tabfin[0];
				nmiss = 1;
			}

			Key<2> edge1 = itr->second;
			edge1.Sort();

			itr = lower_bound( mpParent->mtabINParent.begin(), mpParent->mtabINParent.end(), KeyData< MGSize, Key<2> >( tabfin[1]) );
			if ( itr->first != tabfin[1] )
			{
				if ( idmiss)
					THROW_INTERNAL( "SPointDestructor::Analyze - crash");

				idmiss = tabfin[1];
				nmiss = 2;
			}

			Key<2> edge2 = itr->second;
			edge2.Sort();

			if ( idmiss )
			{
				if ( nmiss == 1)
					if ( idmiss == edge2.cFirst() || idmiss == edge2.cSecond() )
						edge1 = edge2;

				if ( nmiss == 2)
					if ( idmiss == edge1.cFirst() || idmiss == edge1.cSecond() )
						edge2 = edge1;
			}

			if ( edge1 == edge2)
			{
				if ( edge1.cFirst() == idtarget || edge1.cSecond() == idtarget )
					++flag;
			}
		}
	}

}



void SPointDestructor::Init( const vector< Key<2> >& tabsedge)
{
	GVect vpos = mpParent->mpKernel->cGrid().cNode(mINode).cPos();

	//if (	vpos.cX() > 15.5 && vpos.cX() < 16 &&
	//		vpos.cY() > 15.4 && vpos.cY() < 16 &&
	//		vpos.cZ() > 3.5 && vpos.cZ() < 4.5 )
	//{
	//	THROW_INTERNAL( "--- found ---");
	//}

	// find 2 edges
	vector<MGSize> tabedge;
	tabedge.reserve(2);

	for ( MGSize ie=0; ie<tabsedge.size(); ++ie)
	{
		if ( tabsedge[ie].cFirst() == mINode )
			tabedge.push_back( ie);

		if ( tabsedge[ie].cSecond() == mINode )
			tabedge.push_back( ie);
	}

	if ( tabedge.size() != 2)
		THROW_INTERNAL( "EdgeDestr::RemoveNode() :: tabedge.size() != 2" );

	mEdgeL = tabsedge[ tabedge[0] ];
	mEdgeR = tabsedge[ tabedge[1] ];

	if ( mEdgeL.cFirst() == mINode )
		mINodeL = mEdgeL.cSecond();
	else
		mINodeL = mEdgeL.cFirst();

	if ( mEdgeR.cFirst() == mINode )
		mINodeR = mEdgeR.cSecond();
	else
		mINodeR = mEdgeR.cFirst();


	// find ball for inod
	MGSize icell = mpParent->mpKernel->cGrid().cNode(mINode).cCellId();

	if ( icell == 0)
		THROW_INTERNAL( "SPointDestructor::Init :: icell==0 for inod = " << mINode );

	mpParent->mpKernel->FindBall( mtabBall, mINode, icell );

	// find boundary faces of the ball
	vector< Key<3> > tabface;
	for ( MGSize i=0; i<mtabBall.size(); ++i)
	{
		const GCell& cell = mpParent->mpKernel->cGrid().cCell( mtabBall[i] );

		for ( MGSize k=0; k<GCell::SIZE; ++k)
		{
			if ( cell.cCellId(k).first == 0 )
			{
				Face<DIM_3D> face;
				cell.GetFaceVNIn( face, k);

				for ( MGSize j=0; j<Face<DIM_3D>::SIZE; ++j)
					if ( face.cNodeId(j) == mINode )
					{
						mtabBFace.push_back( face );
						tabface.push_back( face.CellKey() );
						//cout << face.cNodeId(0) << " " << face.cNodeId(1) << " " << face.cNodeId(2) << endl;
						//tabface.push_back( Key<3>( face.cNodeId(0), face.cNodeId(1), face.cNodeId(2) ) );
						break;
					}
			}
		}
	}

	// find complements and shells
	//FindComplement( mtabCompl_L, mtabShell_L, mINodeL );
	//FindComplement( mtabCompl_R, mtabShell_R, mINodeR );

	Analyze( mflagL, mINodeL);
	Analyze( mflagR, mINodeR);


	//static MGSize ndbg = 0;
	// dump info
	//if ( mINode == 24107)
	//{
	//	++ndbg;
	//	cout << endl << "!!!!!!!!!! mINode = " << mINode << endl;
	//	cout << "mINodeR = " << mINodeR << ", mflagR = " << mflagR << endl;
	//	cout << "mINodeL = " << mINodeL << ", mflagL = " << mflagL << endl;
	//	WriteGrdTEC<DIM_3D> writer( & mpParent->mpKernel->cGrid() );

	//	writer.WriteCells( "spdestr_ball.dat", mtabBall, "BALL");
	//	writer.WriteFaces( "spdestr_bfaces.dat", tabface, "BFACES");

	//	//if ( ndbg == 2)
	//	//	THROW_INTERNAL( "STOP");
	//}

	//if ( mflagR !=0 || mflagL != 0)
	//	THROW_INTERNAL( "STOP");

}


bool SPointDestructor::Delete()
{
	Operator3D_CollapseEdge opedgeL( *mpParent->mpKernel);
	Operator3D_CollapseEdge opedgeR( *mpParent->mpKernel);

	opedgeL.Init( mINode, mINodeL, mtabBall);
	opedgeR.Init( mINode, mINodeR, mtabBall);


	// make the choice
	bool bokL = true;
	if ( opedgeL.Error() != 0 || mflagL != 0 )
	//if ( opedgeL.Error() != 0 )
		bokL = false;

	bool bokR = true;
	if ( opedgeR.Error() != 0 || mflagR != 0 )
	//if ( opedgeR.Error() != 0 )
		bokR = false;

	if ( ! bokL && ! bokR)	// both are negative
	{
		//WriteGrdTEC<DIM_3D>	write( & mpKernel->cGrid() );
		//write.WriteCells( "_dest_ball.dat", tabball);

		//THROW_INTERNAL( "STOP");

		return false;
	}

	MGSize ch=0; // 1 left; 2 right
	if ( bokL && bokR )
	{
		if ( opedgeR.VolMinAfter() < opedgeL.VolMinAfter() )
			ch = 1;
		else
			ch = 2;
	}

	//if ( (! bokL) || (opedgeR.QAfter() < opedgeL.QAfter() && bokR) )
	if ( (! bokL) || (opedgeR.VolMinAfter() > opedgeL.VolMinAfter() && bokR) )
		ch = 2;

	//if ( (! bokR) || (opedgeL.QAfter() < opedgeR.QAfter() && bokL) )
	if ( (! bokR) || (opedgeL.VolMinAfter() > opedgeR.VolMinAfter() && bokL) )
		ch = 1;

	//if ( bdbg)
	//{
	//	cout << "left  = " << opedgeL.VolMinAfter() << "  :   " << opedgeL.QAfter() << endl;
	//	cout << "right = " << opedgeR.VolMinAfter() << "  :   " << opedgeR.QAfter() << endl;
	//}

	if ( ch != 1 && ch != 2 )
	{
		//cout << "left  = " << volL << "  :   " << qL << endl;
		//cout << "right = " << volR << "  :   " << qR << endl;
		THROW_INTERNAL( "EdgeDestr::RemoveNode() :: bad choice" );
	}

	//cout << endl << "ch = " << ch << endl;
	//cout << "left  = " << volL << " " << qL << endl;
	//cout << "right = " << volR << " " << qR << endl;

		
	// execute
	try
	{
	if ( ch == 1)
		opedgeL.Execute();
	else
	if ( ch == 2)
		opedgeR.Execute();
	else
		return false;
	}
	catch ( EHandler::Except& )
	{
		WriteGrdTEC<DIM_3D>	write( & mpParent->mpKernel->cGrid() );
		write.WriteCells( "_dest_ball.dat", mtabBall);
		throw;
	}


	//if ( mINode == 24107)
	//	THROW_INTERNAL( "STOP");

	return true;
}


void SPointDestructor::UpdateSubEdges( vector< Key<2> >& tabsedge)
{
	Key<2> newsedge(mINodeL, mINodeR);
	newsedge.Sort();

	vector< Key<2> >	tabNSEdge;
	for ( MGSize i=0; i<tabsedge.size(); ++i)
	{
		if ( tabsedge[i] != mEdgeL && tabsedge[i] != mEdgeR)
			tabNSEdge.push_back( tabsedge[i] );
	}

	tabNSEdge.push_back( newsedge );

	tabsedge.swap( tabNSEdge );
}



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

