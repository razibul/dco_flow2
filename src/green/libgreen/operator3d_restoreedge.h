#ifndef __OPERATOR3D_RESTOREEDGE_H__
#define __OPERATOR3D_RESTOREEDGE_H__


#include "libcoresystem/mgdecl.h"
#include "libgreen/tdefs.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Dimension DIM> 
class Grid;

template <Dimension DIM>
class ControlSpace;

template <Dimension DIM> 
class Kernel;


//////////////////////////////////////////////////////////////////////
// class Operator3D_RestoreEdge
//////////////////////////////////////////////////////////////////////
class Operator3D_RestoreEdge
{
	typedef TDefs<DIM_3D>::GVect	GVect;
	typedef TDefs<DIM_3D>::GMetric	GMetric;

	typedef TDefs<DIM_3D>::GNode	GNode;
	typedef TDefs<DIM_3D>::GCell	GCell;
	typedef TDefs<DIM_3D>::GFace	GFace;

public:
	Operator3D_RestoreEdge( const ControlSpace<DIM_3D>& cspace, Kernel<DIM_3D>& kernel) : mCSpace(cspace), mKernel(kernel), mErrorCode(0)	{}

	const MGSize&	Error() const		{ return mErrorCode;}

	void	Reset();

	void	Init( const Key<2>& edgeL, const Key<2>& edgeR);

	void	ExecuteInternal();
	void	ExecuteExternal();

private:
	const ControlSpace<DIM_3D>&	mCSpace;
	Kernel<DIM_3D>&				mKernel;

	MGSize			mErrorCode;


	Key<2>			mEdgeL;
	Key<2>			mEdgeR;

	MGSize			mIdNode;
	MGSize			mIdNodeL;
	MGSize			mIdNodeR;

	vector<MGSize>	mtabInCell;
	vector<MGSize>	mtabExCell;
};



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __OPERATOR3D_RESTOREEDGE_H__

