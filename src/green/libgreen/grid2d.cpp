#include "grid.h"
#include "libcoreio/store.h"
#include "libcoregeom/geom.h"
#include "libgreen/gridgeom.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



template <>
void Grid<DIM_2D>::FindEdges( vector< Key<1> >& tabedg) const
{
	THROW_INTERNAL( "Grid<DIM_2D>::FindEdges : not valid for 2D !");
}

template <>
void Grid<DIM_2D>::FindFaces( vector< Key<2> >& tabfac) const
{
	typedef vector< Key<2> >	TKeyTab;

	TKeyTab	tabtmp;

	tabfac.clear();

	for ( ColCell::const_iterator itr = mtabCell.begin(); itr != mtabCell.end(); ++itr)
	{
		for ( MGSize k=0; k<GCell::SIZE; ++k)
		{
			Key<2> key = (*itr).FaceKey( k);
			key.Sort();
			tabtmp.push_back( key);
		}
	}

	sort( tabtmp.begin(), tabtmp.end() );
	TKeyTab::iterator itrend = unique( tabtmp.begin(), tabtmp.end() );
	tabtmp.erase( itrend, tabtmp.end() );

	tabfac.resize( tabtmp.size() );
	copy( tabtmp.begin(), tabtmp.end(), tabfac.begin() );
}

template <>
void Grid<DIM_2D>::FindInternalEdges( vector< Key<2> >& tabfac) const
{
	typedef vector< Key<2> >	TKeyTab;

	TKeyTab	tabtmp;

	tabfac.clear();

	for ( ColCell::const_iterator itr = mtabCell.begin(); itr != mtabCell.end(); ++itr)
	{
		if ( (*itr).IsExternal() )
			continue;

		for ( MGSize k=0; k<GCell::SIZE; ++k)
		{
			if ( mtabCell[ (*itr).cCellId( k).first ].IsExternal() )
				continue;

			Key<2> key = (*itr).FaceKey( k);
			key.Sort();
			tabtmp.push_back( key);
		}
	}

	sort( tabtmp.begin(), tabtmp.end() );
	TKeyTab::iterator itrend = unique( tabtmp.begin(), tabtmp.end() );
	tabtmp.erase( itrend, tabtmp.end() );

	tabfac.resize( tabtmp.size() );
	copy( tabtmp.begin(), tabtmp.end(), tabfac.begin() );
}


template <>
void Grid<DIM_2D>::UpdateNodeCellIds()
{
	for ( Grid<DIM_2D>::ColCell::const_iterator itrc = mtabCell.begin(); itrc != mtabCell.end(); ++itrc)
		for ( MGSize k=0; k<GCell::SIZE; ++k)
			rNode( itrc->cNodeId(k) ).rCellId() = itrc->cId();
}


template <>
void Grid<DIM_2D>::Dump( const MGString& fname) const
{
	ofstream f( fname.c_str() );

	for ( ColCell::const_iterator itr = mtabCell.begin(); itr != mtabCell.end(); ++itr)
	{
		const GCell &c = *itr;

		c.Dump( *this, f);
	}
}


template <>
void Grid<DIM_2D>::CheckConnectivity() const
{
	cout << "CheckConnectivity\n";
	
	//FILE *f = gtrace.Open();
	ofstream f( "_trace.txt" );
	
	MGFloat	volume = 0.0;
	bool	bOk = true;

	//printf( "CellEnd() = %d\n", CellEnd() );
	for ( ColCell::const_iterator itr = mtabCell.begin(); itr != mtabCell.end(); ++itr)
	{
		const GCell &cell = *itr;
		MGFloat cvol = GridGeom<DIM_2D>::CreateSimplex( cell, *this).Volume();

		volume += cvol;

		if ( cell.cId() != itr.index() )
			f << "bad cell id\n";


		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		{
			MGSize idfc = cell.cCellId( ifc).first;
			Key<2> key1 = cell.FaceKey( ifc);
			key1.Sort();

			if ( idfc)
			{
				const GCell &neicell = cCell(idfc);
				Key<2> key2 = neicell.FaceKey( cell.cCellId( ifc).second);
				key2.Sort();

				if ( neicell.cCellId( cell.cCellId( ifc).second).first != cell.cId() )
				{
					bOk = false;
					f << "\nconnectivity error 1 - cells ids are corrupted\n";
					f << "cell_face_id = " << cell.cCellId( ifc).second << endl;
					cell.Dump( *this, f);
					neicell.Dump( *this, f);
					//Dump( "_grd_conn.dump" );
				}
				
				if ( ! (key1 == key2) )
				{
					bOk = false;
					f << "\nconnectivity error 2 - face nodes are different\n";
					f << "cell_face_id = " << cell.cCellId( ifc).second << endl;
					cell.Dump( *this, f);
					neicell.Dump( *this, f);
					//Dump( "_grd_conn.dump" );
				}
				

				if ( neicell.cCellId( cell.cCellId( ifc).second).second != ifc )
				{
					bOk = false;
					f << "connectivity error 3\n";
				}

			}
		}
	}

	MGFloat boxvolume = Geom::VolumeQUAD( cNode(1).cPos(), cNode(2).cPos(), cNode(3).cPos(), cNode(4).cPos() );

	f << "sum of the cell volumes = " << setprecision(12) << volume << endl;
	f << "box volume = " << setprecision(12) << boxvolume << endl;

	
	if ( ! bOk)
		THROW_INTERNAL( "CRASH !!!");
}



//template <>
//void Grid<DIM_2D>::RebuildConnectivity()
//{
//	cout << "Grid<DIM_2D>::RebuildConnectivity()" << endl;
//
//	typedef pair< Key<DIM_2D>, GNeDef > FaceInfo;
//	vector< FaceInfo >	tabface;
//
//	tabface.reserve( (DIM_2D+1)*SizeCellTab() );
//
//	for ( ColCell::const_iterator itr = mtabCell.begin(); itr != mtabCell.end(); ++itr)
//	{
//		for ( MGSize k=0; k<GCell::SIZE; ++k)
//		{
//			Key<DIM_2D> key = (*itr).FaceKey( k);
//			key.Sort();
//
//			GNeDef	ndef( itr->cId(), k);
//			tabface.push_back( FaceInfo( key, ndef) );
//		}
//	}
//
//	sort( tabface.begin(), tabface.end() );
//
//
//	vector< Key<DIM_2D> >	tabbnd;
//
//	for ( vector<FaceInfo>::iterator itr=tabface.begin(); itr!=tabface.end(); ++itr)
//	{
//		vector<FaceInfo>::iterator itrnext = itr+1;
//
//		if ( itrnext != tabface.end() )
//			if ( itr->first == itrnext->first )
//			{
//				// double case
//				// update
//				const GNeDef& neA = itr->second;
//				const GNeDef& neB = itrnext->second;
//				rCell( neA.first ).rCellId( neA.second ) = neB; 
//				rCell( neB.first ).rCellId( neB.second ) = neA; 
//
//				itr = itrnext;
//				continue;
//			}
//
//		// single case - boundary
//		tabbnd.push_back( itr->first );
//	}
//
//	CheckConnectivity();
//
//	///////////////////////////////////////////////////////////
//	ofstream f( "_bnd_after_reconstr_.dat" );
//
//	cout << "\nWriting TEC from RebuildConnectivity" << std::flush;
//
//	f << "TITLE = \"boundary grid\"\n";
//	f << "VARIABLES = ";
//	f << "\"X\", \"Y\"";
//	f << "\n";
//
//	f << "ZONE T=\"bnd grid\", N=" << SizeNodeTab() << ", E=" << tabbnd.size() << ", F=FEPOINT, ET=LINESEG\n";
//
//	for ( Grid<DIM_2D>::ColNode::const_iterator itrn = cColNode().begin(); itrn != cColNode().end(); ++itrn)
//	{
//		for ( MGSize k=0; k<DIM_2D; ++k)
//			f << setprecision(8) << (*itrn).cPos().cX( k) << " ";
//		f << endl;
//	}
//
//	for ( MGSize i=0; i<tabbnd.size(); ++i)
//	{
//		f << tabbnd[i].cFirst() << " " << tabbnd[i].cSecond() << endl;
//	}
//
//	cout << " - FINISHED\n";
//
//
//}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

