#ifndef __GRIDMASTER_H__
#define __GRIDMASTER_H__

#include "libcoresystem/mgdecl.h"
#include "libcorecommon/triple.h"
#include "libgreen/gridwbnd.h"
#include "libgreen/controlspace.h"
#include "libgreen/gridcontext.h"




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Dimension DIM>
class Master;


//////////////////////////////////////////////////////////////////////
//	class GridOwner
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class GridOwner : public GridOwner< static_cast<Dimension>( DIM-1 ) >
{
	friend class Master<DIM_2D>;
	friend class Master<DIM_3D>;

public:

//protected:
	template <Dimension D>
	MGSize	CreateGrid()
	{
		GridOwner<D>::mtabGrid.push_back( GridWBnd<D>() );
		MGSize id = GridOwner<D>::mtabGrid.size();
		GridOwner<D>::mtabGrid[id-1].rId() = id;
		return id;
	}

	template <Dimension D>
	const GridWBnd<D>&	cGrid( const MGSize& id) const	{ return GridOwner<D>::mtabGrid[id-1]; }

	template <Dimension D>
	GridWBnd<D>&		rGrid( const MGSize& id)		{ return GridOwner<D>::mtabGrid[id-1]; }

	template <Dimension D>
	MGSize	Size() const { return GridOwner<D>::mtabGrid.size(); }

protected:
	vector< GridWBnd<DIM> >	mtabGrid;
};


//////////////////////////////////////////////////////////////////////
//	class GridOwner + specialization for 0D
//////////////////////////////////////////////////////////////////////
template <>
class GridOwner<DIM_0D>
{
public:

protected:
	vector< GridWBnd<DIM_0D> >	mtabGrid;
};





//////////////////////////////////////////////////////////////////////
//	class GridMaster
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class GridMaster : public GridOwner<DIM>
{
	typedef typename TDefs<DIM>::GVect		GVect;
	typedef typename TDefs<DIM>::GMetric	GMetric;
	friend class Master<DIM>;

public:
	const GridContext<GVect>&	cMasterPosCx() const	{ return mMasterPosCx;}
	const GridContext<GMetric>&	cMasterMetricCx() const	{ return mMasterMetricCx;}

protected:
	GridContext<GVect>&			rMasterPosCx()			{ return mMasterPosCx;}
	GridContext<GMetric>&		rMasterMetricCx()		{ return mMasterMetricCx;}

private:
	GridContext<GVect>		mMasterPosCx;
	GridContext<GMetric>	mMasterMetricCx;
};



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __GRIDMASTER_H__
