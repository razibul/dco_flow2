#ifndef __EDGESPLIT_H__
#define __EDGESPLIT_H__

#include "libcoresystem/mgdecl.h"
#include "libgreen/tdefs.h"
#include "libgreen/grid.h"
#include "libgreen/protectentities.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

template <Dimension DIM>
class ControlSpace;

template <Dimension DIM> 
class Kernel;


//////////////////////////////////////////////////////////////////////
// class EdgeSplit
//////////////////////////////////////////////////////////////////////
class EdgeSplit
{
	typedef TDefs<DIM_3D>::GVect	GVect;
	typedef TDefs<DIM_3D>::GNode	GNode;
	typedef TDefs<DIM_3D>::GCell	GCell;
	typedef TDefs<DIM_3D>::GFace	GFace;

	friend class FaceSplit;

public:
	EdgeSplit()  : mpCSpace(NULL), mpKernel(NULL)		{}

	EdgeSplit( const Key<2>& edge, const ControlSpace<DIM_3D>& cspace, Kernel<DIM_3D>& kernel) 
		: mEdge(edge), mpCSpace(&cspace), mpKernel(&kernel)		{}


	MGSize			Size() const						{ return mtabSubEdges.size();}
	const Key<2>&	cEdge()	const						{ return mEdge; }
	const Key<2>&	cSubEdge( const MGSize& id) const	{ return mtabSubEdges[id];}

	MGSize			SizeInNodes() const					{ return mtabINodes.size();}
	const MGSize&	cInNode( const MGSize& id) const	{ return mtabINodes[id]; }

	bool	IsRecoverd();
	void	Recover( ProtectEntities& prent);
	void	UpdateSubElems();

	void	ExportTEC( const MGString& name);

private:
	bool	IsSubEdgeRecovered( const Key<2>& edge);
	void	FindCrossings( vector<GVect>& tabcross);
	void	FindBestCrossings( GVect& vcross, Key<3>& face, const Key<2>& edge);

private:
	const ControlSpace<DIM_3D>	*mpCSpace;
	Kernel<DIM_3D>				*mpKernel;

	Key<2>				mEdge;
	vector<MGSize>		mtabINodes;
	vector< Key<2> >	mtabSubEdges;
};



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __EDGESPLIT_H__
