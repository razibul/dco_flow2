#include "cspacegridglob.h"
#include "libgreen/bndgrid.h"
#include "libcorecommon/progressbar.h"
#include "libgreen/writegrdtec.h"
#include "libgreen/reconstructor.h"
#include "libgreen/kernel.h"
#include "libgreen/readmeandr.h"
#include "libgreen/writemeandr.h"

#include "libgreen/grid_ioproxy.h"
#include "libgreen/gridcontext_ioproxy.h"
#include "libcoreio/readtec.h"

//#include "libgreen/readtec.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//ofstream mFile;
//MGSize mCount;
//MGSize mCountHit;
//MGSize mCountLost;
//


//template< class ELEM_TYPE, MGSize MAX_SIZE>
//inline void DecomposeROOT( SMatrix< MAX_SIZE, ELEM_TYPE>& mtxout, const SMatrix< MAX_SIZE, ELEM_TYPE>& mtx)
//{
//	// TODO :: check if matrix is symetric
//	SMatrix< MAX_SIZE, ELEM_TYPE> mtxD;	// TODO :: should be SVector
//	SMatrix< MAX_SIZE, ELEM_TYPE> mtxtmp = mtx;
//	SMatrix< MAX_SIZE, ELEM_TYPE> mtxN;
//
//	mtxtmp.Decompose( mtxD, mtxN, true);
//
//
//	for ( MGSize k=0; k<MAX_SIZE; ++k)	// TODO :: check if it should not be Size() ???
//		mtxD(k,k) = 1.0 / ::sqrt( mtxD(k,k));
//
//	mtxN.Transp();
//
//	mtxout = mtxD * mtxN;
//	return;
//}
//
//template< class ELEM_TYPE, MGSize MAX_SIZE>
//inline void DecomposeSQR( SMatrix< MAX_SIZE, ELEM_TYPE>& mtxout, const SMatrix< MAX_SIZE, ELEM_TYPE>& mtx)
//{
//
//	// TODO :: check if matrix is symetric
//	SMatrix< MAX_SIZE, ELEM_TYPE> mtxNT;
//	SMatrix< MAX_SIZE, ELEM_TYPE> mtxN;
//
//	mtxN = mtx;
//
//	SVector< ELEM_TYPE, MAX_SIZE> vecD;
//	for ( MGSize i=0; i<MAX_SIZE; ++i)
//	{
//		MGFloat sum = 0.0;
//		for ( MGSize j=0; j<MAX_SIZE; ++j)
//			sum += mtxN(i,j)*mtxN(i,j);
//
//		//vecD(i) = ::sqrt( sum);
//		for ( MGSize j=0; j<MAX_SIZE; ++j)
//			mtxN(i,j) /= sum;
//	}
//
//	mtxNT = mtxN;
//	mtxNT.Transp();
//	mtxout = mtxNT*mtxN;
//	return;
//
//}


template< class ELEM_TYPE, MGSize MAX_SIZE>
inline void DecomposeROOT( SMatrix< MAX_SIZE, ELEM_TYPE>& mtxout, const SMatrix< MAX_SIZE, ELEM_TYPE>& mtx)
{
	// TODO :: check if matrix is symetric
	SVector< MAX_SIZE, ELEM_TYPE> vecD;
	SMatrix< MAX_SIZE, ELEM_TYPE> mtxtmp = mtx;
	SMatrix< MAX_SIZE, ELEM_TYPE> mtxN;

	mtxtmp.Decompose( vecD, mtxN, true);


	for ( MGSize k=0; k<MAX_SIZE; ++k)	// TODO :: check if it should not be Size() ???
		vecD(k) = 1.0 / ::sqrt( vecD(k));
		//mtxD(k,k) = 1.0 / ::pow( mtxD(k,k),0.4);

	SMatrix< MAX_SIZE, ELEM_TYPE> mtxNT = mtxN;
	mtxNT.Transp();

	mtxout.Assemble( mtxN, vecD, mtxNT );
	return;
}

//template< class ELEM_TYPE, MGSize MAX_SIZE>
//inline void DecomposeSQR( SMatrix< MAX_SIZE, ELEM_TYPE>& mtxout, const SMatrix< MAX_SIZE, ELEM_TYPE>& mtx)
//{
//	// TODO :: check if matrix is symetric
//	SMatrix< MAX_SIZE, ELEM_TYPE> mtxtmp = mtx;
//
//	mtxtmp.Invert();
//
//	mtxout = mtxtmp * mtxtmp;
//	return;
//}


template< class ELEM_TYPE, MGSize MAX_SIZE>
inline void DecomposeSQR( SMatrix< MAX_SIZE, ELEM_TYPE>& mtxout, const SMatrix< MAX_SIZE, ELEM_TYPE>& mtx)
{
	// TODO :: check if matrix is symetric
	SVector< MAX_SIZE, ELEM_TYPE> vecD;
	SMatrix< MAX_SIZE, ELEM_TYPE> mtxtmp = mtx;
	SMatrix< MAX_SIZE, ELEM_TYPE> mtxN;

	mtxtmp.Decompose( vecD, mtxN, true);


	for ( MGSize k=0; k<MAX_SIZE; ++k)	// TODO :: check if it should not be Size() ???
		vecD(k) = 1.0 / ( vecD(k) * vecD(k));
		//mtxD(k,k) = 1.0 / ::pow( mtxD(k,k), 1.0/0.4);

	SMatrix< MAX_SIZE, ELEM_TYPE> mtxNT = mtxN;
	mtxNT.Transp();

	mtxout.Assemble( mtxN, vecD, mtxNT);
	return;
}





template <Dimension DIM>
typename TDefs<DIM>::GMetric GridGlobControlSpace<DIM>::GetSpacing( const GVect& vct) const
{
	//cout << "-------------\n";
	vector< pair<MGSize,MGFloat> > tabcoeff;

	//++mCount;

	bool bOk = mInterpol.FindCoeff( tabcoeff, vct, false);

	if ( ! bOk)
	{
		//++mCountLost;
		
		//for ( MGSize k=0; k<DIM; ++k)
		//	mFile << setprecision(8) << vct.cX( k) << " ";
		//mFile << endl;

		if ( mBndGrid.SizeNodeTab() == 0 )
			THROW_INTERNAL( "GridGlobControlSpace<"<<DIM<<">::GetSpacing -- mBndGrid is not initialized");

		return GetBndSpacing( vct);
	}


	//++mCountHit;


	GMetric retmet;
	retmet.Reset();

	typename GMetric::SMtx smtxret;
	smtxret.Init( 0.0 );

	for ( MGSize i=0; i<tabcoeff.size(); ++i)
	{
		MGSize idmet = mGrid.cNode( tabcoeff[i].first ).cMasterId();
		idmet = tabcoeff[i].first;
		SMtx smtx = mMetricCx.cData( idmet);

		//smtx.Write();

		//DecomposeROOT( smtx, smtx);

		//smtx.Write();
		//DecomposeSQR( smtx, smtx);
		//smtx.Write();

		smtxret += tabcoeff[i].second * smtx;
	}

	DecomposeSQR( smtxret, smtxret );

	return GMetric( smtxret);
}


template <Dimension DIM>
typename TDefs<DIM>::GMetric GridGlobControlSpace<DIM>::GetBndSpacing( const GVect& vct) const
{
	//cout << "-------------\n";
	vector< pair<MGSize,MGFloat> > tabcoeff;

	mBndInterpol.FindCoeff( tabcoeff, vct);

	GMetric retmet;
	retmet.Reset();

	typename GMetric::SMtx smtxret;
	smtxret.Init( 0.0 );

	for ( MGSize i=0; i<tabcoeff.size(); ++i)
	{
		MGSize idmet = mBndGrid.cNode( tabcoeff[i].first ).cMasterId();
		SMtx smtx = mBndMetricCx.cData( idmet);

		//smtx.Write();

		//DecomposeROOT( smtx, smtx);

		//smtx.Write();
		//DecomposeSQR( smtx, smtx);
		//smtx.Write();

		smtxret += tabcoeff[i].second * smtx;
	}

	DecomposeSQR( smtxret, smtxret );

	return GMetric( smtxret);
}




template <Dimension DIM>

void GridGlobControlSpace<DIM>::Init( const GridContext<GMetric>& cxmet, const BndGrid<DIM>& bnd)
{
	//cout << "DIM = " << DIM << " UNIDIM = " << UNIDIM << endl;
	//THROW_INTERNAL( "Not implemented");

	GeneratorData<DIM> kerdata;
	Kernel<DIM> kernel( mGrid, kerdata);


	ProgressBar	bar(40);

	bar.Init( bnd.SizeNodeTab() );
	bar.Start();


	GVect	vmin, vmax;

	bnd.FindMinMax( vmin, vmax);
	
	kernel.InitBoundingBox( vmin, vmax);

	kernel.InitCube();

	// setting metric for corner nodes of the bounding box
	for ( typename Grid<DIM>::ColNode::iterator itr = mGrid.rColNode().begin(); itr != mGrid.rColNode().end(); ++itr)
	{
		GMetric	met;
		met.InitIso(1.0);

		SMtx smtx( met);
		DecomposeROOT( smtx, smtx);

		MGSize idm = mMetricCx.Insert( smtx );
		//mMetricCx.Insert( idm, smtx );
		itr->rMasterId() = idm;
	}



	// triangulation of the points
	vector<MGSize>	tabid(bnd.SizeNodeTab());
	for ( MGSize i=1; i<bnd.SizeNodeTab()+1; ++i)
		tabid[i-1] = i;

	random_shuffle( tabid.begin(), tabid.end() );


	for ( MGSize ii=0; ii<tabid.size(); ++ii, ++bar)
	{
		MGSize i = tabid[ii];

		GVect vct = bnd.cNode(i).cPos();


		MGSize inod = kernel.InsertPoint( vct );

		if ( ! inod)
		{
			//char sbuf[512];
			//sprintf( sbuf, "CRASH :: kernel failed to insert new point with coordinates [%lg %lg %lg]", 
			//				vct.cX(), vct.cY(), vct.cZ() );

			THROW_INTERNAL( "CRASH :: kernel failed to insert new point with coordinates [" << 
				vct.cX() << " " << vct.cY() << " " << vct.cZ() << "]" );
		}

		SMtx smtx( cxmet.cData( bnd.cNode(i).cMasterId() ) );
		DecomposeROOT( smtx, smtx);

		MGSize idm = mMetricCx.Insert( smtx );
		//mMetricCx.Insert( idm, smtx );
		mGrid.rNode(inod).rMasterId() = idm;

		//mpGrd->rNode(inod).rMasterId() = bnd.cNode(i).cMasterId();


		//wtec.DoWrite( "_tmp_grd.plt");


		//fprintf( f, "node id = %d\n", i);
		//smtx = SMatrix<MGFloat,3>( bnd.cNode(i).cMetric());
		//smtx.Decompose( smtxD, smtxL);
		//smtxD.Write( f);
		//smtxL.Write( f);
	}
	
	bar.Finish();

	WriteGrdTEC<DIM> wtec( &mGrid);
	wtec.DoWrite( "cspacegrid_dim.plt");

}


template <Dimension DIM>
void GridGlobControlSpace<DIM>::InitFileTEC( const MGString& fname)
{
	MGString fnameTEC = fname + MGString(".dat");

	//ReadTEC<DIM>	reader( IOReadGreenProxy<DIM>( &mGrid, NULL), &mMetricCx );
	//reader.DoRead( fnameTEC);

	IOGreenCxSMtxProxy<DIM> cxproxy( &mMetricCx);
	IOReadGreenProxy<DIM>	grdproxy( &mGrid, NULL);
	IO::ReadTEC	reader( grdproxy, &cxproxy );

	//IO::ReadTEC	reader( IOReadGreenProxy<DIM>( &mGrid, NULL), &IOGreenCxSMtxProxy<DIM>( &mMetricCx) );
	reader.DoRead( fnameTEC);

	for ( MGSize i=1; i<=mMetricCx.Size(); ++i)
	{
		SMtx smtx( mMetricCx.cData(i));
		DecomposeROOT( smtx, smtx);
		mMetricCx.Insert( i, smtx);
	}

}

template <Dimension DIM>
void GridGlobControlSpace<DIM>::PostInitTEC()
{
	vector< Key<DIM> > tabbnd;
	mGrid.RebuildConnectivity( &tabbnd );

	GVect	vmin, vmax;

	map<MGSize,MGSize>	mapbnode;
	for ( MGSize i=0; i<tabbnd.size(); ++i)
	{
		for ( MGSize k=0; k<DIM; ++k)
			if ( mapbnode.find( tabbnd[i].cElem(k) ) == mapbnode.end() )
				mapbnode.insert( map<MGSize,MGSize>::value_type( tabbnd[i].cElem(k), 0 ) );
	}

	vmin = vmax = mGrid.cNode( mapbnode.begin()->first ).cPos();
	MGSize id = 0;
	for ( map<MGSize,MGSize>::iterator itrn = mapbnode.begin(); itrn != mapbnode.end(); ++itrn)
	{
		itrn->second = ++id;
		const GVect& vpos = mGrid.cNode( itrn->first ).cPos();
		vmin = Minimum( vmin, vpos);
		vmax = Maximum( vmax, vpos);
	}


	// setup tree
	mInterpol.rLocalizator().InitBox( Box<DIM>( vmin, vmax) );  

	typename Grid<DIM>::ColCell::const_iterator	itr;
	for ( itr = mGrid.cColCell().begin(); itr != mGrid.cColCell().end(); ++itr)
	{
		//mInterpol.rLocalizator().IncCounter();
		//if ( mInterpol.rLocalizator().TimeToInsert() )
		{
			GVect vc = GridGeom<DIM>::CreateSimplex( *itr, mGrid).Center();
			mInterpol.rLocalizator().TreeInsertSafe( vc, itr->cId() );
		}
	}

	///////////////////////////////////////////////////////////
	ofstream f( "_bnd_.dat" );

	cout << "\nWriting TEC from PostInitTEC" << std::flush;

	f << "TITLE = \"boundary grid\"\n";
	f << "VARIABLES = ";
	f << "\"X\", \"Y\"";
	f << "\n";

	f << "ZONE T=\"bnd grid\", N=" << mapbnode.size() << ", E=" << tabbnd.size() << ", F=FEPOINT, ET=LINESEG\n";

	for ( map<MGSize,MGSize>::const_iterator itrn = mapbnode.begin(); itrn != mapbnode.end(); ++itrn)
	{
		const GVect& vpos = mGrid.cNode( itrn->first ).cPos();
		for ( MGSize k=0; k<DIM; ++k)
			f << setprecision(8) << vpos.cX( k) << " ";
		f << endl;
	}

	for ( MGSize i=0; i<tabbnd.size(); ++i)
	{
		for ( MGSize k=0; k<DIM; ++k)
			f << mapbnode[ tabbnd[i].cElem(k) ] << " ";
		f << endl;
	}

	cout << " - FINISHED\n";
	///////////////////////////////////////////////////////////


	GeneratorData<DIM> kerdata;
	Kernel<DIM> kernel( mBndGrid, kerdata);

	kernel.InitBoundingBox( vmin, vmax);
	kernel.InitCube();

	// setting metric for corner nodes of the bounding box
	for ( typename Grid<DIM>::ColNode::iterator itr = mBndGrid.rColNode().begin(); itr != mBndGrid.rColNode().end(); ++itr)
	{
		GMetric	met;
		met.InitIso(1.0);

		SMtx smtx( met);
		DecomposeROOT( smtx, smtx);

		MGSize idm = mBndMetricCx.Insert( smtx );
		//mMetricCx.Insert( idm, smtx );
		itr->rMasterId() = idm;
	}



	// triangulation of the boundary points
	for ( map<MGSize,MGSize>::iterator itrn = mapbnode.begin(); itrn != mapbnode.end(); ++itrn)
	{
		const GVect& vct = mGrid.cNode( itrn->first ).cPos();


		MGSize inod = kernel.InsertPoint( vct );

		if ( ! inod)
		{
			//char sbuf[512];
			//sprintf( sbuf, "CRASH :: kernel failed to insert new point with coordinates [%lg %lg %lg]", 
			//				vct.cX(), vct.cY(), vct.cZ() );

			THROW_INTERNAL( "CRASH :: kernel failed to insert new point with coordinates [" << 
				vct.cX() << " " << vct.cY() << " " << vct.cZ() << "]" );
		}

		SMtx smtx( mMetricCx.cData( itrn->first ) );
		//DecomposeROOT( smtx, smtx);

		MGSize idm = mBndMetricCx.Insert( smtx );
		//mMetricCx.Insert( idm, smtx );
		mBndGrid.rNode(inod).rMasterId() = idm;

		//mpGrd->rNode(inod).rMasterId() = bnd.cNode(i).cMasterId();


		//wtec.DoWrite( "_tmp_grd.plt");


		//fprintf( f, "node id = %d\n", i);
		//smtx = SMatrix<MGFloat,3>( bnd.cNode(i).cMetric());
		//smtx.Decompose( smtxD, smtxL);
		//smtxD.Write( f);
		//smtxL.Write( f);
	}
	

	WriteGrdTEC<DIM> wtec( &mBndGrid);
	wtec.DoWrite( "_bnd_cspacegrid_dim.plt");


}


template <Dimension DIM>
void GridGlobControlSpace<DIM>::InitFileMEANDR( const MGString& fname)
{
	GridContext<GMetric>	cxMetric;
	ReadMEANDR<DIM>	reader( mGrid, cxMetric );
	reader.DoRead( fname);

	for ( MGSize i=1; i<=cxMetric.Size(); ++i)
	{
		SMtx smtx( cxMetric.cData(i));
		DecomposeROOT( smtx, smtx);
		mMetricCx.Insert( i, smtx);
	}
}


template <Dimension DIM>
void GridGlobControlSpace<DIM>::ExportFileMEANDR( const MGString& fname)
{
	GridContext<GMetric>	cxMetric;

	for ( MGSize i=1; i<=mMetricCx.Size(); ++i)
	{
		SMtx smtx( mMetricCx.cData(i) );
		DecomposeSQR( smtx, smtx);
		cxMetric.Insert( i, GMetric(smtx) );
	}

	WriteMEANDR<DIM>	writer( mGrid, cxMetric );
	writer.DoWrite( fname);
}




template <Dimension DIM>
void GridGlobControlSpace<DIM>::ApplyScale( const MGFloat& scale)
{
	MGFloat c = 1. / sqrt( scale);
	for ( MGSize i=1; i<=mMetricCx.Size(); ++i)
	{
		SMtx smtx( mMetricCx.cData(i) );
		smtx *= c;
		mMetricCx.Insert( i, smtx);
	}
}


template class GridGlobControlSpace<DIM_2D>;
template class GridGlobControlSpace<DIM_3D>;

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

