#include "writegrdtec.h"
#include "libgreen/grid.h"
#include "libgreen/gridgeom.h"
#include "libcoreio/store.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 




template <>
void WriteGrdTEC<DIM_1D>::DoWrite( const MGString& fname, const MGString& gname)
{
	mFName = fname;
	ofstream f( fname.c_str() );

	cout << "\nWriting TEC '" << fname.c_str() << "'" << std::flush;

	f << "TITLE = \"grid " << gname << "\"\n";
	f << "VARIABLES = ";
	f << "\"X\"";
	f << "\n";

	f << "ZONE T=\"grid " << gname << "\", N=" << mpGrd->SizeNodeTab() << ", E=" << mpGrd->SizeCellTab() << ", F=FEPOINT, ET=LINESEG\n";

	for ( Grid<DIM_1D>::ColNode::const_iterator itrn = mpGrd->cColNode().begin(); itrn != mpGrd->cColNode().end(); ++itrn)
	{
		for ( MGSize k=0; k<DIM_1D; ++k)
			//fprintf( f, " %10.7lg", (*itrn).cPos().cX( k) );
			f << setprecision(8) << (*itrn).cPos().cX( k);
		f << endl;
	}

	for ( Grid<DIM_1D>::ColCell::const_iterator itrc = mpGrd->cColCell().begin(); itrc != mpGrd->cColCell().end(); ++itrc)
	{
		//fprintf( f, "%d %d\n", 
		//		mpGrd->cNode( (*itrc).cNodeId(0) ).cWriteId(), 
		//		mpGrd->cNode( (*itrc).cNodeId(1) ).cWriteId() );

	}

	cout << " - FINISHED\n";
}



template <>
void WriteGrdTEC<DIM_2D>::DoWrite( const MGString& fname, const MGString& gname)
{
	mFName = fname;
	ofstream f( fname.c_str() );

	cout << "\nWriting TEC '" << fname.c_str() << "'" << std::flush;

	f << "TITLE = \"grid " << gname << "\"\n";
	f << "VARIABLES = ";
	f << "\"X\", \"Y\"";
	f << "\n";

	f << "ZONE T=\"grid " << gname << "\", N=" << mpGrd->SizeNodeTab() << ", E=" << mpGrd->SizeCellTab() << ", F=FEPOINT, ET=TRIANGLE\n";

	MGSize wid = 1;
	vector<MGSize>	tabid( mpGrd->cColNode().size() );

	for ( Grid<DIM_2D>::ColNode::const_iterator itrn = mpGrd->cColNode().begin(); itrn != mpGrd->cColNode().end(); ++itrn)
	{
		tabid[ (*itrn).cId() ] = wid++;

		for ( MGSize k=0; k<DIM_2D; ++k)
			f << setprecision(18) << (*itrn).cPos().cX( k) << " ";
		f << endl;
	}

	for ( Grid<DIM_2D>::ColCell::const_iterator itrc = mpGrd->cColCell().begin(); itrc != mpGrd->cColCell().end(); ++itrc)
	{
		f << tabid[ (*itrc).cNodeId(0) ] << " " << tabid[ (*itrc).cNodeId(1) ] << " " << tabid[ (*itrc).cNodeId(2) ] << endl;
	}

	cout << " - FINISHED\n";
}



template <>
void WriteGrdTEC<DIM_3D>::DoWrite( const MGString& fname, const MGString& gname)
{
	mFName = fname;
	ofstream f( fname.c_str() );

	cout << "\nWriting TEC '" << fname.c_str() << "'" << std::flush;


	f << "TITLE = \"grid " << gname << "\"\n";
	f << "VARIABLES = ";
	f << "\"X\", \"Y\", \"Z\"" ;
	f << "\n";

	f << "ZONE T=\"grid " << gname << "\", N=" << mpGrd->SizeNodeTab() << ", E=" << mpGrd->SizeCellTab() << ", F=FEPOINT, ET=TETRAHEDRON\n"; 


	MGSize wid = 1;
	vector<MGSize>	tabid( mpGrd->cColNode().size() );

	for ( Grid<DIM_3D>::ColNode::const_iterator itrn = mpGrd->cColNode().begin(); itrn != mpGrd->cColNode().end(); ++itrn)
	{
		tabid[ (*itrn).cId() ] = wid++;

		for ( MGSize k=0; k<DIM_3D; ++k)
			f << setprecision(18) << (*itrn).cPos().cX( k) << " ";
		f << endl;
	}


	for ( Grid<DIM_3D>::ColCell::const_iterator itrc = mpGrd->cColCell().begin(); itrc != mpGrd->cColCell().end(); ++itrc)
	{
		f	<< tabid[ (*itrc).cNodeId(0) ] << " " 
			<< tabid[ (*itrc).cNodeId(1) ] << " " 
			<< tabid[ (*itrc).cNodeId(2) ] << " " 
			<< tabid[ (*itrc).cNodeId(3) ] << endl;
	}

	cout << " - FINISHED\n";
}


template <Dimension DIM>
void WriteGrdTEC<DIM>::WriteCells( const MGString& name, vector<MGSize>& tab, const MGString& zone )
{
	if ( tab.size() == 0 )
	{
		cout << "tab.size() = 0  -- no cells to write" << endl;
		return;
	}

	typedef typename TDefs<DIM>::GCell GCell;
	typedef typename TDefs<DIM>::GVect GVect;

	mFName = name;
	ofstream f( name.c_str() );

	//cout << "\nWriting cell TEC '" << name.c_str() << "'" << std::flush;

	const MGFloat coeff = 1.0;
	
	map<MGSize,MGSize>	mapid;
	map<MGSize,MGSize>::iterator	itr;

	for ( MGSize i=0; i<tab.size(); ++i)
	{
		const GCell& cell = mpGrd->cCell( tab[i]);

		for ( MGSize k=0; k<GCell::SIZE; ++k)
			mapid[ cell.cNodeId( k) ] = 1; 
	}

	MGSize id = 1;
	GVect	vmin, vmax, vn, vc;
	vmin = vmax = mpGrd->cNode( (*mapid.begin()).first ).cPos();

	for ( itr=mapid.begin(); itr!=mapid.end(); ++itr, ++id)
	{
		(*itr).second = id;

		vn = mpGrd->cNode( (*itr).first ).cPos();

		for ( MGSize d=0; d<DIM; ++d)
		{
			if ( vmin.cX(d) > vn.cX(d) )
				vmin.rX(d) = vn.cX(d);

			if ( vmax.cX(d) < vn.cX(d) )
				vmax.rX(d) = vn.cX(d);
		}
	}

	vc = 0.5 * ( vmax + vmin);


	switch ( DIM)
	{
	case DIM_2D :
		{
			f << "VARIABLES = \"X\", \"Y\", \"ID\"\n";
			f << "ZONE T=\"" << zone << "\", N=" << mapid.size() << ", E=" << tab.size() << ", F=FEPOINT, ET=TRIANGLE\n";
			break;
		}
	case DIM_3D :
		{
			f << "VARIABLES = \"X\", \"Y\", \"Z\", \"ID\"\n";
			f << "ZONE T=\"" << zone << "\", N=" << mapid.size() << ", E=" << tab.size() << ", F=FEPOINT, ET=BRICK\n";
			break;
		}
	default :
		THROW_INTERNAL( "unknown DIM inside WriteGrdTEC<DIM>::WriteCells !" );
	}

	for ( itr=mapid.begin(); itr!=mapid.end(); ++itr)
	{
		GVect vct = mpGrd->cNode( (*itr).first ).cPos();
		vn = vct - vc;

		vct = vc + coeff*vn;
		for ( MGSize idim=0; idim<DIM; ++idim)
			f << setprecision(18) << vct.cX(idim) << " ";
		f << (*itr).first << endl;
	}

	MGSize tabcurid[GCell::SIZE];

	for ( MGSize i=0; i<tab.size(); ++i)
	{
		const GCell& cell = mpGrd->cCell( tab[i]);
		for ( MGSize k=0; k<GCell::SIZE; ++k)
		{
			if ( ( itr = mapid.find( cell.cNodeId( k) ) ) != mapid.end() )
			{
				tabcurid[k] = (*itr).second;
			}
			else
			{
				THROW_INTERNAL( "corrupted map of node ids inside WriteExploded(...)");
			}
		}

		switch ( DIM)
		{
		case DIM_2D :
			{
				f << tabcurid[0] << " " << tabcurid[1] << " " << tabcurid[2] << endl;
				break;
			}
		case DIM_3D :
			{
				f	<< tabcurid[0] << " " << tabcurid[1] << " " << tabcurid[2] << " " << tabcurid[2] << " "
					<< tabcurid[3] << " " << tabcurid[3] << " " << tabcurid[3] << " " << tabcurid[3] << endl;
				break;
			}
		default :
			THROW_INTERNAL( "unknown DIM inside WriteGrdTEC<DIM>::WriteCells !" );
		}

	}

	//cout << " - FINISHED\n";
}


template <Dimension DIM>
void WriteGrdTEC<DIM>::WriteEdges( const MGString& name, const vector< Key<2> >& tab, const MGString& zone )
{
	typedef typename TDefs<DIM>::GVect GVect;

	mFName = name;
	ofstream f( name.c_str() );

	cout << "\nWriting cell TEC '" << name.c_str() << "'" << std::flush;

	for ( vector< Key<2> >::const_iterator citr= tab.begin(); citr!=tab.end(); ++citr)
	{
		GVect	v1 = mpGrd->cNode( citr->cFirst() ).cPos();
		GVect	v2 = mpGrd->cNode( citr->cSecond() ).cPos();

		switch ( DIM)
		{
		case DIM_2D :
			{
				f << "VARIABLES = \"X\", \"Y\", \"ID\"" << endl;
				f << "ZONE T=\"" << zone << " [" << citr->cFirst() << "," << citr->cSecond() << "]\"" << endl;
				f << v1.cX() << " " << v1.cY() << " " << citr->cFirst() << endl;
				f << v2.cX() << " " << v2.cY() << " " << citr->cSecond() << endl;
				break;
			}
		case DIM_3D :
			{
				f << "VARIABLES = \"X\", \"Y\", \"Z\", \"ID\"" << endl;
				f << "ZONE T=\"" << zone << " [" << citr->cFirst() << "," << citr->cSecond() << "]\"" << endl;
				f << v1.cX() << " " << v1.cY() << " " << v1.cZ() << " " << citr->cFirst() << endl;
				f << v2.cX() << " " << v2.cY() << " " << v2.cZ() << " " << citr->cSecond() << endl;
				break;
			}
		default :
			THROW_INTERNAL( "unknown DIM inside WriteGrdTEC<DIM>::WriteCells !" );
		}
	}
}

template <Dimension DIM>
void WriteGrdTEC<DIM>::WriteFaces( const MGString& name, const vector< Key<3> >& tab, const MGString& zone )
{
	THROW_INTERNAL( "not implemented");
}

template <>
void WriteGrdTEC<DIM_3D>::WriteFaces( const MGString& name, const vector< Key<3> >& tab, const MGString& zone )
{
	typedef TDefs<DIM_3D>::GVect GVect;

	mFName = name;
	ofstream f( name.c_str() );

	//cout << "\nWriting cell TEC '" << name.c_str() << "'" << std::flush;

	map<MGSize,MGSize>	mapid;
	map<MGSize,MGSize>::iterator	itr;

	for ( MGSize i=0; i<tab.size(); ++i)
		for ( MGSize k=0; k<3; ++k)
			mapid[ tab[i].cElem(k) ] = 1; 

	MGSize id = 1;
	GVect	vmin, vmax, vn, vc;
	vmin = vmax = mpGrd->cNode( (*mapid.begin()).first ).cPos();

	for ( itr=mapid.begin(); itr!=mapid.end(); ++itr, ++id)
	{
		(*itr).second = id;

		vn = mpGrd->cNode( (*itr).first ).cPos();

		for ( MGSize d=0; d<DIM_3D; ++d)
		{
			if ( vmin.cX(d) > vn.cX(d) )
				vmin.rX(d) = vn.cX(d);

			if ( vmax.cX(d) < vn.cX(d) )
				vmax.rX(d) = vn.cX(d);
		}
	}

	vc = 0.5 * ( vmax + vmin);

	f << "VARIABLES = \"X\", \"Y\", \"Z\", \"ID\"\n";
	f << "ZONE T=\"" << zone << "\", N=" << mapid.size() << ", E=" << tab.size() << ", F=FEPOINT, ET=TRIANGLE\n";

	for ( itr=mapid.begin(); itr!=mapid.end(); ++itr)
	{
		GVect vct = mpGrd->cNode( (*itr).first ).cPos();

		//vn = vct - vc;
		//vct = vc + coeff*vn;
		for ( MGSize idim=0; idim<DIM_3D; ++idim)
			f << setprecision(18) << vct.cX(idim) << " ";
		f << (*itr).first << endl;
	}

	for ( MGSize i=0; i<tab.size(); ++i)
	{
		f << mapid[ tab[i].cElem(0) ] << " " << mapid[ tab[i].cElem(1) ] << " " << mapid[ tab[i].cElem(2) ] << endl;
	}
}



template <Dimension DIM>
void WriteGrdTEC<DIM>::WriteExploded( const MGString& name, vector<MGSize>& tab, const MGFloat& coeff )
{
	typedef typename TDefs<DIM>::GCell GCell;
	typedef typename TDefs<DIM>::GVect GVect;

	mFName = name;
	ofstream f( name.c_str() );

	cout << "\nWriting exploded TEC '" << name.c_str() << "'" << std::flush;


	GVect	vmin, vmax, vn, vc;
	vmin = vmax = mpGrd->cNode( mpGrd->cCell( tab[0]).cNodeId( 0) ).cPos();

	for ( MGSize i=0; i<tab.size(); ++i)
	{
		const GCell& cell = mpGrd->cCell( tab[i]);

		for ( MGSize in=0; in<GCell::SIZE; ++in)
		{
			vn = mpGrd->cNode( cell.cNodeId( in) ).cPos();

			for ( MGSize d=0; d<DIM; ++d)
			{
				if ( vmin.cX(d) > vn.cX(d) )
					vmin.rX(d) = vn.cX(d);

				if ( vmax.cX(d) < vn.cX(d) )
					vmax.rX(d) = vn.cX(d);
			}
		}
	}

	vc = 0.5 * ( vmax + vmin);


	//MGSize tabcurid[GCell::SIZE];

	for ( MGSize i=0; i<tab.size(); ++i)
	{
		const GCell& cell = mpGrd->cCell( tab[i]);
		

		switch ( DIM)
		{
		case DIM_2D :
			{
				f << "VARIABLES = \"X\", \"Y\"\n";
				f << "ZONE T=\"cell id=" << tab[i] << "\", N=3, E=1, F=FEPOINT, ET=TRIANGLE\n";
				break;
			}
		case DIM_3D :
			{
				f << "VARIABLES = \"X\", \"Y\", \"Z\"\n";
				f << "ZONE T=\"cell id=" << tab[i] << "\", N=4, E=1, F=FEPOINT, ET=BRICK\n";
				break;
			}
		default :
			THROW_INTERNAL( "unknown DIM inside WriteGrdTEC<DIM>::WriteCells !" );
		}
		THROW_INTERNAL( " CRASH !!!!!!");

		//Geom::Tetrahedron tet = GridGeom::Tet( cell, *mpGrd);
		//GVect	vcc = tet.Center();
		GVect	vcc;
		GVect vd = vcc - vc;

		
		for ( MGSize in=0; in<GCell::SIZE; ++in)
		{
			GVect vct = mpGrd->cNode( cell.cNodeId( in) ).cPos();
			vn = vct - vcc;

			vct = vn + coeff*vd + vc;
			f	<< setprecision(8) << vct.cX() << " "
				<< setprecision(8) << vct.cY() << " "
				<< setprecision(8) << vct.cZ() << endl;
		}

		f << "1 2 3 3 4 4 4 4\n";
	}

	cout << " - FINISHED\n";
}



template class WriteGrdTEC<DIM_2D>;
template class WriteGrdTEC<DIM_3D>;

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

