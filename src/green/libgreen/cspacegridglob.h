#ifndef __CSPACEGRIDCONN_H__
#define __CSPACEGRIDCONN_H__

#include "libcoresystem/mgdecl.h"
#include "libextget/geomentity.h"

#include "libgreen/controlspace.h"
#include "libgreen/grid.h"
#include "libgreen/generatordata.h"
#include "libgreen/kernel.h"
#include "libgreen/gridcontext.h"
#include "libgreen/interpolator.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Dimension DIM>
class BndGrid;


template <Dimension DIM>
class CSpaceMaster;

////\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
//namespace ControlSpaceSpace {
////\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//



//////////////////////////////////////////////////////////////////////
//	class GridGlobControlSpace
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class GridGlobControlSpace : public ControlSpace<DIM>
{
	typedef typename TDefs<DIM>::GVect		GVect;
	typedef typename TDefs<DIM>::GMetric	GMetric;

	typedef typename GMetric::SMtx			SMtx;

	friend class CSpaceMaster<DIM>;

public:
	GridGlobControlSpace() : mInterpol(&mGrid), mBndInterpol(&mBndGrid)		
	{ 
		//mCountLost = mCountHit = mCount = 0;
		//mFile.open( "_lost_points.dat");
	}

	virtual ~GridGlobControlSpace()		
	{ 
		//cout << "mCountLost = " << mCountLost << endl; 
		//cout << "mCountHit = " << mCountHit << endl; 
		//cout << "mCount = " << mCount << endl; 
		//mFile.close();
	}

	virtual GMetric	GetSpacing( const GVect& vct) const;

	GMetric	GetBndSpacing( const GVect& vct) const;

	void	Init( const GridContext<GMetric>& cxmet, const BndGrid<DIM>& bgrid);
	void	InitFileMEANDR( const MGString& fname);
	void	ExportFileMEANDR( const MGString& fname);

	void	InitFileTEC( const MGString& fname);
	void	PostInitTEC();

	void	ApplyScale( const MGFloat& scale);

private:
	Grid<DIM>				mGrid;
	GridContext<SMtx>		mMetricCx;
	Interpolator<DIM>		mInterpol;

	Grid<DIM>				mBndGrid;
	GridContext<SMtx>		mBndMetricCx;
	Interpolator<DIM>		mBndInterpol;


	/////////////////////////
};




////\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
//} // end of namespace ControlSpace
////\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
//
//namespace CSPACE = ControlSpaceSpace;



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __CSPACEGRIDCONN_H__
