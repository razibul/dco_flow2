#include "operator3d_pointface.h"

#include "libgreen/grid.h"
#include "libgreen/kernel.h"
#include "libgreen/gridgeom.h"

#include "libcorecommon/progressbar.h"
#include "libgreen/writegrdtec.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 




void Operator3D_PointFace::Init( const vector<MGSize>& tabcell)
{
	if ( tabcell.size() != 2)
	{
		mErrorCode = 1;
		return;
	}

	mtabOCell = tabcell;

	Grid<DIM_3D>&	grid = mKernel.rGrid();


	const MGSize ic1 = tabcell.front();
	const MGSize ic2 = tabcell.back();
	
	const GCell& cell1 = grid.cCell(ic1);
	const GCell& cell2 = grid.cCell(ic2);

	MGSize	if1, if2;
	bool	bNghb = false;

	// check if they are neighbours
	for ( MGSize i=0; i<GCell::SIZE; ++i)
	{
		if ( cell1.cCellId(i).first == ic2)
		{
			if1 = i;
			if2 = cell1.cCellId(i).second;

			if ( cell2.cCellId( if2 ).first == ic1 && cell2.cCellId( if2 ).second == if1 )
			{
				bNghb = true;
				break;
			}
		}
	}

	if ( ! bNghb)
	{
		TRACE( "Operator3D_Flip23 - cells are not neighbours");
		mErrorCode = 2;
		return;
	}

	Key<3> faceUpLocal( Simplex<DIM_3D>::cFaceConn(if1,0), Simplex<DIM_3D>::cFaceConn(if1,1), Simplex<DIM_3D>::cFaceConn(if1,2) );
	Key<3> faceUp = cell1.FaceKey( if1);

	Key<3> faceLowLocal( Simplex<DIM_3D>::cFaceConn(if2,0), Simplex<DIM_3D>::cFaceConn(if2,1), Simplex<DIM_3D>::cFaceConn(if2,2) );
	Key<3> faceLow = cell2.FaceKey( if2);

	faceUp.Reverse();
	faceUpLocal.Reverse();

	for ( MGSize i=0; i<3; ++i)
	{
		if ( faceUp == faceLow)
			break;

		faceLow.Rotate( 1);
		faceLowLocal.Rotate( 1);
	}

	if ( faceUp != faceLow)
	{
		mErrorCode = 3;
		return;
	}

	mtabCellUp[0] = faceUpLocal.cFirst();
	mtabCellUp[1] = faceUpLocal.cSecond();
	mtabCellUp[2] = faceUpLocal.cThird();
	mtabCellUp[3] = if1;

	mtabCellLow[0] = faceLowLocal.cFirst();
	mtabCellLow[1] = faceLowLocal.cThird();
	mtabCellLow[2] = faceLowLocal.cSecond();
	mtabCellLow[3] = if2;

	// HACK ::
	mPointPos = GVect( 0.0);

	for ( MGSize i=0; i<3; ++i)
		mPointPos += grid.cNode( faceUp.cElem(i) ).cPos();

	mPointPos /= 3.0;
}

void Operator3D_PointFace::Execute( vector<MGSize>& tabcell)
{
	if ( mErrorCode != 0)
		return;

	Grid<DIM_3D>&	grid = mKernel.rGrid();

	const GCell cellUp = grid.cCell( mtabOCell.front() );
	const GCell cellLow = grid.cCell( mtabOCell.back() );

	MGSize idnode = grid.InsertNode( mPointPos);

	GCell	cell;

	vector<GCell> tabcellUp;
	vector<GCell> tabcellLow;

	cell.rNodeId(0) = cellUp.cNodeId( mtabCellUp[0] );
	cell.rNodeId(1) = cellUp.cNodeId( mtabCellUp[1] );
	cell.rNodeId(2) = idnode;
	cell.rNodeId(3) = cellUp.cNodeId( mtabCellUp[3] );
	tabcellUp.push_back( cell);

	cell.rNodeId(0) = cellUp.cNodeId( mtabCellUp[1] );
	cell.rNodeId(1) = cellUp.cNodeId( mtabCellUp[2] );
	cell.rNodeId(2) = idnode;
	cell.rNodeId(3) = cellUp.cNodeId( mtabCellUp[3] );
	tabcellUp.push_back( cell);

	cell.rNodeId(0) = cellUp.cNodeId( mtabCellUp[2] );
	cell.rNodeId(1) = cellUp.cNodeId( mtabCellUp[0] );
	cell.rNodeId(2) = idnode;
	cell.rNodeId(3) = cellUp.cNodeId( mtabCellUp[3] );
	tabcellUp.push_back( cell);


	cell.rNodeId(0) = cellLow.cNodeId( mtabCellLow[2] );
	cell.rNodeId(1) = cellLow.cNodeId( mtabCellLow[0] );
	cell.rNodeId(2) = idnode;
	cell.rNodeId(3) = cellLow.cNodeId( mtabCellLow[3] );
	tabcellLow.push_back( cell);

	cell.rNodeId(0) = cellLow.cNodeId( mtabCellLow[1] );
	cell.rNodeId(1) = cellLow.cNodeId( mtabCellLow[2] );
	cell.rNodeId(2) = idnode;
	cell.rNodeId(3) = cellLow.cNodeId( mtabCellLow[3] );
	tabcellLow.push_back( cell);

	cell.rNodeId(0) = cellLow.cNodeId( mtabCellLow[0] );
	cell.rNodeId(1) = cellLow.cNodeId( mtabCellLow[1] );
	cell.rNodeId(2) = idnode;
	cell.rNodeId(3) = cellLow.cNodeId( mtabCellLow[3] );
	tabcellLow.push_back( cell);

	bool bokvolUp = mKernel.CheckCellVolume( tabcellUp);
	bool bokvolLow = mKernel.CheckCellVolume( tabcellLow);

	if ( !bokvolUp || !bokvolLow )
	{
		THROW_INTERNAL( "Operator3D_PointFace : CRASH !!!" );
	}

	/////////////////////////////////////////////////////////////
	// insert new cells 

	tabcell.clear();

	for ( MGSize i=0; i<tabcellUp.size(); ++i)
	{
		MGSize id = mKernel.InsertCell( tabcellUp[i] );
		tabcell.push_back( id);
	}

	for ( MGSize i=0; i<tabcellLow.size(); ++i)
	{
		MGSize id = mKernel.InsertCell( tabcellLow[i] );
		tabcell.push_back( id);
	}



	if ( tabcell.size() != 6 )
		THROW_INTERNAL( "Operator3D_PointFace : CRASH !!!" );


	/////////////////////////////////////////////////////////////
	// update internal connectivity

	// cell 0  (upper 0)
	mKernel.rGrid().rCell( tabcell[0] ).rCellId(0).first = tabcell[1];	// left
	mKernel.rGrid().rCell( tabcell[0] ).rCellId(0).second = 1;
	mKernel.rGrid().rCell( tabcell[0] ).rCellId(1).first = tabcell[2];	// right
	mKernel.rGrid().rCell( tabcell[0] ).rCellId(1).second = 0;
	mKernel.rGrid().rCell( tabcell[0] ).rCellId(3).first = tabcell[3];	// bottom
	mKernel.rGrid().rCell( tabcell[0] ).rCellId(3).second = 3;

	// cell 1  (upper 1)
	mKernel.rGrid().rCell( tabcell[1] ).rCellId(0).first = tabcell[2];	// left
	mKernel.rGrid().rCell( tabcell[1] ).rCellId(0).second = 1;
	mKernel.rGrid().rCell( tabcell[1] ).rCellId(1).first = tabcell[0];	// right
	mKernel.rGrid().rCell( tabcell[1] ).rCellId(1).second = 0;
	mKernel.rGrid().rCell( tabcell[1] ).rCellId(3).first = tabcell[4];	// bottom
	mKernel.rGrid().rCell( tabcell[1] ).rCellId(3).second = 3;

	// cell 2  (upper 2)
	mKernel.rGrid().rCell( tabcell[2] ).rCellId(0).first = tabcell[0];	// left
	mKernel.rGrid().rCell( tabcell[2] ).rCellId(0).second = 1;
	mKernel.rGrid().rCell( tabcell[2] ).rCellId(1).first = tabcell[1];	// right
	mKernel.rGrid().rCell( tabcell[2] ).rCellId(1).second = 0;
	mKernel.rGrid().rCell( tabcell[2] ).rCellId(3).first = tabcell[5];	// bottom
	mKernel.rGrid().rCell( tabcell[2] ).rCellId(3).second = 3;



	// cell 3  (lower 0)
	mKernel.rGrid().rCell( tabcell[3] ).rCellId(0).first = tabcell[5];	// left
	mKernel.rGrid().rCell( tabcell[3] ).rCellId(0).second = 1;
	mKernel.rGrid().rCell( tabcell[3] ).rCellId(1).first = tabcell[4];	// right
	mKernel.rGrid().rCell( tabcell[3] ).rCellId(1).second = 0;
	mKernel.rGrid().rCell( tabcell[3] ).rCellId(3).first = tabcell[0];	// top
	mKernel.rGrid().rCell( tabcell[3] ).rCellId(3).second = 3;

	// cell 4  (lower 1)
	mKernel.rGrid().rCell( tabcell[4] ).rCellId(0).first = tabcell[3];	// left
	mKernel.rGrid().rCell( tabcell[4] ).rCellId(0).second = 1;
	mKernel.rGrid().rCell( tabcell[4] ).rCellId(1).first = tabcell[5];	// right
	mKernel.rGrid().rCell( tabcell[4] ).rCellId(1).second = 0;
	mKernel.rGrid().rCell( tabcell[4] ).rCellId(3).first = tabcell[1];	// top
	mKernel.rGrid().rCell( tabcell[4] ).rCellId(3).second = 3;

	// cell 5  (lower 2)
	mKernel.rGrid().rCell( tabcell[5] ).rCellId(0).first = tabcell[4];	// left
	mKernel.rGrid().rCell( tabcell[5] ).rCellId(0).second = 1;
	mKernel.rGrid().rCell( tabcell[5] ).rCellId(1).first = tabcell[3];	// right
	mKernel.rGrid().rCell( tabcell[5] ).rCellId(1).second = 0;
	mKernel.rGrid().rCell( tabcell[5] ).rCellId(3).first = tabcell[2];	// top
	mKernel.rGrid().rCell( tabcell[5] ).rCellId(3).second = 3;

	/////////////////////////////////////////////////////////////
	// update external connectivity

	grid.rCell( tabcell[0] ).rCellId(2) = cellUp.cCellId( mtabCellUp[2] );
	grid.rCell( tabcell[1] ).rCellId(2) = cellUp.cCellId( mtabCellUp[0] );
	grid.rCell( tabcell[2] ).rCellId(2) = cellUp.cCellId( mtabCellUp[1] );

	grid.rCell( tabcell[3] ).rCellId(2) = cellLow.cCellId( mtabCellLow[1] );
	grid.rCell( tabcell[4] ).rCellId(2) = cellLow.cCellId( mtabCellLow[0] );
	grid.rCell( tabcell[5] ).rCellId(2) = cellLow.cCellId( mtabCellLow[2] );


	grid.rCell( grid.cCell( tabcell[0] ).cCellId(2).first ).rCellId( grid.cCell( tabcell[0] ).cCellId(2).second ).first = grid.cCell( tabcell[0] ).cId();
	grid.rCell( grid.cCell( tabcell[0] ).cCellId(2).first ).rCellId( grid.cCell( tabcell[0] ).cCellId(2).second ).second = 2;

	grid.rCell( grid.cCell( tabcell[1] ).cCellId(2).first ).rCellId( grid.cCell( tabcell[1] ).cCellId(2).second ).first = grid.cCell( tabcell[1] ).cId();
	grid.rCell( grid.cCell( tabcell[1] ).cCellId(2).first ).rCellId( grid.cCell( tabcell[1] ).cCellId(2).second ).second = 2;

	grid.rCell( grid.cCell( tabcell[2] ).cCellId(2).first ).rCellId( grid.cCell( tabcell[2] ).cCellId(2).second ).first = grid.cCell( tabcell[2] ).cId();
	grid.rCell( grid.cCell( tabcell[2] ).cCellId(2).first ).rCellId( grid.cCell( tabcell[2] ).cCellId(2).second ).second = 2;


	grid.rCell( grid.cCell( tabcell[3] ).cCellId(2).first ).rCellId( grid.cCell( tabcell[3] ).cCellId(2).second ).first = grid.cCell( tabcell[3] ).cId();
	grid.rCell( grid.cCell( tabcell[3] ).cCellId(2).first ).rCellId( grid.cCell( tabcell[3] ).cCellId(2).second ).second = 2;

	grid.rCell( grid.cCell( tabcell[4] ).cCellId(2).first ).rCellId( grid.cCell( tabcell[4] ).cCellId(2).second ).first = grid.cCell( tabcell[4] ).cId();
	grid.rCell( grid.cCell( tabcell[4] ).cCellId(2).first ).rCellId( grid.cCell( tabcell[4] ).cCellId(2).second ).second = 2;

	grid.rCell( grid.cCell( tabcell[5] ).cCellId(2).first ).rCellId( grid.cCell( tabcell[5] ).cCellId(2).second ).first = grid.cCell( tabcell[5] ).cId();
	grid.rCell( grid.cCell( tabcell[5] ).cCellId(2).first ).rCellId( grid.cCell( tabcell[5] ).cCellId(2).second ).second = 2;


	/////////////////////////////////////////////////////////////
	// remove old cells
	// remove old cell from the tree

	for ( MGSize ic=0; ic<mtabOCell.size(); ++ic)
		mKernel.EraseCell( mtabOCell[ic] );

}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

