#include "grid.h"
#include "libcoreio/store.h"
#include "libcoregeom/geom.h"
#include "libgreen/gridgeom.h"

#include "libgreen/writegrdtec.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 





template <>
void Grid<DIM_3D>::FindEdges( vector< Key<2> >& tabedg) const
{
	typedef vector< Key<2> >	TKeyTab;

	TKeyTab	tabtmp;

	tabedg.clear();

	for ( ColCell::const_iterator itr = mtabCell.begin(); itr != mtabCell.end(); ++itr)
	{
		for ( MGSize k=0; k<GCell::ESIZE; ++k)
		{
			Key<2> key = (*itr).EdgeKey( k);
			key.Sort();
			tabtmp.push_back( key);
		}
	}

	sort( tabtmp.begin(), tabtmp.end() );
	TKeyTab::iterator itrend = unique( tabtmp.begin(), tabtmp.end() );
	tabtmp.erase( itrend, tabtmp.end() );

	tabedg.resize( tabtmp.size() );
	copy( tabtmp.begin(), tabtmp.end(), tabedg.begin() );
}

template <>
void Grid<DIM_3D>::FindFaces( vector< Key<3> >& tabfac) const
{
	typedef vector< Key<3> >	TKeyTab;

	TKeyTab	tabtmp;

	tabfac.clear();

	for ( ColCell::const_iterator itr = mtabCell.begin(); itr != mtabCell.end(); ++itr)
	{
		for ( MGSize k=0; k<4; ++k)
		{
			Key<3> key = (*itr).FaceKey( k);
			key.Sort();
			tabtmp.push_back( key);
		}
	}

	sort( tabtmp.begin(), tabtmp.end() );
	TKeyTab::iterator itrend = unique( tabtmp.begin(), tabtmp.end() );
	tabtmp.erase( itrend, tabtmp.end() );

	tabfac.resize( tabtmp.size() );
	copy( tabtmp.begin(), tabtmp.end(), tabfac.begin() );
}


template <>
void Grid<DIM_3D>::FindNullFaces( vector< Key<DIM_3D> >& tabfac) const
{
	typedef vector< Key<3> >	TKeyTab;

	TKeyTab	tabtmp;

	tabfac.clear();

	for ( ColCell::const_iterator itr = mtabCell.begin(); itr != mtabCell.end(); ++itr)
	{
		for ( MGSize k=0; k<4; ++k)
		{
			if ( itr->cCellId(k).first == 0 )
			{
				Key<3> key = (*itr).FaceKey( k);
				key.Sort();
				tabtmp.push_back( key);
			}
		}
	}

	sort( tabtmp.begin(), tabtmp.end() );
	tabtmp.erase( unique( tabtmp.begin(), tabtmp.end() ), tabtmp.end() );

	tabfac.resize( tabtmp.size() );
	copy( tabtmp.begin(), tabtmp.end(), tabfac.begin() );
}


template <>
void Grid<DIM_3D>::FindInternalEdges( vector< Key<2> >& tabfac) const
{
	typedef vector< Key<2> >	TKeyTab;

	TKeyTab	tabtmp;

	tabfac.clear();

	for ( ColCell::const_iterator itr = mtabCell.begin(); itr != mtabCell.end(); ++itr)
	{
		if ( (*itr).IsExternal() )
			continue;

		for ( MGSize k=0; k<GCell::SIZE; ++k)
		{
			if ( mtabCell[ (*itr).cCellId( k).first ].IsExternal() )
				continue;

			Key<2> key;

			key = Key<2>( itr->cFaceNodeId( k, 0), itr->cFaceNodeId( k, 1) );
			key.Sort();
			tabtmp.push_back( key);

			key = Key<2>( itr->cFaceNodeId( k, 1), itr->cFaceNodeId( k, 2) );
			key.Sort();
			tabtmp.push_back( key);

			key = Key<2>( itr->cFaceNodeId( k, 2), itr->cFaceNodeId( k, 0) );
			key.Sort();
			tabtmp.push_back( key);
		}
	}

	sort( tabtmp.begin(), tabtmp.end() );
	TKeyTab::iterator itrend = unique( tabtmp.begin(), tabtmp.end() );
	tabtmp.erase( itrend, tabtmp.end() );

	tabfac.resize( tabtmp.size() );
	copy( tabtmp.begin(), tabtmp.end(), tabfac.begin() );
}



template <>
void Grid<DIM_3D>::UpdateNodeCellIds()
{
	for ( Grid<DIM_3D>::ColCell::const_iterator itrc = mtabCell.begin(); itrc != mtabCell.end(); ++itrc)
		for ( MGSize k=0; k<GCell::SIZE; ++k)
			rNode( itrc->cNodeId(k) ).rCellId() = itrc->cId();
}


template <>
void Grid<DIM_3D>::UpdateNodeCellIds( const vector<MGSize>& tabcell)
{
	for ( MGSize ic=0; ic<tabcell.size(); ++ic)
	{
		const GCell& cell = cCell( tabcell[ic] );

		for ( MGSize k=0; k<GCell::SIZE; ++k)
			rNode( cell.cNodeId(k) ).rCellId() = cell.cId();
	}
}



template <>
void Grid<DIM_3D>::Dump( const MGString& fname) const
{
	ofstream f( fname.c_str() );

	for ( ColCell::const_iterator itr = mtabCell.begin(); itr != mtabCell.end(); ++itr)
	{
		const GCell &c = *itr;

		c.Dump( *this, f);
	}
}


template <>
void Grid<DIM_3D>::CheckConnectivity() const
{
	//cout << "CheckConnectivity\n";
	
	//FILE *f = gtrace.Open();
	ofstream f( "_trace.txt" );
	
	MGFloat	volume = 0.0;
	bool	bOk = true;

	//printf( "CellEnd() = %d\n", CellEnd() );
	for ( ColCell::const_iterator itr = mtabCell.begin(); itr != mtabCell.end(); ++itr)
	{
		const GCell &cell = *itr;
		MGFloat cvol = GridGeom<DIM_3D>::CreateSimplex( cell, *this).Volume();

		volume += cvol;

		if ( cell.cId() != itr.index() )
			f << "bad cell id\n";

		for ( MGSize in=0; in<GCell::SIZE; ++in)
		{
			if ( ! mtabNode.is_valid( cell.cNodeId(in) ) )
			{
				bOk = false;
				f << "\nconnectivity error 0 - node ids are corrupted\n";
				f << "node_id = " << cell.cNodeId(in) << endl;
				cell.Dump( *this, f);
			}
		}


		for ( MGSize ifc=0; ifc<GCell::SIZE; ++ifc)
		{
			MGSize idfc = cell.cCellId( ifc).first;
			Key<3> key1 = cell.FaceKey( ifc);
			key1.Sort();

			if ( idfc)
			{
				const GCell &neicell = cCell(idfc);
				Key<3> key2 = neicell.FaceKey( cell.cCellId( ifc).second);
				key2.Sort();

				if ( ! mtabCell.is_valid( idfc ) )
				{
					bOk = false;
					f << "\nconnectivity error 0 - cell neighbour is invalid\n";
					f << "cell_id = " << idfc << endl;
					cell.Dump( *this, f);
				}

				if ( neicell.cCellId( cell.cCellId( ifc).second).first != cell.cId() )
				{
					bOk = false;
					f << "\nconnectivity error 1 - cells ids are corrupted\n";
					f << "cell_face_id = " << cell.cCellId( ifc).second << endl;
					cell.Dump( *this, f);
					neicell.Dump( *this, f);
					//Dump( "_grd_conn.dump" );
				}
				
				if ( ! (key1 == key2) )
				{
					bOk = false;
					f << "\nconnectivity error 2 - face nodes are different\n";
					f << "cell_face_id = " << cell.cCellId( ifc).second << endl;
					cell.Dump( *this, f);
					neicell.Dump( *this, f);
					//Dump( "_grd_conn.dump" );
				}
				

				if ( neicell.cCellId( cell.cCellId( ifc).second).second != ifc )
				{
					bOk = false;
					f << "connectivity error 3\n";
				}

			}
		}
	}

	MGFloat boxvolume = Geom::VolumeHEX( cNode(1).cPos(), cNode(2).cPos(), cNode(3).cPos(), cNode(4).cPos(),
										 cNode(5).cPos(), cNode(6).cPos(), cNode(7).cPos(), cNode(8).cPos() );

	f << "sum of the cell volumes = " << setprecision(12) << volume << endl;
	f << "box volume = " << setprecision(12) << boxvolume << endl;
	
	
	if ( ! bOk)
		THROW_INTERNAL( "Grid<DIM_3D>::CheckConnectivity() -- CRASH !!!");

	return;

	////////
	//vector<MGSize> tabb;
	//for ( ColCell::const_iterator itr = mtabCell.begin(); itr != mtabCell.end(); ++itr)
	//{
	//	const GCell &cell = *itr;

	//	for ( MGSize in=0; in<GCell::SIZE; ++in)
	//	{
	//		if ( cell.cNodeId(in) == 1405 )
	//			tabb.push_back( itr->cId() );
	//	}
	//}

	//cout << "tabb.size() = " << tabb.size() << endl; 

	//WriteGrdTEC<DIM_3D> writeTEC( this );
	//writeTEC.WriteCells( "__1405.dat", tabb );


	//tabb.clear();
	//tabb.push_back( 3215);
	//writeTEC.WriteCells( "__3215.dat", tabb );


}


template <> 
void Grid<DIM_3D>::CheckQuality() const
{
	const MGFloat treshBad = 5.0;

	MGFloat avAlpha = 0.0;
	MGFloat avBeta = 0.0;
	
	pair<MGFloat,MGFloat> limAlpha;
	pair<MGFloat,MGFloat> limBeta;
	pair<MGFloat,MGFloat> limAngle;

	vector<MGFloat> tabscale;
	const MGSize ns = 40;
	MGFloat xmax = ::log( 1.0e10);
	MGFloat dx = xmax / (MGFloat)(ns);

	for ( MGSize i=0; i<ns; ++i)
		tabscale.push_back( ::exp( (MGFloat)i * dx) );


	vector<MGSize> tabhist;
	tabhist.resize( tabscale.size(), 0);

	vector<MGSize> tabBad;


	bool bFirst = true;

	for ( ColCell::const_iterator itr = mtabCell.begin(); itr != mtabCell.end(); ++itr)
	{
		const GCell &cell = *itr;
		Simplex<DIM_3D> simp = GridGeom<DIM_3D>::CreateSimplex( cell, *this);

		MGFloat vol = simp.Volume();

		MGFloat a = simp.QMeasureALPHA();
		MGFloat b = simp.QMeasureBETAmin();


		MGFloat amin = simp.QMeasureMinAngle();
		MGFloat amax = simp.QMeasureMaxAngle();

		if ( vol <= 0.0)
		{
			cout << "volume problem -  id = " << itr->cId() << vol << " alpha = " << a << " beta = " << b << endl;
			cout << " angle " << amin << " " << amax << endl;
		}

		if ( b > treshBad)
			tabBad.push_back( itr->cId() );

		if ( bFirst)
		{
			limAlpha.first = limAlpha.second = a;
			limBeta.first = limBeta.second = b;
			limAngle.first = amin;
			limAngle.second = amax;
			bFirst = false;
		}

		if ( a > limAlpha.second)
			limAlpha.second = a;

		if ( a < limAlpha.first)
			limAlpha.first = a;

		if ( b > limBeta.second)
			limBeta.second = b;

		if ( b < limBeta.first)
			limBeta.first = b;

		if ( amin < limAngle.first)
			limAngle.first = amin;

		if ( amax > limAngle.second)
			limAngle.second = amax;

		avAlpha += a;
		avBeta += b;


		MGSize k;
		for ( k=0; k<tabscale.size(); ++k)
			if ( b < tabscale[k] )
				break;

		if ( k<tabscale.size() )
			++( tabhist[k] );
		else
			++( tabhist[tabscale.size()-1] );

	}

	cout << setprecision(16);
	cout << "ALPHA = " << limAlpha.first << " - " << limAlpha.second << endl;
	cout << "BETA  = " << limBeta.first << " - " << limBeta.second << endl;

	cout << "Min Angle  = " << limAngle.first * 180/M_PI << endl;
	cout << "Max Angle  = " << limAngle.second * 180/M_PI << endl;

	cout << "average ALPHA = " << avAlpha / mtabCell.size_valid() << endl;
	cout << "average BETA = " << avBeta / mtabCell.size_valid() << endl;


	ofstream of("_histogram.dat");
	for ( MGSize i=0; i<tabscale.size(); ++i)
		of  << tabscale[i] << " " << tabhist[i] << endl;

	of.close();

	cout << "no of bad cells (BETA > " << treshBad << ") = " << tabBad.size() << endl;
	if ( tabBad.size() > 0 )
	{
		WriteGrdTEC<DIM_3D>	wtec( this);
		wtec.WriteCells( "_bad_cells.dat", tabBad );
	}

}


template <>
void Grid<DIM_3D>::RebuildConnectivity()
{
	//cout << "Grid<DIM_2D>::RebuildConnectivity()" << endl;

	//typedef pair< Key<DIM_2D>, GNeDef > FaceInfo;
	//vector< FaceInfo >	tabface;

	//tabface.reserve( (DIM_2D+1)*SizeCellTab() );

	//for ( ColCell::const_iterator itr = mtabCell.begin(); itr != mtabCell.end(); ++itr)
	//{
	//	for ( MGSize k=0; k<GCell::SIZE; ++k)
	//	{
	//		Key<DIM_2D> key = (*itr).FaceKey( k);
	//		key.Sort();

	//		GNeDef	ndef( itr->cId(), k);
	//		tabface.push_back( FaceInfo( key, ndef) );
	//	}
	//}

	//sort( tabface.begin(), tabface.end() );
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

