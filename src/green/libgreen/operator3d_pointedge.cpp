#include "operator3d_pointedge.h"

#include "libgreen/grid.h"
#include "libgreen/kernel.h"
#include "libgreen/gridgeom.h"

#include "libcorecommon/progressbar.h"
#include "libgreen/writegrdtec.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 





void Operator3D_PointEdge::Init( const vector<MGSize>& tabcell, const GVect& pos)
{
	if ( tabcell.size() < 3)
	{
		mErrorCode = 1;
		return;
	}

	mtabOCell = tabcell;

	Grid<DIM_3D>&	grid = mKernel.rGrid();


	/////////////////////////////////////////////////////////////
	// trying to find the common edge
	vector<MGSize> tab;

	const GCell &fstcell = grid.cCell( mtabOCell[0] );

	for ( MGSize i=0; i<GCell::SIZE; ++i)
	{
		MGSize in = fstcell.cNodeId( i);
		MGSize count = 1;

		for ( MGSize ic=1; ic<mtabOCell.size(); ++ic)
		{
			const GCell &celln = grid.cCell( mtabOCell[ic] );
		
			for ( MGSize k=0; k<GCell::SIZE; ++k)
				if ( celln.cNodeId(k) == in)
					++count;
		}

		if ( count == mtabOCell.size() )
			tab.push_back( in);
	}

	if ( tab.size() != 2)
	{
		mErrorCode = 2;
		return;
	}

	mEdge.rFirst()  = tab[0];
	mEdge.rSecond() = tab[1];

	/////////////////////////////////////////////////////////////
	// trying to find the equator
	vector< Key<2> > tabedge;

	for ( MGSize ic=0; ic<mtabOCell.size(); ++ic)
	{
		const GCell &cell = grid.cCell( mtabOCell[ic] );

		for ( MGSize ie=0; ie<GCell::ESIZE; ++ie)
		{
			Key<2> edge = cell.EdgeKey( ie);
			if ( edge.cFirst() != mEdge.cFirst() && edge.cFirst() != mEdge.cSecond() &&
				 edge.cSecond() != mEdge.cFirst() && edge.cSecond() != mEdge.cSecond() )
				 tabedge.push_back( edge);
		}
	}

	if ( tabedge.size() != mtabOCell.size() )
	{
		mErrorCode = 3;
		return;
	}


	/////////////////////////////////////////////////////////////
	// check orientation of the equator
	for ( MGSize ie=0; ie<tabedge.size(); ++ie)
	{
		Key<2>	&edge = tabedge[ie];

		Simplex<DIM_3D> tet( grid.cNode( mEdge.cFirst() ).cPos(),
							 grid.cNode( edge.cFirst() ).cPos(),
							 grid.cNode( edge.cSecond() ).cPos(),
							 grid.cNode( mEdge.cSecond() ).cPos() );

		if ( tet.Volume() < 0.0)
		{
			const MGSize itmp = edge.cFirst();
			edge.rFirst() = edge.cSecond();
			edge.rSecond() = itmp;
		}
	}

	/////////////////////////////////////////////////////////////
	// sort the equator

	map< MGSize, Key<2> > mapeq;
	map< MGSize, Key<2> >::iterator itreq;
	for ( MGSize ie=0; ie<tabedge.size(); ++ie)
		mapeq.insert( map< MGSize, Key<2> >::value_type( tabedge[ie].cFirst(), tabedge[ie] ) );

	tabedge.clear();
	Key<2>	edge = mapeq.begin()->second;

	tabedge.push_back( edge);

	MGSize idbegin = edge.cFirst();

	do
	{
		itreq = mapeq.find( edge.cSecond() );

		if ( itreq == mapeq.end() )
			THROW_INTERNAL( "CRASH :: Operator3D_PointEdge : nonconsistent equator !!!");

		edge = itreq->second;
		tabedge.push_back( edge);
	}
	while ( edge.cSecond() != idbegin && tabedge.size() <= mtabOCell.size() );




	/////////////////////////////////////////////////////////////
	// check the equator is closed
	bool bClosed;

	bClosed = ( tabedge[tabedge.size()-1].cSecond() == tabedge[0].cFirst() );

	for ( MGSize ie=1; ie<tabedge.size(); ++ie)
		if ( tabedge[ie-1].cSecond() != tabedge[ie].cFirst() )
		{
			bClosed = false;
			break;
		}

	if ( ! bClosed )
			THROW_INTERNAL( "CRASH :: Operator3D_PointEdge : nonconsistent equator !!!");


	for ( MGSize ie=0; ie<tabedge.size(); ++ie)
		mtabENode.push_back( tabedge[ie].cSecond() );

	mPointPos = pos;	// TODO :: check the point lies on the edge !!!

	//// HACK ::
	//mPointPos = 0.5 * ( grid.cNode( mEdge.cFirst() ).cPos() + grid.cNode( mEdge.cSecond() ).cPos() );

}


void Operator3D_PointEdge::BuildCells( vector<GCell>& tabcell)
{
	Grid<DIM_3D>&	grid = mKernel.rGrid();


	tabcell.clear();

	mIdNode = grid.InsertNode( mPointPos);

	GCell cell;

	/////////////////////////////////////////////////////////////
	// upper cells

	for ( MGSize i=1; i<mtabENode.size(); ++i)
	{
		cell.rNodeId(0) = mtabENode[i-1];
		cell.rNodeId(1) = mtabENode[i];
		cell.rNodeId(2) = mIdNode;
		cell.rNodeId(3) = mEdge.cSecond();
		tabcell.push_back( cell);
	}

	cell.rNodeId(0) = mtabENode[mtabENode.size()-1];
	cell.rNodeId(1) = mtabENode[0];
	cell.rNodeId(2) = mIdNode;
	cell.rNodeId(3) = mEdge.cSecond();
	tabcell.push_back( cell);


	/////////////////////////////////////////////////////////////
	// lower cells

	for ( MGSize i=1; i<mtabENode.size(); ++i)
	{
		cell.rNodeId(0) = mtabENode[i];
		cell.rNodeId(1) = mtabENode[i-1];
		cell.rNodeId(2) = mIdNode;
		cell.rNodeId(3) = mEdge.cFirst();
		tabcell.push_back( cell);
	}

	cell.rNodeId(0) = mtabENode[0];
	cell.rNodeId(1) = mtabENode[mtabENode.size()-1];
	cell.rNodeId(2) = mIdNode;
	cell.rNodeId(3) = mEdge.cFirst();
	tabcell.push_back( cell);

}


void Operator3D_PointEdge::Execute( vector<MGSize>& tabcell)
{
	if ( mErrorCode != 0)
		return;


	BuildCells( mtabNCell);

	bool bokvol = mKernel.CheckCellVolume( mtabNCell);

	if ( !bokvol )
	{
		THROW_INTERNAL( "Operator3D_PointEdge : (negative volume) CRASH !!!" );
	}


	const MGSize n = mtabENode.size();

	ASSERT( mtabNCell.size() == 2*n );

	tabcell.clear();

	/////////////////////////////////////////////////////////////
	// insert new cells 
	for ( MGSize i=0; i<mtabNCell.size(); ++i)
	{
		//MGSize id = mKernel.rGrid().InsertCell( mtabNCell[i] );
		MGSize id = mKernel.InsertCell( mtabNCell[i] );
		tabcell.push_back( id);
	}

	/////////////////////////////////////////////////////////////
	// update internal connectivity

	for ( MGSize i=0; i<n; ++i)
	{
		mKernel.rGrid().rCell( tabcell[i] ).rCellId( 3 ).first = tabcell[i+n];
		mKernel.rGrid().rCell( tabcell[i] ).rCellId( 3 ).second = 3;

		mKernel.rGrid().rCell( tabcell[i+n] ).rCellId( 3 ).first = tabcell[i];
		mKernel.rGrid().rCell( tabcell[i+n] ).rCellId( 3 ).second = 3;
	}

	// upper band
	for ( MGSize i=1; i<n; ++i)
	{
		mKernel.rGrid().rCell( tabcell[i] ).rCellId( 1 ).first = tabcell[i-1];
		mKernel.rGrid().rCell( tabcell[i] ).rCellId( 1 ).second = 0;

		mKernel.rGrid().rCell( tabcell[i-1] ).rCellId( 0 ).first = tabcell[i];
		mKernel.rGrid().rCell( tabcell[i-1] ).rCellId( 0 ).second = 1;
	}

	mKernel.rGrid().rCell( tabcell[0] ).rCellId( 1 ).first = tabcell[n-1];
	mKernel.rGrid().rCell( tabcell[0] ).rCellId( 1 ).second = 0;

	mKernel.rGrid().rCell( tabcell[n-1] ).rCellId( 0 ).first = tabcell[0];
	mKernel.rGrid().rCell( tabcell[n-1] ).rCellId( 0 ).second = 1;


	// lower band
	for ( MGSize i=1; i<n; ++i)
	{
		mKernel.rGrid().rCell( tabcell[n+i] ).rCellId( 0 ).first = tabcell[n+i-1];
		mKernel.rGrid().rCell( tabcell[n+i] ).rCellId( 0 ).second = 1;

		mKernel.rGrid().rCell( tabcell[n+i-1] ).rCellId( 1 ).first = tabcell[n+i];
		mKernel.rGrid().rCell( tabcell[n+i-1] ).rCellId( 1 ).second = 0;
	}

	mKernel.rGrid().rCell( tabcell[n] ).rCellId( 0 ).first = tabcell[n+n-1];
	mKernel.rGrid().rCell( tabcell[n] ).rCellId( 0 ).second = 1;

	mKernel.rGrid().rCell( tabcell[n+n-1] ).rCellId( 1 ).first = tabcell[n];
	mKernel.rGrid().rCell( tabcell[n+n-1] ).rCellId( 1 ).second = 0;



	/////////////////////////////////////////////////////////////
	// update external connectivity

	typedef map< Key<3>, GNeDef > FaceMap;	
	FaceMap mapface;
	FaceMap::iterator itrmf;

	for ( MGSize i=0; i<mtabOCell.size(); ++i)
	{
		const GCell &cell = mKernel.rGrid().cCell( mtabOCell[i]);

		for ( MGSize ifac=0; ifac<GCell::SIZE; ++ifac)
		{
			Key<3> key = cell.FaceKey( ifac);
			key.Sort();
			mapface.insert( FaceMap::value_type( key, cell.cCellId( ifac) ) );
		}

	}

	for ( MGSize i=0; i<tabcell.size(); ++i)
	{
		GCell &cell = mKernel.rGrid().rCell( tabcell[i]);


		Key<3> keyup = cell.FaceKey( 2);
		keyup.Sort();

		if ( (itrmf = mapface.find( keyup)) == mapface.end() )
		{
			THROW_INTERNAL( "Operator3D_PointEdge - corrupted data !!!");
		}

		cell.rCellId(2) = itrmf->second;

		mKernel.rGrid().rCell( cell.cCellId(2).first ).rCellId( cell.cCellId(2).second ).first = cell.cId();
		mKernel.rGrid().rCell( cell.cCellId(2).first ).rCellId( cell.cCellId(2).second ).second = 2;

	}


	/////////////////////////////////////////////////////////////
	// remove old cells
	// remove old cell from the tree

	for ( MGSize ic=0; ic<mtabOCell.size(); ++ic)
		mKernel.EraseCell( mtabOCell[ic] );


}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

