#include "cspaceio.h"
#include "libcoreio/store.h"
#include "libgreen/cspaceconst.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 



template<>
MGString CSpaceIO<DIM_2D>::mstVersionString = CSP_VER_STRING;

template<>
MGString CSpaceIO<DIM_3D>::mstVersionString = CSP_VER_STRING;

template<>
MGString CSpaceIO<DIM_2D>::mstVersionNumber = CSP_VER_NUMBER;

template<>
MGString CSpaceIO<DIM_3D>::mstVersionNumber = CSP_VER_NUMBER;





template <Dimension DIM>
CSpaceIO<DIM>::CSpaceIO( CSpaceMaster<DIM>& src) : mpSrc( &src)
{
    mRex.InitPattern( "^#([0-9]*)=([_A-Z0-9]*)\\((.*)\\);$");
}



template <Dimension DIM>
bool CSpaceIO<DIM>::CheckVersion_ascii( istream& file)
{
	istringstream is;
	MGString	sname, sver;

	IO::ReadLine( file, is);
	is >> sname >> sver;

	if ( sname != VerStr() != 0 || sver != VerNum() != 0 )
		return false;

	return true;
}



template <Dimension DIM>
void CSpaceIO<DIM>::ReadDataSection( istream& file, const MGString& key, void (CSpaceIO<DIM>::*addFun)( const MGString&) )
{
	MGString	buf, skey;

	IO::ReadLineC( file, buf);
	if ( buf != key + ";" )
	{
		buf = MGString( "expected : '") + key + MGString( "'; ");
		THROW_FILE( buf.c_str(), "" );
	}

	skey = MGString(CSP_END) + "_" + key;
	while ( IO::ReadLineC( file, buf) )
	{
		//cout << buf << endl;
		if ( buf == skey + ";" )
			return;

		(this->* addFun)( buf);
	}

	buf = MGString( "expected : '") + skey + MGString( "'; ");
	THROW_FILE( buf.c_str(), "" );
}



//////////////////////////////////////////////////////////////////////
//	HEADER

template <Dimension DIM>
void CSpaceIO<DIM>::ReadHeader( istream& file)
{
	MGString	buf, skey;

	// HEADER
	IO::ReadLineC( file, buf);
	if ( buf != MGString(CSP_HEADER) + ";" )
	{
		buf = MGString( "expected : '") + MGString( CSP_HEADER) + MGString( "'; ");
		THROW_FILE( buf.c_str(), "" );
	}

	// DIMENSION
	IO::ReadLineC( file, buf);
	IO::Strip( buf, ";");

	Dimension dim = ::StrToDim( buf);
	if ( DIM != dim)
	{
		buf = MGString( "incompatible CSp dimension : '") + MGString( buf) + MGString( "'; ");
		THROW_FILE( buf.c_str(), "" );
	}

	// NAME
	IO::ReadLineC( file, buf);
	IO::Strip( buf, ";");
	mpSrc->mName = buf;

	// DESCRIPTION
	IO::ReadLineC( file, buf);
	IO::Strip( buf, ";");
	mpSrc->mDescription = buf;

	// END
	skey = MGString(CSP_END) + "_" + MGString(CSP_HEADER);
	IO::ReadLineC( file, buf);
	if ( buf != skey + ";" )
	{
		buf = MGString( "expected : '") + skey + MGString( "'; ");
		THROW_FILE( buf.c_str(), "" );
	}

}


template <Dimension DIM>
void CSpaceIO<DIM>::ReadGlobalSection( istream& file)
{
	MGString	buf, skey;

	// GLOBAL
	IO::ReadLineC( file, buf);
	if ( buf != MGString(CSP_GLOBAL) + ";" )
	{
		buf = MGString( "expected : '") + MGString( CSP_GLOBAL) + MGString( "'; ");
		THROW_FILE( buf.c_str(), "" );
	}

	// TYPE
	IO::ReadLineC( file, buf);
	//IO::Strip( buf, ";");

	RegExp		rex;
    rex.InitPattern( "^([_A-Z0-9]*)\\((.*)\\);$");
	rex.SetString( buf);

	if ( rex.IsOk() )
	{
		ASSERT( rex.GetNoSubString() == 2);
		int k = rex.GetNoSubString();
		MGString skey = rex.GetSubString( 1);
		MGString sparams = rex.GetSubString( 2);

		if ( skey == MGString(CSP_GLOB_USERDEF) )
		{
			// do nothing here
		}
		else
		if ( skey == MGString(CSP_GLOB_UNIFORM) )
		{
			MGFloat h;
			istringstream is( sparams );
			is >> h;
			mpSrc->InitGlobalCSpUniform( h);
		}
		else
		if ( skey == MGString(CSP_GLOB_FILE) )
		{
			rex.InitPattern( "^\"(.*)\"$");
			rex.SetString( sparams);
			if ( rex.IsOk() )
			{
				MGString sname = rex.GetSubString( 1);
				mpSrc->InitGlobalCSpFileMEANDROS( sname);
			}
			else
				THROW_FILE( "global section: FILE parameter not recognized", sparams);
		}
		else
		if ( skey == MGString(CSP_GLOB_FILESCALE) )
		{
			rex.InitPattern( "^\"(.*)\",(.*)$");
			rex.SetString( sparams);
			if ( rex.IsOk() )
			{
				MGString sname = rex.GetSubString( 1);
				MGFloat scale;
				istringstream is( rex.GetSubString( 2) );
				is >> scale;

				mpSrc->InitGlobalCSpFileMEANDROS( sname, scale);
			}
			else
				THROW_FILE( "global section: FILE parameter not recognized", sparams);
		}
		else
		if ( skey == MGString(CSP_GLOB_TECFILESCALE) )
		{
			rex.InitPattern( "^\"(.*)\",(.*)$");
			rex.SetString( sparams);
			if ( rex.IsOk() )
			{
				MGString sname = rex.GetSubString( 1);
				MGFloat scale;
				istringstream is( rex.GetSubString( 2) );
				is >> scale;

				mpSrc->InitGlobalCSpFileTEC( sname, scale);
			}
			else
				THROW_FILE( "global section: FILE parameter not recognized", sparams);
		}
		else
		{
			ostringstream os;
			os << "unknow global definition : '" << skey << "'";
			THROW_INTERNAL( os.str() );
		}

	}
	else
		THROW_FILE( "global section parsing error", buf);


	// END
	skey = MGString(CSP_END) + "_" + MGString(CSP_GLOBAL);
	IO::ReadLineC( file, buf);
	if ( buf != skey + ";" )
	{
		buf = MGString( "expected : '") + skey + MGString( "'; ");
		THROW_FILE( buf.c_str(), "" );
	}

}




//////////////////////////////////////////////////////////////////////
//	SOURCE

template <Dimension DIM>
void CSpaceIO<DIM>::ParseDef( MGSize& id, MGString& str1, MGString& str2, const MGString& def)
{
	MGString	strtmp;

	mRex.SetString( def);

	if ( mRex.IsOk() )
	{
		ASSERT( mRex.GetNoSubString() == 3);
		int k = mRex.GetNoSubString();
		strtmp = mRex.GetSubString( 1);
		str1 = mRex.GetSubString( 2);
		str2 = mRex.GetSubString( 3);

		//sscanf( strtmp.c_str(), "%d", &id);
		istringstream is( strtmp );
		is >> id;
	}
	else
		THROW_FILE( "parsing error", def);
}



template <Dimension DIM>
template <Dimension TDIM>
void CSpaceIO<DIM>::AddSourceEnt( const MGString& def)
{
	MGSize		id;
	MGString	str1, str2;

	ParseDef( id, str1, str2, def);

	Source<TDIM,DIM>	sent;

	sent.Init( str1, str2);
	MGSize getid = mpSrc->mSourceOwner.PushBack( sent);

	if ( getid != id)
	{
		ostringstream os;
		os << "bad id in the definition : '" << def << "' - should be:" << getid ;
		THROW_INTERNAL( os.str() );
	}
}


template <>
void CSpaceIO<DIM_2D>::ReadSourceData( istream& file)
{
	ReadDataSection( file, CSP_SEC_VERTEXSRC, &CSpaceIO<DIM_2D>::AddSourceEnt<DIM_0D> );
	//ReadDataSection( file, CSP_SEC_EDGESRC, &CSpaceIO<DIM_2D>::AddSourceEnt<DIM_1D> );
	//ReadDataSection( file, CSP_SEC_FACESRC, &CSpaceIO<DIM_2D>::AddSourceEnt<DIM_2D> );
}

template <>
void CSpaceIO<DIM_3D>::ReadSourceData( istream& file)
{
	ReadDataSection( file, CSP_SEC_VERTEXSRC, &CSpaceIO<DIM_3D>::AddSourceEnt<DIM_0D> );
	//ReadDataSection( file, CSP_SEC_EDGESRC, &CSpaceIO<DIM_3D>::AddSourceEnt<DIM_1D> );
	//ReadDataSection( file, CSP_SEC_FACESRC, &CSpaceIO<DIM_3D>::AddSourceEnt<DIM_2D> );
	//ReadDataSection( file, CSP_SEC_BREPSRC, &CSpaceIO<DIM_3D>::AddSourceEnt<DIM_3D> );
}




template <Dimension DIM>
void CSpaceIO<DIM>::DoRead( const MGString& fname)
{
	ifstream file;
	istringstream is;

	MGString	skey, sname, sver;

	file.open( fname.c_str(), ios::in );
	try
	{
		if ( ! file)
			THROW_FILE( "can not open the file", fname);

		cout << "reading CSp file : '" << fname << "'\n";

		if ( ! CheckVersion_ascii( file) )
		{
			ostringstream os;
			os << "incompatible CSp file version (current ver: '" << VerStr() << " " << VerNum() << "')";

			THROW_INTERNAL( os.str() );
		}

		ReadHeader( file);
		ReadGlobalSection( file);

		if ( ! mpSrc->GetGlobalCSpace() )
			ReadSourceData( file);

		cout << "end of reading CSp file : '" << fname << "'\n";

	}
	catch ( EHandler::Except& e)
	{
		cout << "ERROR: CSpaceIO<DIM>::DoRead - file name: " << fname << endl << endl;
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);

		THROW_INTERNAL("END");
	}
}


template <Dimension DIM>
void CSpaceIO<DIM>::DoWrite( const MGString& fname)
{
}



template class CSpaceIO<DIM_2D>;
template class CSpaceIO<DIM_3D>;

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

