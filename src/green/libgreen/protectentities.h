#ifndef __PROTECTENTITIES_H__
#define __PROTECTENTITIES_H__


#include "libcoresystem/mgdecl.h"
#include "libgreen/tdefs.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


//////////////////////////////////////////////////////////////////////
// class ProtectEntities
//////////////////////////////////////////////////////////////////////
class ProtectEntities
{
public:
	ProtectEntities()	{}

	void	AddEdge( const Key<2>& edge )			{ msetProEdges.insert( edge);}
	void	AddFace( const Key<3>& face )			{ msetProFaces.insert( face);}

	bool	IsEdgeProtected( const Key<2>& edge ) const;
	bool	IsFaceProtected( const Key<3>& face ) const;

	void	EraseEdge( const Key<2>& edge );
	void	EraseFace( const Key<3>& face );

	const set< Key<2> >&	cSetProEdges() const	{ return msetProEdges; }
	const set< Key<3> >&	cSetProFaces() const	{ return msetProFaces; }

private:
	set< Key<2> >					msetProEdges;
	set< Key<3> >					msetProFaces;
};


inline bool ProtectEntities::IsEdgeProtected( const Key<2>& edge ) const
{
	return msetProEdges.find( edge) != msetProEdges.end();
}

inline bool ProtectEntities::IsFaceProtected( const Key<3>& face ) const
{
	return msetProFaces.find( face) != msetProFaces.end();
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __PROTECTENTITIES_H__
