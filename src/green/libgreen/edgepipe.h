#ifndef __EDGEPIPE_H__
#define __EDGEPIPE_H__

#include "libcoresystem/mgdecl.h"
#include "libgreen/tdefs.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Dimension DIM> 
class Kernel;


//////////////////////////////////////////////////////////////////////
// class EdgePipe
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class EdgePipe
{
	typedef typename TDefs<DIM>::GVect		GVect;
	typedef typename TDefs<DIM>::GMetric	GMetric;

	typedef typename TDefs<DIM>::GNode		GNode;
	typedef typename TDefs<DIM>::GCell		GCell;
	typedef typename TDefs<DIM>::GFace		GFace;

public:
	typedef vector< pair<MGSize,MGSize> >	TCollCellNei;
	
	
	
	EdgePipe( Kernel<DIM_3D>& kernel) : mpKernel(&kernel)		{ Reset();}
	
	bool	CreatePipe( const Key<2>& edge);

	void	Reset()	{ mtabCell.clear(); mtabA.clear(); mtabB.clear(); }
	
	MGSize	Size() const	{ return mtabCell.size();}
	

	bool	Recover();
	bool	RecoverSplit( vector< Key<2> >& tabsedge);
	bool	RecoverSteiner();

	MGSize	SplitEdge();

protected:	
	
	bool	RecoverSimple();
	bool	RecoverGeneral( vector< Key<2> >& tabe);
	
	bool	ReduceSPipe(vector<MGSize>& tab, const vector< Key<2> >& tabe);

	void	FindCrossEdges( vector< Key<2> >& tabedg);
	MGSize	NextCell( const MGSize& iccur, const MGSize& icprev);
	MGSize	NextCell( const MGSize& iccur, const MGSize& icprev, const MGSize& inode);
	
	void	Export( const MGString& name);

private:
	Kernel<DIM_3D>	*mpKernel;

	Key<2>			mEdge;
	
	vector<MGSize>	mtabCell;	// cells creating the pipe
	TCollCellNei	mtabA;		// cells conected to frist node of the edge
	TCollCellNei	mtabB;		// cells conected to second node of the edge
};
//////////////////////////////////////////////////////////////////////


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __EDGEPIPE_H__
