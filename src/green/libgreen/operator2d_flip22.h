#ifndef __OPERATOR2D_FLIP22_H__
#define __OPERATOR2D_FLIP22_H__


#include "libcoresystem/mgdecl.h"
#include "libgreen/tdefs.h"
#include "libgreen/grdcell.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

template <Dimension DIM> 
class Grid;


//////////////////////////////////////////////////////////////////////
// class Operator2D_Flip22
//////////////////////////////////////////////////////////////////////
class Operator2D_Flip22
{
	typedef TDefs<DIM_2D>::GVect	GVect;
	typedef TDefs<DIM_2D>::GMetric	GMetric;

	typedef TDefs<DIM_2D>::GNode	GNode;
	typedef TDefs<DIM_2D>::GCell	GCell;
	typedef TDefs<DIM_2D>::GFace	GFace;

public:
	Operator2D_Flip22( Grid<DIM_2D>& grd) : mpGrd(&grd), mbReady(false)	{}

	bool	Check( const MGSize& ic1, const MGSize& ic2);
	bool	Execute( const MGSize& ic1, const MGSize& ic2);
	void	Execute();

private:
	Grid<DIM_2D>	*mpGrd;

	bool			mbReady;
	GCell			mCellUp;
	GCell			mCellLo;
};
//////////////////////////////////////////////////////////////////////


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

#endif // __OPERATOR2D_FLIP22_H__
