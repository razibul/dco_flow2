#include "libcoresystem/mgdecl.h"

#include "libcoreio/readmsh2.h"
#include "libcoreio/writemsh2.h"
#include "libcoreio/writetec.h" 


#include "gridsmoother.h"
#include "optimizer.h"


//////////////////////////////////////////////////////////////////////
int main( int argc, char* argv[])
{
	try
	{
		GREEN::GridSmoother<DIM_3D> smooth3D;

		smooth3D.InitFileMSH2( "out.msh2" );
		//smooth3D.InitFileMSH2( "_final_.msh2" );
		//smooth3D.InitFileMSH2( "grid.msh2" );
		smooth3D.PostInit();

		return 0;

		//---------------------------------------------------------------------
	
		//IO::ReadMSH2	readMSH2( grid);
		//readMSH2.DoRead( "grid.msh2", false);

		//IO::WriteTEC	writeTEC( grid);
		//writeTEC.DoWrite( "out.dat");

		//---------------------------------------------------------------------

		GREEN::GridSmoother<DIM_2D> smooth;

		smooth.InitFileMSH2( "grid.msh2" );
		smooth.PostInit();

		smooth.WriteGridTEC( "_smth_out.dat");

		smooth.Execute();

		smooth.WriteGridTEC( "_smth_out_after.dat");

		//---------------------------------------------------------------------
		//FunctionalTest func;
		//Optimizer opt( &func);
		//opt.DoOptimize();

		return 0;

	}
	catch ( EHandler::Except& e)
	{
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);
	}
	catch( exception &e) 
	{
		TRACE( e.what() );
		cout << endl << "System exception: " << e.what() << endl;
	}
	catch( ...) 
	{
		TRACE( "Unknown exception thrown" );
		cout << endl << "Unknown exception thrown" << endl;
	}

	return 0;
} 
 