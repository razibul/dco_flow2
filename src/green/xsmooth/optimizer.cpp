#include "optimizer.h"



void Optimizer::DoOptimize()
{
	MGSize n = 100;
    MGFloat fx;
    vector<MGFloat> tabx(n,0.0);

    // Initialize the variables.
    for ( MGSize i = 0; i < n; ++i) 
	{
        tabx[i] = -1.5;
    }

    /*
        Start the L-BFGS optimization; this will invoke the callback functions
        evaluate() and progress() when necessary.
        */
    int ret = lbfgs( n, &tabx[0], &fx, _evaluate, _progress, this, NULL);

    /* Report the result. */
    printf("L-BFGS optimization terminated with status code = %d\n", ret);
    printf("  fx = %f, x[0] = %f, x[1] = %f\n", fx, tabx[0], tabx[1]);
        
}

void Optimizer::DoOptimize( vector<MGFloat>& tabx  )
{
	MGSize n = tabx.size();
    MGFloat fx;

	lbfgs_parameter_t param;

	lbfgs_parameter_init( &param);

	param.max_iterations = 200;
	//param.ftol = 1.0e-6;
	//param.delta = 1.0e-1;
	//param.min_step = 1.0e-10;
	param.ftol = 0.4;
	param.xtol = 1.0e-6;
	param.linesearch = LBFGS_LINESEARCH_BACKTRACKING;
	param.gtol = 1.0e-30;

    /*
        Start the L-BFGS optimization; this will invoke the callback functions
        evaluate() and progress() when necessary.
        */
    int ret = lbfgs( n, &tabx[0], &fx, _evaluate, _progress, this, &param);

    /* Report the result. */
    printf("L-BFGS optimization terminated with status code = %d\n", ret);
    printf("  fx = %f, x[0] = %f, x[1] = %f\n", fx, tabx[0], tabx[1]);
       
}

