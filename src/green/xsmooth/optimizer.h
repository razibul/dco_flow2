#ifndef __OPTIMIZER_H__
#define __OPTIMIZER_H__

#include "libcoresystem/mgdecl.h"
#include "liblbfgs/lbfgs.h"

#include "functionalbase.h"




class Optimizer
{
public:

	Optimizer( FunctionalBase* pf) : mpFunc(pf)	{}
	void	DoOptimize();
	void	DoOptimize( vector<MGFloat>& tabx );


private:
	
    static lbfgsfloatval_t _evaluate(
        void *instance,
        const lbfgsfloatval_t *x,
        lbfgsfloatval_t *g,
        const int n,
        const lbfgsfloatval_t step
        )
    {
        return reinterpret_cast<Optimizer*>(instance)->evaluate(x, g, n, step);
    }

    lbfgsfloatval_t evaluate(
        const lbfgsfloatval_t *x,
        lbfgsfloatval_t *g,
        const int n,
        const lbfgsfloatval_t step
        )
    {
        lbfgsfloatval_t fx = 0.0;
		mpFunc->Evaluate( &fx, g, x, n);

        return fx;
    }



    static int _progress( void *instance, const lbfgsfloatval_t *x, const lbfgsfloatval_t *g, const lbfgsfloatval_t fx,
        const lbfgsfloatval_t xnorm, const lbfgsfloatval_t gnorm, const lbfgsfloatval_t step, int n, int k, int ls  )
    {
        return reinterpret_cast<Optimizer*>(instance)->progress(x, g, fx, xnorm, gnorm, step, n, k, ls);
    }

    int progress(
        const lbfgsfloatval_t *x,
        const lbfgsfloatval_t *g,
        const lbfgsfloatval_t fx,
        const lbfgsfloatval_t xnorm,
        const lbfgsfloatval_t gnorm,
        const lbfgsfloatval_t step,
        int n,
        int k,
        int ls
        )
    {
        printf("Iteration %d:  fx = %f\n", k, fx);

        //printf("Iteration %d:\n", k);
        //printf("  fx = %f, x[0] = %f, x[1] = %f\n", fx, x[0], x[1]);
        //printf("  xnorm = %f, gnorm = %f, step = %f\n", xnorm, gnorm, step);
        //printf("\n");
		return 0;
    }

private:
	FunctionalBase*	mpFunc;
};





#endif // __OPTIMIZER_H__
