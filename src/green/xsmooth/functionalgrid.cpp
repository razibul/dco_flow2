#include "functionalgrid.h"

//#include "libhfgeom/hf_typedef.h"
//#include "libhfgeom/hf_simplex.h"
//#include "libcorecommon/array.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GREEN {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 


template <Dimension DIM>
class QualitySimplexVolume
{
public:
	typedef typename GREEN::TDefs<DIM>::GVect	GVect;

	//QualitySimplex( const GVect& v1, const GVect& v2, const GVect& v3 ) : mV1(v1), mV2(v2), mV3(v3)	{}
	QualitySimplexVolume( const GVect* ptab ) : mpTab(ptab) 	{}

	void	Evaluate( MGFloat& val, GVect* tabgrad )
	{
		val = mpTab[0].cX() * mpTab[1].cY()  +  mpTab[1].cX() * mpTab[2].cY()  +  mpTab[2].cX() * mpTab[0].cY()
			- mpTab[0].cX() * mpTab[2].cY()  -  mpTab[1].cX() * mpTab[0].cY()  -  mpTab[2].cX() * mpTab[1].cY();

		val = fabs( val);
		//val *= 0.5;

		tabgrad[0].rX() =   mpTab[1].cY() - mpTab[2].cY();
		tabgrad[0].rY() = - mpTab[1].cX() + mpTab[2].cX();

		tabgrad[1].rX() =   mpTab[2].cY() - mpTab[0].cY();
		tabgrad[1].rY() = - mpTab[2].cX() + mpTab[0].cX();

		tabgrad[2].rX() =   mpTab[0].cY() - mpTab[1].cY();
		tabgrad[2].rY() = - mpTab[0].cX() + mpTab[1].cX();
	}

private:
	//GVect	mV1, mV2, mV3;
	const GVect*	mpTab;
};



template <Dimension DIM>
class QualitySimplex
{
public:
	typedef typename GREEN::TDefs<DIM>::GVect	GVect;

	//QualitySimplex( const GVect& v1, const GVect& v2, const GVect& v3 ) : mV1(v1), mV2(v2), mV3(v3)	{}
	QualitySimplex( const GVect* ptab ) : mpTab(ptab) 	{}

	//MGFloat		Value();

	void	Evaluate( MGFloat& val, GVect* tabgrad )
	{
		const MGFloat& x0 = mpTab[0].cX();
		const MGFloat& y0 = mpTab[0].cY();
		const MGFloat& x1 = mpTab[1].cX();
		const MGFloat& y1 = mpTab[1].cY();
		const MGFloat& x2 = mpTab[2].cX();
		const MGFloat& y2 = mpTab[2].cY();

		MGFloat volume = fabs(x0*y1 - x0*y2 - x1*y0 + x1*y2 + x2*y0 - x2*y1);
		//volume *= 0.5;

		MGFloat b0 = (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2);	//(mpTab[1] - mpTab[2]) * (mpTab[1] - mpTab[2]);
		MGFloat b1 = (x2-x0)*(x2-x0) + (y2-y0)*(y2-y0);	//(mpTab[2] - mpTab[0]) * (mpTab[2] - mpTab[0]);
		MGFloat b2 = (x0-x1)*(x0-x1) + (y0-y1)*(y0-y1);	//(mpTab[0] - mpTab[1]) * (mpTab[0] - mpTab[1]);

		MGFloat brms = ( b0 + b1 + b2);

		val = 1.5 * volume / brms;
		//val = 1.5 * volume*volume / brms;


		tabgrad[0].rX() = 1.5 * ( ( y1-y2) - 2.*(2.*x0-x2-x1) * (volume / brms) ) / brms;
		tabgrad[0].rY() = 1.5 * ( (-x1+x2) - 2.*(2.*y0-y2-y1) * (volume / brms) ) / brms;

		tabgrad[1].rX() = 1.5 * ( ( y2-y0) - 2.*(2.*x1-x2-x0) * (volume / brms) ) / brms;
		tabgrad[1].rY() = 1.5 * ( (-x2+x0) - 2.*(2.*y1-y2-y0) * (volume / brms) ) / brms;

		tabgrad[2].rX() = 1.5 * ( ( y0-y1) - 2.*(2.*x2-x1-x0) * (volume / brms) ) / brms;
		tabgrad[2].rY() = 1.5 * ( (-x0+x1) - 2.*(2.*y2-y1-y0) * (volume / brms) ) / brms;

		//tabgrad[0].rX() = 1.5 * volume * ( 2*( y1-y2) - 2*(2*x0-x2-x1) * (volume / brms) ) / brms;
		//tabgrad[0].rY() = 1.5 * volume * ( 2*(-x1+x2) - 2*(2*y0-y2-y1) * (volume / brms) ) / brms;

		//tabgrad[1].rX() = 1.5 * volume * ( 2*( y2-y0) - 2*(2*x1-x2-x0) * (volume / brms) ) / brms;
		//tabgrad[1].rY() = 1.5 * volume * ( 2*(-x2+x0) - 2*(2*y1-y2-y0) * (volume / brms) ) / brms;

		//tabgrad[2].rX() = 1.5 * volume * ( 2*( y0-y1) - 2*(2*x2-x1-x0) * (volume / brms) ) / brms;
		//tabgrad[2].rY() = 1.5 * volume * ( 2*(-x0+x1) - 2*(2*y2-y1-y0) * (volume / brms) ) / brms;
	}


private:
	//GVect	mV1, mV2, mV3;
	const GVect*	mpTab;
};





template <Dimension DIM>
class FuncQualitySimplex
{
public:
	typedef typename GREEN::TDefs<DIM>::GVect	GVect;

	//QualitySimplex( const GVect& v1, const GVect& v2, const GVect& v3 ) : mV1(v1), mV2(v2), mV3(v3)	{}
	FuncQualitySimplex( const GVect* ptab ) : mpTab(ptab) 	{}

	//MGFloat		Value();

	void	Evaluate( MGFloat& val )
	{
		const MGFloat& x0 = mpTab[0].cX();
		const MGFloat& y0 = mpTab[0].cY();
		const MGFloat& x1 = mpTab[1].cX();
		const MGFloat& y1 = mpTab[1].cY();
		const MGFloat& x2 = mpTab[2].cX();
		const MGFloat& y2 = mpTab[2].cY();

		MGFloat volume = fabs(x0*y1 - x0*y2 - x1*y0 + x1*y2 + x2*y0 - x2*y1);
		//volume *= 0.5;

		MGFloat b0 = (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2);	//(mpTab[1] - mpTab[2]) * (mpTab[1] - mpTab[2]);
		MGFloat b1 = (x2-x0)*(x2-x0) + (y2-y0)*(y2-y0);	//(mpTab[2] - mpTab[0]) * (mpTab[2] - mpTab[0]);
		MGFloat b2 = (x0-x1)*(x0-x1) + (y0-y1)*(y0-y1);	//(mpTab[0] - mpTab[1]) * (mpTab[0] - mpTab[1]);

		MGFloat brms = ( b0 + b1 + b2);

		val = 1.5 * volume / brms;

		//val = 0.5* volume / pow( b0*b1*b2, 1./3.) ;
		//val = 0.5* volume / sqrt( b0*b1*b2) ;
	}


private:
	const GVect*	mpTab;
};


template <Dimension DIM>
class FuncInterpolSimplex
{
public:
	typedef typename GREEN::TDefs<DIM>::GVect	GVect;

	//QualitySimplex( const GVect& v1, const GVect& v2, const GVect& v3 ) : mV1(v1), mV2(v2), mV3(v3)	{}
	FuncInterpolSimplex( const GVect* ptab ) : mpTab(ptab) 	{}

	//MGFloat		Value();

	void	Evaluate( MGFloat& val )
	{
		const MGFloat& x0 = mpTab[0].cX();
		const MGFloat& y0 = mpTab[0].cY();
		const MGFloat& x1 = mpTab[1].cX();
		const MGFloat& y1 = mpTab[1].cY();
		const MGFloat& x2 = mpTab[2].cX();
		const MGFloat& y2 = mpTab[2].cY();

		MGFloat volume = fabs(x0*y1 - x0*y2 - x1*y0 + x1*y2 + x2*y0 - x2*y1);
		//volume *= 0.5;

		MGFloat b0 = (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2);	//(mpTab[1] - mpTab[2]) * (mpTab[1] - mpTab[2]);
		MGFloat b1 = (x2-x0)*(x2-x0) + (y2-y0)*(y2-y0);	//(mpTab[2] - mpTab[0]) * (mpTab[2] - mpTab[0]);
		MGFloat b2 = (x0-x1)*(x0-x1) + (y0-y1)*(y0-y1);	//(mpTab[0] - mpTab[1]) * (mpTab[0] - mpTab[1]);

		MGFloat brms = ( b0 + b1 + b2);

		//val = 1.5 * volume / brms;

		//val = 0.5* volume / pow( b0*b1*b2, 1./3.) ;
		val = 0.5* volume / sqrt( b0*b1*b2) ;
	}


private:
	const GVect*	mpTab;
};





template <Dimension DIM>
class FuncUntangling
{
public:
	typedef typename GREEN::TDefs<DIM>::GVect	GVect;

	FuncUntangling( const GVect* ptab, const MGFloat& beta = 0.1 ) : mBeta(beta), mpTab(ptab) 	{}

	//MGFloat		Value();

	void	Evaluate( MGFloat& val )
	{
		const MGFloat& x0 = mpTab[0].cX();
		const MGFloat& y0 = mpTab[0].cY();
		const MGFloat& x1 = mpTab[1].cX();
		const MGFloat& y1 = mpTab[1].cY();
		const MGFloat& x2 = mpTab[2].cX();
		const MGFloat& y2 = mpTab[2].cY();

		MGFloat volume = fabs(x0*y1 - x0*y2 - x1*y0 + x1*y2 + x2*y0 - x2*y1);
		//volume *= 0.5;

		//val = 0.5* ( fabs( volume - mBeta) - ( volume - mBeta) );

		//val = 0.5* ( sqrt( volume*volume + 4.*mBeta*mBeta) - volume );
		val = 0.5* ( ( volume*volume + 4.*mBeta*mBeta) - volume*volume ) - 0.01;
		//val =  ( ( volume*volume*volume + 4.*mBeta*mBeta) - volume*volume*volume );

		//val = exp( -volume);

	}


private:
	const MGFloat	mBeta;
	const GVect*	mpTab;
};



template <Dimension DIM, class TFunc>
class FuncNumerical
{
public:
	typedef typename GREEN::TDefs<DIM>::GVect	GVect;

	//QualitySimplex( const GVect& v1, const GVect& v2, const GVect& v3 ) : mV1(v1), mV2(v2), mV3(v3)	{}
	FuncNumerical( const GVect* ptab ) 	
	{
		for ( MGSize in=0; in<=DIM; ++in)
			mTab[in] = ptab[in];
	}

	//MGFloat		Value();

	void	Evaluate( MGFloat& val, GVect* tabgrad )
	{
		TFunc	mFunc( mTab);

		const MGFloat eps = 1.0e-6;

		mFunc.Evaluate( val);

		for ( MGSize in=0; in<=DIM; ++in)
		{
			for ( MGSize idim=0; idim<DIM; ++idim)
			{
				MGFloat fA, fB;

				mTab[in].rX(idim) -= eps;
				mFunc.Evaluate( fA);

				mTab[in].rX(idim) += 2*eps;
				mFunc.Evaluate( fB);

				mTab[in].rX(idim) -= eps;

				tabgrad[in].rX(idim) = 0.5* (fB - fA) / eps;
			}
		}
	}

private:
	GVect	mTab[DIM+1];
};




template <Dimension DIM>
void FunctionalGrid<DIM>::Init( const vector<MGSize>& tabbnd)
{
	mtabMap.resize( mGrid.SizeNodeTab(), 0);

	MGSize idglob=0, idloc=0;
	for ( typename Grid<DIM>::ColNode::const_iterator itr = mGrid.cColNode().begin(); itr != mGrid.cColNode().end(); ++itr)
	{
		++idglob;

		if ( itr->cId() != idglob )
			THROW_INTERNAL( "FunctionalGrid<"<<DIM<<">::Init :: node id problem");

		if ( binary_search( tabbnd.begin(), tabbnd.end(), idglob ) )
			continue;

		mtabMap[ idglob - 1 ] = ++idloc;
	}

	mN = idloc;

	if ( mN + tabbnd.size() != mGrid.SizeNodeTab() )
		THROW_INTERNAL( "FunctionalGrid<"<<DIM<<">::Init :: inconsistent sizes");

	//cout << "n var = " << mN << endl;
	//cout << "n grid = " << mGrid.SizeNodeTab() << endl;
	//cout << "n bndgrid = " << tabbnd.size() << endl;

	//cout << endl;
	//for ( MGSize i=0; i<mtabMap.size(); ++i)
	//	cout << " mtabMap["<<i<<"] = " << mtabMap[i] << endl;

	//cout << endl;

}



template <Dimension DIM>
void FunctionalGrid<DIM>::SetupParams( vector<MGFloat>& tabx, vector<MGFloat>& tabgrad)
{
	tabx.resize( mN*DIM, 0.0);
	tabgrad.resize( mN*DIM, 0.0);

	for ( MGSize i=0; i<mtabMap.size(); ++i)
	{
		if ( mtabMap[i] != 0 )
		{
			MGSize idloc = mtabMap[i] - 1;
			GVect vct = mGrid.cNode( i+1 ).cPos();

			for ( MGSize idim=0; idim<DIM; ++idim)
				tabx[ idloc*DIM + idim] = vct.cX(idim);
		}
	}


	//cout << endl;
	//for ( MGSize i=0; i<tabx.size(); ++i)
	//	cout << " x["<<i<<"] = " << tabx[i] << endl;
	//cout << endl;

}



template <Dimension DIM>
void FunctionalGrid<DIM>::Evaluate( MGFloat* val,  MGFloat* tabgrad, const MGFloat* tabx, const MGSize& n)
{
	Reset( val, tabgrad, n);
	
	
	//EvaluateSUM< SubFuncInvP2, QualitySimplex<DIM> >( val,  tabgrad, tabx, n);
	
	EvaluateSUM< SubFuncInvP3, FuncNumerical< DIM, FuncQualitySimplex<DIM> > >( val,  tabgrad, tabx, n);
	EvaluateSUM< SubFuncInvP2, FuncNumerical< DIM, FuncInterpolSimplex<DIM> > >( val,  tabgrad, tabx, n);
	EvaluateSUM< SubFuncInvP1, FuncNumerical< DIM, FuncUntangling<DIM> > >( val,  tabgrad, tabx, n);
	return;

	//EvaluateEXP< FuncNumerical< DIM, FuncUntangling<DIM> > >( val,  tabgrad, tabx, n);


	// multi exp 
	const MGFloat sc = 1.0e6;

	vector<MGFloat> tabg1(n,0.0);
	vector<MGFloat> tabg2(n,0.0);

	MGFloat f1=0.0, f2=0.0;

	EvaluateSUM< SubFuncInvP2, FuncNumerical< DIM, FuncQualitySimplex<DIM> > >( &f1,  &tabg1[0], tabx, n);
	EvaluateSUM< SubFuncInvP1, FuncNumerical< DIM, FuncUntangling<DIM> > >( &f2,  &tabg2[0], tabx, n);
	//EvaluateSUM< SubFuncInvP1, QualitySimplexVolume<DIM> >( &f2,  &tabg2[0], tabx, n);

	MGFloat sume = exp(f1/sc) + exp(f2/sc);

	*val += sc * log( sume);

	for ( MGSize i=0; i<n; ++i)
		tabgrad[i] +=  ( tabg1[i] * exp(f1/sc)  +  tabg2[i] * exp(f2/sc) ) / sume;
}


template <Dimension DIM>
void FunctionalGrid<DIM>::Reset(  MGFloat* val,  MGFloat* tabgrad, const MGSize& n)
{
	*val = 0.0;
	for ( MGSize i=0; i<n; ++i)
		tabgrad[i] = 0.0;
}


template <Dimension DIM>
template <class TSubFunc, class TFunc>
void FunctionalGrid<DIM>::EvaluateSUM( MGFloat* val,  MGFloat* tabgrad, const MGFloat* tabx, const MGSize& n)
{
	typedef typename GREEN::TDefs<DIM>::GVect	GVect;
	typedef typename GREEN::TDefs<DIM>::GCell	GCell;
	GVect tab[DIM+1];

	for ( typename Grid<DIM>::ColCell::const_iterator itr = mGrid.cColCell().begin(); itr != mGrid.cColCell().end(); ++itr)
	{
		const GCell& cell = *itr;

		for ( MGSize in=0; in<=DIM; ++in)
		{
			MGSize idnod = itr->cNodeId(in);
			if ( mtabMap[ idnod - 1 ] == 0 )
			{
				tab[in] = mGrid.cNode( idnod ).cPos();
			}
			else
			{
				MGSize idloc = mtabMap[ idnod - 1 ] - 1;
				for ( MGSize idim=0; idim<DIM; ++idim)
					tab[in].rX(idim) = tabx[ idloc*DIM + idim ];
			}
		}

		GVect tabg1[DIM+1];
		MGFloat q1;

		TFunc qtet( tab);
		qtet.Evaluate( q1, tabg1);

		MGFloat f1 = TSubFunc::Eval( q1);

		*val += f1;

		//*val += f1 + f2;;


		for ( MGSize in=0; in<=DIM; ++in)
		{
			if ( mtabMap[itr->cNodeId(in) - 1] != 0 )
			{
				MGSize idloc = mtabMap[itr->cNodeId(in) - 1] - 1;

				for ( MGSize idim=0; idim<DIM; ++idim)
					tabgrad[ idloc*DIM + idim ] += TSubFunc::EvalDiff( q1) * tabg1[in].cX(idim);
			}
		}

	}

	//cout << " v = " << *val << endl;


	//cout << endl;
	//for ( MGSize i=0; i<n; ++i)
	//	cout << " x["<<i<<"] = " << tabx[i] << endl;

	//cout << endl;
	//for ( MGSize i=0; i<n; ++i)
	//	cout << " grad["<<i<<"] = " << tabgrad[i] << endl;

}



template <Dimension DIM>
template <class TSubFunc, class TFunc>
void FunctionalGrid<DIM>::EvaluateEXP( MGFloat* val,  MGFloat* tabgrad, const MGFloat* tabx, const MGSize& n)
{
	typedef typename GREEN::TDefs<DIM>::GVect	GVect;
	typedef typename GREEN::TDefs<DIM>::GCell	GCell;
	GVect tab[DIM+1];

	const MGFloat c = 1.0e4;
	MGFloat sume = 0.0;

	for ( typename Grid<DIM>::ColCell::const_iterator itr = mGrid.cColCell().begin(); itr != mGrid.cColCell().end(); ++itr)
	{
		const GCell& cell = *itr;

		for ( MGSize in=0; in<=DIM; ++in)
		{
			MGSize idnod = itr->cNodeId(in);
			if ( mtabMap[ idnod - 1 ] == 0 )
			{
				tab[in] = mGrid.cNode( idnod ).cPos();
			}
			else
			{
				MGSize idloc = mtabMap[ idnod - 1 ] - 1;
				for ( MGSize idim=0; idim<DIM; ++idim)
					tab[in].rX(idim) = tabx[ idloc*DIM + idim ];
			}
		}

		GVect tabg1[DIM+1];
		MGFloat q1;

		TFunc qtet( tab);
		qtet.Evaluate( q1, tabg1);

		MGFloat f1 = TSubFunc::Eval( q1) ;

		sume += exp( f1 / c);

		//*val += f1 + f2;;


		for ( MGSize in=0; in<=DIM; ++in)
		{
			if ( mtabMap[itr->cNodeId(in) - 1] != 0 )
			{
				MGSize idloc = mtabMap[itr->cNodeId(in) - 1] - 1;

				for ( MGSize idim=0; idim<DIM; ++idim)
					tabgrad[ idloc*DIM + idim ] += TSubFunc::EvalDiff( q1)  * tabg1[in].cX(idim) * exp( f1 / c) ;
			}
		}

	}

	*val = c * log( sume);
	for ( MGSize i=0; i<n; ++i)
		tabgrad[i] /= sume;


	//cout << " v = " << *val << endl;


	//cout << endl;
	//for ( MGSize i=0; i<n; ++i)
	//	cout << " x["<<i<<"] = " << tabx[i] << endl;

	//cout << endl;
	//for ( MGSize i=0; i<n; ++i)
	//	cout << " grad["<<i<<"] = " << tabgrad[i] << endl;

}


template class FunctionalGrid<DIM_2D>;
template class FunctionalGrid<DIM_3D>;

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GREEN
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\// 

