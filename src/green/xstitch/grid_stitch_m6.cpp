
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <map>
#include <string>
#include <vector>
//#include <regex>

#include "libcorecommon/key.h"
#include "libcorecommon/progressbar.h"
#include "libcoregeom/simplex.h"
#include "libcoregeom/vect.h"
#include "libcoregeom/dimension.h"
#include "libcorecommon/regexp.h"


typedef Key<2> Key2;
typedef Key<3> Key3;

using namespace std;

#ifdef WIN32
using namespace std::tr1;
#endif


const int edgeidsKey[6][2] = {	{ 0, 1},
								{ 1, 2},
								{ 2, 0},
								{ 0, 3},
								{ 1, 3},
								{ 2, 3} } ;
class Vect3D
{
public:
	Vect3D() : mX(0.), mY(0.), mZ(0.)	{}

	double	mX, mY, mZ;
};


const double VZERO = 1.0e-10;

//inline bool operator == ( const Vect& c1, const Vect& c2 )
//{
//	if ( fabs( c1.mX - c2.mX ) < VZERO && fabs( c1.mY - c2.mY ) < VZERO && fabs( c1.mZ - c2.mZ ) < VZERO )
//		return true;
//	else
//		return false;
//}
//
//
//inline bool operator != ( const Vect& c1, const Vect& c2 )
//{
//	return !(c1 == c2);
//}
//
//
//inline bool operator < ( const Vect& c1, const Vect& c2 )
//{
//	if ( fabs( c1.mX - c2.mX ) > VZERO )
//	{
//		if ( c1.mX + VZERO < c2.mX )
//			return true;
//		else 
//			return false;
//	}
//	else
//	{
//		if ( fabs( c1.mY - c2.mY ) > VZERO )
//		{
//			if ( c1.mY + VZERO < c2.mY )
//				return true;
//			else 
//				return false;
//		}
//		else
//		{
//			return false;
//		}
//	}
//}


inline bool operator == ( const Vect3D& c1, const Vect3D& c2 )
{
	if ( fabs( c1.mX - c2.mX ) < VZERO &&
		 fabs( c1.mY - c2.mY ) < VZERO &&
		 fabs( c1.mZ - c2.mZ ) < VZERO )

		return true;
	else
		return false;
}

inline bool operator != ( const Vect3D& c1, const Vect3D& c2 )
{
	if ( c1 == c2 )
		return false;
	else
		return true;
}

inline bool operator < ( const Vect3D& c1, const Vect3D& c2 )
{
	if ( fabs( c1.mX - c2.mX ) > VZERO )
	{
		if ( c1.mX + VZERO < c2.mX )
			return true;
		else 
			return false;
	}
	else
	{
		if ( fabs( c1.mY - c2.mY ) > VZERO )
		{
			if ( c1.mY + VZERO < c2.mY )
				return true;
			else 
				return false;
		}
		else
		{
			if ( fabs( c1.mZ - c2.mZ ) > VZERO )
			{
				if ( c1.mZ + VZERO < c2.mZ )
					return true;
				else 
					return false;
			}
			else
			{
				return false;
			}
		}
	}
}



//inline bool operator == ( const Vect3D& c1, const Vect3D& c2 )
//{
//	if ( fabs( c1.mX - c2.mX ) < VZERO &&
//		 fabs( c1.mY - c2.mY ) < VZERO &&
//		 fabs( c1.mZ - c2.mZ ) < VZERO )
//
//		return true;
//	else
//		return false;
//}
//
//inline bool operator != ( const Vect3D& c1, const Vect3D& c2 )
//{
//	if ( c1 == c2 )
//		return false;
//	else
//		return true;
//}
//
//inline bool operator < ( const Vect3D& c1, const Vect3D& c2 )
//{
//	if ( fabs( c1.mX - c2.mX ) > VZERO )
//	{
//		if ( c1.mX + VZERO < c2.mX )
//			return true;
//		else 
//			return false;
//	}
//	else
//	{
//		if ( fabs( c1.mY - c2.mY ) > VZERO )
//		{
//			if ( c1.mY + VZERO < c2.mY )
//				return true;
//			else 
//				return false;
//		}
//		else
//		{
//			if ( fabs( c1.mZ - c2.mZ ) > VZERO )
//			{
//				if ( c1.mZ + VZERO < c2.mZ )
//					return true;
//				else 
//					return false;
//			}
//			else
//			{
//				return false;
//			}
//		}
//	}
//}


inline double Dist( const Vect3D& c1, const Vect3D& c2 )
{
	return sqrt( (c1.mX-c2.mX)*(c1.mX-c2.mX) +  (c1.mY-c2.mY)*(c1.mY-c2.mY) + (c1.mZ-c2.mZ)*(c1.mZ-c2.mZ) );
}


class Cell
{
public:
	int	mid1, mid2, mid3, mid4;
};


class EdgeSplit
{
public:
	typedef vector< Vect3D >				ExtraPnts;
	typedef Key< 2, Vect3D >				Edge;
	typedef pair< Edge, vector< int > >		EdgeWithDivision;
	typedef vector< EdgeWithDivision >		ColEdgeSplit;

	void ReadSplit( const string& name)
	{
		cout << "ReadSplit - " << name << endl;
		ProgressBar	bar(40);

		ifstream f( name.c_str() );

		if ( ! f)
		{
			cout << name << " - does not exists" << endl;
			return;
		}

		string buff;
		istringstream is;
		int edgenum;
		getline( f, buff);
		is.clear();
		is.str( buff );

		is >> edgenum;

		bar.Init(edgenum);
		bar.Start();
		for(int i=0; i<edgenum; ++i, ++bar)
		{
			//vector< Vect3D >	vdivisionpnts;
			int tmp;
			Edge edgeTmp;
			getline( f, buff);
			is.clear();
			is.str( buff );

			is >> tmp >> edgeTmp.rFirst().mX >> edgeTmp.rFirst().mY >> edgeTmp.rFirst().mZ;

			getline( f, buff);
			is.clear();
			is.str( buff );

			is >> tmp >> edgeTmp.rSecond().mX >> edgeTmp.rSecond().mY >> edgeTmp.rSecond().mZ;

			edgeTmp.Sort();

			int extrapntsnum;
			Vect3D posTmp;
			getline( f, buff);
			is.clear();
			is.str( buff );

			is >> extrapntsnum;
			vector< int> idsTmp;

			for(int j=0; j< extrapntsnum; ++j)
			{
				getline( f, buff);
				is.clear();
				is.str( buff );

				is >> tmp >> posTmp.mX >> posTmp.mY >> posTmp.mZ;
				mExtrPnts.push_back( posTmp);
				idsTmp.push_back( mExtrPnts.size() - 1);
				//vdivisionpnts.push_back( posTmp);
			}
			sort( idsTmp.begin(), idsTmp.end() );
			//sort( vdivisionpnts.begin(), vdivisionpnts.end() );

			mSplit.push_back( EdgeWithDivision( edgeTmp, idsTmp) );
			getline( f, buff);	//empty line
		}
		bar.Finish();
	}

	Vect3D&		rExtraPnt( const int& i)	{ return mExtrPnts[i]; }
//private:
	ExtraPnts		mExtrPnts;
	ColEdgeSplit	mSplit;

};

class Grid
{
public:

	void ReadUnstruct( const string& name)
	{
		cout << "ReadUnstruct - " << name << endl;
		ProgressBar	bar(40);


		const int offset = mtabNode.size();

		ifstream f( name.c_str() );
		string buff;

		getline( f, buff);
		getline( f, buff);
		getline( f, buff);

		int nn, ne;
		istringstream is;

		//cmatch res;
		//regex rex( "N=([0-9]*).*E=([0-9]*)");
		//regex_search( buff.c_str(), res, rex);
		//cout << res[1] << " :: " << res[2] << "\n";

		//is.clear();
		//is.str( res[1] );
		//is >> nn;

		//is.clear();
		//is.str( res[2] );
		//is >> ne;


		RegExp	rex;
		rex.InitPattern( "N=([0-9]*).*E=([0-9]*)" );
		rex.SetString( buff);

		if ( rex.IsOk() )
		{
			is.clear();
			is.str( rex.GetSubString( 1) );
			is >> nn;

			is.clear();
			is.str( rex.GetSubString( 2) );
			is >> ne;

			cout << nn << " :: " << ne << "\n";
		}


		mtabNode.reserve( nn + mtabNode.size() );
		mtabCell.reserve( ne + mtabCell.size() );

		Vect3D vct;

		bar.Init( nn );
		bar.Start();
		for ( int i=0; i<nn; ++i, ++bar)
		{
			getline( f, buff);
			is.clear();
			is.str( buff );
			is >> vct.mX >> vct.mY >> vct.mZ;
			mtabNode.push_back( vct);
		}
		bar.Finish();

		Cell cell;

		bar.Init( ne );
		bar.Start();
		for ( int i=0; i<ne; ++i, ++bar)
		{
			getline( f, buff);
			is.clear();
			is.str( buff );
			is >> cell.mid1 >> cell.mid2 >> cell.mid3 >> cell.mid4;
			cell.mid1 += offset;
			cell.mid2 += offset;
			cell.mid3 += offset;
			cell.mid4 += offset;
			mtabCell.push_back( cell);
		}
		bar.Finish();
	}

	void ReadStruct( const string& name)
	{
		this->mSplit.ReadSplit( "extrapnts.txt");

		cout << "ReadStruct - " << name << endl;
		
		cout << "this->mSplit.mSplit.size() = " << this->mSplit.mSplit.size() << endl;
		
		ProgressBar	bar(40);

		const int offset = mtabNode.size();

		ifstream f( name.c_str() );
		string buff;

		getline( f, buff);
		getline( f, buff);

		int nn, ne;
		istringstream is;

		//cmatch res;
		//regex rex( "NODES=([0-9]*).*ELEMENTS=([0-9]*)");
		//regex_search( buff.c_str(), res, rex);
		//cout << res[1] << " :: " << res[2] << "\n";

		//is.clear();
		//is.str( res[1] );
		//is >> nn;

		//is.clear();
		//is.str( res[2] );
		//is >> ne;


		RegExp	rex;
		rex.InitPattern( "NODES=([0-9]*).*ELEMENTS=([0-9]*)" );
		rex.SetString( buff);

		if ( rex.IsOk() )
		{
			is.clear();
			is.str( rex.GetSubString( 1) );
			is >> nn;

			is.clear();
			is.str( rex.GetSubString( 2) );
			is >> ne;
		
			cout << nn << " :: " << ne << "\n";
		}


		mtabNode.reserve( nn + mtabNode.size() );
		mtabCell.reserve( ne + mtabCell.size() );

		Vect3D vct;
		bar.Init( nn );
		bar.Start();
		for ( int i=0; i<nn; ++i, ++bar)
		{
			getline( f, buff);
			is.clear();
			is.str( buff );
			is >> vct.mX >> vct.mY >> vct.mZ;
			mtabNode.push_back( vct);
		}
		bar.Finish();

		map< int, int>	extranodesIdsmap;

		for( int i = 0; i < mSplit.mExtrPnts.size(); ++i)	//Pushing extranodes;
		{
			mtabNode.push_back( mSplit.mExtrPnts[i] );

			extranodesIdsmap[i] = mtabNode.size() ; //in cell indexing 1, 2, ..., n
		}
		

		Cell cell;
		bar.Init( ne );
		bar.Start();
		double tmpx;
		for ( int i=0; i<ne; ++i, ++bar)
		{
			getline( f, buff);
			is.clear();
			is.str( buff );
			is >> cell.mid1 >> cell.mid2 >> tmpx >> cell.mid3 >> cell.mid4 >> tmpx >> tmpx >> tmpx;

			cell.mid1 += offset;
			cell.mid2 += offset;
			cell.mid3 += offset;
			cell.mid4 += offset;

			int idTab[] = { cell.mid1, cell.mid2, cell.mid3, cell.mid4 };
			bool isDivided = false;

			for( int j=0; j<this->mSplit.mSplit.size(); ++j)
			{
				int isoncell = IsOnCell( cell, j);

				if(isoncell == -1)
					continue;
				else
				{
					vector< int> vnodesId;
					int lastId = -1;
					for( int jj=0; jj<this->mSplit.mSplit[j].second.size(); ++jj)	//loop over extra points on edge
					{
						bool nodesInUse[] = { false, false, false, false }; 
						int extranodeid = this->mSplit.mSplit[j].second[jj];
						nodesInUse[ edgeidsKey[isoncell][0] ] = true;
						nodesInUse[ edgeidsKey[isoncell][1] ] = true;

						vnodesId.clear();

						for( int k=0; k<4; ++k) //looking for nodes not in use
						{
							if( !nodesInUse[k] )
							{
								vnodesId.push_back( idTab[k] );
							}
						}

						double	dis1 = Dist( this->mtabNode[ idTab[ edgeidsKey[isoncell][0] ]-1 ], this->mSplit.rExtraPnt(extranodeid) ),
								dis2 = Dist( this->mtabNode[ idTab[ edgeidsKey[isoncell][1] ]-1 ], this->mSplit.rExtraPnt(extranodeid) );
						if( dis1 < dis2 ) //first base node typed
						{
							vnodesId.push_back( idTab[ edgeidsKey[isoncell][0] ] );
							vnodesId.push_back( extranodesIdsmap[extranodeid] );
							lastId = idTab[ edgeidsKey[isoncell][1] ];

							idTab[ edgeidsKey[isoncell][0] ] = extranodesIdsmap[extranodeid];

						}
						else			//second base node typed
						{
							vnodesId.push_back( idTab[ edgeidsKey[isoncell][1] ] );
							vnodesId.push_back( extranodesIdsmap[extranodeid] );
							lastId = idTab[ edgeidsKey[isoncell][0] ];

							idTab[ edgeidsKey[isoncell][1] ] = extranodesIdsmap[extranodeid];
						}

						Cell cellTmp;
						cellTmp.mid1 = vnodesId[0];
						cellTmp.mid2 = vnodesId[1];
						cellTmp.mid3 = vnodesId[2];
						cellTmp.mid4 = vnodesId[3];

						mtabCell.push_back( cellTmp);
						isDivided = true;
					}

					vnodesId[2] = lastId;

					Cell cellTmp;
					cellTmp.mid1 = vnodesId[0];
					cellTmp.mid2 = vnodesId[1];
					cellTmp.mid3 = vnodesId[2];
					cellTmp.mid4 = vnodesId[3];

					mtabCell.push_back( cellTmp);

				}
			}	
			if(!isDivided)
				mtabCell.push_back( cell);
		}
		bar.Finish();
	}

	int IsOnCell( Cell& cell, const int& edgeId)
	{
		int idTab[] = { cell.mid1, cell.mid2, cell.mid3, cell.mid4 };

		for( int j=0; j<6; ++j)	//loop over tetra edges
		{
			Key<2,Vect3D>	edgeTmp;
			edgeTmp.rFirst() = mtabNode[ idTab[ edgeidsKey[j][0] ]-1 ];
			edgeTmp.rSecond() = mtabNode[ idTab[ edgeidsKey[j][1] ]-1 ];

			edgeTmp.Sort();	//key with sorted positions of nodes

			if( this->mSplit.mSplit[edgeId].first == edgeTmp) //edge to split found
			{
				return j;
			}
		}

		return -1;
	}
	void CheckVolumes()
	{
		double vol;
		int		posVol = 0,
				negVol = 0;
		int		nodeIdTmp;

		int		repVol = 0,
				unrepVol = 0;

		cout << "\nChecking volumes..." << endl;

		ProgressBar	bar(40);
		bar.Init( this->mtabCell.size() );
		bar.Start();

		for(int i=0; i<this->mtabCell.size(); ++i, ++bar )
		{
			SpaceGeomTools::Vect3D vect[4];
			vect[0].rX() = this->mtabNode[ this->mtabCell[i].mid1 - 1 ].mX;
			vect[0].rY() = this->mtabNode[ this->mtabCell[i].mid1 - 1 ].mY;
			vect[0].rZ() = this->mtabNode[ this->mtabCell[i].mid1 - 1 ].mZ;

			vect[1].rX() = this->mtabNode[ this->mtabCell[i].mid2 - 1 ].mX;
			vect[1].rY() = this->mtabNode[ this->mtabCell[i].mid2 - 1 ].mY;
			vect[1].rZ() = this->mtabNode[ this->mtabCell[i].mid2 - 1 ].mZ;

			vect[2].rX() = this->mtabNode[ this->mtabCell[i].mid3 - 1 ].mX;
			vect[2].rY() = this->mtabNode[ this->mtabCell[i].mid3 - 1 ].mY;
			vect[2].rZ() = this->mtabNode[ this->mtabCell[i].mid3 - 1 ].mZ;

			vect[3].rX() = this->mtabNode[ this->mtabCell[i].mid4 - 1 ].mX;
			vect[3].rY() = this->mtabNode[ this->mtabCell[i].mid4 - 1 ].mY;
			vect[3].rZ() = this->mtabNode[ this->mtabCell[i].mid4 - 1 ].mZ;

			const SpaceGeomTools::Simplex<static_cast<SpaceGeomTools::Dimension>(3) >  simpl(	vect[0], vect[1], vect[2], vect[3] );
			vol = simpl.Volume();

			if(vol > 0)
			{

				++posVol;
			}
			else
			{
				nodeIdTmp = this->mtabCell[i].mid1;
				this->mtabCell[i].mid1 = this->mtabCell[i].mid3;
				this->mtabCell[i].mid3 = nodeIdTmp;

				++negVol;

				vect[0].rX() = this->mtabNode[ this->mtabCell[i].mid1 - 1 ].mX;
				vect[0].rY() = this->mtabNode[ this->mtabCell[i].mid1 - 1 ].mY;
				vect[0].rZ() = this->mtabNode[ this->mtabCell[i].mid1 - 1 ].mZ;

				vect[1].rX() = this->mtabNode[ this->mtabCell[i].mid2 - 1 ].mX;
				vect[1].rY() = this->mtabNode[ this->mtabCell[i].mid2 - 1 ].mY;
				vect[1].rZ() = this->mtabNode[ this->mtabCell[i].mid2 - 1 ].mZ;

				vect[2].rX() = this->mtabNode[ this->mtabCell[i].mid3 - 1 ].mX;
				vect[2].rY() = this->mtabNode[ this->mtabCell[i].mid3 - 1 ].mY;
				vect[2].rZ() = this->mtabNode[ this->mtabCell[i].mid3 - 1 ].mZ;

				vect[3].rX() = this->mtabNode[ this->mtabCell[i].mid4 - 1 ].mX;
				vect[3].rY() = this->mtabNode[ this->mtabCell[i].mid4 - 1 ].mY;
				vect[3].rZ() = this->mtabNode[ this->mtabCell[i].mid4 - 1 ].mZ;

				const SpaceGeomTools::Simplex<static_cast<SpaceGeomTools::Dimension>(3) >  simpl2(	vect[0], vect[1], vect[2], vect[3] );
				vol = simpl2.Volume();

				vol = simpl2.Volume();

				if(vol > 0)	++repVol;
				else		++unrepVol;

			}
		}

		bar.Finish();
		cout << "\nNumber of negative volume elements:\t" << negVol
			 << "\nNumber of positive volume elements:\t" << posVol << endl;

		cout << "\nNumber of repaired volume elements:\t" << repVol
			 << "\nNumber of unrepaired volume elements:\t" << unrepVol << endl;
	}

	void RemoveDuplicateNodes()
	{
		cout << endl;
		cout << "RemoveDuplicateNodes" << endl;
		ProgressBar	bar(40);

		multimap< Vect3D, int>	nodmap;

		bar.Init( mtabNode.size() );
		bar.Start();
		for ( int i=0; i<mtabNode.size(); ++i, ++bar)
			nodmap.insert( multimap< Vect3D, int>::value_type( mtabNode[i], i+1) );
		bar.Finish();

		cout << "mtabNode.size() = " << mtabNode.size() << endl;
		cout << "nodmap.size() = " << nodmap.size() << endl;

		multimap< Vect3D, int>::iterator	itr, itrold;

		map<int,int>	idmap;
		vector<Vect3D>	tabnn;

		int id = 1;
		itrold = itr = nodmap.begin();

		tabnn.push_back( itr->first);
		idmap.insert( map<int,int>::value_type( itr->second, id ) );
		
		itr->second = id;

		bar.Init( nodmap.size() );
		bar.Start();
		for ( ++itr; itr!=nodmap.end(); ++itr, ++itrold, ++bar)
		{
			if ( ! ( itrold->first == itr->first ) )
			{
				tabnn.push_back( itr->first);
				++id;
			}

			idmap.insert( map<int,int>::value_type( itr->second, id ) );
			itr->second = id;
		}
		bar.Finish();

		cout << id << " " << tabnn.size() << endl;

		bar.Init( mtabCell.size() );
		bar.Start();
		for ( int i=0; i<mtabCell.size(); ++i, ++bar)
		{
			Cell& cell = mtabCell[i];

			cell.mid1 = idmap[cell.mid1];
			cell.mid2 = idmap[cell.mid2];
			cell.mid3 = idmap[cell.mid3];
			cell.mid4 = idmap[cell.mid4];
		}
		bar.Finish();

		mtabNode.swap( tabnn);
		cout << mtabNode.size() << endl;
	}

	void ExtractBoundary()
	{
		cout << endl;
		cout << "ExtractBoundary" << endl;
		ProgressBar	bar(40);

		//vector<Key3> tabface;
		//vector<Key3>::iterator itrf, itrfe;

		//for ( int i=0; i<mtabCell.size(); ++i)
		//{
		//	Cell& cell = mtabCell[i];
		//	Key3 key( cell.mid1, cell.mid2, cell.mid3 );	
		//	key.Sort();
		//	tabface.push_back( key );
		//}
		//sort( tabface.begin(), tabface.end() );
		//itrfe = unique( tabface.begin(), tabface.end() );

		//cout << " tabfac = " << tabface.size() << endl;
		//cout << " tabfac unique = " << itrfe - tabface.begin() << endl;


		vector<Key3> tabface;
		vector<Key3>::iterator itr, itre;

		tabface.reserve( 2 * mtabCell.size() );


		bar.Init( mtabCell.size() );
		bar.Start();
		for ( int i=0; i<mtabCell.size(); ++i, ++bar)
		{
			Cell& cell = mtabCell[i];
			Key3 key;

			key = Key3( cell.mid1, cell.mid2, cell.mid3 );	
			key.Sort();
			tabface.push_back( key );

			key = Key3( cell.mid1, cell.mid4, cell.mid2 );	
			key.Sort();
			tabface.push_back( key );

			key = Key3( cell.mid2, cell.mid4, cell.mid3 );	
			key.Sort();
			tabface.push_back( key );

			key = Key3( cell.mid3, cell.mid4, cell.mid1 );	
			key.Sort();
			tabface.push_back( key );
		}
		bar.Finish();

		//cout << " before = " << tabedge.size() << endl;
		sort( tabface.begin(), tabface.end() );

		vector<Key3> tabbnd;
		for ( itr=tabface.begin(); itr!=tabface.end(); ++itr)
		{
			vector<Key3>::iterator itrnext = itr+1;

			if ( itrnext!=tabface.end() )
			{
				if ( *itr != *itrnext)
					tabbnd.push_back( *itr);
				else
					itr = itrnext;
			}
			else
				tabbnd.push_back( *itr);
		}

		cout << " tabbnd = " << tabbnd.size() << endl;

		//sort( tabbnd.begin(), tabbnd.end() );
		//itre = unique( tabbnd.begin(), tabbnd.end() );
		//cout << " tabbnd unique = " << itre - tabbnd.begin() << endl;

		//////////////////////
		const double ymin = 1.0e-4;
		typedef pair< vector<Key3>::iterator, vector<Key3>::iterator > TRange;
		TRange range;
		
		mtabBnd.resize( 3);		// 0 - viscous; 1 - symmetry; 2 - farfield

		vector<bool> taborient;

		taborient.resize( tabbnd.size(), false);

		bar.Init( mtabCell.size() );
		bar.Start();
		for ( int i=0; i<mtabCell.size(); ++i, ++bar)
		{
			Cell& cell = mtabCell[i];
			Key<4> ckey( cell.mid1, cell.mid2, cell.mid3, cell.mid4 );

			for ( int k=0; k<4; ++k)
			{
				Key<4> ckeyloc = ckey;
				ckeyloc.Rotate( k);

				Key3 key( ckeyloc.cFirst(), ckeyloc.cSecond(), ckeyloc.cThird() );
				if ( k % 2 != 0 )
					key.Reverse();
	
				Key3 keys = key;
				keys.Sort();

				range = equal_range( tabbnd.begin(), tabbnd.end(), keys );

				if ( range.second - range.first == 1 )
				{
					int id = range.first - tabbnd.begin();
					if ( tabbnd[id] != keys)
						cout << id << "  bnd corrupted !!!" << endl;


					Vect3D vc;
					vc.mX = ( mtabNode[key.cFirst()-1].mX + mtabNode[key.cSecond()-1].mX + mtabNode[key.cThird()-1].mX ) / 3.;
					vc.mY = ( mtabNode[key.cFirst()-1].mY + mtabNode[key.cSecond()-1].mY + mtabNode[key.cThird()-1].mY ) / 3.;
					vc.mZ = ( mtabNode[key.cFirst()-1].mZ + mtabNode[key.cSecond()-1].mZ + mtabNode[key.cThird()-1].mZ ) / 3.;

					double d = sqrt( vc.mX*vc.mX + vc.mY*vc.mY + vc.mZ*vc.mZ );

					if ( mtabNode[key.cFirst()-1].mY < ymin && mtabNode[key.cSecond()-1].mY < ymin && mtabNode[key.cThird()-1].mY < ymin )
					//if ( mtabNode[key.cFirst()-1].mY > -ymin && mtabNode[key.cSecond()-1].mY > -ymin && mtabNode[key.cThird()-1].mY > -ymin )
					{
						mtabBnd[1].push_back( key);
					}
					else
					if ( d > 50. )
					//if ( d > 200. )
					{
						mtabBnd[2].push_back( key);
					}
					else
					{
						mtabBnd[0].push_back( key);
					}

					taborient[id] = true;
				}
			}
		}
		bar.Finish();

		for ( int i=0; i<taborient.size(); ++i)
			if ( ! taborient[i] )
					cout << i << "  bface not found !!!" << endl;

		cout << mtabBnd[0].size() << endl;
		cout << mtabBnd[1].size() << endl;
		cout << mtabBnd[2].size() << endl;
		cout << "sum = " << mtabBnd[0].size() + mtabBnd[1].size() + mtabBnd[2].size() << endl;

		//////////////////////
		string name = "out_surf.dat";
		cout << "Write surf TEC - " << name << endl;

		ofstream of( name);

		for ( int u=0; u<3; ++u)
		{
			of << "VARIABLES = \"X\", \"Y\", \"Z\"" << endl;
			of << "ZONE T=\"grid\", N=" << mtabNode.size() << ", E=" << mtabBnd[u].size() << ", F=FEPOINT, ET=TRIANGLE" << endl;

			bar.Init( mtabNode.size() );
			bar.Start();
			for ( int i=0; i<mtabNode.size(); ++i, ++bar)
			{
				Vect3D& v = mtabNode[i];
				of << setprecision( 16) << v.mX << " " << v.mY << " " << v.mZ << endl;
			}
			bar.Finish();

			bar.Init( tabbnd.size() );
			bar.Start();
			for ( int i=0; i<mtabBnd[u].size(); ++i, ++bar)
			{
				of << mtabBnd[u][i].cFirst() << " " << mtabBnd[u][i].cSecond() << " " << mtabBnd[u][i].cThird() << endl;
			}
			bar.Finish();
		}


		//of << "VARIABLES = \"X\", \"Y\", \"Z\"" << endl;
		//of << "ZONE T=\"grid\", N=" << mtabNode.size() << ", E=" << tabbnd.size() << ", F=FEPOINT, ET=TRIANGLE" << endl;

		//bar.Init( mtabNode.size() );
		//bar.Start();
		//for ( int i=0; i<mtabNode.size(); ++i, ++bar)
		//{
		//	Vect3D& v = mtabNode[i];
		//	of << setprecision( 16) << v.mX << " " << v.mY << " " << v.mZ << endl;
		//}
		//bar.Finish();

		//bar.Init( tabbnd.size() );
		//bar.Start();
		//for ( int i=0; i<tabbnd.size(); ++i, ++bar)
		//{
		//	of << tabbnd[i].cFirst() << " " << tabbnd[i].cSecond() << " " << tabbnd[i].cThird() << endl;
		//}
		//bar.Finish();
		////////////////////////

		//////////////////////
		//multimap<int,Key3> mapbface;
		//for ( itr=tabbnd.begin(); itr!=tabbnd.end(); ++itr)
		//{
		//	mapbface.insert( multimap<int,Key3>::value_type( itr->cFirst(), *itr) );
		//	mapbface.insert( multimap<int,Key3>::value_type( itr->cSecond(), *itr) );
		//}

		//multimap<int,Key2>::iterator imap, ifst, ilst;
		//typedef pair< multimap<int,Key2>::iterator, multimap<int,Key2>::iterator > TRange;
		//TRange range;
		//Key2 ky;


		//do
		//{
		//	mtabBnd.push_back( vector<Key2>() );
		//	vector<Key2>& rtab = mtabBnd[mtabBnd.size()-1];
		//
		//	ky = mapbface.begin()->second;
		//	rtab.push_back( ky );

		//	do
		//	{
		//		ky.Sort();

		//		range = mapbface.equal_range( ky.cFirst() );
		//		for ( multimap<int,Key2>::iterator i=range.first; i!=range.second; ++i )
		//			if ( i->second == ky )
		//			{
		//				mapbface.erase( i);
		//				break;
		//			}

		//		range = mapbface.equal_range( ky.cSecond() );
		//		for ( multimap<int,Key2>::iterator i=range.first; i!=range.second; ++i )
		//			if ( i->second == ky )
		//			{
		//				mapbface.erase( i);
		//				break;
		//			}

		//		int id = rtab.size() - 1;
		//		//cout << id << endl;

		//		range = mapbface.equal_range( rtab[id].cSecond() );
		//		if ( range.first == range.second )
		//			break;

		//		for ( imap=range.first; imap!=range.second; ++imap )
		//			if ( imap->second != rtab[id] )
		//			{
		//				ifst = imap;
		//				break;
		//			}


		//		ky = ifst->second;

		//		if ( ky.cFirst() != rtab[id].cSecond() )
		//			ky.Reverse();

		//		rtab.push_back( ky );
		//	} 
		//	while ( true);

		//	cout << rtab.size() << endl;
		//}
		//while ( mapbface.size() > 0);

	}

	void Write( const string& name)
	{
		cout << "Write TEC - " << name << endl;
		ProgressBar	bar(40);

		ofstream of( name);

		of << "VARIABLES = \"X\", \"Y\", \"Z\"" << endl;
		of << "ZONE T=\"grid\", N=" << mtabNode.size() << ", E=" << mtabCell.size() << ", F=FEPOINT, ET=TETRAHEDRON" << endl;


		bar.Init( mtabNode.size() );
		bar.Start();
		for ( int i=0; i<mtabNode.size(); ++i, ++bar)
		{
			Vect3D& v = mtabNode[i];
			of << setprecision( 16) << v.mX << " " << v.mY << " " << v.mZ << endl;
		}
		bar.Finish();

		bar.Init( mtabCell.size() );
		bar.Start();
		for ( int i=0; i<mtabCell.size(); ++i, ++bar)
		{
			Cell& cell = mtabCell[i];
			of << cell.mid1 << " " << cell.mid2 << " " << cell.mid3 << " " << cell.mid4 << endl;
		}
		bar.Finish();
	}


	void WriteMSH2( const string& name)
	{
		cout << "Write MSH2 - " << name << endl;
		ProgressBar	bar(40);

		ofstream of( name);

		of << "MSH_VER 2.0" << endl;
		of << "3D" << endl;
		of << "Onera M6 grid after stitching" << endl;


		of << mtabNode.size() << endl;
		bar.Init( mtabNode.size() );
		bar.Start();
		for ( int i=0; i<mtabNode.size(); ++i, ++bar)
		{
			Vect3D& v = mtabNode[i];
			of << setprecision( 16) << v.mX << " " << v.mY << " " << v.mZ << endl;
		}
		bar.Finish();

		of << "1" << endl;
		of << "4 " << mtabCell.size() << endl;
		bar.Init( mtabCell.size() );
		bar.Start();
		for ( int i=0; i<mtabCell.size(); ++i, ++bar)
		{
			Cell& cell = mtabCell[i];
			of << cell.mid1 << " " << cell.mid2 << " " << cell.mid3 << " " << cell.mid4 << endl;
		}
		bar.Finish();

		int n=0;
		for ( int i=0; i<mtabBnd.size(); ++i)
			n += mtabBnd[i].size();

		of << "1" << endl;
		of << "3 " << n << endl;
		for ( int i=0; i<mtabBnd.size(); ++i)
			for ( int j=0; j<mtabBnd[i].size(); ++j)
			{
				Key3& cell = mtabBnd[i][j];
				//if ( j==1)
				//	cell.Reverse();
				of << i+1 << " " << cell.cFirst() << " " << cell.cSecond() << " " << cell.cThird() << endl;
			}

	}

private:
	EdgeSplit		mSplit;

	vector<Vect3D>	mtabNode;
	vector<Cell>	mtabCell;

	vector< vector<Key3> >	mtabBnd;
};


int main()
{
	cout << "HELLO" << endl;
	Grid grid;
	//EdgeSplit	Split;

	//Split.ReadSplit( "extrapnts.txt");

	grid.ReadUnstruct( "_final_.dat");
	grid.ReadStruct( "onera_Gen3D_tetra.dat");

	grid.RemoveDuplicateNodes();

	//grid.Write( "out.dat");
	//return 0;

	grid.ExtractBoundary();

	grid.CheckVolumes();

	grid.Write( "out.dat");
	grid.WriteMSH2( "out.msh2");

	return 0;
}

//int main()
//{
//	cout << "HELLO" << endl;
//	Grid grid;
//	//EdgeSplit	Split;
//
//	//Split.ReadSplit( "extrapnts.txt");
//
//	grid.ReadUnstruct( "_final_.dat");
//	grid.ReadStruct( "a3_Gen3D_tetra.dat");
//
//	grid.RemoveDuplicateNodes();
//
//	//grid.Write( "out.dat");
//	//return 0;
//
//	grid.ExtractBoundary();
//
//	grid.CheckVolumes();
//
//	grid.Write( "out.dat");
//	grid.WriteMSH2( "out.msh2");
//
//	return 0;
//}

