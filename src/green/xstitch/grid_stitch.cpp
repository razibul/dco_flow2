
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <map>
#include <string>
#include <vector>
#include <regex>

#include "key.h"


typedef Key<2> Key2;
typedef Key<3> Key3;

using namespace std;

class Vect
{
public:
	double	mX, mY;
};


const double VZERO = 1.0e-7;

inline bool operator == ( const Vect& c1, const Vect& c2 )
{
	if ( fabs( c1.mX - c2.mX ) < VZERO && fabs( c1.mY - c2.mY ) < VZERO )
		return true;
	else
		return false;
}


inline bool operator != ( const Vect& c1, const Vect& c2 )
{
	return !(c1 == c2);
}


inline bool operator < ( const Vect& c1, const Vect& c2 )
{
	if ( fabs( c1.mX - c2.mX ) > VZERO )
	{
		if ( c1.mX + VZERO < c2.mX )
			return true;
		else 
			return false;
	}
	else
	{
		if ( fabs( c1.mY - c2.mY ) > VZERO )
		{
			if ( c1.mY + VZERO < c2.mY )
				return true;
			else 
				return false;
		}
		else
		{
			return false;
		}
	}
}
  



class Cell
{
public:
	int	mid1, mid2, mid3;
};



class Grid
{
public:

	void ReadUnstruct( const string& name)
	{
		const int offset = mtabNode.size();

		ifstream f( name.c_str() );
		string buff;

		getline( f, buff);
		getline( f, buff);
		getline( f, buff);

		std::tr1::cmatch res;
		tr1::regex rex( "N=([0-9]*).*E=([0-9]*)");
		std::tr1::regex_search( buff.c_str(), res, rex);
		std::cout << res[1] << " :: " << res[2] << "\n";

		int nn, ne;
		istringstream is;

		is.clear();
		is.str( res[1] );
		is >> nn;

		is.clear();
		is.str( res[2] );
		is >> ne;

		Vect vct;
		for ( int i=0; i<nn; ++i)
		{
			getline( f, buff);
			is.clear();
			is.str( buff );
			is >> vct.mX >> vct.mY;
			mtabNode.push_back( vct);
		}

		Cell cell;
		for ( int i=0; i<ne; ++i)
		{
			getline( f, buff);
			is.clear();
			is.str( buff );
			is >> cell.mid1 >> cell.mid2 >> cell.mid3;
			cell.mid1 += offset;
			cell.mid2 += offset;
			cell.mid3 += offset;
			mtabCell.push_back( cell);
		}
	}

	void ReadStruct( const string& name)
	{
		const int offset = mtabNode.size();

		ifstream f( name.c_str() );
		string buff;

		getline( f, buff);
		getline( f, buff);

		std::tr1::cmatch res;
		tr1::regex rex( "NODES=([0-9]*).*ELEMENTS=([0-9]*)");
		std::tr1::regex_search( buff.c_str(), res, rex);
		std::cout << res[1] << " :: " << res[2] << "\n";

		int nn, ne;
		istringstream is;

		is.clear();
		is.str( res[1] );
		is >> nn;

		is.clear();
		is.str( res[2] );
		is >> ne;

		Vect vct;
		for ( int i=0; i<nn; ++i)
		{
			getline( f, buff);
			is.clear();
			is.str( buff );
			is >> vct.mX >> vct.mY;
			mtabNode.push_back( vct);
		}


		Cell cell1, cell2;
		for ( int i=0; i<ne; ++i)
		{
			getline( f, buff);
			is.clear();
			is.str( buff );
			is >> cell1.mid1 >> cell1.mid2 >> cell1.mid3 >> cell2.mid2;
			cell2.mid1 = cell1.mid3;
			cell2.mid3 = cell1.mid1;

			cell1.mid1 += offset;
			cell1.mid2 += offset;
			cell1.mid3 += offset;

			cell2.mid1 += offset;
			cell2.mid2 += offset;
			cell2.mid3 += offset;
			mtabCell.push_back( cell1);
			mtabCell.push_back( cell2);
		}
	}

	void RemoveDuplicateNodes()
	{
		multimap< Vect, int>	nodmap;
		for ( int i=0; i<mtabNode.size(); ++i)
			nodmap.insert( multimap< Vect, int>::value_type( mtabNode[i], i+1) );

		cout << "mtabNode.size() = " << mtabNode.size() << endl;
		cout << "nodmap.size() = " << nodmap.size() << endl;

		multimap< Vect, int>::iterator	itr, itrold;

		map<int,int>	idmap;
		vector<Vect>	tabnn;

		int id = 1;
		itrold = itr = nodmap.begin();

		tabnn.push_back( itr->first);
		idmap.insert( map<int,int>::value_type( itr->second, id ) );
		
		itr->second = id;

		for ( ++itr; itr!=nodmap.end(); ++itr, ++itrold)
		{
			if ( itrold->first != itr->first )
			{
				tabnn.push_back( itr->first);
				++id;
			}

			idmap.insert( map<int,int>::value_type( itr->second, id ) );
			itr->second = id;
		}

		//cout << id << " " << tabnn.size() << endl;

		for ( int i=0; i<mtabCell.size(); ++i)
		{
			Cell& cell = mtabCell[i];

			cell.mid1 = idmap[cell.mid1];
			cell.mid2 = idmap[cell.mid2];
			cell.mid3 = idmap[cell.mid3];
		}

		mtabNode.swap( tabnn);
		cout << mtabNode.size() << endl;
	}

	void ExtractBoundary()
	{
		//vector<Key3> tabface;
		//vector<Key3>::iterator itrf, itrfe;

		//for ( int i=0; i<mtabCell.size(); ++i)
		//{
		//	Cell& cell = mtabCell[i];
		//	Key3 key( cell.mid1, cell.mid2, cell.mid3 );	
		//	key.Sort();
		//	tabface.push_back( key );
		//}
		//sort( tabface.begin(), tabface.end() );
		//itrfe = unique( tabface.begin(), tabface.end() );

		//cout << " tabfac = " << tabface.size() << endl;
		//cout << " tabfac unique = " << itrfe - tabface.begin() << endl;

		vector<Key2> tabedge;
		vector<Key2>::iterator itr, itre;

		for ( int i=0; i<mtabCell.size(); ++i)
		{
			Cell& cell = mtabCell[i];
			Key2 key;

			key = Key2(cell.mid1,cell.mid2);	
			key.Sort();
			tabedge.push_back( key );

			key = Key2(cell.mid2,cell.mid3);	
			key.Sort();
			tabedge.push_back( key );

			key = Key2(cell.mid3,cell.mid1);	
			key.Sort();
			tabedge.push_back( key );
		}

		//cout << " before = " << tabedge.size() << endl;
		sort( tabedge.begin(), tabedge.end() );

		vector<Key2> tabbnd;
		for ( itr=tabedge.begin(); itr!=tabedge.end(); ++itr)
		{
			vector<Key2>::iterator itrnext = itr+1;

			if ( itrnext!=tabedge.end() )
			{
				if ( *itr != *itrnext)
					tabbnd.push_back( *itr);
				else
					itr = itrnext;
			}
			else
				tabbnd.push_back( *itr);
		}

		cout << " tabbnd = " << tabbnd.size() << endl;

		sort( tabbnd.begin(), tabbnd.end() );
		itre = unique( tabbnd.begin(), tabbnd.end() );
		cout << " tabbnd unique = " << itre - tabbnd.begin() << endl;

		////////////////////
		multimap<int,Key2> mapbface;
		for ( itr=tabbnd.begin(); itr!=tabbnd.end(); ++itr)
		{
			mapbface.insert( multimap<int,Key2>::value_type( itr->cFirst(), *itr) );
			mapbface.insert( multimap<int,Key2>::value_type( itr->cSecond(), *itr) );
		}

		multimap<int,Key2>::iterator imap, ifst, ilst;
		typedef pair< multimap<int,Key2>::iterator, multimap<int,Key2>::iterator > TRange;
		TRange range;
		Key2 ky;


		do
		{
			mtabBnd.push_back( vector<Key2>() );
			vector<Key2>& rtab = mtabBnd[mtabBnd.size()-1];
		
			ky = mapbface.begin()->second;
			rtab.push_back( ky );

			do
			{
				ky.Sort();

				range = mapbface.equal_range( ky.cFirst() );
				for ( multimap<int,Key2>::iterator i=range.first; i!=range.second; ++i )
					if ( i->second == ky )
					{
						mapbface.erase( i);
						break;
					}

				range = mapbface.equal_range( ky.cSecond() );
				for ( multimap<int,Key2>::iterator i=range.first; i!=range.second; ++i )
					if ( i->second == ky )
					{
						mapbface.erase( i);
						break;
					}

				int id = rtab.size() - 1;
				//cout << id << endl;

				range = mapbface.equal_range( rtab[id].cSecond() );
				if ( range.first == range.second )
					break;

				for ( imap=range.first; imap!=range.second; ++imap )
					if ( imap->second != rtab[id] )
					{
						ifst = imap;
						break;
					}


				ky = ifst->second;

				if ( ky.cFirst() != rtab[id].cSecond() )
					ky.Reverse();

				rtab.push_back( ky );
			} 
			while ( true);

			cout << rtab.size() << endl;
		}
		while ( mapbface.size() > 0);

	}

	void Write( const string& name)
	{
		ofstream of( name);

		of << "VARIABLES = \"X\", \"Y\"" << endl;
		of << "ZONE T=\"grid\", N=" << mtabNode.size() << ", E=" << mtabCell.size() << ", F=FEPOINT, ET=TRIANGLE" << endl;


		for ( int i=0; i<mtabNode.size(); ++i)
		{
			Vect& v = mtabNode[i];
			of << setprecision( 12) << v.mX << " " << v.mY << endl;
		}

		for ( int i=0; i<mtabCell.size(); ++i)
		{
			Cell& cell = mtabCell[i];
			of << cell.mid1 << " " << cell.mid2 << " " << cell.mid3 << endl;
		}
	}


	void WriteMSH2( const string& name)
	{
		ofstream of( name);

		of << "MSH_VER 2.0" << endl;
		of << "2D" << endl;
		of << "grid after stitching" << endl;


		of << mtabNode.size() << endl;
		for ( int i=0; i<mtabNode.size(); ++i)
		{
			Vect& v = mtabNode[i];
			of << setprecision( 12) << v.mX << " " << v.mY << endl;
		}

		of << "1" << endl;
		of << "3 " << mtabCell.size() << endl;
		for ( int i=0; i<mtabCell.size(); ++i)
		{
			Cell& cell = mtabCell[i];
			of << cell.mid1 << " " << cell.mid2 << " " << cell.mid3 << endl;
		}

		int n=0;
		for ( int i=0; i<mtabBnd.size(); ++i)
			n += mtabBnd[i].size();

		of << "1" << endl;
		of << "2 " << n << endl;
		for ( int i=0; i<mtabBnd.size(); ++i)
			for ( int j=0; j<mtabBnd[i].size(); ++j)
			{
				Key2& cell = mtabBnd[i][j];
				if ( j==1)
					cell.Reverse();
				of << i+1 << " " << cell.cFirst() << " " << cell.cSecond() << endl;
			}

	}

private:
	vector<Vect>	mtabNode;
	vector<Cell>	mtabCell;

	vector< vector<Key2> >	mtabBnd;
};


int main()
{
	Grid grid;

	grid.ReadUnstruct( "_final_.dat");
	grid.ReadStruct( "testTec.dat");

	grid.RemoveDuplicateNodes();
	grid.ExtractBoundary();

	grid.Write( "out.dat");
	grid.WriteMSH2( "out.msh2");

	return 0;
}