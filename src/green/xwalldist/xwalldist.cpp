#include <omp.h>


#include "libcoresystem/mgdecl.h"
#include "libcorecommon/key.h"
#include "libcoregeom/vect.h"

#include "libcoreio/readmsh2.h"
#include "libcoreio/writemsh2.h"
#include "libcoreio/writetec.h"
#include "libcoreio/writesol.h"
#include "libcorecommon/progressbar.h"
#include "libcorecommon/regexp.h"

#include "libhfgeom/test_intersection.h"


INIT_VERSION("0.1","'xextruder - wip'");


template <Dimension DIM>
class WallDistCalc;


//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class Grid : public IO::GridReadFacade, public IO::GridWriteFacade
{
public:
	friend class WallDistCalc<DIM>;

	typedef pair<Key<DIM>,MGSize>	BFPair;
	typedef Vect<DIM>				GVect;


	virtual Dimension	Dim() const		{ return DIM;}

	virtual void	IOSetName( const MGString& name)							{ mGrdName = name;}
	virtual void	IOGetName( MGString& name) const							{ name = mGrdName;}

	virtual void	IOTabNodeResize( const MGSize& n)							{ mtabNode.resize(n); }
	virtual void	IOTabCellResize( const MGSize& type, const MGSize& n)		{ if (type!=DIM+1) THROW_INTERNAL( "bad cell type "<< type ); mtabCell.resize(n); }
	virtual void	IOTabBFaceResize( const MGSize& type, const MGSize& n)		{ if (type!=DIM) THROW_INTERNAL( "bad bface type "<< type ); mtabBFace.resize(n); }

	virtual void	IOSetNodePos( const vector<MGFloat>& tabx, const MGSize& id );
	virtual void	IOSetCellIds( const vector<MGSize>& tabid, const MGSize& type, const MGSize& id );
	virtual void	IOSetBFaceIds( const vector<MGSize>& tabid, const MGSize& type, const MGSize& id );
	virtual void	IOSetBFaceSurf( const MGSize& idsurf, const MGSize& type, const MGSize& id );


	virtual MGSize	IOSizeNodeTab() const										{ return mtabNode.size(); }
	virtual MGSize	IOSizeCellTab( const MGSize& type) const					{ if (type!=DIM+1) THROW_INTERNAL( "bad cell type "<< type ); return mtabCell.size(); }
	virtual MGSize	IOSizeBFaceTab( const MGSize& type) const					{ if (type!=DIM) THROW_INTERNAL( "bad bface type "<< type ); return mtabBFace.size(); }

	virtual void	IOGetNodePos( vector<MGFloat>& tabx, const MGSize& id) const;
	virtual void	IOGetCellIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const;
	virtual void	IOGetBFaceIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const;
	virtual void	IOGetBFaceSurf( MGSize& idsurf, const MGSize& type, const MGSize& id) const;

	virtual MGSize	IONumCellTypes() const										{ return 1;}
	virtual MGSize	IONumBFaceTypes() const										{ return 1;}
	virtual MGSize	IOCellType( const MGSize& i) const							{ ASSERT(i==0); return DIM+1; }
	virtual MGSize	IOBFaceType( const MGSize& i) const							{ ASSERT(i==0); return DIM;}


private:
	string					mGrdName;

	vector<GVect>			mtabNode;
	vector< Key<DIM+1> >	mtabCell;
	vector< BFPair >		mtabBFace;
};

template <Dimension DIM>
void Grid<DIM>::IOSetNodePos( const vector<MGFloat>& tabx, const MGSize& id )
{
	ASSERT( tabx.size() == DIM);
	for ( MGSize i=0; i<DIM; ++i)
		mtabNode[id].rX(i) = tabx[i];
}

template <Dimension DIM>
void Grid<DIM>::IOSetCellIds( const vector<MGSize>& tabid, const MGSize& type, const MGSize& id )
{
	ASSERT( type == DIM+1);
	ASSERT( tabid.size() == type);

	for ( MGSize k=0; k<=DIM; ++k)
		mtabCell[id].rElem(k) = tabid[k];
}

template <Dimension DIM>
void Grid<DIM>::IOSetBFaceIds( const vector<MGSize>& tabid, const MGSize& type, const MGSize& id )
{
	ASSERT( type == DIM);
	ASSERT( tabid.size() == type);

	for ( MGSize k=0; k<DIM; ++k)
		mtabBFace[id].first.rElem(k) = tabid[k];
}

template <Dimension DIM>
void Grid<DIM>::IOSetBFaceSurf( const MGSize& idsurf, const MGSize& type, const MGSize& id )
{
	ASSERT( type == DIM);
	mtabBFace[id].second = idsurf;
}

template <Dimension DIM>
inline void Grid<DIM>::IOGetNodePos( vector<MGFloat>& tabx, const MGSize& id) const
{
	ASSERT( tabx.size() == DIM);
	for ( MGSize i=0; i<DIM; ++i)
		tabx[i] = mtabNode[id].cX(i);
}


template <Dimension DIM>
inline void Grid<DIM>::IOGetCellIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const
{
	ASSERT( type == DIM+1);
	ASSERT( tabid.size() == type);

	for ( MGSize k=0; k<=DIM; ++k)
		tabid[k] = mtabCell[id].cElem(k);
}


template <Dimension DIM>
inline void Grid<DIM>::IOGetBFaceIds( vector<MGSize>& tabid, const MGSize& type, const MGSize& id) const
{
	ASSERT( type == DIM);
	ASSERT( tabid.size() == type);

	for ( MGSize k=0; k<DIM; ++k)
		tabid[k] = mtabBFace[id].first.cElem(k);
}

template <Dimension DIM>
inline void Grid<DIM>::IOGetBFaceSurf(MGSize& idsurf, const MGSize& type, const MGSize& id) const
{
	ASSERT( type == DIM);
	idsurf = mtabBFace[id].second;
}


//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class WallDistCalc : public IO::SolFacade
{
public:
	typedef Vect<DIM>				GVect;


	WallDistCalc( const Grid<DIM>& grid, const vector<MGSize>& tabf ) : mGrid(grid), mtabSurfId(tabf)	{}

	void	DoCalc();


	virtual Dimension	Dim() const		{ return DIM;}

	virtual void	IOSetName( const MGString& name)							{ mGrdName = name;}
	virtual void	IOGetName( MGString& name) const							{ name = mGrdName;}

	virtual MGSize	IOSize() const								{ return mtabDist.size(); };
	virtual MGSize	IOBlockSize() const							{ return 1; }

	virtual void	IOResize( const MGSize& n)					{ mtabDist.resize( n); }

	virtual void	IOGetBlockVector( vector<MGFloat>& tabx, const MGSize& id) const;
	virtual void	IOSetBlockVector( const vector<MGFloat>& tabx, const MGSize& id );

	virtual void	DimToUndim( vector<MGFloat>& tab ) const	{}
	virtual void	UndimToDim( vector<MGFloat>& tab ) const	{}

private:
	MGFloat			FindDistance( const Key<DIM>& key, const GVect& vnod);

	MGFloat			VertexDistance( const GVect& v, const GVect& vn)			{ return (v-vn).module(); }
	MGFloat			EdgeDistance( const GVect& v1, const GVect& v2, const GVect& vn);
	MGFloat			FaceDistance( const GVect& v1, const GVect& v2, const GVect& v3, const GVect& vn);

private:
	string				mGrdName;
	const Grid<DIM>&	mGrid;

	vector<MGSize>		mtabSurfId;
	vector<MGFloat>		mtabDist;
};


template <Geom::Dimension DIM>
inline void WallDistCalc<DIM>::IOGetBlockVector( vector<MGFloat>& tabx, const MGSize& id) const
{
	if ( tabx.size() != 1)
		THROW_INTERNAL( "WallDistCalc::IOGetBlockVector - tabx.size() != ESIZE");

	tabx[0] = mtabDist[id];
}


template <Geom::Dimension DIM>
inline void WallDistCalc<DIM>::IOSetBlockVector( const vector<MGFloat>& tabx, const MGSize& id )
{
	if ( tabx.size() != 1)
		THROW_INTERNAL( "WallDistCalc::IOSetBlockVector - tabx.size() != ESIZE");

	mtabDist[id] = tabx[0];
}



template <Geom::Dimension DIM>
MGFloat WallDistCalc<DIM>::EdgeDistance( const GVect& v1, const GVect& v2, const GVect& vn)
{
	if ( (vn - v1) * (v2 - v1) < 0.0 )
		return VertexDistance( v1, vn);

	if ( (vn - v2) * (v1 - v2) < 0.0 )
		return VertexDistance( v2, vn);

	GVect a = vn - v1;
	GVect b = v2 - v1;

	GVect d = a - (a*b)/(b*b) * b;

	return d.module();
}


template <Geom::Dimension DIM>
MGFloat WallDistCalc<DIM>::FaceDistance( const GVect& v1, const GVect& v2, const GVect& v3, const GVect& vn)
{
	THROW_INTERNAL( "Not implemented");
}

template <>
MGFloat WallDistCalc<DIM_3D>::FaceDistance( const GVect& v1, const GVect& v2, const GVect& v3, const GVect& vn)
{
	typedef HFGeom::GVector<MGFloat,3> Vector;

	HFGeom::SubSimplex<MGFloat,0,3> suba( HFGeom::SubSimplex<MGFloat,0,3>::ARRAY( Vector(  vn.cX(), vn.cY(), vn.cZ()) ) );	
	HFGeom::SubSimplex<MGFloat,2,3> subb( HFGeom::SubSimplex<MGFloat,2,3>::ARRAY( Vector(  v1.cX(), v1.cY(), v1.cZ()), Vector(  v2.cX(), v2.cY(), v2.cZ()), Vector(  v3.cX(), v3.cY(), v3.cZ())) );	

	HFGeom::IntersectionProx<MGFloat,MGFloat,0,2,3> inter( suba, subb);
	inter.Execute();

	MGFloat	tabB[3];
	inter.GetBBaryCoords( tabB);

	bool bin = true;
	for ( MGSize i=0; i<3; ++i)
		if ( tabB[i] > 0.0 )
			bin = false;

	if ( bin) 
	{
		HFGeom::Rational<MGFloat> err = inter.Error2();
		return err.cNumer() / err.cDenom();
		//err.Simplify();
	}

	return -1.0;

}


template <Geom::Dimension DIM>
MGFloat WallDistCalc<DIM>::FindDistance( const Key<DIM>& key, const GVect& vnod)
{
	THROW_INTERNAL( "Not implemented");
}


template <>
MGFloat WallDistCalc<DIM_2D>::FindDistance( const Key<DIM_2D>& key, const GVect& vnod)
{
	GVect v1 = mGrid.mtabNode[ key.cElem(0) ];
	GVect v2 = mGrid.mtabNode[ key.cElem(1) ];

	return EdgeDistance( v1, v2, vnod);
}


template <>
MGFloat WallDistCalc<DIM_3D>::FindDistance( const Key<DIM_3D>& key, const GVect& vnod)
{
	vector<MGFloat> tabd;
	GVect v1 = mGrid.mtabNode[ key.cElem(0) ];
	GVect v2 = mGrid.mtabNode[ key.cElem(1) ];
	GVect v3 = mGrid.mtabNode[ key.cElem(2) ];

	//tabd.push_back( (v1 - vnod).module() );
	//tabd.push_back( (v2 - vnod).module() );
	//tabd.push_back( (v3 - vnod).module() );

	tabd.push_back( EdgeDistance( v1, v2, vnod) );
	tabd.push_back( EdgeDistance( v2, v3, vnod) );
	tabd.push_back( EdgeDistance( v3, v1, vnod) );

	MGFloat fdist = FaceDistance( v1, v2, v3, vnod);
	if ( fdist > 0.0 )
		tabd.push_back( fdist );


	sort( tabd.begin(), tabd.end() );
	return tabd[0];
}


template <Dimension DIM>
void WallDistCalc<DIM>::DoCalc()
{
	mtabDist.resize( mGrid.mtabNode.size(), 0.0 );


	sort( mtabSurfId.begin(), mtabSurfId.end() );

	ProgressBar	bar(60);

	int nthreads = omp_get_max_threads();//omp_get_num_threads();

	bar.Init( mGrid.mtabNode.size() / nthreads );
	bar.PrintReferenceBar();
	bar.Start();

	#pragma omp parallel for
	for ( int inod=0; inod<mGrid.mtabNode.size(); ++inod)
	//for ( MGSize inod=0; inod<mGrid.mtabNode.size(); ++inod, ++bar)
	{

		GVect vnod = mGrid.mtabNode[inod];
		MGFloat distmin = -1.0;

		for ( MGSize ifac=0; ifac<mGrid.mtabBFace.size(); ++ifac)
		{
			if ( binary_search( mtabSurfId.begin(), mtabSurfId.end(), mGrid.mtabBFace[ifac].second ) )
			{
				MGFloat dist = FindDistance( mGrid.mtabBFace[ifac].first, vnod);

				if ( dist < distmin || distmin < 0.0 )
					distmin = dist;
			}
		}

		mtabDist[inod] = distmin;

		int th_id = omp_get_thread_num();
		if ( th_id == 0 )
			++bar;
	}

	bar.Finish();

}


//////////////////////////////////////////////////////////////////////
int main( int argc, char* argv[])
{
	try
	{
		cout << "ver.: " << VERSION_NUMBER << "  build: " << VERSION_DATE << " " << VERSION_TIME << endl << endl;
		//---------------------------------------------------------------------
	
		if ( argc < 4)
		{
			printf( "call:\txwalldist [2D|3D] [msh2 name] [wall flag list]\n");
			return 1;
		}


		MGString fname = argv[2];
		bool bBIN = false;
		MGString dauxname;

		RegExp	rex;
		rex.InitPattern( "^([^\\.]*)\\.msh2$" );
		rex.SetString( fname);

		if ( rex.IsOk() )
		{
			istringstream is;

			cout << rex.GetSubString( 1) << endl;
			dauxname = rex.GetSubString( 1) + MGString( ".daux");
		}
		else
			THROW_INTERNAL( "not a .msh2 name");

		vector<MGSize> tabflag;

		for ( MGSize i=3; i<(MGSize)argc; ++i)
		{
			MGSize flag;
			istringstream is;
			is.str( argv[i] );
			is >> flag;
			tabflag.push_back( flag);
		}

		for ( MGSize i=0;i<tabflag.size(); ++i)
			cout << tabflag[i] << " ";
		cout << endl;

		bBIN = IO::ReadMSH2::IsBin( fname);
		//Dimension dim = IO::ReadMSH2::ReadDim( name, bBin);

		if ( MGString(argv[1]) == MGString("2D") )
		{

			Grid<DIM_2D>	grid2d;

			IO::ReadMSH2	reader( grid2d);
			reader.DoRead( fname, bBIN);

			WallDistCalc<DIM_2D> wallcalc( grid2d, tabflag);
			wallcalc.DoCalc();

			IO::WriteSOL	wsol( wallcalc, fname);
			wsol.DoWrite( dauxname, bBIN);

			IO::WriteTEC	wtec( grid2d, &wallcalc );
			wtec.DoWrite( "wallout.dat");
		}
		else
		if ( MGString(argv[1]) == MGString("3D") )
		{
			Grid<DIM_3D>	grid3d;

			IO::ReadMSH2	reader( grid3d);
			reader.DoRead( fname, bBIN);

			WallDistCalc<DIM_3D> wallcalc( grid3d, tabflag);
			wallcalc.DoCalc();

			IO::WriteSOL	wsol( wallcalc, fname);
			wsol.DoWrite( dauxname, bBIN);

			IO::WriteTEC	wtec( grid3d, &wallcalc );
			wtec.DoWrite( "wallout.dat");
		}
		else
		{
			printf( "call:\txwalldist [2D|3D] [msh2 name] [wall flag list]\n");
			return 1;
		}

		//---------------------------------------------------------------------

		return 0;


	}
	catch ( EHandler::Except& e)
	{
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);
	}
	catch( exception &e) 
	{
		TRACE( e.what() );
		cout << endl << "System exception: " << e.what() << endl;
	}
	catch( ...) 
	{
		TRACE( "Unknown exception thrown" );
		cout << endl << "Unknown exception thrown" << endl;
	}

	return 0;
} 

