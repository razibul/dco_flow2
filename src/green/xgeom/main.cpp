
#pragma warning(disable: 4800)	// warning C4800: �int� : forcing value to bool �true� or �false� (performance warning)
#include <mpirxx.h>
#pragma warning(default: 4800)


//#include <ttmath.h>

#include "mgdecl.h"
#include "smatrix.h"
#include "hf_gvector.h"
#include "hf_gmatrix.h"
#include "hf_initmatrix.h"

#include "hf_determinant.h"
#include "hf_ludecomp.h"
#include "hf_qrdecomp.h"
#include "hf_svdecomp.h"

#include "test_determinant.h"
#include "test_ludecomp.h"
#include "test_qrdecomp.h"
#include "test_svdecomp.h"


#include "stopwatch.h"



INIT_VERSION("0.1","");

//typedef ttmath::Int<5>		BigInt;
//typedef ttmath::Big<1,2>	BigFloat; 
//
//
//BigFloat sqrt( const BigFloat& a)	{ BigFloat b = a; return b.Sqrt(); }







int main( int argc, char* argv[])
{
	try
	{
		cout << "ver.: " << VERSION_NUMBER << "  build: " << VERSION_DATE << " " << VERSION_TIME << endl << endl;
		///////////////////////////////////////////////////////////////////////
		
		mpf_set_default_prec( 180);
	
		/*
		cout << "BigInt = " << sizeof( BigInt ) << endl;

		BigInt a,b;
		
		a = 123456;		// conversion from int
		b = "98767878";	// conversion from 'const char *'
	
		SimpleCalculating_INT(a,b); 


		cout << endl << "BigFloat = " << sizeof( BigFloat ) << endl;

		BigFloat c,d;
		
		c = "123456.543456";	// conversion from 'const char *'
		d = "98767878.124322";	// conversion from 'const char *'
	 
		SimpleCalculating_FLOAT(c,d); 

		return 0;

		long long uu = 1234LL;

		cout << "long long = " << sizeof( long long) << endl << endl;
		*/

		///////////////////////////////////////////////////////////////////////
		/*
		//typedef HFGeom::GMatrix<12,mpz_class>	MTX;
		//typedef HFGeom::GMatrix<12,mpf_class>	MTX;
		typedef HFGeom::GMatrix<12,long long>	MTX;

		MTX mtxtmp;

		HFGeom::InitMatrix< MTX::TYPE, MTX >::Random( mtxtmp, 3, -100, 100);

		mtxtmp.Write( 10);

		HFGeom::Determinant< MTX::TYPE, MTX >	det( mtxtmp);
		det.Execute();
		cout << det.GetDeterminant() << endl;

		MTX::TYPE tt = det.DeterminantNxN();

		cout << tt << endl;
		*/

		////////////////
		typedef HFGeom::GMatrix<12,mpz_class>	MTX;
		//typedef HFGeom::GMatrix<12,BigInt>	MTX;

		MTX mtxtmp;
		HFGeom::InitMatrix< MTX::TYPE, MTX >::Random( mtxtmp, 6, -100, 100);

		MGSize count = 100000;
		MTX::TYPE res = 0;

		StopWatch	swatch;

		HFGeom::Determinant< MTX::TYPE, MTX >	det( mtxtmp);

		swatch.Start();

		for ( MGSize i=0; i<count; ++i)
		{
			det.Execute();
			res += det.GetDeterminant(); 
		}

		swatch.Mark();

		cout << "count = " << count << " time = " << swatch.cDuration() << " -- res = " << setprecision(50) << res << endl;

		return 0;

		///////////////////////////////////////////////////////////////////////

		HFGeom::Test_Determinant	tdet;

		tdet.CheckDeterminant_Random<mpf_class,6>( 5, -10000, 10000);
		tdet.CheckDeterminant_Hilbert<mpf_class,6>( 5);

		return 0;




		MTX	mtxA;

		cout << sizeof( MTX::TYPE ) << endl;
		
		////mtxA.Write( 10);

		////HFGeom::InitMatrix< MTX::TYPE, MTX >::Diagonal( mtxA, 5, 2);
		////mtxA.Write( 10);

		////HFGeom::InitMatrix< MTX::TYPE, MTX >::Hilbert( mtxA, 3);
		//HFGeom::InitMatrix< MTX::TYPE, MTX >::Random( mtxA, 5, -5000, 5000);
		//mtxA.Write( 10);

		//HFGeom::Determinant< MTX::TYPE, MTX >	det( mtxA);
		//HFGeom::QRDecomp< MTX::TYPE, MTX >		qrdec( mtxA);

		//det.Execute();
		//qrdec.Execute();

		//cout << "det (DET) = " << setprecision(50) << det.GetDeterminant() << endl;
		//cout << "det (QR)  = " << setprecision(50) << qrdec.GetDeterminant() << endl;



		//HFGeom::InitMatrix< MGFloat, HFGeom::GMatrix<12,MGFloat> >::Random( mtxA, 5, -3, 10);
		//mtxA.Write( 10);


		return 0;
		///////////////////////////////////////////////////////////////////////


		MGSize nnn = 20;


		HFGeom::Test_QRDecomp	qrd;
		HFGeom::Test_LUDecomp	lud;


		for ( MGSize i=2; i<=nnn; ++i)
		{
			cout << endl << "size = " << i << endl;

			//qrd.Exper2<double,36>( 15);
			//qrd.CheckDecomposition<double,36>( i, i);
			qrd.CheckDeterminant<double,36>( i);
			lud.CheckDeterminant<double,36>( i);
			//qrd.CheckDeterminant<mpf_class,36>( i);

			qrd.CheckDeterminant<mpf_class,36>( i);
			lud.CheckDeterminant<mpf_class,36>( i);
		}

		//qrd.CheckDeterminant<mpf_class,36>( nnn);

		//qrd.CheckSolve<mpf_class,36>( nnn);

		//qrd.Exper<double,16>( 5, 3);
		//qrd.Exper<mpf_class,16>( 5, 3);



		//testLUD_.CheckDeterminant<mpf_class,36>( nnn);

		//testLUD_.CheckSolve<mpf_class,36>( nnn);

		return 0;


	

		//HFGeom::Test_SVDecomp	testSVD;

		//testSVD.CheckDecomposition<double,16>( 6, 4);


		//return 0;

		HFGeom::Test_QRDecomp	testQRD;

		//testQRD.CheckDecomposition<double,16>( 5, 3);
		//return 0;


		testQRD.CheckDecomposition<float,16>( 16, 10);
		testQRD.CheckDecomposition<double,16>( 16, 10);
		testQRD.CheckDecomposition<mpf_class,16>( 16, 10);
		cout << endl;


		HFGeom::Test_LUDecomp	testLUD;

		testLUD.CheckSolve<float,16>( 9);
		testLUD.CheckSolve<double,16>( 9);
		testLUD.CheckSolve<mpf_class,16>( 9);
		cout << endl;

		testLUD.CheckDeterminant<float,16>( 9);
		testLUD.CheckDeterminant<double,16>( 9);
		testLUD.CheckDeterminant<mpf_class,16>( 9);
		cout << endl;

		return 0;


		////////////////////////////////////////////
		// MPIR test

		mpf_t ap_a, ap_res;
		mpf_t ap_tmp, ap_coeff;

		mpf_init( ap_a);
		mpf_init( ap_res);
		mpf_init( ap_tmp);
		mpf_init( ap_coeff);

		mpf_init_set_d( ap_a, 1.0);
		mpf_init_set_d( ap_coeff, 6.0);

		mpf_div( ap_res, ap_a, ap_coeff);

		gmp_printf ("1/3 = %.200Ff\n", ap_res);

		////////////////////////////////////////////
		// GVector test

		HFGeom::GVector<3,MGFloat>	dv1;
		HFGeom::GVector<3,MGSize>	iv1;


		mpf_class hc = mpf_class(2) / mpf_class(3);

		HFGeom::GVector<3,mpf_class>	hv1( 0, 1, 2);
		HFGeom::GVector<3,mpf_class>	hv2( 2, 5, 7);
		HFGeom::GVector<3,mpf_class>	hvres = hv1 + hv2 * hc;

		hvres.Write( 50);

		cout << hv1 * hv2 << " " << sqrt( hv1 * hv2 ) << endl;

		////////////////////////////////////////////
		// GMatrix test

		HFGeom::GMatrix<3,MGFloat>	dmtx1(2,3,1.5);
		HFGeom::GMatrix<3,MGSize>	imtx1(3,2,11);

		dmtx1.Write( 100);
		imtx1.Write( 100);


		return 0;
	}
	catch ( EHandler::Except& e)
	{
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);
	}
	catch( exception &e) 
	{
		TRACE( e.what() );
		cout << endl << "System exception: " << e.what() << endl;
	}
	catch( ...) 
	{
		TRACE( "Unknown exception thrown" );
		cout << endl << "Unknown exception thrown" << endl;
	}

	return 0;
} 

