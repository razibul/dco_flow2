#ifndef __HF_TEST_SVDECOMP_H__
#define __HF_TEST_SVDECOMP_H__


#include "mgdecl.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace HFGeomTools {


class Test_SVDecomp
{
public:
	bool	Run();

//private:

	template <class T, MGSize NMAX>
	bool CheckDecomposition( const MGSize& nrow, const MGSize& ncol);

};


template <class T, MGSize NMAX>
bool Test_SVDecomp::CheckDecomposition( const MGSize& nrow, const MGSize& ncol)
{
	typedef HFGeom::GMatrix<NMAX,T> MTX;

	if ( max(nrow, ncol) > NMAX)
		THROW_INTERNAL( "Test_QRDecomp : nrow or ncol is greater then " << NMAX );

	MTX	mtx(nrow,ncol);

	for ( MGSize i=0; i<nrow; ++i)
		for ( MGSize j=0; j<ncol; ++j)
			mtx(i,j) = T(1) / T(i+j+1.);

	HFGeom::SVDecomp< T, MTX >	qrd( mtx);

	qrd.Execute();

	return true;
}


} // end of namespace HFGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace HFGeom = HFGeomTools;



#endif // __HF_TEST_SVDECOMP_H__  
