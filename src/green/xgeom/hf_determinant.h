#ifndef __HF_DETERMINANT_H__
#define __HF_DETERMINANT_H__


#include "mgdecl.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace HFGeomTools {

//////////////////////////////////////////////////////////////////////
// class Determinant
//////////////////////////////////////////////////////////////////////
template< class T, class MATRIX>
class Determinant
{
public:
	typedef MGSize	size_type;

	Determinant( const MATRIX& mtx);

	void	Execute();

	T	DeterminantNxN();

	T	GetDeterminant()	{ return mValue;}
	T	Error();

private:
	T	Determinant2x2() const;
	T	Determinant3x3() const;
	T	Determinant4x4() const;
	T	Determinant5x5() const;
	T	Determinant6x6() const;

private:
	MATRIX		mA;
	size_type	mN;
	T			mValue;
};
//////////////////////////////////////////////////////////////////////


template< class T, class MATRIX>
Determinant<T,MATRIX>::Determinant( const MATRIX& mtx) : mA(mtx), mN(mtx.NRows()), mValue(0)
{
	if ( mtx.NRows() != mtx.NCols() )
		THROW_INTERNAL( "Determinant : Determinant : n_rows must be equal n_cols");
}


template< class T, class MATRIX>
inline void Determinant< T, MATRIX>::Execute()
{
	switch ( mA.NRows() )
	{
	case 2:
		mValue = Determinant2x2();
		return;

	case 3:
		mValue = Determinant3x3();
		return;

	case 4:
		mValue = Determinant4x4();
		return;

	case 5:
		mValue = Determinant5x5();
		return;

	case 6:
		mValue = Determinant6x6();
		return;
	}

	THROW_INTERNAL( "not implemented");

}


template< class T, class MATRIX>
T Determinant<T,MATRIX>::DeterminantNxN() 
{
	T					sign;
	vector<size_type>	tabPivot(mN);

	sign = 1;
	for ( size_type i=0; i<mN; ++i) 
		tabPivot[i] = i;

	for ( size_type j=0; j<mN; ++j)
	{
		//// find pivot
		//size_type p = j;
		//for ( size_type i=j+1; i<mN; ++i) 
		//{
		//	if ( abs( mA(i,j)) > abs( mA(p,j)) )
		//		p = i;
		//}

		//// swap rows if necessary
		//if ( p != j)
		//{
		//	for ( size_type k=0; k<mN; ++k)
		//	{
		//		T tmp = mA(p,k);
		//		mA(p,k) = mA(j,k);
		//		mA(j,k) = tmp;
		//	}

		//	sign = - sign;
		//}

		//tabPivot[j] = p;

		if ( mA(j,j) == 0 )
		{
			return T(0);
			//THROW_INTERNAL( "Determinant : singular matrix" );
		}

		// carry on with elimination
		for ( size_type i=j+1; i<mN; ++i)
		{
			//T tmp = mA(i,j) /= mA(j,j);
			T tmp = mA(i,j);
			for ( size_type k=j+1; k<mN; ++k)
				mA(i,k) = mA(i,k) * mA(j,j) - tmp * mA(j,k);
		}

	}


	return mA(mN-1,mN-1);

	//mA.Write();

	//for ( size_type i=0; i<mN; ++i)
	//	cout << mtabPivot[i] << " ";
	//cout << endl;
}


template< class T, class MATRIX>
inline T Determinant< T, MATRIX>::Determinant2x2() const
{
	return	mA(0,0)*mA(1,1) - mA(0,1)*mA(1,0) ;
}


template< class T, class MATRIX>
inline T Determinant< T, MATRIX>::Determinant3x3() const
{
	return	mA(0,0)*( mA(1,1)*mA(2,2) - mA(1,2)*mA(2,1) ) -
			mA(0,1)*( mA(1,0)*mA(2,2) - mA(1,2)*mA(2,0) ) +
			mA(0,2)*( mA(1,0)*mA(2,1) - mA(1,1)*mA(2,0) );

}


template< class T, class MATRIX>
inline T Determinant< T, MATRIX>::Determinant4x4() const
{
	T det2_23_01 = mA(2,0)*mA(3,1) - mA(2,1)*mA(3,0);
	T det2_23_02 = mA(2,0)*mA(3,2) - mA(2,2)*mA(3,0);
	T det2_23_03 = mA(2,0)*mA(3,3) - mA(2,3)*mA(3,0);
	T det2_23_12 = mA(2,1)*mA(3,2) - mA(2,2)*mA(3,1);
	T det2_23_13 = mA(2,1)*mA(3,3) - mA(2,3)*mA(3,1);
	T det2_23_23 = mA(2,2)*mA(3,3) - mA(2,3)*mA(3,2);

	T det3_123_012 = mA(1,0)*det2_23_12 - mA(1,1)*det2_23_02 + mA(1,2)*det2_23_01;
	T det3_123_013 = mA(1,0)*det2_23_13 - mA(1,1)*det2_23_03 + mA(1,3)*det2_23_01;
	T det3_123_023 = mA(1,0)*det2_23_23 - mA(1,2)*det2_23_03 + mA(1,3)*det2_23_02;
	T det3_123_123 = mA(1,1)*det2_23_23 - mA(1,2)*det2_23_13 + mA(1,3)*det2_23_12;

	return mA(0,0)*det3_123_123 - mA(0,1)*det3_123_023 + mA(0,2)*det3_123_013 - mA(0,3)*det3_123_012;
}


template< class T, class MATRIX>
inline T Determinant< T, MATRIX>::Determinant5x5() const
{
	T det2_34_01 = mA(3,0)*mA(4,1) - mA(3,1)*mA(4,0);
	T det2_34_02 = mA(3,0)*mA(4,2) - mA(3,2)*mA(4,0);
	T det2_34_03 = mA(3,0)*mA(4,3) - mA(3,3)*mA(4,0);
	T det2_34_04 = mA(3,0)*mA(4,4) - mA(3,4)*mA(4,0);
	T det2_34_12 = mA(3,1)*mA(4,2) - mA(3,2)*mA(4,1);
	T det2_34_13 = mA(3,1)*mA(4,3) - mA(3,3)*mA(4,1);
	T det2_34_14 = mA(3,1)*mA(4,4) - mA(3,4)*mA(4,1);
	T det2_34_23 = mA(3,2)*mA(4,3) - mA(3,3)*mA(4,2);
	T det2_34_24 = mA(3,2)*mA(4,4) - mA(3,4)*mA(4,2);
	T det2_34_34 = mA(3,3)*mA(4,4) - mA(3,4)*mA(4,3);

	T det3_234_012 = mA(2,0)*det2_34_12 - mA(2,1)*det2_34_02  + mA(2,2)*det2_34_01;
	T det3_234_013 = mA(2,0)*det2_34_13 - mA(2,1)*det2_34_03  + mA(2,3)*det2_34_01;
	T det3_234_014 = mA(2,0)*det2_34_14 - mA(2,1)*det2_34_04  + mA(2,4)*det2_34_01;
	T det3_234_023 = mA(2,0)*det2_34_23 - mA(2,2)*det2_34_03  + mA(2,3)*det2_34_02;
	T det3_234_024 = mA(2,0)*det2_34_24 - mA(2,2)*det2_34_04  + mA(2,4)*det2_34_02;
	T det3_234_034 = mA(2,0)*det2_34_34 - mA(2,3)*det2_34_04  + mA(2,4)*det2_34_03;
	T det3_234_123 = mA(2,1)*det2_34_23 - mA(2,2)*det2_34_13  + mA(2,3)*det2_34_12;
	T det3_234_124 = mA(2,1)*det2_34_24 - mA(2,2)*det2_34_14  + mA(2,4)*det2_34_12;
	T det3_234_134 = mA(2,1)*det2_34_34 - mA(2,3)*det2_34_14  + mA(2,4)*det2_34_13;
	T det3_234_234 = mA(2,2)*det2_34_34 - mA(2,3)*det2_34_24  + mA(2,4)*det2_34_23;

	T det4_1234_0123 = mA(1,0)*det3_234_123 - mA(1,1)*det3_234_023  + mA(1,2)*det3_234_013 - mA(1,3)*det3_234_012;
	T det4_1234_0124 = mA(1,0)*det3_234_124 - mA(1,1)*det3_234_024  + mA(1,2)*det3_234_014 - mA(1,4)*det3_234_012;
	T det4_1234_0134 = mA(1,0)*det3_234_134 - mA(1,1)*det3_234_034  + mA(1,3)*det3_234_014 - mA(1,4)*det3_234_013;
	T det4_1234_0234 = mA(1,0)*det3_234_234 - mA(1,2)*det3_234_034  + mA(1,3)*det3_234_024 - mA(1,4)*det3_234_023;
	T det4_1234_1234 = mA(1,1)*det3_234_234 - mA(1,2)*det3_234_134  + mA(1,3)*det3_234_124 - mA(1,4)*det3_234_123;

	return mA(0,0)*det4_1234_1234 - mA(0,1)*det4_1234_0234 + mA(0,2)*det4_1234_0134 - mA(0,3)*det4_1234_0124 + mA(0,4)*det4_1234_0123;
}



template< class T, class MATRIX>
inline T Determinant< T, MATRIX>::Determinant6x6() const
{
	T det2_45_34 = mA(4,3)*mA(5,4) - mA(4,4)*mA(5,3);
	T det2_45_24 = mA(4,2)*mA(5,4) - mA(4,4)*mA(5,2);
	T det2_45_23 = mA(4,2)*mA(5,3) - mA(4,3)*mA(5,2);
	T det2_45_14 = mA(4,1)*mA(5,4) - mA(4,4)*mA(5,1);
	T det2_45_13 = mA(4,1)*mA(5,3) - mA(4,3)*mA(5,1);
	T det2_45_12 = mA(4,1)*mA(5,2) - mA(4,2)*mA(5,1);
	T det2_45_04 = mA(4,0)*mA(5,4) - mA(4,4)*mA(5,0);
	T det2_45_03 = mA(4,0)*mA(5,3) - mA(4,3)*mA(5,0);
	T det2_45_02 = mA(4,0)*mA(5,2) - mA(4,2)*mA(5,0);
	T det2_45_01 = mA(4,0)*mA(5,1) - mA(4,1)*mA(5,0);
	T det2_45_35 = mA(4,3)*mA(5,5) - mA(4,5)*mA(5,3);
	T det2_45_25 = mA(4,2)*mA(5,5) - mA(4,5)*mA(5,2);
	T det2_45_15 = mA(4,1)*mA(5,5) - mA(4,5)*mA(5,1);
	T det2_45_05 = mA(4,0)*mA(5,5) - mA(4,5)*mA(5,0);
	T det2_45_45 = mA(4,4)*mA(5,5) - mA(4,5)*mA(5,4);
	
	T det3_345_234 = mA(3,2)*det2_45_34 - mA(3,3)*det2_45_24  + mA(3,4)*det2_45_23;
	T det3_345_134 = mA(3,1)*det2_45_34 - mA(3,3)*det2_45_14  + mA(3,4)*det2_45_13;
	T det3_345_124 = mA(3,1)*det2_45_24 - mA(3,2)*det2_45_14  + mA(3,4)*det2_45_12;
	T det3_345_123 = mA(3,1)*det2_45_23 - mA(3,2)*det2_45_13  + mA(3,3)*det2_45_12;
	T det3_345_034 = mA(3,0)*det2_45_34 - mA(3,3)*det2_45_04  + mA(3,4)*det2_45_03;
	T det3_345_024 = mA(3,0)*det2_45_24 - mA(3,2)*det2_45_04  + mA(3,4)*det2_45_02;
	T det3_345_023 = mA(3,0)*det2_45_23 - mA(3,2)*det2_45_03  + mA(3,3)*det2_45_02;
	T det3_345_014 = mA(3,0)*det2_45_14 - mA(3,1)*det2_45_04  + mA(3,4)*det2_45_01;
	T det3_345_013 = mA(3,0)*det2_45_13 - mA(3,1)*det2_45_03  + mA(3,3)*det2_45_01;
	T det3_345_012 = mA(3,0)*det2_45_12 - mA(3,1)*det2_45_02  + mA(3,2)*det2_45_01;
	T det3_345_235 = mA(3,2)*det2_45_35 - mA(3,3)*det2_45_25  + mA(3,5)*det2_45_23;
	T det3_345_135 = mA(3,1)*det2_45_35 - mA(3,3)*det2_45_15  + mA(3,5)*det2_45_13;
	T det3_345_125 = mA(3,1)*det2_45_25 - mA(3,2)*det2_45_15  + mA(3,5)*det2_45_12;
	T det3_345_035 = mA(3,0)*det2_45_35 - mA(3,3)*det2_45_05  + mA(3,5)*det2_45_03;
	T det3_345_025 = mA(3,0)*det2_45_25 - mA(3,2)*det2_45_05  + mA(3,5)*det2_45_02;
	T det3_345_015 = mA(3,0)*det2_45_15 - mA(3,1)*det2_45_05  + mA(3,5)*det2_45_01;
	T det3_345_245 = mA(3,2)*det2_45_45 - mA(3,4)*det2_45_25  + mA(3,5)*det2_45_24;
	T det3_345_145 = mA(3,1)*det2_45_45 - mA(3,4)*det2_45_15  + mA(3,5)*det2_45_14;
	T det3_345_045 = mA(3,0)*det2_45_45 - mA(3,4)*det2_45_05  + mA(3,5)*det2_45_04;
	T det3_345_345 = mA(3,3)*det2_45_45 - mA(3,4)*det2_45_35  + mA(3,5)*det2_45_34;

	T det4_2345_1234 = mA(2,1)*det3_345_234 - mA(2,2)*det3_345_134  + mA(2,3)*det3_345_124 - mA(2,4)*det3_345_123;
	T det4_2345_0234 = mA(2,0)*det3_345_234 - mA(2,2)*det3_345_034  + mA(2,3)*det3_345_024 - mA(2,4)*det3_345_023;
	T det4_2345_0134 = mA(2,0)*det3_345_134 - mA(2,1)*det3_345_034  + mA(2,3)*det3_345_014 - mA(2,4)*det3_345_013;
	T det4_2345_0124 = mA(2,0)*det3_345_124 - mA(2,1)*det3_345_024  + mA(2,2)*det3_345_014 - mA(2,4)*det3_345_012;
	T det4_2345_0123 = mA(2,0)*det3_345_123 - mA(2,1)*det3_345_023  + mA(2,2)*det3_345_013 - mA(2,3)*det3_345_012;
	T det4_2345_1235 = mA(2,1)*det3_345_235 - mA(2,2)*det3_345_135  + mA(2,3)*det3_345_125 - mA(2,5)*det3_345_123;
	T det4_2345_0235 = mA(2,0)*det3_345_235 - mA(2,2)*det3_345_035  + mA(2,3)*det3_345_025 - mA(2,5)*det3_345_023;
	T det4_2345_0135 = mA(2,0)*det3_345_135 - mA(2,1)*det3_345_035  + mA(2,3)*det3_345_015 - mA(2,5)*det3_345_013;
	T det4_2345_0125 = mA(2,0)*det3_345_125 - mA(2,1)*det3_345_025  + mA(2,2)*det3_345_015 - mA(2,5)*det3_345_012;
	T det4_2345_1245 = mA(2,1)*det3_345_245 - mA(2,2)*det3_345_145  + mA(2,4)*det3_345_125 - mA(2,5)*det3_345_124;
	T det4_2345_0245 = mA(2,0)*det3_345_245 - mA(2,2)*det3_345_045  + mA(2,4)*det3_345_025 - mA(2,5)*det3_345_024;
	T det4_2345_0145 = mA(2,0)*det3_345_145 - mA(2,1)*det3_345_045  + mA(2,4)*det3_345_015 - mA(2,5)*det3_345_014;
	T det4_2345_1345 = mA(2,1)*det3_345_345 - mA(2,3)*det3_345_145  + mA(2,4)*det3_345_135 - mA(2,5)*det3_345_134;
	T det4_2345_0345 = mA(2,0)*det3_345_345 - mA(2,3)*det3_345_045  + mA(2,4)*det3_345_035 - mA(2,5)*det3_345_034;
	T det4_2345_2345 = mA(2,2)*det3_345_345 - mA(2,3)*det3_345_245  + mA(2,4)*det3_345_235 - mA(2,5)*det3_345_234;

	T det5_12345_01234 = mA(1,0)*det4_2345_1234 - mA(1,1)*det4_2345_0234  + mA(1,2)*det4_2345_0134 - mA(1,3)*det4_2345_0124 + mA(1,4)*det4_2345_0123;
	T det5_12345_01235 = mA(1,0)*det4_2345_1235 - mA(1,1)*det4_2345_0235  + mA(1,2)*det4_2345_0135 - mA(1,3)*det4_2345_0125 + mA(1,5)*det4_2345_0123;
	T det5_12345_01245 = mA(1,0)*det4_2345_1245 - mA(1,1)*det4_2345_0245  + mA(1,2)*det4_2345_0145 - mA(1,4)*det4_2345_0125 + mA(1,5)*det4_2345_0124;
	T det5_12345_01345 = mA(1,0)*det4_2345_1345 - mA(1,1)*det4_2345_0345  + mA(1,3)*det4_2345_0145 - mA(1,4)*det4_2345_0135 + mA(1,5)*det4_2345_0134;
	T det5_12345_02345 = mA(1,0)*det4_2345_2345 - mA(1,2)*det4_2345_0345  + mA(1,3)*det4_2345_0245 - mA(1,4)*det4_2345_0235 + mA(1,5)*det4_2345_0234;
	T det5_12345_12345 = mA(1,1)*det4_2345_2345 - mA(1,2)*det4_2345_1345  + mA(1,3)*det4_2345_1245 - mA(1,4)*det4_2345_1235 + mA(1,5)*det4_2345_1234;

	return		mA(0,0)*det5_12345_12345 
			-	mA(0,1)*det5_12345_02345 
			+	mA(0,2)*det5_12345_01345 
			-	mA(0,3)*det5_12345_01245 
			+	mA(0,4)*det5_12345_01235
			-	mA(0,5)*det5_12345_01234;
}



} // end of namespace HFGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace HFGeom = HFGeomTools;



#endif // __HF_DETERMINANT_H__  
