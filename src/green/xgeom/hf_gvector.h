#ifndef __HF_GVECTOR_H__
#define __HF_GVECTOR_H__


#include "mgdecl.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace HFGeomTools {




template <MGSize MAX_SIZE, class ELEM_TYPE> 
class GVector;

template <MGSize MAX_SIZE, class ELEM_TYPE> 
GVector<MAX_SIZE,ELEM_TYPE> operator *( const ELEM_TYPE&,  const GVector<MAX_SIZE,ELEM_TYPE>&);

template <MGSize MAX_SIZE, class ELEM_TYPE> 
GVector<MAX_SIZE,ELEM_TYPE> operator *( const GVector<MAX_SIZE,ELEM_TYPE>&, const ELEM_TYPE&);

template <MGSize MAX_SIZE, class ELEM_TYPE> 
ELEM_TYPE	operator *( const GVector<MAX_SIZE,ELEM_TYPE>&, const GVector<MAX_SIZE,ELEM_TYPE>&);   // dot product

template <MGSize MAX_SIZE, class ELEM_TYPE> 
GVector<MAX_SIZE,ELEM_TYPE> operator +( const GVector<MAX_SIZE,ELEM_TYPE>&, const GVector<MAX_SIZE,ELEM_TYPE>&);

template <MGSize MAX_SIZE, class ELEM_TYPE> 
GVector<MAX_SIZE,ELEM_TYPE> operator -( const GVector<MAX_SIZE,ELEM_TYPE>&, const GVector<MAX_SIZE,ELEM_TYPE>&);

template <MGSize MAX_SIZE, class ELEM_TYPE> 
GVector<MAX_SIZE,ELEM_TYPE> Minimum( const GVector<MAX_SIZE,ELEM_TYPE>& vec1,  const GVector<MAX_SIZE,ELEM_TYPE>& vec2 );

template <MGSize MAX_SIZE, class ELEM_TYPE> 
GVector<MAX_SIZE,ELEM_TYPE> Maximum( const GVector<MAX_SIZE,ELEM_TYPE>& vec1,  const GVector<MAX_SIZE,ELEM_TYPE>& vec2 );



//////////////////////////////////////////////////////////////////////
//	class GVector
//////////////////////////////////////////////////////////////////////
template <MGSize MAX_SIZE, class ELEM_TYPE> 
class GVector
{
public:
	typedef ELEM_TYPE	TYPE;

	//////////////////////////////////////////////////////////////////
	// constructors
	GVector( const ELEM_TYPE& x);
	GVector( const ELEM_TYPE& x, const ELEM_TYPE& y);
	GVector( const ELEM_TYPE& x, const ELEM_TYPE& y, const ELEM_TYPE& z);
	GVector( const ELEM_TYPE& x, const ELEM_TYPE& y, const ELEM_TYPE& z, const ELEM_TYPE& w);
	GVector( const GVector<MAX_SIZE,ELEM_TYPE>& vec);
	GVector();

    //////////////////////////////////////////////////////////////////
	// declaration of friend two argument operators
	friend GVector		operator *<>( const ELEM_TYPE&,  const GVector&);
	friend GVector		operator *<>( const GVector&, const ELEM_TYPE&);
	friend ELEM_TYPE	operator *<>( const GVector&, const GVector&);   // dot product
	friend GVector		operator +<>( const GVector&, const GVector&);
	friend GVector		operator -<>( const GVector&, const GVector&);

	friend GVector		Minimum<>( const GVector&, const GVector& );
	friend GVector		Maximum<>( const GVector&, const GVector& );


    //////////////////////////////////////////////////////////////////
	// one argument operators
	GVector<MAX_SIZE,ELEM_TYPE>&	operator  =( const GVector<MAX_SIZE,ELEM_TYPE> &vec);
	GVector<MAX_SIZE,ELEM_TYPE>&	operator +=( const GVector<MAX_SIZE,ELEM_TYPE>&);
	GVector<MAX_SIZE,ELEM_TYPE>&	operator -=( const GVector<MAX_SIZE,ELEM_TYPE>&);
	GVector<MAX_SIZE,ELEM_TYPE>&	operator *=( const ELEM_TYPE&);

	const ELEM_TYPE&	cX( const MGSize& i) const	{ return mtab[i];}
	ELEM_TYPE&			rX( const MGSize& i)		{ return mtab[i];}

	const ELEM_TYPE&	cX() const		{ return mtab[0];}
	const ELEM_TYPE&	cY() const		{ ASSERT(MAX_SIZE>1); return mtab[1];}
	const ELEM_TYPE&	cZ() const		{ ASSERT(MAX_SIZE>2); return mtab[2];}
	const ELEM_TYPE&	cW() const		{ ASSERT(MAX_SIZE>3); return mtab[3];}

	ELEM_TYPE&			rX()			{ return mtab[0];}
	ELEM_TYPE&			rY()			{ ASSERT(MAX_SIZE>1); return mtab[1];}
	ELEM_TYPE&			rZ()			{ ASSERT(MAX_SIZE>2); return mtab[2];}
	ELEM_TYPE&			rW()			{ ASSERT(MAX_SIZE>3); return mtab[3];}

	const ELEM_TYPE*	cTab() const	{ return mtab;}

	void	Write( const MGSize& iprec, ostream& f=cout) const;

protected:
	ELEM_TYPE mtab[MAX_SIZE];
};
//////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////

template <MGSize MAX_SIZE, class ELEM_TYPE> 
inline GVector<MAX_SIZE,ELEM_TYPE>::GVector( const GVector<MAX_SIZE,ELEM_TYPE> &vec)
{
	for ( MGSize i=0; i<MAX_SIZE; ++i)
		mtab[i] = vec.mtab[i];
}

template <MGSize MAX_SIZE, class ELEM_TYPE> 
inline GVector<MAX_SIZE,ELEM_TYPE>::GVector( const ELEM_TYPE& x)
{
	for ( MGSize i=0; i<MAX_SIZE; mtab[i++]=x);
}

template <MGSize MAX_SIZE, class ELEM_TYPE> 
inline GVector<MAX_SIZE,ELEM_TYPE>::GVector( const ELEM_TYPE& x, const ELEM_TYPE& y)
{
		ASSERT( MAX_SIZE == 2);
		mtab[0] = x;
		mtab[1] = y;
}

template <MGSize MAX_SIZE, class ELEM_TYPE> 
inline GVector<MAX_SIZE,ELEM_TYPE>::GVector( const ELEM_TYPE& x, const ELEM_TYPE& y, const ELEM_TYPE& z)
{
		ASSERT( MAX_SIZE == 3);
		mtab[0] = x;
		mtab[1] = y;
		mtab[2] = z;
}

template <MGSize MAX_SIZE, class ELEM_TYPE> 
inline GVector<MAX_SIZE,ELEM_TYPE>::GVector( const ELEM_TYPE& x, const ELEM_TYPE& y, const ELEM_TYPE& z, const ELEM_TYPE& w)
{
		ASSERT( MAX_SIZE == 4);
		mtab[0] = x;
		mtab[1] = y;
		mtab[2] = z;
		mtab[3] = w;
}


template <MGSize MAX_SIZE, class ELEM_TYPE> 
inline GVector<MAX_SIZE,ELEM_TYPE>::GVector()
{
	for ( MGSize i=0; i<MAX_SIZE; mtab[i++]=(ELEM_TYPE)0.0);
}


template <MGSize MAX_SIZE, class ELEM_TYPE> 
inline GVector<MAX_SIZE,ELEM_TYPE>& GVector<MAX_SIZE,ELEM_TYPE>::operator =( const GVector<MAX_SIZE,ELEM_TYPE> &vec)
{
	for ( MGSize i=0; i<MAX_SIZE; ++i)
		mtab[i] = vec.mtab[i];
	return *this;
}

template <MGSize MAX_SIZE, class ELEM_TYPE> 
inline GVector<MAX_SIZE,ELEM_TYPE>& GVector<MAX_SIZE,ELEM_TYPE>::operator+=( const GVector<MAX_SIZE,ELEM_TYPE>& vec)
{
	for ( MGSize i=0; i<MAX_SIZE; ++i)
		mtab[i] += vec.mtab[i];
	return *this;
}


template <MGSize MAX_SIZE, class ELEM_TYPE> 
inline GVector<MAX_SIZE,ELEM_TYPE>& GVector<MAX_SIZE,ELEM_TYPE>::operator-=( const GVector<MAX_SIZE,ELEM_TYPE>& vec)
{
	for ( MGSize i=0; i<MAX_SIZE; ++i)
		mtab[i] -= vec.mtab[i];
	return *this;
}

template <MGSize MAX_SIZE, class ELEM_TYPE> 
inline GVector<MAX_SIZE,ELEM_TYPE>& GVector<MAX_SIZE,ELEM_TYPE>::operator*=( const ELEM_TYPE& doub)
{
	for ( MGSize i=0; i<MAX_SIZE; ++i)
		mtab[i] *= doub;
	return *this;
}


template <MGSize MAX_SIZE, class ELEM_TYPE> 
inline void GVector<MAX_SIZE,ELEM_TYPE>::Write( const MGSize& iprec, ostream& f) const
{
	f << "[ ";
	for( MGSize i=0; i<MAX_SIZE; i++)
		f << /*setw(12) <<*/ setprecision(iprec) << cX(i) << " ";
	f << "]\n";
}




//////////////////////////////////////////////////////////////////////
template <MGSize MAX_SIZE, class ELEM_TYPE> 
inline GVector<MAX_SIZE,ELEM_TYPE> operator-( const GVector<MAX_SIZE,ELEM_TYPE>& vec1, const GVector<MAX_SIZE,ELEM_TYPE>& vec2)
{
	GVector<MAX_SIZE,ELEM_TYPE>	v;
	for ( MGSize i=0; i<MAX_SIZE; ++i)
		v.mtab[i] = vec1.mtab[i] - vec2.mtab[i];
	return v;
}

template <MGSize MAX_SIZE, class ELEM_TYPE> 
inline GVector<MAX_SIZE,ELEM_TYPE> operator+( const GVector<MAX_SIZE,ELEM_TYPE>& vec1, const GVector<MAX_SIZE,ELEM_TYPE>& vec2)
{
	GVector<MAX_SIZE,ELEM_TYPE>	v;
	for ( MGSize i=0; i<MAX_SIZE; ++i)
		v.mtab[i] = vec1.mtab[i] + vec2.mtab[i];
	return v;
}

template <MGSize MAX_SIZE, class ELEM_TYPE> 
inline ELEM_TYPE operator*( const GVector<MAX_SIZE,ELEM_TYPE>& vec1, const GVector<MAX_SIZE,ELEM_TYPE>& vec2)
{
	ELEM_TYPE	e(0);
	for ( MGSize i=0; i<MAX_SIZE; ++i)
		e += vec1.mtab[i] * vec2.mtab[i];
	return e;
}

template <MGSize MAX_SIZE, class ELEM_TYPE> 
inline GVector<MAX_SIZE,ELEM_TYPE> operator*( const ELEM_TYPE& e, const GVector<MAX_SIZE,ELEM_TYPE>& vec)
{
	GVector<MAX_SIZE,ELEM_TYPE>	v;
	for ( MGSize i=0; i<MAX_SIZE; ++i)
		v.mtab[i] = e * vec.mtab[i];
	return v;
}

template <MGSize MAX_SIZE, class ELEM_TYPE> 
inline GVector<MAX_SIZE,ELEM_TYPE> operator*( const GVector<MAX_SIZE,ELEM_TYPE>& vec, const ELEM_TYPE& e)
{
	GVector<MAX_SIZE,ELEM_TYPE>	v;
	for ( MGSize i=0; i<MAX_SIZE; ++i)
		v.mtab[i] = e * vec.mtab[i];
	return v;
}

template <MGSize MAX_SIZE, class ELEM_TYPE> 
inline GVector<MAX_SIZE,ELEM_TYPE> Minimum(  const GVector<MAX_SIZE,ELEM_TYPE>& vec1,  const GVector<MAX_SIZE,ELEM_TYPE>& vec2 )
{
	GVector<MAX_SIZE,ELEM_TYPE>	v;
	for ( MGSize i=0; i<MAX_SIZE; ++i)
		v.mtab[i] = ( vec1.mtab[i] < vec2.mtab[i] )  ?  vec1.mtab[i]  : vec2.mtab[i];
	return v;
}

template <MGSize MAX_SIZE, class ELEM_TYPE> 
inline GVector<MAX_SIZE,ELEM_TYPE> Maximum(  const GVector<MAX_SIZE,ELEM_TYPE>& vec1,  const GVector<MAX_SIZE,ELEM_TYPE>& vec2 )
{
	GVector<MAX_SIZE,ELEM_TYPE>	v;
	for ( MGSize i=0; i<MAX_SIZE; ++i)
		v.mtab[i] = ( vec1.mtab[i] > vec2.mtab[i] )  ?  vec1.mtab[i]  : vec2.mtab[i];
	return v;
}



} // end of namespace HFGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace HFGeom = HFGeomTools;



#endif // __HF_GVECTOR_H__
