#ifndef __BOX_H__
#define __BOX_H__

#include "mgdecl.h"
#include "hf_gvector.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace HFGeomTools {




//////////////////////////////////////////////////////////////////////
//  class Box
//////////////////////////////////////////////////////////////////////
template <MGSize DIM, class T>
class GBox
{
public:
	typedef GVector<DIM,T>	GVect;

	GBox() : mvMin( GVect( static_cast<T>(0)) ), GVect( static_cast<T>(0)) )	{}
	GBox( const GVect& vmin, const GVect& vmax) : mvMin( vmin), mvMax( vmax)	{}

	void	ExportTEC( ostream *f)	{};
	
	void	Equalize();

	bool	IsInside( const GVect& vct) const;
	bool	IsOverlapping( const GBox<DIM,T>& box) const;
	
	GVect			Center()		{ return (mvMin + mvMax)/2.0;}

	const GVect&	cVMin() const	{ return mvMin;}
		  GVect&	rVMin() 		{ return mvMin;}
		  
	const GVect&	cVMax() const	{ return mvMax;}
		  GVect&	rVMax() 		{ return mvMax;}


private:
	GVect	mvMin;
	GVect	mvMax;
};
//////////////////////////////////////////////////////////////////////

template <MGSize DIM, class T>
inline void GBox<DIM,T>::Equalize()
{
	GVect	dv;
	T	h;
	
	dv = mvMax - mvMin;

	h = 0;
	for ( MGInt i=0; i<DIM; ++i)
		if ( h < fabs( dv.cX(i) ) )
			h = fabs( dv.cX(i) );
		
	for ( MGInt i=0; i<DIM; ++i)
		if ( fabs( dv.cX(i) ) < h )
		{
			mvMax.rX(i) += 0.5*(h-dv.cX(i));
			mvMin.rX(i) -= 0.5*(h-dv.cX(i));
		}

}

template <MGSize DIM, class T>

inline bool GBox<DIM,T>::IsInside( const GVect& vct) const
{
	for ( MGInt i=0; i<DIM; ++i)
		if ( vct.cX(i) > mvMax.cX(i) || vct.cX(i) < mvMin.cX(i) )
			return false;

	return true;
}

//template < class T>
//inline bool Box<DIM_1D,T>::IsOverlapping( const Box<DIM_1D>& box) const
//{
//	if ( box.IsInside( VMin() ) || box.IsInside( VMax() ) ||
//		 IsInside( box.VMin() ) || IsInside( box.VMax() )  )
//	{
//		 return true;
//	}
//
//	return false;
//}


template <MGSize DIM, class T>

inline bool GBox<DIM,T>::IsOverlapping( const GBox<DIM,T>& box) const
{
	GBox<DIM_1D,T>	box1, box2;

	for ( MGSize i=0; i<DIM; ++i)
	{
		box1.rVMin() = Vect1D( cVMin().cX(i) );
		box1.rVMax() = Vect1D( cVMax().cX(i) );

		box2.rVMin() = Vect1D( box.cVMin().cX(i) );
		box2.rVMax() = Vect1D( box.cVMax().cX(i) );

		if ( (!box1.IsInside( box2.cVMin() )) && (!box1.IsInside( box2.cVMax() )) &&
			 (!box2.IsInside( box1.cVMin() )) && (!box2.IsInside( box1.cVMax() ))  )
			 return false;
	}

	return true;
}


template <MGSize DIM, class T>

inline GBox<DIM,T> Sum( const GBox<DIM,T>& box1, const GBox<DIM,T>& box2)
{
	GBox<DIM,T> box;
	box.rVMin() = Minimum( box1.cVMin(), box2.cVMin() );
	box.rVMax() = Maximum( box1.cVMax(), box2.cVMax() );

	return box;
}


/*
template<>
inline void Box<DIM_1D>::ExportTEC( ostream *f)
{
	f << "VARIABLES = \"X\"" << endl;
	f << "ZONE I=2, F=POINT" << endl;
	f << mvMin.cX() << endl;
	f << mvMax.cX() << endl;
}


template<>
inline void Box<DIM_1D>::ExportTEC( FILE *f)
{
	fprintf( f, "VARIABLES = \"X\"\n");
	fprintf( f, "ZONE I=%d, F=POINT\n", 2);
	fprintf( f, "%lg\n", mvMin.cX() );
	fprintf( f, "%lg\n", mvMax.cX() );
}

template<>
inline void Box<DIM_2D>::ExportTEC( FILE *f)
{
	fprintf( f, "VARIABLES = \"X\",\"Y\"\n");

	fprintf( f, "ZONE  N=4, E=1, F=FEPOINT, ET=QUADRILATERAL\n");

	fprintf( f, "%lg %lg\n", mvMin.cX(), mvMin.cY() );
	fprintf( f, "%lg %lg\n", mvMax.cX(), mvMin.cY() );
	fprintf( f, "%lg %lg\n", mvMax.cX(), mvMax.cY() );
	fprintf( f, "%lg %lg\n", mvMin.cX(), mvMax.cY() );
	fprintf( f, "1 2 3 4\n" );
}

template<>
inline void Box<DIM_3D>::ExportTEC( FILE *f)
{
	fprintf( f, "VARIABLES = \"X\",\"Y\",\"Z\"\n");

	fprintf( f, "ZONE  N=8, E=1, F=FEPOINT, ET=BRICK\n");

	fprintf( f, "%lg %lg %lg\n", mvMin.cX(), mvMin.cY(), mvMin.cZ() );
	fprintf( f, "%lg %lg %lg\n", mvMax.cX(), mvMin.cY(), mvMin.cZ() );
	fprintf( f, "%lg %lg %lg\n", mvMax.cX(), mvMax.cY(), mvMin.cZ() );
	fprintf( f, "%lg %lg %lg\n", mvMin.cX(), mvMax.cY(), mvMin.cZ() );
	fprintf( f, "%lg %lg %lg\n", mvMin.cX(), mvMin.cY(), mvMax.cZ() );
	fprintf( f, "%lg %lg %lg\n", mvMax.cX(), mvMin.cY(), mvMax.cZ() );
	fprintf( f, "%lg %lg %lg\n", mvMax.cX(), mvMax.cY(), mvMax.cZ() );
	fprintf( f, "%lg %lg %lg\n", mvMin.cX(), mvMax.cY(), mvMax.cZ() );
	fprintf( f, "1 2 3 4 5 6 7 8\n" );
}
*/



} // end of namespace HFGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace HFGeom = HFGeomTools;




#endif // __BOX_H__
