#ifndef __HF_QRDECOMP_H__
#define __HF_QRDECOMP_H__


#include "mgdecl.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace HFGeomTools {

//////////////////////////////////////////////////////////////////////
// class QRDecomp
//////////////////////////////////////////////////////////////////////
template< class T, class MATRIX>
class QRDecomp
{
public:
	typedef MGSize	size_type;

	QRDecomp( const MATRIX& mtx);

	void	Execute();
	void	AssembleQ( MATRIX& mtxQ);
	void	AssembleR( MATRIX& mtxR);
	T		GetDeterminant();
	void	Solve( MATRIX& mtxB);

	T		Error();

private:
	MATRIX		mA;
	vector<T>	mtabD;	
	size_type	mNrow;
	size_type	mNcol;
	T			mSign;
};
//////////////////////////////////////////////////////////////////////


template< class T, class MATRIX>
QRDecomp<T,MATRIX>::QRDecomp( const MATRIX& mtx) : mA(mtx), mtabD(mtx.NCols()), mNrow(mtx.NRows()), mNcol(mtx.NCols()), mSign(1)	
{
	if ( mtx.NRows() < mtx.NCols() )
		THROW_INTERNAL( "QRDecomp : QRDecomp : n_rows must be greater or equal then n_cols");
}


template< class T, class MATRIX>
void QRDecomp<T,MATRIX>::Execute()
{
	for ( size_type k=0; k<mNcol; ++k) 
	{
		// Compute 2-norm of k-th column without under/overflow.
		T nrm = 0;
		for ( size_type i=k; i<mNrow; ++i) 
			nrm += mA(i,k)*mA(i,k);
		nrm = sqrt( nrm);

		if ( nrm != T(0) ) 
		{
			// Form k-th Householder vector.
			if ( mA(k,k) < T(0) ) 
				nrm = -nrm;

			for ( size_type i=k; i<mNrow; ++i) 
				mA(i,k) /= nrm;

			mA(k,k) += T(1);

			// Apply transformation to remaining columns.
			for ( size_type j=k+1; j<mNcol; ++j) 
			{
				T s = 0.0; 
				for ( size_type i=k; i<mNrow; ++i) 
					s += mA(i,k)*mA(i,j);

				s = -s/mA(k,k);
				for ( size_type i=k; i<mNrow; ++i) 
					mA(i,j) += s*mA(i,k);

			}
		}

		mtabD[k] = -nrm;
	}

	if ( mtabD.size() % 2 != 0 )
		mSign = -1;
	else
		mSign = 1;


	///////////////////////
	// SECOND VERSION
	//for ( size_type j=0; j<mNcol; ++j)
	//{
	//	T s = T(0);

	//	for ( size_type i=j; i<mNrow; ++i)
	//		s += mA(i,j) * mA(i,j);
	//	s = sqrt( s);

	//	//mtabD[j] = (mA(j,j) > T(0)) ? (-s) : (s); // does not work for MPIRXX !!!

	//	if ( mA(j,j) > T(0) )
	//		mtabD[j] = -s;
	//	else
	//		mtabD[j] = s;

	//	T fc = sqrt( s * ( s + abs(mA(j,j)) ) );

	//	mA(j,j) = mA(j,j) - mtabD[j];

	//	for ( size_type k=j; k<mNrow; ++k)
	//		mA(k,j) /= fc;

	//	for ( size_type i=j+1; i<mNcol; ++i)
	//	{
	//		s = T(0);
	//
	//		for ( size_type k=j; k<mNrow; ++k)
	//			s += mA(k,j)*mA(k,i);

	//		for ( size_type k=j; k<mNrow; ++k)
	//			mA(k,i) -= s* mA(k,j);
	//	}

	//}

	//mA.Write();
}


template< class T, class MATRIX>
void QRDecomp<T,MATRIX>::AssembleQ( MATRIX& mtxQ)
{
	mtxQ.Init( 0);
	mtxQ.Resize( mNrow, mNcol);

	for ( size_type k=0; k<mNcol; ++k) 
		mtxQ(k,k) = T(1);

	for ( size_type l=mNcol; l>0; --l) 
	{
		size_type k = l-1;

		for ( size_type j=k; j<mNcol; ++j) 
		{
			if ( mA(k,k) != 0) 
			{
				T s = T(0);
				for ( size_type i=k; i<mNrow; ++i) 
					s += mA(i,k) * mtxQ(i,j);

				s = s / mA(k,k); // should be commented for 2nd version
				for ( size_type i=k; i<mNrow; ++i) 
					mtxQ(i,j) -= s * mA(i,k);
			}
		}
	}

}

template< class T, class MATRIX>
void QRDecomp<T,MATRIX>::AssembleR( MATRIX& mtxR)
{
	mtxR.Init( 0);
	mtxR.Resize( mNcol, mNcol);

	for ( size_type j=0; j<mNcol; ++j)
	{
		mtxR(j,j) = mtabD[j];
		for ( size_type k=j+1; k<mNcol; ++k)
			mtxR(j,k) = mA(j,k);
	}
}



template< class T, class MATRIX>
void QRDecomp<T,MATRIX>::Solve( MATRIX& mtxB)
{
	size_type nbcol = mtxB.NCols(); 

	// Compute Y = transpose(Q)*B
	for ( size_type k=0; k<mNcol; ++k) 
	{
		for ( size_type j=0; j<nbcol; ++j) 
		{
			T s = T(0); 
			for ( size_type i=k; i<mNrow; ++i) 
				s += mA(i,k) * mtxB(i,j);

			s = s / mA(k,k);
			for ( size_type i=k; i<mNrow; ++i) 
				mtxB(i,j) -= s * mA(i,k);
		}
	}

	// Solve R*X = Y;
	for ( size_type l=mNcol; l>0; --l) 
	{
		size_type k = l-1;

		for ( size_type j=0; j<nbcol; ++j) 
			mtxB(k,j) /= mtabD[k];

		for ( size_type i=0; i<k; ++i) 
			for (size_type j=0; j<nbcol; ++j) 
				mtxB(i,j) -= mtxB(k,j) * mA(i,k);
	}

}


template< class T, class MATRIX>
T QRDecomp<T,MATRIX>::GetDeterminant()
{
	T d = mtabD[0] * mSign;

	for ( size_type j=1; j<mtabD.size(); ++j)
		d *= mtabD[j];

	return d;
}


template< class T, class MATRIX>
T QRDecomp<T,MATRIX>::Error()
{
	T tmax = abs( mtabD[0] );
	T tmin = abs( mtabD[0] );

	for ( size_type j=1; j<mtabD.size(); ++j)
	{
		if ( abs(mtabD[j]) > tmax) tmax = abs(mtabD[j]);
		if ( abs(mtabD[j]) < tmin) tmin = abs(mtabD[j]);
	}

	return tmax / tmin;


	//T ctmp, c;

	//ctmp = c = T(0);//T(1) / mtabD[0];

	//for ( size_type j=1; j<mNcol; ++j)
	//{
	//	T sum = T(0);
	//	for ( size_type k=0; k<j-1; ++k)
	//		sum += mA(k,j)*mA(k,j);

	//	ctmp = sqrt(sum) / mtabD[j];

	//	if ( ctmp > c)
	//		c = ctmp;
	//}

	//for ( size_type j=0; j<mtabD.size(); ++j)
	//	cout << " ------ " << mtabD[j] << endl;

	//return c;
}


} // end of namespace HFGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace HFGeom = HFGeomTools;



#endif // __HF_QRDECOMP_H__  
