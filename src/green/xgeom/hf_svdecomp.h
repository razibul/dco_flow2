#ifndef __HF_SVDECOMP_H__
#define __HF_SVDECOMP_H__


#include "mgdecl.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace HFGeomTools {




//////////////////////////////////////////////////////////////////////
// class SVDecomp
//////////////////////////////////////////////////////////////////////
template< class T, class MATRIX>
class SVDecomp
{
public:
	typedef MGSize	size_type;

	SVDecomp( const MATRIX& mtx);

	void	Execute();
	//void	AssembleQ( MATRIX& mtxQ);
	//void	AssembleR( MATRIX& mtxR);
	//T		Determinant();
	//void	Solve( MATRIX& mtxB);

private:
	MATRIX		mA;
	MATRIX		mU;
	MATRIX		mV;
	vector<T>	mtabS;
	size_type	mNrow;
	size_type	mNcol;
};
//////////////////////////////////////////////////////////////////////


template< class T, class MATRIX>
SVDecomp<T,MATRIX>::SVDecomp( const MATRIX& mtx) : mA(mtx), mNrow(mtx.NRows()), mNcol(mtx.NCols())	
{
	//if ( mtx.NRows() < mtx.NCols() )
	//	THROW_INTERNAL( "QRDecomp : QRDecomp : n_rows must be greater or equal then n_cols");
}


template< class T, class MATRIX>
void SVDecomp<T,MATRIX>::Execute()
{
	size_type nu = min( mNrow, mNcol);
	mtabS.resize( min( mNrow+1, mNcol) );
	mU.Resize( mNrow, nu, T(0) );
	mV.Resize( mNcol, mNcol, T(0) );


}

} // end of namespace HFGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace HFGeom = HFGeomTools;



#endif // __HF_SVDECOMP_H__  
