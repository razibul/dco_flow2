#ifndef __HF_LINSUBSPACE_H__
#define __HF_LINSUBSPACE_H__


#include "mgdecl.h"
#include "hf_gvector.h"
#include "hf_gmatrix.h"
#include "hf_qrdecomp.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace HFGeomTools {


template <MGSize DIM, MGSize UNIDIM, class T>
class LinSubSpace
{
public:
	typedef GVector<UNIDIM,T>	UVect;
	typedef GVector<DIM,T>		LVect;
	typedef GMatrix<UNIDIM+1,T>	Matrix;

	enum { CODIM = UNIDIM - DIM };

	LinSubSpace()		{}


private:
	UVect	mtabDir[DIM];

	Matrix	mBasis;
	Matrix	mCoBasis;
};


} // end of namespace HFGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace HFGeom = HFGeomTools;



#endif // __HF_LINSUBSPACE_H__  
