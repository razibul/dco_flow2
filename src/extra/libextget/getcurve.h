#ifndef __GETCURVE_H__
#define __GETCURVE_H__


#include "libextget/geomentity.h"
#include "libextget/getconst.h"

#include "libcorecommon/interpol.h"
#include "libextnurbs/nurbscurve.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GETSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//




//////////////////////////////////////////////////////////////////////
// class Line
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class Line : public GeomEntity<DIM_1D,DIM>
{
	typedef typename GeomEntity<DIM_1D,DIM>::UVect	UVect;
	typedef typename GeomEntity<DIM_1D,DIM>::GVect	GVect;

public:
	enum { SIZE = GeomEntity<DIM_1D,DIM>::SIZE};

	Line( const MGSize& id=0) : GeomEntity<DIM_1D,DIM>()	{}
	Line( const UVect& vo, const UVect& vt, const MGSize& id=0) : mOrigin(vo), mTnVect(vt), GeomEntity<DIM_1D,DIM>()	{}
	virtual ~Line()											{}
	

	static const char*		ClassName()							{ return GET_LINE;}

	virtual MGString	GetName() const							{ return ClassName();};
	virtual bool		IsType( const MGString& type) const		{ return type == ClassName() ? true : false; }

	virtual void		Init( const MGString& sname, MGString& sparams);
	virtual void		WriteDef( ostream& of) const;


	virtual bool		IsPeriodic( const MGSize& idir) const	{ return false;}
	virtual MGFloat		GetPeriod( const MGSize& idir) const	{ return 0.0;}

	virtual GVect		FindParam( const UVect& vct) const;

	virtual void		GetCoord( UVect& vret, const GVect& vct) const;
	virtual void		GetFstDeriv( UVect tab[DIM_1D], const GVect& vct) const;
	virtual void		GetSndDeriv( UVect tab[SIZE], const GVect& vct) const;

	virtual void		Eval( UVect& vret, UVect tab1[DIM], UVect tab2[SIZE], const GVect& vct) const;

protected:
	UVect&	rOrigin()	{ return mOrigin;}	// base point
	UVect&	rTnVect()	{ return mTnVect;}	// tangent vector

private:
	UVect	mOrigin;	// base point
	UVect	mTnVect;	// tangent vector

};


//////////////////////////////////////////////////////////////////////
// class Circle
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class Circle : public GeomEntity<DIM_1D,DIM>
{
	typedef typename GeomEntity<DIM_1D,DIM>::UVect	UVect;
	typedef typename GeomEntity<DIM_1D,DIM>::GVect	GVect;

public:
	enum { SIZE = GeomEntity<DIM_1D,DIM>::SIZE};

	Circle( const MGSize& id=0) : GeomEntity<DIM_1D,DIM>()	{}
	virtual ~Circle()											{}
	

	static const char*		ClassName()							{ return GET_CIRCLE;}

	virtual MGString	GetName() const							{ return ClassName();};
	virtual bool		IsType( const MGString& type) const		{ return type == ClassName() ? true : false; }

	virtual void		Init( const MGString& sname, MGString& sparams);
	virtual void		WriteDef( ostream& of) const;


	virtual bool		IsPeriodic( const MGSize& idir) const	{ return true;}
	virtual MGFloat		GetPeriod( const MGSize& idir) const	{ return 2.0*M_PI;}

	virtual GVect		FindParam( const UVect& vct) const;

	virtual void		GetCoord( UVect& vret, const GVect& vct) const;
	virtual void		GetFstDeriv( UVect tab[DIM_1D], const GVect& vct) const;
	virtual void		GetSndDeriv( UVect tab[SIZE], const GVect& vct) const;

	virtual void		Eval( UVect& vret, UVect tab1[DIM], UVect tab2[SIZE], const GVect& vct) const;

protected:
	UVect&		rCenter()	{ return mCenter;}	// center point
	UVect&		rAxisOX()	{ return mAxisOX;}	// OX vector
	UVect&		rAxisOY()	{ return mAxisOY;}	// OY vector
	MGFloat&	rRadius()	{ return mRadius;}	// radius

private:
	UVect	mCenter;	// center point
	UVect	mAxisOX;	// OX vector
	UVect	mAxisOY;	// OY vector
	MGFloat	mRadius;	// radius

};


//////////////////////////////////////////////////////////////////////
// class Ellipse
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class Ellipse : public GeomEntity<DIM_1D,DIM>
{
	typedef typename GeomEntity<DIM_1D,DIM>::UVect	UVect;
	typedef typename GeomEntity<DIM_1D,DIM>::GVect	GVect;

public:
	enum { SIZE = GeomEntity<DIM_1D,DIM>::SIZE};

	Ellipse( const MGSize& id=0) : GeomEntity<DIM_1D,DIM>()	{}
	virtual ~Ellipse()											{}
	

	static const char*		ClassName()							{ return GET_ELLIPSE;}

	virtual MGString	GetName() const							{ return ClassName();};
	virtual bool		IsType( const MGString& type) const		{ return type == ClassName() ? true : false; }

	virtual void		Init( const MGString& sname, MGString& sparams);
	virtual void		WriteDef( ostream& of) const;


	virtual bool		IsPeriodic( const MGSize& idir) const	{ return true;}
	virtual MGFloat		GetPeriod( const MGSize& idir) const	{ return 2.0*M_PI;}

	virtual GVect		FindParam( const UVect& vct) const;

	virtual void		GetCoord( UVect& vret, const GVect& vct) const;
	virtual void		GetFstDeriv( UVect tab[DIM_1D], const GVect& vct) const;
	virtual void		GetSndDeriv( UVect tab[SIZE], const GVect& vct) const;

	virtual void		Eval( UVect& vret, UVect tab1[DIM], UVect tab2[SIZE], const GVect& vct) const;

protected:
	UVect&		rCenter()		{ return mCenter;}		// center point
	UVect&		rAxisOX()		{ return mAxisOX;}		// OX vector
	UVect&		rAxisOY()		{ return mAxisOY;}		// OY vector
	MGFloat&	rSemiAxisOX()	{ return mSemiAxisOX;}	// semi axis OX
	MGFloat&	rSemiAxisOY()	{ return mSemiAxisOY;}	// semi axis OY

private:
	UVect	mCenter;		// center point
	UVect	mAxisOX;		// OX vector
	UVect	mAxisOY;		// OY vector
	MGFloat	mSemiAxisOX;	// semi axis length OX
	MGFloat mSemiAxisOY;		// semi axis length OY
};


//////////////////////////////////////////////////////////////////////
// class Spline
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class Spline : public GeomEntity<DIM_1D,DIM>
{
	typedef typename GeomEntity<DIM_1D,DIM>::UVect	UVect;
	typedef typename GeomEntity<DIM_1D,DIM>::GVect	GVect;

public:
	enum { SIZE = GeomEntity<DIM_1D,DIM>::SIZE};

	Spline( const MGSize& id=0) : mbPeriodic(false), mPeriod(0.0), GeomEntity<DIM_1D,DIM>()	{}
	virtual ~Spline()										{}
	

	static const char*		ClassName()							{ return GET_SPLINE;}

	virtual MGString	GetName() const							{ return ClassName();};
	virtual bool		IsType( const MGString& type) const		{ return type == ClassName() ? true : false; }	

	virtual void		Init( const MGString& sname, MGString& sparams);
	virtual void		WriteDef( ostream& of) const;


	virtual bool		IsPeriodic( const MGSize& idir) const	{ return mbPeriodic;}
	virtual MGFloat		GetPeriod( const MGSize& idir) const	{ return mPeriod;}

	virtual GVect		FindParam( const UVect& vct) const;

	virtual void		GetCoord( UVect& vret, const GVect& vct) const;
	virtual void		GetFstDeriv( UVect tab[DIM_1D], const GVect& vct) const;
	virtual void		GetSndDeriv( UVect tab[SIZE], const GVect& vct) const;

	virtual void		Eval( UVect& vret, UVect tab1[DIM], UVect tab2[SIZE], const GVect& vct) const;

private:
	bool	mbPeriodic;
	MGFloat	mPeriod;

	UVect	mOrigin;	// base point
	UVect	mTnVect;	// tangent vector

	MGSize		mNum;
	::Spline	mFun[DIM];	// Spline class from interpol.h

	MGFloatArr		mtabS;
	MGFloatArr		mtabX[DIM];

};



//////////////////////////////////////////////////////////////////////
// class NurbsCrv
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class NurbsCrv : public GeomEntity<DIM_1D,DIM>
{
	typedef typename GeomEntity<DIM_1D,DIM>::UVect	UVect;
	typedef typename GeomEntity<DIM_1D,DIM>::GVect	GVect;

public:
	enum { SIZE = GeomEntity<DIM_1D,DIM>::SIZE};

	NurbsCrv( const MGSize& id=0) : mbPeriodic(false), mPeriod(0.0), GeomEntity<DIM_1D,DIM>()	{}
	virtual ~NurbsCrv()										{}
	

	static const char*		ClassName()							{ return GET_NURBSCRV;}

	virtual MGString	GetName() const							{ return ClassName();};
	virtual bool		IsType( const MGString& type) const		{ return type == ClassName() ? true : false; }	

	virtual void		Init( const MGString& sname, MGString& sparams);
	virtual void		WriteDef( ostream& of) const;


	virtual bool		IsPeriodic( const MGSize& idir) const	{ return mbPeriodic;}
	virtual MGFloat		GetPeriod( const MGSize& idir) const	{ return mPeriod;}

	virtual GVect		FindParam( const UVect& vct) const;

	virtual void		GetCoord( UVect& vret, const GVect& vct) const;
	virtual void		GetFstDeriv( UVect tab[DIM_1D], const GVect& vct) const;
	virtual void		GetSndDeriv( UVect tab[SIZE], const GVect& vct) const;

	virtual void		Eval( UVect& vret, UVect tab1[DIM], UVect tab2[SIZE], const GVect& vct) const;

	virtual void		ExportTEC( ostream& os) const;

protected:
	bool&		rbPeriodic()	{ return mbPeriodic;}
	MGFloat&	rPeriod()		{ return mPeriod;}

	NURBS::NurbsCurve<DIM,MGFloat>&		rNurbsCrv()		{ return mNurbsCrv;}


private:
	bool	mbPeriodic;
	MGFloat	mPeriod;

	NURBS::NurbsCurve<DIM,MGFloat>	mNurbsCrv;

};




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GETSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace GET = GETSpace;


#endif // __GETCURVE_H__
