#include "geotopoio.h"

#include "libcoreio/store.h"
#include "libextget/geotopomaster.h"
#include "libextget/getconst.h"
#include "libextget/getpoint.h"
#include "libextget/getcurve.h"
#include "libextget/getsurface.h"
#include "libextget/geomowner.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GETSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

template<>
MGString GeoTopoIO<DIM_2D>::mstVersionString = GET_VER_STRING;

template<>
MGString GeoTopoIO<DIM_3D>::mstVersionString = GET_VER_STRING;

template<>
MGString GeoTopoIO<DIM_2D>::mstVersionNumber = GET_VER_NUMBER;

template<>
MGString GeoTopoIO<DIM_3D>::mstVersionNumber = GET_VER_NUMBER;



template <Dimension DIM>
GeoTopoIO<DIM>::GeoTopoIO( GeoTopoMaster<DIM>& get) : mpGeT(&get)		
{
    mRex.InitPattern( "^#([0-9]*)=([_A-Z0-9]*)\\((.*)\\);$");

	InitPointCreators();
	InitCurveCreators();
	InitSurfaceCreators();
}

template <Dimension DIM>
GeoTopoIO<DIM>::~GeoTopoIO()		
{
	for ( MGSize i=0; i<mtabPointCreator.size(); ++i)
		delete mtabPointCreator[i];

	for ( MGSize i=0; i<mtabCurveCreator.size(); ++i)
		delete mtabCurveCreator[i];

	for ( MGSize i=0; i<mtabSurfaceCreator.size(); ++i)
		delete mtabSurfaceCreator[i];
}



template <Dimension DIM>
void GeoTopoIO<DIM>::InitPointCreators()
{
	mtabPointCreator.push_back( new Factory< Point<DIM>, GeomEntity<DIM_0D,DIM> > );
}


template <Dimension DIM>
void GeoTopoIO<DIM>::InitCurveCreators()
{
	mtabCurveCreator.push_back( new Factory< Line<DIM>, GeomEntity<DIM_1D,DIM> > );
	mtabCurveCreator.push_back( new Factory< Circle<DIM>, GeomEntity<DIM_1D,DIM> > );
	mtabCurveCreator.push_back( new Factory< Ellipse<DIM>, GeomEntity<DIM_1D,DIM> > );
	mtabCurveCreator.push_back( new Factory< Spline<DIM>, GeomEntity<DIM_1D,DIM> > );
	mtabCurveCreator.push_back( new Factory< NurbsCrv<DIM>, GeomEntity<DIM_1D,DIM> > );
}


template <Dimension DIM>
void GeoTopoIO<DIM>::InitSurfaceCreators()
{
	// empty
}

template <>
void GeoTopoIO<DIM_3D>::InitSurfaceCreators()
{
	mtabSurfaceCreator.push_back( new Factory< Plane<DIM_3D>, GeomEntity<DIM_2D,DIM_3D> > );
	mtabSurfaceCreator.push_back( new Factory< Sphere<DIM_3D>, GeomEntity<DIM_2D,DIM_3D> > );
	mtabSurfaceCreator.push_back( new Factory< Torus<DIM_3D>, GeomEntity<DIM_2D,DIM_3D> > );
	mtabSurfaceCreator.push_back( new Factory< NurbsSrf<DIM_3D>, GeomEntity<DIM_2D,DIM_3D> > );
	mtabSurfaceCreator.push_back( new Factory< CylindricalSrf<DIM_3D>, GeomEntity<DIM_2D,DIM_3D> > );
	mtabSurfaceCreator.push_back( new Factory< ConicalSrf<DIM_3D>, GeomEntity<DIM_2D,DIM_3D> > );
}



template <Dimension DIM>
bool GeoTopoIO<DIM>::CheckVersion_ascii( istream& file)
{
	istringstream is;
	MGString	sname, sver;

	IO::ReadLine( file, is);
	is >> sname >> sver;

	if ( sname != VerStr() != 0 || sver != VerNum() != 0 )
		return false;

	return true;
}



template <Dimension DIM>
void GeoTopoIO<DIM>::ReadDataSection( istream& file, const MGString& key, void (GeoTopoIO<DIM>::*addFun)( const MGString&) )
{
	MGString	buf, skey;

	IO::ReadLineC( file, buf);
	if ( buf != key + ";" )
	{
		buf = MGString( "expected : '") + key + MGString( "'; ");
		THROW_FILE( buf.c_str(), "" );
	}

	skey = MGString(GET_END) + "_" + key;
	while ( IO::ReadLineC( file, buf) )
	{
		//cout << buf << endl;
		if ( buf == skey + ";" )
			return;

		(this->* addFun)( buf);
	}

	buf = MGString( "expected : '") + skey + MGString( "'; ");
	THROW_FILE( buf.c_str(), "" );
}


//////////////////////////////////////////////////////////////////////
//	HEADER

template <Dimension DIM>
void GeoTopoIO<DIM>::ReadHeader( istream& file)
{
	MGString	buf, skey;

	// HEADER
	IO::ReadLineC( file, buf);
	if ( buf != MGString(GET_HEADER) + ";" )
	{
		buf = MGString( "expected : '") + MGString( GET_HEADER) + MGString( "'; ");
		THROW_FILE( buf.c_str(), "" );
	}

	// DIMENSION
	IO::ReadLineC( file, buf);
	IO::Strip( buf, ";");

	Dimension dim = ::StrToDim( buf);
	if ( DIM != dim)
	{
		buf = MGString( "incompatible GeT dimension : '") + MGString( buf) + MGString( "'; ");
		THROW_FILE( buf.c_str(), "" );
	}

	// NAME
	IO::ReadLineCww( file, buf);
	IO::StripBlanks( buf);
	IO::Strip( buf, ";");
	mpGeT->mName = buf;

	// DESCRIPTION
	IO::ReadLineCww( file, buf);
	IO::StripBlanks( buf);
	IO::Strip( buf, ";");
	mpGeT->mDescription = buf;

	// END
	skey = MGString(GET_END) + "_" + MGString(GET_HEADER);
	IO::ReadLineC( file, buf);
	if ( buf != skey + ";" )
	{
		buf = MGString( "expected : '") + skey + MGString( "'; ");
		THROW_FILE( buf.c_str(), "" );
	}

}


template <Dimension DIM>
void GeoTopoIO<DIM>::ParseDef( MGSize& id, MGString& str1, MGString& str2, const MGString& def)
{
	mRex.SetString( def);

	if ( mRex.IsOk() )
	{
		istringstream is;
		is.clear();

		ASSERT( mRex.GetNoSubString() == 3);
		int k = mRex.GetNoSubString();

		is.str( mRex.GetSubString( 1) );
		str1 = mRex.GetSubString( 2);
		str2 = mRex.GetSubString( 3);

		is >> id;
	}
	else
		THROW_FILE( "parsing error", def);
}

template <Dimension DIM>
void GeoTopoIO<DIM>::ParseDef( MGSize& id, MGString& str1, MGString& str2,  MGString& sname, const MGString& def)
{
	mRex.SetString( def);

	if ( mRex.IsOk() )
	{
		istringstream is;
		is.clear();

		ASSERT( mRex.GetNoSubString() == 3);
		int k = mRex.GetNoSubString();

		is.str( mRex.GetSubString( 1) );
		str1 = mRex.GetSubString( 2);
		str2 = mRex.GetSubString( 3);

		is >> id;

		
		sname = "";

		RegExp	rex;
		
		rex.InitPattern( "^'([_A-Za-z0-9 ]*)',(.*)$");
		rex.SetString( str2);
		
		if ( rex.IsOk() )
		{
			sname = rex.GetSubString( 1);
			str2 = rex.GetSubString( 2);
		}

	}
	else
		THROW_FILE( "parsing error", def);
}


//////////////////////////////////////////////////////////////////////
//	GEOMETRY

template <Dimension DIM>
void GeoTopoIO<DIM>::AddPoint( const MGString& def)
{
	MGSize		id;
	MGString	str1, str2, name;

	ParseDef( id, str1, str2, name, def);

	GeomEntity<DIM_0D,DIM>*	ptr = NULL;

	for ( MGSize i=0; i<mtabPointCreator.size(); ++i)
	{
		ptr = mtabPointCreator[i]->Create( id, str1, str2 );
		if ( ptr )
		{
			ptr->rName() = name;
			break;
		}
	}

	if ( ! ptr)
	{
		MGString buf = MGString( "unrecognized definition : '") + def + MGString( "'; ");
		THROW_INTERNAL( buf.c_str() );
	}

//	GeomOwner<DIM,DIM> gg;// = mpGeT->mGOwner;
//	MGSize getid = gg.template PushBack<DIM_0D>();
//	MGSize getid = gg.PushBack<DIM_0D>( ptr);

	MGSize getid = mpGeT->mGOwner.template PushBack<DIM_0D>( ptr);

	if ( getid != id)
	{
		ostringstream os;
		os << "bad id in the definition : '" << def << "' - should be:" << getid ;
		THROW_INTERNAL( os.str() );
	}
}

template <Dimension DIM>
void GeoTopoIO<DIM>::AddCurve( const MGString& def)
{
	MGSize		id;
	MGString	str1, str2, name;

	ParseDef( id, str1, str2, name, def);

	GeomEntity<DIM_1D,DIM>*	ptr = NULL;

	for ( MGSize i=0; i<mtabCurveCreator.size(); ++i)
	{
		ptr = mtabCurveCreator[i]->Create( id, str1, str2 );
		if ( ptr )
		{
			ptr->rName() = name;
			break;
		}
	}

	if ( ! ptr)
	{
		MGString buf = MGString( "unrecognized definition : '") + def + MGString( "'; ");
		THROW_INTERNAL( buf.c_str() );
	}

	MGSize getid = mpGeT->mGOwner.template PushBack<DIM_1D>( ptr);

	if ( getid != id)
	{
		ostringstream os;
		os << "bad id in the definition : '" << def << "' - should be:" << getid ;
		THROW_INTERNAL( os.str() );
	}
}

template <Dimension DIM>
void GeoTopoIO<DIM>::AddSurface( const MGString& def)
{
	MGSize		id;
	MGString	str1, str2, name;

	ParseDef( id, str1, str2, name, def);

	GeomEntity<DIM_2D,DIM>*	ptr = NULL;

	for ( MGSize i=0; i<mtabSurfaceCreator.size(); ++i)
	{
		ptr = mtabSurfaceCreator[i]->Create( id, str1, str2 );
		if ( ptr )
		{
			ptr->rName() = name;
			break;
		}
	}

	if ( ! ptr)
	{
		MGString buf = MGString( "unrecognized definition : '") + def + MGString( "'; ");
		THROW_INTERNAL( buf.c_str() );
	}

	MGSize getid = mpGeT->mGOwner.template PushBack<DIM_2D>( ptr);

	if ( getid != id)
	{
		ostringstream os;
		os << "bad id in the definition : '" << def << "' - should be:" << getid ;
		THROW_INTERNAL( os.str() );
	}

}



template <>
void GeoTopoIO<DIM_2D>::ReadGeometryData( istream& file)
{
	ReadDataSection( file, GET_SEC_POINT, &GeoTopoIO<DIM_2D>::AddPoint );
	ReadDataSection( file, GET_SEC_CURVE, &GeoTopoIO<DIM_2D>::AddCurve );
}

template <>
void GeoTopoIO<DIM_3D>::ReadGeometryData( istream& file)
{
	ReadDataSection( file, GET_SEC_POINT, &GeoTopoIO<DIM_3D>::AddPoint );
	ReadDataSection( file, GET_SEC_CURVE, &GeoTopoIO<DIM_3D>::AddCurve );
	ReadDataSection( file, GET_SEC_SURFACE, &GeoTopoIO<DIM_3D>::AddSurface );
}

template <Dimension DIM>
void GeoTopoIO<DIM>::ReadGeometrySection( istream& file)
{
	MGString	buf, skey;

	// GEOMETRY
	IO::ReadLineC( file, buf);
	if ( buf != MGString(GET_GEOMETRY) + ";" )
	{
		buf = MGString( "expected : '") + MGString( GET_GEOMETRY) + MGString( "'; ");
		THROW_FILE( buf.c_str(), "" );
	}

	ReadGeometryData( file);

	// END
	skey = MGString(GET_END) + "_" + MGString(GET_GEOMETRY);
	IO::ReadLineC( file, buf);
	if ( buf != skey + ";" )
	{
		buf = MGString( "expected : '") + skey + MGString( "'; ");
		THROW_FILE( buf.c_str(), "" );
	}
}



//////////////////////////////////////////////////////////////////////
//	TOPOLOGY

template <Dimension DIM>
template <Dimension TDIM>
void GeoTopoIO<DIM>::AddTopoEnt( const MGString& def)
{
	MGSize		id;
	MGString	str1, str2, name;

	ParseDef( id, str1, str2, name, def);

	TopoEntity<TDIM>	tent;

	tent.Init( str1, str2);
	tent.rName() = name;

	MGSize getid = mpGeT->mTOwner.PushBack( tent);

	if ( getid != id)
	{
		ostringstream os;
		os << "bad id in the definition : '" << def << "' - should be:" << getid ;
		THROW_INTERNAL( os.str() );
	}
}


template <>
void GeoTopoIO<DIM_2D>::ReadTopologyData( istream& file)
{
	ReadDataSection( file, GET_SEC_VERTEX, &GeoTopoIO<DIM_2D>::AddTopoEnt<DIM_0D> );
	ReadDataSection( file, GET_SEC_EDGE, &GeoTopoIO<DIM_2D>::AddTopoEnt<DIM_1D> );
	ReadDataSection( file, GET_SEC_FACE, &GeoTopoIO<DIM_2D>::AddTopoEnt<DIM_2D> );
}

template <>
void GeoTopoIO<DIM_3D>::ReadTopologyData( istream& file)
{
	ReadDataSection( file, GET_SEC_VERTEX, &GeoTopoIO<DIM_3D>::AddTopoEnt<DIM_0D> );
	ReadDataSection( file, GET_SEC_EDGE, &GeoTopoIO<DIM_3D>::AddTopoEnt<DIM_1D> );
	ReadDataSection( file, GET_SEC_FACE, &GeoTopoIO<DIM_3D>::AddTopoEnt<DIM_2D> );
	ReadDataSection( file, GET_SEC_BREP, &GeoTopoIO<DIM_3D>::AddTopoEnt<DIM_3D> );
	//ReadDataSection( file, GET_SEC_VERTEX, &GeoTopoIO<DIM_3D>::AddVertex );
	//ReadDataSection( file, GET_SEC_EDGE, &GeoTopoIO<DIM_3D>::AddEdge );
	//ReadDataSection( file, GET_SEC_FACE, &GeoTopoIO<DIM_3D>::AddFace );
	//ReadDataSection( file, GET_SEC_BREP, &GeoTopoIO<DIM_3D>::AddBRep );
}

template <Dimension DIM>
void GeoTopoIO<DIM>::ReadTopologySection( istream& file)
{
	MGString	buf, skey;

	// TOPOLOGY
	IO::ReadLineC( file, buf);
	if ( buf != MGString(GET_TOPOLOGY) + ";" )
	{
		buf = MGString( "expected : '") + MGString( GET_TOPOLOGY) + MGString( "'; ");
		THROW_FILE( buf.c_str(), "" );
	}

	ReadTopologyData( file);


	// END
	skey = MGString(GET_END) + "_" + MGString(GET_TOPOLOGY);
	IO::ReadLineC( file, buf);
	if ( buf != skey + ";" )
	{
		buf = MGString( "expected : '") + skey + MGString( "'; ");
		THROW_FILE( buf.c_str(), "" );
	}
}


//////////////////////////////////////////////////////////////////////

template <Dimension DIM>
void GeoTopoIO<DIM>::DoRead( const MGString& fname)
{
	ifstream file;
	istringstream is;

	MGString	skey, sname, sver;

	file.open( fname.c_str(), ios::in );
	try
	{
		if ( ! file)
			THROW_FILE( "can not open the file", fname);

		cout << "reading GeT file : '" << fname << "'\n";

		if ( ! CheckVersion_ascii( file) )
		{
			ostringstream os;
			os << "incompatible GeT file version (current ver: '" << VerStr() << " " << VerNum() << "')";

			THROW_INTERNAL( os.str() );
		}

		ReadHeader( file);
		ReadGeometrySection( file);
		ReadTopologySection( file);

		//RemoveUnreferenced();

		cout << "end of reading GeT file : '" << fname << "'\n";

	}
	catch ( EHandler::Except& e)
	{
		cout << "ERROR: GeoTopoIO<DIM>::DoRead - file name: " << fname << endl << endl;
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);

		throw;
		//THROW_INTERNAL("END");
	}

}

//////////////////////////////////////////////////////////////////////



template <Dimension DIM>
void GeoTopoIO<DIM>::WriteHeader( ostream& file)
{
	file << VerStr() << " " << VerNum() << endl ;
	file << endl;

	file << GET_HEADER << ";" << endl;

	file << "\t" << ::DimToStr(DIM) << ";" << endl;

	if ( mpGeT->mName.length() == 0 )
		mpGeT->mName = "unknown name";

	file << "\t" << mpGeT->mName << ";" << endl;

	if ( mpGeT->mDescription.length() == 0 )
		mpGeT->mDescription = "unknown description";

	file << "\t" << mpGeT->mDescription << ";" << endl;

	file << MGString(GET_END) + "_" + MGString(GET_HEADER) << ";" << endl;
	file << endl;
}


template <Dimension DIM>
void GeoTopoIO<DIM>::WriteGeometrySection( ostream& file)
{
	MGString	buf;

	file.precision( 10);


	file << GET_GEOMETRY << ";" << endl;
	file << endl;


	// POINT section
	file << "\t" << GET_SEC_POINT << ";" << endl;

	for ( MGSize i=1; i<=mpGeT->cGeom().template Size<DIM_0D>(); ++i)
	{
		file << "\t";
		file << "#" << i << " = ";
		(mpGeT->cGeom().template cGEnt<DIM_0D>( i ))->WriteDef( file);
		file << ";" << endl;
	}

	file << "\t" << MGString(GET_END) + "_" + MGString(GET_SEC_POINT) << ";" << endl;
	file << endl;


	// CURVE section
	file << "\t" << GET_SEC_CURVE << ";" << endl;

	for ( MGSize i=1; i<=mpGeT->cGeom().template Size<DIM_1D>(); ++i)
	{
		file << "\t";
		file << "#" << i << " = ";
		(mpGeT->cGeom().template cGEnt<DIM_1D>( i ))->WriteDef( file);
		file << ";" << endl;
	}

	file << "\t" << MGString(GET_END) + "_" + MGString(GET_SEC_CURVE) << ";" << endl;
	file << endl;

	// SURFACE section
	file << "\t" << GET_SEC_SURFACE << ";" << endl;

	for ( MGSize i=1; i<=mpGeT->cGeom().template Size<DIM_2D>(); ++i)
	{
		file << "\t";
		file << "#" << i << " = ";
		(mpGeT->cGeom().template cGEnt<DIM_2D>( i ))->WriteDef( file);
		file << ";" << endl;
	}

	file << "\t" << MGString(GET_END) + "_" + MGString(GET_SEC_SURFACE) << ";" << endl;
	file << endl;


	file << MGString(GET_END) + "_" + MGString(GET_GEOMETRY) << ";" << endl;
	file << endl;
}


template <Dimension DIM>
void GeoTopoIO<DIM>::WriteTopologySection( ostream& file)
{
}



// TODO :: use templates to remove redundancy...
template <>
void GeoTopoIO<DIM_2D>::WriteTopologySection( ostream& file)
{
	file << GET_TOPOLOGY << ";" << endl;
	file << endl;

	// VERTEX section
	file << "\t" << GET_SEC_VERTEX << ";" << endl;
	for ( MGSize i=1; i<=mpGeT->cTopo().Size<DIM_0D>(); ++i)
	{
		file << "\t";
		file << "#" << i << " = ";
		(mpGeT->cTopo().cTEnt<DIM_0D>( i )).WriteDef( file);
		file << ";" << endl;
	}
	file << "\t" << MGString(GET_END) + "_" + MGString(GET_SEC_VERTEX) << ";" << endl;
	file << endl;

	// EDGE section
	file << "\t" << GET_SEC_EDGE << ";" << endl;
	for ( MGSize i=1; i<=mpGeT->cTopo().Size<DIM_1D>(); ++i)
	{
		file << "\t";
		file << "#" << i << " = ";
		(mpGeT->cTopo().cTEnt<DIM_1D>( i )).WriteDef( file);
		file << ";" << endl;
	}
	file << "\t" << MGString(GET_END) + "_" + MGString(GET_SEC_EDGE) << ";" << endl;
	file << endl;

	// FACE section
	file << "\t" << GET_SEC_FACE << ";" << endl;
	for ( MGSize i=1; i<=mpGeT->cTopo().Size<DIM_2D>(); ++i)
	{
		file << "\t";
		file << "#" << i << " = ";
		(mpGeT->cTopo().cTEnt<DIM_2D>( i )).WriteDef( file);
		file << ";" << endl;
	}
	file << "\t" << MGString(GET_END) + "_" + MGString(GET_SEC_FACE) << ";" << endl;
	file << endl;


	file << MGString(GET_END) + "_" + MGString(GET_TOPOLOGY) << ";" << endl;
	file << endl;
}

template <>
void GeoTopoIO<DIM_3D>::WriteTopologySection( ostream& file)
{
	file << GET_TOPOLOGY << ";" << endl;
	file << endl;

	// VERTEX section
	file << "\t" << GET_SEC_VERTEX << ";" << endl;
	for ( MGSize i=1; i<=mpGeT->cTopo().Size<DIM_0D>(); ++i)
	{
		file << "\t";
		file << "#" << i << " = ";
		(mpGeT->cTopo().cTEnt<DIM_0D>( i )).WriteDef( file);
		file << ";" << endl;
	}
	file << "\t" << MGString(GET_END) + "_" + MGString(GET_SEC_VERTEX) << ";" << endl;
	file << endl;

	// EDGE section
	file << "\t" << GET_SEC_EDGE << ";" << endl;
	for ( MGSize i=1; i<=mpGeT->cTopo().Size<DIM_1D>(); ++i)
	{
		file << "\t";
		file << "#" << i << " = ";
		(mpGeT->cTopo().cTEnt<DIM_1D>( i )).WriteDef( file);
		file << ";" << endl;
	}
	file << "\t" << MGString(GET_END) + "_" + MGString(GET_SEC_EDGE) << ";" << endl;
	file << endl;

	// FACE section
	file << "\t" << GET_SEC_FACE << ";" << endl;
	for ( MGSize i=1; i<=mpGeT->cTopo().Size<DIM_2D>(); ++i)
	{
		file << "\t";
		file << "#" << i << " = ";
		(mpGeT->cTopo().cTEnt<DIM_2D>( i )).WriteDef( file);
		file << ";" << endl;
	}
	file << "\t" << MGString(GET_END) + "_" + MGString(GET_SEC_FACE) << ";" << endl;
	file << endl;

	// BREP section
	file << "\t" << GET_SEC_BREP << ";" << endl;
	for ( MGSize i=1; i<=mpGeT->cTopo().Size<DIM_3D>(); ++i)
	{
		file << "\t";
		file << "#" << i << " = ";
		(mpGeT->cTopo().cTEnt<DIM_3D>( i )).WriteDef( file);
		file << ";" << endl;
	}
	file << "\t" << MGString(GET_END) + "_" + MGString(GET_SEC_BREP) << ";" << endl;
	file << endl;


	file << MGString(GET_END) + "_" + MGString(GET_TOPOLOGY) << ";" << endl;
	file << endl;
}

template <Dimension DIM>
void GeoTopoIO<DIM>::DoWrite( const MGString& fname)
{
	ofstream file;
	istringstream is;

	MGString	skey, sname, sver;

	file.open( fname.c_str(), ios::out );
	try
	{
		if ( ! file)
			THROW_FILE( "can not open the file", fname);

		cout << "writing GeT file (current ver: '" << VerStr() << " " << VerNum() << "') : '" << fname << "'\n";


		WriteHeader( file);
		WriteGeometrySection( file);
		WriteTopologySection( file);

		cout << "end of writing GeT file : '" << fname << "'\n";

	}
	catch ( EHandler::Except& e)
	{
		cout << "ERROR: GeoTopoIO<DIM>::DoWrite - file name: " << fname << endl << endl;
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);

		THROW_INTERNAL("END");
	}
}



template <Dimension DIM>
void GeoTopoIO<DIM>::ExportCurvTEC( ostream& file)
{
	THROW_INTERNAL("GeoTopoIO<DIM>::ExportSurfTEC - only 3D version can be used");
}

template <>
void GeoTopoIO<DIM_3D>::ExportCurvTEC( ostream& file)
{
	for ( MGSize i=1; i<=mpGeT->cGeom().Size<DIM_1D>(); ++i)
	{
		(mpGeT->cGeom().cGEnt<DIM_1D>( i ))->ExportTEC( file);
	}
}

template <Dimension DIM>
void GeoTopoIO<DIM>::ExportSurfTEC( ostream& file)
{
	THROW_INTERNAL("GeoTopoIO<DIM>::ExportSurfTEC - only 3D version can be used");
}

template <>
void GeoTopoIO<DIM_3D>::ExportSurfTEC( ostream& file)
{
	for ( MGSize i=1; i<=mpGeT->cGeom().Size<DIM_2D>(); ++i)
	{
		(mpGeT->cGeom().cGEnt<DIM_2D>( i ))->ExportTEC( file);
	}
}


template <Dimension DIM>
void GeoTopoIO<DIM>::ExportTEC( const MGString& fname)
{
	ofstream file;
	istringstream is;

	MGString	skey, sname, sver;

	file.open( fname.c_str(), ios::out );
	try
	{
		if ( ! file)
			THROW_FILE( "can not open the file", fname);

		cout << "writing TEC file\n";


		ExportCurvTEC( file);
		//ExportSurfTEC( file);

		cout << "end of writing TEC file : '" << fname << "'\n";

	}
	catch ( EHandler::Except& e)
	{
		cout << "ERROR: GeoTopoIO<DIM>::DoWrite - file name: " << fname << endl << endl;
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);

		THROW_INTERNAL("END");
	}
}



template <Dimension DIM>
void GeoTopoIO<DIM>::RemoveUnreferenced()
{
	THROW_INTERNAL( "not implemented !!!");
}

template <>
void GeoTopoIO<DIM_3D>::RemoveUnreferenced()
{
	vector<MGSize> tabedg;
	tabedg.resize( mpGeT->cTopo().Size<DIM_1D>() + 1, 0);

	vector<MGSize> tabvrt;
	tabvrt.resize( mpGeT->cTopo().Size<DIM_0D>() + 1, 0);

	// find unreferenced edges and vertices
	for ( MGSize i=1; i<=mpGeT->cTopo().Size<DIM_2D>(); ++i)
	{
		// get face
		const TopoEntity<DIM_2D>& tent = mpGeT->cTopo().cTEnt<DIM_2D>( i );

		for ( MGSize iloop=0; iloop<tent.cTab().size(); ++iloop)
		{
			const GET::TopoLoop	&loop = tent.cTab()[iloop].first;

			for ( MGSize iedg=0; iedg<loop.size(); ++iedg)
			{
				// get edge
				const MGSize &ide = loop[iedg].first;
				const TopoEntity<DIM_1D>& eent = mpGeT->cTopo().cTEnt<DIM_1D>( ide );

				tabedg[ide] ++;

				// get vertices
				MGSize idv1 = eent.cTab()[0].first[0].first;
				MGSize idv2 = eent.cTab()[0].first[1].first;

				tabvrt[idv1] ++;
				tabvrt[idv2] ++;
			}
		}
	}


	TopoOwner<DIM_3D> town;// = mpGeT->mTOwner;

	MGSize id=0;
	for ( MGSize i=1; i<tabvrt.size(); ++i)
	{
		if ( tabvrt[i] != 0 )
		{
			tabvrt[i] = ++id;

			TopoEntity<DIM_0D> tent = mpGeT->cTopo().cTEnt<DIM_0D>( i );

			MGSize getid = town.PushBack( tent);

			if ( getid != id)
			{
				THROW_INTERNAL( "Crash inside GeoTopoIO<DIM_3D>::RemoveUnreferenced()");
			}
		}
	}



	id=0;
	for ( MGSize i=1; i<tabedg.size(); ++i)
	{
		if ( tabedg[i] != 0 )
		{
			tabedg[i] = ++id;

			TopoEntity<DIM_1D> tent = mpGeT->cTopo().cTEnt<DIM_1D>( i );

			MGSize idv1 = tent.cTab()[0].first[0].first;
			MGSize idv2 = tent.cTab()[0].first[1].first;

			tent.mtabLoop[0].first[0].first = tabvrt[idv1];
			tent.mtabLoop[0].first[1].first = tabvrt[idv2];

			MGSize getid = town.PushBack( tent);

			if ( getid != id)
			{
				THROW_INTERNAL( "Crash inside GeoTopoIO<DIM_3D>::RemoveUnreferenced()");
			}
		}
	}

	// update faces
	for ( MGSize i=1; i<=mpGeT->cTopo().Size<DIM_2D>(); ++i)
	{
		// get face
		TopoEntity<DIM_2D> tent = mpGeT->cTopo().cTEnt<DIM_2D>( i );

		for ( MGSize iloop=0; iloop<tent.cTab().size(); ++iloop)
		{
			TopoLoop	&loop = tent.mtabLoop[iloop].first;

			for ( MGSize iedg=0; iedg<loop.size(); ++iedg)
			{
				// get edge
				MGSize ide = loop[iedg].first;
				loop[iedg].first = tabedg[ide];

			}
		}

		MGSize getid = town.PushBack( tent);
	}

	mpGeT->mTOwner = town;

}


template <Dimension DIM>
void GeoTopoIO<DIM>::RemoveUnreferencedGeom()
{
	THROW_INTERNAL( "not implemented !!!");
}

template <>
void GeoTopoIO<DIM_3D>::RemoveUnreferencedGeom()
{
	vector<MGSize> tabfac, tabedg, tabvrt;

	tabfac.resize( mpGeT->cGeom().Size<DIM_2D>() + 1, 0);
	tabedg.resize( mpGeT->cGeom().Size<DIM_1D>() + 1, 0);
	tabvrt.resize( mpGeT->cGeom().Size<DIM_0D>() + 1, 0);

	// find unreferenced faces
	for ( MGSize i=1; i<=mpGeT->cTopo().Size<DIM_2D>(); ++i)
	{
		// get face
		const TopoEntity<DIM_2D>& tent = mpGeT->cTopo().cTEnt<DIM_2D>( i );
		tabfac[ tent.cGeomId() ] ++;
	}

	// find unreferenced edges
	for ( MGSize i=1; i<=mpGeT->cTopo().Size<DIM_1D>(); ++i)
	{
		// get edge
		const TopoEntity<DIM_1D>& tent = mpGeT->cTopo().cTEnt<DIM_1D>( i );
		tabedg[ tent.cGeomId() ] ++;
	}

	// find unreferenced vert
	for ( MGSize i=1; i<=mpGeT->cTopo().Size<DIM_0D>(); ++i)
	{
		// get vert
		const TopoEntity<DIM_0D>& tent = mpGeT->cTopo().cTEnt<DIM_0D>( i );
		tabvrt[ tent.cGeomId() ] ++;
	}


	mpGeT->mGOwner.RebuidTab<DIM_0D>( tabvrt);
	mpGeT->mGOwner.RebuidTab<DIM_1D>( tabedg);
	mpGeT->mGOwner.RebuidTab<DIM_2D>( tabfac);

	TopoOwner<DIM_3D> town;// = mpGeT->mTOwner;

	for ( MGSize i=1; i<=mpGeT->cTopo().Size<DIM_0D>(); ++i)
	{
		TopoEntity<DIM_0D> tent = mpGeT->cTopo().cTEnt<DIM_0D>( i );
		tent.mGeomEntId = tabvrt[ tent.cGeomId() ];

		MGSize getid = town.PushBack( tent);

		if ( getid != i)
		{
			THROW_INTERNAL( "Crash inside GeoTopoIO<DIM_3D>::RemoveUnreferencedGeom()");
		}
	}

	for ( MGSize i=1; i<=mpGeT->cTopo().Size<DIM_1D>(); ++i)
	{
		TopoEntity<DIM_1D> tent = mpGeT->cTopo().cTEnt<DIM_1D>( i );
		tent.mGeomEntId = tabedg[ tent.cGeomId() ];

		MGSize getid = town.PushBack( tent);

		if ( getid != i)
		{
			THROW_INTERNAL( "Crash inside GeoTopoIO<DIM_3D>::RemoveUnreferencedGeom()");
		}
	}

	for ( MGSize i=1; i<=mpGeT->cTopo().Size<DIM_2D>(); ++i)
	{
		TopoEntity<DIM_2D> tent = mpGeT->cTopo().cTEnt<DIM_2D>( i );
		tent.mGeomEntId = tabfac[ tent.cGeomId() ];

		MGSize getid = town.PushBack( tent);

		if ( getid != i)
		{
			THROW_INTERNAL( "Crash inside GeoTopoIO<DIM_3D>::RemoveUnreferencedGeom()");
		}
	}

	mpGeT->mTOwner = town;
}

template <Dimension DIM>
void GeoTopoIO<DIM>::ExtractFaces( const vector<MGSize> tabfid )
{
	THROW_INTERNAL( "not implemented !!!");
}

template <>
void GeoTopoIO<DIM_3D>::ExtractFaces( const vector<MGSize> tabfid )
{
	vector<MGSize> tabedg;
	tabedg.resize( mpGeT->cTopo().Size<DIM_1D>() + 1, 0);

	vector<MGSize> tabvrt;
	tabvrt.resize( mpGeT->cTopo().Size<DIM_0D>() + 1, 0);

	// find unwanted edges and vertices
	for ( MGSize i=0; i<tabfid.size(); ++i)
	{
		// get face
		const TopoEntity<DIM_2D>& tent = mpGeT->cTopo().cTEnt<DIM_2D>( tabfid[i] );

		for ( MGSize iloop=0; iloop<tent.cTab().size(); ++iloop)
		{
			const GET::TopoLoop	&loop = tent.cTab()[iloop].first;

			for ( MGSize iedg=0; iedg<loop.size(); ++iedg)
			{
				// get edge
				const MGSize &ide = loop[iedg].first;
				const TopoEntity<DIM_1D>& eent = mpGeT->cTopo().cTEnt<DIM_1D>( ide );

				tabedg[ide] ++;

				// get vertices
				MGSize idv1 = eent.cTab()[0].first[0].first;
				MGSize idv2 = eent.cTab()[0].first[1].first;

				tabvrt[idv1] ++;
				tabvrt[idv2] ++;
			}
		}
	}


	TopoOwner<DIM_3D> town;// = mpGeT->mTOwner;

	MGSize id=0;
	for ( MGSize i=1; i<tabvrt.size(); ++i)
	{
		if ( tabvrt[i] != 0 )
		{
			tabvrt[i] = ++id;

			TopoEntity<DIM_0D> tent = mpGeT->cTopo().cTEnt<DIM_0D>( i );

			MGSize getid = town.PushBack( tent);

			if ( getid != id)
			{
				THROW_INTERNAL( "Crash inside GeoTopoIO<DIM_3D>::RemoveUnreferenced()");
			}
		}
	}



	id=0;
	for ( MGSize i=1; i<tabedg.size(); ++i)
	{
		if ( tabedg[i] != 0 )
		{
			tabedg[i] = ++id;

			TopoEntity<DIM_1D> tent = mpGeT->cTopo().cTEnt<DIM_1D>( i );

			MGSize idv1 = tent.cTab()[0].first[0].first;
			MGSize idv2 = tent.cTab()[0].first[1].first;

			tent.mtabLoop[0].first[0].first = tabvrt[idv1];
			tent.mtabLoop[0].first[1].first = tabvrt[idv2];

			MGSize getid = town.PushBack( tent);

			if ( getid != id)
			{
				THROW_INTERNAL( "Crash inside GeoTopoIO<DIM_3D>::RemoveUnreferenced()");
			}
		}
	}

	// update faces
	//for ( MGSize i=1; i<=mpGeT->cTopo().Size<DIM_2D>(); ++i)
	for ( MGSize i=0; i<tabfid.size(); ++i)
	{
		// get face
		TopoEntity<DIM_2D> tent = mpGeT->cTopo().cTEnt<DIM_2D>( tabfid[i] );

		for ( MGSize iloop=0; iloop<tent.cTab().size(); ++iloop)
		{
			TopoLoop	&loop = tent.mtabLoop[iloop].first;

			for ( MGSize iedg=0; iedg<loop.size(); ++iedg)
			{
				// get edge
				MGSize ide = loop[iedg].first;
				loop[iedg].first = tabedg[ide];

			}
		}

		MGSize getid = town.PushBack( tent);
	}

	mpGeT->mTOwner = town;

	RemoveUnreferencedGeom();
}




template class GeoTopoIO<DIM_2D>;
template class GeoTopoIO<DIM_3D>;


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GETSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

