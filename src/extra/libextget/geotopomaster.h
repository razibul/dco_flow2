#ifndef __GEOTOPOMASTER_H__
#define __GEOTOPOMASTER_H__

#include "geomowner.h"
#include "topoowner.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GETSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


template <Dimension DIM>
class GeoTopoIO;

class ConvertStep;


//////////////////////////////////////////////////////////////////////
//	class GeoTopoMaster
//////////////////////////////////////////////////////////////////////
template <Dimension DIM>
class GeoTopoMaster
{
	friend class GeoTopoIO<DIM>;
	friend class ConvertStep;

public:
	GeoTopoMaster()		{}

	const TopoOwner<DIM>&		cTopo() const	{ return mTOwner;}
	const GeomOwner<DIM,DIM>&	cGeom() const	{ return mGOwner;}

	const MGString&	cName() const				{ return mName;}
	const MGString&	cDescription()const			{ return mDescription;}

	void	SyncNormals();

protected:
	MGString&	rName()				{ return mName;}
	MGString&	rDescription()		{ return mDescription;}

	TopoOwner<DIM>&		rTopo()		{ return mTOwner;}
	GeomOwner<DIM,DIM>&	rGeom()		{ return mGOwner;}

private:
	MGString	mName;
	MGString	mDescription;

	GeomOwner<DIM,DIM>	mGOwner;
	TopoOwner<DIM>		mTOwner;

};

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GETSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace GET = GETSpace;


#endif // __GEOTOPOMASTER_H__
