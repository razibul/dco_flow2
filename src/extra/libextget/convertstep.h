#ifndef __CONVERTSTEP_H__
#define __CONVERTSTEP_H__

#include "libcoresystem/mgdecl.h"
#include "libextstep/stepmaster.h"
#include "libextget/geotopomaster.h"




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GETSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


//////////////////////////////////////////////////////////////////////
// class ConvertStep
//////////////////////////////////////////////////////////////////////
class ConvertStep
{
	typedef map<MGSize,MGInt>	ConvertMap;
public:
	ConvertStep( Step::StepMaster* pstep=NULL, GeoTopoMaster<DIM_3D>* pget=NULL) : mpStep(pstep), mpGeT(pget)	{}

	void	DoConvert();

protected:
	void	InitMapId();

	void	BuildVertexTab();
	void	BuildEdgeTab();
	void	BuildFaceTab();
	void	BuildBRepTab();

	MGSize	CreateVertexPnt( const MGSize& stepid);
	MGSize	CreateEdgeCrv( const MGSize& stepid);
	MGSize	CreateFaceSrf( const MGSize& stepid);
	MGSize	CreateBRep( const MGSize& stepid);
	MGSize	CreateBRepVoids( const MGSize& stepid);


	MGSize	CreatePoint( const MGSize& stepid);
	MGSize	CreateCurve( const MGSize& stepid);
	MGSize	CreateSurface( const MGSize& stepid);


	MGSize	CreateLine( const MGSize& stepid);
	MGSize	CreateCircle( const MGSize& stepid);
	MGSize	CreateEllipse( const MGSize& stepid);
	MGSize	CreateCrvBSplineWKnots( const MGSize& stepid);
	MGSize	CreateCrvRationalBSpline( const MGSize& stepid);

	MGSize	CreatePlane( const MGSize& stepid);
	MGSize	CreateSrfBSplineWKnots( const MGSize& stepid);
	MGSize	CreateSrfRationalBSpline( const MGSize& stepid);
	MGSize	CreateCylindricalSrf( const MGSize& stepid);
	MGSize	CreateConicalSrf( const MGSize& stepid);
	MGSize	CreateSphericalSrf( const MGSize& stepid);
	MGSize	CreateToroidalSrf( const MGSize& stepid);
	MGSize	CreateExtrusionSrf( const MGSize& stepid);
	
	Vect3D	GetStpDirection( const MGSize& stepid);
	Vect3D	GetStpVector( const MGSize& stepid);
	Vect3D	GetStpPointCoor( const MGSize& stepid);

	void	BuildAxes( const MGSize& stepidAxis, const MGSize& stepidRefDir, Vect3D& vOX, Vect3D& vOY, Vect3D& vOZ);


	MGInt	GetId( const MGSize& stepid) const;
	void	UpdateId( const MGSize& stepid, const MGSize& getid);


	const Step::StepEntity*		GetStepEnt( const MGSize& id);
	
	template <class T>
	const T*	GetStepEntity( const MGSize& id);

	template <class T>
	T* Create( const MGString& sname, const MGString& sparams);

	//Vect3D GetStepDirection( const MGSize& stepid );
	//Vect3D GetStepVector( const MGSize& stepid );
	//Vect3D GetStepPointCoordinates( const MGSize& stepid );


private:
	Step::StepMaster		*mpStep;
	GeoTopoMaster<DIM_3D>	*mpGeT;

	ConvertMap	mmapId;
};
//////////////////////////////////////////////////////////////////////

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GETSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace GET = GETSpace;



#endif // __CONVERTSTEP_H__
