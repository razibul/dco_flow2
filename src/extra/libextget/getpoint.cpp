#include "getpoint.h"

#include "libcoreio/store.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GETSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


template <Dimension DIM>
void Point<DIM>::Init( const MGString& sname, MGString& sparams)	
{
	if ( sname != ClassName())
	{
		MGString buf = MGString( "expected : '") + MGString( ClassName()) + MGString( "' not a: ") + sname;
		THROW_FILE( buf.c_str(), "" );
	}

	vector<MGFloat> tab;

	IO::ReadTabFloat( tab, sparams );
	if ( tab.size() != DIM)
		THROW_INTERNAL( "DIM coordinates expected" );

	for ( MGSize k=0; k<DIM; ++k)
		mVect.rX(k) = tab[k];

	sparams = "";
}

template <Dimension DIM>
void Point<DIM>::WriteDef( ostream& of) const
{
	of << setprecision(16); 
	of << ClassName() << "( ";

	of << mVect.cX(0);
	for ( MGSize k=1; k<DIM; ++k)
		of << ", " << mVect.cX(k);

	of << " )";
}


template class Point<DIM_2D>;
template class Point<DIM_3D>;



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GETSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
