#ifndef __GEOMENTITY_H__
#define __GEOMENTITY_H__

#include "libcorecommon/entity.h"
#include "libcoregeom/vect.h"
#include "libcoregeom/metric.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace GETSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

using namespace Geom;


//////////////////////////////////////////////////////////////////////
//	class GeomEntity
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, Dimension UNIDIM>
class GeomEntity : public Entity
{
public:
	typedef Vect<UNIDIM>	UVect;
	typedef Vect<DIM>		GVect;
	typedef Metric<DIM>		GMetric;

public:
	enum { SIZE = DIM*(DIM+1)/2};

	GeomEntity() : Entity()			{}

	static const char*	ClassName()								{ return "?";}

	virtual MGString	GetName() const							{ return ClassName();};
	virtual bool		IsType( const MGString& type) const		{ return false;}

	virtual void		Init( const MGString& sname, MGString& sparams)			= 0;
	virtual void		WriteDef( ostream& of) const 							= 0;


	virtual bool		IsPeriodic( const MGSize& idir) const					= 0;
	virtual MGFloat		GetPeriod( const MGSize& idir) const					= 0;

	virtual GVect		FindParam( const UVect& vct) const 						= 0;

	virtual void		GetCoord( UVect& vret, const GVect& vct) const 			= 0;
	virtual void		GetFstDeriv( UVect tab[DIM], const GVect& vct) const	= 0;
	virtual void		GetSndDeriv( UVect tab[SIZE], const GVect& vct) const	= 0;

	virtual void		Eval( UVect& vret, UVect tab1[DIM], UVect tab2[SIZE], const GVect& vct) const	= 0;

	virtual void		ExportTEC( ostream& os) const	
		{ 
			cout << "GeomEntity<>::ExportTEC Not implemented" << endl;
			//THROW_INTERNAL("GeomEntity<>::ExportTEC Not implemented");
		}

private:
};

//////////////////////////////////////////////////////////////////////
//	class NullGeomEntity
//////////////////////////////////////////////////////////////////////
template <Dimension DIM, Dimension UNIDIM>
class NullGeomEntity : public GeomEntity<DIM,UNIDIM>
{
public:
	typedef Vect<UNIDIM>	UVect;
	typedef Vect<DIM>		GVect;
	typedef Metric<DIM>		GMetric;

public:
	enum { SIZE = DIM*(DIM+1)/2};

	NullGeomEntity()			{}

	virtual void		Init( const MGString& sname, MGString& sparams)			{}
	virtual void		WriteDef( ostream& of) const 							{};


	virtual bool		IsPeriodic( const MGSize& idir) const					{ return false;}
	virtual MGFloat		GetPeriod( const MGSize& idir) const					{ return 0;}

	virtual GVect		FindParam( const UVect& vct) const 						
	{ 
		GVect	gvct;
		for ( MGSize i=0; i<DIM; ++i)
			gvct.rX(i) = vct.cX(i);
		return gvct;
	}

	virtual void		GetCoord( UVect& vret, const GVect& vct) const 			
	{ 
		vret = UVect(0.0);
		for ( MGSize i=0; i<DIM; ++i)
			vret.rX(i) = vct.cX(i);
	}

	virtual void		GetFstDeriv( UVect tab[DIM], const GVect& vct) const
	{
		for ( MGSize i=0; i<DIM; ++i)
			tab[i] = 1.0;
	}

	virtual void		GetSndDeriv( UVect tab[SIZE], const GVect& vct) const
	{
		for ( MGSize i=0; i<SIZE; ++i)
			tab[i] = 0.0;
	}

	virtual void		Eval( UVect& vret, UVect tab1[DIM], UVect tab2[SIZE], const GVect& vct) const
	{
		for ( MGSize i=0; i<DIM; ++i)
			tab1[i] = 1.0;

		for ( MGSize i=0; i<SIZE; ++i)
			tab2[i] = 0.0;
	}

};



//////////////////////////////////////////////////////////////////////
//	class GeomEntity - specialization for DIM = DIM_0D   ( Point )
//////////////////////////////////////////////////////////////////////
template <Dimension UNIDIM>
class GeomEntity<DIM_0D,UNIDIM> : public Entity
{
public:
	typedef Vect<UNIDIM>	UVect;

public:
	GeomEntity() : Entity()			{}

	virtual void		Init( const MGString& sname, MGString& sparams)			= 0;
	virtual void		WriteDef( ostream& of) const 							= 0;

	virtual void		GetCoord( UVect& vret) const 				= 0;

private:
};


//////////////////////////////////////////////////////////////////////

typedef GeomEntity<DIM_0D, DIM_2D>	Point2D;
typedef GeomEntity<DIM_1D, DIM_2D>	Curve2D;

typedef GeomEntity<DIM_0D, DIM_3D>	Point3D;
typedef GeomEntity<DIM_1D, DIM_3D>	Curve3D;
typedef GeomEntity<DIM_2D, DIM_3D>	Surface;


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace GETSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace GET = GETSpace;


#endif // __GEOMENTITY_H__
