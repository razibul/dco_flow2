
LIST ( APPEND metis_files
	balance.c
	bucketsort.c
	ccgraph.c
	coarsen.c
	compress.c
	debug.c
	defs.h
	estmem.c
	fm.c
	fortran.c
	frename.c
	graph.c
	initpart.c
	kmetis.c
	kvmetis.c
	kwayfm.c
	kwayrefine.c
	kwayvolfm.c
	kwayvolrefine.c
	macros.h
	match.c
	mbalance.c
	mbalance2.c
	mcoarsen.c
	memory.c
	mesh.c
	meshpart.c
	metis.h
	mfm.c
	mfm2.c
	mincover.c
	minitpart.c
	minitpart2.c
	mkmetis.c
	mkwayfmh.c
	mkwayrefine.c
	mmatch.c
	mmd.c
	mpmetis.c
	mrefine.c
	mrefine2.c
	mutil.c
	myqsort.c
	ometis.c
	parmetis.c
	pmetis.c
	pqueue.c
	proto.h
	refine.c
	rename.h
	separator.c
	sfm.c
	srefine.c
	stat.c
	struct.h
	subdomains.c
	timing.c
	util.c 
) 

INCLUDE_DIRECTORIES (
  ${FLOW2_SOURCE_DIR}/${NAME_CORE}
  ${FLOW2_SOURCE_DIR}/${NAME_AUX}
) 

ADD_LIBRARY ( metis ${metis_files} )
