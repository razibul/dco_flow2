#include "stepmaster.h"
#include "libextstep/stepconst.h"
#include "libextstep/stepfactory.h"
#include "libextstep/stepgeometry.h"
#include "libextstep/steptopology.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace StepSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


StepMaster::StepMaster()
{
	mRex.InitPattern( "^#([0-9]*)=([_A-Z0-9]*)\\((.*)\\);$");

	mtabFactory.push_back( new Factory<StepDirection> );
	mtabFactory.push_back( new Factory<StepVector> );
	mtabFactory.push_back( new Factory<StepAxis1Placement> );
	mtabFactory.push_back( new Factory<StepAxis2Placement3D> );
	mtabFactory.push_back( new Factory<StepCartesianPoint> );

	mtabFactory.push_back( new Factory<StepLine> );
	mtabFactory.push_back( new Factory<StepCircle> );
	mtabFactory.push_back( new Factory<StepEllipse> );
	mtabFactory.push_back( new Factory<StepBSplineCurve> );
	mtabFactory.push_back( new Factory<StepBSplineCurveWithKnots> );
	mtabFactory.push_back( new Factory<StepQuasiUniformCurve> );
	mtabFactory.push_back( new Factory<StepRationalBSplineCurve> );
	
	mtabFactory.push_back( new Factory<StepPlane> );
	mtabFactory.push_back( new Factory<StepCylindricalSurface> );
	mtabFactory.push_back( new Factory<StepConicalSurface> );
	mtabFactory.push_back( new Factory<StepSphericalSurface> );
	mtabFactory.push_back( new Factory<StepToroidalSurface> );
	mtabFactory.push_back( new Factory<StepSurfaceOfRevolution> );
	mtabFactory.push_back( new Factory<StepSurfaceOfLinearExtrusion> );
	mtabFactory.push_back( new Factory<StepBSplineSurface> );
	mtabFactory.push_back( new Factory<StepBSplineSurfaceWithKnots> );
	mtabFactory.push_back( new Factory<StepQuasiUniformSurface> );
	mtabFactory.push_back( new Factory<StepRationalBSplineSurface> );

	mtabFactory.push_back( new Factory<StepVertexPoint> );
	mtabFactory.push_back( new Factory<StepVertexLoop> );
	mtabFactory.push_back( new Factory<StepEdgeCurve> );
	mtabFactory.push_back( new Factory<StepOrientedEdge> );
	mtabFactory.push_back( new Factory<StepEdgeLoop> );
	mtabFactory.push_back( new Factory<StepFaceBound> );
	mtabFactory.push_back( new Factory<StepFaceOuterBound> );

	mtabFactory.push_back( new Factory<StepAdvancedFace> );
	mtabFactory.push_back( new Factory<StepOpenShell> );
	mtabFactory.push_back( new Factory<StepClosedShell> );
	mtabFactory.push_back( new Factory<StepOrientedClosedShell> );

	mtabFactory.push_back( new Factory<StepManifoldSolidBRep> );
	mtabFactory.push_back( new Factory<StepBRepWithVoids> );
	mtabFactory.push_back( new Factory<StepShellBasedSurfaceModel> );

	mtabFactory.push_back( new Factory<StepUnknownObject> );
	

	cout << endl;
	for ( MGSize i=0; i<mtabFactory.size(); ++i)
	{
		cout << mtabFactory[i]->ObjectName() << endl;
	}
	cout << endl;
}

StepMaster::~StepMaster()
{
	for ( MGSize i=0; i<mtabFactory.size(); ++i)
		if ( mtabFactory[i] )
			delete mtabFactory[i];

}


void StepMaster::WriteDotGraph( const MGString& name, const MGString& stepname, const MGString& type)
{
	ofstream ofile;

	ofile.open( name.c_str(), ios::out );

	MGString colorTopo = "palegreen";
	MGString colorGlobal = "\".7 .3 1.0\"";

	try
	{
		if ( ! ofile)
			THROW_FILE( "can not open the file", name);

		ofile << "digraph G {" << endl
			 << "graph [	fontname = \"Helvetica-Oblique\"," << endl
			 << "fontsize = 36," << endl
			 << "label = \"\\nSTEP Dependencies Graph: " << stepname <<"\"," << endl
			 << "oriantation = landscape," << endl
			 << "size = \"8,11\"," << endl
			 << "nodesep = 0.25" << endl
			 << "rotate=90," << endl
			 << "ranksep = 3.0," << endl
			 << "]; " << endl
			 << "node [	" << endl
			 << "fontname = Arial," << endl
			 << "fontsize = 30," << endl
			 << "shape = box," << endl
			 << "style=filled," << endl
			 << "color=" << colorGlobal << "," << endl
			 << "width = 7.5," << endl
			 << "height = 1 ];" << endl
			 << "edge [	" << endl
			 << "arrowsize = 2 ];" << endl;

		for ( StepMap::iterator itr=mMap.begin(); itr != mMap.end(); ++itr)
		{
			const StepEntity*	ptr = (*itr).second.cpObject();

			if ( ptr )//&& (*itr).second.cFlag() )
			{
				if ( ptr->IsType( type ) )
				{
					vector<MGSize> tabid;
					
					ptr->GetDotDepend( tabid);

					if ( ptr->IsType( STP_TOPOLOGIC_REPRESENTATION_ITEM ) )
					{
							ofile << "\"#" << (*itr).first << "\\n" << ptr->GetStepName() << "\"";
							ofile << "[shape=box,style=filled,color=\"" << colorTopo << "\"];" << endl;
					}

					for ( vector<MGSize>::iterator itab=tabid.begin(); itab!=tabid.end(); ++itab)
					{
						StepMap::iterator iref;
						if ( (iref = mMap.find( *itab)) != mMap.end() )
						{
							ofile << "\"#" << (*itr).first << "\\n" << (*itr).second.cpObject()->GetStepName() << "\"";
							ofile << " -> ";
							if ( (*iref).second.cpObject() )
								ofile << "\"#" << (*iref).first << "\\n" << (*iref).second.cpObject()->GetStepName() << "\"";
							else
								ofile << "\"#" << (*iref).first << "\\n" << (*iref).second.cName() << " (?)\"";
							ofile << endl;
						}
						else
							THROW_INTERNAL( "refereneced id not found !");
					}
				}

			}
		}


		ofile << "}" << endl;


	}
	catch ( EHandler::Except& e)
	{
		cout << "ERROR: StepMaster::WriteDotGraph - file name: " << name << endl << endl;
		TRACE_EXCEPTION( e);
		TRACE_TO_STDERR( e);

		THROW_INTERNAL("END");
	}
}



void StepMaster::RemoveUnreferencedObj( const MGSize& id)
{
	vector<MGSize> tabid;
	StepMap::iterator itr;

	// searching for topmost entities
	if ( !id)
		for ( itr=mMap.begin(); itr != mMap.end(); ++itr)
		{
			if ( (*itr).second.cName() == STP_MANIFOLD_SOLID_BREP ||
				 (*itr).second.cName() == STP_BREP_WITH_VOIDS ||
				 (*itr).second.cName() == STP_SHELL_BASED_SURFACE_MODEL )
				 tabid.push_back( (*itr).first );
		}
	else
		tabid.push_back( id );

	//tabid.push_back( 13297 );
	//tabid.push_back( 13340 );
	//tabid.push_back( 16395 );
	//tabid.push_back( 17032 );
	//tabid.push_back( 14044 );

	// flaging all entities used for domain definition
	do
	{
		vector<MGSize> tabtmp;

		for ( MGSize i=0; i<tabid.size(); ++i)
		{
			if ( (itr=mMap.find( tabid[i])) == mMap.end() )
				THROW_INTERNAL( "refereneced id not found !");

			if ( (*itr).second.cpObject() == NULL)
				THROW_INTERNAL( "object does not exist !");

			(*itr).second.cpObject()->GetDotDepend( tabtmp);
			(*itr).second.rFlag() = true;
		}

		MGSize n = tabid.size();
		tabid = tabtmp;
		n = tabid.size();
	}
	while ( tabid.size() > 0);


	// removing unflaged entities
	vector<StepMap::iterator>	taber;

	for ( itr=mMap.begin(); itr != mMap.end(); ++itr)
		if ( ! (*itr).second.cFlag() )
			taber.push_back( itr);

	cout << "mMap size before = " << mMap.size() << endl;
	for ( MGSize i=0; i< taber.size(); ++i)
		mMap.erase( taber[i] );
	cout << "mMap size after = " << mMap.size() << endl;
}




void StepMaster::CreateStepObjects()
{
	for ( StepMap::iterator itr=mMap.begin(); itr != mMap.end(); ++itr)
	{
		StepEntity*	ptr = NULL;

		for ( MGSize i=0; i<mtabFactory.size(); ++i)
		{
			ptr = mtabFactory[i]->Create( (*itr).first, (*itr).second.cName(), (*itr).second.cParams() );
			if ( ptr )
			{
				(*itr).second.SetObjectPtr( ptr);
				break;
			}
		}
	}

	//return;


	// find unrecognized STEP entities

	set<MGString>	tab;

	for ( StepMap::iterator itr=mMap.begin(); itr != mMap.end(); ++itr)
	{
		if ( (*itr).second.cpObject() == NULL)
			tab.insert( (*itr).second.cName() );
	}

	cout << endl << "unknown STEP entities:" << endl;
	for ( set<MGString>::iterator itr = tab.begin(); itr != tab.end(); ++itr)
		cout << (*itr) << endl;

}



void StepMaster::Read( const MGString& name)
{
	ifstream	file( name.c_str());

	if ( ! file.good() )
		THROW_FILE( "could not open the file", name.c_str() );

	MGString buf;

	// ISO-10303-21
	ReadLine( buf, file);

	if ( buf != MGString(STP_ISO) + ";" )
	{
		buf = MGString( "STEP file should start with '") + MGString( STP_ISO) + MGString( "'; ");
		THROW_FILE( buf.c_str(), name.c_str() );
	}

	ReadHeader( file, name );
	ReadData( file, name );

	// END_ISO-10303-21
	ReadLine( buf, file);

	if ( buf != MGString(STP_END_ISO) + ";" )
	{
		buf = MGString( "STEP file should end with '") + MGString( STP_END_ISO) + MGString( "'; ");
		THROW_FILE( buf.c_str(), name.c_str() );
	}

	cout << "Ready; map size = " << mMap.size() << endl;

	mName = name;

	return;

}




bool StepMaster::ReadLine( MGString& buf, istream& file)
{
	char	ch, chp;
	bool	bCom = false;
	bool	bStr = false;

	ch = chp = '\0';
	buf.clear();

	file.get( ch);
	while ( ! file.eof() )
	{
		if ( chp == '/' && ch == '*' )
		{
			bCom = true;
			buf.erase( buf.end()-1);
		}


		if ( !bCom )
		{
			if ( ch == '\'' && !bStr )
				bStr = true;
			else
				if ( ch == '\'' && bStr )
					bStr = false;

			if ( bStr)
			{
				//if ( ch == '\n')
				//	THROW_FILE( "ERROR: new line character in the string constant", name.c_str() );

				if ( ch != '\n' && ch != '\r')
					buf.push_back( ch);
			}
			else
			{
				if ( ch != '\n' && ch != '\r' && ch != ' ' && ch != '\t')
					buf.push_back( ch);

				if ( ch == ';')
					return (!file.eof());
			}

		}


		if ( chp == '*' && ch == '/' )
			bCom = false;


		chp = ch;

		file.get( ch);

	}

	return (!file.eof());
}



void StepMaster::ReadHeader( istream& file, const MGString& name)
{
	MGString	buf, str1, str2;
	RegExp		rex;

	rex.InitPattern( "^([_A-Z0-9]*)\\((.*)\\);");


	// HEADER
	ReadLine( buf, file);

	if ( buf != MGString(STP_HEADER) + ";" )
	{
		buf = MGString( "expected : '") + MGString( STP_HEADER) + MGString( "'; ");
		THROW_FILE( buf.c_str(), name.c_str() );
	}

	// FILE_DESCRIPTION
	ReadLine( buf, file);
	cout << buf << endl;

	rex.SetString( buf);
	if ( rex.IsOk() )
	{
		str1 = rex.GetSubString( 1);
		str2 = rex.GetSubString( 2);
	}

	if ( str1 != MGString(STP_FILE_DESCR) )
	{
		buf = MGString( "expected : '") + MGString( STP_FILE_DESCR) + MGString( "'; ");
		THROW_FILE( buf.c_str(), name.c_str() );
	}

	// FILE_NAME
	ReadLine( buf, file);
	cout << buf << endl;

	rex.SetString( buf);
	if ( rex.IsOk() )
	{
		str1 = rex.GetSubString( 1);
		str2 = rex.GetSubString( 2);
	}

	if ( str1 != MGString(STP_FILE_NAME) )
	{
		buf = MGString( "expected : '") + MGString( STP_FILE_NAME) + MGString( "'; ");
		THROW_FILE( buf.c_str(), name.c_str() );
	}

	// FILE_SCHEMA
	ReadLine( buf, file);
	cout << buf << endl;

	rex.SetString( buf);
	if ( rex.IsOk() )
	{
		str1 = rex.GetSubString( 1);
		str2 = rex.GetSubString( 2);
	}

	if ( str1 != MGString(STP_FILE_SCHEMA) )
	{
		buf = MGString( "expected : '") + MGString( STP_FILE_SCHEMA) + MGString( "'; ");
		THROW_FILE( buf.c_str(), name.c_str() );
	}

	// ENDSEC
	ReadLine( buf, file);

	if ( buf != MGString(STP_ENDSEC) + ";" )
	{
		buf = MGString( "expected : '") + MGString( STP_ENDSEC) + MGString( "'; ");
		THROW_FILE( buf.c_str(), name.c_str() );
	}

}

void StepMaster::ReadData( istream& file, const MGString& name)
{

	MGString	buf;

	// DATA
	ReadLine( buf, file);

	if ( buf != MGString(STP_DATA) + ";" )
	{
		buf = MGString( "expected : '") + MGString( STP_DATA) + MGString( "'; ");
		THROW_FILE( buf.c_str(), name.c_str() );
	}


	// read DATA section
	while ( ReadLine( buf, file) )
	{
		//cout << buf << endl;
		if ( buf == MGString(STP_ENDSEC) + ";" )
			return;

		AddToMap( buf);//cout << buf << endl;

	}

	buf = MGString( "expected : '") + MGString( STP_ENDSEC) + MGString( "'; ");
	THROW_FILE( buf.c_str(), name.c_str() );

}



// adds to map new description based on buf
void StepMaster::AddToMap( const MGString& buf)
{
	MGString	str1, str2, str3;

	mRex.SetString( buf);

	if ( mRex.IsOk() )
	{
		ASSERT( mRex.GetNoSubString() == 3);
		int k = mRex.GetNoSubString();
		str1 = mRex.GetSubString( 1);
		str2 = mRex.GetSubString( 2);
		str3 = mRex.GetSubString( 3);

		MGInt 			id;

		sscanf( str1.c_str(), "%d", &id);
		
		//if ( id > mMaxKey)
		//	mMaxKey = id;
			
		if ( mMap.find( id) != mMap.end() )
			THROW_FILE( "duplicate identificator", buf);

		mMap.insert( StepMap::value_type( id, StepDef( id, str2, str3 ) ) );
	}
	else
		THROW_FILE( "parsing error", buf);

}



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace StepSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
