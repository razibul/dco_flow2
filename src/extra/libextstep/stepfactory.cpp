#include "stepfactory.h"
#include "libextstep/stepgeometry.h"

#include "libcorecommon/regexp.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace StepSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


MGInt Factory<StepUnknownObject>::GetId( const MGString& sname, const vector< pair<MGString,MGString> >& tab)
{
	for ( MGInt i=0; i<(MGInt)tab.size(); ++i)
		if ( tab[i].first == sname)
			return i;

	return -1;
}

//RATIONAL_B_SPLINE_CURVE
StepEntity* Factory<StepUnknownObject>::CreateRationalBSplineCurve( const MGSize& id, const vector< pair<MGString,MGString> >& tab)
{
	MGInt ind = GetId( STP_RATIONAL_B_SPLINE_CURVE, tab);

	MGInt k1 = GetId( STP_REPRESENTATION_ITEM, tab);
	MGInt k2 = GetId( STP_B_SPLINE_CURVE, tab);
	MGInt k3 = GetId( STP_B_SPLINE_CURVE_WITH_KNOTS, tab);

	if ( ind < 0 || k1 < 0 || k2 < 0 || k3 < 0 )
		return NULL;

	MGString sparams = tab[k1].second + "," + tab[k2].second + "," + tab[k3].second + "," + tab[ind].second;

	StepRationalBSplineCurve* ptr = new StepRationalBSplineCurve;

	ptr->Init( id, STP_RATIONAL_B_SPLINE_CURVE, sparams);
	if ( sparams.length() > 0 )
		THROW_INTERNAL( "not every argument has been interpreted" );

	return ptr;
}

StepEntity* Factory<StepUnknownObject>::CreateRationalBSplineCurve_QuasiUniform( const MGSize& id, const vector< pair<MGString,MGString> >& tab)
{
	MGInt ind = GetId( STP_RATIONAL_B_SPLINE_CURVE, tab);

	MGInt k1 = GetId( STP_REPRESENTATION_ITEM, tab);
	MGInt k2 = GetId( STP_B_SPLINE_CURVE, tab);
	MGInt k3 = GetId( STP_QUASI_UNIFORM_CURVE, tab);

	if ( ind < 0 || k1 < 0 || k2 < 0 || k3 < 0 )
		return NULL;


	MGString sparams = tab[k1].second + "," + tab[k2].second + "," + STP_QUASI_UNIFORM_CURVE + "," + tab[ind].second;

	StepRationalBSplineCurve* ptr = new StepRationalBSplineCurve;

	ptr->Init( id, STP_RATIONAL_B_SPLINE_CURVE, sparams);
	if ( sparams.length() > 0 )
		THROW_INTERNAL( "not every argument has been interpreted" );

	return ptr;
}

//RATIONAL_B_SPLINE_SURFACE
StepEntity* Factory<StepUnknownObject>::CreateRationalBSplineSurface( const MGSize& id, const vector< pair<MGString,MGString> >& tab)
{
	MGInt ind = GetId( STP_RATIONAL_B_SPLINE_SURFACE, tab);

	MGInt k1 = GetId( STP_REPRESENTATION_ITEM, tab);
	MGInt k2 = GetId( STP_B_SPLINE_SURFACE, tab);
	MGInt k3 = GetId( STP_B_SPLINE_SURFACE_WITH_KNOTS, tab);

	if ( ind < 0 || k1 < 0 || k2 < 0 || k3 < 0 )
		return NULL;

	MGString sparams = tab[k1].second + "," + tab[k2].second + "," + tab[k3].second + "," + tab[ind].second;

	StepRationalBSplineSurface* ptr = new StepRationalBSplineSurface;

	ptr->Init( id, STP_RATIONAL_B_SPLINE_SURFACE, sparams);
	if ( sparams.length() > 0 )
		THROW_INTERNAL( "not every argument has been interpreted" );

	return ptr;
}

StepEntity* Factory<StepUnknownObject>::CreateRationalBSplineSurface_QuasiUniform( const MGSize& id, const vector< pair<MGString,MGString> >& tab)
{
	MGInt ind = GetId( STP_RATIONAL_B_SPLINE_SURFACE, tab);

	MGInt k1 = GetId( STP_REPRESENTATION_ITEM, tab);
	MGInt k2 = GetId( STP_B_SPLINE_SURFACE, tab);
	MGInt k3 = GetId( STP_QUASI_UNIFORM_SURFACE, tab);

	if ( ind < 0 || k1 < 0 || k2 < 0 || k3 < 0 )
		return NULL;

	MGString sparams = tab[k1].second + "," + tab[k2].second + "," + STP_QUASI_UNIFORM_SURFACE + "," + tab[ind].second;

	StepRationalBSplineSurface* ptr = new StepRationalBSplineSurface;

	ptr->Init( id, STP_RATIONAL_B_SPLINE_SURFACE, sparams);
	if ( sparams.length() > 0 )
		THROW_INTERNAL( "not every argument has been interpreted" );

	return ptr;
}



StepEntity* Factory<StepUnknownObject>::Create( const MGSize& id, const MGString& sname, const MGString& sparams)
{
	try
	{
		if ( sname != "" || sparams.length() == 0 )
			return NULL;


		vector< pair<MGString,MGString> >	tabdef;

		RegExp		rex;
		MGString	buf, bufname, bufparam;

		rex.InitPattern( "^([_A-Z0-9]+)\\((.+)$");
		rex.SetString( sparams);
		while ( rex.IsOk() )
		{
			bufname = rex.GetSubString(1);
			buf = rex.GetSubString(2);

			bufparam.clear();
			MGSize count = 1;
			MGString::iterator itr;
			for ( itr=buf.begin(); itr!=buf.end(); ++itr)
			{
				if ( *itr == ')' )
					--count;
				else if ( *itr == '(' )
					++count;

				if ( count == 0)
					break;

				bufparam.push_back( *itr );
			}
			
			buf.erase( buf.begin(), ++itr);


			tabdef.push_back( pair<MGString,MGString>( bufname, bufparam) );

			if ( buf.size() == 0 )
				break;

			rex.InitPattern( "^([_A-Z0-9]+)\\((.*)$");
			rex.SetString( buf);
		}

		StepEntity* ptr;

		if ( ptr = CreateRationalBSplineCurve( id, tabdef) )
			return ptr;

		if ( ptr = CreateRationalBSplineCurve_QuasiUniform( id, tabdef) )
			return ptr;

		if ( ptr = CreateRationalBSplineSurface( id, tabdef) )
			return ptr;

		if ( ptr = CreateRationalBSplineSurface_QuasiUniform( id, tabdef) )
			return ptr;

		//cout << endl;
		//for ( MGSize k=0; k<tabdef.size(); ++k)
		//	cout << "- " << tabdef[k].first << "\n (" << tabdef[k].second << ")\n";

	}
	catch ( EHandler::Except&)
	{
		cout << endl;
		cout << StepUnknownObject::ClassName() << " : id = " << id << "  params = \"" << sparams << "\"" << endl;

		throw;
	}

	return NULL;
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace StepSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

