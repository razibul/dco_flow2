#include "stepgeometry.h"
#include "libcorecommon/regexp.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace StepSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//





//////////////////////////////////////////////////////////////////////
// class StepGeometricRepresentationItem
//////////////////////////////////////////////////////////////////////
void StepGeometricRepresentationItem::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	StepRepresentationItem::Init( id, sname, sparams);
}


//////////////////////////////////////////////////////////////////////
// class StepPoint
//////////////////////////////////////////////////////////////////////
void StepPoint::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	StepGeometricRepresentationItem::Init( id, sname, sparams);
}


//////////////////////////////////////////////////////////////////////
// class StepCurve
//////////////////////////////////////////////////////////////////////
void StepCurve::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	StepGeometricRepresentationItem::Init( id, sname, sparams);
}


//////////////////////////////////////////////////////////////////////
// class StepBoundedCurve
//////////////////////////////////////////////////////////////////////
void StepBoundedCurve::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	StepCurve::Init( id, sname, sparams);
}


//////////////////////////////////////////////////////////////////////
// class StepSurface
//////////////////////////////////////////////////////////////////////
void StepSurface::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	StepGeometricRepresentationItem::Init( id, sname, sparams);
}


//////////////////////////////////////////////////////////////////////
// class StepBoundedSurface
//////////////////////////////////////////////////////////////////////
void StepBoundedSurface::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	StepSurface::Init( id, sname, sparams);
}


//////////////////////////////////////////////////////////////////////
// class StepDirection
//////////////////////////////////////////////////////////////////////
void StepDirection::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	StepGeometricRepresentationItem::Init( id, sname, sparams);

	RegExp	rex;
	rex.InitPattern( "^\\((.*)\\)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		vector<MGFloat> tab;
		ReadTabFloat( tab, rex.GetSubString( 1) );
		//if ( tab.size() != 3)
		//	THROW_INTERNAL( "three coordinates expected" );
		if ( tab.size() != 3)
		{
			tab.resize( 3, 0.0);
			cout << id << " " << sname << " " << rex.GetSubString( 1) << " - three coordinates expected" << endl;
		}

		mVect = Vect3D( tab[0], tab[1], tab[2]);

		sparams = rex.GetSubString( 2);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );

}


//////////////////////////////////////////////////////////////////////
// class StepVector
//////////////////////////////////////////////////////////////////////
void StepVector::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	StepGeometricRepresentationItem::Init( id, sname, sparams);

	RegExp	rex;
	rex.InitPattern( "^#([0-9]*),([0-9\\+\\.eE-]*)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		istringstream is;

		is.clear();
		is.str( rex.GetSubString( 1) );
		is >> mDirId;

		is.clear();
		is.str( rex.GetSubString( 2) );
		is >> mLength;

		sparams = rex.GetSubString( 3);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );

}


//////////////////////////////////////////////////////////////////////
// class StepAxis1Placement
//////////////////////////////////////////////////////////////////////
void StepAxis1Placement::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	StepGeometricRepresentationItem::Init( id, sname, sparams);

	RegExp	rex;
	rex.InitPattern( "^#([0-9]*),#([0-9]*)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		istringstream is;

		is.clear();
		is.str( rex.GetSubString( 1) );
		is >> mIdPoint;

		is.clear();
		is.str( rex.GetSubString( 2) );
		is >> mIdAxis;

		sparams = rex.GetSubString( 4);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );

}


//////////////////////////////////////////////////////////////////////
// class StepAxis2Placement3D
//////////////////////////////////////////////////////////////////////
void StepAxis2Placement3D::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	StepGeometricRepresentationItem::Init( id, sname, sparams);

//#51=AXIS2_PLACEMENT_3D(' ',#50,$,$) ;

	RegExp	rex;
	//rex.InitPattern( "^#([0-9]*),#([0-9]*),#([0-9]*)(.*)$" );
	rex.InitPattern( "^#([0-9]*),([$#0-9]*),([$#0-9]*)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		istringstream is;

		//cout << rex.GetNoSubString() << endl;
		//for ( int i=1; i<=rex.GetNoSubString(); ++i)
		//cout << i << " " << rex.GetSubString(i) << endl;


		is.clear();
		is.str( rex.GetSubString( 1) );
		is >> mIdPoint;

		if ( rex.GetSubString( 2) == "$" )
			mIdAxis = 0;
		else
		{
			is.clear();
			MGString buf = rex.GetSubString( 2);
			buf.erase( buf.begin() );
			is.str( buf );
			is >> mIdAxis;
		}

		if ( rex.GetSubString( 3) == "$" )
			mIdRefDir = 0;
		else
		{
			is.clear();
			MGString buf = rex.GetSubString( 3);
			buf.erase( buf.begin() );
			is.str( buf );
			is >> mIdRefDir;
		}

		//cout << mIdPoint << " " << mIdOX  << " " << mIdOY << endl;

		sparams = rex.GetSubString( 4);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );

		//THROW_INTERNAL( "END" );
}


//////////////////////////////////////////////////////////////////////
// class StepCartesianPoint
//////////////////////////////////////////////////////////////////////
void StepCartesianPoint::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	StepPoint::Init( id, sname, sparams);

	RegExp	rex;
	rex.InitPattern( "^\\((.*)\\)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		vector<MGFloat> tab;
		ReadTabFloat( tab, rex.GetSubString( 1) );
		//if ( tab.size() != 3)
		//	THROW_INTERNAL( "three coordinates expected" );
		if ( tab.size() != 3)
		{
			tab.resize( 3, 0.0);
			//cout << sname << " - three coordinates expected" << endl;
			cout << id << " " << sname << " " << rex.GetSubString( 1) << " - three coordinates expected" << endl;
		}


		mVect = Vect3D( tab[0], tab[1], tab[2]);

		sparams = rex.GetSubString( 2);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );

}

//////////////////////////////////////////////////////////////////////
// class StepLine
//////////////////////////////////////////////////////////////////////
void StepLine::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	StepCurve::Init( id, sname, sparams);

	RegExp	rex;
	rex.InitPattern( "^#([0-9]*),#([0-9]*)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		istringstream is;

		is.clear();
		is.str( rex.GetSubString( 1) );
		is >> mIdPoint;

		is.clear();
		is.str( rex.GetSubString( 2) );
		is >> mIdVect;

		sparams = rex.GetSubString( 3);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );
}



//////////////////////////////////////////////////////////////////////
// class StepCircle
//////////////////////////////////////////////////////////////////////
void StepCircle::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	StepCurve::Init( id, sname, sparams);

	RegExp	rex;
	rex.InitPattern( "^#([0-9]*),([0-9\\+\\.eE-]*)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		istringstream is;

		is.clear();
		is.str( rex.GetSubString( 1) );
		is >> mIdAxis;

		is.clear();
		is.str( rex.GetSubString( 2) );
		is >> mRadius;

		sparams = rex.GetSubString( 3);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );
}


//////////////////////////////////////////////////////////////////////
// class StepEllipse
//////////////////////////////////////////////////////////////////////
void StepEllipse::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	StepCurve::Init( id, sname, sparams);

	RegExp	rex;
	rex.InitPattern( "^#([0-9]*),([0-9\\+\\.eE-]*),([0-9\\+\\.eE-]*)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		istringstream is;

		is.clear();
		is.str( rex.GetSubString( 1) );
		is >> mIdAxis;

		is.clear();
		is.str( rex.GetSubString( 2) );
		is >> mSemiAxis1;

		is.clear();
		is.str( rex.GetSubString( 3) );
		is >> mSemiAxis2;

		sparams = rex.GetSubString( 4);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );
}



//////////////////////////////////////////////////////////////////////
// class StepBSplineCurve
//////////////////////////////////////////////////////////////////////
void StepBSplineCurve::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	StepBoundedCurve::Init( id, sname, sparams);

	RegExp	rex;
	rex.InitPattern( "^([0-9]*),\\((.*)\\),([\\.A-Z_]*),([\\.A-Z_]*),([\\.A-Z_]*)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		istringstream is;

		is.clear();
		is.str( rex.GetSubString( 1) );
		is >> mDeg;

		ReadTabId( mtabIdPoint, rex.GetSubString( 2) );
		if ( mtabIdPoint.size() == 0)
			THROW_INTERNAL( "empty list of points found" );

		mFlag1 = ReadFlagBSplineCurveForm( rex.GetSubString( 3) );
		mFlag2 = ReadFlag( rex.GetSubString( 4) );
		mFlag3 = ReadFlag( rex.GetSubString( 5) );


		sparams = rex.GetSubString( 6);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );

}


//////////////////////////////////////////////////////////////////////
// class StepBSplineCurveWithKnots
//////////////////////////////////////////////////////////////////////
void StepBSplineCurveWithKnots::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	StepBSplineCurve::Init( id, sname, sparams);

	RegExp	rex;

	rex.InitPattern( "^\\((.*)\\),\\((.*)\\),([\\.A-Z_]*)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		istringstream is;

		ReadTabUInt( mtabMult, rex.GetSubString( 1) );
		if ( mtabMult.size() == 0)
			THROW_INTERNAL( "empty list of knot multiplicity found" );

		ReadTabFloat( mtabKnot, rex.GetSubString( 2) );
		if ( mtabKnot.size() == 0)
			THROW_INTERNAL( "empty list of knots found" );

		if ( mtabKnot.size() != mtabMult.size() )
			THROW_INTERNAL( "different size of knot and mult arrays" );

		mFlag = ReadFlagKnotType( rex.GetSubString( 3) );

		sparams = rex.GetSubString( 4);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );

		return;
	}
	else
	{

		rex.InitPattern( "^([\\.A-Z_]*)(.*)$" );
		rex.SetString( sparams);

		if ( rex.IsOk() )
		{
			MGString str = rex.GetSubString( 1);

			if ( str == STP_QUASI_UNIFORM_CURVE )
			{
				MGSize n = cTabPntId().size() - cDeg() + 1;
				MGFloat du = 1.0 / (MGFloat)(n-1);

				mtabKnot.resize(n);
				mtabMult.resize(n);

				for ( MGSize i=0; i<n; ++i)
				{
					mtabKnot[i] = i * du;
					mtabMult[i] = 1;
				}

				mtabMult[0] = mtabMult[n-1] = cDeg() + 1;

				mFlag = FLAG_KNTP_UNSPECIFIED;
			}

			sparams = rex.GetSubString( 2);
			if ( ! sparams.empty() )
				if ( sparams[0] == ',' )
					sparams.erase( sparams.begin() );

			return;

		}
	}

	THROW_INTERNAL( "unrecognized definition" );

}


//////////////////////////////////////////////////////////////////////
// class StepQuasiUniformCurve
//////////////////////////////////////////////////////////////////////
void StepQuasiUniformCurve::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	sparams += "," + MGString( STP_QUASI_UNIFORM_CURVE);
	StepBSplineCurveWithKnots::Init( id, sname, sparams);
}



//////////////////////////////////////////////////////////////////////
// class StepRationalBSplineCurve
//////////////////////////////////////////////////////////////////////
void StepRationalBSplineCurve::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	StepBSplineCurveWithKnots::Init( id, sname, sparams);

	RegExp	rex;
	rex.InitPattern( "^\\((.*)\\)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		ReadTabFloat( mtabWght, rex.GetSubString( 1) );
		if ( mtabWght.size() == 0)
			THROW_INTERNAL( "empty list of knots found" );

		//if ( mtabWght.size() != mtabIdPoint.size() )
		//	THROW_INTERNAL( "different size of weight and point arrays" );

		sparams = rex.GetSubString( 2);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );

}







//////////////////////////////////////////////////////////////////////
// class StepPlane
//////////////////////////////////////////////////////////////////////
void StepPlane::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	StepSurface::Init( id, sname, sparams);

	RegExp	rex;
	rex.InitPattern( "^#([0-9]*)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		istringstream is;

		is.clear();
		is.str( rex.GetSubString( 1) );
		is >> mIdAxis;

		sparams = rex.GetSubString( 2);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );

}


//////////////////////////////////////////////////////////////////////
// class StepCylindricalSurface
//////////////////////////////////////////////////////////////////////
void StepCylindricalSurface::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	StepSurface::Init( id, sname, sparams);

	RegExp	rex;
	rex.InitPattern( "^#([0-9]*),([0-9\\+\\.eE-]*)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		istringstream is;

		is.clear();
		is.str( rex.GetSubString( 1) );
		is >> mIdAxis;

		is.clear();
		is.str( rex.GetSubString( 2) );
		is >> mRadius;

		sparams = rex.GetSubString( 3);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );

}


//////////////////////////////////////////////////////////////////////
// class StepConicalSurface
//////////////////////////////////////////////////////////////////////
void StepConicalSurface::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	StepSurface::Init( id, sname, sparams);

	RegExp	rex;
	rex.InitPattern( "^#([0-9]*),([0-9\\+\\.eE-]*),([0-9\\+\\.eE-]*)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		istringstream is;

		is.clear();
		is.str( rex.GetSubString( 1) );
		is >> mIdAxis;

		is.clear();
		is.str( rex.GetSubString( 2) );
		is >> mRadius;

		is.clear();
		is.str( rex.GetSubString( 3) );
		is >> mAngle;

		sparams = rex.GetSubString( 4);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );

}


//////////////////////////////////////////////////////////////////////
// class StepSphericalSurface
//////////////////////////////////////////////////////////////////////
void StepSphericalSurface::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	StepSurface::Init( id, sname, sparams);

	RegExp	rex;
	rex.InitPattern( "^#([0-9]*),([0-9\\+\\.eE-]*)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		istringstream is;

		is.clear();
		is.str( rex.GetSubString( 1) );
		is >> mIdAxis;

		is.clear();
		is.str( rex.GetSubString( 2) );
		is >> mRadius;

		sparams = rex.GetSubString( 3);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );

}

//////////////////////////////////////////////////////////////////////
// class StepSphericalSurface
//////////////////////////////////////////////////////////////////////
void StepToroidalSurface::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	StepSurface::Init( id, sname, sparams);

	RegExp	rex;
	rex.InitPattern( "^#([0-9]*),([0-9\\+\\.eE-]*),([0-9\\+\\.eE-]*)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		istringstream is;

		is.clear();
		is.str( rex.GetSubString( 1) );
		is >> mIdAxis;

		is.clear();
		is.str( rex.GetSubString( 2) );
		is >> mRadius1;

		is.clear();
		is.str( rex.GetSubString( 3) );
		is >> mRadius2;

		sparams = rex.GetSubString( 4);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );

}


//////////////////////////////////////////////////////////////////////
// class StepSurfaceOfRevolution
//////////////////////////////////////////////////////////////////////
void StepSurfaceOfRevolution::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	StepSurface::Init( id, sname, sparams);

	RegExp	rex;
	rex.InitPattern( "^#([0-9]*),#([0-9]*)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		istringstream is;

		is.clear();
		is.str( rex.GetSubString( 1) );
		is >> mIdAxis;

		is.clear();
		is.str( rex.GetSubString( 2) );
		is >> mIdCurve;

		sparams = rex.GetSubString( 3);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );

}

//////////////////////////////////////////////////////////////////////
// class StepSurfaceOfLinearExtrusion
//////////////////////////////////////////////////////////////////////
void StepSurfaceOfLinearExtrusion::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	StepSurface::Init( id, sname, sparams);

	RegExp	rex;
	rex.InitPattern( "^#([0-9]*),#([0-9]*)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		istringstream is;

		is.clear();
		is.str( rex.GetSubString( 1) );
		is >> mIdCurve;

		is.clear();
		is.str( rex.GetSubString( 2) );
		is >> mIdVect;

		sparams = rex.GetSubString( 3);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );

}


//////////////////////////////////////////////////////////////////////
// class StepBSplineSurface
//////////////////////////////////////////////////////////////////////
void StepBSplineSurface::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	StepBoundedSurface::Init( id, sname, sparams);

	RegExp	rex;
	rex.InitPattern( "^([0-9]*),([0-9]*),\\((.*)\\),([\\.A-Z_]*),([\\.A-Z_]*),([\\.A-Z_]*),([\\.A-Z_]*)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		istringstream is;

		is.clear();
		is.str( rex.GetSubString( 1) );
		is >> mDegU;

		is.clear();
		is.str( rex.GetSubString( 2) );
		is >> mDegV;

		ReadMtxId( mmtxIdPoint, rex.GetSubString( 3) );

		if ( mmtxIdPoint.size() == 0 )
			THROW_INTERNAL( "empty list of points found" );

		if ( mmtxIdPoint[0].size() == 0 )
			THROW_INTERNAL( "empty list of points found" );

		mFlag1 = ReadFlagBSplineSurfaceForm( rex.GetSubString( 4) );
		mFlagCU = ReadFlag( rex.GetSubString( 5) );
		mFlagCV = ReadFlag( rex.GetSubString( 6) );
		mFlag3 = ReadFlag( rex.GetSubString( 7) );

		cout << mFlagCU << " " << mFlagCV <<  endl;

		sparams = rex.GetSubString( 8);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );

}



//////////////////////////////////////////////////////////////////////
// class StepBSplineSurfaceWithKnots
//////////////////////////////////////////////////////////////////////
void StepBSplineSurfaceWithKnots::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	StepBSplineSurface::Init( id, sname, sparams);

	RegExp	rex;
	rex.InitPattern( "^\\(([^\\(\\)]*)\\),\\(([^\\(\\)]*)\\),\\(([^\\(\\)]*)\\),\\(([^\\(\\)]*)\\),([\\.A-Z_]*)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		istringstream is;

		ReadTabUInt( mtabMultU, rex.GetSubString( 1) );
		if ( mtabMultU.size() == 0)
			THROW_INTERNAL( "empty list of knot multiplicity found" );

		ReadTabUInt( mtabMultV, rex.GetSubString( 2) );
		if ( mtabMultV.size() == 0)
			THROW_INTERNAL( "empty list of knot multiplicity found" );

		ReadTabFloat( mtabKnotU, rex.GetSubString( 3) );
		if ( mtabKnotU.size() == 0)
			THROW_INTERNAL( "empty list of knots found" );
		
		ReadTabFloat( mtabKnotV, rex.GetSubString( 4) );
		if ( mtabKnotV.size() == 0)
			THROW_INTERNAL( "empty list of knots found" );


		if ( mtabKnotU.size() != mtabMultU.size() )
			THROW_INTERNAL( "different size of knot and mult arrays" );

		if ( mtabKnotV.size() != mtabMultV.size() )
			THROW_INTERNAL( "different size of knot and mult arrays" );

		mFlag = ReadFlagKnotType( rex.GetSubString( 5) );

		sparams = rex.GetSubString( 6);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );

		return;
	}
	else
	{

		rex.InitPattern( "^([\\.A-Z_]*)(.*)$" );
		rex.SetString( sparams);

		if ( rex.IsOk() )
		{
			MGString str = rex.GetSubString( 1);

			if ( str == STP_QUASI_UNIFORM_SURFACE )
			{
				MGSize nu = cMtxPntId().size() - cDegU() + 1;
				MGFloat du = 1.0 / (MGFloat)(nu-1);

				mtabKnotU.resize(nu);
				mtabMultU.resize(nu);

				for ( MGSize i=0; i<nu; ++i)
				{
					mtabKnotU[i] = i * du;
					mtabMultU[i] = 1;
				}

				mtabMultU[0] = mtabMultU[nu-1] = cDegU() + 1;


				MGSize nv = cMtxPntId()[0].size() - cDegV() + 1;
				MGFloat dv = 1.0 / (MGFloat)(nv-1);

				mtabKnotV.resize(nv);
				mtabMultV.resize(nv);

				for ( MGSize i=0; i<nv; ++i)
				{
					mtabKnotV[i] = i * dv;
					mtabMultV[i] = 1;
				}

				mtabMultV[0] = mtabMultV[nv-1] = cDegV() + 1;

				mFlag = FLAG_KNTP_UNSPECIFIED;
			}

			sparams = rex.GetSubString( 2);
			if ( ! sparams.empty() )
				if ( sparams[0] == ',' )
					sparams.erase( sparams.begin() );

			return;

		}
	}

	THROW_INTERNAL( "unrecognized definition" );

}


//////////////////////////////////////////////////////////////////////
// class StepQuasiUnformSurface
//////////////////////////////////////////////////////////////////////
void StepQuasiUniformSurface::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	sparams += "," + MGString( STP_QUASI_UNIFORM_SURFACE);
	StepBSplineSurfaceWithKnots::Init( id, sname, sparams);
}


//////////////////////////////////////////////////////////////////////
// class StepRationalBSplineSurface
//////////////////////////////////////////////////////////////////////
void StepRationalBSplineSurface::Init( const MGSize& id, const MGString& sname, MGString& sparams)
{
	StepBSplineSurfaceWithKnots::Init( id, sname, sparams);

	RegExp	rex;
	rex.InitPattern( "^\\((.*)\\)(.*)$" );
	rex.SetString( sparams);

	if ( rex.IsOk() )
	{
		ReadMtxFloat( mmtxWght, rex.GetSubString( 1) );
		if ( mmtxWght.size() == 0)
			THROW_INTERNAL( "empty list of knots found" );
		if ( mmtxWght[0].size() == 0)
			THROW_INTERNAL( "empty list of knots found" );

		//if ( mtabWght.size() != mtabIdPoint.size() )
		//	THROW_INTERNAL( "different size of weight and point arrays" );

		sparams = rex.GetSubString( 2);
		if ( ! sparams.empty() )
			if ( sparams[0] == ',' )
				sparams.erase( sparams.begin() );
	}
	else
		THROW_INTERNAL( "unrecognized definition" );

}



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace StepSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
