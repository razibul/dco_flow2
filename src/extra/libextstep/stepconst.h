#ifndef __STEPCONST_H__
#define __STEPCONST_H__


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace StepSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


// constant variables used in STEP files

extern const char STPHD_TOPOLOGY_SCHEMA[];
extern const char STPHD_GEOMETRY_SCHEMA[];
extern const char STPHD_GEO_MODEL_SCHEMA[];


extern const char STP_HEADER[];
extern const char STP_FILE_DESCR[];
extern const char STP_FILE_NAME[];
extern const char STP_FILE_SCHEMA[];
extern const char STP_ENDSEC[];
extern const char STP_DATA[];
extern const char STP_ISO[];
extern const char STP_END_ISO[];

extern const char STP_FLAG_TRUE[];
extern const char STP_FLAG_FALSE[];
extern const char STP_FLAG_U[];
extern const char STP_FLAG_UNSPECIFIED[];
extern const char STP_FLAG_UNIFORM_KNOTS[];
extern const char STP_FLAG_QUASI_UNIFORM_KNOTS[];
extern const char STP_FLAG_PIECEWISE_BEZIER_KNOTS[];
extern const char STP_FLAG_POLYLINE_FORM[];
extern const char STP_FLAG_CIRCULAR_ARC[];
extern const char STP_FLAG_ELLIPTIC_ARC[];
extern const char STP_FLAG_PARABOLIC_ARC[];
extern const char STP_FLAG_HYPERBOLIC_ARC[];

extern const char STP_FLAG_SURF_OF_REVOLUTION[];
extern const char STP_FLAG_PLANE_SURF[];
extern const char STP_FLAG_CYLINDRICAL_SURF[];
extern const char STP_FLAG_CONICAL_SURF[];
extern const char STP_FLAG_SPHERICAL_SURF[];
extern const char STP_FLAG_TOROIDAL_SURF[];
extern const char STP_FLAG_SURF_OF_REVOLUTION[];
extern const char STP_FLAG_RULED_SURF[];
extern const char STP_FLAG_GENERALISED_CONE[];
extern const char STP_FLAG_QUADRIC_SURF[];
extern const char STP_FLAG_SURF_OF_LINEAR_EXTRUSION[];


extern const char STP_REPRESENTATION_ITEM[];


extern const char STP_GEOMETRIC_REPRESENTATION_ITEM[];

extern const char STP_DIRECTION[];
extern const char STP_VECTOR[];
extern const char STP_AXIS1_PLACEMENT[];
extern const char STP_AXIS2_PLACEMENT_3D[];

extern const char STP_POINT[];
extern const char STP_CARTESIAN_POINT[];

extern const char STP_CURVE[];
extern const char STP_PCURVE[];
extern const char STP_BOUNDED_CURVE[];
extern const char STP_B_SPLINE_CURVE[];
extern const char STP_B_SPLINE_CURVE_WITH_KNOTS[];
extern const char STP_QUASI_UNIFORM_CURVE[];
extern const char STP_RATIONAL_B_SPLINE_CURVE[];
extern const char STP_LINE[];
extern const char STP_CIRCLE[];
extern const char STP_ELLIPSE[];
extern const char STP_TRIMMED_CURVE[];

extern const char STP_SURFACE[];
extern const char STP_BOUNDED_SURFACE[];
extern const char STP_B_SPLINE_SURFACE[];
extern const char STP_B_SPLINE_SURFACE_WITH_KNOTS[];
extern const char STP_QUASI_UNIFORM_SURFACE[];
extern const char STP_RATIONAL_B_SPLINE_SURFACE[];
extern const char STP_PLANE[];
extern const char STP_CYLINDRICAL_SURFACE[];
extern const char STP_CONICAL_SURFACE[];
extern const char STP_SPHERICAL_SURFACE[];
extern const char STP_SURFACE_OF_REVOLUTION[];
extern const char STP_SURFACE_OF_LINEAR_EXTRUSION[];
extern const char STP_TOROIDAL_SURFACE[];






extern const char STP_TOPOLOGIC_REPRESENTATION_ITEM[];

extern const char STP_VERTEX_POINT[];
extern const char STP_VERTEX_LOOP[];

extern const char STP_EDGE_CURVE[];
extern const char STP_ORIENTED_EDGE[];
extern const char STP_EDGE_LOOP[];

extern const char STP_ADVANCED_FACE[];
extern const char STP_FACE_BOUND[];
extern const char STP_FACE_OUTER_BOUND[];
extern const char STP_OPEN_SHELL[];
extern const char STP_CLOSED_SHELL[];
extern const char STP_ORIENTED_CLOSED_SHELL[];

extern const char STP_BREP_WITH_VOIDS[];
extern const char STP_MANIFOLD_SOLID_BREP[];
extern const char STP_GEOMETRIC_CURVE_SET[];
extern const char STP_SHELL_BASED_SURFACE_MODEL[];

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace StepSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace Step = StepSpace;


#endif // __STEPCONST_H__
