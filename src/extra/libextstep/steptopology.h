#ifndef __STEPTOPOLOGY_H__
#define __STEPTOPOLOGY_H__

#include "libcoresystem/mgdecl.h"
#include "libextstep/stepconst.h"
#include "libextstep/stepentity.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace StepSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


//////////////////////////////////////////////////////////////////////
// class StepTopologicRepresentationItem
//////////////////////////////////////////////////////////////////////
class StepTopologicRepresentationItem : public StepRepresentationItem
{
public:
	StepTopologicRepresentationItem()		{}

	static const char*		ClassName()		{ return STP_TOPOLOGIC_REPRESENTATION_ITEM;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;	

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);
};


inline void StepTopologicRepresentationItem::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepRepresentationItem::GetDotDepend(tab);
}

inline bool StepTopologicRepresentationItem::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepRepresentationItem::IsType( type);
}



//////////////////////////////////////////////////////////////////////
// class StepVertexPoint
//////////////////////////////////////////////////////////////////////
class StepVertexPoint : public StepTopologicRepresentationItem
{
public:
	StepVertexPoint() : mIdPoint(0)			{}

	static const char*		ClassName()		{ return STP_VERTEX_POINT;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;


	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);

	const MGSize&	cIdPoint() const		{ return mIdPoint;}


private:
	MGSize	mIdPoint;
};

inline void StepVertexPoint::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepTopologicRepresentationItem::GetDotDepend(tab);
	tab.push_back( mIdPoint);
}

inline bool StepVertexPoint::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepTopologicRepresentationItem::IsType( type);
}




//////////////////////////////////////////////////////////////////////
// class StepVertexLoop
//////////////////////////////////////////////////////////////////////
class StepVertexLoop : public StepTopologicRepresentationItem
{
public:
	StepVertexLoop() : mIdVertex(0)			{}

	static const char*		ClassName()		{ return STP_VERTEX_LOOP;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);

	const MGSize&	cIdVertex() const		{ return mIdVertex;}


private:
	MGSize	mIdVertex;
};

inline void StepVertexLoop::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepTopologicRepresentationItem::GetDotDepend(tab);
	tab.push_back( mIdVertex);
}

inline bool StepVertexLoop::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepTopologicRepresentationItem::IsType( type);
}


//////////////////////////////////////////////////////////////////////
// class StepEdgeCurve
//////////////////////////////////////////////////////////////////////
class StepEdgeCurve : public StepTopologicRepresentationItem
{
public:
	StepEdgeCurve() : mIdCurve(0), mIdVertex1(0), mIdVertex2(0), mflOrient(FLAG_TRUE)			{}

	static const char*		ClassName()		{ return STP_EDGE_CURVE;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);

	const MGSize&	cIdCurve() const		{ return mIdCurve;}
	const MGSize&	cIdVertex1() const		{ return mIdVertex1;}
	const MGSize&	cIdVertex2() const		{ return mIdVertex2;}
	const StepFlag	cflOrient() const		{ return mflOrient;}


private:
	MGSize		mIdCurve;
	MGSize		mIdVertex1;
	MGSize		mIdVertex2;
	StepFlag	mflOrient;
};


inline void StepEdgeCurve::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepTopologicRepresentationItem::GetDotDepend(tab);
	tab.push_back( mIdCurve);
	tab.push_back( mIdVertex1);
	tab.push_back( mIdVertex2);
}

inline bool StepEdgeCurve::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepTopologicRepresentationItem::IsType( type);
}



//////////////////////////////////////////////////////////////////////
// class StepOrientedEdge
//////////////////////////////////////////////////////////////////////
class StepOrientedEdge : public StepTopologicRepresentationItem
{
public:
	StepOrientedEdge() : mIdEdge(0), mflOrient(FLAG_TRUE)			{}

	static const char*		ClassName()		{ return STP_ORIENTED_EDGE;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);

	const MGSize&	cIdEdge() const			{ return mIdEdge;}
	const StepFlag	cflOrient() const		{ return mflOrient;}


private:
	MGSize		mIdEdge;
	StepFlag	mflOrient;
};


inline void StepOrientedEdge::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepTopologicRepresentationItem::GetDotDepend(tab);
	tab.push_back( mIdEdge);
}

inline bool StepOrientedEdge::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepTopologicRepresentationItem::IsType( type);
}




//////////////////////////////////////////////////////////////////////
// class StepEdgeLoop
//////////////////////////////////////////////////////////////////////
class StepEdgeLoop : public StepTopologicRepresentationItem
{
public:
	StepEdgeLoop()							{}

	static const char*		ClassName()		{ return STP_EDGE_LOOP;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);

	const vector<MGSize>&	ctabIdEdge() const		{ return mtabIdEdge;}


private:
	vector<MGSize>	mtabIdEdge;
};


inline void StepEdgeLoop::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepTopologicRepresentationItem::GetDotDepend(tab);
	for ( vector<MGSize>::const_iterator itr=mtabIdEdge.begin(); itr!=mtabIdEdge.end(); ++itr)
		tab.push_back( *itr);
}

inline bool StepEdgeLoop::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepTopologicRepresentationItem::IsType( type);
}



//////////////////////////////////////////////////////////////////////
// class StepFaceBound
//////////////////////////////////////////////////////////////////////
class StepFaceBound : public StepTopologicRepresentationItem
{
public:
	StepFaceBound() : mIdLoop(0), mflOrient(FLAG_TRUE)	{}

	static const char*		ClassName()		{ return STP_FACE_BOUND;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);

	const MGSize&	cIdLoop() const			{ return mIdLoop;}
	const StepFlag	cflOrient() const		{ return mflOrient;}

private:
	MGSize		mIdLoop;
	StepFlag	mflOrient;
};


inline void StepFaceBound::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepTopologicRepresentationItem::GetDotDepend(tab);
	tab.push_back( mIdLoop);
}

inline bool StepFaceBound::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepTopologicRepresentationItem::IsType( type);
}




//////////////////////////////////////////////////////////////////////
// class StepFaceOuterBound
//////////////////////////////////////////////////////////////////////
class StepFaceOuterBound : public StepFaceBound
{
public:
	StepFaceOuterBound()					{}

	static const char*		ClassName()		{ return STP_FACE_OUTER_BOUND;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);

private:
};


inline void StepFaceOuterBound::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepFaceBound::GetDotDepend(tab);
}

inline bool StepFaceOuterBound::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepFaceBound::IsType( type);
}




//////////////////////////////////////////////////////////////////////
// class StepAdvancedFace
//////////////////////////////////////////////////////////////////////
class StepAdvancedFace : public StepTopologicRepresentationItem
{
public:
	StepAdvancedFace() : mIdSurface(0), mflOrient(FLAG_TRUE)	{}

	static const char*		ClassName()		{ return STP_ADVANCED_FACE;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);

	const MGSize&			cIdSurface() const		{ return mIdSurface;}
	const StepFlag&			cflOrient() const		{ return mflOrient;}
	const vector<MGSize>&	ctabIdBound() const		{ return mtabIdBound;}

private:
	MGSize			mIdSurface;
	vector<MGSize>	mtabIdBound;
	StepFlag		mflOrient;
};

inline void StepAdvancedFace::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepTopologicRepresentationItem::GetDotDepend(tab);
			tab.push_back( mIdSurface);
			for ( vector<MGSize>::const_iterator itr=mtabIdBound.begin(); itr!=mtabIdBound.end(); ++itr)
				tab.push_back( *itr);

}

inline bool StepAdvancedFace::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepTopologicRepresentationItem::IsType( type);
}



//////////////////////////////////////////////////////////////////////
// class StepOpenShell
//////////////////////////////////////////////////////////////////////
class StepOpenShell : public StepTopologicRepresentationItem
{
public:
	StepOpenShell()							{}

	static const char*		ClassName()		{ return STP_OPEN_SHELL;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);

	const vector<MGSize>&	ctabIdFace() const		{ return mtabIdFace;}

private:
	vector<MGSize>	mtabIdFace;
};


inline void StepOpenShell::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepTopologicRepresentationItem::GetDotDepend(tab);
	for ( vector<MGSize>::const_iterator itr=mtabIdFace.begin(); itr!=mtabIdFace.end(); ++itr)
		tab.push_back( *itr);

}

inline bool StepOpenShell::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepTopologicRepresentationItem::IsType( type);
}


//////////////////////////////////////////////////////////////////////
// class StepClosedShell
//////////////////////////////////////////////////////////////////////
class StepClosedShell : public StepOpenShell
{
public:
	StepClosedShell()						{}

	static const char*		ClassName()		{ return STP_CLOSED_SHELL;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);
};


inline void StepClosedShell::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepOpenShell::GetDotDepend(tab);
}

inline bool StepClosedShell::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepOpenShell::IsType( type);
}




//////////////////////////////////////////////////////////////////////
// class StepOrientedClosedShell
//////////////////////////////////////////////////////////////////////
class StepOrientedClosedShell : public StepTopologicRepresentationItem
{
public:
	StepOrientedClosedShell() : mIdCShell(0), mflOrient(FLAG_TRUE)	{}

	static const char*		ClassName()		{ return STP_ORIENTED_CLOSED_SHELL;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);

	const MGSize&	cIdCShell() const			{ return mIdCShell;}
	const StepFlag	cflOrient() const		{ return mflOrient;}


private:
	MGSize		mIdCShell;
	StepFlag	mflOrient;
};


inline void StepOrientedClosedShell::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepTopologicRepresentationItem::GetDotDepend(tab);
	tab.push_back( mIdCShell);
}

inline bool StepOrientedClosedShell::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepTopologicRepresentationItem::IsType( type);
}



//////////////////////////////////////////////////////////////////////
// class StepManifoldSolidBRep
//////////////////////////////////////////////////////////////////////
class StepManifoldSolidBRep : public StepTopologicRepresentationItem
{
public:
	StepManifoldSolidBRep() : mIdCShell(0)	{}

	static const char*		ClassName()		{ return STP_MANIFOLD_SOLID_BREP;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);

	const MGSize&	cIdCShell() const		{ return mIdCShell;}

private:
	MGSize			mIdCShell;		// ClosedShell id
};

inline void StepManifoldSolidBRep::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepTopologicRepresentationItem::GetDotDepend(tab);
	tab.push_back( mIdCShell);
}

inline bool StepManifoldSolidBRep::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepTopologicRepresentationItem::IsType( type);
}




//////////////////////////////////////////////////////////////////////
// class StepBRepWithVoids
//////////////////////////////////////////////////////////////////////
class StepBRepWithVoids : public StepManifoldSolidBRep
{
public:
	StepBRepWithVoids()	{}

	static const char*		ClassName()		{ return STP_BREP_WITH_VOIDS;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);

	const vector<MGSize>&	ctabIdShell() const		{ return mtabIdShell;}


private:
	vector<MGSize>	mtabIdShell;	// OrientedClosedShell
};


inline void StepBRepWithVoids::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepManifoldSolidBRep::GetDotDepend(tab);
	for ( vector<MGSize>::const_iterator itr=mtabIdShell.begin(); itr!=mtabIdShell.end(); ++itr)
		tab.push_back( *itr);
}

inline bool StepBRepWithVoids::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepManifoldSolidBRep::IsType( type);
}


//////////////////////////////////////////////////////////////////////
// class StepShellBasedSurfaceModel
//////////////////////////////////////////////////////////////////////
class StepShellBasedSurfaceModel : public StepTopologicRepresentationItem
{
public:
	StepShellBasedSurfaceModel()	{}

	static const char*		ClassName()		{ return STP_SHELL_BASED_SURFACE_MODEL;}

	virtual MGString	GetStepName() const							{ return ClassName();};
	virtual void		GetDotDepend( vector<MGSize>& tab) const;
	virtual bool		IsType( const MGString& type) const;

	void	Init( const MGSize& id, const MGString& sname, MGString& sparams);

	const vector<MGSize>&	ctabIdShell() const		{ return mtabIdShell;}


private:
	vector<MGSize>	mtabIdShell;	// Shell
};


inline void StepShellBasedSurfaceModel::GetDotDepend( vector<MGSize>& tab) const	
{ 
	StepTopologicRepresentationItem::GetDotDepend(tab);
	for ( vector<MGSize>::const_iterator itr=mtabIdShell.begin(); itr!=mtabIdShell.end(); ++itr)
		tab.push_back( *itr);
}

inline bool StepShellBasedSurfaceModel::IsType( const MGString& type) const		
{ 
	if ( type == ClassName() )
		return true;
	else
		return StepTopologicRepresentationItem::IsType( type);
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace StepSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace Step = StepSpace;


#endif // __STEPTOPOLOGY_H__
