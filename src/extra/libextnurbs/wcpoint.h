#ifndef __WCPOINT_H__
#define __WCPOINT_H__

#include "libcoresystem/mgdecl.h"
#include "libextnurbs/nurbsconst.h"
#include "libcoregeom/vect.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace NURBSSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

using namespace Geom;


//////////////////////////////////////////////////////////////////////
// class WCPoint
//////////////////////////////////////////////////////////////////////
template < Dimension DIM, class T >
class WCPoint : public Vect< static_cast<Dimension>(DIM+1), T >
{
public:
	typedef Vect<static_cast<Dimension>(DIM+1),T> WVect;
	typedef Vect<DIM,T>	GVect;

	WCPoint() : WVect()							{}
	WCPoint( const WVect& vct) : WVect( vct)	{}
	WCPoint( const GVect& vct)	
		{
			for ( MGSize i=0; i<DIM; ++i)
				WVect::rX(i) = vct.cX(i);
			rWeight() = 1.0;
		}

	//WCPoint( const GVect& vct) : Vect4D( vct.X(), vct.Y(), vct.Z(), 0.0)		{}

	const T&	cWeight() const	{ return WVect::cX(DIM); }
	T&			rWeight()		{ return WVect::rX(DIM); }


	GVect		ProjectW() const
		{
			GVect vct;
			for ( MGSize i=0; i<DIM; ++i)
				vct.rX(i) = WVect::cX(i);
			return vct;
		}


	GVect		Pos() const
		{
			GVect vct;
			for ( MGSize i=0; i<DIM; ++i)
				vct.rX(i) = WVect::cX(i) / cWeight();
			return vct;
		}

};

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace NURBS = NURBSSpace;


#endif // __WCPOINT_H__
