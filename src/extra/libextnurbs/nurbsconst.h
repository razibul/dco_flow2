#ifndef __NURBSCONST_H__
#define __NURBSCONST_H__

#include "libcoresystem/mgdecl.h"
#include "libextnurbs/matrix.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace NURBSSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


extern const	MGFloat	NURBS_ZERO;


//template <class T>
//T min( const T& t1, const T& t2)
//{
//	return (t1 < t2 ? t1: t2);
//}



inline MGInt Binomial( const MGInt& n, const MGInt& k)
{
	if ( k > n )
		return 0;
	if ( k == 0 )
		return 1;
	
	return Binomial(n-1, k) + Binomial(n-1, k-1);
}

template <class T>
inline void BinomialMtx( Matrix<T>& mtxBin)
{
	int n,k ;
	
	// Setup the first line
	mtxBin(0,0) = 1.0 ;
	for(k=mtxBin.NCols()-1;k>0;--k)
		mtxBin(0,k) = 0.0 ;

	// Setup the other lines
	for(n=0;n<mtxBin.NRows()-1;n++)
	{
		mtxBin(n+1,0) = 1.0 ;
		for(k=1;k<mtxBin.NCols();k++)
			if(n+1<k)
				mtxBin(n,k) = 0.0 ;
			else
				mtxBin(n+1,k) = mtxBin(n,k) + mtxBin(n,k-1) ;
	}
}


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace NURBS = NURBSSpace;


#endif // __NURBSCONST_H__
