#ifndef __MTXBAND_H__
#define __MTXBAND_H__

#include "libcoresystem/mgdecl.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace NURBSSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


#define SWAP(a,b) {dum=(a);(a)=(b);(b)=dum;} 
#define TINY 1.0e-20 


//////////////////////////////////////////////////////////////////////
// class MtxBand
//////////////////////////////////////////////////////////////////////
class MtxBand
{
public:
	MtxBand();
	MtxBand( const MGInt& n, const MGInt& mlo, const MGInt& mup);
	~MtxBand();

	void	Init( const MGFloat& d);

    MGFloat&			operator()(MGInt i, MGInt j)		{ return mAmtx[i][j]; }
    const MGFloat&		operator()(MGInt i, MGInt j) const	{ return mAmtx[i][j]; }


	// LU decomposition; allocates temporaries used later in Solve
	void	Decompose();			

	// Solve system of A*X=B; on output X are stored in mtxB
	void	Solve( vector<MGFloat>& mtxB );

	// removes temporaries used for calc. solution in Solve
	void	Clean()		{ DestroyTmp();}	

	const MGInt&	N() const		{ return mN;}	
	const MGInt&	Mlow() const	{ return mMlow;}	
	const MGInt&	Mup() const		{ return mMup;}	

protected:
	MGInt	mMlow;
	MGInt	mMup;
	MGInt	mN;
	MGFloat	**mAmtx;

	MGFloat	**mAlmtx;
	MGInt	*mItab;
	MGFloat	mD;


	void	AllocTmp();
	void	DestroyTmp();
};
//////////////////////////////////////////////////////////////////////


inline MtxBand::MtxBand()
{
	mAmtx = mAlmtx = NULL;
	mItab = NULL;
}

inline MtxBand::MtxBand( const MGInt& n, const MGInt& mlo, const MGInt& mup)
{
	MGInt	i;
	mN = n;
	mMlow = mlo;
	mMup = mup;

	mAlmtx = NULL;
	mItab = NULL;

	mAmtx = new MGFloat* [mN];
	for ( i=0; i<mN; mAmtx[i++] = new MGFloat[mMup+mMlow+1] );
}

inline MtxBand::~MtxBand()
{
	MGInt	i;

	if ( mAmtx)
		for ( i=0; i<mN; i++)
			if ( mAmtx[i] ) delete mAmtx[i];

	DestroyTmp();
}


inline void MtxBand::AllocTmp()
{
	MGInt	i;
	
	ASSERT( !mAlmtx && !mItab );
	mAlmtx = new MGFloat* [mN];
	for ( i=0; i<mN; mAlmtx[i++] = new MGFloat[mMup+mMlow+1] );

	mItab = new MGInt[mN];
}


inline void MtxBand::DestroyTmp()
{
	MGInt	i;

	if ( mAlmtx)
		for ( i=0; i<mN; i++)
			if ( mAlmtx[i] ) delete mAlmtx[i];

	if ( mItab) delete mItab;

	mAlmtx = NULL;
	mItab = NULL;
}


inline void  MtxBand::Init( const MGFloat& d)
{
	if ( mAmtx)
	{
		MGInt	i, j;
		for ( i=0; i<mN; i++)
			for ( j=0; j<mMup+mMlow+1; j++)
				mAmtx[i][j] = d;

	}
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace NURBS = NURBSSpace;



#endif // __MTXBAND_H__
