#ifndef __KNOTVECTOR_H__
#define __KNOTVECTOR_H__

#include "libcoresystem/mgdecl.h"
#include "libextnurbs/nurbsconst.h"
#include "libcoregeom/vect.h"
#include "libextnurbs/matrix.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace NURBSSpace {
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//


//////////////////////////////////////////////////////////////////////
// class KnotVector
//////////////////////////////////////////////////////////////////////
template <class T>
class KnotVector : public vector<T>
{
public:
	KnotVector()		{}
	KnotVector( const vector<T>& tab)	{ Init( tab);};

	void Init( const vector<T>& tab);

	void Redistribute( const T& length);

	const T&	UMin() const	{ return *(this->begin());}
	const T&	UMax() const	{ return (*this)[this->size()-1];}


	MGSize	FindSpan( const T& u, const MGSize& deg) const;

	void	CalcBasis( const MGSize& iSpan,  const T& u, const MGSize& deg, vector<T>& tabNf ) const;

	template <class MATRIX_T>
	void	CalcDerBasis( const MGSize& iSpan,  const T& u, const MGSize& deg, const MGSize& nder, MATRIX_T& mtxD ) const;

};
//////////////////////////////////////////////////////////////////////


template <class T>
inline void KnotVector<T>::Init( const vector<T>& tab)
{
	this->resize( tab.size() );
	copy(tab.begin(), tab.end(), this->begin() );
	//for ( MGSize i=0; i<tab.size(); i++)
	//	(*this)[i] = tab[i];
}


template <class T>
inline void KnotVector<T>::Redistribute( const T& length)
{
	T	tfirst = vector<T>::front();
	T	tlast = vector<T>::back();
	T	tdist = tlast - tfirst;

	for ( typename vector<T>::iterator itr = vector<T>::begin(); itr != vector<T>::end(); ++itr)
	{
		*itr = ((*itr) - tfirst) * length / tdist;
	}

}


template <class T>
inline MGSize KnotVector<T>::FindSpan( const T& u, const MGSize& deg) const
{
	MGInt	nocp = this->size() - deg - 1;;
	MGInt	ilow, ihigh, imid;

	ASSERT ( nocp > 0 );

	//if ( fabs(u - (*this)[nocp]) < NURBS_ZERO )
	if ( u >= (*this)[nocp] )
		return nocp - 1;

	if( u <= (*this)[deg])
		return deg;

	ilow = deg - 1;
	ihigh = nocp;
	imid = (ilow + ihigh) /2;

	//while ( u - (*this)[imid] < - NURBS_ZERO  || u - (*this)[imid+1] > - NURBS_ZERO )
	while ( u < (*this)[imid]  || u >= (*this)[imid+1] )
	{
		//if ( u - (*this)[imid] < - NURBS_ZERO )
		if ( u < (*this)[imid] )
			ihigh = imid;
		else
			ilow = imid;

		imid = (ilow + ihigh) /2;
	}
	return imid;
}


template <class T>
inline void KnotVector<T>::CalcBasis( const MGSize& iSpan, const T& u, const MGSize& deg, vector<T>& tabNf ) const
{
	vector<T>	tabLf, tabRt;
	T	dsav, dtmp;

	tabNf.resize( deg+1);
	tabLf.resize( deg+1);
	tabRt.resize( deg+1);

	tabNf[0] = 1.0;

	MGInt nocp = this->size() - deg - 1;
	ASSERT ( nocp > 0 );


	for ( MGSize j=1; j<=deg; j++)
	{
		tabLf[j] = u - (*this)[iSpan+1-j];
		tabRt[j] = (*this)[iSpan+j] - u;
		dsav = 0.0;

		for ( MGSize i=0; i<j; i++)
		{
			dtmp = tabNf[i] / ( tabRt[i+1] + tabLf[j-i]);
			tabNf[i] = dsav + tabRt[i+1]*dtmp;
			dsav = tabLf[j-i]*dtmp;
		}
		tabNf[j] = dsav;
	}
}


template <class T>
template <class MATRIX_T>
inline void KnotVector<T>::CalcDerBasis( const MGSize& iSpan,  const T& u, 
			const MGSize& deg, const MGSize& nder, MATRIX_T& mtxD ) const
{
	MGInt	ipk, irk, j1, j2, is1, is2;
	MGInt	nocp;
	T	dsav, dtmp, d;

	MATRIX_T	mtxNdu;
	MATRIX_T	mtxA;
	vector<T>	tabLf, tabRt;

	mtxNdu.Resize( deg+1, deg+1);
	mtxA.Resize( 2, deg+1);

	tabLf.resize( deg+1);
	tabRt.resize( deg+1);

	mtxD.Resize( nder+1, deg+1);

	nocp = this->size() - deg - 1;

	mtxNdu(0,0) = 1.0;

	for ( MGSize j=1; j<=deg; j++)
	{
		tabLf[j] = u - (*this)[iSpan+1-j];
		tabRt[j] = (*this)[iSpan+j] - u;
		dsav = 0.0;
		for ( MGSize r=0; r<j; r++)
		{
			mtxNdu(j,r) = tabRt[r+1] + tabLf[j-r];
			dtmp = mtxNdu(r,j-1) / mtxNdu(j,r);
			
			mtxNdu(r,j) = dsav + tabRt[r+1]*dtmp;
			dsav = tabLf[j-r]*dtmp;
		}
		mtxNdu(j,j) = dsav;
	}

	for ( MGSize j=0; j<=deg; j++)
		mtxD(0,j) = mtxNdu(j,deg);

	for ( MGInt r=0; r<=static_cast<MGInt>(deg); r++)
	{
		is1 = 0;
		is2 = 1;
		mtxA(0,0) = 1.0;
		for ( MGInt k=1; k<=static_cast<MGInt>(nder); k++)
		{
			d = 0.0;
			irk = r - k;
			ipk = deg - k;
			if ( r >= k )
			{
				mtxA(is2,0) = mtxA(is1,0) / mtxNdu(ipk+1,irk);
				d = mtxA(is2,0) * mtxNdu(irk,ipk);
			}
			if ( irk >= -1 )
				j1 = 1;
			else 
				j1 = -irk;

			if ( r-1 <= ipk )
				j2 = k - 1;
			else
				j2 = deg - r;

			for ( MGInt j=j1; j<=j2; j++)
			{
				mtxA(is2,j) = ( mtxA(is1,j) - mtxA(is1,j-1) ) / mtxNdu(ipk+1,irk+j);
				d += mtxA(is2,j) * mtxNdu(irk+j,ipk);
			}

			if ( r <= ipk)
			{
				mtxA(is2,k) = -mtxA(is1,k-1) / mtxNdu(ipk+1,r);
				d += mtxA(is2,k) * mtxNdu(r,ipk);
			}

			mtxD(k,r) = d;
			MGSize j = is1;
			is1 = is2;
			is2 = j;
		}
	}

	MGSize r = deg;
	for ( MGSize k=1; k<=nder; k++)
	{
		for ( MGSize j=0; j<=deg; j++)
//		for ( j=deg; j>=0; j--)
			mtxD(k,j) *= r;
		r *= (deg-k);
	}
}

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
} // end of namespace IOSpace
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace NURBS = NURBSSpace;


#endif // __KNOTVECTOR_H__
