#ifndef __HF_GMATRIX_H__
#define __HF_GMATRIX_H__


#include "libhfgeom/hf_typedef.h"
#include "libhfgeom/hf_gvector.h"


#ifdef _MATRIX_SIZE_CHECKING

	#define CHECK(f) \
	{ \
		if (! (f) ) \
			THROW_INTERNAL( "CHECK error: '" #f "'"); \
	}

#else // _MATRIX_SIZE_CHECKING

	#define CHECK(f) \
	{ \
	}

#endif // _MATRIX_SIZE_CHECKING




//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace HFGeomTools {


//////////////////////////////////////////////////////////////////////
// class GMatrix
//////////////////////////////////////////////////////////////////////
template<class ELEM_TYPE, TSize MAX_SIZE>
class GMatrix
{
public:
	enum { SIZE = MAX_SIZE * MAX_SIZE };
	typedef ELEM_TYPE	TYPE;

	static TSize	MaxSize()		{ return MAX_SIZE;}


	GMatrix();
	GMatrix( const TSize& nrows, const TSize& ncols);
	GMatrix( const TSize& nrows, const TSize& ncols, const ELEM_TYPE& d);
	GMatrix( const GMatrix<ELEM_TYPE,MAX_SIZE>& a);
	GMatrix( const GVector<ELEM_TYPE,MAX_SIZE>& v);


    void	Init( const ELEM_TYPE& d);
	void	InitDiagUni( const TSize& n);
	void	InitDiag( const GVector<ELEM_TYPE,MAX_SIZE>& v);
	void	InitDiag( const TSize& n, const ELEM_TYPE& t);

    void	Resize( const TSize& nrows, const TSize& ncols);
	void	Resize( const TSize& nrows, const TSize& ncols, const ELEM_TYPE& d)	{ Resize(nrows, ncols); Init(d);}


	const TSize&	NRows() const	{ return mNRows;}
	const TSize&	NCols() const	{ return mNCols;}

	TSize&			rNRows() 	{ return mNRows;}
	TSize&			rNCols() 	{ return mNCols;}


    ELEM_TYPE&			operator()(const TSize& i, const TSize& j)		{ return mtab[i*MAX_SIZE + j]; }
    const ELEM_TYPE&	operator()(const TSize& i, const TSize& j) const	{ return mtab[i*MAX_SIZE + j]; }

    ELEM_TYPE&			operator[](const TSize& i)							{ return mtab[i]; }
    const ELEM_TYPE&	operator[](const TSize& i) const					{ return mtab[i]; }


	GMatrix<ELEM_TYPE, MAX_SIZE>&	operator = ( const GMatrix<ELEM_TYPE,MAX_SIZE>& a);
	GMatrix<ELEM_TYPE, MAX_SIZE>&	operator = ( const GVector<ELEM_TYPE,MAX_SIZE>& v);

	GMatrix<ELEM_TYPE, MAX_SIZE>&	operator+=( const GMatrix<ELEM_TYPE,MAX_SIZE>& a);
	GMatrix<ELEM_TYPE, MAX_SIZE>&	operator-=( const GMatrix<ELEM_TYPE,MAX_SIZE>& a);

	GMatrix<ELEM_TYPE, MAX_SIZE>&	operator+=( const ELEM_TYPE& d);
	GMatrix<ELEM_TYPE, MAX_SIZE>&	operator-=( const ELEM_TYPE& d);
	GMatrix<ELEM_TYPE, MAX_SIZE>&	operator*=( const ELEM_TYPE& d);



	void	InsertRow( const TSize& irow, const GVector<ELEM_TYPE,MAX_SIZE>& v);
	void	InsertCol( const TSize& icol, const GVector<ELEM_TYPE,MAX_SIZE>& v);

	void	InsertSubMtx( const GMatrix<ELEM_TYPE,MAX_SIZE>& mtx, const TSize& irow, const TSize& icol, const TSize& nrow, const TSize& ncol);
	void	ExtractSubMtx( GMatrix<ELEM_TYPE,MAX_SIZE>& mtx, const TSize& irow, const TSize& icol, const TSize& nrow, const TSize& ncol) const;

	void	Mult( GVector<ELEM_TYPE,MAX_SIZE>& vres, const GVector<ELEM_TYPE,MAX_SIZE>& v) const;


	void	Transp();
	void	SquareOut();
	void	SquareIn();

	void	Write( const TSize& iprec=5, ostream& f=cout) const;


protected:

	ELEM_TYPE* GetTab() { return mtab; };

private:
	TSize		mNRows;
	TSize		mNCols;
    ELEM_TYPE	mtab[SIZE];
};
//////////////////////////////////////////////////////////////////////





template<class ELEM_TYPE, TSize MAX_SIZE>
inline GMatrix<ELEM_TYPE, MAX_SIZE>::GMatrix() : mNRows(MAX_SIZE), mNCols(MAX_SIZE)
{
	for ( TSize i=0; i<SIZE; mtab[i++]=ELEM_TYPE(0) );
}

template<class ELEM_TYPE, TSize MAX_SIZE>
inline GMatrix<ELEM_TYPE, MAX_SIZE>::GMatrix( const TSize& nrows, const TSize& ncols) : mNRows(nrows), mNCols(ncols)
{
	CHECK( nrows <= MAX_SIZE);
	CHECK( ncols <= MAX_SIZE);

	for ( TSize i=0; i<SIZE; mtab[i++]=ELEM_TYPE(0) );
}

template<class ELEM_TYPE, TSize MAX_SIZE>
inline GMatrix<ELEM_TYPE, MAX_SIZE>::GMatrix( const TSize& nrows, const TSize& ncols, const ELEM_TYPE& d) : mNRows(nrows), mNCols(ncols)
{
	CHECK( nrows <= MAX_SIZE);
	CHECK( ncols <= MAX_SIZE);

	for ( TSize i=0; i<SIZE; mtab[i++]=d );

	//for ( TSize i=0; i<n; i++)
	//	mtab[i] = d;
}


template<class ELEM_TYPE, TSize MAX_SIZE>
inline GMatrix<ELEM_TYPE, MAX_SIZE>::GMatrix( const GMatrix<ELEM_TYPE, MAX_SIZE>& a) : mNRows(a.NRows()), mNCols(a.NCols())
{
	CHECK( a.NRows() <= MAX_SIZE);
	CHECK( a.NCols() <= MAX_SIZE);

    for (TSize i=0; i < mNRows; ++i)
      for (TSize j=0; j < mNCols; ++j)
        (*this)(i,j) = a(i,j);
}

template<class ELEM_TYPE, TSize MAX_SIZE>
inline GMatrix<ELEM_TYPE, MAX_SIZE>::GMatrix( const GVector<ELEM_TYPE,MAX_SIZE>& v) : mNRows(v.NRows()), mNCols(1)
{
	CHECK( v.NRows() <= MAX_SIZE);

	for ( TSize i=0; i<mNRows; ++i)
		(*this)(i,0) = v[i];
}




template<class ELEM_TYPE, TSize MAX_SIZE>
inline void GMatrix<ELEM_TYPE, MAX_SIZE>::Init( const ELEM_TYPE& d)
{
	for ( TSize i=0; i<SIZE; mtab[i++]=d );
	//const TSize n = MAX_SIZE*MAX_SIZE;
	//for ( TSize i=0; i<n; ++i)
	//	mtab[i] = d;
}


template<class ELEM_TYPE, TSize MAX_SIZE>
inline void GMatrix<ELEM_TYPE, MAX_SIZE>::InitDiagUni( const TSize& n)
{
	CHECK( n <= MAX_SIZE);
	mNRows = mNCols = n;

	for ( TSize i=0; i<SIZE; mtab[i++]=ELEM_TYPE(0) );

	for (TSize i=0; i < mNRows; ++i)
		(*this)(i,i) = ELEM_TYPE(1);
}


template<class ELEM_TYPE, TSize MAX_SIZE>
inline void GMatrix<ELEM_TYPE, MAX_SIZE>::InitDiag( const GVector<ELEM_TYPE,MAX_SIZE>& v)
{
	CHECK( v.Size() <= MAX_SIZE);
	mNRows = mNCols = v.Size();

	for ( TSize i=0; i<SIZE; mtab[i++]=ELEM_TYPE(0) );

    for (TSize i=0; i < mNRows; ++i)
		(*this)(i,i) = v(i);
}

template<class ELEM_TYPE, TSize MAX_SIZE>
inline void GMatrix<ELEM_TYPE, MAX_SIZE>::InitDiag( const TSize& n, const ELEM_TYPE& t)
{
	CHECK( n <= MAX_SIZE);
	mNRows = mNCols = n;

	for ( TSize i=0; i<SIZE; mtab[i++]=ELEM_TYPE(0) );

    for (TSize i=0; i < mNRows; ++i)
		(*this)(i,i) = t;
}


template<class ELEM_TYPE, TSize MAX_SIZE>
inline void GMatrix<ELEM_TYPE, MAX_SIZE>::Resize( const TSize& nrows, const TSize& ncols)
{
	CHECK( nrows <= MAX_SIZE);
	CHECK( ncols <= MAX_SIZE);
	mNRows = nrows;
	mNCols = ncols;
}

//////////////////////////////////////////////////////////////////////


template<class ELEM_TYPE, TSize MAX_SIZE>
inline void GMatrix<ELEM_TYPE, MAX_SIZE>::Transp()
{
	GMatrix<ELEM_TYPE, MAX_SIZE>	atmp(mNCols, mNRows);

	for( TSize i=0; i<mNRows; ++i)
		for( TSize j=0; j<mNCols; ++j)
			atmp(j,i) = (*this)(i,j);

	(*this) = atmp;
}


template<class ELEM_TYPE, TSize MAX_SIZE>
inline void GMatrix<ELEM_TYPE, MAX_SIZE>::SquareOut()
{
	GMatrix<ELEM_TYPE, MAX_SIZE>	atmp(mNCols, mNRows);

	for( TSize i=0; i<mNRows; ++i)
		for( TSize j=0; j<mNCols; ++j)
		{
			atmp(i,j) = (*this)(0,i) * (*this)(0,j);
			for( TSize k=1; k<mNCols; ++k)
				atmp(i,j) += (*this)(k,i) * (*this)(k,j);
		}

	(*this) = atmp;
}

template<class ELEM_TYPE, TSize MAX_SIZE>
inline void GMatrix<ELEM_TYPE, MAX_SIZE>::SquareIn()
{
	GMatrix<ELEM_TYPE, MAX_SIZE>	atmp(mNCols, mNRows);

	for( TSize i=0; i<mNRows; ++i)
		for( TSize j=0; j<mNCols; ++j)
		{
			atmp(i,j) = (*this)(i,0) * (*this)(j,0);
			for( TSize k=1; k<mNCols; ++k)
				atmp(i,j) += (*this)(i,k) * (*this)(j,k);
		}

	(*this) = atmp;
}

template<class ELEM_TYPE, TSize MAX_SIZE>
inline void GMatrix<ELEM_TYPE, MAX_SIZE>::Mult( GVector<ELEM_TYPE,MAX_SIZE>& vres, const GVector<ELEM_TYPE,MAX_SIZE>& v) const
{
	for( TSize i=0; i<mNRows; ++i)
	{
		vres[i] = ELEM_TYPE(0);
		for( TSize j=0; j<mNCols; ++j)
			vres[i] += (*this)(i,j) * v[j];
	}
}




//template<class ELEM_TYPE, TSize MAX_SIZE>
//template < TSize N, class T >
//inline void GMatrix<ELEM_TYPE, MAX_SIZE>::InsertSubMtx( const TSize& irow, const TSize& icol, const GMatrix<N,T>& mtx)
//{
//	CHECK( mtx.NRows()+irow <= mNRows && mtx.NCols()+icol <= mNCols );
//
//	if ( !( mtx.NRows()+irow <= mNRows && mtx.NCols()+icol <= mNCols ) )
//		THROW_INTERNAL( "CRASH !");
//
//	for ( TSize i=0; i<mtx.NRows(); ++i)
//		for ( TSize j=0; j<mtx.NCols(); ++j)
//			(*this)(i+irow,j+icol) = mtx(i,j);
//}


template<class ELEM_TYPE, TSize MAX_SIZE>
inline void GMatrix<ELEM_TYPE, MAX_SIZE>::ExtractSubMtx( GMatrix<ELEM_TYPE,MAX_SIZE>& mtx, const TSize& irow, const TSize& icol, const TSize& nrow, const TSize& ncol) const
{
	CHECK( irow+nrow < MAX_SIZE);
	CHECK( icol+ncol < MAX_SIZE);

	mtx.Resize( nrow, ncol);

	for ( TSize i=0; i<nrow; ++i)
		for ( TSize j=0; j<ncol; ++j)
			mtx(i,j) = (*this)(i+irow,j+icol);
}


template<class ELEM_TYPE, TSize MAX_SIZE>
inline void GMatrix<ELEM_TYPE, MAX_SIZE>::InsertSubMtx( const GMatrix<ELEM_TYPE,MAX_SIZE>& mtx, const TSize& irow, const TSize& icol, const TSize& nrow, const TSize& ncol)
{
	CHECK( irow+nrow < MAX_SIZE);
	CHECK( icol+ncol < MAX_SIZE);

	mtx.Resize( nrow, ncol);

	for ( TSize i=0; i<nrow; ++i)
		for ( TSize j=0; j<ncol; ++j)
			(*this)(i+irow,j+icol) = mtx(i,j);
}





//////////////////////////////////////////////////////////////////////

template<class ELEM_TYPE, TSize MAX_SIZE>
inline GMatrix<ELEM_TYPE, MAX_SIZE>& GMatrix<ELEM_TYPE, MAX_SIZE>::operator = ( const GMatrix<ELEM_TYPE, MAX_SIZE>& a)
{
	CHECK( a.NRows() <= MAX_SIZE);
	CHECK( a.NCols() <= MAX_SIZE);
	mNRows = a.mNRows;
	mNCols = a.mNCols;

    for (TSize i=0; i < mNRows; ++i)
      for (TSize j=0; j < mNCols; ++j)
        (*this)(i,j) = a(i,j);

	return *this;
}


template<class ELEM_TYPE, TSize MAX_SIZE>
inline GMatrix<ELEM_TYPE,MAX_SIZE>& GMatrix<ELEM_TYPE,MAX_SIZE>::operator += ( const GMatrix<ELEM_TYPE,MAX_SIZE>& a)
{
	CHECK( (NRows() == a.NRows()) && (NCols() == a.NCols()) );

	for( TSize i=0; i<a.NRows(); ++i)
		for( TSize j=0; j<a.NCols(); ++j)
			(*this)(i,j) += a(i,j);

	return *this;
}

template<class ELEM_TYPE, TSize MAX_SIZE>
inline GMatrix<ELEM_TYPE,MAX_SIZE>& GMatrix<ELEM_TYPE,MAX_SIZE>::operator -= ( const GMatrix<ELEM_TYPE,MAX_SIZE>& a)
{
	CHECK( (NRows() == a.NRows()) && (NCols() == a.NCols()) );

	for( TSize i=0; i<a.NRows(); ++i)
		for( TSize j=0; j<a.NCols(); ++j)
			(*this)(i,j) -= a(i,j);

	return *this;
}

template<class ELEM_TYPE, TSize MAX_SIZE>
inline GMatrix<ELEM_TYPE,MAX_SIZE>& GMatrix<ELEM_TYPE,MAX_SIZE>::operator += ( const ELEM_TYPE& d)
{
	for( TSize i=0; i<NRows(); ++i)
		for( TSize j=0; j<NCols(); ++j)
			(*this)(i,j) += d;

	return *this;
}

template<class ELEM_TYPE, TSize MAX_SIZE>
inline GMatrix<ELEM_TYPE,MAX_SIZE>& GMatrix<ELEM_TYPE,MAX_SIZE>::operator -= ( const ELEM_TYPE& d)
{
	for( TSize i=0; i<NRows(); ++i)
		for( TSize j=0; j<NCols(); ++j)
			(*this)(i,j) -= d;

	return *this;
}

template<class ELEM_TYPE, TSize MAX_SIZE>
inline GMatrix<ELEM_TYPE,MAX_SIZE>& GMatrix<ELEM_TYPE,MAX_SIZE>::operator *= ( const ELEM_TYPE& d)
{
	for( TSize i=0; i<NRows(); ++i)
		for( TSize j=0; j<NCols(); ++j)
			(*this)(i,j) *= d;

	return *this;
}


//////////////////////////////////////////////////////////////////////

template<class ELEM_TYPE, TSize MAX_SIZE>
inline GMatrix<ELEM_TYPE,MAX_SIZE> operator * ( const GMatrix<ELEM_TYPE,MAX_SIZE>& a1, const GMatrix<ELEM_TYPE,MAX_SIZE>& a2)
{
	CHECK( a1.NCols() == a2.NRows() );

	GMatrix<ELEM_TYPE,MAX_SIZE>	atmp(a1.NRows(), a2.NCols());
	for( TSize i=0; i<a1.NRows(); ++i)
		for( TSize j=0; j<a2.NCols(); ++j)
		{
			atmp(i,j) = ELEM_TYPE(0);
			for( TSize k=0; k<a1.NCols(); ++k)
				atmp(i,j) += a1(i,k) * a2(k,j);
		}

	return atmp;
}


template<class ELEM_TYPE, TSize MAX_SIZE>
inline GMatrix<ELEM_TYPE,MAX_SIZE> operator * ( const ELEM_TYPE& d, const GMatrix<ELEM_TYPE,MAX_SIZE>& a)
{
	GMatrix<ELEM_TYPE,MAX_SIZE>	atmp(a.NRows(), a.NCols());
	for( TSize i=0; i<a.NRows(); ++i)
		for( TSize j=0; j<a.NCols(); ++j)
			atmp(i,j) = d * a(i,j);

	return atmp;
}


template<class ELEM_TYPE, TSize MAX_SIZE>
inline GMatrix<ELEM_TYPE,MAX_SIZE> operator * ( const GMatrix<ELEM_TYPE,MAX_SIZE>& a, const ELEM_TYPE& d)
{
	GMatrix<ELEM_TYPE,MAX_SIZE>	atmp(a.NRows(), a.NCols());
	for( TSize i=0; i<a.NRows(); ++i)
		for( TSize j=0; j<a.NCols(); ++j)
			atmp(i,j) = d * a(i,j);

	return atmp;
}


template<class ELEM_TYPE, TSize MAX_SIZE>
inline GMatrix<ELEM_TYPE,MAX_SIZE> operator / ( const GMatrix<ELEM_TYPE,MAX_SIZE>& a, const ELEM_TYPE& d)
{
	GMatrix<ELEM_TYPE,MAX_SIZE>	atmp(a.NRows(), a.NCols());
	for( TSize i=0; i<a.NRows(); ++i)
		for( TSize j=0; j<a.NCols(); ++j)
			atmp(i,j) = a(i,j) / d;

	return atmp;
}


template<class ELEM_TYPE, TSize MAX_SIZE>
inline GMatrix<ELEM_TYPE,MAX_SIZE> operator + ( const GMatrix<ELEM_TYPE,MAX_SIZE>& a1, const GMatrix<ELEM_TYPE,MAX_SIZE>& a2)
{
	CHECK( (a1.NRows() == a2.NRows()) && (a1.NCols() == a2.NCols()) );

	GMatrix<ELEM_TYPE,MAX_SIZE>	atmp(a1.NRows(), a1.NCols());
	for( TSize i=0; i<a1.NRows(); ++i)
		for( TSize j=0; j<a1.NCols(); ++j)
			atmp(i,j) = a1(i,j) + a2(i,j);

	return atmp;
}


template<class ELEM_TYPE, TSize MAX_SIZE>
inline GMatrix<ELEM_TYPE,MAX_SIZE> operator - ( const GMatrix<ELEM_TYPE,MAX_SIZE>& a1, const GMatrix<ELEM_TYPE,MAX_SIZE>& a2)
{
	CHECK( (a1.NRows() == a2.NRows()) && (a1.NCols() == a2.NCols()) );

	GMatrix<ELEM_TYPE,MAX_SIZE>	atmp(a1.NRows(), a1.NCols());
	for( TSize i=0; i<a1.NRows(); ++i)
		for( TSize j=0; j<a1.NCols(); ++j)
			atmp(i,j) = a1(i,j) - a2(i,j);

	return atmp;
}


//////////////////////////////////////////////////////////////////////

template<class ELEM_TYPE, TSize MAX_SIZE>
inline void GMatrix<ELEM_TYPE, MAX_SIZE>::Write( const TSize& iprec, ostream& f) const
{
	f << "+-";
	for( TSize j=0; j<mNCols; ++j)
		f << "-------------";
	f << "-+" << endl;

	for( TSize i=0; i<mNRows; ++i)
	{
		f << "| ";
		for( TSize j=0; j<mNCols; ++j)
			f << setprecision(iprec) << setw(12) << (*this)(i,j) << " ";
		f << " |" << endl;
	}

	f << "+-";
	for( TSize j=0; j<mNCols; ++j)
		f << "-------------";
	f << "-+" << endl;
}



} // end of namespace HFGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace HFGeom = HFGeomTools;

#endif // __HF_GMATRIX_H__