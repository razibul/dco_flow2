#ifndef __HF_TREE_H__
#define __HF_TREE_H__

#include "libhfgeom/hf_typedef.h"
#include "libhfgeom/hf_treeleaf.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace HFGeomTools {


//////////////////////////////////////////////////////////////////////
//  class Tree
//////////////////////////////////////////////////////////////////////
template <class TELEM, class TCOMP, class DATA, TDimension DIM>
class Tree : public Leaf<TELEM,TCOMP,DATA,DIM>
{
public:
	typedef	Leaf<TELEM,TCOMP,DATA,DIM>						TLeaf;
	typedef typename Leaf<TELEM,TCOMP,DATA,DIM>::TVect		TVect;
	typedef typename Leaf<TELEM,TCOMP,DATA,DIM>::TCmpVect	TCmpVect;
	typedef typename Leaf<TELEM,TCOMP,DATA,DIM>::TPair		TPair;
	typedef typename Leaf<TELEM,TCOMP,DATA,DIM>::TBox		TBox;


	// ns - max number of points stored at single leaf
	// zero - box border threshold
	explicit Tree( const TELEM& zero, const TSize& ns);

	~Tree();
	
	const GBox<TELEM,DIM>&		cBox() const		{ return mBox;}
	
	
	
	void	InitBox( const GBox<TELEM,DIM>& box);
	void	Insert( const TVect& pos, const DATA& dat);
	void	Erase( const TVect& pos, const DATA& dat);

	// true -  the res is valid
	// false - search failed (should never happend if the pos is inside the box)
	bool	GetClosestItem( TPair& res, const TVect& pos) const;
	
	bool	GetRangeItems( vector<TPair>& tab, const TVect& pos, const TELEM& dist) const;

	// destroys all the children and data
	void	RemoveAll()		{ if ( mpRoot) Remove( mpRoot); }
	
	void	ExportTEC( char sname[]);


protected:

	TLeaf*	FindLeaf( const TVect& pos);

	// needs some modification in order to properly remove data from the tree
	void	FindAndRemove( const TVect& pos, const DATA& dat);

	void	Remove( TLeaf* plf);

	void	RangeSearch( const GBox<TELEM,DIM>& rec, vector<TLeaf*>& tab) const;
	bool	CheckRange( const TVect& vct)	{ return mBox.IsInside( vct);}
	bool	ClosestItemFromList( TPair& res, const TVect& pos, vector<TLeaf*>& tab) const;

	
private:
	TELEM			mZero;
	TSize			mDataSize;			// maximum allowed size of the mtabData; can be changed during runtime

	GBox<TELEM,DIM>	mBox;
	TLeaf			*mpRoot;
};
//////////////////////////////////////////////////////////////////////



template <class TELEM, class TCOMP, class DATA, TDimension DIM> 
inline Tree<TELEM,TCOMP,DATA,DIM>::Tree( const TELEM& zero, const TSize& ns)
{
	mZero = zero;
	mDataSize = ns;
	mpRoot = NULL;
	mBox = GBox<TELEM,DIM>( TVect(), TVect() );
}

template <class TELEM, class TCOMP, class DATA, TDimension DIM> 
inline Tree<TELEM,TCOMP,DATA,DIM>::~Tree()
{
	if ( mpRoot)
		delete mpRoot;
}


template <class TELEM, class TCOMP, class DATA, TDimension DIM> 
void Tree<TELEM,TCOMP,DATA,DIM>::InitBox( const GBox<TELEM,DIM>& box)	
{
	TVect	dv;
	
	mBox = box; 

	dv = mBox.cVMax() - mBox.cVMin();

	TELEM	h = 0;
	for ( TSize i=0; i<DIM; ++i)
		if ( h < abs( dv.cX(i) ) )
			h = abs( dv.cX(i) );
		
	for ( TSize i=0; i<DIM; ++i)
		if ( abs( dv.cX(i) ) < h )
		{
			TELEM dx = (h - dv.cX(i)) / TELEM(2);

			mBox.rVMax().rX(i) += dx;
			mBox.rVMin().rX(i) -= dx;
		}

	TLeaf::rCenter() = mBox.cVMin(); // it is used for boundbox calc in mRoot leaf
}


template <class TELEM, class TCOMP, class DATA, TDimension DIM> 
void Tree<TELEM,TCOMP,DATA,DIM>::Insert( const TVect& pos, const DATA& dat)
{
	if ( mpRoot == NULL)
		mpRoot = new TLeaf( mBox.Center(), this );
	
	mpRoot->Insert( pos, dat, mDataSize, mBox );
}



template <class TELEM, class TCOMP, class DATA, TDimension DIM> 
void Tree<TELEM,TCOMP,DATA,DIM>::RangeSearch( const GBox<TELEM,DIM>& rec, vector<TLeaf*>& tab) const
{
	if ( mpRoot)
		mpRoot->RangeSearch( rec, tab);
}


template <class TELEM, class TCOMP, class DATA, TDimension DIM> 
bool Tree<TELEM,TCOMP,DATA,DIM>::ClosestItemFromList( TPair& res, const TVect& pos, vector<TLeaf*>& tab) const
{
	typename vector<TLeaf*>::iterator	itr;
	TPair	data;
	TLeaf*	plf = NULL;
	TSize	ind;
	TCOMP	dmin;
	bool	bFirst = true;
	
	for ( itr= tab.begin(); itr != tab.end(); ++itr)
	{
		if ( bFirst)
		{
			if ( ! (*itr)->HaveData() )
				THROW_INTERNAL( "problem inside Tree<TELEM,TCOMP,DATA,DIM>::ClosestItemFromList(...)");

			ind = (*itr)->IndClosestData( pos);

			TCmpVect vct = pos - (*itr)->cPos(ind);
			dmin = sqrt( vct * vct);

			res.first = (*itr)->cPos(ind);
			res.second = (*itr)->cData(ind);
			plf = *itr;
			bFirst = false;
		}
		else
		{
			if ( ! (*itr)->HaveData() )
				THROW_INTERNAL( "problem inside Tree<TELEM,TCOMP,DATA,DIM>::ClosestItemFromList(...)");

			ind = (*itr)->IndClosestData( pos);

			TCmpVect vct = pos - (*itr)->cPos(ind);
			TCOMP dst = sqrt( vct * vct);

			if ( dst < dmin)
			{
				res.first = (*itr)->cPos(ind);
				res.second = (*itr)->cData(ind);
				plf = *itr;
				dmin = dst;
			}
		}
	}
	return true;
}


template <class TELEM, class TCOMP, class DATA, TDimension DIM> 
bool Tree<TELEM,TCOMP,DATA,DIM>::GetClosestItem( TPair& res, const TVect& pos) const
{
	static vector<TLeaf*>				tab;
	typename vector<TLeaf*>::iterator	itr;
	
	bool bret;
	TPair			data;
	TLeaf*			plf;
	GBox<TCOMP,DIM>	rec;
	TCOMP			dst;

	TCmpVect	vdist = mBox.cVMax() - mBox.cVMin();
	TCOMP	dmin = vdist * vdist;

	tab.reserve(80);
	
	if ( mpRoot && mBox.IsInside( pos) )
	{
		plf = mpRoot->Traverse( pos);
		if ( plf->HaveData())
		{
			TSize idat = plf->IndClosestData( pos);

			TCmpVect vct = pos - plf->cPos(idat);
			dst = sqrt( vct * vct );
		}
		else
		{
			if ( plf == mpRoot)
				rec = mBox;
			else
				rec = plf->FindBoundBox();
			
			plf->RangeSearch( rec, tab);
			bret = ClosestItemFromList( data, pos, tab);
			
			tab.clear();

			if ( ! bret )
				THROW_INTERNAL("Problem ...");


			TCmpVect vct = pos - data.first;
			dst = sqrt( vct * vct );
		}
		
		for ( TSize i=0; i<DIM; ++i)
		{
			rec.rVMin().rX(i) = TCOMP( pos.cX(i)) - TCOMP( dst) - TCOMP( mZero);
			rec.rVMax().rX(i) = TCOMP( pos.cX(i)) + TCOMP( dst) + TCOMP( mZero);
		}
		
		RangeSearch( rec, tab);
		bret = ClosestItemFromList( res, pos, tab);

		//static TELEM rs = 0;
		//static TELEM rw = 0;
		//static TELEM rmax = 0;
		//if ( tab.size() > rmax)
		//	rmax = tab.size();
		//rs += tab.size();
		//rw += 1;
		//printf( "avr tree tab size = %lf  %lf\n", rs/ rw, rmax);

		tab.resize(0);
		
		return true;
	}
	
	return false;
}


template <class TELEM, class TCOMP, class DATA, TDimension DIM> 
bool Tree<TELEM,TCOMP,DATA,DIM>::GetRangeItems( vector<TPair>& tabres, const TVect& pos, const TELEM& dist) const
{
	GBox<TCOMP,DIM>	rec;

	static vector<TLeaf*>				tab;

	if ( mpRoot )
	{
		for ( TSize i=0; i<DIM; ++i)
		{
			rec.rVMin().rX(i) = TCOMP( pos.cX(i)) - TCOMP( dist) - TCOMP( mZero);
			rec.rVMax().rX(i) = TCOMP( pos.cX(i)) + TCOMP( dist) + TCOMP( mZero);
		}

		
		RangeSearch( rec, tab);

		for ( typename vector<TLeaf*>::iterator itr= tab.begin(); itr != tab.end(); ++itr)
		{
			if ( ! (*itr)->HaveData() )
				continue;
				//THROW_INTERNAL( "problem inside Tree<TELEM,TCOMP,DATA,DIM>::GetRangeItems(...)");

			for ( TSize k=0; k<(*itr)->mtabData.size(); ++k)
			{
				TCmpVect cvct = TCmpVect(pos) - TCmpVect( (*itr)->cPos(k) );
				TCOMP dmod = sqrt( cvct * cvct);;

				if ( dmod <= dist + TCOMP( mZero) )
				{
					TPair res( (*itr)->cPos(k), (*itr)->cData(k) );
					//res.first = (*itr)->cPos(k);
					//res.second = (*itr)->cData(k);

					tabres.push_back( res);
				}
			}

		}

		return true;
	}

	return false;
}



template <class TELEM, class TCOMP, class DATA, TDimension DIM> 
Leaf<TELEM,TCOMP,DATA,DIM>* Tree<TELEM,TCOMP,DATA,DIM>::FindLeaf( const TVect& pos)
{
	if ( mpRoot)
		return mpRoot->Traverse( pos);
		
	return NULL;
}




template <class TELEM, class TCOMP, class DATA, TDimension DIM> 
void Tree<TELEM,TCOMP,DATA,DIM>::Erase( const TVect& pos, const DATA& dat)
{
	TLeaf	*plf = NULL;

	plf = FindLeaf( pos);

	plf->Erase( pos, dat);

	if ( ! plf->HaveData() )
		Remove( plf);
}


template <class TELEM, class TCOMP, class DATA, TDimension DIM> 
void Tree<TELEM,TCOMP,DATA,DIM>::FindAndRemove( const TVect& pos, const DATA& dat)
{
	TLeaf	*plf = NULL;

	plf = FindLeaf( pos);

	if ( plf->Data() != dat)
		THROW_INTERNAL( "Tree::FindAndRemove : not consistent data");

	Remove( plf);
}


template <class TELEM, class TCOMP, class DATA, TDimension DIM> 
void Tree<TELEM,TCOMP,DATA,DIM>::Remove( TLeaf* plf)
{
	TLeaf* ppar;

	ASSERT( plf != NULL);

	if ( plf == mpRoot)
	{
		mpRoot = NULL;
	}
	else
	{
		ppar = plf->DetachFromParent();

		if ( ppar != NULL)
			if ( ppar->IsEmpty() )
				Remove( ppar);
	}

	delete plf;
}



template <class TELEM, class TCOMP, class DATA, TDimension DIM> 
void Tree<TELEM,TCOMP,DATA,DIM>::ExportTEC( char sname[])
{
	FILE *ftmp = fopen( sname, "wt");

	if ( mpRoot)
		mpRoot->ExportTEC( ftmp);
		
	fclose( ftmp);
}


} // end of namespace HFGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace HFGeom = HFGeomTools;



#endif // __HF_TREE_H__
