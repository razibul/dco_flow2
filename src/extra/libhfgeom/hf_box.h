#ifndef __HF_BOX_H__
#define __HF_BOX_H__

#include "libhfgeom/hf_typedef.h"
#include "libhfgeom/hf_gvector.h"



//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace HFGeomTools {




//////////////////////////////////////////////////////////////////////
//  class Box
//////////////////////////////////////////////////////////////////////
template <class TELEM, TDimension DIM>
class GBox
{
public:
	typedef GVector<TELEM,DIM>	GVect;

	GBox() : mvMin( GVect( static_cast<TELEM>(0)) ), mvMax( GVect( static_cast<TELEM>(0)) )	{}
	GBox( const GVect& vmin, const GVect& vmax) : mvMin( vmin), mvMax( vmax)	{}

	template <class T> 
	GBox( const GBox<T,DIM> &box) : mvMin( box.cVMin() ), mvMax( box.cVMax() )	{}

	GBox( const GBox<TELEM,DIM+1> &box, const TSize& idim)
	{
		rVMin() = GVect( box.cVMin(), idim);
		rVMax() = GVect( box.cVMax(), idim);
	}



	void	ExportTEC( ostream *f)	{};
	
	void	Equalize();

	bool	IsInside( const GVect& vct) const;
	bool	IsOverlapping( const GBox<TELEM,DIM>& box) const;

	bool	IsInsidePlus( const GVect& vct) const;
	bool	IsOverlappingPlus( const GBox<TELEM,DIM>& box) const;

	GVect			Center() const	{ return (mvMin + mvMax) / TELEM(2);}

	const GVect&	cVMin() const	{ return mvMin;}
		  GVect&	rVMin() 		{ return mvMin;}
		  
	const GVect&	cVMax() const	{ return mvMax;}
		  GVect&	rVMax() 		{ return mvMax;}


private:
	GVect	mvMin;
	GVect	mvMax;
};
//////////////////////////////////////////////////////////////////////


//template <class TELEM, TDimension DIM>
//template <class T> 
//inline GBox<TELEM,DIM>::GBox( const GBox<T,DIM> &box)
//{
//		mtab[i] = TELEM( vec.cX(i) );
//}


template <class TELEM, TDimension DIM>
inline void GBox<TELEM,DIM>::Equalize()
{
	GVect	dv;
	TELEM	h;
	
	dv = mvMax - mvMin;

	h = 0;
	for ( TSize i=0; i<DIM; ++i)
		if ( h < abs( dv.cX(i) ) )
			h = abs( dv.cX(i) );
		
	for ( TSize i=0; i<DIM; ++i)
		if ( abs( dv.cX(i) ) < h )
		{
			TELEM	d = ( h - dv.cX(i) ) / TELEM(2);
			mvMax.rX(i) += d;
			mvMin.rX(i) -= d;
			//mvMax.rX(i) += 0.5*(h-dv.cX(i));
			//mvMin.rX(i) -= 0.5*(h-dv.cX(i));
		}

}

template <class TELEM, TDimension DIM>
inline bool GBox<TELEM,DIM>::IsInside( const GVect& vct) const
{
	for ( TSize i=0; i<DIM; ++i)
		if ( vct.cX(i) > mvMax.cX(i) || vct.cX(i) < mvMin.cX(i) )
			return false;

	return true;
}

template <class TELEM, TDimension DIM>
inline bool GBox<TELEM,DIM>::IsInsidePlus( const GVect& vct) const
{
	for ( TSize i=0; i<DIM; ++i)
		if ( vct.cX(i) >= mvMax.cX(i) || vct.cX(i) <= mvMin.cX(i) )
			return false;

	return true;
}

//template < class T>
//inline bool Box<DIM_1D,T>::IsOverlapping( const Box<DIM_1D>& box) const
//{
//	if ( box.IsInside( VMin() ) || box.IsInside( VMax() ) ||
//		 IsInside( box.VMin() ) || IsInside( box.VMax() )  )
//	{
//		 return true;
//	}
//
//	return false;
//}


template <class TELEM, TDimension DIM>
inline bool GBox<TELEM,DIM>::IsOverlapping( const GBox<TELEM,DIM>& box) const
{
	GBox<TELEM,DIM_1D>	box1, box2;

	for ( TSize i=0; i<DIM; ++i)
	{
		box1.rVMin() = typename GBox<TELEM,DIM_1D>::GVect( cVMin().cX(i) );
		box1.rVMax() = typename GBox<TELEM,DIM_1D>::GVect( cVMax().cX(i) );

		box2.rVMin() = typename GBox<TELEM,DIM_1D>::GVect( box.cVMin().cX(i) );
		box2.rVMax() = typename GBox<TELEM,DIM_1D>::GVect( box.cVMax().cX(i) );

		if ( (!box1.IsInside( box2.cVMin() )) && (!box1.IsInside( box2.cVMax() )) &&
			 (!box2.IsInside( box1.cVMin() )) && (!box2.IsInside( box1.cVMax() ))  )
			 return false;
	}

	return true;
}


template <class TELEM, TDimension DIM>
inline bool GBox<TELEM,DIM>::IsOverlappingPlus( const GBox<TELEM,DIM>& box) const
{
	GBox<TELEM,DIM_1D>	box1, box2;

	for ( TSize i=0; i<DIM; ++i)
	{
		box1.rVMin() = typename GBox<TELEM,DIM_1D>::GVect( cVMin().cX(i) );
		box1.rVMax() = typename GBox<TELEM,DIM_1D>::GVect( cVMax().cX(i) );

		box2.rVMin() = typename GBox<TELEM,DIM_1D>::GVect( box.cVMin().cX(i) );
		box2.rVMax() = typename GBox<TELEM,DIM_1D>::GVect( box.cVMax().cX(i) );

		if ( (!box1.IsInsidePlus( box2.cVMin() )) && (!box1.IsInsidePlus( box2.cVMax() )) &&
			 (!box2.IsInsidePlus( box1.cVMin() )) && (!box2.IsInsidePlus( box1.cVMax() ))  )
			 return false;
	}

	return true;
}


template <class TELEM, TDimension DIM>
inline GBox<TELEM,DIM> Sum( const GBox<TELEM,DIM>& box1, const GBox<TELEM,DIM>& box2)
{
	GBox<TELEM,DIM> box;
	box.rVMin() = Minimum( box1.cVMin(), box2.cVMin() );
	box.rVMax() = Maximum( box1.cVMax(), box2.cVMax() );

	return box;
}


/*
template<>
inline void Box<DIM_1D>::ExportTEC( ostream *f)
{
	f << "VARIABLES = \"X\"" << endl;
	f << "ZONE I=2, F=POINT" << endl;
	f << mvMin.cX() << endl;
	f << mvMax.cX() << endl;
}


template<>
inline void Box<DIM_1D>::ExportTEC( FILE *f)
{
	fprintf( f, "VARIABLES = \"X\"\n");
	fprintf( f, "ZONE I=%d, F=POINT\n", 2);
	fprintf( f, "%lg\n", mvMin.cX() );
	fprintf( f, "%lg\n", mvMax.cX() );
}

template<>
inline void Box<DIM_2D>::ExportTEC( FILE *f)
{
	fprintf( f, "VARIABLES = \"X\",\"Y\"\n");

	fprintf( f, "ZONE  N=4, E=1, F=FEPOINT, ET=QUADRILATERAL\n");

	fprintf( f, "%lg %lg\n", mvMin.cX(), mvMin.cY() );
	fprintf( f, "%lg %lg\n", mvMax.cX(), mvMin.cY() );
	fprintf( f, "%lg %lg\n", mvMax.cX(), mvMax.cY() );
	fprintf( f, "%lg %lg\n", mvMin.cX(), mvMax.cY() );
	fprintf( f, "1 2 3 4\n" );
}

template<>
inline void Box<DIM_3D>::ExportTEC( FILE *f)
{
	fprintf( f, "VARIABLES = \"X\",\"Y\",\"Z\"\n");

	fprintf( f, "ZONE  N=8, E=1, F=FEPOINT, ET=BRICK\n");

	fprintf( f, "%lg %lg %lg\n", mvMin.cX(), mvMin.cY(), mvMin.cZ() );
	fprintf( f, "%lg %lg %lg\n", mvMax.cX(), mvMin.cY(), mvMin.cZ() );
	fprintf( f, "%lg %lg %lg\n", mvMax.cX(), mvMax.cY(), mvMin.cZ() );
	fprintf( f, "%lg %lg %lg\n", mvMin.cX(), mvMax.cY(), mvMin.cZ() );
	fprintf( f, "%lg %lg %lg\n", mvMin.cX(), mvMin.cY(), mvMax.cZ() );
	fprintf( f, "%lg %lg %lg\n", mvMax.cX(), mvMin.cY(), mvMax.cZ() );
	fprintf( f, "%lg %lg %lg\n", mvMax.cX(), mvMax.cY(), mvMax.cZ() );
	fprintf( f, "%lg %lg %lg\n", mvMin.cX(), mvMax.cY(), mvMax.cZ() );
	fprintf( f, "1 2 3 4 5 6 7 8\n" );
}
*/



} // end of namespace HFGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace HFGeom = HFGeomTools;




#endif // __HF_BOX_H__
