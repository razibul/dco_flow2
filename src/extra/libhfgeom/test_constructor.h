#ifndef __HF_TEST_DETERMINANT_H__
#define __HF_TEST_DETERMINANT_H__


//#include "sysdecl.h"
#include "libcoresystem/mgdecl.h"
#include "libhfgeom/hf_typedef.h"
#include "libhfgeom/hf_constructor.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace HFGeomTools {


//////////////////////////////////////////////////////////////////////
// class Test_Constructor
//////////////////////////////////////////////////////////////////////
class Test_Constructor
{
public:
	bool	Run();

//private:

	template <class T, class TC, TDimension DIM>
	bool CheckCrossProduct_Random( const T& rmin, const T& rmax, const TSize& n);

	template <class T, class TC, TDimension DIM>
	bool CheckCrossProduct_Base();

};
//////////////////////////////////////////////////////////////////////


template <class T, class TC, TDimension DIM>
bool Test_Constructor::CheckCrossProduct_Random( const T& rmin, const T& rmax, const TSize& n)
{
	typedef GVector<T,DIM> VECT;
	typedef GVector<TC,DIM> CVECT;
	typedef ArrayData<VECT,DIM-1> ARRAY;

	for ( TSize iter=0; iter<n; ++iter)
	{
		VECT	tabv[DIM-1];

		for ( TSize i=0; i<DIM-1; ++i)
		{
			for ( TSize j=0; j<DIM; ++j)
			{
				T u = static_cast<T>( ( TC(rmax - rmin) * TC( rand()) ) / TC(RAND_MAX) + TC(rmin) );
				tabv[i].rX(j) = u;
			}

			tabv[i].Write();
		}

		CVECT vr = CrossProduct<T,TC,DIM>( (ARRAY(tabv)) );

		vr.Write();

		cout << endl;
	}

	return true;
}


template <class T, class TC, TDimension DIM>
bool Test_Constructor::CheckCrossProduct_Base()
{
	typedef GVector<T,DIM> VECT;
	typedef GVector<TC,DIM> CVECT;
	typedef ArrayData<VECT,DIM-1> ARRAY;

	VECT	tabv[DIM-1];

	for ( TSize i=0; i<DIM-1; ++i)
	{
		for ( TSize j=0; j<DIM; ++j)
		{
			if ( i == j)
				tabv[i].rX(j) = T(1);
			else
				tabv[i].rX(j) = T(0);
		}

		tabv[i].Write();
	}

	CVECT vr = CrossProduct<T,TC,DIM>( (ARRAY(tabv)) );

	vr.Write();

	cout << endl;

	return true;
}



} // end of namespace HFGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace HFGeom = HFGeomTools;



#endif // __HF_TEST_DETERMINANT_H__