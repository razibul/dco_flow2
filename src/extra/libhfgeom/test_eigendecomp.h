#ifndef __HF_TEST_EIGENDECOMP_H__
#define __HF_TEST_EIGENDECOMP_H__


//#include "sysdecl.h"
#include "libcoresystem/mgdecl.h"
#include "libhfgeom/hf_typedef.h"
#include "libhfgeom/hf_eigendecomp.h"
#include "libhfgeom/hf_initmatrix.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace HFGeomTools {


//////////////////////////////////////////////////////////////////////
// class Test_Determinant
//////////////////////////////////////////////////////////////////////
class Test_EigenDecomp
{
public:
	bool	Run();

//private:

	template <class T, TSize NMAX>
	bool CheckDecomp_Random( const TSize& n, const T& rmin, const T& rmax);

};
//////////////////////////////////////////////////////////////////////


template <class T, TSize NMAX>
bool Test_EigenDecomp::CheckDecomp_Random( const TSize& n, const T& rmin, const T& rmax)
{
	typedef HFGeom::GMatrix<T,NMAX> MTX;
	typedef HFGeom::GVector<T,NMAX> VEC;

	if ( n > NMAX)
		THROW_INTERNAL( "Test_Determinant : n (" << n << ") is greater then " << NMAX );

	MTX	mtx(n,n);
	HFGeom::InitMatrix< typename MTX::TYPE, MTX >::RandomSymmetric( mtx, n, rmin, rmax);

	mtx.Write();

	HFGeom::EigenDecomp< T, VEC, MTX >	eigen( mtx);

	eigen.Execute();



	MTX L, D, LT, A, ProN, ProNT;

	L = eigen.cMatrixR();
	LT = L;
	LT.Transp();
	D.InitDiag( eigen.cVectorD() );

	//A = LT * D * L;

	cout << "D =" << endl;
	D.Write();
	cout << "L =" << endl;
	L.Write();
	cout << "LT * D * L =" << endl;
	(LT * D * L).Write();

	(L*LT).Write();
	(LT*L).Write();

	eigen.AssembleProjMtx( ProN);
	cout << "ProN =" << endl;
	ProN.Write();

	ProNT = ProN;
	ProNT.Transp();

	(ProNT * ProN).Write();

	cout << "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" << endl;

	HFGeom::EigenDecomp< T, VEC, MTX >	eigen2( (ProNT * ProN) );
	eigen2.Execute();

	eigen.AssembleProjMtx( ProN);
	cout << "ProN =" << endl;
	ProN.Write();

	ProNT = ProN;
	ProNT.Transp();

	(ProNT * ProN).Write();
	(ProN * ProNT).Write();

	cout << "ProN.SquareOut =" << endl;
	ProNT = ProN;
	ProNT.SquareOut();
	ProNT.Write();

	cout << "ProN.SquareIn =" << endl;
	ProNT = ProN;
	ProNT.SquareIn();
	ProNT.Write();

	return true;
}




} // end of namespace HFGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace HFGeom = HFGeomTools;



#endif // __HF_TEST_EIGENDECOMP_H__
