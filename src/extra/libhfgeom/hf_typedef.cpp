
#include "hf_typedef.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace HFGeomTools {


const char	STRDIM_NONE[]	= "NONE";
const char	STRDIM_0D[]		= "0D";
const char	STRDIM_1D[]		= "1D";
const char	STRDIM_2D[]		= "2D";
const char	STRDIM_3D[]		= "3D";
const char	STRDIM_4D[]		= "4D"; 



TDimension StrToDim( const string& str)
{
	if		( str ==  string(STRDIM_1D) )
		return DIM_1D;
	else if ( str ==  string(STRDIM_2D) )
		return DIM_2D;
	else if ( str ==  string(STRDIM_3D) )
		return DIM_3D;
	else if ( str ==  string(STRDIM_4D) )
		return DIM_4D;
	else
		return 0;
}

string DimToStr( const TDimension& dim)
{
	switch (dim)
	{
	case DIM_1D:
		return string( STRDIM_1D);
	case DIM_2D:
		return string( STRDIM_2D);
	case DIM_3D:
		return string( STRDIM_3D);
	case DIM_4D:
		return string( STRDIM_4D);
	}

	return string( STRDIM_NONE);
} 

} // end of namespace HFGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

