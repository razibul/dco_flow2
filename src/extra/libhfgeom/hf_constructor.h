#ifndef __HF_CONSTRUCTOR_H__  
#define __HF_CONSTRUCTOR_H__  


#include "libhfgeom/hf_typedef.h"
#include "libhfgeom/hf_gvector.h"
#include "libhfgeom/hf_rational.h"

#include "libcorecommon/array.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace HFGeomTools {



//////////////////////////////////////////////////////////////////////
// CrossProduct - range of TC = (DIM-1) * range of T
template <class T, class TC, TDimension DIM>
GVector<TC,DIM>	CrossProduct( const ArrayData< GVector<T,DIM>,DIM-1>& data)
{
	GVector<TC,DIM> vret;
	T coeff( (DIM%2 ? -1 : 1) );

	for ( TSize idim=0; idim<DIM; ++idim)
	{
		coeff = - coeff;

		GMatrix<TC,DIM-1> mtx(DIM-1,DIM-1);

		for ( TSize ir=0; ir<DIM-1; ++ir)
			for ( TSize ic=0; ic<DIM; ++ic)
			{
				if ( idim == ic)
					continue;

				TSize imc = (ic < idim ? ic : ic - 1);
				mtx(ir,imc) = data[ir].cX(ic);
			}

		vret.rX(idim) = coeff * DeterminantFunc( mtx);
	}

	return vret;
}


//////////////////////////////////////////////////////////////////////
// SpannedArea2 - range of TC = DIM * UNIDIM * range of T
template <class T, class TC, TDimension DIM, TDimension UNIDIM>
TC	SpannedArea2( const ArrayData< GVector<T,UNIDIM>,DIM>& data)
{
	GMatrix<TC,DIM> mtx;

	for ( TSize ir=0; ir<DIM; ++ir)
	{
		mtx(ir,ir) = GVector<TC,UNIDIM>( data[ir] ) * GVector<TC,UNIDIM>( data[ir] );
		for ( TSize ic=ir+1; ic<DIM; ++ic)
			mtx(ir,ic) = mtx(ic,ir) = data[ic] * data[ir];
	}
		
	return DeterminantFunc( mtx);
}



//////////////////////////////////////////////////////////////////////
// DistPointPoint2 - range of TC = 2 * range of T
template <class T, class TC, TDimension UNIDIM>
TC	DistPointPoint2( const GVector<T,UNIDIM>& p1, const GVector<T,UNIDIM>& p2)
{
	GVector<TC,UNIDIM> dp = GVector<TC,UNIDIM>(p2) - GVector<TC,UNIDIM>(p1);
	return dp * dp;
}



//////////////////////////////////////////////////////////////////////
// DistPointPoint2 - range of TC = 2 * range of T
template <class T, class TC, TDimension DIM, TDimension UNIDIM>
TC	DistPointSubspace( const GVector<T,UNIDIM>& point, const ArrayData< GVector<T,UNIDIM>,DIM>& data)
{
	TC abase = SpannedArea2<T,TC,DIM,UNIDIM>( data);
	TC atot  = SpannedArea2<T,TC,DIM+1,UNIDIM>( ArrayData< GVector<T,UNIDIM>,DIM+1>( point, data) );

	Rational<TC> ret(abase,atot);

	return  atot/abase;
}


template <class T, class TC, TDimension DIM, TDimension UNIDIM>
Rational<TC> IsInsideSubsimplex( const GVector<T,UNIDIM>& point, const ArrayData< GVector<T,UNIDIM>,DIM+1>& data)
{
}

//////////////////////////////////////////////////////////////////////
// DistPointPoint2 - range of TC = 2 * range of T
//template <class T, class TC, TDimension UNIDIM>
//TC	DistPointEdge2( const GVector<T,UNIDIM>& pnt, const GVector<T,UNIDIM>& e1, const GVector<T,UNIDIM>& e2)
//{
//	GVector<TC,UNIDIM> dp = GVector<TC,UNIDIM>(p2) - GVector<TC,UNIDIM>(p1);
//	return dp * dp;
//}





//////////////////////////////////////////////////////////////////////
// class Constructor
//////////////////////////////////////////////////////////////////////
template< class T, TDimension DIM>
class Constructor
{
public:
	typedef GVector<T,DIM>	GVect;

	Constructor()	{}

	template <class TC>
	GVector<TC,DIM>	CrossProduct( const ArrayData<GVect,DIM-1>& data)
	{
		GVector<TC,DIM> vret;
		T coeff( 1);

		for ( TSize idim=0; idim<DIM; ++idim)
		{
			coeff = - coeff;

			GMatrix<TC,DIM-1> mtx(DIM-1,DIM-1);

			for ( TSize ir=0; ir<DIM-1; ++ir)
				for ( TSize ic=0; ic<DIM; ++ic)
				{
					if ( idim == ic)
						continue;

					TSize imc = (ic < idim ? ic : ic - 1);
					mtx(ir,imc) = data[ir].cX(ic);
				}

			vret.rX(idim) = coeff * DeterminantFunc( mtx);
		}

		return vret;
	}

private:
};
//////////////////////////////////////////////////////////////////////


} // end of namespace HFGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace HFGeom = HFGeomTools;



#endif // __HF_CONSTRUCTOR_H__  
