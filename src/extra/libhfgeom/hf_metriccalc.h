#ifndef __HF_METRICCALC_H__
#define __HF_METRICCALC_H__


//#include "sysdecl.h"
#include "libcoresystem/mgdecl.h"
#include "libhfgeom/hf_typedef.h"
#include "libhfgeom/hf_eigendecomp.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace HFGeomTools {


//////////////////////////////////////////////////////////////////////
// class MetricCalc
// - T - should be floating point type
//////////////////////////////////////////////////////////////////////
template< class T, class VECTOR, class MATRIX>
class MetricCalc
{
public:
};




} // end of namespace HFGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace HFGeom = HFGeomTools;

#endif // __HF_METRICCALC_H__
