#ifndef __HF_TEST_TREE_H__
#define __HF_TEST_TREE_H__


#include "libhfgeom/hf_typedef.h"
#include "libhfgeom/hf_tree.h"

#include "libcorecommon/stopwatch.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace HFGeomTools {



template <class TE, class TC, TSize DIM>
class TreeSearch
{
public:
	typedef GVector<TE,DIM>	TVect;
	typedef Tree< TE, TC, TSize, DIM >	TTree;

	TreeSearch() : mTree( 1, 30)		{}

	void	Init( const TSize& n, const TE& min, const TE& max);

	void	InitTree();
	void	SearchTree( const TVect& vct);
	void	SearchPower( const TVect& vct);

	void	RangeSearchTree( const TVect& vct, const TE& dist);
	void	RangeSearchPower( const TVect& vct, const TE& dist);

private:
	typename TTree::TBox		mBox;

	TTree			mTree;
	vector<TVect>	mtabPoint;
};


template <class TE, class TC, TSize DIM>
void TreeSearch<TE,TC,DIM>::Init( const TSize& n, const TE& min, const TE& max)
{
	TVect vmin, vmax;

	for ( TSize idim=0; idim<DIM; ++idim)
	{
		vmin.rX( idim) = min;
		vmax.rX( idim) = max;
	}

	mBox = typename TTree::TBox( vmin, vmax);


	//mtabPoint.push_back( TVect( max/2, max/2) );
	//mtabPoint.push_back( TVect( max-1, max/2) );
	//mtabPoint.push_back( TVect( max-1, max-1) );
	//mtabPoint.push_back( TVect( min+1, min+1) );
	//mtabPoint.push_back( TVect( 0, 0) );
	//return;


	mtabPoint.resize( n);

	for ( TSize i=0; i<n; ++i)
	{
		TVect v;
		for ( TSize idim=0; idim<DIM; ++idim)
		{
			TC dist = TC( max) - TC( min);
			TC d = dist * TC(rand());
			d = d / TC( RAND_MAX);
			d = d + TC( min);
			v.rX( idim) = TE( d);
		}

		mtabPoint[i] = v;
	}

	sort( mtabPoint.begin(), mtabPoint.end() );

	mtabPoint.erase( unique( mtabPoint.begin(), mtabPoint.end() ), mtabPoint.end() );

	cout << mtabPoint.size() << " " << n << endl;

	cout << endl;
	//for ( TSize i=0; i<mtabPoint.size(); ++i)
	//	mtabPoint[i].Write();
}


template <class TE, class TC, TSize DIM>
void TreeSearch<TE,TC,DIM>::InitTree()
{
	mTree.InitBox( mBox);

	for ( TSize i=0; i<mtabPoint.size(); ++i)
	{
		//mtabPoint[i].Write();
		mTree.Insert( mtabPoint[i], i );
	}
}


template <class TE, class TC, TSize DIM>
void TreeSearch<TE,TC,DIM>::SearchPower( const TVect& vct)
{
	TSize ind = 0;
	TVect vres = mtabPoint[0];

	GVector<TC,DIM> vc = vct - vres;
	TC dist = vc * vc;

	for ( TSize i=0; i<mtabPoint.size(); ++i)
	{
		vc = vct - mtabPoint[i];
		TC distnew = vc * vc;
		if ( dist > distnew)
		{
			dist = distnew;
			vres = mtabPoint[i];
			ind = i;
		}
	}

	cout << "index = " << ind << "; dist = " << sqrt(dist) << "; ";
	vres.Write();
	cout << endl;
}



template <class TE, class TC, TSize DIM>
void TreeSearch<TE,TC,DIM>::SearchTree( const TVect& vct)
{
	typename TTree::TPair	res;

	bool bfound;
	for ( TSize i=0; i<100; ++i)
		bfound = mTree.GetClosestItem( res, vct );


	if ( bfound)
	{
		GVector<TC,DIM> vc = vct - res.first;
		TC dist = vc * vc;

		cout << "index = " << res.second << "; dist = " << sqrt(dist) << "; ";
		res.first.Write();
	}
	else
		cout << "problem..." << endl;
}

template <class TE, class TC, TSize DIM>
void TreeSearch<TE,TC,DIM>::RangeSearchTree( const TVect& vct, const TE& dist)
{
	vector< typename TTree::TPair >	tabres;

	cout << endl << "max dist = " << dist << endl;

	bool bfound = mTree.GetRangeItems( tabres, vct, dist );

	if ( bfound)
	{
		TSize count = 0;
		for ( TSize i=0; i<tabres.size(); ++i)
		{
			GVector<TC,DIM> vc = TVect(vct) - TVect(tabres[i].first);
			TC distnew = vc * vc;

			cout << "index = " << tabres[i].second << "; dist = " << sqrt(distnew) << "; ";
			tabres[i].first.Write();
			++count;
		}
		cout << "count = " << count << endl << endl;
	}
	else
		cout << "problem..." << endl;
}


template <class TE, class TC, TSize DIM>
void TreeSearch<TE,TC,DIM>::RangeSearchPower( const TVect& vct, const TE& dist)
{
	TSize ind = 0;
	TVect vres = mtabPoint[0];

	GVector<TC,DIM> vc;
	
	cout << endl << "max dist = " << dist << endl;

	TSize count = 0;
	for ( TSize i=0; i<mtabPoint.size(); ++i)
	{
		vc = TVect(vct) - TVect(mtabPoint[i]);
		TC distnew = vc * vc;
		if ( TC(dist)*TC(dist) >= distnew)
		{
			cout << "index = " << i << "; dist = " << sqrt(distnew) << "; ";
			mtabPoint[i].Write();
			//cout << endl;
			++count;
		}
	}

	cout << "count = " << count << endl << endl;
}



//////////////////////////////////////////////////////////////////////
// class Test_Tree
//////////////////////////////////////////////////////////////////////
class Test_Tree
{
public:
	bool	Run();

//private:

	template <class TE, class TC, TSize DIM>
	bool CheckSearch( const TSize& n, const TE& min, const TE& max);

	template <class TE, class TC, TSize DIM>
	bool CheckRangeSearch( const TSize& n, const TE& dist, const TE& min, const TE& max);

};
//////////////////////////////////////////////////////////////////////


template <class TE, class TC, TSize DIM>
bool Test_Tree::CheckRangeSearch( const TSize& n, const TE& dist, const TE& min, const TE& max)
{
	cout << endl << "============================================" << endl;
	StopWatch	swatch;

	TreeSearch<TE, TC, DIM>			test;
	typename TreeSearch<TE, TC, DIM>::TVect	vct;

	for ( TSize i=0; i<DIM; ++i)
		vct.rX( i ) = 100;

	cout << " -- init points  ";
	swatch.Reset(); swatch.Start();
		test.Init( n, min, max);
	swatch.Mark();
	cout << " :: time " << setprecision(6) << swatch.cDuration() << endl;

	cout << " -- init tree    ";
	swatch.Reset(); swatch.Start();
		test.InitTree();
	swatch.Mark();
	cout << " :: time " << setprecision(6) << swatch.cDuration() << endl;

	cout << " -- power search ";
	swatch.Reset(); swatch.Start();
		test.RangeSearchPower( vct, dist);
	swatch.Mark();
	cout << " :: time " << setprecision(6) << swatch.cDuration() << endl;

	cout << " -- tree search  ";
	swatch.Reset(); swatch.Start();
		test.RangeSearchTree( vct, dist);
	swatch.Mark();
	cout << " :: time " << setprecision(6) << swatch.cDuration() << endl;

	return true;
}

template <class TE, class TC, TSize DIM>
bool Test_Tree::CheckSearch( const TSize& n, const TE& min, const TE& max)
{
	cout << endl << "============================================" << endl;
	StopWatch	swatch;

	TreeSearch<TE, TC, DIM>			test;
	typename TreeSearch<TE, TC, DIM>::TVect	vct;

	for ( TSize i=0; i<DIM; ++i)
		vct.rX( i ) = 100;

	cout << " -- init points  ";
	swatch.Reset(); swatch.Start();
		test.Init( n, min, max);
	swatch.Mark();
	cout << " :: time " << setprecision(6) << swatch.cDuration() << endl;

	cout << " -- init tree    ";
	swatch.Reset(); swatch.Start();
		test.InitTree();
	swatch.Mark();
	cout << " :: time " << setprecision(6) << swatch.cDuration() << endl;

	cout << " -- power search ";
	swatch.Reset(); swatch.Start();
		test.SearchPower( vct);
	swatch.Mark();
	cout << " :: time " << setprecision(6) << swatch.cDuration() << endl;

	cout << " -- tree search  ";
	swatch.Reset(); swatch.Start();
		test.SearchTree( vct);
	swatch.Mark();
	cout << " :: time " << setprecision(6) << swatch.cDuration() << endl;

	return true;
}


bool Test_Tree::Run()
{
	//CheckRangeSearch< SInt32, sbigint<64>, 3 >( 10000000, 27374182, -1073741823, 1073741823);		// 2^30 - 1
	//CheckRangeSearch< Float64, Float64, 3 >( 10000000, 10374182, -1073741823, 1073741823);		// 2^30 - 1
	CheckRangeSearch< SInt32, sbigint<96>, 3 >( 10000000, 10374182, -1073741823, 1073741823);		// 2^30 - 1
	
	//CheckSearch< SInt32, SInt64, 3 >( 200000, -1073741823, 1073741823);		// 2^30 - 1


	//CheckSearch< SInt32, SInt64, 3 >( 2000000, -1073741823, 1073741823);		// 2^30 - 1
	//CheckSearch< SInt32, sbigint<64>, 3 >( 2000000, -1073741823, 1073741823);		// 2^30 - 1
	//CheckSearch< SInt32, sbigint<160>, 3 >( 2000000, -1073741823, 1073741823);		// 2^30 - 1

	//CheckSearch< SInt64, sbigint<160>, 3 >( 4000000, -4611686018427387903LL, 4611686018427387903LL);		// 2^62 - 1

	//CheckSearch< Float64, Float64, 3 >( 4000000, -4611686018427387903LL, 4611686018427387903LL);		// 2^62 - 1

	return true;
}



} // end of namespace HFGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace HFGeom = HFGeomTools;



#endif // __HF_TEST_TREE_H__
