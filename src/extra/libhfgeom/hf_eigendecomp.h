#ifndef __EIGENDECOMP_H__
#define __EIGENDECOMP_H__


//#include "sysdecl.h"
#include "libcoresystem/mgdecl.h"
#include "libhfgeom/hf_typedef.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace HFGeomTools {


//////////////////////////////////////////////////////////////////////
// class EigenDecomp
// - T - should be floating point type
//////////////////////////////////////////////////////////////////////
template< class T, class VECTOR, class MATRIX>
class EigenDecomp
{
	enum { SMTX_MAX_ROTATIONS = 30 };
public:
	typedef TSize	size_type;

	EigenDecomp( const MATRIX& mtx);

	void	Execute();
	T		GetDeterminant();

	void	AssembleProjMtx( MATRIX& mtx) const;
	void	AssembleMtxROOT( MATRIX& mtx) const;
	void	AssembleMtxSQR( MATRIX& mtx) const;

	const MATRIX&	cMatrixR() const	{ return mmtxL; }
	const VECTOR&	cVectorD() const	{ return mvecD; }

protected:
	void	RotateMatrix( MATRIX& a, T& g, T& h, T& s, T& tau, const TSize& i, const TSize& j, const TSize& k, const TSize& l );

private:
	size_type	mN;
	MATRIX		mmtxA;
	MATRIX		mmtxL;
	VECTOR		mvecD;
};
//////////////////////////////////////////////////////////////////////


template< class T, class VECTOR, class MATRIX>
EigenDecomp<T,VECTOR,MATRIX>::EigenDecomp( const MATRIX& mtx) : mN(mtx.NRows()), mmtxA(mtx)
{
	if ( mtx.NRows() != mtx.NCols() )
		THROW_INTERNAL( "EigenDecomp : EigenDecomp : square matrix expected");
}


template< class T, class VECTOR, class MATRIX>
void EigenDecomp<T,VECTOR,MATRIX>::Execute()
{
	T eps = numeric_limits<T>::epsilon();
	
	//T tresh, theta, tau, t, sm, s, h, g, c, tmp;
	T tresh, t, theta, c, s, tau;

	VECTOR	b;
	VECTOR	z;
	VECTOR	w;


	// initialize
	mmtxL.InitDiagUni( mN);


	for ( TSize ip=0; ip<mN; ++ip) 
	{
		b(ip) = w(ip) = mmtxA(ip,ip);
		z(ip) = 0.0;
	}

	// begin rotation sequence
	TSize i;
	for ( i=0; i<SMTX_MAX_ROTATIONS; ++i) 
	{
		T sm = T(0);
		for ( TSize ip=0; ip<mN-1; ++ip) 
		{
			for ( TSize iq=ip+1; iq<mN; ++iq)
			{
				sm += abs( mmtxA(ip,iq) );
			}
		}

		if ( sm == T(0) )
			break;

		if (i < 3)                                // first 3 sweeps
			tresh = T(0.2) * sm / T(mN*mN);
		else
			tresh = T(0);


		for ( TSize ip=0; ip<mN-1; ++ip) 
		{
			for ( TSize iq=ip+1; iq<mN; ++iq) 
			{
				T g = T(100) * abs( mmtxA(ip,iq) );

				// after 4 sweeps
				if ( i > 3 && g <= eps * abs( w(ip)) && g <= eps * abs( w(iq)) )
				{
					mmtxA(ip,iq) = T(0);
				}
				else if ( abs( mmtxA(ip,iq) ) > tresh) 
				{
					T h = w(iq) - w(ip);

					if ( g <= eps * abs( h) )
					{
						t = ( mmtxA(ip,iq) ) / h;
					} 
					else 
					{
						theta = T(0.5)*h / ( mmtxA(ip,iq) );
						t = T(1) / ( abs(theta) + sqrt( T(1) + theta*theta) );

						if ( theta < T(0) )
							t = -t;
					}

					c = T(1) / sqrt( T(1) + t*t);
					s = t*c;
					tau = s / (T(1) + c);
					h = t*mmtxA(ip,iq);
					z(ip) -= h;
					z(iq) += h;
					w(ip) -= h;
					w(iq) += h;
					mmtxA(ip,iq) = T(0);

					for ( TSize j = 0; j < ip; ++j) 
						RotateMatrix( mmtxA, g, h, s, tau, j, ip, j, iq );

					for ( TSize j = ip+1; j < iq; ++j) 
						RotateMatrix( mmtxA, g, h, s, tau, ip, j, j, iq );

					for ( TSize j=iq+1; j<mN; ++j) 
						RotateMatrix( mmtxA, g, h, s, tau, ip, j, iq, j );

					for ( TSize j=0; j<mN; ++j) 
						//RotateMatrix( mmtxL, g, h, s, tau, j, ip, j, iq );
						RotateMatrix( mmtxL, g, h, s, tau, ip, j, iq, j );

				}
			}

		}

		for ( TSize ip=0; ip<mN; ++ip) 
		{
			b(ip) += z(ip);
			w(ip) = b(ip);
			z(ip) = T(0);
		}
	}

	// this should NEVER be called
	if ( i >= SMTX_MAX_ROTATIONS )
	{
		mmtxA.Write();
		THROW_INTERNAL( "SMatrix Decompose error: Error extracting eigenfunctions");
	}


	if ( false)
	//if ( bsort)
	{
		// sort eigenfunctions                 these changes do not affect accuracy 
		for ( TSize j=0; j<mN-1; ++j)                  // boundary incorrect
		{
			TSize k = j;
			T tmp = w(k);
			for ( TSize i=j+1; i<mN; ++i)                // boundary incorrect, shifted already
			{
				if (w(i) >= tmp)                   // why exchage if same?
				{
					k = i;
					tmp = w(k);
				}
			}
			if ( k != j) 
			{
				w(k) = w(j);
				w(j) = tmp;

				for ( TSize i=0; i<mN; ++i) 
				{
					T tmp = mmtxL(i,j);
					mmtxL(i,j) = mmtxL(i,k);
					mmtxL(i,k) = tmp;
				}
			}
		}
	}

	for ( TSize ip=0; ip<mN; ++ip) 
		mvecD(ip) = w(ip);

	//cout << "mmtxA" << endl;
	//mmtxA.Write();

	//cout << "mmtxL" << endl;
	//mmtxL.Write();

	return;
}

template< class T, class VECTOR, class MATRIX>
inline void EigenDecomp<T,VECTOR,MATRIX>::RotateMatrix( MATRIX& a, T& g, T& h, T& s, T& tau, const TSize& i, const TSize& j, const TSize& k, const TSize& l )
{
	g = a(i,j);
	h = a(k,l);
	a(i,j) = g - s*( h+g*tau );
	a(k,l) = h + s*( g-h*tau );
} 


template< class T, class VECTOR, class MATRIX>
inline T EigenDecomp<T,VECTOR,MATRIX>::GetDeterminant()
{
	T det = mvecD(0);
	for ( TSize i=1; i<mN; ++i)
		det *= mvecD(i);

	return det;
}


template< class T, class VECTOR, class MATRIX>
inline void EigenDecomp<T,VECTOR,MATRIX>::AssembleProjMtx( MATRIX& mtx) const
{
	for ( TSize i=0; i<mN; ++i)
	{
		//T lam = sqrt( mvecD(i) );
		T lam = sqrt( fabs(mvecD(i)) );
		for ( TSize j=0; j<mN; ++j)
			mtx(i,j) = mmtxL(i,j) * lam;
	}
}

template< class T, class VECTOR, class MATRIX>
inline void EigenDecomp<T,VECTOR,MATRIX>::AssembleMtxROOT( MATRIX& mtx) const
{
	mtx.Resize( mmtxL.NRows(), mmtxL.NCols() );

	for( TSize i=0; i<mmtxL.NRows(); ++i)
		for( TSize j=0; j<mmtxL.NCols(); ++j)
		{
			mtx(i,j) = 0.0;
			for( TSize k=0; k<mmtxL.NCols(); ++k)
				mtx(i,j) += mmtxL(i,k) * ( T(1) / sqrt( mvecD(k) ) ) * mmtxL(k,j);
		}
}

template< class T, class VECTOR, class MATRIX>
inline void EigenDecomp<T,VECTOR,MATRIX>::AssembleMtxSQR( MATRIX& mtx) const
{
	mtx.Resize( mmtxL.NRows(), mmtxL.NCols() );

	for( TSize i=0; i<mmtxL.NRows(); ++i)
		for( TSize j=0; j<mmtxL.NCols(); ++j)
		{
			mtx(i,j) = 0.0;
			for( TSize k=0; k<mmtxL.NCols(); ++k)
				mtx(i,j) += mmtxL(i,k) * ( T(1) / (mvecD(k) * mvecD(k)) ) * mmtxL(k,j);
		}
}



} // end of namespace HFGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace HFGeom = HFGeomTools;

#endif // __EIGENDECOMP_H__
