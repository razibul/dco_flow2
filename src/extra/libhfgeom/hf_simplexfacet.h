#ifndef __HF_SIMPLEXFACET_H__
#define __HF_SIMPLEXFACET_H__

#include "libhfgeom/hf_typedef.h"
#include "libhfgeom/hf_simplexbase.h"


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
namespace HFGeomTools {



//////////////////////////////////////////////////////////////////////
// class SimplexFacet
//////////////////////////////////////////////////////////////////////
template < class T, TDimension DIM, template< class T, size_t N > class DATA = ArrayData >
class SimplexFacet : public SimplexBase<T,DIM-1,DIM,DATA>
{
public:
	typedef SimplexBase<T,DIM-1,DIM,DATA>	TBase;
	typedef typename TBase::GVect	GVect;
	typedef typename TBase::ARRAY	ARRAY;
	
	enum { SIZE = TBase::SIZE };

public:
	SimplexFacet( const ARRAY& data) : TBase( data)	{}


	template <class TC>
	bool	IsAbove( const GVect& vct) const
	{
		GMatrix<TC,DIM> mtx(DIM,DIM);

		for ( TSize i=0; i<DIM-1; ++i)
			for ( TSize j=0; j<DIM; ++j)
				mtx(i,j) = TC( this->mData[i+1].cX(j)) - TC( this->mData[0].cX(j) );

		for ( TSize j=0; j<DIM; ++j)
			mtx(DIM-1,j) = TC( vct.cX(j)) - TC( this->mData[0].cX(j) );

		//mtx.Write();

		return ( DeterminantFunc( mtx) > TC(0) );

		//Determinant< TC, GMatrix<TC,DIM> >  det(mtx);
		//det.Execute();

		//return ( det.GetDeterminant() > TC(0) );
	}

};




} // end of namespace HFGeomTools
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//

namespace HFGeom = HFGeomTools;


#endif // __HF_SIMPLEXFACET_H__

